package com.vanmarcke.email.converters.impl;

import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.vanmarcke.core.model.IBMOrderModel;
import com.vanmarcke.core.model.VanmarckeContactModel;
import com.vanmarcke.email.dto.AddressDTO;
import com.vanmarcke.email.dto.SuborderDTO;
import com.vanmarcke.email.dto.TransportInformationDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import jersey.repackaged.com.google.common.collect.Sets;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link MandrillTransportInformationConverterTest} class contains the unit tests for the
 * {@link MandrillTransportInformationConverter} class.
 *
 * @author Tom van den Berg, Christiaan Janssen
 * @since 16-09-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MandrillTransportInformationConverterTest {

    private static final String TRANSPORT_INFORMATION_VAR_NAME = "transport";
    private static final String B2B_UNIT_ID = RandomStringUtils.randomAlphabetic(10);

    private static final String LOCALE = "en";
    private static final String ORDER_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String USER_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String DELIVERY_COMMENT = RandomStringUtils.randomAlphabetic(10);
    private static final String CONTACT_PERSON_EMAIL = RandomStringUtils.randomAlphabetic(10);
    private static final String CONTACT_PERSON_TELEPHONE = RandomStringUtils.randomAlphabetic(10);
    private static final String PRODUCT_SUMMARY = RandomStringUtils.randomAlphabetic(10);
    private static final double BASE_PRICE = 79.00;
    private static final double TOTAL_PRICE = 79.00;
    private static final Date DATE = Date.from(new Calendar.Builder()
            .setDate(2001, 3, 3)
            .setTimeOfDay(14, 50, 0).build().toInstant());

    private static final String ADDRESS_LINE1 = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_LINE2 = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_TOWN = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_FIRST_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_LAST_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_POSTAL_CODE = "3500";
    private static final String ADDRESS_TELEPHONE = "012 15 15 15";
    private static final String ADDRESS_COUNTRY = "België";
    private static final String ADDRESS_COMPANY = RandomStringUtils.randomAlphabetic(10);
    private static final String ADDRESS_COMPANY_NAME = RandomStringUtils.randomAlphabetic(10);

    private static final long QUANTITY = 1L;
    private static final String SKU = RandomStringUtils.randomAlphabetic(10);

    @InjectMocks
    private MandrillTransportInformationConverter converter;

    @Test
    public void testConvert() {
        // given
        final B2BUnitModel company = mock(B2BUnitModel.class);
        when(company.getUid()).thenReturn(B2B_UNIT_ID);
        when(company.getName()).thenReturn(ADDRESS_COMPANY_NAME);

        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        when(user.getName()).thenReturn(USER_NAME);
        when(user.getDefaultB2BUnit()).thenReturn(company);

        final VanmarckeContactModel contactPerson = mock(VanmarckeContactModel.class);
        when(contactPerson.getEmail()).thenReturn(CONTACT_PERSON_EMAIL);
        when(contactPerson.getTelephone()).thenReturn(CONTACT_PERSON_TELEPHONE);

        final CurrencyModel currency = mock(CurrencyModel.class);

        final ProductModel product = mock(ProductModel.class);
        when(product.getCode()).thenReturn(SKU);
        when(product.getSummary(Locale.ENGLISH)).thenReturn(PRODUCT_SUMMARY);

        final OrderEntryModel orderEntry = mock(OrderEntryModel.class);
        when(orderEntry.getQuantity()).thenReturn(QUANTITY);
        when(orderEntry.getProduct()).thenReturn(product);
        when(orderEntry.getDeliveryDate()).thenReturn(DATE);
        when(orderEntry.getBasePrice()).thenReturn(BASE_PRICE);
        when(orderEntry.getTotalPrice()).thenReturn(TOTAL_PRICE);

        final IBMOrderModel suborder = mock(IBMOrderModel.class);
        when(suborder.getCode()).thenReturn(ORDER_CODE);
        when(suborder.getEntries()).thenReturn(Sets.newHashSet(orderEntry));

        final CountryModel country = mock(CountryModel.class);

        final AddressModel shippingAddressModel = mock(AddressModel.class);
        when(shippingAddressModel.getLine1()).thenReturn(ADDRESS_LINE1);
        when(shippingAddressModel.getLine2()).thenReturn(ADDRESS_LINE2);
        when(shippingAddressModel.getTown()).thenReturn(ADDRESS_TOWN);
        when(shippingAddressModel.getFirstname()).thenReturn(ADDRESS_FIRST_NAME);
        when(shippingAddressModel.getLastname()).thenReturn(ADDRESS_LAST_NAME);
        when(shippingAddressModel.getPostalcode()).thenReturn(ADDRESS_POSTAL_CODE);
        when(shippingAddressModel.getPhone1()).thenReturn(ADDRESS_TELEPHONE);
        when(shippingAddressModel.getCountry()).thenReturn(country);
        when(country.getName(Locale.ENGLISH)).thenReturn(ADDRESS_COUNTRY);
        when(shippingAddressModel.getPhone1()).thenReturn(ADDRESS_TELEPHONE);
        when(shippingAddressModel.getCompany()).thenReturn(ADDRESS_COMPANY);

        final AddressModel paymentAddressModel = mock(AddressModel.class);
        when(paymentAddressModel.getLine1()).thenReturn(ADDRESS_LINE1);
        when(paymentAddressModel.getLine2()).thenReturn(ADDRESS_LINE2);
        when(paymentAddressModel.getTown()).thenReturn(ADDRESS_TOWN);
        when(paymentAddressModel.getFirstname()).thenReturn(ADDRESS_FIRST_NAME);
        when(paymentAddressModel.getLastname()).thenReturn(ADDRESS_LAST_NAME);
        when(paymentAddressModel.getPostalcode()).thenReturn(ADDRESS_POSTAL_CODE);
        when(paymentAddressModel.getPhone1()).thenReturn(ADDRESS_TELEPHONE);
        when(paymentAddressModel.getCountry()).thenReturn(country);
        when(country.getName(Locale.ENGLISH)).thenReturn(ADDRESS_COUNTRY);
        when(paymentAddressModel.getPhone1()).thenReturn(ADDRESS_TELEPHONE);
        when(paymentAddressModel.getCompany()).thenReturn(ADDRESS_COMPANY);
        when(company.getName()).thenReturn(ADDRESS_COMPANY_NAME);

        final OrderModel order = mock(OrderModel.class);
        when(order.getLocale()).thenReturn(LOCALE);
        when(order.getCurrency()).thenReturn(currency);
        when(order.getUser()).thenReturn(user);
        when(order.getContactPerson()).thenReturn(contactPerson);
        when(order.getEntries()).thenReturn(Collections.singletonList(orderEntry));
        when(order.getDeliveryAddress()).thenReturn(shippingAddressModel);
        when(order.getDeliveryComment()).thenReturn(DELIVERY_COMMENT);
        when(order.getPaymentAddress()).thenReturn(paymentAddressModel);
        when(order.getIbmOrders()).thenReturn(Sets.newHashSet(suborder));

        when(orderEntry.getOrder()).thenReturn(order);

        // when
        final List<MandrillMessage.MergeVarBucket> mergeVarBucket = converter.convert(order);

        // then
        assertThat(mergeVarBucket).isNotEmpty();
        MandrillMessage.MergeVarBucket resultEntry = mergeVarBucket.get(0);
        MandrillMessage.MergeVar[] vars = resultEntry.getVars();
        assertThat(vars).isNotEmpty();
        MandrillMessage.MergeVar var = vars[0];
        assertThat(var.getName()).isEqualTo(TRANSPORT_INFORMATION_VAR_NAME);

        final TransportInformationDTO result = (TransportInformationDTO) mergeVarBucket.get(0).getVars()[0].getContent();
        assertThat(result.getCustomer_id()).isEqualTo(B2B_UNIT_ID);
        assertThat(result.getContact_person_telephone()).isEqualTo(CONTACT_PERSON_TELEPHONE);
        assertThat(result.getContact_person_email()).isEqualTo(CONTACT_PERSON_EMAIL);
        assertThat(result.getTransport_information()).isEqualTo(DELIVERY_COMMENT);
        assertThat(result.getSuborders()).hasSize(1);

        final SuborderDTO suborderDTO = result.getSuborders().get(0);
        assertThat(suborderDTO.getCode()).isEqualTo(ORDER_CODE);
        assertThat(suborderDTO.getDelivery_date()).isEqualTo("03-04-2001");
        assertThat(suborderDTO.getOrder_entries()).hasSize(1);

        final AddressDTO shippingAddressDTO = result.getShipment_address();
        assertThat(shippingAddressDTO.getAddress1()).isEqualTo(ADDRESS_LINE1);
        assertThat(shippingAddressDTO.getAddress2()).isEqualTo(ADDRESS_LINE2);
        assertThat(shippingAddressDTO.getCity()).isEqualTo(ADDRESS_TOWN);
        assertThat(shippingAddressDTO.getPostal_code()).isEqualTo(ADDRESS_POSTAL_CODE);
        assertThat(shippingAddressDTO.getTelephone()).isEqualTo(ADDRESS_TELEPHONE);
        assertThat(shippingAddressDTO.getCountry()).isEqualTo(ADDRESS_COUNTRY);
        assertThat(shippingAddressDTO.getName()).isEqualTo(ADDRESS_FIRST_NAME + " " + ADDRESS_LAST_NAME);

        final AddressDTO paymentAddressDTO = result.getPayment_address();
        assertThat(paymentAddressDTO.getAddress1()).isEqualTo(ADDRESS_LINE1);
        assertThat(paymentAddressDTO.getAddress2()).isEqualTo(ADDRESS_LINE2);
        assertThat(paymentAddressDTO.getCity()).isEqualTo(ADDRESS_TOWN);
        assertThat(paymentAddressDTO.getPostal_code()).isEqualTo(ADDRESS_POSTAL_CODE);
        assertThat(paymentAddressDTO.getTelephone()).isEqualTo(ADDRESS_TELEPHONE);
        assertThat(paymentAddressDTO.getCountry()).isEqualTo(ADDRESS_COUNTRY);
        assertThat(paymentAddressDTO.getName()).isEqualTo(ADDRESS_FIRST_NAME + " " + ADDRESS_LAST_NAME);
    }
}