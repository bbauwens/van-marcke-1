package com.vanmarcke.email.converters.impl;

import com.google.common.collect.Sets;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.vanmarcke.core.model.IBMOrderModel;
import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.email.dto.CustomerInteractionDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.OrderModel;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link MandrillCustomerInteractionConverterTest} class contains the unit tests for the
 * {@link MandrillCustomerInteractionConverter} class.
 *
 * @author Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 05-01-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MandrillCustomerInteractionConverterTest {

    private static final String B2B_UNIT_ID = RandomStringUtils.randomAlphabetic(10);
    private static final String EXTERNAL_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String CUSTOMER_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String MESSAGE = RandomStringUtils.randomAlphabetic(10);

    @InjectMocks
    private MandrillCustomerInteractionConverter mandrillCustomerInteractionConverter;

    @Test
    public void testConvert() {
        // given
        final B2BUnitModel company = mock(B2BUnitModel.class);
        when(company.getUid()).thenReturn(B2B_UNIT_ID);

        final B2BCustomerModel user = mock(B2BCustomerModel.class);
        when(user.getName()).thenReturn(CUSTOMER_NAME);
        when(user.getDefaultB2BUnit()).thenReturn(company);

        final IBMOrderModel suborder = mock(IBMOrderModel.class);
        when(suborder.getCode()).thenReturn(EXTERNAL_CODE);

        final OrderModel order = mock(OrderModel.class);
        when(order.getUser()).thenReturn(user);
        when(order.getIbmOrders()).thenReturn(Sets.newHashSet(suborder));

        final OrderCustomerInteractionModel customerInteraction = mock(OrderCustomerInteractionModel.class);
        when(customerInteraction.getOrder()).thenReturn(order);
        when(customerInteraction.getMessage()).thenReturn(MESSAGE);

        // when
        final List<MandrillMessage.MergeVarBucket> result = mandrillCustomerInteractionConverter.convert(customerInteraction);

        // then
        assertThat(result).hasSize(1);
        assertThat(result.get(0)).isNotNull();
        assertThat(result.get(0).getVars()).hasSize(1);
        assertThat(result.get(0).getVars()[0]).isNotNull();
        assertThat(result.get(0).getVars()[0].getName()).isEqualTo("ci");

        final CustomerInteractionDTO customerInteractionDTO = (CustomerInteractionDTO) result.get(0).getVars()[0].getContent();
        assertThat(customerInteractionDTO).isNotNull();
        assertThat(customerInteractionDTO.getCustomer_name()).isEqualTo(CUSTOMER_NAME);
        assertThat(customerInteractionDTO.getMessage()).isEqualTo(MESSAGE);
        assertThat(customerInteractionDTO.getCustomer_id()).isEqualTo(B2B_UNIT_ID);
        assertThat(customerInteractionDTO.getSuborders()).hasSize(1);
        assertThat(customerInteractionDTO.getSuborders().get(0)).isNotNull();
        assertThat(customerInteractionDTO.getSuborders().get(0).getCode()).isEqualTo(EXTERNAL_CODE);
    }
}