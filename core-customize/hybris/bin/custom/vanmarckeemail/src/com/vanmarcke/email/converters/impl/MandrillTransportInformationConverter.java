package com.vanmarcke.email.converters.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.vanmarcke.email.converters.MandrillAbstractOrderConverter;
import com.vanmarcke.email.dto.TransportInformationDTO;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Collections;
import java.util.List;

/**
 * Implements methods to prepare data for the transport information emails.
 *
 * @author Tom van den Berg
 * @since 15-09-2020
 */
public class MandrillTransportInformationConverter extends MandrillAbstractOrderConverter<OrderModel> {

    private static final String TRANSPORT_INFORMATION_VAR_NAME = "transport";

    /**
     * Creates a new instance of the {@link MandrillTransportInformationConverter} class.
     *
     * @param messageFacade the message facade
     * @param i18NService   the I18N service
     */
    protected MandrillTransportInformationConverter(final LocalizedMessageFacade messageFacade,
                                                    final I18NService i18NService) {
        super(messageFacade, i18NService);
    }

    /**
     * Converts an order into the appropriate format for Mandrill.
     *
     * @param orderModel the order model
     * @return The object represented by a MergeVarBucket.
     */
    @Override
    public List<MandrillMessage.MergeVarBucket> convert(final OrderModel orderModel) {
        final TransportInformationDTO transportDTO = new TransportInformationDTO();
        transportDTO.setCustomer_name(orderModel.getUser().getName());
        transportDTO.setCustomer_id(getB2BUnitId(orderModel.getUser()));
        transportDTO.setTransport_information(orderModel.getDeliveryComment());
        transportDTO.setPayment_address(populatePaymentAddress(orderModel));
        transportDTO.setShipment_address(populateShipmentAddress(orderModel));
        transportDTO.setSuborders(getSuborders(orderModel));

        if (orderModel.getContactPerson() != null) {
            transportDTO.setContact_person_email(orderModel.getContactPerson().getEmail());
            transportDTO.setContact_person_telephone(orderModel.getContactPerson().getTelephone());
        }

        return createMergeVarBucket(transportDTO);
    }

    /**
     * Returns a list of {@link MandrillMessage.MergeVarBucket} instances based on the given Transport Information DTO.
     *
     * @param transportDTO the transport information DTO
     * @return a list of MergeVarBuckets
     */
    protected List<MandrillMessage.MergeVarBucket> createMergeVarBucket(final TransportInformationDTO transportDTO) {
        MandrillMessage.MergeVar transportVar = new MandrillMessage.MergeVar();
        transportVar.setName(TRANSPORT_INFORMATION_VAR_NAME);
        transportVar.setContent(transportDTO);
        MandrillMessage.MergeVarBucket orderBucket = new MandrillMessage.MergeVarBucket();
        orderBucket.setVars(new MandrillMessage.MergeVar[]{transportVar});
        return Collections.singletonList(orderBucket);
    }
}
