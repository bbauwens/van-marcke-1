package com.vanmarcke.email.converters;

import be.elision.mandrillextension.converter.MandrillConverter;
import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.core.model.IBMOrderModel;
import com.vanmarcke.email.dto.AddressDTO;
import com.vanmarcke.email.dto.OrderDTO;
import com.vanmarcke.email.dto.OrderEntryDTO;
import com.vanmarcke.email.dto.SuborderDTO;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.LocaleUtils.toLocale;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

/**
 * Converter for the basic information on orders
 */
public abstract class MandrillAbstractOrderConverter<T extends AbstractOrderModel> implements MandrillConverter<T> {

    private static final String DELIVERY_DATE_FORMAT = "dd-MM-yyyy";

    protected final LocalizedMessageFacade messageFacade;
    protected final I18NService i18NService;
    private final ConcurrentMap<String, NumberFormat> currencyFormats = new ConcurrentHashMap<>();

    protected MandrillAbstractOrderConverter(LocalizedMessageFacade messageFacade, I18NService i18NService) {
        this.messageFacade = messageFacade;
        this.i18NService = i18NService;
    }

    protected void populate(final AbstractOrderModel source, final OrderDTO target) {
        CustomerModel user = (CustomerModel) source.getUser();
        Locale locale = LocaleUtils.toLocale(source.getLocale());
        i18NService.setLocalizationFallbackEnabled(true);

        target.setCustomer_name(user.getName());

        //Set price information
        populateCurrency(source, target);
        target.setDelivery_cost(formatPrice(source.getDeliveryCost(), source));
        target.setTax(formatPrice(source.getTotalTax(), source));
        target.setSub_total(formatPrice(source.getSubtotal(), source));
        target.setTotal(formatPrice(source.getTotalPrice() + source.getTotalTax(), source));

        //Set order entries


        populateGeneralInformation(source, target, locale);
    }

    /**
     * Populates the currency of the order on the OrderDTO.
     *
     * @param source the abstract order model
     * @param target the order DTO
     */
    private void populateCurrency(AbstractOrderModel source, OrderDTO target) {
        if (source.getCurrency() != null) {
            target.setCurrency(source.getCurrency().getSymbol());
        }
    }

    protected OrderEntryDTO convertOrderEntry(AbstractOrderEntryModel orderEntry, Locale locale) {
        ProductModel productModel = orderEntry.getProduct();
        OrderEntryDTO orderEntryDTO = new OrderEntryDTO();
        orderEntryDTO.setSummary(productModel.getSummary(locale) == null ? "" : productModel.getSummary(locale));
        orderEntryDTO.setQuantity(orderEntry.getQuantity().toString());
        orderEntryDTO.setUnit_price(formatPrice(orderEntry.getBasePrice(), orderEntry.getOrder()));
        orderEntryDTO.setTotal_price(formatPrice(orderEntry.getTotalPrice(), orderEntry.getOrder()));
        orderEntryDTO.setSku(productModel.getCode());
        return orderEntryDTO;
    }

    protected String formatPrice(double price, final AbstractOrderModel order) {
        final NumberFormat format = getCurrencyFormat(toLocale(order.getLocale()), order.getCurrency());
        return format.format(price);
    }

    protected NumberFormat getCurrencyFormat(final Locale locale, final CurrencyModel currency) {
        final String key = locale.toString() + "_" + currency.getIsocode();
        NumberFormat numberFormat = currencyFormats.get(key);
        if (numberFormat == null) {
            final NumberFormat currencyFormat = createCurrencyFormat(locale, currency);
            numberFormat = currencyFormats.putIfAbsent(key, currencyFormat);
            if (numberFormat == null) {
                numberFormat = currencyFormat;
            }
        }
        // don't allow multiple references
        return (NumberFormat) numberFormat.clone();
    }

    protected NumberFormat createCurrencyFormat(final Locale locale, final CurrencyModel currency) {
        final NumberFormat currencyFormat = DecimalFormat.getInstance(locale);
        currencyFormat.setMaximumFractionDigits(currency.getDigits());
        currencyFormat.setMinimumFractionDigits(currency.getDigits());
        return currencyFormat;
    }

    /**
     * Populates general information on the order DTO
     *
     * @param order    the order model
     * @param orderDTO the order DTO
     */
    private void populateGeneralInformation(AbstractOrderModel order, OrderDTO orderDTO, Locale locale) {
        if (order instanceof OrderModel) {
            orderDTO.setCompany_name_van_marcke(
                    (messageFacade.getMessageForCodeAndLocale("vanmarcke.company.name", locale)));
        }
    }

    /**
     * Returns the shipment address.
     *
     * @param order the order
     * @return the shipment address dto
     */
    protected AddressDTO populateShipmentAddress(OrderModel order) {
        AddressModel address = order.getDeliveryAddress();
        AddressDTO addressDTO = convertAddressToDTO(address, getCurrentLocale(order));
        setAddressName(address, addressDTO);
        return addressDTO;
    }

    /**
     * Returns the payment address.
     *
     * @return the payment address dto
     */
    protected AddressDTO populatePaymentAddress(OrderModel order) {
        AddressModel address = order.getPaymentAddress();
        AddressDTO addressDTO = convertAddressToDTO(address, getCurrentLocale(order));
        setAddressName(address, addressDTO);
        return addressDTO;
    }

    /**
     * Converts the given delivery {@link AddressModel} to an {@link AddressDTO}.
     *
     * @param addressModel the address model
     * @return the addres dto
     */
    protected AddressDTO convertAddressToDTO(AddressModel addressModel, Locale locale) {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAddress1(addressModel.getLine1());
        addressDTO.setAddress2(getAddressLine2(addressModel));
        addressDTO.setCity(addressModel.getTown());
        addressDTO.setPostal_code(addressModel.getPostalcode());
        addressDTO.setTelephone(addressModel.getPhone1());
        addressDTO.setMobile(addressModel.getPhone2());
        addressDTO.setCountry(addressModel.getCountry().getName(locale));
        return addressDTO;
    }

    /**
     * Returns the second address line for the given {@code address}.
     *
     * @param address the address
     * @return the second address line
     */
    private String getAddressLine2(AddressModel address) {
        String line2 = "";
        if (StringUtils.isNotEmpty(address.getLine2())) {
            line2 += address.getLine2();
        }
        if (StringUtils.isNotEmpty(address.getAppartment())) {
            line2 += " ";
            line2 += address.getAppartment();
        }
        return line2;
    }

    /**
     * Populates the name field on the Address DTO.
     *
     * @param addressDTO the address DTO
     * @param address    the address model
     */
    protected void setAddressName(AddressModel address, AddressDTO addressDTO) {
        if (StringUtils.isNotBlank(address.getFirstname()) || StringUtils.isNotBlank(address.getLastname())) {
            addressDTO.setName(trimToEmpty(address.getFirstname()) + " " + trimToEmpty(address.getLastname()));
        } else {
            addressDTO.setName(trimToEmpty(address.getCompany()));
        }
    }

    /**
     * Determines the current locale of the order.
     *
     * @param order the order
     * @return the locale
     */
    protected Locale getCurrentLocale(OrderModel order) {
        return org.apache.commons.lang.LocaleUtils.toLocale(order.getLocale());
    }

    /**
     * Retrieves the B2B Unit UID.
     *
     * @param customer the customer
     * @return the B2B Unit UID
     */
    protected String getB2BUnitId(UserModel customer) {
        if (customer instanceof B2BCustomerModel) {
            B2BCustomerModel b2bCustomer = (B2BCustomerModel) customer;
            if (b2bCustomer.getDefaultB2BUnit() != null) {
                return b2bCustomer.getDefaultB2BUnit().getUid();
            }
        }
        return null;
    }

    /**
     * Returns the suborder DTOs for the given {@code orderModel}.
     *
     * @param orderModel the order model
     * @return the suborder DTOs
     */
    protected List<SuborderDTO> getSuborders(final OrderModel orderModel) {
        final List<SuborderDTO> suborders = new ArrayList<>();
        final Locale locale = getCurrentLocale(orderModel);
        for (IBMOrderModel ibmOrder : orderModel.getIbmOrders()) {
            final SuborderDTO suborder = new SuborderDTO();
            suborder.setCode(ibmOrder.getCode());
            suborder.setDelivery_date(getDeliveryDate(ibmOrder));
            suborder.setOrder_entries(getOrderEntries(orderModel, ibmOrder, locale));
            suborders.add(suborder);
        }
        return suborders;
    }

    /**
     * Returns the order entries for the given {@code suborder}.
     *
     * @param orderModel the order model
     * @param suborder   the suborder model
     * @param locale     the locale to use for date, valuta, and currency formatting
     * @return the order entries
     */
    private List<OrderEntryDTO> getOrderEntries(final OrderModel orderModel,
                                                final IBMOrderModel suborder,
                                                final Locale locale) {
        return suborder.getEntries()
                .stream()
                .map(entry -> convertOrderEntry(entry, locale))
                .collect(Collectors.toList());
    }

    /**
     * Returns the delivery date for the given {@code suborder}.
     *
     * @param suborder the suborder
     * @return the delivery date
     */
    private String getDeliveryDate(final IBMOrderModel suborder) {
        return suborder.getEntries()
                .stream()
                .findFirst()
                .map(entry -> FastDateFormat.getInstance(DELIVERY_DATE_FORMAT).format(entry.getDeliveryDate()))
                .orElse(null);
    }
}