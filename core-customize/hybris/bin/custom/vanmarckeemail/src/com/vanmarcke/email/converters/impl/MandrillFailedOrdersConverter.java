package com.vanmarcke.email.converters.impl;

import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import com.vanmarcke.email.converters.MandrillAbstractFailedOrdersConverter;
import com.vanmarcke.email.dto.FailedOrdersDTO;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.Collections;
import java.util.List;

/**
 * Converter for failed orders
 *
 * @author Niels Raemaekers
 * @since 19-05-2020
 */
public class MandrillFailedOrdersConverter extends MandrillAbstractFailedOrdersConverter<List<OrderModel>> {

    private static final String FAILED_ORDERS_VAR_NAME = "orders";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MergeVarBucket> convert(List<OrderModel> allFailedOrders) {
        final FailedOrdersDTO failedOrdersDTO = new FailedOrdersDTO();

        super.populate(allFailedOrders, failedOrdersDTO);

        //Create MergeVarBucket to return
        final MergeVar orderVar = new MergeVar();
        orderVar.setName(FAILED_ORDERS_VAR_NAME);
        orderVar.setContent(failedOrdersDTO);
        final MergeVarBucket orderBucket = new MergeVarBucket();
        orderBucket.setVars(new MergeVar[]{orderVar});
        return Collections.singletonList(orderBucket);
    }
}
