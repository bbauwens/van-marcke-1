package com.vanmarcke.email.converters;

import be.elision.mandrillextension.converter.MandrillConverter;
import com.vanmarcke.email.dto.FailedOrderDTO;
import com.vanmarcke.email.dto.FailedOrdersDTO;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.ArrayList;
import java.util.List;

/**
 * Converter for the basic information on failed orders
 *
 * @author Niels Raemaekers
 * @since 19-05-2020
 */
public abstract class MandrillAbstractFailedOrdersConverter<T extends List<OrderModel>> implements MandrillConverter<T> {

    private static final String ORDER_DATE_FORMAT = "dd-MM-yyyy HH:mm";

    /**
     * Populate the target FailedOrderDTO list
     *
     * @param source the given source object
     * @param target the given target object
     */
    protected void populate(final List<OrderModel> source, final FailedOrdersDTO target) {
        List<FailedOrderDTO> allFailedOrders = new ArrayList<>();

        source.forEach(order -> {
            FailedOrderDTO failedOrderDTO = new FailedOrderDTO();
            CustomerModel user = (CustomerModel) order.getUser();

            failedOrderDTO.setOrder_number(order.getExternalCode());
            failedOrderDTO.setCustomer_id(getB2BUnitId(user));
            failedOrderDTO.setOrder_date(FastDateFormat.getInstance(ORDER_DATE_FORMAT).format(order.getDate()));

            allFailedOrders.add(failedOrderDTO);
        });

        target.setFailed_orders(allFailedOrders);
    }

    /**
     * Retrieves the B2B Unit UID.
     *
     * @param customer the customer
     * @return the B2B Unit UID
     */
    private String getB2BUnitId(UserModel customer) {
        if (customer instanceof B2BCustomerModel) {
            B2BCustomerModel b2bCustomer = (B2BCustomerModel) customer;
            if (b2bCustomer.getDefaultB2BUnit() != null) {
                return b2bCustomer.getDefaultB2BUnit().getUid();
            }
        }
        return null;
    }
}