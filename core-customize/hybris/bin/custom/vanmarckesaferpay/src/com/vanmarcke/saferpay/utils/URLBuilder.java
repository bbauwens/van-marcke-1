package com.vanmarcke.saferpay.utils;

import org.apache.commons.lang3.builder.Builder;

public class URLBuilder implements Builder<String> {

    private StringBuilder url;
    private String baseUrl = "";
    private String token = "";
    private String path = "";

    private URLBuilder() {
    }

    public static URLBuilder aSaferPayUrl() {
        return new URLBuilder();
    }

    private void appendSegment(final String segment) {
        if (url.length() == 0) {
            url.append(segment);
            return;
        }

        if (segment.charAt(0) != '/') {
            url.append("/");
        }

        url.append(segment);
    }

    public URLBuilder withBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public URLBuilder withToken(String token) {
        this.token = token;
        return this;
    }

    public URLBuilder withPath(String path) {
        this.path = path;
        return this;
    }

    @Override
    public String build() {
        url = new StringBuilder();
        appendSegment(baseUrl);
        appendSegment(path);
        appendSegment(token);
        return url.toString();
    }
}