package com.vanmarcke.saferpay.exceptions;

/**
 * Exception used within the context of processing external payment provider communications.
 */
public class PlaceOrderException extends SaferpayException {

    /**
     * Provides an instance of the PlaceOrderException.
     *
     * @param message the message
     */
    public PlaceOrderException(String message) {
        super(message);
    }

    /**
     * Provides an instance of the PlaceOrderException.
     *
     * @param message the message
     * @param cause   the cause
     */
    public PlaceOrderException(String message, Throwable cause) {
        super(message, cause);
    }
}
