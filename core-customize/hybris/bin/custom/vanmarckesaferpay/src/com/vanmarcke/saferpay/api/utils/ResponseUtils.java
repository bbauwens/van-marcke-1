package com.vanmarcke.saferpay.api.utils;

import com.vanmarcke.saferpay.api.data.Response;

import static org.apache.commons.lang.StringUtils.isEmpty;

public final class ResponseUtils {

    private ResponseUtils() {
    }

    /**
     * Indicates whether the response is success or error.
     *
     * @param response the response
     * @param <T>      the response type
     * @return <code>true</code> if success otherwise <code>false</code>
     */
    public static <T extends Response> boolean checkIfSuccess(final T response) {
        return response != null && isEmpty(response.getErrorName());
    }
}