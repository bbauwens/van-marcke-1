package com.vanmarcke.saferpay.exceptions;

/**
 * Exception used within the context of processing external payment provider communications.
 */
public class PaymentValidationException extends SaferpayException {

    /**
     * Provides an instance of the PaymentValidationException.
     *
     * @param message the message
     */
    public PaymentValidationException(String message) {
        super(message);
    }


}
