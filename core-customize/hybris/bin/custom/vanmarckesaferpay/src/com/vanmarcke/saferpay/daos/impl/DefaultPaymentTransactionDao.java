package com.vanmarcke.saferpay.daos.impl;

import com.vanmarcke.saferpay.daos.PaymentTransactionDao;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static java.lang.String.format;
import static java.util.Collections.singletonMap;

public class DefaultPaymentTransactionDao extends DefaultGenericDao<PaymentTransactionModel> implements PaymentTransactionDao {

    /**
     * Creates a new instance of {@link DefaultPaymentTransactionDao}
     */
    public DefaultPaymentTransactionDao() {
        super(PaymentTransactionModel._TYPECODE);
    }

    @Override
    public PaymentTransactionModel findByCode(final String code) {
        checkArgument(code != null, "code cannot be null");

        final List<PaymentTransactionModel> result = find(singletonMap(PaymentTransactionModel.CODE, code));
        validateIfSingleResult(result, format("Payment transaction with code '%s' not found!", code),
                format("Code '%s' is not unique, %d payment transactions found!", code, result.size()));
        return result.get(0);
    }
}