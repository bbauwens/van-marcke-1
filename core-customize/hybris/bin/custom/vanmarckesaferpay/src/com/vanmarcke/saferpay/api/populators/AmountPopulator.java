package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.Amount;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;

import static com.vanmarcke.saferpay.constants.VanmarckesaferpayConstants.BIG_DECIMAL_HUNDRED;
import static java.math.BigDecimal.ZERO;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

public class AmountPopulator<SOURCE extends AbstractOrderModel> implements Populator<SOURCE, Amount> {

    private static final String VALUE_FORMAT = "%.0f";

    @Override
    public void populate(final SOURCE source, final Amount target) {
        target.setCurrencyCode(source.getCurrency().getIsocode());
        target.setValue(String.format(VALUE_FORMAT, calculateTotal(source)));
    }

    protected BigDecimal calculateTotal(final AbstractOrderModel source) {
        if (source.getTotalPrice() == null) {
            return ZERO;
        }

        BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());

        // Add the taxes to the total price if the cart is net; if the total was null taxes should be null as well
        if (isTrue(source.getNet()) && totalPrice.compareTo(ZERO) != 0 && source.getTotalTax() != null) {
            totalPrice = totalPrice.add(BigDecimal.valueOf(source.getTotalTax().doubleValue()));
        }

        return totalPrice.multiply(BIG_DECIMAL_HUNDRED);
    }
}