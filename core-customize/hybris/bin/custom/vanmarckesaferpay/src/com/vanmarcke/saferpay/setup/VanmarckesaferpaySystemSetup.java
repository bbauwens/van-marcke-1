package com.vanmarcke.saferpay.setup;

import com.vanmarcke.saferpay.constants.VanmarckesaferpayConstants;
import de.hybris.platform.core.initialization.SystemSetup;

@SystemSetup(extension = VanmarckesaferpayConstants.EXTENSIONNAME)
public class VanmarckesaferpaySystemSetup {

}