package com.vanmarcke.saferpay.facades.populators;

import com.vanmarcke.saferpay.facades.data.CartTokenData;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.site.BaseSiteService;

/**
 * Converts a cart token to a {@link CartTokenData} instance.
 *
 * @author Tom van den Berg
 * @since 05-02-2021
 */
public class VMKCartTokenDataPopulator implements Populator<String, CartTokenData> {

    private static final String SEPARATOR = "_";

    private final BaseSiteService baseSiteService;

    /**
     * Provides an instance of the VMKCartTokenDataPopulator.
     *
     * @param baseSiteService the base site service
     */
    public VMKCartTokenDataPopulator(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(String source, CartTokenData target) {
        target.setCartUid(getCartGuidFromToken(source));
        target.setBaseSite(getBaseSiteFromToken(source));
    }

    /**
     * {@inheritDoc}
     */
    private BaseSiteModel getBaseSiteFromToken(String paymentToken) {
        BaseSiteModel baseSite = null;
        String siteId = extractSegment(paymentToken, 1);
        if (siteId != null) {
            baseSite = baseSiteService.getBaseSiteForUID(siteId);
        }
        return baseSite;
    }

    /**
     * {@inheritDoc}
     */
    private String getCartGuidFromToken(String paymentToken) {
        return extractSegment(paymentToken, 0);
    }

    /**
     * Extract from the given token the segment on the given position.
     *
     * @param token    the token
     * @param position the position
     * @return the segment
     */
    private String extractSegment(String token, int position) {
        String result = null;

        String[] splittedToken = token.split(SEPARATOR);
        if (splittedToken.length >= position + 1) {
            result = splittedToken[position];
        }
        return result;
    }
}
