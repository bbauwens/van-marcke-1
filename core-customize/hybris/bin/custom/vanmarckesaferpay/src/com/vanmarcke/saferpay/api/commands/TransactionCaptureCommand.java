package com.vanmarcke.saferpay.api.commands;

import com.vanmarcke.saferpay.api.data.TransactionCaptureRequest;
import com.vanmarcke.saferpay.api.data.TransactionCaptureResponse;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command to capture a transaction
 */
@CommandConfig(serviceName = "transactionCapture", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/Payment/v1/Transaction/Capture")
public class TransactionCaptureCommand extends AbstractCommand<TransactionCaptureResponse> {

    /**
     * Creates a new instance of the {@link TransactionCaptureCommand} class.
     *
     * @param baseURL  the base URL
     * @param username the username
     * @param password the password
     * @param request  the request
     */
    public TransactionCaptureCommand(final String baseURL,
                                     final String username,
                                     final String password,
                                     final TransactionCaptureRequest request) {
        super(baseURL, username, password);
        setPayLoad(request);
    }
}