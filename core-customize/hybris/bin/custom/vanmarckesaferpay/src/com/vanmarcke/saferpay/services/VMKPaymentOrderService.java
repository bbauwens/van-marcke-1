package com.vanmarcke.saferpay.services;

import com.vanmarcke.saferpay.exceptions.SaferpayException;
import com.vanmarcke.saferpay.facades.data.CartTokenData;
import de.hybris.platform.commercefacades.order.data.OrderData;

/**
 * Defines methods for handling orders within a payment provider context.
 *
 * @author Tom van den Berg
 * @since 05-02-2021
 */
public interface VMKPaymentOrderService {

    /**
     * Place an order for the given parameters.
     *
     * @param cartToken the cart token
     * @return the order
     */
    OrderData placeOrderForCartToken(CartTokenData cartToken) throws SaferpayException;

    /**
     * Determine whether an order already exists for the given parameters.
     * <p>
     * If no cart is present for the given token, check if an order exists for the same token.
     * If a cart is present for the token, check the status of the cart.
     * If the cart is currently being processed, poll for its completion and retrieve the result.
     *
     * @param cartToken the cart token
     * @return the order
     */
    OrderData checkForExistingOrder(CartTokenData cartToken) throws SaferpayException;
}
