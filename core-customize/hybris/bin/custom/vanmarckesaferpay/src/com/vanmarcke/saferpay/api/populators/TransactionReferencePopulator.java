package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.TransactionReference;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import static com.vanmarcke.core.util.PaymentTransactionUtils.getCurrentPaymentTransactionEntry;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * The {@link TransactionReference} class is used to populate instances of the {@link AbstractOrderModel} class with the
 * information of the instances of the {@link SOURCE} class.
 *
 * @param <SOURCE> the order model
 * @author Taki Korovessis, Tom van den Berg, Christiaan Janssen
 * @since 31-01-2020
 */
public class TransactionReferencePopulator<SOURCE extends AbstractOrderModel> implements Populator<SOURCE, TransactionReference> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, TransactionReference target) {
        PaymentTransactionEntryModel paymentTransactionEntry = getCurrentPaymentTransactionEntry(source.getPaymentTransactions());
        if (paymentTransactionEntry != null && isNotEmpty(paymentTransactionEntry.getRequestId())) {
            target.setTransactionId(paymentTransactionEntry.getRequestId());
        } else {
            target.setOrderId(source.getCode());
        }
    }
}