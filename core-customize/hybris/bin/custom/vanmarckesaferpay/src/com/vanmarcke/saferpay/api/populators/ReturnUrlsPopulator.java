package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.ReturnUrls;
import com.vanmarcke.saferpay.services.VMKPaymentUrlService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;

/**
 * Creates and populates the return URLs for the Saferpay initialization step.
 *
 * @author Taki Korovessis, Tom van den Berg
 * @since 24-1-2020
 */
public class ReturnUrlsPopulator<SOURCE extends AbstractOrderModel> implements Populator<SOURCE, ReturnUrls> {

    private static final String CONFIG_URL_PATH_SUCCESS = "saferpay.paymentpage.url.path.success";
    private static final String CONFIG_URL_PATH_FAIL = "saferpay.paymentpage.url.path.fail";
    private static final String CONFIG_URL_PATH_ABORT = "saferpay.paymentpage.url.path.abort";

    private final VMKPaymentUrlService paymentUrlService;

    /**
     * Provides an instance of the ReturnUrlsPopulator.
     *
     * @param paymentUrlService the payment url service
     */
    public ReturnUrlsPopulator(VMKPaymentUrlService paymentUrlService) {
        this.paymentUrlService = paymentUrlService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, ReturnUrls target) {
        target.setAbort(paymentUrlService.getUrlForCart(source, CONFIG_URL_PATH_ABORT));
        target.setFail(paymentUrlService.getUrlForCart(source, CONFIG_URL_PATH_FAIL));
        target.setSuccess(paymentUrlService.getUrlForCart(source, CONFIG_URL_PATH_SUCCESS));
    }
}
