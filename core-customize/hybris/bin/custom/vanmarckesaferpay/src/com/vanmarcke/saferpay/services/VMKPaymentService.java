package com.vanmarcke.saferpay.services;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;

/**
 * The Payment Page Interface
 */
public interface VMKPaymentService {

    /**
     * Initializes the Payment and generates the RedirectUrl for the Payment Page for the specified cart.
     *
     * @param cart the cart
     * @return the Redirect URL
     */
    String initializePayment(CartModel cart);

    /**
     * Validates the Payment
     *
     * @param abstractOrder the order
     * @return {@code true} if the Payment transaction was successful, otherwise {@code false}
     */
    boolean validatePayment(AbstractOrderModel abstractOrder);
}