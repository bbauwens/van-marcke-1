package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.Amount;
import com.vanmarcke.saferpay.api.data.Payment;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import static java.lang.String.format;

public class PaymentPopulator<SOURCE extends AbstractOrderModel> implements Populator<SOURCE, Payment> {

    private static final String DESCRIPTION_FORMAT = "Payment for order %s";

    private final Converter<SOURCE, Amount> amountConverter;

    public PaymentPopulator(final Converter<SOURCE, Amount> amountConverter) {
        this.amountConverter = amountConverter;
    }

    @Override
    public void populate(final SOURCE source, final Payment target) {
        target.setAmount(amountConverter.convert(source));
        target.setOrderId(source.getCode());
        target.setDescription(format(DESCRIPTION_FORMAT, source.getCode()));
    }
}