package com.vanmarcke.saferpay.facades;

import com.vanmarcke.saferpay.model.SaferpayNotificationModel;

/**
 * Interface for Payment Notifications Facade
 */
public interface PaymentNotificationFacade {

    /**
     * Create a SaferpayNotification for the given order ID
     *
     * @param orderID the given order ID
     * @return the created SaferpayNotificationModel
     */
    SaferpayNotificationModel createNotificationForCartToken(String orderID);
}
