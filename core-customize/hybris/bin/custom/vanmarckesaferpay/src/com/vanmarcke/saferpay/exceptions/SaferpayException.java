package com.vanmarcke.saferpay.exceptions;

/**
 * General exception related to saferpay payments
 */
public class SaferpayException extends Exception {

    /**
     * Provides an instance of the SaferpayException.
     *
     * @param message the message
     */
    public SaferpayException(String message) {
        super(message);
    }

    /**
     * Provides an instance of the SaferpayException.
     *
     * @param message the message
     * @param cause   the cause
     */
    public SaferpayException(String message, Throwable cause) {
        super(message, cause);
    }
}
