package com.vanmarcke.saferpay.api.commands;

import com.vanmarcke.saferpay.api.data.PaymentPageAssertRequest;
import com.vanmarcke.saferpay.api.data.PaymentPageAssertResponse;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command to assert a transaction with the Payment Page.
 */
@CommandConfig(serviceName = "paymentPageAssert", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/Payment/v1/PaymentPage/Assert")
public class PaymentPageAssertCommand extends AbstractCommand<PaymentPageAssertResponse> {

    /**
     * Creates a new instance of the {@link PaymentPageAssertCommand} class.
     *
     * @param baseURL  the base URL
     * @param username the username
     * @param password the password
     * @param request  the request
     */
    public PaymentPageAssertCommand(final String baseURL,
                                    final String username,
                                    final String password,
                                    final PaymentPageAssertRequest request) {
        super(baseURL, username, password);
        setPayLoad(request);
    }

}