package com.vanmarcke.saferpay.job;

import com.vanmarcke.saferpay.daos.PaymentNotificationDao;
import com.vanmarcke.saferpay.exceptions.PaymentValidationException;
import com.vanmarcke.saferpay.exceptions.SaferpayException;
import com.vanmarcke.saferpay.facades.VMKPaymentFacade;
import com.vanmarcke.saferpay.model.SaferpayNotificationAsyncCallbackCronJobModel;
import com.vanmarcke.saferpay.model.SaferpayNotificationModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.RandomStringUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SaferpayNotificationAsyncCallbackJobTest {

    private static final String TOKEN = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private PaymentNotificationDao paymentNotificationDao;

    @Mock
    private VMKPaymentFacade paymentFacade;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private SaferpayNotificationAsyncCallbackJob saferpayNotificationAsyncCallbackJob;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        saferpayNotificationAsyncCallbackJob.setModelService(modelService);
    }

    @Test
    public void testPerform() throws SaferpayException {
        SaferpayNotificationAsyncCallbackCronJobModel cronJob = mock(SaferpayNotificationAsyncCallbackCronJobModel.class);
        SaferpayNotificationModel notificationOld = mock(SaferpayNotificationModel.class);
        SaferpayNotificationModel notificationNew = mock(SaferpayNotificationModel.class);

        when(notificationOld.getCreationtime()).thenReturn(DateTime.now().minusMinutes(30).toDate());
        when(notificationNew.getCreationtime()).thenReturn(DateTime.now().minusMinutes(10).toDate());

        when(paymentNotificationDao.findAllPaymentNotifications()).thenReturn(Arrays.asList(notificationNew, notificationOld));

        when(notificationOld.getCartToken()).thenReturn(TOKEN);

        PerformResult result = saferpayNotificationAsyncCallbackJob.perform(cronJob);

        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

        verify(modelService).remove(notificationOld);
        verify(modelService, never()).remove(notificationNew);

        verify(notificationOld).getCartToken();
        verify(notificationNew, never()).getCartToken();

        verify(paymentNotificationDao).findAllPaymentNotifications();
        verify(paymentFacade).processPaymentConfirmation(TOKEN);
    }

    @Test
    public void testPerform_exception() throws SaferpayException {
        SaferpayNotificationAsyncCallbackCronJobModel cronJob = mock(SaferpayNotificationAsyncCallbackCronJobModel.class);
        SaferpayNotificationModel notification = mock(SaferpayNotificationModel.class);
        when(notification.getCreationtime()).thenReturn(DateTime.now().minusMinutes(30).toDate());

        when(paymentNotificationDao.findAllPaymentNotifications()).thenReturn(Collections.singletonList(notification));

        when(notification.getCartToken()).thenReturn(TOKEN);
        when(paymentFacade.processPaymentConfirmation(TOKEN)).thenThrow(new PaymentValidationException(""));

        PerformResult result = saferpayNotificationAsyncCallbackJob.perform(cronJob);

        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

        verify(modelService, never()).remove(notification);
        verify(paymentNotificationDao).findAllPaymentNotifications();
        verify(paymentFacade).processPaymentConfirmation(TOKEN);
    }
}