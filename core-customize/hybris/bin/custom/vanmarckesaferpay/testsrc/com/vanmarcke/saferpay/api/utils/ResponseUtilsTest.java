package com.vanmarcke.saferpay.api.utils;

import com.vanmarcke.saferpay.api.data.Response;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ResponseUtilsTest {

    @Test
    public void testCheckIfSuccess_success() {
        Response response = mock(Response.class);

        boolean result = ResponseUtils.checkIfSuccess(response);

        assertThat(result).isTrue();
    }

    @Test
    public void testCheckIfSuccess_error() {
        Response response = mock(Response.class);

        when(response.getErrorName()).thenReturn("ERROR-CODE");

        boolean result = ResponseUtils.checkIfSuccess(response);

        assertThat(result).isFalse();
    }

}