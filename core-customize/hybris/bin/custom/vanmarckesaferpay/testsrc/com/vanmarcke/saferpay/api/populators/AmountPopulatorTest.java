package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.Amount;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AmountPopulatorTest {

    @InjectMocks
    private AmountPopulator amountPopulator;

    @Test
    public void testPopulate_withNet() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        CurrencyModel currencyModel = mock(CurrencyModel.class);

        when(currencyModel.getIsocode()).thenReturn("EUR");

        when(abstractOrderModel.getCurrency()).thenReturn(currencyModel);
        when(abstractOrderModel.getNet()).thenReturn(true);
        when(abstractOrderModel.getTotalPrice()).thenReturn(123.46);
        when(abstractOrderModel.getTotalTax()).thenReturn(25.93);

        Amount result = new Amount();
        amountPopulator.populate(abstractOrderModel, result);

        assertThat(result.getCurrencyCode()).isEqualTo("EUR");
        assertThat(result.getValue()).isEqualTo("14939");
    }

    @Test
    public void testPopulate_withGross() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        CurrencyModel currencyModel = mock(CurrencyModel.class);

        when(currencyModel.getIsocode()).thenReturn("EUR");

        when(abstractOrderModel.getCurrency()).thenReturn(currencyModel);
        when(abstractOrderModel.getNet()).thenReturn(false);
        when(abstractOrderModel.getTotalPrice()).thenReturn(123.46);
        when(abstractOrderModel.getTotalTax()).thenReturn(25.93);

        Amount result = new Amount();
        amountPopulator.populate(abstractOrderModel, result);

        assertThat(result.getCurrencyCode()).isEqualTo("EUR");
        assertThat(result.getValue()).isEqualTo("12346");
    }

    @Test
    public void testPopulate_withoutTotalPrice() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        CurrencyModel currencyModel = mock(CurrencyModel.class);

        when(currencyModel.getIsocode()).thenReturn("EUR");

        when(abstractOrderModel.getCurrency()).thenReturn(currencyModel);
        when(abstractOrderModel.getTotalPrice()).thenReturn(null);

        Amount result = new Amount();
        amountPopulator.populate(abstractOrderModel, result);

        assertThat(result.getCurrencyCode()).isEqualTo("EUR");
        assertThat(result.getValue()).isEqualTo("0");

        verify(abstractOrderModel, never()).getNet();
        verify(abstractOrderModel, never()).getTotalTax();
    }

}