package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.Amount;
import com.vanmarcke.saferpay.api.data.Payment;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentPopulatorTest {

    @Mock
    private Converter<AbstractOrderModel, Amount> amountConverter;
    @InjectMocks
    private PaymentPopulator paymentPopulator;

    @Test
    public void testPopulate() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        Amount amountData = mock(Amount.class);

        when(abstractOrderModel.getCode()).thenReturn("order-code");

        when(amountConverter.convert(abstractOrderModel)).thenReturn(amountData);

        Payment result = new Payment();
        paymentPopulator.populate(abstractOrderModel, result);

        assertThat(result.getAmount()).isEqualTo(amountData);
        assertThat(result.getOrderId()).isEqualTo("order-code");
        assertThat(result.getDescription()).isEqualTo("Payment for order order-code");
    }

}