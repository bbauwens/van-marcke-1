package com.vanmarcke.saferpay.api.commands;

import com.vanmarcke.saferpay.api.data.PaymentPageInitializeRequest;
import com.vanmarcke.saferpay.api.data.PaymentPageInitializeResponse;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.text.MessageFormat;
import java.util.HashMap;

import static org.fest.assertions.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * The {@link PaymentPageInitializeCommandTest} class contains the unit tests for the
 * {@link PaymentPageInitializeCommand} class.
 *
 * @author Taki Korovessis
 * @since 24-01-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentPageInitializeCommandTest {

    private static final String BASE_URL = RandomStringUtils.random(10);
    private static final String USERNAME = RandomStringUtils.random(10);
    private static final String PASSWORD = RandomStringUtils.random(10);

    @Mock
    private PaymentPageInitializeRequest request;

    private PaymentPageInitializeCommand command;

    @Before
    public void setUp() {
        command = new PaymentPageInitializeCommand(BASE_URL, USERNAME, PASSWORD, request);
    }

    @Test
    public void testConstructor() {
        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(command.getServiceName()).isEqualTo("paymentPageInitialize");
        assertThat(command.getUrl()).isEqualTo("/Payment/v1/PaymentPage/Initialize");
        assertThat(command.getResponseType()).isEqualTo(PaymentPageInitializeResponse.class);
        assertThat(command.getPayLoad()).isEqualTo(request);
    }

    @Test
    public void testBuildHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();

        command.buildHttpHeaders(httpHeaders);

        assertThat(httpHeaders).hasSize(2);
        assertThat(httpHeaders.getAccept()).containsExactly(APPLICATION_JSON, APPLICATION_JSON);
        assertThat(httpHeaders.getContentType()).isEqualTo(APPLICATION_JSON);
    }

    @Test
    public void testBuildUri() {
        String actualURL = command.buildUri(new HashMap<>());

        assertThat(actualURL).isEqualTo(MessageFormat.format("{0}/Payment/v1/PaymentPage/Initialize", BASE_URL));
    }
}