package com.vanmarcke.saferpay.services.impl;

import com.vanmarcke.saferpay.utils.URLBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentUrlServiceImplTest {

    private static final String BASE_URL = RandomStringUtils.randomAlphabetic(10);
    private static final String URL_PATH_KEY = RandomStringUtils.randomAlphabetic(10);
    private static final String PATH = RandomStringUtils.randomAlphabetic(10);
    private static final String CART_GUID = RandomStringUtils.randomAlphabetic(10);
    private static final String SITE_GUID = RandomStringUtils.randomAlphabetic(10);
    private static final String TOKEN = CART_GUID + "_" + SITE_GUID;

    private static final String SAFERPAY_URL = URLBuilder.aSaferPayUrl()
            .withBaseUrl(BASE_URL)
            .withPath(PATH)
            .withToken(TOKEN).build();

    private AbstractOrderModel cart;
    private BaseSiteModel baseSiteModel;
    private Configuration configuration;

    @Mock
    private ConfigurationService configurationService;
    @Mock
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
    @InjectMocks
    private VMKPaymentUrlServiceImpl paymentUrlService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Before
    public void setUp() throws Exception {
        cart = mock(AbstractOrderModel.class);
        baseSiteModel = mock(BaseSiteModel.class);
        configuration = mock(Configuration.class);

        when(baseSiteModel.getUid()).thenReturn(SITE_GUID);
        when(cart.getSite()).thenReturn(baseSiteModel);
        when(cart.getGuid()).thenReturn(CART_GUID);
    }

    @Test
    public void getGetUrlForCart() {
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, true, null))
                .thenReturn(BASE_URL);
        when(configurationService.getConfiguration()).thenReturn(configuration);
        when(configuration.getString(URL_PATH_KEY)).thenReturn(PATH);

        String result = paymentUrlService.getUrlForCart(cart, URL_PATH_KEY);

        assertThat(result).isEqualTo(SAFERPAY_URL);

        verify(siteBaseUrlResolutionService).getWebsiteUrlForSite(baseSiteModel, Boolean.TRUE, null);
        verify(configurationService).getConfiguration();
        verify(configuration).getString(URL_PATH_KEY);
    }

    @Test
    public void getGetUrlForCart_baseUrlEmpty() {
        when(cart.getSite()).thenReturn(baseSiteModel);
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, true, null))
                .thenReturn("");
        when(configurationService.getConfiguration()).thenReturn(configuration);
        when(configuration.getString(URL_PATH_KEY)).thenReturn(PATH);

        expectedException.expectMessage("The base url cannot be empty");
        expectedException.expect(IllegalArgumentException.class);

        paymentUrlService.getUrlForCart(cart, URL_PATH_KEY);


        verify(siteBaseUrlResolutionService).getWebsiteUrlForSite(baseSiteModel, Boolean.TRUE, null);
        verify(configurationService).getConfiguration();
        verify(configuration).getString(URL_PATH_KEY);
    }

    @Test
    public void getGetUrlForCart_pathEmpty() {
        when(cart.getSite()).thenReturn(baseSiteModel);
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, true, null))
                .thenReturn(BASE_URL);
        when(configurationService.getConfiguration()).thenReturn(configuration);
        when(configuration.getString(URL_PATH_KEY)).thenReturn("");

        expectedException.expectMessage("The path cannot be empty");
        expectedException.expect(IllegalArgumentException.class);

        paymentUrlService.getUrlForCart(cart, URL_PATH_KEY);

        verify(siteBaseUrlResolutionService).getWebsiteUrlForSite(baseSiteModel, Boolean.TRUE, null);
        verify(configurationService).getConfiguration();
        verify(configuration).getString(URL_PATH_KEY);
    }

    @Test
    public void getGetUrlForCart_tokenHasNoGuid() {
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, true, null))
                .thenReturn(BASE_URL);
        when(configurationService.getConfiguration()).thenReturn(configuration);
        when(configuration.getString(URL_PATH_KEY)).thenReturn(PATH);

        when(cart.getGuid()).thenReturn(null);
        expectedException.expectMessage("Cart ID cannot be null");
        expectedException.expect(IllegalStateException.class);

        paymentUrlService.getUrlForCart(cart, URL_PATH_KEY);

        verify(siteBaseUrlResolutionService).getWebsiteUrlForSite(baseSiteModel, Boolean.TRUE, null);
        verify(configurationService).getConfiguration();
        verify(configuration).getString(URL_PATH_KEY);
    }

    @Test
    public void getGetUrlForCart_tokenHasSite() {
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, true, null))
                .thenReturn(BASE_URL);
        when(configurationService.getConfiguration()).thenReturn(configuration);
        when(configuration.getString(URL_PATH_KEY)).thenReturn(PATH);

        when(cart.getSite()).thenReturn(null);
        expectedException.expectMessage("The site cannot be null");
        expectedException.expect(IllegalStateException.class);

        paymentUrlService.getUrlForCart(cart, URL_PATH_KEY);

        verify(siteBaseUrlResolutionService).getWebsiteUrlForSite(baseSiteModel, Boolean.TRUE, null);
        verify(configurationService).getConfiguration();
        verify(configuration).getString(URL_PATH_KEY);
    }
}