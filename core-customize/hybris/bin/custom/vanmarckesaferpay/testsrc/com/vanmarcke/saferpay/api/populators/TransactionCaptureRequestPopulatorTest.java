package com.vanmarcke.saferpay.api.populators;

import com.vanmarcke.saferpay.api.data.RequestHeader;
import com.vanmarcke.saferpay.api.data.TransactionCaptureRequest;
import com.vanmarcke.saferpay.api.data.TransactionReference;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TransactionCaptureRequestPopulatorTest {

    @Mock
    private Converter<AbstractOrderModel, RequestHeader> requestHeaderConverter;
    @Mock
    private Converter<AbstractOrderModel, TransactionReference> transactionReferenceConverter;

    private TransactionCaptureRequestPopulator transactionCaptureRequestPopulator;

    @Before
    public void setup() {
        transactionCaptureRequestPopulator = new TransactionCaptureRequestPopulator(requestHeaderConverter, transactionReferenceConverter);
    }

    @Test
    public void testPopulate() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        RequestHeader requestHeader = mock(RequestHeader.class);
        TransactionReference transactionReference = mock(TransactionReference.class);

        when(requestHeaderConverter.convert(abstractOrderModel)).thenReturn(requestHeader);

        when(transactionReferenceConverter.convert(abstractOrderModel)).thenReturn(transactionReference);

        TransactionCaptureRequest result = new TransactionCaptureRequest();
        transactionCaptureRequestPopulator.populate(abstractOrderModel, result);

        assertThat(result.getRequestHeader()).isEqualTo(requestHeader);
        assertThat(result.getTransactionReference()).isEqualTo(transactionReference);
    }

}