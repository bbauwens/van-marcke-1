package com.vanmarcke.saferpay.services.impl;

import com.vanmarcke.core.order.VMKOrderDao;
import com.vanmarcke.facades.order.VMKCheckoutFacade;
import com.vanmarcke.saferpay.exceptions.PaymentInvalidException;
import com.vanmarcke.saferpay.exceptions.PlaceOrderException;
import com.vanmarcke.saferpay.exceptions.SaferpayException;
import com.vanmarcke.saferpay.facades.data.CartTokenData;
import com.vanmarcke.saferpay.services.VMKPaymentService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentOrderServiceImplTest {

    private static final String CART_UID = RandomStringUtils.randomAlphabetic(10);
    private static final String PAYMENT_PROCESS_SEVERE_ERROR = "checkout.severe.error.process.payment";

    private OrderData orderData;
    private CartModel cart;
    private OrderModel orderModel;
    private CartTokenData cartToken;
    private BaseSiteModel baseSite;

    @Mock
    private SessionService sessionService;
    @Mock
    private CartService cartService;
    @Mock
    private CommerceCartService commerceCartService;
    @Mock
    private VMKPaymentService paymentService;
    @Mock
    private VMKCheckoutFacade checkoutFacade;
    @Mock
    private Converter<OrderModel, OrderData> orderConverter;
    @Mock
    private VMKOrderDao orderDao;
    @Mock
    private VMKPaymentOrderPollingServiceImpl pollingService;
    @InjectMocks
    private VMKPaymentOrderServiceImpl paymentOrderService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        orderData = new OrderData();
        cart = mock(CartModel.class);
        orderModel = mock(OrderModel.class);
        cartToken = mock(CartTokenData.class);
        baseSite = mock(BaseSiteModel.class);
        when(cartToken.getCartUid()).thenReturn(CART_UID);
        when(cartToken.getBaseSite()).thenReturn(baseSite);

        when(commerceCartService.getCartForGuidAndSite(CART_UID, baseSite))
                .thenReturn(cart);

        when(paymentService.validatePayment(cart)).thenReturn(true);
        when(orderDao.findOrderByGuidAndSite(CART_UID, baseSite)).thenReturn(orderModel);
        when(orderConverter.convert(orderModel)).thenReturn(orderData);

    }

    @Test
    public void testIsInvalid_invalid() {
        when(paymentService.validatePayment(cart)).thenReturn(false);

        assertThat(paymentOrderService.isInvalid(cart)).isTrue();
        verify(paymentService).validatePayment(cart);
    }

    @Test
    public void testIsInvalid_valid() {
        assertThat(paymentOrderService.isInvalid(cart)).isFalse();
        verify(paymentService).validatePayment(cart);
    }

    @Test
    public void testPlaceOrderForCartToken() throws InvalidCartException, SaferpayException {
        when(sessionService.executeInLocalViewWithParams(anyMap(), any(SessionExecutionBody.class))).thenReturn(orderData);

        assertThat(paymentOrderService.placeOrderForCartToken(cartToken))
                .isEqualTo(orderData);

//        verify(checkoutFacade).placeOrder();
        verify(commerceCartService).getCartForGuidAndSite(CART_UID, baseSite);
        verify(paymentService).validatePayment(cart);
    }

    @Test
    public void testPlaceOrderForCartToken_cartNull() throws SaferpayException {
        when(commerceCartService.getCartForGuidAndSite(CART_UID, baseSite))
                .thenReturn(null);

        expectedException.expectMessage(PAYMENT_PROCESS_SEVERE_ERROR);
        expectedException.expect(PlaceOrderException.class);

        paymentOrderService.placeOrderForCartToken(cartToken);

        verify(commerceCartService).getCartForGuidAndSite(CART_UID, baseSite);
        verifyZeroInteractions(checkoutFacade, paymentService);
    }

    @Test
    public void testPlaceOrderForCartToken_invalid() throws InvalidCartException, SaferpayException {
        when(paymentService.validatePayment(cart)).thenReturn(false);

        expectedException.expect(PaymentInvalidException.class);
        expectedException.expectMessage(PAYMENT_PROCESS_SEVERE_ERROR);

        paymentOrderService.placeOrderForCartToken(cartToken);

        verify(paymentService).validatePayment(cart);
        verify(commerceCartService).getCartForGuidAndSite(CART_UID, baseSite);
        verifyZeroInteractions(checkoutFacade);
    }

    @Test
    public void testPlaceOrderForCartToken_placeOrderException() throws InvalidCartException, SaferpayException {
        RuntimeException exception = mock(RuntimeException.class);

        when(checkoutFacade.placeOrder()).thenThrow(new RuntimeException());
        when(sessionService.executeInLocalViewWithParams(anyMap(), any(SessionExecutionBody.class))).thenThrow(exception);

        expectedException.expect(PlaceOrderException.class);
        expectedException.expectMessage(PAYMENT_PROCESS_SEVERE_ERROR);

        paymentOrderService.placeOrderForCartToken(cartToken);

        verify(paymentService).validatePayment(cart);
        verify(commerceCartService).getCartForGuidAndSite(CART_UID, baseSite);
    }

    @Test
    public void testCheckForExistingOrder_orderAlreadyPlaced() throws SaferpayException {
        when(commerceCartService.getCartForGuidAndSite(CART_UID, baseSite))
                .thenReturn(null);
        assertThat(paymentOrderService.checkForExistingOrder(cartToken)).isEqualTo(orderData);

        verify(commerceCartService).getCartForGuidAndSite(CART_UID, baseSite);
        verify(orderDao).findOrderByGuidAndSite(CART_UID, baseSite);
    }

    @Test
    public void testCheckForExistingOrder_orderBeingProcessed() throws SaferpayException {
        when(commerceCartService.getCartForGuidAndSite(CART_UID, baseSite)).thenReturn(cart);
        when(cart.getStatus()).thenReturn(OrderStatus.ORDER_CREATION_IN_PROGRESS);
        when(pollingService.pollOrderCreationProcess(cartToken)).thenReturn(orderModel);

        assertThat(paymentOrderService.checkForExistingOrder(cartToken)).isEqualTo(orderData);

        verify(commerceCartService).getCartForGuidAndSite(CART_UID, baseSite);
        verify(orderDao, never()).findOrderByGuidAndSite(CART_UID, baseSite);
        verify(pollingService).pollOrderCreationProcess(cartToken);
        verify(orderConverter).convert(orderModel);
    }

    @Test
    public void testCheckForExistingOrder_pollOrderNull() throws SaferpayException {
        when(commerceCartService.getCartForGuidAndSite(CART_UID, baseSite)).thenReturn(cart);
        when(cart.getStatus()).thenReturn(OrderStatus.ORDER_CREATION_IN_PROGRESS);
        when(pollingService.pollOrderCreationProcess(cartToken)).thenReturn(null);

        assertThat(paymentOrderService.checkForExistingOrder(cartToken)).isNull();

        verify(commerceCartService).getCartForGuidAndSite(CART_UID, baseSite);
        verify(orderDao, never()).findOrderByGuidAndSite(CART_UID, baseSite);
        verify(pollingService).pollOrderCreationProcess(cartToken);
        verify(orderConverter, never()).convert(orderModel);
    }
}