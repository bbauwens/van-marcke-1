package com.vanmarcke.saferpay.facades.impl;

import com.vanmarcke.saferpay.model.SaferpayNotificationModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPaymentNotificationFacadeTest {

    private static final String TOKEN = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private ModelService modelService;
    @InjectMocks
    private VMKPaymentNotificationFacade defaultPaymentNotificationFacade;

    @Test
    public void testCreateNotificationForOrderID() {
        SaferpayNotificationModel saferpayNotification = mock(SaferpayNotificationModel.class);
        OrderModel order = mock(OrderModel.class);

        when(modelService.create(SaferpayNotificationModel.class)).thenReturn(saferpayNotification);

        SaferpayNotificationModel result = defaultPaymentNotificationFacade.createNotificationForCartToken(TOKEN);

        assertThat(result).isEqualTo(saferpayNotification);

        verify(modelService).create(SaferpayNotificationModel.class);
        verify(modelService).save(result);
        verify(saferpayNotification).setCartToken(TOKEN);
    }
}