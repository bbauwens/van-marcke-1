package com.vanmarcke.saferpay.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPaymentTransactionStrategyTest {

    @Mock
    private ModelService modelService;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private PaymentService paymentService;
    @InjectMocks
    private DefaultPaymentTransactionStrategy defaultPaymentTransactionStrategy;

    private Map<String, PaymentTransactionType> transactionStatusMap;

    @Before
    public void setup() {
        transactionStatusMap = new HashMap<>();
        transactionStatusMap.put("AUTHORIZED", PaymentTransactionType.AUTHORIZATION);
        transactionStatusMap.put("PENDING", PaymentTransactionType.CAPTURE);

        defaultPaymentTransactionStrategy.setTransactionStatusMap(transactionStatusMap);
    }

    @Test
    public void testSavePaymentTransaction() {
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);

        when(modelService.create(PaymentTransactionModel.class)).thenReturn(paymentTransactionModel);

        PaymentTransactionModel result = defaultPaymentTransactionStrategy.savePaymentTransaction("merchant-code", "token");

        assertThat(result).isEqualTo(paymentTransactionModel);

        verify(paymentTransactionModel).setCode("merchant-code");
        verify(paymentTransactionModel).setRequestToken("token");
        verify(paymentTransactionModel).setPaymentProvider("Saferpay");

        verify(modelService).save(paymentTransactionModel);
    }

    @Test
    public void testSavePaymentTransactionEntry() {
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        CurrencyModel currencyModel = mock(CurrencyModel.class);
        Date date = mock(Date.class);

        when(modelService.create(PaymentTransactionEntryModel.class)).thenReturn(paymentTransactionEntryModel);

        when(paymentService.getNewPaymentTransactionEntryCode(paymentTransactionModel, PaymentTransactionType.AUTHORIZATION)).thenReturn("transaction-entry-code");

        when(commonI18NService.getCurrency("EUR")).thenReturn(currencyModel);

        defaultPaymentTransactionStrategy.savePaymentTransactionEntry(paymentTransactionModel, "transaction-id", "AUTHORIZED", date, BigDecimal.TEN, "EUR");

        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.AUTHORIZATION);
        verify(paymentTransactionEntryModel).setTime(date);
        verify(paymentTransactionEntryModel).setPaymentTransaction(paymentTransactionModel);
        verify(paymentTransactionEntryModel).setRequestId("transaction-id");
        verify(paymentTransactionEntryModel).setTransactionStatus("ACCEPTED");
        verify(paymentTransactionEntryModel).setTransactionStatusDetails("SUCCESFULL");
        verify(paymentTransactionEntryModel).setCode("transaction-entry-code");
        verify(paymentTransactionEntryModel).setAmount(BigDecimal.TEN);
        verify(paymentTransactionEntryModel).setCurrency(currencyModel);

        verify(paymentTransactionModel).setRequestId("transaction-id");

        verify(modelService).save(paymentTransactionEntryModel);
        verify(modelService).save(paymentTransactionModel);
        verify(modelService).refresh(paymentTransactionModel);
    }

    @Test
    public void testSavePaymentTransactionEntry_withTransactionStatusPending() {
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        CurrencyModel currencyModel = mock(CurrencyModel.class);
        Date date = mock(Date.class);

        when(modelService.create(PaymentTransactionEntryModel.class)).thenReturn(paymentTransactionEntryModel);

        when(paymentService.getNewPaymentTransactionEntryCode(paymentTransactionModel, PaymentTransactionType.CAPTURE)).thenReturn("transaction-entry-code");

        when(commonI18NService.getCurrency("EUR")).thenReturn(currencyModel);

        defaultPaymentTransactionStrategy.savePaymentTransactionEntry(paymentTransactionModel, "transaction-id", "PENDING", date, BigDecimal.TEN, "EUR");

        verify(paymentTransactionEntryModel).setType(PaymentTransactionType.CAPTURE);
        verify(paymentTransactionEntryModel).setTime(date);
        verify(paymentTransactionEntryModel).setPaymentTransaction(paymentTransactionModel);
        verify(paymentTransactionEntryModel).setRequestId("transaction-id");
        verify(paymentTransactionEntryModel).setTransactionStatus("REVIEW");
        verify(paymentTransactionEntryModel).setTransactionStatusDetails("REVIEW_NEEDED");
        verify(paymentTransactionEntryModel).setCode("transaction-entry-code");
        verify(paymentTransactionEntryModel).setAmount(BigDecimal.TEN);
        verify(paymentTransactionEntryModel).setCurrency(currencyModel);

        verify(paymentTransactionModel).setRequestId("transaction-id");

        verify(modelService).save(paymentTransactionEntryModel);
        verify(modelService).save(paymentTransactionModel);
        verify(modelService).refresh(paymentTransactionModel);
    }

}