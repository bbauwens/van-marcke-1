package com.vanmarcke.hotfolder.dataimport.batch.tax.populator;

import com.vanmarcke.hotfolder.data.TaxDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.price.TaxModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;

/**
 * The {@link VMKTaxReversePopulatorTest} class contains the unit tests for the {@link VMKTaxReversePopulator} class.
 *
 * @author Christiaan Janssen
 * @since 02-04-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKTaxReversePopulatorTest {

    private static final String TYPE = RandomStringUtils.random(10);

    @InjectMocks
    private VMKTaxReversePopulator taxReversePopulator;

    @Test
    public void testPopulate_withoutValues() {
        // given
        TaxDTO taxDTO = new TaxDTO();

        TaxModel taxModel = new TaxModel();

        // when
        taxReversePopulator.populate(taxDTO, taxModel);

        // then
        assertThat(taxModel.getCode()).isNull();
        assertThat(taxModel.getType()).isNull();
    }

    @Test
    public void testPopulate_withValues() {
        // given
        TaxDTO taxDTO = new TaxDTO();
        taxDTO.setType(TYPE);

        TaxModel taxModel = new TaxModel();

        // when
        taxReversePopulator.populate(taxDTO, taxModel);

        // then
        assertThat(taxModel.getType()).isEqualTo(TYPE);
    }
}