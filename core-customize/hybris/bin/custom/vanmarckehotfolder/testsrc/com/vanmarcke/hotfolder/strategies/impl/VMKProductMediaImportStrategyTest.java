package com.vanmarcke.hotfolder.strategies.impl;

import com.google.common.collect.Sets;
import com.vanmarcke.core.enums.VMBranchType;
import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.hotfolder.data.MediaAttributeType;
import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.providers.VMKItemProvider;
import com.vanmarcke.hotfolder.resourcespace.enums.MetaDataField;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;
import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.util.Lists;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductMediaImportStrategyTest {

    private static final String MEDIA_TYPE = RandomStringUtils.randomAlphabetic(10);
    private static final String EXTERNAL_ID = RandomStringUtils.randomAlphabetic(10);

    private static final String PRODUCT_A = RandomStringUtils.randomAlphabetic(10);
    private static final String BRAND_A = RandomStringUtils.randomAlphabetic(10);
    private static final String BRAND_SERIES_A = RandomStringUtils.randomAlphabetic(10);
    private static final String SUPPLIER_ITEM_REF_A = RandomStringUtils.randomAlphabetic(10);
    private static final String MANUFACTURER_A = RandomStringUtils.randomAlphabetic(10);
    private static final String BOOKKEEPING_SUPPLIER_A = RandomStringUtils.randomAlphabetic(10);

    private static final String PRODUCT_B = RandomStringUtils.randomAlphabetic(10);
    private static final String BRAND_B = RandomStringUtils.randomAlphabetic(10);
    private static final String BRAND_SERIES_B = RandomStringUtils.randomAlphabetic(10);
    private static final String SUPPLIER_ITEM_REF_B = RandomStringUtils.randomAlphabetic(10);
    private static final String MANUFACTURER_B = RandomStringUtils.randomAlphabetic(10);
    private static final String BOOKKEEPING_SUPPLIER_B = RandomStringUtils.randomAlphabetic(10);

    private static final String QUALIFIER = RandomStringUtils.randomAlphabetic(10);
    private static final String CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String ATTRIBUTE = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private VMKItemProvider<ProductModel> itemProvider;

    @Mock
    private VMKResourceSpaceService integrationService;

    @Mock
    private ModelService modelService;

    @Spy
    @InjectMocks
    private VMKProductMediaImportStrategy productMediaImportStrategy;

    @Before
    public void setUp() {
        productMediaImportStrategy.setMediaType(MEDIA_TYPE);
    }

    @Test
    public void testGetItems() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        MediaData media = new MediaData();
        media.setProducts(Lists.newArrayList(PRODUCT_A, PRODUCT_B));

        ProductModel productA = new ProductModel();
        ProductModel productB = new ProductModel();

        when(itemProvider.get(catalogVersion, Lists.newArrayList(PRODUCT_A, PRODUCT_B))).thenReturn(Sets.newLinkedHashSet(Lists.newArrayList(productA, productB)));

        Set<ProductModel> actualItems = productMediaImportStrategy.getItems(catalogVersion, media);

        Assertions
                .assertThat(actualItems)
                .containsOnly(productA, productB);

        verify(itemProvider).get(catalogVersion, Lists.newArrayList(PRODUCT_A, PRODUCT_B));
    }

    @Test
    public void testUpdateMetadataWithPhoto() {
        productMediaImportStrategy.setResourceType(ResourceType.PHOTO);

        BrandCategoryModel brandA = new BrandCategoryModel();
        brandA.setCode(BRAND_A);

        VariantProductModel productA = new VariantProductModel();
        productA.setCode(PRODUCT_A);
        productA.setSupercategories(Collections.singletonList(brandA));
        productA.setBrandSeries(BRAND_SERIES_A);
        productA.setVendorItemNumber_current(SUPPLIER_ITEM_REF_A);
        productA.setSupplier_Logistic(MANUFACTURER_A);
        productA.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_A);
        productA.setVmBranch(VMBranchType.SERVICES);

        BrandCategoryModel brandB = new BrandCategoryModel();
        brandB.setCode(BRAND_B);

        VariantProductModel productB = new VariantProductModel();
        productB.setCode(PRODUCT_B);
        productB.setSupercategories(Collections.singletonList(brandB));
        productB.setBrandSeries(BRAND_SERIES_B);
        productB.setVendorItemNumber_current(SUPPLIER_ITEM_REF_B);
        productB.setSupplier_Logistic(MANUFACTURER_B);
        productB.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_B);
        productB.setVmBranch(VMBranchType.HVAC);

        productMediaImportStrategy.updateMetaData(EXTERNAL_ID, Sets.newHashSet(Lists.newArrayList(Locale.US, Locale.UK)), Sets.newLinkedHashSet(Lists.newArrayList(productA, productB)));

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.PHOTO_TYPE, MEDIA_TYPE); //todo
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SKU, PRODUCT_A + "," + PRODUCT_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDNAME, BRAND_A + "," + BRAND_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDSERIE, BRAND_SERIES_A + "," + BRAND_SERIES_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SUPPLIER_ITEM_REF, SUPPLIER_ITEM_REF_A + "," + SUPPLIER_ITEM_REF_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.MANUFACTURER, MANUFACTURER_A + "," + MANUFACTURER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BOOKKEEPING_SUPPLIER, BOOKKEEPING_SUPPLIER_A + "," + BOOKKEEPING_SUPPLIER_B);
        verifyNoMoreInteractions(integrationService);
    }

    @Test
    public void testUpdateMetadataWithDocument() {
        productMediaImportStrategy.setResourceType(ResourceType.DOCUMENT);

        BrandCategoryModel brandA = new BrandCategoryModel();
        brandA.setCode(BRAND_A);

        VariantProductModel productA = new VariantProductModel();
        productA.setCode(PRODUCT_A);
        productA.setSupercategories(Collections.singletonList(brandA));
        productA.setBrandSeries(BRAND_SERIES_A);
        productA.setVendorItemNumber_current(SUPPLIER_ITEM_REF_A);
        productA.setSupplier_Logistic(MANUFACTURER_A);
        productA.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_A);
        productA.setVmBranch(VMBranchType.SERVICES);

        BrandCategoryModel brandB = new BrandCategoryModel();
        brandB.setCode(BRAND_B);

        VariantProductModel productB = new VariantProductModel();
        productB.setCode(PRODUCT_B);
        productB.setSupercategories(Collections.singletonList(brandB));
        productB.setBrandSeries(BRAND_SERIES_B);
        productB.setVendorItemNumber_current(SUPPLIER_ITEM_REF_B);
        productB.setSupplier_Logistic(MANUFACTURER_B);
        productB.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_B);
        productB.setVmBranch(VMBranchType.HVAC);

        productMediaImportStrategy.updateMetaData(EXTERNAL_ID, Sets.newHashSet(Lists.newArrayList(Locale.US, Locale.UK)), Sets.newLinkedHashSet(Lists.newArrayList(productA, productB)));

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SKU, PRODUCT_A + "," + PRODUCT_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDNAME, BRAND_A + "," + BRAND_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDSERIE, BRAND_SERIES_A + "," + BRAND_SERIES_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SUPPLIER_ITEM_REF, SUPPLIER_ITEM_REF_A + "," + SUPPLIER_ITEM_REF_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.MANUFACTURER, MANUFACTURER_A + "," + MANUFACTURER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BOOKKEEPING_SUPPLIER, BOOKKEEPING_SUPPLIER_A + "," + BOOKKEEPING_SUPPLIER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_LANGUAGE, Locale.US.toString() + "," + Locale.UK.toString());
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_BRANCHE, VMBranchType.SERVICES.getCode() + "," + VMBranchType.HVAC.getCode());
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_TYPE, MEDIA_TYPE);
        verifyNoMoreInteractions(integrationService);
    }

    @Test
    public void testUpdateMetadataWithInvalidSelClassifications() {
        productMediaImportStrategy.setResourceType(ResourceType.DOCUMENT);

        BrandCategoryModel brandA = new BrandCategoryModel();
        brandA.setCode(BRAND_A);

        VariantProductModel productA = new VariantProductModel();
        productA.setCode(PRODUCT_A);
        productA.setSupercategories(Collections.singletonList(brandA));
        productA.setBrandSeries(BRAND_SERIES_A);
        productA.setVendorItemNumber_current(SUPPLIER_ITEM_REF_A);
        productA.setSupplier_Logistic(MANUFACTURER_A);
        productA.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_A);
        productA.setVmBranch(VMBranchType.SERVICES);
        productA.setSelClassification("V");

        BrandCategoryModel brandB = new BrandCategoryModel();
        brandB.setCode(BRAND_B);

        VariantProductModel productB = new VariantProductModel();
        productB.setCode(PRODUCT_B);
        productB.setSupercategories(Collections.singletonList(brandB));
        productB.setBrandSeries(BRAND_SERIES_B);
        productB.setVendorItemNumber_current(SUPPLIER_ITEM_REF_B);
        productB.setSupplier_Logistic(MANUFACTURER_B);
        productB.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_B);
        productB.setVmBranch(VMBranchType.HVAC);
        productB.setSelClassification("C");

        productMediaImportStrategy.updateMetaData(EXTERNAL_ID, Sets.newHashSet(Lists.newArrayList(Locale.US, Locale.UK)), Sets.newLinkedHashSet(Lists.newArrayList(productA, productB)));

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_TYPE, MEDIA_TYPE);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SKU, PRODUCT_A + "," + PRODUCT_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDNAME, BRAND_A + "," + BRAND_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDSERIE, BRAND_SERIES_A + "," + BRAND_SERIES_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SUPPLIER_ITEM_REF, "");
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.MANUFACTURER, MANUFACTURER_A + "," + MANUFACTURER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BOOKKEEPING_SUPPLIER, BOOKKEEPING_SUPPLIER_A + "," + BOOKKEEPING_SUPPLIER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_LANGUAGE, Locale.US.toString() + "," + Locale.UK.toString());
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.DOCUMENT_BRANCHE, VMBranchType.SERVICES.getCode() + "," + VMBranchType.HVAC.getCode());
        verifyNoMoreInteractions(integrationService);
    }

    @Test
    public void testUpdateMetadataWithOtherResourceType() {
        BrandCategoryModel brandA = new BrandCategoryModel();
        brandA.setCode(BRAND_A);

        VariantProductModel productA = new VariantProductModel();
        productA.setCode(PRODUCT_A);
        productA.setSupercategories(Collections.singletonList(brandA));
        productA.setBrandSeries(BRAND_SERIES_A);
        productA.setVendorItemNumber_current(SUPPLIER_ITEM_REF_A);
        productA.setSupplier_Logistic(MANUFACTURER_A);
        productA.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_A);

        BrandCategoryModel brandB = new BrandCategoryModel();
        brandB.setCode(BRAND_B);

        VariantProductModel productB = new VariantProductModel();
        productB.setCode(PRODUCT_B);
        productB.setSupercategories(Collections.singletonList(brandB));
        productB.setBrandSeries(BRAND_SERIES_B);
        productB.setVendorItemNumber_current(SUPPLIER_ITEM_REF_B);
        productB.setSupplier_Logistic(MANUFACTURER_B);
        productB.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_B);

        productMediaImportStrategy.updateMetaData(EXTERNAL_ID, Sets.newHashSet(Lists.newArrayList(Locale.US, Locale.UK)), Sets.newLinkedHashSet(Lists.newArrayList(productA, productB)));

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SKU, PRODUCT_A + "," + PRODUCT_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDNAME, BRAND_A + "," + BRAND_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDSERIE, BRAND_SERIES_A + "," + BRAND_SERIES_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SUPPLIER_ITEM_REF, SUPPLIER_ITEM_REF_A + "," + SUPPLIER_ITEM_REF_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.MANUFACTURER, MANUFACTURER_A + "," + MANUFACTURER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BOOKKEEPING_SUPPLIER, BOOKKEEPING_SUPPLIER_A + "," + BOOKKEEPING_SUPPLIER_B);
        verifyNoMoreInteractions(integrationService);
    }

    @Test
    public void testUpdateMetadataWithVariants() {
        BrandCategoryModel brandA = new BrandCategoryModel();
        brandA.setCode(BRAND_A);

        VariantProductModel variantA = new VariantProductModel();
        variantA.setCode(PRODUCT_A);
        variantA.setSupercategories(Collections.singletonList(brandA));
        variantA.setBrandSeries(BRAND_SERIES_A);
        variantA.setVendorItemNumber_current(SUPPLIER_ITEM_REF_A);
        variantA.setSupplier_Logistic(MANUFACTURER_A);
        variantA.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_A);

        ProductModel productA = new ProductModel();
        productA.setVariants(Collections.singletonList(variantA));

        BrandCategoryModel brandB = new BrandCategoryModel();
        brandB.setCode(BRAND_B);

        VariantProductModel variantB = new VariantProductModel();
        variantB.setCode(PRODUCT_B);
        variantB.setSupercategories(Collections.singletonList(brandB));
        variantB.setBrandSeries(BRAND_SERIES_B);
        variantB.setVendorItemNumber_current(SUPPLIER_ITEM_REF_B);
        variantB.setSupplier_Logistic(MANUFACTURER_B);
        variantB.setSupplier_Bookkeeper(BOOKKEEPING_SUPPLIER_B);

        ProductModel productB = new ProductModel();
        productB.setVariants(Collections.singletonList(variantB));

        productMediaImportStrategy.updateMetaData(EXTERNAL_ID, Sets.newHashSet(Lists.newArrayList(Locale.US, Locale.UK)), Sets.newLinkedHashSet(Lists.newArrayList(productA, productB)));

        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SKU, PRODUCT_A + "," + PRODUCT_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDNAME, BRAND_A + "," + BRAND_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BRANDSERIE, BRAND_SERIES_A + "," + BRAND_SERIES_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.SUPPLIER_ITEM_REF, SUPPLIER_ITEM_REF_A + "," + SUPPLIER_ITEM_REF_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.MANUFACTURER, MANUFACTURER_A + "," + MANUFACTURER_B);
        verify(integrationService).updateField(EXTERNAL_ID, MetaDataField.BOOKKEEPING_SUPPLIER, BOOKKEEPING_SUPPLIER_A + "," + BOOKKEEPING_SUPPLIER_B);
        verifyNoMoreInteractions(integrationService);
    }

    @Test
    public void testUpdate() {
        ProductModel product = ProductModelMockBuilder.aBaseProduct(CODE, Locale.CHINA).build();

        MediaContainerModel container = new MediaContainerModel();
        container.setQualifier(QUALIFIER);

        MediaModel mediaThumbnail = new MediaModel();
        MediaModel mediaPreview = new MediaModel();

        MediaFormatModel mediaFormatThumbnail = new MediaFormatModel();
        MediaFormatModel mediaFormatPreview = new MediaFormatModel();

        mediaFormatThumbnail.setQualifier(Size.THUMBNAIL_VANMARCKE.name());
        mediaFormatPreview.setQualifier(Size.PREVIEW.name());

        mediaThumbnail.setMediaFormat(mediaFormatThumbnail);
        mediaPreview.setMediaFormat(mediaFormatPreview);

        Collection<MediaModel> medias = new ArrayList<>();
        medias.add(mediaThumbnail);
        medias.add(mediaPreview);
        container.setMedias(medias);

        doNothing().when(productMediaImportStrategy).callSuper(container, product, ATTRIBUTE, MediaAttributeType.PACKSHOT);

        productMediaImportStrategy.update(container, product, ATTRIBUTE, MediaAttributeType.PACKSHOT);

        verify(productMediaImportStrategy).callSuper(container, product, ATTRIBUTE, MediaAttributeType.PACKSHOT);
        verify(modelService).setAttributeValue(product, ProductModel.PICTURE, mediaPreview);
        verify(modelService).setAttributeValue(product, ProductModel.THUMBNAIL, mediaThumbnail);
    }
}