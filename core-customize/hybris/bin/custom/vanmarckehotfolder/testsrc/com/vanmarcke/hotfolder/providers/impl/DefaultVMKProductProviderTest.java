package com.vanmarcke.hotfolder.providers.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKProductProviderTest {

    private static final String EXCEPTION_MSG = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private ProductService productService;
    @InjectMocks
    private DefaultVMKProductProvider defaultVMKProductProvider;

    @Test
    public void testGet() {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        ProductModel productModel1 = mock(ProductModel.class);
        ProductModel productModel2 = mock(ProductModel.class);
        UnknownIdentifierException exception = new UnknownIdentifierException(EXCEPTION_MSG);

        when(productService.getProductForCode(catalogVersion, "code1")).thenReturn(productModel1);
        when(productService.getProductForCode(catalogVersion, "code3")).thenReturn(null);
        when(productService.getProductForCode(catalogVersion, "code4")).thenThrow(exception);
        when(productService.getProductForCode(catalogVersion, "code2")).thenReturn(productModel2);

        Set<ProductModel> result = defaultVMKProductProvider.get(catalogVersion, asList("code1", "code2", "code2", "code3", "code4"));
        assertThat(result).containsOnly(productModel1, productModel2);
    }

    @Test(expected = IllegalStateException.class)
    public void testGet_withBaseProductsAndVariantProducts() {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        VariantProductModel productModel1 = mock(VariantProductModel.class);
        ProductModel productModel2 = mock(ProductModel.class);

        when(productService.getProductForCode(catalogVersion, "code1")).thenReturn(productModel1);
        when(productService.getProductForCode(catalogVersion, "code2")).thenReturn(productModel2);

        defaultVMKProductProvider.get(catalogVersion, asList("code1", "code2"));
    }
}