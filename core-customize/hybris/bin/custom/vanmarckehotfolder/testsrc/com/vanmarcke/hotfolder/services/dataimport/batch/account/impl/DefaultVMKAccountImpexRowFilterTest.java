package com.vanmarcke.hotfolder.services.dataimport.batch.account.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKAccountImpexRowFilterTest {

    @InjectMocks
    private DefaultVMKAccountImpexRowFilter accountImpexRowFilter;

    @Test
    public void testFilter_withBillingAddress() {
        Map<Integer, String> row = new HashMap<>();
        row.put(3, "Y");

        boolean result = accountImpexRowFilter.filter(row);
        assertThat(result).isTrue();
    }

    @Test
    public void testFilter_withoutBillingAddress() {
        Map<Integer, String> row = new HashMap<>();
        row.put(3, "N");

        boolean result = accountImpexRowFilter.filter(row);
        assertThat(result).isFalse();
    }

}