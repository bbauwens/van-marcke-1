package com.vanmarcke.hotfolder.services.dataimport.batch.account.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKAccountImportASMEmployeeAdapterTest {

    @Mock
    private ModelService modelService;
    @Mock
    private UserService userService;
    @InjectMocks
    private DefaultVMKAccountImportASMEmployeeAdapter defaultVMKAccountImportASMEmployeeAdapter;

    @Test
    public void testPerformImport_alreadyExistingASMEmployee() {
        final String cellValue = "EA:teckortrijk@vmk.be";
        final String affectationEmail = "teckortrijk@vmk.be";
        final Item b2bUnit = mock(Item.class);
        final EmployeeModel asmEmployee = mock(EmployeeModel.class);

        when(this.userService.getUserForUID(affectationEmail)).thenReturn(asmEmployee);
        this.defaultVMKAccountImportASMEmployeeAdapter.performImport(cellValue, b2bUnit);

        verify(this.modelService, never()).create(EmployeeModel.class);
    }

    @Test
    public void testPerformImport_ASMEmployeeNotExists() {
        final String cellValue = "EA:teckortrijk@vmk.be";
        final String affectationEmail = "teckortrijk@vmk.be";
        final Item b2bUnit = mock(Item.class);
        final EmployeeModel newASMEmployee = mock(EmployeeModel.class);
        final UserGroupModel ug = mock(UserGroupModel.class);

        when(this.modelService.create(EmployeeModel.class)).thenReturn(newASMEmployee);
        when(this.userService.getUserForUID(affectationEmail)).thenThrow(UnknownIdentifierException.class);
        when(this.userService.getUserGroupForUID("asagentsalesmanagergroup")).thenReturn(ug);
        this.defaultVMKAccountImportASMEmployeeAdapter.performImport(cellValue, b2bUnit);

        verify(this.modelService).create(EmployeeModel.class);
        verify(this.modelService).save(newASMEmployee);
    }
}