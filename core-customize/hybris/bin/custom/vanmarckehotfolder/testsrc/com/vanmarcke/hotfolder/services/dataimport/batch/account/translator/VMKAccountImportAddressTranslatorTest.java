package com.vanmarcke.hotfolder.services.dataimport.batch.account.translator;

import com.vanmarcke.hotfolder.services.dataimport.batch.account.VMKAccountImportAddressAdapter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.Item;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

@UnitTest
public class VMKAccountImportAddressTranslatorTest {

    private static final String TEST_IMPORT = "address";

    private VMKAccountImportAddressTranslator translator;
    @Mock
    private VMKAccountImportAddressAdapter vmkAccountImportAddressAdapter;
    @Mock
    private Item item;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        (this.translator = new VMKAccountImportAddressTranslator()).setVMKAccountImportAddressAdapter(this.vmkAccountImportAddressAdapter);
    }

    @Test
    public void test() {
        this.translator.performImport(TEST_IMPORT, this.item);
        ((VMKAccountImportAddressAdapter) verify((Object) this.vmkAccountImportAddressAdapter)).performImport(TEST_IMPORT, this.item);
    }
}