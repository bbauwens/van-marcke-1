package com.vanmarcke.hotfolder.resourcespace.service.impl;

import com.vanmarcke.hotfolder.resourcespace.api.VMKCreateResourceCommand;
import com.vanmarcke.hotfolder.resourcespace.api.VMKDeleteResourceCommand;
import com.vanmarcke.hotfolder.resourcespace.api.VMKGetResourcePathCommand;
import com.vanmarcke.hotfolder.resourcespace.api.VMKUpdateFieldCommand;
import com.vanmarcke.hotfolder.resourcespace.enums.MetaDataField;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;
import com.vanmarcke.hotfolder.services.VMKMediaService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.model.ModelService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import java.io.File;
import java.util.GregorianCalendar;

import static java.lang.String.format;
import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * The {@code DefaultVMKResourceSpaceServiceTest} class contains the unit tests for the
 * {@link DefaultVMKResourceSpaceService} class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 09-10-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKResourceSpaceServiceTest {

    private static final String FILE_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String RESOURCE_ID = RandomStringUtils.randomNumeric(10);
    private static final String VALUE = RandomStringUtils.randomAlphabetic(10);
    private static final String EXTENSION = RandomStringUtils.randomAlphabetic(3);

    @Mock
    private ModelService modelService;

    @Mock
    private VMKMediaService mediaService;

    @Mock
    private CommandExecutor commandExecutor;

    @Mock
    private DefaultVMKResourceSpaceService resourceSpaceService;

    @Captor
    private ArgumentCaptor<VMKCreateResourceCommand> vmkCreateResourceCommandArgumentCaptor;

    @Captor
    private ArgumentCaptor<VMKDeleteResourceCommand> vmkDeleteResourceCommandArgumentCaptor;


    @Captor
    private ArgumentCaptor<VMKGetResourcePathCommand> vmkGetResourcePathCommandArgumentCaptor;

    @Captor
    private ArgumentCaptor<VMKUpdateFieldCommand> vmkUpdateFieldCommandArgumentCaptor;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        resourceSpaceService = spy(new DefaultVMKResourceSpaceService(modelService, mediaService, commandExecutor, false, "https://localhost:9002", "https://resourcespace", "https://vanmarcke"));
    }

    @Test
    public void testCreateResource() {
        File file = mock(File.class);

        GregorianCalendar cal = new GregorianCalendar(2020, 3, 16);

        CatalogUnawareMediaModel media = mock(CatalogUnawareMediaModel.class);
        when(media.getURL()).thenReturn("/medias?context=vanmarcke");
        when(media.getCreationtime()).thenReturn(cal.getTime());

        when(mediaService.createMediaFromFile(file, CatalogUnawareMediaModel.class)).thenReturn(media);

        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(HttpStatus.OK);
        when(response.getPayLoad()).thenReturn(RESOURCE_ID);

        when(commandExecutor.executeCommand(vmkCreateResourceCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        doNothing().when(resourceSpaceService).updateField(RESOURCE_ID, MetaDataField.TITLE, FILE_NAME);
        doNothing().when(resourceSpaceService).updateField(RESOURCE_ID, MetaDataField.DATE, "2020-04-16 00:00");

        String actualResourceID = resourceSpaceService.create(file, FILE_NAME, ResourceType.DOCUMENT);

        assertThat(actualResourceID).isEqualTo(RESOURCE_ID);

        assertThat(vmkCreateResourceCommandArgumentCaptor.getValue().getRequestParams())
                .hasSize(8)
                .includes(entry("function", "create_resource"),
                        entry("param1", ResourceType.DOCUMENT.value()),
                        entry("param2", "0"),
                        entry("param3", "https%3A%2F%2Flocalhost%3A9002%2Fmedias%3Fcontext%3Dvanmarcke"),
                        entry("param4", ""),
                        entry("param5", ""),
                        entry("param6", "false"),
                        entry("param7", ""));

        verify(mediaService).createMediaFromFile(file, CatalogUnawareMediaModel.class);
        verify(resourceSpaceService).updateField(RESOURCE_ID, MetaDataField.TITLE, FILE_NAME);
        verify(resourceSpaceService).updateField(RESOURCE_ID, MetaDataField.DATE, "2020-04-16 00:00");
        verify(modelService).remove(media);
    }

    @Test
    public void testCreateResourceWithError() {
        File file = mock(File.class);

        GregorianCalendar cal = new GregorianCalendar(2020, 3, 16);

        CatalogUnawareMediaModel media = mock(CatalogUnawareMediaModel.class);
        when(media.getURL()).thenReturn("/medias?context=vanmarcke");
        when(media.getCreationtime()).thenReturn(cal.getTime());

        when(mediaService.createMediaFromFile(file, CatalogUnawareMediaModel.class)).thenReturn(media);

        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(INTERNAL_SERVER_ERROR);

        when(commandExecutor.executeCommand(vmkCreateResourceCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        thrown.expect(SystemException.class);
        thrown.expectMessage(format("Could not create new resource with resource type '%s' and upload url '%s'.", ResourceType.DOCUMENT, "https://localhost:9002/medias?context=vanmarcke"));

        resourceSpaceService.create(file, FILE_NAME, ResourceType.DOCUMENT);
    }

    @Test
    public void testDeleteWithSuccessfulResponse() {
        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(HttpStatus.OK);
        when(response.getPayLoad()).thenReturn("true");

        when(commandExecutor.executeCommand(vmkDeleteResourceCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        resourceSpaceService.delete(RESOURCE_ID);

        assertThat(vmkDeleteResourceCommandArgumentCaptor.getValue().getRequestParams())
                .hasSize(2)
                .includes(entry("function", "delete_resource"),
                        entry("param1", RESOURCE_ID));
    }

    @Test
    public void testDeleteWithUnsuccessfulResponse() {
        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(HttpStatus.OK);
        when(response.getPayLoad()).thenReturn("false");

        when(commandExecutor.executeCommand(vmkDeleteResourceCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        resourceSpaceService.delete(RESOURCE_ID);

        assertThat(vmkDeleteResourceCommandArgumentCaptor.getValue().getRequestParams())
                .hasSize(2)
                .includes(entry("function", "delete_resource"),
                        entry("param1", RESOURCE_ID));
    }

    @Test
    public void testDeleteWithError() {
        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(INTERNAL_SERVER_ERROR);

        when(commandExecutor.executeCommand(vmkUpdateFieldCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        thrown.expect(SystemException.class);
        thrown.expectMessage(format("Could not remove resource '%s'.", RESOURCE_ID));

        resourceSpaceService.delete(RESOURCE_ID);
    }

    @Test
    public void testGetResourcePath() {
        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(HttpStatus.OK);
        when(response.getPayLoad()).thenReturn("\"https:\\/\\/resourcespace\\/filestore\\/vanmarcke\\/1\\/4\\/5\\/9_38024c09f01fe87\\/1459_480a1f29e553fdc.jpg\"");

        when(commandExecutor.executeCommand(vmkGetResourcePathCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        String result = resourceSpaceService.getPath(RESOURCE_ID, Size.MEDIUM, EXTENSION);

        assertThat(result).isEqualTo("https://vanmarcke/filestore/vanmarcke/1/4/5/9_38024c09f01fe87/1459_480a1f29e553fdc.jpg");

        assertThat(vmkGetResourcePathCommandArgumentCaptor.getValue().getRequestParams())
                .hasSize(6)
                .includes(entry("function", "get_resource_path"),
                        entry("param1", RESOURCE_ID),
                        entry("param2", "false"),
                        entry("param3", Size.MEDIUM.value()),
                        entry("param4", "false"),
                        entry("param5", "jpg"));
    }

    @Test
    public void testGetResourcePathWithoutSize() {
        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(HttpStatus.OK);
        when(response.getPayLoad()).thenReturn("\"https:\\/\\/resourcespace\\/filestore\\/vanmarcke\\/1\\/4\\/5\\/9_38024c09f01fe87\\/1459_480a1f29e553fdc.pdf\"");

        when(commandExecutor.executeCommand(vmkGetResourcePathCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        String result = resourceSpaceService.getPath(RESOURCE_ID, null, EXTENSION);

        assertThat(result).isEqualTo("https://vanmarcke/filestore/vanmarcke/1/4/5/9_38024c09f01fe87/1459_480a1f29e553fdc.pdf");

        assertThat(vmkGetResourcePathCommandArgumentCaptor.getValue().getRequestParams())
                .hasSize(6)
                .includes(entry("function", "get_resource_path"),
                        entry("param1", RESOURCE_ID),
                        entry("param2", "false"),
                        entry("param3", ""),
                        entry("param4", "false"),
                        entry("param5", EXTENSION));
    }

    @Test
    public void testGetResourcePathwithError() {
        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(INTERNAL_SERVER_ERROR);

        when(commandExecutor.executeCommand(any(VMKGetResourcePathCommand.class), eq(null))).thenReturn(response);

        thrown.expect(SystemException.class);
        thrown.expectMessage(format("Could not get path for resource '%s' and extension '%s'.", RESOURCE_ID, EXTENSION));

        resourceSpaceService.getPath(RESOURCE_ID, Size.MEDIUM, EXTENSION);
    }

    @Test
    public void testUpdateFieldWithSuccessfulResponse() {
        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(HttpStatus.OK);
        when(response.getPayLoad()).thenReturn("true");

        when(commandExecutor.executeCommand(vmkUpdateFieldCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        resourceSpaceService.updateField(RESOURCE_ID, MetaDataField.SKU, VALUE);

        assertThat(vmkUpdateFieldCommandArgumentCaptor.getValue().getRequestParams())
                .hasSize(4)
                .includes(entry("function", "update_field"),
                        entry("param1", RESOURCE_ID),
                        entry("param2", MetaDataField.SKU.value()),
                        entry("param3", VALUE));
    }

    @Test
    public void testUpdateFieldWithEmptyValue() {
        resourceSpaceService.updateField(RESOURCE_ID, MetaDataField.SKU, null);

        verifyZeroInteractions(commandExecutor);
    }

    @Test
    public void testUpdateFieldWithUnsuccessfulResponse() {
        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(HttpStatus.OK);
        when(response.getPayLoad()).thenReturn("false");

        when(commandExecutor.executeCommand(vmkUpdateFieldCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        resourceSpaceService.updateField(RESOURCE_ID, MetaDataField.SKU, VALUE);

        assertThat(vmkUpdateFieldCommandArgumentCaptor.getValue().getRequestParams())
                .hasSize(4)
                .includes(entry("function", "update_field"),
                        entry("param1", RESOURCE_ID),
                        entry("param2", MetaDataField.SKU.value()),
                        entry("param3", VALUE));
    }

    @Test
    public void testUpdateFieldWithError() {
        Response<String> response = mock(Response.class);
        when(response.getHttpStatus()).thenReturn(INTERNAL_SERVER_ERROR);

        when(commandExecutor.executeCommand(vmkUpdateFieldCommandArgumentCaptor.capture(), eq(null))).thenReturn(response);

        resourceSpaceService.updateField(RESOURCE_ID, MetaDataField.SKU, VALUE);

        assertThat(vmkUpdateFieldCommandArgumentCaptor.getValue().getRequestParams())
                .hasSize(4)
                .includes(entry("function", "update_field"),
                        entry("param1", RESOURCE_ID),
                        entry("param2", MetaDataField.SKU.value()),
                        entry("param3", VALUE));
    }
}