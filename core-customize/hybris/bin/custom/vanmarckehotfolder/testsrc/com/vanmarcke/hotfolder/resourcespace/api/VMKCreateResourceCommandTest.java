package com.vanmarcke.hotfolder.resourcespace.api;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCreateResourceCommandTest {

    private VMKCreateResourceCommand command;

    @Test
    public void testConstructor() {
        command = new VMKCreateResourceCommand("type", "url");

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.GET);
        assertThat(command.getServiceName()).isEqualTo("vmkResourceSpaceService");
        assertThat(command.getUrl()).isEqualTo("/");
        assertThat(command.getRequestParams()).hasSize(8)
                .includes(entry("function", "create_resource"),
                        entry("param1", "type"),
                        entry("param2", "0"),
                        entry("param3", "url"),
                        entry("param4", ""),
                        entry("param5", ""),
                        entry("param6", "false"),
                        entry("param7", ""));
        assertThat(command.getResponseType()).isEqualTo(String.class);
    }

    @Test
    public void testBuildUri() {
        Map<String, Object> properties = new HashMap<>();
        properties.put("baseUrl", "https://localhost:9002");
        properties.put("user", "admin");
        properties.put("privateKey", "27b6f4ebee4848a87da46b123f44aa7e02c4605002ea3d1416c159a0f8c03217");

        command = new VMKCreateResourceCommand("type", "https://localhost:9002/medias/vanmarcke.pdf?context=blue");

        String result = command.buildUri(properties);

        assertThat(result).isEqualTo("https://localhost:9002/?user=admin&function=create_resource&param1=type&param2=0&param3=https%3A%2F%2Flocalhost%3A9002%2Fmedias%2Fvanmarcke.pdf%3Fcontext%3Dblue&param4=&param5=&param6=false&param7=&sign=271b8dbe82da01f30b88c69eb97eb2b8c19c1d06b254d69e55841bc70e90288c");
    }

}