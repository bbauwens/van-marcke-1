package com.vanmarcke.hotfolder.strategies.media.impl;

import com.vanmarcke.hotfolder.data.MediaFactoryParameter;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import com.vanmarcke.hotfolder.services.VMKMediaService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.*;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * The {@code DefaultVMKMediaFactoryStrategyTest} class contains the unit tests for the
 * {@link DefaultVMKMediaFactoryStrategy} class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 29-10-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKMediaFactoryStrategyTest {

    private static final String CODE = RandomStringUtils.random(10);
    private static final String OLD_EXTERNAL_ID = RandomStringUtils.randomAlphabetic(10);
    private static final String EXTERNAL_ID = RandomStringUtils.randomAlphabetic(10);
    private static final String EXTENSION = "jpg";
    private static final String BASE_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String FILE_NAME = BASE_NAME + "." + EXTENSION;
    private static final String FOLDER_QUALIFIER = RandomStringUtils.randomAlphabetic(10);
    private static final String URL = RandomStringUtils.randomAlphabetic(10);
    private static final String NAME_US = RandomStringUtils.randomAlphabetic(10);
    private static final String NAME_UK = RandomStringUtils.randomAlphabetic(10);
    private static final String VIDEO = "VIDEO";

    @Mock
    private ModelService modelService;

    @Mock
    private VMKResourceSpaceService integrationService;

    @Mock
    private VMKMediaService mediaService;

    @InjectMocks
    private DefaultVMKMediaFactoryStrategy defaultVMKMediaFactoryStrategy;

    @Test
    public void testCreateWithRecentlyUpdatedItem() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        MediaFactoryParameter parameter = new MediaFactoryParameter();
        parameter.setCatalogVersion(catalogVersion);
        parameter.setQualifier(CODE);

        MediaModel media = mock(MediaModel.class);
        when(media.getExternalID()).thenReturn(EXTERNAL_ID);
        when(media.getModifiedtime()).thenReturn(new Date());

        when(mediaService.getMedia(catalogVersion, CODE)).thenReturn(media);

        MediaModel actualMedia = defaultVMKMediaFactoryStrategy.create(parameter);

        Assertions
                .assertThat(actualMedia)
                .isEqualTo(media);

        verify(mediaService).getMedia(catalogVersion, CODE);
        verifyZeroInteractions(integrationService);
        verifyNoMoreInteractions(mediaService);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testCreateWithNewItem() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        File file = mock(File.class);
        when(file.getName()).thenReturn(FILE_NAME);

        Map<Locale, String> localizedNames = new HashMap<>();
        localizedNames.put(Locale.US, NAME_US);
        localizedNames.put(Locale.UK, NAME_UK);

        MediaFactoryParameter parameter = new MediaFactoryParameter();
        parameter.setCatalogVersion(catalogVersion);
        parameter.setQualifier(CODE);
        parameter.setFile(file);
        parameter.setType("PHOTO");
        parameter.setFolderQualifier(FOLDER_QUALIFIER);
        parameter.setLocalizedNames(localizedNames);

        UnknownIdentifierException e = mock(UnknownIdentifierException.class);

        when(mediaService.getMedia(catalogVersion, CODE)).thenThrow(e);

        MediaModel media = mock(MediaModel.class);
        when(media.getModifiedtime()).thenReturn(new Date());

        when(modelService.create(MediaModel.class)).thenReturn(media);

        when(integrationService.create(file, BASE_NAME, ResourceType.PHOTO)).thenReturn(EXTERNAL_ID);

        MediaFolderModel folder = new MediaFolderModel();

        when(mediaService.getOrCreateMediaFolder(FOLDER_QUALIFIER)).thenReturn(folder);

        when(integrationService.getPath(EXTERNAL_ID, null, EXTENSION)).thenReturn(URL);

        MediaModel actualMedia = defaultVMKMediaFactoryStrategy.create(parameter);

        Assertions
                .assertThat(actualMedia)
                .isEqualTo(media);

        verify(mediaService).getMedia(catalogVersion, CODE);
        verify(modelService).create(MediaModel.class);
        verify(integrationService, never()).delete(anyString());
        verify(integrationService).create(file, BASE_NAME, ResourceType.PHOTO);
        verify(mediaService).getOrCreateMediaFolder(FOLDER_QUALIFIER);
        verify(integrationService).getPath(EXTERNAL_ID, null, EXTENSION);
        verify(modelService).save(media);

        verify(media).setCode(CODE);
        verify(media).setCatalogVersion(catalogVersion);
        verify(media).setExternalID(EXTERNAL_ID);
        verify(media).setFolder(folder);
        verify(media).setRealFileName(FILE_NAME);
        verify(media).setURL(URL);
        verify(media).setName(NAME_US, Locale.US);
        verify(media).setName(NAME_UK, Locale.UK);
    }

    @Test
    public void testCreateWithNewItem_VIDEO() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        File file = mock(File.class);
        when(file.getName()).thenReturn(FILE_NAME);

        Map<Locale, String> localizedNames = new HashMap<>();
        localizedNames.put(Locale.US, NAME_US);
        localizedNames.put(Locale.UK, NAME_UK);

        MediaFactoryParameter parameter = new MediaFactoryParameter();
        parameter.setCatalogVersion(catalogVersion);
        parameter.setQualifier(CODE);
        parameter.setFile(file);
        parameter.setType(VIDEO);
        parameter.setFolderQualifier(FOLDER_QUALIFIER);
        parameter.setLocalizedNames(localizedNames);

        UnknownIdentifierException e = mock(UnknownIdentifierException.class);

        when(mediaService.getMedia(catalogVersion, CODE)).thenThrow(e);

        MediaModel media = mock(MediaModel.class);
        when(media.getModifiedtime()).thenReturn(new Date());

        when(modelService.create(MediaModel.class)).thenReturn(media);

        when(integrationService.create(file, BASE_NAME, ResourceType.VIDEO)).thenReturn(EXTERNAL_ID);

        MediaFolderModel folder = new MediaFolderModel();

        when(mediaService.getOrCreateMediaFolder(FOLDER_QUALIFIER)).thenReturn(folder);

        MediaFormatModel videoMediaFormat = mock(MediaFormatModel.class);
        when(mediaService.getFormat(VIDEO)).thenReturn(videoMediaFormat);

        when(integrationService.getPath(EXTERNAL_ID, null, EXTENSION)).thenReturn(URL);

        MediaModel actualMedia = defaultVMKMediaFactoryStrategy.create(parameter);

        Assertions
                .assertThat(actualMedia)
                .isEqualTo(media);

        verify(mediaService).getMedia(catalogVersion, CODE);
        verify(mediaService).getFormat(VIDEO);
        verify(modelService).create(MediaModel.class);
        verify(integrationService, never()).delete(anyString());
        verify(integrationService).create(file, BASE_NAME, ResourceType.VIDEO);
        verify(mediaService).getOrCreateMediaFolder(FOLDER_QUALIFIER);
        verify(integrationService).getPath(EXTERNAL_ID, null, EXTENSION);
        verify(modelService).save(media);

        verify(media).setMediaFormat(videoMediaFormat);
        verify(media).setCode(CODE);
        verify(media).setCatalogVersion(catalogVersion);
        verify(media).setExternalID(EXTERNAL_ID);
        verify(media).setFolder(folder);
        verify(media).setRealFileName(FILE_NAME);
        verify(media).setURL(URL);
        verify(media).setName(NAME_US, Locale.US);
        verify(media).setName(NAME_UK, Locale.UK);
    }

    @Test
    public void testCreateWithExistingItem() {
        CatalogVersionModel catalogVersion = new CatalogVersionModel();

        File file = mock(File.class);
        when(file.getName()).thenReturn(FILE_NAME);

        Map<Locale, String> localizedNames = new HashMap<>();
        localizedNames.put(Locale.US, NAME_US);
        localizedNames.put(Locale.UK, NAME_UK);

        MediaFactoryParameter parameter = new MediaFactoryParameter();
        parameter.setCatalogVersion(catalogVersion);
        parameter.setQualifier(CODE);
        parameter.setFile(file);
        parameter.setType("PHOTO");
        parameter.setFolderQualifier(FOLDER_QUALIFIER);
        parameter.setLocalizedNames(localizedNames);

        GregorianCalendar calendar = new GregorianCalendar(2020, Calendar.JANUARY, 1);

        MediaModel media = mock(MediaModel.class);
        when(media.getExternalID()).thenReturn(OLD_EXTERNAL_ID);
        when(media.getModifiedtime()).thenReturn(calendar.getTime());

        when(mediaService.getMedia(catalogVersion, CODE)).thenReturn(media);

        when(integrationService.create(file, BASE_NAME, ResourceType.PHOTO)).thenReturn(EXTERNAL_ID);

        MediaFolderModel folder = new MediaFolderModel();

        when(mediaService.getOrCreateMediaFolder(FOLDER_QUALIFIER)).thenReturn(folder);

        when(integrationService.getPath(EXTERNAL_ID, null, EXTENSION)).thenReturn(URL);

        MediaModel actualMedia = defaultVMKMediaFactoryStrategy.create(parameter);

        Assertions
                .assertThat(actualMedia)
                .isEqualTo(media);

        verify(mediaService).getMedia(catalogVersion, CODE);
        verify(modelService, never()).create(MediaModel.class);
        verify(integrationService).delete(OLD_EXTERNAL_ID);
        verify(integrationService).create(file, BASE_NAME, ResourceType.PHOTO);
        verify(mediaService).getOrCreateMediaFolder(FOLDER_QUALIFIER);
        verify(integrationService).getPath(EXTERNAL_ID, null, EXTENSION);
        verify(modelService).save(media);

        verify(media).setExternalID(EXTERNAL_ID);
        verify(media).setFolder(folder);
        verify(media).setRealFileName(FILE_NAME);
        verify(media).setURL(URL);
        verify(media).setName(NAME_US, Locale.US);
        verify(media).setName(NAME_UK, Locale.UK);
    }
}