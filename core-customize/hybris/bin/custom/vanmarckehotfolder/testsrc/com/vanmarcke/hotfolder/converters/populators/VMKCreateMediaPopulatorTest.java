package com.vanmarcke.hotfolder.converters.populators;

import com.vanmarcke.hotfolder.data.MediaData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCreateMediaPopulatorTest {

    private VMKCreateMediaPopulator createMediaPopulator;

    @Before
    public void setup() {
        createMediaPopulator = new VMKCreateMediaPopulator();
    }

    @Test
    public void testPopulate() {
        File source = importFile("test/vmkcreatemediadatapopulator/testcase1.meta");
        Map<Locale, String> localizedNames = new HashMap<>();
        Locale locale_fr_fr = new Locale("fr", "fr");
        Locale locale_nl_be = new Locale("nl", "be");
        localizedNames.put(locale_fr_fr, "testcase1");
        localizedNames.put(locale_nl_be, "Annotatie 2019-10-01 140658");

        MediaData result = new MediaData();
        createMediaPopulator.populate(source, result);

        assertThat(result.getQualifier()).isEqualTo("testcase1");
        assertThat(result.getBrands()).containsExactly("b123546", "b123555");
        assertThat(result.getCategories()).containsExactly("c123546", "c123555");
        assertThat(result.getProducts()).containsExactly("p123546", "p123555");
        assertThat(result.getLocalizedNames()).isEqualTo(localizedNames);
    }

    @Test
    public void testPopulate_withOnlyProducts() {
        File source = importFile("test/vmkcreatemediadatapopulator/testcase2.meta");
        Map<Locale, String> localizedNames = new HashMap<>();
        Locale locale_nl_be = new Locale("nl", "be");
        localizedNames.put(locale_nl_be, "Annotatie 2019-10-01 140658");

        MediaData result = new MediaData();
        createMediaPopulator.populate(source, result);

        assertThat(result.getQualifier()).isEqualTo("testcase2");
        assertThat(result.getBrands()).isEmpty();
        assertThat(result.getCategories()).isEmpty();
        assertThat(result.getProducts()).containsExactly("123546", "123555");
        assertThat(result.getLocalizedNames()).isEqualTo(localizedNames);
    }

    @Test
    public void testPopulate_withOnlyBrands() {
        File source = importFile("test/vmkcreatemediadatapopulator/testcase3.meta");
        Map<Locale, String> localizedNames = new HashMap<>();
        localizedNames.put(Locale.ENGLISH, "Annotation 2019-10-01 140658");

        MediaData result = new MediaData();
        createMediaPopulator.populate(source, result);

        assertThat(result.getQualifier()).isEqualTo("testcase3");
        assertThat(result.getBrands()).containsExactly("SAUNIER DUVAL", "GEBERIT");
        assertThat(result.getCategories()).isEmpty();
        assertThat(result.getProducts()).isEmpty();
        assertThat(result.getLocalizedNames()).isEqualTo(localizedNames);
    }

    @Test
    public void testPopulate_withOnlyCategories() {
        File source = importFile("test/vmkcreatemediadatapopulator/testcase4.meta");
        Map<Locale, String> localizedNames = new HashMap<>();
        localizedNames.put(Locale.GERMAN, "Anmerkung 2019-10-01 140658");

        MediaData result = new MediaData();
        createMediaPopulator.populate(source, result);

        assertThat(result.getQualifier()).isEqualTo("testcase4");
        assertThat(result.getBrands()).isEmpty();
        assertThat(result.getCategories()).containsExactly("cat_toilets");
        assertThat(result.getProducts()).isEmpty();
        assertThat(result.getLocalizedNames()).isEqualTo(localizedNames);
    }

    @Test
    public void testPopulate_withoutTitles() {
        File source = importFile("test/vmkcreatemediadatapopulator/testcase5.meta");
        Map<Locale, String> localizedNames = new HashMap<>();
        localizedNames.put(Locale.ENGLISH, "testcase5");

        MediaData result = new MediaData();
        createMediaPopulator.populate(source, result);

        assertThat(result.getQualifier()).isEqualTo("testcase5");
        assertThat(result.getProducts()).containsExactly("123546");
        assertThat(result.getLocalizedNames()).isEqualTo(localizedNames);
    }

    private File importFile(final String fileName) {
        final ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }

}