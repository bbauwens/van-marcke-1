package com.vanmarcke.hotfolder.providers.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKCategoryProviderTest {

    private static final String EXCEPTION_MSG = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private CategoryService categoryService;
    @InjectMocks
    private DefaultVMKCategoryProvider defaultVMKCategoryProvider;

    @Test
    public void testGet() {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);
        CategoryModel categoryModel2 = mock(CategoryModel.class);
        BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);
        UnknownIdentifierException exception1 = new UnknownIdentifierException(EXCEPTION_MSG);
        AmbiguousIdentifierException exception2 = new AmbiguousIdentifierException(EXCEPTION_MSG);
        IllegalArgumentException exception3 = new IllegalArgumentException(EXCEPTION_MSG);

        when(categoryService.getCategoryForCode(catalogVersion, "code1")).thenReturn(categoryModel1);
        when(categoryService.getCategoryForCode(catalogVersion, "code3")).thenReturn(null);
        when(categoryService.getCategoryForCode(catalogVersion, "code4")).thenThrow(exception1);
        when(categoryService.getCategoryForCode(catalogVersion, "code6")).thenThrow(exception2);
        when(categoryService.getCategoryForCode(catalogVersion, "code7")).thenThrow(exception3);
        when(categoryService.getCategoryForCode(catalogVersion, "code2")).thenReturn(brandCategoryModel);
        when(categoryService.getCategoryForCode(catalogVersion, "code5")).thenReturn(categoryModel2);

        Set<CategoryModel> result = defaultVMKCategoryProvider.get(catalogVersion, asList("code1", "code2", "code2", "code3", "code4", "code5", "code6", "code7"));

        assertThat(result).containsOnly(categoryModel1, categoryModel2);
    }

}