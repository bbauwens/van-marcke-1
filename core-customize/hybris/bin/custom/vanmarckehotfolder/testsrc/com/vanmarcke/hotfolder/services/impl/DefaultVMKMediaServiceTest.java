package com.vanmarcke.hotfolder.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaIOException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static org.fest.assertions.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKMediaServiceTest {

    @Mock
    private FlexibleSearchService flexibleSearchService;
    @Mock
    private ModelService modelService;
    @InjectMocks
    @Spy
    private DefaultVMKMediaService defaultVMKMediaService = new DefaultVMKMediaService();

    @Test
    public void testCreateMediaFromFile() {
        doNothing().when(defaultVMKMediaService).setStreamForMedia(any(MediaModel.class), any(InputStream.class));

        CatalogUnawareMediaModel catalogUnawareMediaModel = mock(CatalogUnawareMediaModel.class);

        File file = importFile("test/defaultvmkmediaservice/testcase1.txt");

        when(modelService.create(CatalogUnawareMediaModel.class)).thenReturn(catalogUnawareMediaModel);

        CatalogUnawareMediaModel result = defaultVMKMediaService.createMediaFromFile(file, CatalogUnawareMediaModel.class);

        assertThat(result).isEqualTo(catalogUnawareMediaModel);

        verify(catalogUnawareMediaModel).setCode(Mockito.endsWith("-testcase1"));
        verify(catalogUnawareMediaModel).setRealFileName("testcase1.txt");

        verify(modelService).save(catalogUnawareMediaModel);
        verify(modelService).refresh(catalogUnawareMediaModel);
    }

    @Test(expected = MediaIOException.class)
    public void testCreateMediaFromFile_withException() {
        doThrow(IOException.class).when(defaultVMKMediaService).setStreamForMedia(any(MediaModel.class), any(InputStream.class));

        CatalogUnawareMediaModel catalogUnawareMediaModel = mock(CatalogUnawareMediaModel.class);

        File file = importFile("test/defaultvmkmediaservice/testcase1.txt");

        when(modelService.create(CatalogUnawareMediaModel.class)).thenReturn(catalogUnawareMediaModel);

        defaultVMKMediaService.createMediaFromFile(file, CatalogUnawareMediaModel.class);
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testGetMediaContainer() throws Exception {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        //whenNew(MediaContainerModel.class).withNoArguments().thenReturn(mediaContainerModel);

        when(flexibleSearchService.getModelByExample(any(MediaContainerModel.class))).thenThrow(ModelNotFoundException.class);

        defaultVMKMediaService.getMediaContainer(catalogVersion, "qualifier");
    }

    @Test
    public void testGetMediaContainer_withExistingMediaContainer() throws Exception {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        MediaContainerModel existingMediaContainerModel = mock(MediaContainerModel.class);

        when(flexibleSearchService.getModelByExample(any(MediaContainerModel.class))).thenReturn(existingMediaContainerModel);

        MediaContainerModel result = defaultVMKMediaService.getMediaContainer(catalogVersion, "qualifier");

        assertThat(result).isNotNull();
    }

    @Test
    public void testGetOrCreateMediaFolder() {
        MediaFolderModel mediaFolder = mock(MediaFolderModel.class);

        doThrow(UnknownIdentifierException.class).when(defaultVMKMediaService).getFolder("qualifier");

        when(modelService.create(MediaFolderModel.class)).thenReturn(mediaFolder);

        MediaFolderModel result = defaultVMKMediaService.getOrCreateMediaFolder("qualifier");

        assertThat(result).isEqualTo(mediaFolder);

        verify(mediaFolder).setQualifier("qualifier");
        verify(mediaFolder).setPath("qualifier");

        verify(modelService).save(mediaFolder);
    }

    @Test
    public void testGetOrCreateMediaFolder_withExistingMediaFolder() {
        MediaFolderModel mediaFolder = mock(MediaFolderModel.class);

        doReturn(mediaFolder).when(defaultVMKMediaService).getFolder("qualifier");

        MediaFolderModel result = defaultVMKMediaService.getOrCreateMediaFolder("qualifier");

        assertThat(result).isEqualTo(mediaFolder);

        verifyZeroInteractions(modelService);
    }

    @Test
    public void testGetOrCreateMediaFormat() {
        MediaFormatModel mediaFormatModel = mock(MediaFormatModel.class);

        doThrow(UnknownIdentifierException.class).when(defaultVMKMediaService).getFormat("qualifier");

        when(modelService.create(MediaFormatModel.class)).thenReturn(mediaFormatModel);

        MediaFormatModel result = defaultVMKMediaService.getOrCreateMediaFormat("qualifier");

        assertThat(result).isEqualTo(mediaFormatModel);

        verify(mediaFormatModel).setQualifier("qualifier");

        verify(modelService).save(mediaFormatModel);
    }

    @Test
    public void testGetOrCreateMediaFormat_withExistingMediaFormat() {
        MediaFormatModel mediaFormatModel = mock(MediaFormatModel.class);

        doReturn(mediaFormatModel).when(defaultVMKMediaService).getFormat("qualifier");

        MediaFormatModel result = defaultVMKMediaService.getOrCreateMediaFormat("qualifier");

        assertThat(result).isEqualTo(mediaFormatModel);

        verifyZeroInteractions(modelService);
    }

    private File importFile(final String fileName) {
        final ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }

}