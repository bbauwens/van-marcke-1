package com.vanmarcke.hotfolder.resourcespace.api;

import eu.elision.integration.command.AbstractRESTCommand;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static eu.elision.integration.command.executer.CommandExecutor.BASE_URL;
import static eu.elision.integration.command.executer.CommandExecutor.USER;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.apache.commons.collections4.MapUtils.getString;

public abstract class AbstractVMKCommand<T> extends AbstractRESTCommand<T> {

    @Override
    protected String buildUri(final Map<String, Object> properties) {
        final StringBuilder queryBuilder = new StringBuilder()
                .append("user=").append(getString(properties, USER))
                .append(getRequestParams().entrySet()
                        .stream()
                        .map(e -> "&" + e.getKey() + "=" + e.getValue())
                        .collect(joining()));

        final String query = queryBuilder.toString();

        final String sign = sha256Hex(getString(properties, "privateKey").concat(query));

        return getString(properties, BASE_URL) + getUrl() + "?" + query + "&sign=" + sign;
    }

    @Override
    protected RestTemplate newRestTemplate() {
        final RestTemplate restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultVMKUriTemplateHandler());
        return restTemplate;
    }
}