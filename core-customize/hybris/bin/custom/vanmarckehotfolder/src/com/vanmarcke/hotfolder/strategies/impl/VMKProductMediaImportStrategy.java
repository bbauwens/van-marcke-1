package com.vanmarcke.hotfolder.strategies.impl;

import com.vanmarcke.core.enums.VMBranchType;
import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.hotfolder.data.MediaAttributeType;
import com.vanmarcke.hotfolder.data.MediaData;
import com.vanmarcke.hotfolder.resourcespace.enums.MetaDataField;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;
import com.vanmarcke.hotfolder.strategies.AbstractVMKMediaImportStrategy;
import com.vanmarcke.hotfolder.utils.VMKMediaImportUtils;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import static com.vanmarcke.hotfolder.constants.VanmarckehotfolderConstants.MEDIA;
import static com.vanmarcke.hotfolder.constants.VanmarckehotfolderConstants.MEDIA.CHECKBOX_LIST_SEPARATOR;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.collections4.IterableUtils.matchesAny;

/**
 * The {@code AbstractVMKCategoryMediaImportStrategy} class contains common business logic to import product media.
 *
 * @author Taki Korovessis, Tom van den Berg, Christiaan Janssen
 * @since 13-10-2019
 */
public class VMKProductMediaImportStrategy extends AbstractVMKMediaImportStrategy<ProductModel> {

    private static final String COMMODITY = "C";
    private static final String PRIVATE = "V";

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<ProductModel> getItems(CatalogVersionModel catalogVersion, MediaData media) {
        return itemProvider.get(catalogVersion, media.getProducts());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(MediaContainerModel container, ProductModel product, String attribute, MediaAttributeType attributeType) {
        callSuper(container, product, attribute, attributeType);

        if (MediaAttributeType.PACKSHOT.equals(attributeType) && 0 == getSequenceId(container)) {
            modelService.setAttributeValue(product, ProductModel.PICTURE, VMKMediaImportUtils.getMediaForSize(container, Size.PREVIEW));
            modelService.setAttributeValue(product, ProductModel.THUMBNAIL, VMKMediaImportUtils.getMediaForSize(container, Size.THUMBNAIL_VANMARCKE));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateMetaData(String externalID, Set<Locale> locales, Set<ProductModel> products) {
        super.updateMetaData(externalID, locales, products);

        Set<ProductModel> variantProducts;
        if (!matchesAny(products, VariantProductModel.class::isInstance)) {
            variantProducts = new LinkedHashSet<>(products.size());
            products.forEach(e -> variantProducts.addAll(e.getVariants()));
        } else {
            variantProducts = new LinkedHashSet<>(products);
        }

        integrationService.updateField(externalID, MetaDataField.SKU, variantProducts.stream()
                .map(ProductModel::getCode)
                .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));

        Set<BrandCategoryModel> brands = new LinkedHashSet<>();
        variantProducts.forEach(p -> p.getSupercategories().stream()
                .filter(BrandCategoryModel.class::isInstance)
                .map(BrandCategoryModel.class::cast)
                .forEach(brands::add));

        integrationService.updateField(externalID, MetaDataField.BRANDNAME, brands.stream()
                .map(BrandCategoryModel::getCode)
                .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));

        integrationService.updateField(externalID, MetaDataField.BRANDSERIE, variantProducts.stream()
                .map(ProductModel::getBrandSeries)
                .filter(Objects::nonNull)
                .distinct()
                .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));

        integrationService.updateField(externalID, MetaDataField.SUPPLIER_ITEM_REF, variantProducts.stream()
                .filter(this::isNotPrivateOrCommodity)
                .map(ProductModel::getVendorItemNumber_current)
                .filter(Objects::nonNull)
                .distinct()
                .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));

        integrationService.updateField(externalID, MetaDataField.MANUFACTURER, variantProducts.stream()
                .map(ProductModel::getSupplier_Logistic)
                .filter(Objects::nonNull)
                .distinct()
                .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));

        integrationService.updateField(externalID, MetaDataField.BOOKKEEPING_SUPPLIER, variantProducts.stream()
                .map(ProductModel::getSupplier_Bookkeeper)
                .filter(Objects::nonNull)
                .distinct()
                .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));

        if (ResourceType.DOCUMENT == resourceType) {
            integrationService.updateField(externalID, MetaDataField.DOCUMENT_LANGUAGE, locales.stream()
                    .map(Locale::toString)
                    .collect(joining(CHECKBOX_LIST_SEPARATOR)));

            integrationService.updateField(externalID, MetaDataField.DOCUMENT_BRANCHE, variantProducts.stream()
                    .map(ProductModel::getVmBranch)
                    .filter(Objects::nonNull)
                    .distinct()
                    .map(VMBranchType::getCode)
                    .collect(joining(MEDIA.CHECKBOX_LIST_SEPARATOR)));
        }
    }

    /**
     * Checks whether the given {@code product}'s sel classification is either private or commodity.
     *
     * @param product the product to check
     * @return {@code true} in case the given {@code product}'s sel classification is neither private nor commodity,
     * {@code false} otherwise
     */
    private boolean isNotPrivateOrCommodity(ProductModel product) {
        return !PRIVATE.equals(product.getSelClassification()) && !COMMODITY.equals(product.getSelClassification());
    }

    protected void callSuper(MediaContainerModel container, ProductModel product, String attribute, MediaAttributeType attributeType) {
        // Added for testing purposes
        super.update(container, product, attribute, attributeType);
    }
}