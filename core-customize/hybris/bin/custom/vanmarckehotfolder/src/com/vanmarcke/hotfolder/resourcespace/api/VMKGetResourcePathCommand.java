package com.vanmarcke.hotfolder.resourcespace.api;

import eu.elision.integration.command.configuration.annotation.CommandConfig;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

@CommandConfig(serviceName = "vmkResourceSpaceService", url = "/")
public class VMKGetResourcePathCommand extends AbstractVMKCommand<String> {

    public VMKGetResourcePathCommand(final String resource, final String size, final String extension) {
        addRequestParam("function", "get_resource_path");
        addRequestParam("param1", resource);
        addRequestParam("param2", "false");
        addRequestParam("param3", defaultIfBlank(size, EMPTY));
        addRequestParam("param4", "false");
        addRequestParam("param5", extension);
    }

}