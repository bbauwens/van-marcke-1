package com.vanmarcke.hotfolder.resourcespace.service.impl;

import com.google.common.base.Preconditions;
import com.vanmarcke.hotfolder.resourcespace.api.VMKCreateResourceCommand;
import com.vanmarcke.hotfolder.resourcespace.api.VMKDeleteResourceCommand;
import com.vanmarcke.hotfolder.resourcespace.api.VMKGetResourcePathCommand;
import com.vanmarcke.hotfolder.resourcespace.api.VMKUpdateFieldCommand;
import com.vanmarcke.hotfolder.resourcespace.enums.MetaDataField;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;
import com.vanmarcke.hotfolder.resourcespace.service.VMKResourceSpaceService;
import com.vanmarcke.hotfolder.services.VMKMediaService;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.model.ModelService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.UnsupportedEncodingException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;
import static org.springframework.http.HttpStatus.OK;

/**
 * The {@code DefaultVMKResourceSpaceService} class implements the business logic to integrate with ResourceSpace.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 09-10-2019
 */
public class DefaultVMKResourceSpaceService implements VMKResourceSpaceService {

    private static final Logger LOGGER = Logger.getLogger(DefaultVMKResourceSpaceService.class);

    private static final String DEV_MEDIA_URL = "https://picsum.photos/1920/1080.jpg";
    private static final String DEV_EXTENSION = "jpg";

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
    private static final String RS_SIZE_EXTENSION = "jpg";

    private final ModelService modelService;
    private final VMKMediaService mediaService;
    private final CommandExecutor commandExecutor;
    private final Boolean developmentMode;
    private final String mediaBaseUrl;
    private final String resourceSpaceMediaBaseUrl;
    private final String vanMarckeMediaBaseUrl;

    /**
     * Creates a new instance of the {@link DefaultVMKResourceSpaceService} class.
     *
     * @param modelService              the model service
     * @param mediaService              the media service
     * @param commandExecutor           the command executor
     * @param developmentMode           flag to indicate whether development mode is enabled
     * @param mediaBaseUrl              the default media base URL in SAP Commerce
     * @param resourceSpaceMediaBaseUrl the media base URL in ResourceSpace
     * @param vanMarckeMediaBaseUrl     the media base URL in the Van Marcke storefront
     */
    public DefaultVMKResourceSpaceService(ModelService modelService,
                                          VMKMediaService mediaService,
                                          CommandExecutor commandExecutor,
                                          Boolean developmentMode,
                                          String mediaBaseUrl,
                                          String resourceSpaceMediaBaseUrl,
                                          String vanMarckeMediaBaseUrl) {
        this.modelService = modelService;
        this.mediaService = mediaService;
        this.commandExecutor = commandExecutor;
        this.developmentMode = BooleanUtils.isTrue(developmentMode);
        this.mediaBaseUrl = mediaBaseUrl;
        this.resourceSpaceMediaBaseUrl = resourceSpaceMediaBaseUrl;
        this.vanMarckeMediaBaseUrl = vanMarckeMediaBaseUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String create(File file, String title, ResourceType type) {
        validateParameterNotNullStandardMessage("file", file);
        validateParameterNotNullStandardMessage("title", title);
        Preconditions.checkArgument(StringUtils.isNotBlank(title), "Parameter title can not be blank or null.");

        MediaModel media = null;
        try {
            media = mediaService.createMediaFromFile(file, CatalogUnawareMediaModel.class);

            String id = create(media, type);
            updateField(id, MetaDataField.TITLE, title);
            updateField(id, MetaDataField.DATE, FastDateFormat.getInstance(DATE_FORMAT).format(media.getCreationtime()));
            return id;
        } finally {
            if (media != null) {
                modelService.remove(media);
            }
        }
    }

    /**
     * Creates a new resource for the given {@code media} and {@code type}.
     *
     * @param media the media
     * @param type  the type
     * @return the resource ID
     */
    private String create(MediaModel media, ResourceType type) {
        String url = getURL(media);

        Response<String> response = commandExecutor.executeCommand(new VMKCreateResourceCommand(type.value(), url), null);
        if (isSuccessful(response) && !"false".equals(response.getPayLoad())) {
            return response.getPayLoad();
        }

        throw new SystemException(format("Could not create new resource with resource type '%s' and upload url '%s'.", type, url));
    }

    /**
     * Returns the URL for the given {@code media}.
     *
     * @param media the media
     * @return the URL
     */
    private String getURL(MediaModel media) {
        return developmentMode ? DEV_MEDIA_URL : mediaBaseUrl + media.getURL();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(String id) {
        validateParameterNotNullStandardMessage("resource ID", id);

        Response<String> response = commandExecutor.executeCommand(new VMKDeleteResourceCommand(id), null);
        if (isSuccessful(response)) {
            if (!BooleanUtils.toBoolean(response.getPayLoad())) {
                LOGGER.error(format("Failed to remove resource '%s'.", id));
            }
            return;
        }

        throw new SystemException(format("Could not remove resource '%s'.", id));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPath(String id, Size size, String extension) {
        validateParameterNotNullStandardMessage("resource ID", id);

        Response<String> response = commandExecutor.executeCommand(new VMKGetResourcePathCommand(id, getSize(size), getExtension(size, extension)), null);
        if (isSuccessful(response)) {
            return extractUrl(response.getPayLoad());
        }

        throw new SystemException(format("Could not get path for resource '%s' and extension '%s'.", id, extension));
    }

    /**
     * Returns the size for the given {@code size}.
     *
     * @param size the size
     * @return the size value
     */
    private String getSize(Size size) {
        return size != null ? size.value() : null;
    }

    /**
     * Returns the extension for the given {@code size} and {@code extension}.
     *
     * @param size      the size
     * @param extension the extension
     * @return the extension
     */
    private String getExtension(Size size, String extension) {
        return size != null && StringUtils.isNotEmpty(size.value()) ? RS_SIZE_EXTENSION : developmentMode ? DEV_EXTENSION : extension;
    }

    /**
     * Extracts the URL from the given {@code payload} and returns it.
     *
     * @param payload the payload which contains the URL
     * @return the URL
     */
    private String extractUrl(String payload) {
        String url = StringUtils.replace(StringUtils.substringBetween(payload, "\""), "\\/", "/");
        return StringUtils.replace(url, resourceSpaceMediaBaseUrl, vanMarckeMediaBaseUrl);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateField(String id, MetaDataField field, String value) {
        validateParameterNotNullStandardMessage("resource ID", id);
        validateParameterNotNullStandardMessage("field", field);

        if (StringUtils.isEmpty(value)) {
            return;
        }

        try {
            Response<String> response = commandExecutor.executeCommand(new VMKUpdateFieldCommand(id, field.value(), value), null);
            if (!isSuccessful(response) || !BooleanUtils.toBoolean(response.getPayLoad())) {
                LOGGER.error(format("Failed to set value '%s' for resource '%s' and field '%s'.", value, id, field));
            }
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(format("Failed to set value '%s' for resource '%s' and field '%s'.", value, id, field));
        }
    }

    /**
     * Checks whether the given {@code response} is successful.
     *
     * @param response the response
     * @return {@code true} in case the given {@code response} is successful, {@code false} otherwise
     */
    private boolean isSuccessful(Response<String> response) {
        return response.getHttpStatus() == OK && StringUtils.isNotEmpty(response.getPayLoad());
    }
}