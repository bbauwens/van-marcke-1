package com.vanmarcke.hotfolder.translators;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static de.hybris.platform.commerceservices.enums.SiteChannel.*;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.split;

public class VMKCountryMarketSiteChannelTranslator extends AbstractValueTranslator {

    @Override
    public Object importValue(final String valueExpr, final Item toItem) throws JaloInvalidParameterException {
        clearStatus();

        final Set<SiteChannel> channels = new HashSet<>(3);
        final String[] channelExpr = split(valueExpr, ',');

        if (channelExpr[0].equalsIgnoreCase("Y")) {
            channels.add(B2B);
        }

        if (channelExpr[1].equalsIgnoreCase("Y")) {
            channels.add(DIY);
        }

        if (channelExpr[2].equalsIgnoreCase("Y")) {
            channels.add(B2C);
        }

        return channels.isEmpty() ? null : channels;
    }

    @Override
    public String exportValue(final Object value) throws JaloInvalidParameterException {
        clearStatus();

        if (value instanceof Collection) {
            final Collection<SiteChannel> channels = (Collection<SiteChannel>) value;
            return channels.stream()
                    .map(SiteChannel::getCode)
                    .collect(joining(", "));
        } else {
            return EMPTY;
        }
    }
}