package com.vanmarcke.hotfolder.services;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;

import java.io.File;

public interface VMKMediaService extends MediaService {

    /**
     * Creates a media from a file
     *
     * @param file  the file
     * @param clazz the model class
     * @param <T>   the type of the media
     * @return a media
     */
    <T extends MediaModel> T createMediaFromFile(File file, Class<T> clazz);

    /**
     * Gets the media container identified unique by given qualifier and catalog version.
     *
     * @param catalogVersion the catalog version
     * @param qualifier      the qualifier
     * @return the matching media container
     */
    MediaContainerModel getMediaContainer(CatalogVersionModel catalogVersion, String qualifier);

    /**
     * Gets or creates a media folder based on the specified qualifier
     *
     * @param qualifier the qualifier
     * @return the media folder
     */
    MediaFolderModel getOrCreateMediaFolder(String qualifier);

    /**
     * Gets or creates a media format based on the specified qualifier
     *
     * @param qualifier the qualifier
     * @return the media format
     */
    MediaFormatModel getOrCreateMediaFormat(String qualifier);

}