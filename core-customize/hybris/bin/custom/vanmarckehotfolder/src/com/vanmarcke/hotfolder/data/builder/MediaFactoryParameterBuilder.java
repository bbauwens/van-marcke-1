package com.vanmarcke.hotfolder.data.builder;

import com.vanmarcke.hotfolder.data.MediaFactoryParameter;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.io.File;
import java.util.Locale;
import java.util.Map;

/**
 * The {@code MediaFactoryParameterBuilder} class is an implementation of the builder pattern. It makes the creation of
 * instance of the {@link MediaFactoryParameter} class easier.
 *
 * @author Christiaan Janssen
 * @since 08-04-2020
 */
public final class MediaFactoryParameterBuilder {

    private final String qualifier;
    private final String folderQualifier;
    private final ResourceType type;
    private final File file;
    private final CatalogVersionModel catalogVersion;
    private final Map<Locale, String> localizedNames;

    /**
     * Creates a new instance of the {@link MediaFactoryParameterBuilder} class.
     *
     * @param qualifier       the qualifier
     * @param folderQualifier the folder qualifier
     * @param type            the type of resource
     * @param file            the file
     * @param catalogVersion  the catalog version
     * @param localizedNames  the localized names
     */
    private MediaFactoryParameterBuilder(String qualifier,
                                         String folderQualifier,
                                         ResourceType type,
                                         File file,
                                         CatalogVersionModel catalogVersion,
                                         Map<Locale, String> localizedNames) {
        this.qualifier = qualifier;
        this.folderQualifier = folderQualifier;
        this.type = type;
        this.file = file;
        this.catalogVersion = catalogVersion;
        this.localizedNames = localizedNames;
    }

    /**
     * Creates a new instance of the {@link MediaFactoryParameterBuilder} class.
     *
     * @param qualifier       the qualifier
     * @param folderQualifier the folder qualifier
     * @param type            the type of resource
     * @param file            the file
     * @param catalogVersion  the catalog version
     * @param localizedNames  the localized names
     * @return the {@link MediaFactoryParameterBuilder} instance for method chaining.
     */
    public static MediaFactoryParameterBuilder aMediaFactoryParameter(String qualifier,
                                                                      String folderQualifier,
                                                                      ResourceType type,
                                                                      File file,
                                                                      CatalogVersionModel catalogVersion,
                                                                      Map<Locale, String> localizedNames) {
        return new MediaFactoryParameterBuilder(qualifier, folderQualifier, type, file, catalogVersion, localizedNames);
    }

    /**
     * Creates and returns a new instance of the {@link MediaFactoryParameter} class.
     *
     * @return the {@link MediaFactoryParameter} instance
     */
    public MediaFactoryParameter build() {
        MediaFactoryParameter parameter = new MediaFactoryParameter();
        parameter.setQualifier(qualifier);
        parameter.setFolderQualifier(folderQualifier);
        parameter.setType(type.name());
        parameter.setFile(file);
        parameter.setCatalogVersion(catalogVersion);
        parameter.setLocalizedNames(localizedNames);
        return parameter;
    }
}
