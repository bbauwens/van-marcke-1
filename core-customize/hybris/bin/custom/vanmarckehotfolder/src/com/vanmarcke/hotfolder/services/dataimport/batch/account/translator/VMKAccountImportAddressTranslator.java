package com.vanmarcke.hotfolder.services.dataimport.batch.account.translator;

import com.vanmarcke.hotfolder.services.dataimport.batch.account.VMKAccountImportAddressAdapter;
import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import org.apache.commons.lang.StringUtils;

/**
 * Translator for updating the account address.
 */
public class VMKAccountImportAddressTranslator extends AbstractSpecialValueTranslator {

    private static final String MODIFIER_NAME_ADAPTER = "adapter";
    private static final String DEFAULT_IMPORT_ADAPTER_NAME = "defaultVMKAccountImportAddressAdapter";

    private VMKAccountImportAddressAdapter vmkAccountImportAddressAdapter;

    @Override
    public void init(final SpecialColumnDescriptor columnDescriptor) {
        String beanName = columnDescriptor.getDescriptorData().getModifier(MODIFIER_NAME_ADAPTER);
        if (StringUtils.isBlank(beanName)) {
            beanName = DEFAULT_IMPORT_ADAPTER_NAME;
        }
        this.vmkAccountImportAddressAdapter = (VMKAccountImportAddressAdapter) Registry.getApplicationContext().getBean(beanName);
    }

    @Override
    public void performImport(final String cellValue, final Item processedItem) {
        this.vmkAccountImportAddressAdapter.performImport(cellValue, processedItem);
    }

    /**
     * @param vmkAccountImportAddressAdapter the vmkAccountImportAddressAdapter to set
     */
    public void setVMKAccountImportAddressAdapter(final VMKAccountImportAddressAdapter vmkAccountImportAddressAdapter) {
        this.vmkAccountImportAddressAdapter = vmkAccountImportAddressAdapter;
    }
}
