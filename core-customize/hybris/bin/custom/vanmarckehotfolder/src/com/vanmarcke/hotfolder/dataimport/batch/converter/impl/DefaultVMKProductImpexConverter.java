package com.vanmarcke.hotfolder.dataimport.batch.converter.impl;

import com.vanmarcke.hotfolder.translators.hooks.VMKImpexTranslatorHook;
import de.hybris.platform.acceleratorservices.dataimport.batch.converter.impl.DefaultImpexConverter;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

import static java.lang.String.format;
import static org.apache.commons.collections4.MapUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

public class DefaultVMKProductImpexConverter extends DefaultImpexConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultVMKProductImpexConverter.class);

    private static final char APPROVAL_STATUS_CHAR = 'A';
    private static final char BASE_PRODUCT_CHAR = 'B';
    private static final char ENUM_CHAR = 'E';
    private static final char SEQUENCE_CHAR = 'S';

    private String impexRow;
    private String catalog;

    private CatalogVersionService catalogVersionService;
    private ProductService productService;
    private Map<String, VMKImpexTranslatorHook> translatorHooks;

    private VariantProductModel product;

    @Override
    public String convert(final Map<Integer, String> row, final Long sequenceId) {
        if (isNotEmpty(row)) {
            final String productCode = row.get(1);
            if (isBlank(productCode)) {
                throw new IllegalArgumentException("Missing value for product code");
            }
            product = getProductForCode(productCode);
        }
        return super.convert(row, sequenceId);
    }

    @Override
    protected void processRow(final Map<Integer, String> row, final Long sequenceId, final StringBuilder builder,
                              final int copyIdx, final int idx, final int endIdx) {
        if (endIdx < 0) {
            throw new SystemException("Invalid row syntax [brackets not closed]: " + impexRow);
        }

        builder.append(impexRow.substring(copyIdx, idx));

        switch (impexRow.charAt(idx + 1)) {
            case SEQUENCE_CHAR:
                builder.append(sequenceId);
                break;
            case APPROVAL_STATUS_CHAR:
            case BASE_PRODUCT_CHAR:
            case ENUM_CHAR:
                builder.append(translate(row, idx, endIdx));
                break;
            default:
                processValues(row, builder, idx, endIdx);
        }
    }

    protected String translate(final Map<Integer, String> row, final int idx, final int endIdx) {
        final String translatorIndex = impexRow.substring(idx + 1, endIdx);
        final String rowIndex = impexRow.substring(idx + 2, endIdx);
        final String value = isNumeric(rowIndex) ? row.get(Integer.valueOf(rowIndex)) : null;
        return translatorHooks.get(translatorIndex).translate(value, product);
    }

    protected VariantProductModel getProductForCode(final String code) {
        try {
            CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(catalog, "Staged");
            ProductModel product = productService.getProductForCode(catalogVersion, code);
            if (product instanceof VariantProductModel) {
                return (VariantProductModel) product;
            }
            LOGGER.error(format("Product with code '%s' and catalog version '%s.%s' is not a variant!", code, catalog, "Staged"));
        } catch (UnknownIdentifierException e) {
            // expected in case of new products
            LOGGER.debug(format("Product with code '%s' and catalog version '%s.%s' not found!", code, catalog, "Staged"));
        }
        return null;
    }

    @Override
    public void setImpexRow(final String impexRow) {
        super.setImpexRow(impexRow);
        this.impexRow = impexRow;
    }

    @Required
    public void setCatalog(final String catalog) {
        this.catalog = catalog;
    }

    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    @Required
    public void setTranslatorHooks(final Map<String, VMKImpexTranslatorHook> translatorHooks) {
        this.translatorHooks = translatorHooks;
    }
}