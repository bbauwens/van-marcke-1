package com.vanmarcke.hotfolder.event.handler;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;

/**
 * The {@link VMKAfterSaveEventHandler} interface defines the methods for after save handling.
 *
 * @author Christiaan Janssen
 * @since 31-08-2020
 */
public interface VMKAfterSaveEventHandler<T extends ItemModel> extends AfterSaveListener {

    /**
     * Handles the given {@code event}.
     *
     * @param event the after save event
     */
    void handle(AfterSaveEvent event, T item);
}
