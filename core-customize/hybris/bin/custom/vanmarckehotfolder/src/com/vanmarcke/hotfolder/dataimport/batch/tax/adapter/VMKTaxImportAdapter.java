package com.vanmarcke.hotfolder.dataimport.batch.tax.adapter;

import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.jalo.Item;

/**
 * The {@link VMKTaxImportAdapter} interface defines the methods to import tax information.
 *
 * @author Christiaan Janssen
 * @since 02-04-2021
 */
public interface VMKTaxImportAdapter {

    /**
     * Creates the a {@link TaxModel} and {@link TaxRowModel} istance for the given {@code taxInformation} and {@code item}.
     *
     * @param taxInformation the tax information
     * @param item           the item
     * @throws IllegalArgumentException if the taxInformation is empty, null or invalid or the item is null
     */
    void performImport(String taxInformation, Item item);
}
