package com.vanmarcke.hotfolder.resourcespace.api;

import eu.elision.integration.command.configuration.annotation.CommandConfig;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

@CommandConfig(serviceName = "vmkResourceSpaceService", url = "/")
public class VMKUpdateFieldCommand extends AbstractVMKCommand<String> {

    public VMKUpdateFieldCommand(final String resource, final String field, final String value) throws UnsupportedEncodingException {
        addRequestParam("function", "update_field");
        addRequestParam("param1", resource);
        addRequestParam("param2", field);
        addRequestParam("param3", URLEncoder.encode(defaultIfEmpty(value, EMPTY), StandardCharsets.UTF_8));
    }
}