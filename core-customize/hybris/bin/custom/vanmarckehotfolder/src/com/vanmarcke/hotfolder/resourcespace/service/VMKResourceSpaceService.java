package com.vanmarcke.hotfolder.resourcespace.service;

import com.vanmarcke.hotfolder.resourcespace.enums.MetaDataField;
import com.vanmarcke.hotfolder.resourcespace.enums.ResourceType;
import com.vanmarcke.hotfolder.resourcespace.enums.Size;

import java.io.File;

/**
 * The {@code VMKResourceSpaceService} interface defines the business logic to integrate with ResourceSpace.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 09-10-2019
 */
public interface VMKResourceSpaceService {

    /**
     * Creates a new resource for the given {@code file}, {@code title}, and {@code type}.
     *
     * @param file  the file to upload
     * @param title the title to set
     * @param type  the resource type
     * @return the ID of the newly created resource
     */
    String create(File file, String title, ResourceType type);

    /**
     * Removes the resource with the given {@code id}.
     *
     * @param id the unique identifier of the resource
     */
    void delete(String id);

    /**
     * Returns the path of the resource with the given {@code id}, {@code size}, and {@code extension}.
     *
     * @param id        the unique identifier of the resource
     * @param size      the requested size
     * @param extension the requested extension
     * @return the path of the resource
     */
    String getPath(String id, Size size, String extension);

    /**
     * Updates the {@code field} of the resource with the given {@code id}.
     *
     * @param id    the unique identifier of the resource
     * @param field the field to update
     * @param value the value to set
     */
    void updateField(String id, MetaDataField field, String value);
}