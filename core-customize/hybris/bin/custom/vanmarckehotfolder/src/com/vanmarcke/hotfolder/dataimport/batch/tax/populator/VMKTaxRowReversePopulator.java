package com.vanmarcke.hotfolder.dataimport.batch.tax.populator;

import com.vanmarcke.hotfolder.data.TaxDTO;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

/**
 * The {@link VMKTaxReversePopulator} class is used to populate instances of the {@link TaxRowModel} class with the
 * information from the instances of the {@link TaxDTO} class.
 *
 * @author Christiaan Janssen
 * @since 02-04-2021
 */
public class VMKTaxRowReversePopulator implements Populator<TaxDTO, TaxRowModel> {

    private final CommonI18NService commonI18NService;

    /**
     * Creates a new instance of the {@link VMKTaxRowReversePopulator} class.
     *
     * @param commonI18NService the I18N service
     */
    public VMKTaxRowReversePopulator(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(TaxDTO taxDTO, TaxRowModel taxRowModel) {
        taxRowModel.setCurrency(taxDTO.getCurrencyIsoCode() == null ? null : commonI18NService.getCurrency(taxDTO.getCurrencyIsoCode()));
        taxRowModel.setUg(taxDTO.getCountryIsoCode() == null ? null : UserTaxGroup.valueOf(taxDTO.getCountryIsoCode()));
        taxRowModel.setValue(taxDTO.getValue());
    }
}
