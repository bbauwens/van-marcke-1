package com.vanmarcke.hotfolder.resourcespace.enums;

public enum MetaDataField {
    // GLOBAL
    KEYWORDS_OTHER("1"), TITLE("8"), CREDIT("10"), DATE("12"), DESCRIPTION("18"),
    ORIGINAL_FILENAME("51"), SKU("86"), BRANDNAME("87"), BRANDSERIE("88"),
    SUPPLIER_ITEM_REF("89"), BOOKKEEPING_SUPPLIER("90"), MANUFACTURER("95"),
    // PHOTO
    PHOTO_TYPE("93"),
    //VIDEO
    FRAME_RATE("76"), VIDEO_BITRATE("77"), ASPECT_RATIO("78"), VIDEO_SIZE("79"),
    // DOCUMENT
    DOCUMENT_LANGUAGE("91"), DOCUMENT_TYPE("92"), DOCUMENT_BRANCHE("94");

    private final String value;

    MetaDataField(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}