package com.vanmarcke.hotfolder.dataimport.batch.product;

import de.hybris.platform.jalo.Item;

/**
 * Adapter to translate a brand import row into a service call.
 */
public interface VMKProductBrandImportAdapter {

    /**
     * Import a brand value.
     *
     * @param cellValue the cell value
     * @param product   the product
     */
    void performImport(String cellValue, Item product);
}