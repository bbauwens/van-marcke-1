package com.vanmarcke.hotfolder.services.dataimport.batch.account.impl;

import com.google.common.base.Preconditions;
import com.vanmarcke.hotfolder.services.dataimport.batch.account.VMKAccountImportASMEmployeeAdapter;
import com.vanmarcke.hotfolder.services.dataimport.batch.account.VMKAccountImportAddressAdapter;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.Collections;

/**
 * Default implementation of {@link VMKAccountImportAddressAdapter}.
 */
public class DefaultVMKAccountImportASMEmployeeAdapter implements VMKAccountImportASMEmployeeAdapter {
    private static final Logger LOG = Logger.getLogger(DefaultVMKAccountImportASMEmployeeAdapter.class);
    private static final String ASM_USER_GROUP = "asagentsalesmanagergroup";

    private ModelService modelService;
    private UserService userService;

    /**
     * Constructor for DefaultVMKAccountImportASMEmployeeAdapter
     *
     * @param modelService the modelService
     * @param userService  the userService
     */
    public DefaultVMKAccountImportASMEmployeeAdapter(final ModelService modelService, final UserService userService) {
        this.modelService = modelService;
        this.userService = userService;
    }

    @Override
    public void performImport(final String cellValue, final Item b2bUnit) {
        Preconditions.checkState(StringUtils.isNotEmpty(cellValue));
        Preconditions.checkNotNull(b2bUnit);

        try {
            final String[] values = cellValue.split(":", -1);
            final String affectationCode = values[0];
            final String affectationEmail = StringUtils.lowerCase(values[1]);

            if (StringUtils.isNotBlank(affectationEmail)) {
                findOrCreateASMEmployee(affectationEmail, affectationCode);
            }
        } catch (final Exception e) {
            LOG.warn("Could not import ASM Employee for " + b2bUnit + ": " + e);
            throw new SystemException("Could not import ASM Employee for " + b2bUnit, e);
        }
    }

    /**
     * Find or Create the ASM Employee based on the affectationEmail. If one exists, we do nothing.
     *
     * @param affectationEmail the affectation email
     * @param affectationCode  the affectation code
     * @return the employee model
     */
    private EmployeeModel findOrCreateASMEmployee(final String affectationEmail, final String affectationCode) {
        try {
            return (EmployeeModel) this.userService.getUserForUID(affectationEmail);
        } catch (final UnknownIdentifierException ex) {
            return createASMEmployee(affectationEmail, affectationCode);
        }
    }

    /**
     * Create an ASM employee with the given affectationEmail and affectationCode
     *
     * @param affectationEmail the affectation email
     * @param affectationCode  the affectation code
     * @return the employee model
     */
    protected EmployeeModel createASMEmployee(final String affectationEmail, final String affectationCode) {
        final EmployeeModel newASMEmployee = this.modelService.create(EmployeeModel.class);
        newASMEmployee.setUid(affectationEmail);
        newASMEmployee.setGroups(Collections.singleton(this.userService.getUserGroupForUID(ASM_USER_GROUP)));
        this.modelService.save(newASMEmployee);
        return newASMEmployee;
    }
}
