package com.vanmarcke.hotfolder.tasks;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexConverter;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.ImpexTransformerTask;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Required;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.util.Arrays.asList;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.startsWith;

public class DefaultVMKImpexTransformerTask extends ImpexTransformerTask {

    private static final String COMMENT_OUT = "#";

    private SessionService sessionService;
    private UserService userService;

    @Override
    public BatchHeader execute(final BatchHeader header) {
        return sessionService.executeInLocalView(new SessionExecutionBody() {
            @Override
            public BatchHeader execute() {
                try {
                    return doExecute(header);
                } catch (final Exception e) {
                    throw new SystemException(e);
                }
            }
        }, userService.getAdminUser());
    }

    protected BatchHeader doExecute(final BatchHeader header) throws UnsupportedEncodingException, FileNotFoundException {
        return super.execute(header);
    }

    @Override
    protected boolean convertFile(final BatchHeader header, final File file, final File impexFile, final ImpexConverter converter) {
        boolean result = false;
        PrintWriter errorWriter = null;

        try (OutputStream impexOutputStream = new FileOutputStream(impexFile);
             PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(impexOutputStream, getEncoding())));
             BufferedReader csvReader = new BufferedReader(new FileReader(file))) {
            writer.println(getReplacedHeader(header, converter));
            String line;
            int counter = 0;
            final String fieldSeparator = valueOf(getFieldSeparator());
            while ((line = csvReader.readLine()) != null) {
                if (counter >= getLinesToSkip() && !startsWith(line, COMMENT_OUT)) {
                    final List<String> values = asList(line.split(fieldSeparator))
                            .stream()
                            .map(String::trim)
                            .collect(toList());
                    final Map<Integer, String> row = IntStream.range(0, values.size())
                            .boxed()
                            .collect(toMap(identity(), values::get));
                    if (converter.filter(row)) {
                        try {
                            writer.println(converter.convert(row, header.getSequenceId()));
                            result = true;
                        } catch (final IllegalArgumentException e) {
                            errorWriter = writeErrorLine(file, line, errorWriter, e);
                        }
                    }
                }
                counter++;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(errorWriter);
        }

        return result;
    }

    /**
     * Prints an error line containing the reason and the source line to the error file
     *
     * @param file        the file
     * @param sourceLine  the source line
     * @param errorWriter the error writer
     * @param exception   the exception
     * @return error writer
     * @throws UnsupportedEncodingException
     * @throws FileNotFoundException
     */
    protected PrintWriter writeErrorLine(final File file, final String sourceLine, final PrintWriter errorWriter,
                                         final IllegalArgumentException exception) throws UnsupportedEncodingException, FileNotFoundException {
        PrintWriter result = errorWriter;
        if (result == null) {
            result = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(getErrorFile(file)), getEncoding())));
        }
        result.println(format("%s: %s", exception.getMessage(), sourceLine));
        return result;
    }

    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }
}
