package com.vanmarcke.facades.storesession.converters.populators;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.facades.site.data.BaseSiteData;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.util.Assert;

/**
 * Populator for BaseSiteData
 */
public class VMKSitePopulator<SOURCE extends BaseSiteModel, TARGET extends BaseSiteData> implements Populator<SOURCE, TARGET> {

    private Converter<CountryModel, CountryData> countryConverter;

    public VMKSitePopulator(final Converter<CountryModel, CountryData> countryConverter) {
        this.countryConverter = countryConverter;
    }

    @Override
    public void populate(final SOURCE source, final TARGET target) {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setUid(source.getUid());
        target.setName(source.getName());
        if (source instanceof CMSSiteModel) {
            final CMSSiteModel cmsSiteModel = (CMSSiteModel) source;

            populateCountry(cmsSiteModel, target);

            populateStartingNavigationNode(cmsSiteModel, target);
        }
    }

    protected void populateStartingNavigationNode(final CMSSiteModel source, final TARGET target) {
        if (source.getStartNavigationNode() != null) {
            target.setStartNavigationNode(source.getStartNavigationNode().getUid());
        }
    }

    protected void populateCountry(final CMSSiteModel source, final TARGET target) {
        final CommerceGroupModel commerceGroupModel = source.getCommerceGroup();
        if (commerceGroupModel != null) {
            target.setCountry(this.countryConverter.convert(commerceGroupModel.getCountry()));
        }
    }

}
