package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.sitechannel.AbstractSiteChannelAwarePopulator;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Variant populator to defining attributes
 */
public class VMKProductVariantDefiningAttributesPopulator extends AbstractSiteChannelAwarePopulator<VariantProductModel, VariantOptionData> {

    private final ClassificationService classificationService;
    private final Converter<Feature, FeatureData> featureConverter;

    @Autowired
    public VMKProductVariantDefiningAttributesPopulator(final ClassificationService classificationService, final Converter<Feature, FeatureData> featureConverter) {
        this.classificationService = classificationService;
        this.featureConverter = featureConverter;
    }

    @Override
    protected void doPopulate(final VariantProductModel source, final VariantOptionData target) {
        final FeatureList productFeatures = this.classificationService.getFeatures(source);
        final List<Feature> features = productFeatures.getFeatures();
        final Map<String, FeatureData> definingAttributeWithValueMap = new LinkedHashMap<>();

        source.getClassificationClasses().stream()
                .map(ClassificationClassModel::getDefiningClassificationAttributeAssignments)
                .filter(CollectionUtils::isNotEmpty)
                .forEach(definingAttributes -> addFeaturesToDefiningAttributesMap(features, definingAttributeWithValueMap, definingAttributes));

        target.setDefiningAttributes(definingAttributeWithValueMap);
    }

    private void addFeaturesToDefiningAttributesMap(List<Feature> features, Map<String, FeatureData> definingAttributeWithValueMap, List<ClassAttributeAssignmentModel> definingAttributes) {
        for (final ClassAttributeAssignmentModel cs : definingAttributes) {
            final ClassificationAttributeModel classificationAttribute = cs.getClassificationAttribute();
            final String attributeCode = classificationAttribute.getCode();

            features.stream()
                    .filter(feature -> feature.getClassAttributeAssignment().getClassificationAttribute().getCode().equals(attributeCode))
                    .findFirst()
                    .ifPresent(currentFeature -> definingAttributeWithValueMap.put(attributeCode, this.featureConverter.convert(currentFeature)));

        }
    }
}