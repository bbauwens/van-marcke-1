package com.vanmarcke.facades.storesession.impl;

import com.vanmarcke.facades.search.VMKSearchFacade;
import com.vanmarcke.facades.site.data.BaseSiteData;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import com.vanmarcke.services.cms.VMKCmsSiteService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storesession.VMKStoreSessionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.storesession.impl.DefaultStoreSessionFacade;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;
import java.util.List;

import static de.hybris.platform.commerceservices.enums.SiteChannel.B2B;

/**
 * Custom implementation for DefaultStoreSessionFacade
 */
public class VMKStoreSessionFacadeImpl extends DefaultStoreSessionFacade implements VMKStoreSessionFacade {

    private final VMKCmsSiteService cmsSiteService;
    private final Converter<BaseSiteModel, BaseSiteData> siteConverter;
    private final VMKSearchFacade<PointOfServiceData> pointOfServiceSearchFacade;
    private final VMKPointOfServiceService pointOfServiceService;
    private final Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;

    /**
     * Constructor for VMKStoreSessionFacadeImpl
     * @param cmsSiteService             the cmsSiteService
     * @param siteConverter              the siteConverter
     * @param pointOfServiceSearchFacade the pointOfServiceSearchFacade
     * @param pointOfServiceService      the pointOfServiceService
     * @param pointOfServiceConverter    the pointOfServiceConverter
     */
    public VMKStoreSessionFacadeImpl(VMKCmsSiteService cmsSiteService,
                                     Converter<BaseSiteModel, BaseSiteData> siteConverter,
                                     VMKSearchFacade<PointOfServiceData> pointOfServiceSearchFacade,
                                     VMKPointOfServiceService pointOfServiceService,
                                     Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter) {
        this.cmsSiteService = cmsSiteService;
        this.siteConverter = siteConverter;
        this.pointOfServiceSearchFacade = pointOfServiceSearchFacade;
        this.pointOfServiceService = pointOfServiceService;
        this.pointOfServiceConverter = pointOfServiceConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<BaseSiteData> getAllSites() {
        return siteConverter.convertAll(cmsSiteService.getSupportedSites(B2B));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BaseSiteData getCurrentSite() {
        return siteConverter.convert(cmsSiteService.getCurrentSite());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCurrentSite(String uid) {
        ((VMKStoreSessionService) getStoreSessionService()).setCurrentSite(uid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PointOfServiceData> getAllStores() {
        return pointOfServiceSearchFacade.search();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceData getCurrentStore() {
        PointOfServiceModel currentPointOfService = pointOfServiceService.getCurrentPointOfService();
        return currentPointOfService != null ? pointOfServiceConverter.convert(currentPointOfService) : null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCurrentStore(String uid) {
        ((VMKStoreSessionService) getStoreSessionService()).setCurrentStore(uid);
    }
}