package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.converters.populator.DeliveryOrderEntryGroupPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.DeliveryOrderEntryGroupData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.util.ArrayList;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * Populates {@link AbstractOrderData} with {@link DeliveryOrderEntryGroupData}.
 *
 * @author Taki Korovessis
 * @since 14-12-2019
 */
public class VMKDeliveryOrderEntryGroupPopulator extends DeliveryOrderEntryGroupPopulator {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createUpdateShipGroupData(AbstractOrderEntryModel entryModel, AbstractOrderData target) {
        AbstractOrderModel orderModel = entryModel.getOrder();
        if (orderModel.getDeliveryMode() instanceof ZoneDeliveryModeModel) {
            DeliveryOrderEntryGroupData groupData = null;
            if (isNotEmpty(target.getDeliveryOrderGroups())) {
                groupData = target.getDeliveryOrderGroups().iterator().next();
            }
            if (groupData == null) {
                groupData = new DeliveryOrderEntryGroupData();
                groupData.setEntries(new ArrayList<>());
                target.getDeliveryOrderGroups().add(groupData);
            }

            updateGroupTotalPriceWithTax(entryModel, groupData);
            groupData.getEntries().add(getOrderEntryData(target, entryModel.getEntryNumber()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long sumDeliveryItemsQuantity(AbstractOrderModel source) {
        long sum = 0;
        if (source.getDeliveryMode() instanceof ZoneDeliveryModeModel) {
            for (AbstractOrderEntryModel entryModel : source.getEntries()) {
                sum += entryModel.getQuantity().longValue();
            }
        }
        return sum;
    }
}