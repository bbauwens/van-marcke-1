package com.vanmarcke.facades.product.converters.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

/**
 * This populator populates the delivery/pickup date for the cart entry.
 *
 * @author Niels Raemaekers
 * @since 19-04-2021
 */
public class VMKOrderEntryDeliveryPickupDatePopulator implements Populator<AbstractOrderEntryModel, OrderEntryData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(AbstractOrderEntryModel source, OrderEntryData target) {
        target.setDeliveryPickupDate(source.getDeliveryDate());
    }
}
