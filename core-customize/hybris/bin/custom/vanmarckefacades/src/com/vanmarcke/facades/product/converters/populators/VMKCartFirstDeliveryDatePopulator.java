package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingEntryInfoResponseData;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.delivery.VMKFirstDateAvailabilityService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This populator determines and populates the first and last possible delivery dates for carts.
 *
 * @author Niels Raemaekers
 * @since 19-03-2021
 */
public class VMKCartFirstDeliveryDatePopulator implements Populator<CartModel, CartData> {

    private final VMKDeliveryInfoService deliveryInfoService;
    private final UserService userService;
    private final VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService;

    /**
     * Provides an instance of the VMKCartFirstDeliveryDatePopulator.
     *
     * @param deliveryInfoService             the delivery info service
     * @param userService                     the user service
     * @param vmkFirstDateAvailabilityService the first available day service
     */
    public VMKCartFirstDeliveryDatePopulator(VMKDeliveryInfoService deliveryInfoService, UserService userService, VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService) {
        this.deliveryInfoService = deliveryInfoService;
        this.userService = userService;
        this.vmkFirstDateAvailabilityService = vmkFirstDateAvailabilityService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CartModel cartModel, CartData cartData) {
        final Map<String, Date> deliveryDates = retrieveDeliveryDates(cartModel);

        if (MapUtils.isNotEmpty(deliveryDates)) {
            for (OrderEntryData entry : cartData.getEntries()) {
                Date firstPossibleDate = deliveryDates.get(entry.getProduct().getCode());
                entry.setFirstPossibleDate(vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(firstPossibleDate));
            }
        }
    }

    /**
     * Retrieve the deliveryDate for the given cart
     *
     * @param cart the given cart
     * @return Map with the products and dates
     */
    protected Map<String, Date> retrieveDeliveryDates(CartModel cart) {
        //Call fails for anon because we require a delivery address in the request
        if (!userService.isAnonymousUser(userService.getCurrentUser())) {
            return deliveryInfoService.getShippingDateInformation(cart)
                    .map(ShippingDateInfoResponseData::getResults)
                    .stream()
                    .flatMap(List::stream)
                    .filter(shippingEntry -> ObjectUtils.allNotNull(shippingEntry.getProductNumber(), shippingEntry.getDeliveryDate()))
                    .collect(Collectors.toMap(ShippingEntryInfoResponseData::getProductNumber, ShippingEntryInfoResponseData::getDeliveryDate));
        }
        return Collections.emptyMap();
    }
}

