package com.vanmarcke.facades.search.converters;

import de.hybris.platform.converters.impl.AbstractConverter;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

/**
 * Converting converter that uses a list of configured converters to convert the target during conversion.
 * Class not an abstract, but it allows to declare it as an abstract bean in Spring, otherwise we'd get
 * BeanInstantiationException.
 */
public class AbstractConvertingConverter<SOURCE, TARGET> extends AbstractConverter<SOURCE, TARGET> implements
        ConverterList<SOURCE, TARGET> {

    private List<Converter<SOURCE, TARGET>> converters;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Converter<SOURCE, TARGET>> getConverters() {
        return this.converters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConverters(List<Converter<SOURCE, TARGET>> converters) {
        this.converters = converters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SOURCE source, TARGET target) {
        convertSourceToTarget(source, target);
    }

    /**
     * Goes over the list with converters and performs convert for each entry
     * to convert the source to the target
     *
     * @param source the source
     * @param target the target
     */
    protected void convertSourceToTarget(SOURCE source, TARGET target) {

        List<Converter<SOURCE, TARGET>> list = getConverters();

        if (list == null) {
            return;
        }

        for (Converter<SOURCE, TARGET> converter : list) {
            if (converter != null) {
                converter.convert(source, target);
            }
        }
    }
}
