package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.util.TaxValue;
import org.apache.commons.collections4.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.summingDouble;

public class VMKAbstractOrderTaxesPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T> {

    public static final String KEY_VAT = "VAT";
    public static final String KEY_OTHER = "OTHER";
    private PriceDataFactory priceDataFactory;

    public VMKAbstractOrderTaxesPopulator(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    @Override
    public void populate(final S source, final T target) {
        if (CollectionUtils.isNotEmpty(source.getEntries())) {
            Map<String, PriceData> taxesByType = new HashMap<>();
            List<TaxRowModel> filteredTaxes = new ArrayList<>();
            CMSSiteModel currentSite = (CMSSiteModel) source.getSite();
            String currencyIsoCode = source.getCurrency().getIsocode();

            //get all taxRows for each entry, but filter out on "ug" based on currentSite
            source.getEntries().forEach(entry -> {
                List<TaxRowModel> taxes = entry.getProduct().getEurope1Taxes()
                        .stream()
                        .filter(tax -> tax != null && currentSite.getCommerceGroup() != null && tax.getUg().equals(currentSite.getCommerceGroup().getUserTaxGroup()))
                        .collect(Collectors.toList());
                filteredTaxes.addAll(taxes);
            });

            calculateVAT(taxesByType, source, currencyIsoCode);

            calculateProductTaxes(taxesByType, source, filteredTaxes, currencyIsoCode);
            target.setTaxes(taxesByType);
        }
    }

    /**
     * find VAT tax for given order
     * @param taxesByType map of taxes to add to
     * @param source the source order
     * @param currencyIsoCode the currency iso code
     */
    protected void calculateVAT(Map<String, PriceData> taxesByType, AbstractOrderModel source, String currencyIsoCode) {
        Collection<TaxValue> orderTaxValues = source.getTotalTaxValues();
        TaxValue vatTaxValue = orderTaxValues.stream()
                .filter(o -> o.getCode().equals("VAT"))
                .findFirst()
                .orElse(null);


        if (vatTaxValue != null) {
            double totalVATValue = orderTaxValues.stream()
                    .filter(o -> o.getCode().equals("VAT"))
                    .map(TaxValue::getAppliedValue)
                    .collect(summingDouble(e -> e));

            taxesByType.put(KEY_VAT, priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(totalVATValue), currencyIsoCode));
        } else {
            taxesByType.put(KEY_VAT, priceDataFactory.create(PriceDataType.BUY, BigDecimal.ZERO, currencyIsoCode));
        }
    }

    /**
     * Calculate the other product taxes such as BEBAT or RECUPEL
     *
     * @param taxesByType   map containing the taxes by type
     * @param source        the abstract order
     * @param filteredTaxes the filtered taxes containing only entries for current site
     */
    protected void calculateProductTaxes(Map<String, PriceData> taxesByType, AbstractOrderModel source, List<TaxRowModel> filteredTaxes, String currencyIsoCode) {
        List<String> productTaxes = filteredTaxes.stream()
                .filter(e -> e.getProduct() != null)
                .map(TaxRowModel::getTax)
                .map(TaxModel::getCode)
                .collect(Collectors.toList());
        Collection<TaxValue> orderTaxValues = source.getTotalTaxValues();
        double totalProductTaxes = orderTaxValues.stream()
                .filter(e -> productTaxes.contains(e.getCode()))
                .map(TaxValue::getAppliedValue)
                .collect(summingDouble(e -> e));

        taxesByType.put(KEY_OTHER, priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(totalProductTaxes), currencyIsoCode));
    }
}