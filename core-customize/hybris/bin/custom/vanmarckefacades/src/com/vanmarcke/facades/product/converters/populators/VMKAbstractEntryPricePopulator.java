package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.sitechannel.AbstractSiteChannelAwarePopulator;
import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import static org.apache.commons.lang3.BooleanUtils.isNotTrue;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

/**
 * TODO: there is not point to this abstraction, consider refactor.
 */
public abstract class VMKAbstractEntryPricePopulator<S extends ItemModel, T> extends AbstractSiteChannelAwarePopulator<S, T> {

    private PriceDataFactory priceDataFactory;
    private VMKNetPriceLookupStrategy blueNetPriceLookupStrategy;
    private CommercePriceService commercePriceService;

    protected VMKAbstractEntryPricePopulator(PriceDataFactory priceDataFactory, VMKNetPriceLookupStrategy blueNetPriceLookupStrategy, CommercePriceService commercePriceService) {
        this.priceDataFactory = priceDataFactory;
        this.blueNetPriceLookupStrategy = blueNetPriceLookupStrategy;
        this.commercePriceService = commercePriceService;
    }

    protected void populate(final S source, final T target, final ProductModel product, final Boolean checkoutMode, final CurrencyModel currency, final Boolean showNetPrice, final boolean includeNetPrice) {
        // in the abstract order case, prices are already pre populated with default/correct values
        prePopulatePricing(source, target);

        if (product instanceof VariantProductModel && isNotTrue(checkoutMode)) {
            populateNetPrice(target, product, currency);

            if (isTrue(showNetPrice)) {
                populateBasePrice(target, product, currency);

                if (!includeNetPrice) {
                    populateTotalFromBasePrice(source, target, currency);
                }
            }
            if (includeNetPrice) {
                populateTotalFromNetPrice(source, target, currency);
            }
        }
    }

    protected abstract Boolean shouldPopulateNetPrice(S source);

    protected abstract void prePopulatePricing(final S source, final T target);

    protected abstract void populateNetPrice(final T target, final ProductModel product, final CurrencyModel currency);

    protected abstract void populateBasePrice(final T target, final ProductModel product, final CurrencyModel currency);

    protected abstract void populateTotalFromBasePrice(final S source, final T target, final CurrencyModel currency);

    protected abstract void populateTotalFromNetPrice(final S source, final T target, final CurrencyModel currency);

    protected PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    protected VMKNetPriceLookupStrategy getBlueNetPriceLookupStrategy() {
        return blueNetPriceLookupStrategy;
    }

    protected CommercePriceService getCommercePriceService() {
        return commercePriceService;
    }
}
