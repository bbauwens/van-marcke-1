package com.vanmarcke.facades.principal.customer.populators;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.site.BaseSiteService;

import static org.apache.commons.lang3.BooleanUtils.isTrue;

/**
 * Blue Customer Populator
 */
public class VMKCustomerShowNetPricePopulator implements Populator<CustomerModel, CustomerData> {

    private final BaseSiteService baseSiteService;

    public VMKCustomerShowNetPricePopulator (BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CustomerModel source, CustomerData target) {
        target.setShowNetPrice(isTrue(source.getShowNetPrice()) || baseSiteService.getCurrentBaseSite().getChannel() == SiteChannel.DIY);
    }
}
