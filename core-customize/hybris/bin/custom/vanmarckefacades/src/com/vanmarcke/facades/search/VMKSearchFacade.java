package com.vanmarcke.facades.search;

import java.util.List;

/**
 * Custom interface to search a Solr Index
 */
public interface VMKSearchFacade<T> {

    /**
     * Find list of objects in Solr
     *
     * @return List of Search Results of type T
     */
    List<T> search();

    /**
     * Find list of objects in Solr for the term
     *
     * @param term Term to search for
     * @return List of Search Results of type T
     */
    List<T> textSearch(String term);

}