package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.facades.sitechannel.AbstractSiteChannelAwarePopulator;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.variants.model.VariantProductModel;

/**
 * Populator to populate Features for VariantOptionData
 */
public class VMKProductVariantFeaturesPopulator extends AbstractSiteChannelAwarePopulator<VariantProductModel, VariantOptionData> {

    @Override
    protected void doPopulate(final VariantProductModel source, final VariantOptionData target) {
        //Features will be populated here as soon as we have more info
        //Create map with FeatureCode as key and FeatureValue for Variant as value
    }
}
