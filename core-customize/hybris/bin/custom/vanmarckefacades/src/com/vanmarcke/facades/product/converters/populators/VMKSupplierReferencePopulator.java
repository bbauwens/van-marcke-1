package com.vanmarcke.facades.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

/**
 * Populates supplier reference for product {@link ProductData}.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 07-02-2020
 */
public class VMKSupplierReferencePopulator implements Populator<ProductModel, ProductData> {

    private static final String COMMODITY = "C";
    private static final String PRIVATE = "V";

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(ProductModel source, ProductData target) {
        String selClassification = source.getSelClassification();
        if (!PRIVATE.equals(selClassification) && !COMMODITY.equals(selClassification)) {
            target.setSupplierReference(source.getVendorItemNumber_current());
        }
    }
}
