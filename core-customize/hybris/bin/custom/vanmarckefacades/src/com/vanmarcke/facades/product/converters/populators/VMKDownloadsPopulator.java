package com.vanmarcke.facades.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import org.apache.commons.io.FilenameUtils;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

/**
 * Populates documents on {@link ImageData} instances.
 *
 * @author Paesmans, Taki Korovessis, Tom van den Berg
 * @since 08-08-2018
 */
public class VMKDownloadsPopulator implements Populator<MediaModel, ImageData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final MediaModel source, final ImageData target) {
        target.setUrl(source.getURL());
        target.setName(FilenameUtils.removeExtension(defaultIfBlank(source.getName(), source.getRealFileName())));
    }
}
