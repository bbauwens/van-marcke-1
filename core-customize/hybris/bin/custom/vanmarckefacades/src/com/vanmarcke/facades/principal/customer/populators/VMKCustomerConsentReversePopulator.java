package com.vanmarcke.facades.principal.customer.populators;

import com.vanmarcke.core.model.VMKConsentModel;
import com.vanmarcke.facades.data.VMKConsentData;
import de.hybris.platform.converters.Populator;

/**
 * This populator populates {@link VMKConsentData} to {@link VMKConsentModel}.
 *
 * @author Tom van den Berg
 * @since 05-06-2020
 */
public class VMKCustomerConsentReversePopulator implements Populator<VMKConsentData, VMKConsentModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(VMKConsentData source, VMKConsentModel target) {
        target.setConsentType(source.getConsentType());
        target.setOptType(source.getOptType());
    }
}
