package com.vanmarcke.facades.search.converters.populators;

import com.vanmarcke.facades.analytics.data.AnalyticsData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.util.Assert;

/**
 * Custom VMK SearchResultProduct Populator
 */
public class VMKSearchResultAnalyticsPopulator implements Populator<SearchResultValueData, ProductData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SearchResultValueData source, ProductData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setAnalyticsData(populateAnalyticsData(source));
    }

    /**
     * Creates an {@link AnalyticsData} instance from the given {@link SearchResultValueData}.
     *
     * @param source the search result value data
     * @return analytics data
     */
    protected AnalyticsData populateAnalyticsData(SearchResultValueData source) {
        AnalyticsData analyticsData = new AnalyticsData();
        analyticsData.setCategory(getValue(source, "nearestCategory"));
        analyticsData.setBrand(getValue(source, "brandName"));
        return analyticsData;
    }

    /**
     * Retrieves the given property from the given {@link SearchResultValueData}.
     *
     * @param source       the search result value data
     * @param propertyName the property name
     * @param <T>          the type
     * @return the property
     */
    private <T> T getValue(final SearchResultValueData source, final String propertyName) {
        if (source.getValues() == null) {
            return null;
        }
        // DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
        return (T) source.getValues().get(propertyName);
    }
}
