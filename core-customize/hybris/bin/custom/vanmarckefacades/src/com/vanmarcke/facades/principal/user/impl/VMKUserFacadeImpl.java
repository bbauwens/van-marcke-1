package com.vanmarcke.facades.principal.user.impl;

import com.vanmarcke.facades.principal.user.VMKUserFacade;
import com.vanmarcke.services.b2b.VMKSSOUserService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.impl.DefaultUserFacade;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Custom implementation for UserFacade
 */
public class VMKUserFacadeImpl extends DefaultUserFacade implements VMKUserFacade {

    private static final Logger LOG = LoggerFactory.getLogger(VMKUserFacadeImpl.class);

    private final BaseSiteService baseSiteService;
    private final CommerceCartService commerceCartService;
    private final VMKPointOfServiceService pointOfServiceService;
    private final VMKSSOUserService vmkssoUserService;

    /**
     * Constructor for {@link VMKUserFacadeImpl}
     *
     * @param baseSiteService       the {@link BaseSiteService}
     * @param commerceCartService   the {@link CommerceCartService}
     * @param pointOfServiceService the {@link VMKPointOfServiceService}
     * @param vmkssoUserService     the {@link VMKSSOUserService}
     */
    public VMKUserFacadeImpl(BaseSiteService baseSiteService, CommerceCartService commerceCartService, VMKPointOfServiceService pointOfServiceService, VMKSSOUserService vmkssoUserService) {
        this.baseSiteService = baseSiteService;
        this.commerceCartService = commerceCartService;
        this.pointOfServiceService = pointOfServiceService;
        this.vmkssoUserService = vmkssoUserService;
    }

    @Override
    public void syncSessionSite() {
        final UserModel user = getUserService().getCurrentUser();
        user.setSessionSite(baseSiteService.getCurrentBaseSite());
        getModelService().save(user);

    }

    @Override
    public void syncSessionStore() {
        final UserModel user = getUserService().getCurrentUser();
        user.setSessionStore(pointOfServiceService.getCurrentPointOfService());
        getModelService().save(user);
    }

    @Override
    public boolean saveNetPricePreference(boolean showNetPrice) {
        final UserModel user = getUserService().getCurrentUser();
        final Boolean currentShowNetPrice = user.getShowNetPrice();

        if (!getUserService().isAnonymousUser(user)) {
            // update show net price indicator
            user.setShowNetPrice(showNetPrice);
            getModelService().save(user);
            getModelService().refresh(user);

            // trigger recalculation if needed
            if ((currentShowNetPrice == null || !currentShowNetPrice.equals(showNetPrice)) && getCartService().hasSessionCart()) {
                final CartModel sessionCart = getCartService().getSessionCart();
                try {
                    final CommerceCartParameter parameter = new CommerceCartParameter();
                    parameter.setEnableHooks(true);
                    parameter.setCart(sessionCart);
                    commerceCartService.recalculateCart(parameter);
                } catch (final CalculationException e) {
                    LOG.error("Failed to recalculate order [" + sessionCart.getCode() + "]", e);
                }
            }
        }
        return user.getShowNetPrice();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceModel getNearestStoreForB2BCustomer() {
        return getNearestStoreForB2BCustomer(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceModel getNearestStoreForB2BCustomer(BaseStoreModel baseStore) {
        final B2BCustomerModel user = (B2BCustomerModel) getUserService().getCurrentUser();
        AddressModel billingAddress = user.getDefaultB2BUnit().getBillingAddress();
        if (Objects.nonNull(billingAddress)) {
            if (Objects.nonNull(baseStore)) {
                return vmkssoUserService.getNearestStore(user, baseStore);
            } else {
                return vmkssoUserService.getNearestStore(user);
            }
        }
        return null;
    }
}
