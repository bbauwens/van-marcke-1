package com.vanmarcke.facades.cms.converters.populator;

import com.vanmarcke.core.model.ConsentParagraphComponentModel;
import com.vanmarcke.facades.cms.ConsentParagraphData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang3.BooleanUtils;

/**
 * The {@link VMKConsentParagraphPopulator} class is used to populate instances of the {@link ConsentParagraphData}
 * with the data of the {@link ConsentParagraphComponentModel} class.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
public class VMKConsentParagraphPopulator implements Populator<ConsentParagraphComponentModel, ConsentParagraphData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(ConsentParagraphComponentModel source, ConsentParagraphData target) throws ConversionException {
        target.setConsentType(source.getConsentType());
        target.setEditable(BooleanUtils.toBoolean(source.getEditable()));
        target.setTitle(source.getTitle());
        target.setContent(source.getContent());
    }
}
