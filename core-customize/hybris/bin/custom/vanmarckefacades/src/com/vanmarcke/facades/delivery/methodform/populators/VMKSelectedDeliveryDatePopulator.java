package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;

import java.text.SimpleDateFormat;

/**
 * Provides the {@link DeliveryMethodForm} with the previously selected delivery date.
 *
 * @author Tom van den Berg
 * @since 30-01-2020
 */
public class VMKSelectedDeliveryDatePopulator implements Populator<CartModel, DeliveryMethodForm> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CartModel source, DeliveryMethodForm target) {
        if (source.getDeliveryDate() != null) {
            String date = new SimpleDateFormat("yyyy-MM-dd").format(source.getDeliveryDate());
            if (source.getDeliveryMode() == null || source.getDeliveryMode().getCode().endsWith("standard")) {
                target.setSelectedShippingDate(date);
            } else if (source.getDeliveryMode().getCode().endsWith("tec")) {
                target.setSelectedPickupDate(date);
            }
        }
    }
}
