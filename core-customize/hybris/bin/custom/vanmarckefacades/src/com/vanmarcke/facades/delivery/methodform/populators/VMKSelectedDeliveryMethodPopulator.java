package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.PICKUP;
import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.SHIPPING;

/**
 * Populates the previously selected delivery method on the {@link DeliveryMethodForm}.
 *
 * @author Tom van den Berg
 * @since 7-01-2020
 */
public class VMKSelectedDeliveryMethodPopulator implements Populator<CartModel, DeliveryMethodForm> {

    private final VMKStoreSessionFacade storeSessionFacade;

    /**
     * Provides an instance of the VMKSelectedDeliveryMethodPopulator.
     *
     * @param storeSessionFacade the store session facade
     */
    public VMKSelectedDeliveryMethodPopulator(VMKStoreSessionFacade storeSessionFacade) {
        this.storeSessionFacade = storeSessionFacade;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CartModel source, DeliveryMethodForm target) {
        if (source.getDeliveryMode() instanceof ZoneDeliveryModeModel) {
            target.setDeliveryMethod(SHIPPING);
        } else {
            target.setDeliveryMethod(PICKUP);
        }
    }
}
