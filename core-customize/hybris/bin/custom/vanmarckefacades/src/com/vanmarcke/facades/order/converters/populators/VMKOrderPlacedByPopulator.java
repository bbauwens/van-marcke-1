package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

/**
 * Populates the placed by information on the {@link OrderData}.
 * <p>
 * By default the {@link UserModel#getUid()} is populated as {@link OrderData#getPlacedBy()}.
 *
 * @param <S> the generic type {@link OrderModel}
 * @param <T> the generic type {@link OrderData}
 */
public class VMKOrderPlacedByPopulator<S extends OrderModel, T extends OrderData> implements Populator<S, T> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final S source, final T target) {
        final UserModel placedBy = source.getPlacedBy();
        if (placedBy != null) {
            target.setPlacedBy(defaultIfBlank(placedBy.getName(), placedBy.getUid()));
        }
    }
}