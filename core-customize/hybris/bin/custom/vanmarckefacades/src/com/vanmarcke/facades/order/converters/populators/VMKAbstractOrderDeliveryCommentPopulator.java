package com.vanmarcke.facades.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Populator that adds a delivery comment to the abstract order data
 */
public class VMKAbstractOrderDeliveryCommentPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T> {

    @Override
    public void populate(final S source, final T target) throws ConversionException {
        target.setDeliveryComment(source.getDeliveryComment());
    }
}
