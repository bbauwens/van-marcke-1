package com.vanmarcke.facades.paymentmodes.converters.populators;

import com.vanmarcke.facades.payment.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Populator that populates the PaymentMode Data object
 */
public class VMKPaymentModePopulator implements Populator<PaymentModeModel, PaymentModeData> {

    private Converter<MediaModel, ImageData> imageConverter;

    /**
     * Provides an instance of the VMKPaymentModePopulator.
     *
     * @param imageConverter the image converter
     */
    public VMKPaymentModePopulator(final Converter<MediaModel, ImageData> imageConverter) {
        this.imageConverter = imageConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final PaymentModeModel source, final PaymentModeData target) throws ConversionException {
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setDescription(source.getDescription());
        if (source.getThumbnail() != null) {
            target.setThumbnail(this.imageConverter.convert(source.getThumbnail()));
        }
    }
}