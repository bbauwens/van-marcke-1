package com.vanmarcke.facades.search.converters.populators;

import de.hybris.platform.commercefacades.search.converters.populator.FacetPopulator;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.converters.Populator;

/**
 * Custom implementation of {@link FacetPopulator}
 *
 * @author Niels Raemaekers
 * @since 18-06-2021
 */
public class VMKFacetPopulator<QUERY, STATE> extends FacetPopulator<QUERY, STATE> implements Populator<FacetData<QUERY>, FacetData<STATE>> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final FacetData<QUERY> source, final FacetData<STATE> target) {
        super.populate(source, target);

        target.setUnitSymbol(source.getUnitSymbol());
    }
}
