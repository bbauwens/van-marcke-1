package com.vanmarcke.facades.search.builder;

import com.vanmarcke.facades.search.data.VariantSearchStateData;
import de.hybris.platform.commercefacades.search.data.SearchFilterQueryData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.FilterQueryOperator;

import java.util.List;
import java.util.Set;

/**
 * Builder for VariantSearchStateData class
 *
 * @author Cristian Stoica
 * @since 16-11-2021
 */
public class VariantSearchStateDataBuilder {

    VariantSearchStateData searchStateData;

    public VariantSearchStateDataBuilder() {
        searchStateData = new VariantSearchStateData();
        final SearchQueryData searchQueryData = new SearchQueryData();
        searchStateData.setQuery(searchQueryData);
    }

    public VariantSearchStateDataBuilder withCategoryCode(String categoryCode) {
        this.searchStateData.setCategoryCode(categoryCode);
        return this;
    }

    public VariantSearchStateDataBuilder withSearchQuery(String searchQuery) {
        this.searchStateData.getQuery().setValue(searchQuery);
        return this;
    }

    public VariantSearchStateDataBuilder withContext(SearchQueryContext searchQueryContext) {
        this.searchStateData.getQuery().setSearchQueryContext(searchQueryContext);
        return this;
    }

    public VariantSearchStateDataBuilder withFilterQuery(String key, String value, FilterQueryOperator operator) {
        final SearchFilterQueryData baseProductFilterQueryData = new SearchFilterQueryData();
        baseProductFilterQueryData.setKey(key);
        baseProductFilterQueryData.setOperator(operator);
        baseProductFilterQueryData.setValues(Set.of(value));

        List<SearchFilterQueryData> filterTerms = searchStateData.getQuery().getFilterQueries();
        if (filterTerms == null) {
            filterTerms = List.of(baseProductFilterQueryData);
            searchStateData.getQuery().setFilterQueries(filterTerms);
        } else {
            filterTerms.add(baseProductFilterQueryData);
        }
        return this;
    }

    public VariantSearchStateData build() {
        return searchStateData;
    }
}
