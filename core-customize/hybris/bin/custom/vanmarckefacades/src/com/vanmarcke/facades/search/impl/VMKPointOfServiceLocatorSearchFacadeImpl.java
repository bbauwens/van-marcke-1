package com.vanmarcke.facades.search.impl;

import com.vanmarcke.facades.search.AbstractVMKPointOfServiceSearchFacade;
import com.vanmarcke.facades.search.VMKPointOfServiceLocatorSearchFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

import static com.vanmarcke.facades.constants.VanmarckeFacadesConstants.SOLR.POINT_OF_SERVICE.INDEX_COORDINATE;
import static de.hybris.platform.solrfacetsearch.search.OrderField.SortOrder.ASCENDING;
import static java.util.Collections.emptyList;

public class VMKPointOfServiceLocatorSearchFacadeImpl extends AbstractVMKPointOfServiceSearchFacade implements VMKPointOfServiceLocatorSearchFacade {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKPointOfServiceLocatorSearchFacadeImpl.class);

    @Override
    public StoreFinderSearchPageData<PointOfServiceData> search(final String locationText, final GeoPoint geoPoint, final PageableData pageableData) {
        return search(locationText, geoPoint, pageableData.getPageSize(), pageableData.getCurrentPage(), pageableData.getSort(), Collections.emptyList());
    }

    @Override
    public StoreFinderSearchPageData<PointOfServiceData> search(final String locationText, final GeoPoint geoPoint, final int pageSize, final int offSet, final String sort) {
        return search(locationText, geoPoint, pageSize, offSet, sort, Collections.emptyList());
    }

    @Override
    public StoreFinderSearchPageData<PointOfServiceData> search(final String locationText, final GeoPoint geoPoint, final int pageSize, final int offSet, final String sort, final List<String> alternativeTECStores) {
        try {
            final SearchResult searchResult = getSearchService().search(createSearchQuery(locationText, geoPoint, pageSize, offSet, alternativeTECStores));
            return createStoreFinderSearchResult(searchResult, pageSize, offSet, sort);
        } catch (final Exception e) {
            LOGGER.error("Failed to search for point of services.", e);
        }

        return createEmptyResult();
    }

    protected SearchQuery createSearchQuery(final String locationText, final GeoPoint geoPoint, final int pageSize, final int offSet, final List<String> alternativeTECStores) throws FacetConfigServiceException {
        final SearchQuery searchQuery = getSearchService().createTextSearchQuery(locationText, getFacetSearchConfig(), getIndexedType());
        searchQuery.setPageSize(pageSize);
        searchQuery.setOffset(offSet);
        addSearchQueryParamsAndSortForGeoPoint(searchQuery, geoPoint, alternativeTECStores);
        return searchQuery;
    }

    protected void addSearchQueryParamsAndSortForGeoPoint(final SearchQuery searchQuery, final GeoPoint geoPoint, final List<String> alternativeTECStores) {
        if (CollectionUtils.isNotEmpty(alternativeTECStores)) {
            searchQuery.addFilterQuery("name", SearchQuery.Operator.OR, alternativeTECStores.toArray(new String[alternativeTECStores.size()]));
        }
        if (geoPoint != null) {
            searchQuery.addRawParam("fq", "{!geofilt}");
            searchQuery.addRawParam("spatial", "true");
            searchQuery.addRawParam("sfield", INDEX_COORDINATE);
            searchQuery.addRawParam("pt", String.format("%s,%s", geoPoint.getLatitude(), geoPoint.getLongitude()));
            searchQuery.addRawParam("d", String.valueOf(1000000));
            searchQuery.addRawParam("fl", "*,distance:geodist()");
            searchQuery.addSort("geodist()", ASCENDING);
        }
    }

    protected StoreFinderSearchPageData<PointOfServiceData> createStoreFinderSearchResult(final SearchResult searchResult, final int pageSize, final int offSet, final String sort) {
        final StoreFinderSearchPageData<PointOfServiceData> storeFinderSearchPageData = new StoreFinderSearchPageData<>();
        storeFinderSearchPageData.setResults(searchResult.getResultData(getConverterType()));

        final PaginationData paginationData = new PaginationData();

        paginationData.setPageSize(pageSize);
        paginationData.setCurrentPage(offSet);
        paginationData.setSort(sort);

        paginationData.setTotalNumberOfResults(searchResult.getNumberOfResults());
        paginationData.setNumberOfPages((int) searchResult.getNumberOfPages());
        storeFinderSearchPageData.setPagination(paginationData);

        return storeFinderSearchPageData;
    }

    protected StoreFinderSearchPageData<PointOfServiceData> createEmptyResult() {
        final StoreFinderSearchPageData<PointOfServiceData> storeFinderSearchPageData = new StoreFinderSearchPageData<>();
        storeFinderSearchPageData.setResults(emptyList());
        return storeFinderSearchPageData;
    }

}