package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.services.category.VMKCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.stream.Collectors;

/**
 * Custom Populator for Category
 */
public class VMKCategoryPopulator implements Populator<CategoryModel, CategoryData> {

    private static final String MEDIA_FORMAT = "THUMBNAIL";
    private static final String CATEGORY = "category";
    private static final String BRAND = "brand";

    private final Converter<CategoryModel, CategoryData> categoryConverter;
    private final Converter<MediaModel, ImageData> imageConverter;
    private final ProductService productService;
    private final VMKCategoryService categoryService;

    /**
     * Provides an instance of the BlueCategoryPopulator.
     *
     * @param categoryConverter the category converter
     * @param imageConverter    the image converter
     * @param productService    the product service
     * @param categoryService   the category service
     */
    public VMKCategoryPopulator(Converter<CategoryModel, CategoryData> categoryConverter, Converter<MediaModel, ImageData> imageConverter, ProductService productService, VMKCategoryService categoryService) {
        this.categoryConverter = categoryConverter;
        this.imageConverter = imageConverter;
        this.productService = productService;
        this.categoryService = categoryService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CategoryModel source, CategoryData target) {
        this.categoryConverter.convert(source, target);

        if (source instanceof BrandCategoryModel) {
            populateBrandLogo(source, target);
            target.setCategoryType(BRAND);
        } else {
            populateImages(source, target);
            target.setCategoryType(CATEGORY);
        }

        populateSubCategoryList(source, target);
    }

    /**
     * Populates the target with the list of sub categories.
     *
     * @param category the category model
     * @param target   the category data
     */
    private void populateSubCategoryList(CategoryModel category, CategoryData target) {
        target.setChildren(category.getCategories()
                .stream()
                .filter(productService::containsProductsForCategory)
                .map(e -> {
                    CategoryData d = new CategoryData();
                    this.populate(e, d);
                    return d;
                })
                .collect(Collectors.toList()));
    }

    /**
     * Populates the target with an image.
     *
     * @param source the category model
     * @param target the category data
     */
    private void populateImages(CategoryModel source, CategoryData target) {
        MediaModel image = source.getPicture();
        if (image != null) {
            target.setImage(imageConverter.convert(image));
        }
    }

    /**
     * Populates the target with a brand logo.
     *
     * @param source the category model
     * @param target the category data
     */
    private void populateBrandLogo(CategoryModel source, CategoryData target) {
        categoryService
                .getLocalizedBrandLogoForFormat((BrandCategoryModel) source, MEDIA_FORMAT)
                .ifPresent(mediaModel -> target.setImage(imageConverter.convert(mediaModel)));
    }
}