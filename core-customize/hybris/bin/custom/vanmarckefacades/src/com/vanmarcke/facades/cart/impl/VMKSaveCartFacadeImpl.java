package com.vanmarcke.facades.cart.impl;

import com.vanmarcke.facades.cart.VMKSaveCartFacade;
import com.vanmarcke.services.util.PdfUtils;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartParameterData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartResultData;
import de.hybris.platform.commercefacades.order.impl.DefaultSaveCartFacade;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.MediaType;

import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * Customer implementation for the {@link VMKSaveCartFacade}
 */
public class VMKSaveCartFacadeImpl extends DefaultSaveCartFacade implements VMKSaveCartFacade {

    private final MediaService mediaService;
    private final Converter<CartModel, CartData> cartSummaryConverter;

    private PdfUtils pdfUtils;

    /**
     * Provides an instance of the VMKSaveCartFacadeImpl
     *
     * @param mediaService         the media service
     * @param cartSummaryConverter the cart summary converter
     */
    public VMKSaveCartFacadeImpl(final MediaService mediaService, Converter<CartModel, CartData> cartSummaryConverter, final PdfUtils pdfUtils) {
        this.mediaService = mediaService;
        this.cartSummaryConverter = cartSummaryConverter;
        this.pdfUtils = pdfUtils;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream exportTechnicalDataSheetsForSavedCartId(final String cartId) {
        final CartModel savedCart = getCommerceCartService().getCartForCodeAndUser(cartId, getUserService().getCurrentUser());

        if (savedCart != null) {
            final List<InputStream> dataSheets = savedCart.getEntries()
                    .stream()
                    .map(AbstractOrderEntryModel::getProduct)
                    .map(ProductModel::getTech_data_sheet)
                    .filter(Objects::nonNull)
                    .filter(m -> (MediaType.APPLICATION_PDF_VALUE).equals(m.getMime()))
                    .map(this.mediaService::getStreamFromMedia)
                    .collect(Collectors.toList());

            if (CollectionUtils.isNotEmpty(dataSheets)) {
                return pdfUtils.mergePdfFiles(dataSheets);
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommerceSaveCartResultData getCartSummary(CommerceSaveCartParameterData inputParameters) throws CommerceSaveCartException {
        CartModel cartToRetrieve;

        if (StringUtils.isNotEmpty(inputParameters.getCartId())) {
            cartToRetrieve = getCommerceCartService().getCartForCodeAndUser(inputParameters.getCartId(), getUserService().getCurrentUser());
        } else {
            throw new CommerceSaveCartException("Cart code cannot be empty");
        }

        if (null == cartToRetrieve) {
            throw new CommerceSaveCartException("Cannot find a cart for code [" + inputParameters.getCartId() + "]");
        }

        final CommerceSaveCartResultData saveCartResultData = new CommerceSaveCartResultData();
        saveCartResultData.setSavedCartData(cartSummaryConverter.convert(cartToRetrieve));

        return saveCartResultData;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommerceSaveCartResultData getCartForCodeAndCurrentUser(final CommerceSaveCartParameterData inputParameters) throws CommerceSaveCartException {
        CartModel cartToRetrieve;

        if (StringUtils.isNotEmpty(inputParameters.getCartId())) {
            cartToRetrieve = getCommerceCartService().getCartForCodeAndUser(inputParameters.getCartId(), getUserService().getCurrentUser());
        } else {
            throw new CommerceSaveCartException("Cart code cannot be empty");
        }

        if (null == cartToRetrieve) {
            throw new CommerceSaveCartException("Cannot find a cart for code [" + inputParameters.getCartId() + "]");
        }

        recalculateCart(cartToRetrieve);

        final CommerceSaveCartResultData saveCartResultData = new CommerceSaveCartResultData();
        saveCartResultData.setSavedCartData(getCartConverter().convert(cartToRetrieve));

        return saveCartResultData;
    }

    protected void recalculateCart(final CartModel cart) throws CommerceSaveCartException {
        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setEnableHooks(true);
        parameter.setCart(cart);
        try {
            getCommerceCartService().recalculateCart(parameter);
        } catch (final CalculationException ex) {
            throw new CommerceSaveCartException(format("Cannot recalculate saved cart with code [%s]", cart.getCode()), ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchPageData<CartData> getSavedCartsForCurrentUser(PageableData pageableData, List<OrderStatus> orderStatus) {
        final SearchPageData<CartData> result = new SearchPageData<>();
        final SearchPageData<CartModel> savedCartModels = getCommerceSaveCartService().getSavedCartsForSiteAndUser(pageableData,
                getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser(), orderStatus);

        result.setPagination(savedCartModels.getPagination());
        result.setSorts(savedCartModels.getSorts());

        final List<CartData> savedCartDatas = cartSummaryConverter.convertAll(savedCartModels.getResults());

        result.setResults(savedCartDatas);
        return result;
    }
}