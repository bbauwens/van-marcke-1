package com.vanmarcke.facades.consent.impl;

import com.google.gson.Gson;
import com.vanmarcke.core.model.VMKConsentModel;
import com.vanmarcke.facades.consent.VMKConsentFacade;
import com.vanmarcke.facades.data.VMKConsentData;
import com.vanmarcke.services.consent.VMKConsentService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Provides implementations for the {@link VMKConsentFacade}.
 *
 * @author Tom van den Berg
 * @since 12-06-2020
 */
public class VMKConsentFacadeImpl implements VMKConsentFacade {

    private final VMKConsentService consentService;
    private final Converter<VMKConsentData, VMKConsentModel> consentReverseConverter;
    private final Converter<VMKConsentModel, VMKConsentData> consentConverter;

    /**
     * Provides an instance of the {@code VMKConsentFacadeImpl}.
     *
     * @param consentService          the consent service
     * @param consentReverseConverter the reverse consent converter
     * @param consentConverter        the consent converter
     */
    public VMKConsentFacadeImpl(VMKConsentService consentService,
                                Converter<VMKConsentData, VMKConsentModel> consentReverseConverter,
                                Converter<VMKConsentModel, VMKConsentData> consentConverter) {
        this.consentService = consentService;
        this.consentReverseConverter = consentReverseConverter;
        this.consentConverter = consentConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void process(List<VMKConsentData> consents, HttpServletRequest request) {
        consentService.processConsents(consentReverseConverter.convertAll(consents), request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getConsentsAsString() {
        List<VMKConsentData> consents = consentConverter.convertAll(consentService.getConsents());
        return new Gson().toJson(consents);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<VMKConsentData> getConsents() {
        return consentConverter.convertAll(consentService.getConsents());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getConsentTypes() {
        return consentService.getConsentTypes();
    }
}
