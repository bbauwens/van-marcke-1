package com.vanmarcke.facades.search;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;

import java.util.List;

/**
 * Paged {@link PointOfServiceData} Solr interface.
 */
public interface VMKPointOfServiceLocatorSearchFacade extends VMKSearchFacade<PointOfServiceData> {

    /**
     * Finds all {@link PointOfServiceData} for the given search criteria.
     *
     * @param locationText the location text
     * @param geoPoint     the geo point
     * @param pageableData the search query pagination
     * @return A paged result of {@link PointOfServiceData}
     */
    StoreFinderSearchPageData<PointOfServiceData> search(String locationText, GeoPoint geoPoint, PageableData pageableData);

    /**
     * Finds all {@link PointOfServiceData} for the given search criteria.
     *
     * @param locationText the location text
     * @param geoPoint     the geo point
     * @param pageSize     the search query page size
     * @param offSet       the search query offset
     * @param sort         the search query sort
     * @return A paged result of {@link PointOfServiceData}
     */
    StoreFinderSearchPageData<PointOfServiceData> search(String locationText, GeoPoint geoPoint, int pageSize, int offSet, String sort);

    /**
     * Finds all {@link PointOfServiceData} for the given search criteria.
     *
     * @param locationText         the location text
     * @param geoPoint             the geo point
     * @param pageSize             the search query page size
     * @param offSet               the search query offset
     * @param sort                 the search query sort
     * @param alternativeTECStores the alternative TEC Stores to search with
     * @return A paged result of {@link PointOfServiceData}
     */
    StoreFinderSearchPageData<PointOfServiceData> search(String locationText, GeoPoint geoPoint, int pageSize, int offSet, String sort, List<String> alternativeTECStores);
}