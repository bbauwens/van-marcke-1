package com.vanmarcke.facades.paymentmodes;

import com.vanmarcke.facades.payment.data.PaymentModeData;

import java.util.List;

/**
 * Interface for Payment Modes
 */
public interface VMKPaymentModeFacade {

    /**
     * Set the payment modes which are available for the given baseStore on the model
     *
     * @return List with the paymentModeData
     */
    List<PaymentModeData> getSupportedPaymentModes();
}
