package com.vanmarcke.facades.delivery;

import com.vanmarcke.facades.data.PickupInfoData;
import com.vanmarcke.facades.data.ShippingInfoData;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Retrieves and converts delivery information.
 */
public interface VMKDeliveryInformationFacade {

    /**
     * Converts the given cart into a {@link ShippingInfoData} instance.
     *
     * @param cart the cart
     * @return the shipping info
     */
    ShippingInfoData getShippingInfo(CartModel cart);

    /**
     * Converts the given cart into a {@link PickupInfoData} instance.
     *
     * @param cart         the cart
     * @param selectedDate the selected date
     * @return the pickup info
     */
    PickupInfoData getPickupInfo(CartModel cart, String selectedDate);

    /**
     * Converts the given cart into a {@link DeliveryMethodForm} instance.
     *
     * @param cart the cart
     * @return the delivery method form
     */
    DeliveryMethodForm getDeliveryMethodForm(CartModel cart);
}
