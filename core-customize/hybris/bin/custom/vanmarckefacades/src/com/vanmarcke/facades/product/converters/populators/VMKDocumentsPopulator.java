package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.facades.data.DownloadsData;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static java.util.Collections.singletonList;
import static java.util.Objects.isNull;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * Populates {@link ProductData} instances with {@link DownloadsData}.
 */
public class VMKDocumentsPopulator extends AbstractProductPopulator<ProductModel, ProductData> {

    private final Converter<MediaModel, ImageData> downloadsConverter;
    private final I18NService i18NService;

    /**
     * Provides an instance of the {@code VMKDocumentsPopulator}.
     *
     * @param downloadsConverter the downloads converter
     * @param i18NService        the i18n service
     */
    public VMKDocumentsPopulator(Converter<MediaModel, ImageData> downloadsConverter,
                                 I18NService i18NService) {
        this.downloadsConverter = downloadsConverter;
        this.i18NService = i18NService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(ProductModel source, ProductData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        if (source instanceof VariantProductModel) {
            populateManuals(source, target);
            populateCertificates(source, target);
            populateDOP(source, target);
            populateNormalisation(source, target);
            populateTechDataSheet(source, target);
            populateSafetyDataSheet(source, target);
            populateProductSpecificationSheet(source, target);
            populateWarranty(source, target);
            populateSparePartsList(source, target);
            populateEcoDataSheet(source, target);
            populateBrandCatalogs(source, target);

            target.setDocumentsVisible(hasDocuments(target));
        }
    }

    /**
     * Determines whether any document has been populated on the given product data instance.
     *
     * @param target the product data
     * @return true if any document has been populated
     */
    private Boolean hasDocuments(ProductData target) {
        return isNotEmpty(target.getManuals())
                || isNotEmpty(target.getCertificates())
                || !isNull(target.getDOP())
                || !isNull(target.getNormalisation())
                || !isNull(target.getTechnicalDataSheet())
                || !isNull(target.getSafetyDataSheet())
                || !isNull(target.getProductSpecificationSheet())
                || !isNull(target.getWarranty())
                || !isNull(target.getSparePartsList())
                || !isNull(target.getEcoDataSheet());
    }

    /**
     * Populates the eco data sheet
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateEcoDataSheet(ProductModel source, ProductData target) {
        MediaModel media = getLocalizedMedia(source, ProductModel.ECO_DATA_SHEET);
        if (media != null) {
            target.setEcoDataSheet(downloadsConverter.convert(media));
        }
    }


    /**
     * Populates the spare parts list
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateSparePartsList(ProductModel source, ProductData target) {
        MediaModel media = getLocalizedMedia(source, ProductModel.SPARE_PARTS_LIST);
        if (media != null) {
            target.setSparePartsList(downloadsConverter.convert(media));
        }
    }

    /**
     * Populates the warranty
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateWarranty(ProductModel source, ProductData target) {
        MediaModel media = getLocalizedMedia(source, ProductModel.WARRANTY);
        if (media != null) {
            target.setWarranty(downloadsConverter.convert(media));
        }
    }

    /**
     * Populates the product specification sheet
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateProductSpecificationSheet(ProductModel source, ProductData target) {
        MediaModel media = getLocalizedMedia(source, ProductModel.PRODUCT_SPECIFICATION_SHEET);
        if (media != null) {
            target.setProductSpecificationSheet(downloadsConverter.convert(media));
        }
    }

    /**
     * Populates the safety data sheet
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateSafetyDataSheet(ProductModel source, ProductData target) {
        MediaModel media = getLocalizedMedia(source, ProductModel.SAFETY_DATA_SHEET);
        if (media != null) {
            target.setSafetyDataSheet(downloadsConverter.convert(media));
        }
    }

    /**
     * Populates the technical data sheet
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateTechDataSheet(ProductModel source, ProductData target) {
        MediaModel media = getLocalizedMedia(source, ProductModel.TECH_DATA_SHEET);
        if (media != null) {
            target.setTechnicalDataSheet(downloadsConverter.convert(media));
        }
    }

    /**
     * Populates the normalisation
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateNormalisation(ProductModel source, ProductData target) {
        MediaModel media = getLocalizedMedia(source, ProductModel.NORMALISATION);
        if (media != null) {
            target.setNormalisation(downloadsConverter.convert(media));
        }
    }

    /**
     * Populates the DOP
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateDOP(ProductModel source, ProductData target) {
        MediaModel media = getLocalizedMedia(source, ProductModel.DOP);
        if (media != null) {
            target.setDOP(downloadsConverter.convert(media));
        }
    }

    /**
     * Populates the certificates
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateCertificates(ProductModel source, ProductData target) {
        Collection<MediaModel> medias = getLocalizedMediaCollection(source, ProductModel.CERTIFICATES);
        if (isNotEmpty(medias)) {
            target.setCertificates(downloadsConverter.convertAll(medias));
        }
    }

    /**
     * Populates the manuals
     *
     * @param source the product model
     * @param target the product data
     */
    private void populateManuals(ProductModel source, ProductData target) {
        List<ImageData> manuals = new ArrayList<>();

        Collection<MediaModel> generalManuals = getLocalizedMediaCollection(source, ProductModel.GENERALMANUALS);
        Collection<MediaModel> instructionManuals = getLocalizedMediaCollection(source, ProductModel.INSTRUCTIONMANUALS);
        Collection<MediaModel> userManuals = getLocalizedMediaCollection(source, ProductModel.USERMANUALS);
        Collection<MediaModel> maintenanceManuals = getLocalizedMediaCollection(source, ProductModel.MAINTENANCEMANUALS);


        if (isNotEmpty(generalManuals)) {
            manuals.addAll(downloadsConverter.convertAll(generalManuals));
        }

        if (isNotEmpty(instructionManuals)) {
            manuals.addAll(downloadsConverter.convertAll(instructionManuals));
        }

        if (isNotEmpty(userManuals)) {
            manuals.addAll(downloadsConverter.convertAll(userManuals));
        }

        if (isNotEmpty(maintenanceManuals)) {
            manuals.addAll(downloadsConverter.convertAll(maintenanceManuals));
        }

        target.setManuals(manuals);
    }

    /**
     * Populates the brand catalogs
     *
     * @param source the product model
     * @param target the product data
     */
    protected void populateBrandCatalogs(ProductModel source, ProductData target) {
        if (isNotEmpty(source.getSupercategories())) {
            BrandCategoryModel brandCategory = source.getSupercategories().stream()
                    .filter(BrandCategoryModel.class::isInstance)
                    .findFirst()
                    .map(BrandCategoryModel.class::cast)
                    .orElse(null);
            if (brandCategory != null && brandCategory.getBrandCatalog() != null) {
                target.setBrandCatalogs(singletonList(downloadsConverter.convert(brandCategory.getBrandCatalog())));
            }
        }
    }

    /**
     * Retrieves a localized mediamodel from the given product and based on the given attribute.
     * <p>
     * If the current active locale yields no results, try all fall back locales.
     *
     * @param source    the product
     * @param attribute the product attribute
     * @return the media model
     */
    protected MediaModel getLocalizedMedia(ProductModel source, String attribute) {
        MediaModel result = getModelService().getAttributeValue(source, attribute, i18NService.getCurrentLocale());

        if (result == null) {
            Locale[] fallbackLocales = i18NService.getFallbackLocales(i18NService.getCurrentLocale());

            for (Locale fallbackLocale : fallbackLocales) {
                result = getModelService().getAttributeValue(source, attribute, fallbackLocale);
                if (result != null) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Retrieves a collection of localized mediamodels from the given product and based on the given attribute.
     * <p>
     * If the current active locale yields no results, try all fall back locales.
     *
     * @param source    the product
     * @param attribute the product attribute
     * @return the collection of mediamodels
     */
    protected Collection<MediaModel> getLocalizedMediaCollection(ProductModel source, String attribute) {
        Collection<MediaModel> result = getModelService().getAttributeValue(source, attribute, i18NService.getCurrentLocale());

        if (result.isEmpty()) {
            Locale[] fallbackLocales = i18NService.getFallbackLocales(i18NService.getCurrentLocale());

            for (Locale fallbackLocale : fallbackLocales) {
                result = getModelService().getAttributeValue(source, attribute, fallbackLocale);
                if (isNotEmpty(result)) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Returns the model service
     *
     * @return the model service
     */
    protected ModelService getModelService() {
        // added for testing purposes
        return super.getModelService();
    }
}
