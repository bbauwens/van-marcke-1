package com.vanmarcke.facades.search.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.facades.search.AbstractVMKPointOfServiceSearchFacade;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import java.util.List;

import static de.hybris.platform.solrfacetsearch.search.OrderField.SortOrder.ASCENDING;
/**
 * Facade which provides methods to search for point of services.
 */
public class VMKPointOfServiceSearchFacadeImpl extends AbstractVMKPointOfServiceSearchFacade {

    private static final int PAGE_SIZE = 1_000_000;
    private VMKCommerceGroupService vmkCommerceGroupService;

    /**
     * Constructor for DefaultVMKPointOfServiceSearchFacade
     *
     * @param vmkCommerceGroupService the vmkCommerceGroupService
     */
    public VMKPointOfServiceSearchFacadeImpl(final VMKCommerceGroupService vmkCommerceGroupService) {
        this.vmkCommerceGroupService = vmkCommerceGroupService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<PointOfServiceData> doSearch(final SearchQuery searchQuery) throws FacetSearchException {
        populateSearchQuery(searchQuery);
        return super.doSearch(searchQuery);
    }

    /**
     * Provides the given search query with the required parameters.
     *
     * @param searchQuery the search query
     */
    protected void populateSearchQuery(final SearchQuery searchQuery) {
        searchQuery.setPageSize(PAGE_SIZE);
        final CommerceGroupModel commerceGroup = this.vmkCommerceGroupService.getCurrentCommerceGroup();
        if (commerceGroup != null) {
            searchQuery.addRawParam("fq", "country_string:" + commerceGroup.getCountry().getIsocode());
        }
        searchQuery.addSort("postalCode", ASCENDING);
    }
}