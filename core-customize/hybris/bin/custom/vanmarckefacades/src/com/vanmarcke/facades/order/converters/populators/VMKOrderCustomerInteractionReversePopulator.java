package com.vanmarcke.facades.order.converters.populators;

import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.facades.customerinteraction.data.CustomerInteractionData;
import de.hybris.platform.b2b.services.B2BOrderService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

/**
 * Blue Customer Interaction Reverse Populator
 */
public class VMKOrderCustomerInteractionReversePopulator implements Populator<CustomerInteractionData, OrderCustomerInteractionModel> {

    private final UserService userService;
    private final BaseSiteService baseSiteService;
    private final BaseStoreService baseStoreService;
    private final B2BOrderService b2bOrderService;

    /**
     * Constructor for BlueCustomerInteractionReversePopulator
     *
     * @param userService      the userService
     * @param baseSiteService  the baseSiteService
     * @param baseStoreService the baseStoreService
     * @param b2bOrderService  the b2bOrderService
     */
    public VMKOrderCustomerInteractionReversePopulator(final UserService userService, final BaseSiteService baseSiteService, final BaseStoreService baseStoreService, final B2BOrderService b2bOrderService) {
        this.userService = userService;
        this.baseSiteService = baseSiteService;
        this.baseStoreService = baseStoreService;
        this.b2bOrderService = b2bOrderService;
    }

    @Override
    public void populate(final CustomerInteractionData source, final OrderCustomerInteractionModel target) {
        target.setUser(userService.getCurrentUser());
        target.setSite(baseSiteService.getCurrentBaseSite());
        target.setStore(baseStoreService.getCurrentBaseStore());
        target.setType(source.getType());
        target.setMessage(source.getMessage());
        target.setOrder(b2bOrderService.getOrderForCode(source.getOrderNumber()));
    }
}
