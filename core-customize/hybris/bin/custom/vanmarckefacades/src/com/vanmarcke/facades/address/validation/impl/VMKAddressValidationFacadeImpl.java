package com.vanmarcke.facades.address.validation.impl;

import com.vanmarcke.cpi.data.ptv.PTVAddressData;
import com.vanmarcke.cpi.data.ptv.PTVResponseData;
import com.vanmarcke.cpi.services.PTVXServerService;
import com.vanmarcke.facades.address.validation.VMKAddressValidationFacade;
import com.vanmarcke.facades.addressvalidation.data.AddressValidationResponseData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;

/**
 * Implementation of the AddressValidationFacade interface
 */
public class VMKAddressValidationFacadeImpl implements VMKAddressValidationFacade {

    private PTVXServerService validationService;
    private Converter<AddressData, PTVAddressData> addressDataConverter;
    private Converter<PTVResponseData, AddressValidationResponseData> addressResponseDataConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public AddressValidationResponseData validateAddress(AddressData addressData) {
        PTVAddressData ptvAddressData = addressDataConverter.convert(addressData);
        PTVResponseData response = validationService.findAddress(ptvAddressData);
        return correctAddress(addressResponseDataConverter.convert(response), addressData);
    }

    /**
     * Checks whether the right translation is used for the town/city and corrects it if necessary.
     *
     * @param response        the validation response
     * @param originalAddress the original address
     * @return the validation result
     */
    private AddressValidationResponseData correctAddress(AddressValidationResponseData response, AddressData originalAddress) {
        for (AddressData addressData : response.getResultList()) {
            if (addressData.getTown2().equalsIgnoreCase(originalAddress.getTown())) {
                addressData.setTown(addressData.getTown2());
            }
        }
        return response;
    }

    @Required
    public void setValidationService(PTVXServerService validationService) {
        this.validationService = validationService;
    }

    @Required
    public void setAddressDataConverter(Converter<AddressData, PTVAddressData> addressDataConverter) {
        this.addressDataConverter = addressDataConverter;
    }

    @Required
    public void setAddressResponseDataConverter(Converter<PTVResponseData, AddressValidationResponseData> addressResponseDataConverter) {
        this.addressResponseDataConverter = addressResponseDataConverter;
    }
}
