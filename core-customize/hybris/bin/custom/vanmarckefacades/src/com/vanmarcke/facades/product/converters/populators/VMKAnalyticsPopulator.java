package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.facades.analytics.data.AnalyticsData;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.LocaleUtils;
import org.springframework.util.Assert;

import java.util.Locale;

/**
 * The {@code VMKAnalyticsPopulator} class is used to populate the product attributes that enabled advanced
 * e-commerce analytics.
 *
 * @author Joris Cryns, Christiaan Janssen
 * @since 27-09-2019
 */
public class VMKAnalyticsPopulator implements Populator<ProductModel, AnalyticsData> {

    private static final Locale DEFAULT_LOCALE = LocaleUtils.toLocale("nl_BE");

    private final CategoryService categoryService;

    /**
     * Creates a new instance of the {@link VMKAnalyticsPopulator} class.
     *
     * @param categoryService the category service
     */
    public VMKAnalyticsPopulator(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(ProductModel source, AnalyticsData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        populateName(source, target);
        populateCategory(source, target);
        populateBrand(source, target);
    }

    /**
     * Populates the name for the given {@code source}.
     *
     * @param source the product
     * @param target the analytics data
     */
    private void populateName(ProductModel source, AnalyticsData target) {
        target.setName(source.getName(DEFAULT_LOCALE));
    }

    /**
     * Populates the product category for the given {@code source}.
     *
     * @param source the product
     * @param target the analytics data
     */
    private void populateCategory(ProductModel source, AnalyticsData target) {
        target.setCategory(categoryService.getCategoryPathForProduct(getBaseProduct(source), CategoryModel.class)
                .stream()
                .reduce((c1, c2) -> c2)
                .map(cat -> cat.getName(DEFAULT_LOCALE))
                .orElseGet(String::new));
    }

    /**
     * Checks if the given {@code source} is a base product.
     * <p>
     * If the given {@code source} is a variant, its base product will be returned.
     *
     * @param source the source product
     * @return the base product
     */
    protected ProductModel getBaseProduct(ProductModel source) {
        ProductModel baseProduct = source;
        if (baseProduct instanceof VariantProductModel) {
            baseProduct = ((VariantProductModel) baseProduct).getBaseProduct();
        }
        return baseProduct;
    }

    /**
     * Populates the brand for the given {@code source}.
     *
     * @param source the product
     * @param target the analytics data
     */
    private void populateBrand(ProductModel source, AnalyticsData target) {
        target.setBrand(categoryService.getCategoryPathForProduct(source, BrandCategoryModel.class)
                .stream()
                .reduce((c1, c2) -> c2)
                .map(cat -> cat.getName(DEFAULT_LOCALE))
                .orElseGet(String::new));
    }
}