package com.vanmarcke.facades.product.converters.populators;

import com.google.common.collect.Iterables;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.NoSuchElementException;

/**
 * Populates necessary attributes for Google Structured Data
 *
 * @author Giani Ifrim
 * @since 20-07-2021
 */
public class VMKProductStructuredDataPopulator implements Populator<ProductModel, ProductData> {

    private static final Logger LOG = Logger.getLogger(VMKProductStructuredDataPopulator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(ProductModel productModel, ProductData productData) throws ConversionException {
        Collection<CategoryModel> superCategories = productModel.getSupercategories();
        CategoryModel superCategory = Iterables.get(superCategories, 0);

        productData.setSuperCategory(superCategory.getName());
        productData.setGtin(productModel.getVendorItemBarcode_current());
        productData.setMpn(productModel.getVendorItemNumber_current());
        productData.setProductId(productModel.getVendorItemNumber_current());
        productData.setIdentifier(productModel.getVendorItemBarcode_current());

        try {
            populateThumbnail(productModel, productData);
        } catch (NoSuchElementException e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(e);
            }
        }
    }

    /**
     * Populate the target thumbnail
     * Search and get the thumbnail from variants if it does not exist on the source object.
     *
     * @param productModel source
     * @param productData  target
     */
    private void populateThumbnail(ProductModel productModel, ProductData productData) {
        if (productModel.getThumbnail() != null) {
            productData.setThumbnail(productModel.getThumbnail().getURL());
        } else if (CollectionUtils.isNotEmpty(productModel.getVariants())) {
            productData.setThumbnail(productModel.getVariants().stream()
                    .filter(p -> p.getThumbnail() != null)
                    .findFirst().orElseThrow().getThumbnail().getURL());
        }
    }

}
