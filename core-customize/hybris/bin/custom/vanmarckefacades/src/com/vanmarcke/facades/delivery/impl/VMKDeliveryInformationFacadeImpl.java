package com.vanmarcke.facades.delivery.impl;

import com.vanmarcke.cpi.data.order.EntryInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingEntryInfoResponseData;
import com.vanmarcke.facades.data.PickupInfoData;
import com.vanmarcke.facades.data.ShippingInfoData;
import com.vanmarcke.facades.data.deliverypickup.product.DeliveryPickupProductData;
import com.vanmarcke.facades.delivery.VMKDeliveryInformationFacade;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.delivery.VMKFirstDateAvailabilityService;
import com.vanmarcke.services.delivery.VMKPickupInfoService;
import com.vanmarcke.services.delivery.VMKShippingInfoService;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.order.cart.VMKTecCollectReservationMessageService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import com.vanmarcke.services.util.VMKDateUtils;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This facade provides delivery info.
 *
 * @author Tom van den Berg, Niels Raemaekers
 * @since 30-12-2020
 */
public class VMKDeliveryInformationFacadeImpl implements VMKDeliveryInformationFacade {

    private static final Logger LOG = LogManager.getLogger();

    private final Converter<CartModel, DeliveryMethodForm> deliveryMethodFormConverter;
    private final Populator<PointOfServiceModel, PointOfServiceData> pointOfServicePopulator;
    private final VMKDeliveryInfoService vmkDeliveryInfoService;
    private final VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService;
    private final VMKBaseStoreService baseStoreService;
    private final VMKPointOfServiceService pointOfServiceService;
    private final VMKPickupInfoService pickupInfoService;
    private final VMKShippingInfoService shippingInfoService;
    private final VMKTecCollectReservationMessageService tecCollectReservationMessageService;
    private final VMKOpeningScheduleService openingScheduleService;

    /**
     * Provides an instance of the VMKDeliveryInformationFacadeImpl.
     *
     * @param deliveryMethodFormConverter         the delivery method form converter
     * @param pointOfServicePopulator             the point of service populator
     * @param vmkDeliveryInfoService              the delivery info service
     * @param vmkFirstDateAvailabilityService     the first available day service
     * @param baseStoreService                    the base store service
     * @param pointOfServiceService               the point of service service
     * @param pickupInfoService                   the pickup info service
     * @param shippingInfoService                 the shipping info service
     * @param tecCollectReservationMessageService the tec collect reservation message service
     * @param openingScheduleService              the opening schedule service
     */

    public VMKDeliveryInformationFacadeImpl(Converter<CartModel, DeliveryMethodForm> deliveryMethodFormConverter, Populator<PointOfServiceModel, PointOfServiceData> pointOfServicePopulator, VMKDeliveryInfoService vmkDeliveryInfoService,
                                            VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService, VMKBaseStoreService baseStoreService, VMKPointOfServiceService pointOfServiceService, VMKPickupInfoService pickupInfoService,
                                            VMKShippingInfoService shippingInfoService, VMKTecCollectReservationMessageService tecCollectReservationMessageService, VMKOpeningScheduleService openingScheduleService) {
        this.deliveryMethodFormConverter = deliveryMethodFormConverter;
        this.pointOfServicePopulator = pointOfServicePopulator;
        this.vmkDeliveryInfoService = vmkDeliveryInfoService;
        this.vmkFirstDateAvailabilityService = vmkFirstDateAvailabilityService;
        this.baseStoreService = baseStoreService;
        this.pointOfServiceService = pointOfServiceService;
        this.pickupInfoService = pickupInfoService;
        this.shippingInfoService = shippingInfoService;
        this.tecCollectReservationMessageService = tecCollectReservationMessageService;
        this.openingScheduleService = openingScheduleService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShippingInfoData getShippingInfo(final CartModel cart) {
        final ShippingInfoData shippingInfo = new ShippingInfoData();
        vmkDeliveryInfoService.getShippingDateInformation(cart).ifPresent(shippingDateInfoResponse -> {
            shippingInfo.setFirstPossibleShippingDate(shippingInfoService.getFirstPossibleShippingDate(cart, shippingDateInfoResponse));
            shippingInfo.setProducts(getDeliveryProductData(cart, shippingDateInfoResponse));
        });
        return shippingInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PickupInfoData getPickupInfo(final CartModel cart, final String selectedDate) {
        final PickupInfoData pickupInfoData = new PickupInfoData();
        vmkDeliveryInfoService.getPickupDateInformation(cart).ifPresent(pickupDateResponse -> {
            final PointOfServiceModel currentPointOfService = pointOfServiceService.getCurrentPointOfService(cart);
            final boolean tecCollectPossible = pickupInfoService.isTecCollectPossible(cart, pickupDateResponse);
            final Date todayDate = Date.from(LocalDate.now().atStartOfDay(ZoneId.of("UTC")).toInstant()); // using UTC since date will be formatted by Jackson

            final Optional<Date> firstPossiblePickupDate = pickupInfoService.getFirstPossiblePickupDate(cart, pickupDateResponse);
            final Optional<Date> lastPossiblePickupDate = pickupInfoService.getLastPossiblePickupDate(pickupDateResponse);
            final Optional<Date> pickupDateForPreselect = findPickupDateForPreselect(currentPointOfService, todayDate, selectedDate, lastPossiblePickupDate.orElse(null), tecCollectPossible);

            final List<PointOfServiceModel> allPointOfService = baseStoreService.getCurrentBaseStore().getPointsOfService();
            final Set<PointOfServiceModel> alternativeTec = new HashSet<>(pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart));
            final List<PointOfServiceData> allPointOfServiceWithPickupDates = getPointOfServiceData(todayDate, pickupDateForPreselect.orElse(null), lastPossiblePickupDate.orElse(null), currentPointOfService, allPointOfService, alternativeTec, tecCollectPossible);

            pickupInfoData.setTecCollectPossible(tecCollectPossible);
            pickupInfoData.setAlternativePointOfService(allPointOfServiceWithPickupDates);
            pickupInfoData.setOpenOnSaturday(openingScheduleService.isStoreOpenOnSaturday(currentPointOfService));

            firstPossiblePickupDate.ifPresentOrElse(pickupDate -> {
                        pickupInfoData.setFirstPossiblePickupDate(pickupDate);
                        pickupInfoData.setReservationMessage(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, pickupDate, tecCollectPossible));
                    },
                    () -> LOG.debug("Unable to determine first possible pickup date [cart: {} | point of service: {}]", cart::getCode, currentPointOfService::getName));
            lastPossiblePickupDate.ifPresentOrElse(pickupDate -> pickupInfoData.setProducts(getPickupProductData(cart, pickupDateResponse, pickupDate)),
                    () -> LOG.debug("Unable to determine last possible pickup date [cart: {} | point of service: {}]", cart::getCode, currentPointOfService::getName));
            pickupDateForPreselect.ifPresentOrElse(pickupInfoData::setPickupDateForPreselect,
                    () -> LOG.debug("Unable to determine pickup date to be preselected [cart: {} | point of service: {}]", cart::getCode, currentPointOfService::getName));
        });
        return pickupInfoData;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DeliveryMethodForm getDeliveryMethodForm(final CartModel cart) {
        return deliveryMethodFormConverter.convert(cart);
    }

    /**
     * Convert Pickup response to Data
     *
     * @param cart the current cart
     * @return List with pickup product data
     */
    private List<DeliveryPickupProductData> getPickupProductData(final CartModel cart, final PickupDateInfoResponseData response, final Date lastPossiblePickupDate) {
        final Map<String, Date> productsByPickupDate = CollectionUtils.emptyIfNull(response.getProducts()).stream()
                .filter(pickupEntry -> ObjectUtils.allNotNull(pickupEntry.getProductNumber(), pickupEntry.getPickupDate()))
                .collect(Collectors.toMap(EntryInfoResponseData::getProductNumber, EntryInfoResponseData::getPickupDate));

        return getDeliveryPickupProductData(productsByPickupDate, lastPossiblePickupDate, cart);
    }

    private List<PointOfServiceData> getPointOfServiceData(final Date todayDate, final Date pickupDateForPreselect, final Date lastPossiblePickupDate, final PointOfServiceModel currentPointOfService,
                                                           final List<PointOfServiceModel> allPointOfService, final Set<PointOfServiceModel> alternativeTec, final boolean tecCollectPossible) {
        final List<PointOfServiceData> pointsOfService = allPointOfService.stream()
                .map(pos -> {
                    final PointOfServiceData pointOfService = new PointOfServiceData();
                    final boolean isTecCollect = alternativeTec.contains(pos) || pos.equals(currentPointOfService) && tecCollectPossible;

                    pointOfServicePopulator.populate(pos, pointOfService);
                    openingScheduleService.findOpenPickupDateForPointOfService(pos, isTecCollect ? todayDate : lastPossiblePickupDate).ifPresent(pointOfService::setPickupDate);

                    return pointOfService;
                })
                .sorted(Comparator.comparing(PointOfServiceData::getDisplayName))
                .collect(Collectors.toList());

        if (pickupDateForPreselect != null) {
            pointsOfService.forEach(pointOfService -> {
                final boolean isPickupPossibleOnCurrentPickupDate = VMKDateUtils.daysBetween(pointOfService.getPickupDate(), pickupDateForPreselect) >= 0;
                pointOfService.setDaysUntilPickup(VMKDateUtils.daysBetween(todayDate, pointOfService.getPickupDate()));
                pointOfService.setEnabledOnSelectedDate(isPickupPossibleOnCurrentPickupDate && openingScheduleService.isStoreOpenOnGivenDate(pointOfService.getName(), pickupDateForPreselect));
            });
        }
        return pointsOfService;
    }

    /**
     * Use pickup date selected by user if possible, otherwise use first possible pickup date for current point of service.
     * <p>
     * NOTE: if no pickup date was selected by user and tec collect is possible then current point of service's NEXT working day after today will be returned.
     *
     * @param currentPointOfService  the current point of service
     * @param todayDate              today's date
     * @param selectedDate           the selected date
     * @param lastPossiblePickupDate the last possible pickup date
     * @param tecCollectPossible     is tec collect possible
     * @return the pickup date to be selected in pickup date picker
     */
    private Optional<Date> findPickupDateForPreselect(final PointOfServiceModel currentPointOfService, final Date todayDate, final String selectedDate, final Date lastPossiblePickupDate, final boolean tecCollectPossible) {
        final Date startDate;
        if (selectedDate != null) {
            startDate = parseSelectedDate(selectedDate);
        } else if (tecCollectPossible || (lastPossiblePickupDate != null && DateUtils.isSameDay(todayDate, lastPossiblePickupDate))) {
            startDate = DateUtils.addDays(todayDate, 1);
        } else {
            startDate = lastPossiblePickupDate;
        }
        return openingScheduleService.findOpenPickupDateForPointOfService(currentPointOfService, startDate);
    }

    /**
     * Convert Shipping response to Data
     *
     * @param cart                     the current cart
     * @param shippingDateInfoResponse the shipping date info response
     * @return List with shipping product data
     */
    private List<DeliveryPickupProductData> getDeliveryProductData(final CartModel cart, final ShippingDateInfoResponseData shippingDateInfoResponse) {
        final Date lastPossibleShippingDate = shippingInfoService.getLastPossibleShippingDate(shippingDateInfoResponse);
        final Map<String, Date> productMap = CollectionUtils.emptyIfNull(shippingDateInfoResponse.getResults()).stream()
                .filter(shippingEntry -> ObjectUtils.allNotNull(shippingEntry.getProductNumber(), shippingEntry.getDeliveryDate()))
                .collect(Collectors.toMap(ShippingEntryInfoResponseData::getProductNumber, ShippingEntryInfoResponseData::getDeliveryDate));

        return getDeliveryPickupProductData(productMap, lastPossibleShippingDate, cart);
    }

    /**
     * Get the delivery/pickup product data
     *
     * @param source           the data in raw format
     * @param lastPossibleDate the last possible date
     * @return list with the data
     */
    private List<DeliveryPickupProductData> getDeliveryPickupProductData(final Map<String, Date> source,
                                                                         final Date lastPossibleDate,
                                                                         final CartModel cart) {
        List<DeliveryPickupProductData> target = new ArrayList<>();

        source.forEach((productCode, deliveryPickupDate) -> {
            DeliveryPickupProductData deliveryPickupProductData = new DeliveryPickupProductData();
            deliveryPickupProductData.setProductCode(productCode);

            if (deliveryPickupDate != null) {
                final Date firstPossibleDate = vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(deliveryPickupDate);
                deliveryPickupProductData.setFirstPossibleDate(firstPossibleDate);

                if (lastPossibleDate != null) {
                    final Date lastValidPossibleDate = firstPossibleDate.compareTo(lastPossibleDate) > 0 ? firstPossibleDate : lastPossibleDate;
                    deliveryPickupProductData.setLastPossibleDate(lastValidPossibleDate);
                }
            }
            deliveryPickupProductData.setSelectDate(cart.getDeliveryDate());
            target.add(deliveryPickupProductData);
        });

        return target;
    }

    /**
     * Parses date string in UTC timezone as this date is expected to be formatted to string by Jackson with default settings.
     *
     * @param date the date string
     * @return parsed date
     */
    private Date parseSelectedDate(final String date) {
        return Date.from(LocalDate.parse(date).atStartOfDay(ZoneId.of("UTC")).toInstant());
    }
}
