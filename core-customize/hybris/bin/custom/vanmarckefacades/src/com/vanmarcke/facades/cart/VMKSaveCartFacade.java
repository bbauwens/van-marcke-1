package com.vanmarcke.facades.cart;

import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartParameterData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartResultData;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;

import java.io.InputStream;

/**
 * Custom Interface for {@link SaveCartFacade}
 */
public interface VMKSaveCartFacade extends SaveCartFacade {

    /**
     * Gets all Technical Data Sheets for {@link de.hybris.platform.core.model.product.ProductModel} of {@link de.hybris.platform.core.model.order.CartModel}
     * and combine them into one.
     *
     * @param cartId Id of the {@link de.hybris.platform.core.model.order.CartModel}
     * @return {@link InputStream} that contains all Technical Data Sheets
     */
    InputStream exportTechnicalDataSheetsForSavedCartId(String cartId);

    /**
     * Returns the cart summary for the given {@code inputParemeters}.
     *
     * @param inputParameters the input parameters
     * @return the cart summary
     */
    CommerceSaveCartResultData getCartSummary(CommerceSaveCartParameterData inputParameters) throws CommerceSaveCartException;
}