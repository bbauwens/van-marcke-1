package com.vanmarcke.facades.cart.validation;

import com.vanmarcke.services.MessageData;

/**
 * The {@code VMKCartValidationFacade} interface defines the facade methods concerning the cart validation.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 03-02-2020
 */
public interface VMKCartValidationFacade {

    /**
     * Checks whether the current cart is valid.
     * <p>
     * Returns an validation message code in case the cart is invalid or {@code null} otherwise.
     *
     * @return the validation message
     */
    MessageData checkCartValidityAndProcessResponse();
}
