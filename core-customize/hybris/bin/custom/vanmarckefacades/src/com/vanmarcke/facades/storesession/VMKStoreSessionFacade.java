package com.vanmarcke.facades.storesession;

import com.vanmarcke.facades.site.data.BaseSiteData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;

import java.util.Collection;
import java.util.List;

/**
 * Custom interface for StoreSessionFacade
 */
public interface VMKStoreSessionFacade extends StoreSessionFacade {

    /**
     * Gets the available sites
     *
     * @return collection with BaseSiteData
     */
    Collection<BaseSiteData> getAllSites();

    /**
     * Gets current site stored in session.
     *
     * @return current site for the current store.
     */
    BaseSiteData getCurrentSite();

    /**
     * Sets the current site in the session
     *
     * @param uid the uid for the Site
     */
    void setCurrentSite(String uid);

    /**
     * Gets all stores
     *
     * @return Collection of storedata
     */
    List<PointOfServiceData> getAllStores();

    /**
     * gets current favorite store stored in session
     *
     * @return String containing the favorite store
     */
    PointOfServiceData getCurrentStore();

    /**
     * Sets the current favorite store in the session
     *
     * @param uid the uid for the Store
     */
    void setCurrentStore(String uid);
}
