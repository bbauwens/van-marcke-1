package com.vanmarcke.facades.cart.impl;

import com.vanmarcke.facades.cart.VMKCartFacade;
import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.services.exception.UnsupportedDeliveryDateException;
import com.vanmarcke.services.order.cart.VMKCommerceCartService;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Date;
import java.util.List;

/**
 * Custom implementation for the CartFacade
 */
public class VMKCartFacadeImpl extends DefaultCartFacade implements VMKCartFacade {

    private final Converter<AbstractOrderModel, SavedCartData> savedCartConverter;

    /**
     * Creates a new instance of the {@link VMKCartFacadeImpl} class.
     *
     * @param savedCartConverter the saved cart converter
     */
    public VMKCartFacadeImpl(final Converter<AbstractOrderModel, SavedCartData> savedCartConverter) {
        this.savedCartConverter = savedCartConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CartModificationData> addToCart(final List<AddToCartParams> addToCartParamsList) throws CommerceCartMergingException {
        final List<CommerceCartModification> commerceCartModificationList = ((VMKCommerceCartService) getCommerceCartService()).addToCart(getCommerceCartParameterConverter().convertAll(addToCartParamsList));
        return getCartModificationConverter().convertAll(commerceCartModificationList);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateCartCheckoutMode(final Boolean checkoutMode) {
        if (getCartService().hasSessionCart()) {
            final CommerceCartParameter parameter = new CommerceCartParameter();
            parameter.setCart(getCartService().getSessionCart());
            parameter.setCheckoutMode(checkoutMode);
            parameter.setEnableHooks(true);
            ((VMKCommerceCartService) getCommerceCartService()).updateCartCheckoutMode(parameter);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SavedCartData getSavedCartDetails(String cartGuid) {
        CartModel cart = getCommerceCartService().getCartForGuidAndSite(cartGuid, getBaseSiteService().getCurrentBaseSite());
        return savedCartConverter.convert(cart);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CartData setRequestedDeliveryDate(final Date requestedDeliveryDate) throws UnsupportedDeliveryDateException {
        CartData cartData = null;
        if (hasSessionCart()) {
            final CartModel cart = getCartService().getSessionCart();

            final CommerceCartParameter parameter = new CommerceCartParameter();
            parameter.setCart(cart);

            ((VMKCommerceCartService) getCommerceCartService()).setRequestedDeliveryDate(parameter, requestedDeliveryDate);

            cartData = getCartConverter().convert(cart);
        }
        return cartData;
    }
}