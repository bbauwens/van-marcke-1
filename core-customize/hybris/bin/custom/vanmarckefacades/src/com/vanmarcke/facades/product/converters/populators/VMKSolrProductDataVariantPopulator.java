package com.vanmarcke.facades.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Populate Variant Option Data for product list view from product data resulted from a solr search
 *
 * @author Cristi Stoica
 * @since 21-07-2021
 */
public class VMKSolrProductDataVariantPopulator implements Populator<ProductData, VariantOptionData> {

    @Override
    public void populate(ProductData source, VariantOptionData target) throws ConversionException {
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setUrl(source.getUrl());
        target.setDescription(source.getDescription());
        target.setStockStatus(source.getStockStatus());
    }
}
