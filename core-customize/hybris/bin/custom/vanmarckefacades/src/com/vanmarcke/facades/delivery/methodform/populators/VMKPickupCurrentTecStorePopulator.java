package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Populates the already chosen TEC store on the {@link DeliveryMethodForm}.
 *
 * @author Tom van den Berg
 * @since 30-12-2020
 */
public class VMKPickupCurrentTecStorePopulator implements Populator<CartModel, DeliveryMethodForm> {

    private final VMKStoreSessionFacade storeSessionFacade;

    /**
     * Provides an instance of the VMKPickupCurrentTecStorePopulator.
     *
     * @param storeSessionFacade the store session facade
     */
    public VMKPickupCurrentTecStorePopulator(VMKStoreSessionFacade storeSessionFacade) {
        this.storeSessionFacade = storeSessionFacade;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(CartModel source, DeliveryMethodForm target) {
        target.setTecUid(getCurrentTecStore(source));
    }

    /**
     * Retrieves the current TEC store UID.
     *
     * @param cart the cart
     * @return the TEC UID
     */
    private String getCurrentTecStore(CartModel cart) {
        if (cart.getDeliveryPointOfService() != null) {
            return cart.getDeliveryPointOfService().getName();
        } else if (storeSessionFacade.getCurrentStore() != null) {
            return storeSessionFacade.getCurrentStore().getName();
        }

        return null;
    }
}
