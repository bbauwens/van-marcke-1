package com.vanmarcke.facades.cms.impl;

import com.vanmarcke.core.model.ConsentParagraphComponentModel;
import com.vanmarcke.facades.cms.ConsentParagraphData;
import com.vanmarcke.facades.cms.VMKConsentParagraphComponentFacade;
import com.vanmarcke.services.consent.VMKConsentParagraphComponentService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

/**
 * The {@link VMKConsentParagraphComponentFacadeImpl} class implements the {@link VMKConsentParagraphComponentFacade}
 * interface.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
public class VMKConsentParagraphComponentFacadeImpl implements VMKConsentParagraphComponentFacade {

    private final VMKConsentParagraphComponentService consentParagraphComponentService;
    private final Converter<ConsentParagraphComponentModel, ConsentParagraphData> consentParagraphConverter;

    /**
     * Creates a new instance of the {@link VMKConsentParagraphComponentFacadeImpl} class.
     *
     * @param consentParagraphComponentService the consent paragraph component
     * @param consentParagraphConverter        the consent paragraph converter
     */
    public VMKConsentParagraphComponentFacadeImpl(VMKConsentParagraphComponentService consentParagraphComponentService, Converter<ConsentParagraphComponentModel,
            ConsentParagraphData> consentParagraphConverter) {
        this.consentParagraphComponentService = consentParagraphComponentService;
        this.consentParagraphConverter = consentParagraphConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ConsentParagraphData> getConsentParagraphs() {
        return consentParagraphConverter.convertAll(consentParagraphComponentService.getConsentParagraphs());
    }
}
