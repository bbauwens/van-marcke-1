package com.vanmarcke.facades.search.impl;

import com.vanmarcke.facades.search.AbstractVMKSearchFacade;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;

/**
 * Default Category Search Facade implementation
 */
public class VMKCategorySearchFacadeImpl extends AbstractVMKSearchFacade<CategoryData> {

    @Override
    protected FacetSearchConfig getFacetSearchConfig() throws FacetConfigServiceException {
        return getFacetSearchConfigService().getConfiguration(getCmsSiteService().getCurrentSite().getCategorySolrFacetSearchConfiguration().getName());
    }
}