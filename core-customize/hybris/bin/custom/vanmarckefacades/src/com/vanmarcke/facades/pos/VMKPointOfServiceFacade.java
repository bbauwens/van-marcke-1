package com.vanmarcke.facades.pos;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

/**
 * Facade which provides methods related to {@link PointOfServiceModel} instances.
 *
 * @author Tom van den Berg, Giani Ifrim
 * @since 30-12-2020
 */
public interface VMKPointOfServiceFacade {

    /**
     * Get the Alternative Tec Stores sorted on distance and limited to the static Page Size defined here
     *
     * @param cart the cart
     * @return the Alternative Tec Stores sorted on distance and limited
     */
    List<PointOfServiceData> findAlternativePointOfServicesCloseByCurrentStore(CartModel cart);


    /**
     * Get fallback POS for the current base site.
     * @return a POS linked to the current base site.
     */
    PointOfServiceModel getFallbackPOS();
}
