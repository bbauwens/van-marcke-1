package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKOrderPlacedByPopulatorTest {

    private VMKOrderPlacedByPopulator vmkOrderPlacedByPopulator;

    @Mock
    private UserModel userModel;
    @Mock
    private OrderModel orderModel;

    @Before
    public void setup() {
        vmkOrderPlacedByPopulator = new VMKOrderPlacedByPopulator();

        when(userModel.getUid()).thenReturn("user-id");
    }

    @Test
    public void testPopulate_should_do_nothing_when_placed_by_user_is_not_present() {
        OrderData result = new OrderData();
        vmkOrderPlacedByPopulator.populate(orderModel, result);

        assertThat(result.getPlacedBy()).isNull();
    }

    @Test
    public void testPopulate_should_populate_name_when_name_is_present() {
        when(userModel.getName()).thenReturn("user-name");

        when(orderModel.getPlacedBy()).thenReturn(userModel);

        OrderData result = new OrderData();
        vmkOrderPlacedByPopulator.populate(orderModel, result);

        assertThat(result.getPlacedBy()).isEqualTo("user-name");
    }

    @Test
    public void testPopulate_should_populate_uid_when_name_is_not_present() {
        when(orderModel.getPlacedBy()).thenReturn(userModel);

        OrderData result = new OrderData();
        vmkOrderPlacedByPopulator.populate(orderModel, result);

        assertThat(result.getPlacedBy()).isEqualTo("user-id");
    }

}