package com.vanmarcke.facades.product.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.facades.product.data.BrandData;
import com.vanmarcke.facades.product.data.PriceListData;
import com.vanmarcke.services.category.VMKCategoryService;
import com.vanmarcke.services.product.price.VMKNetPriceLookupStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.*;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductFacadeImplTest {

    @Mock
    private VMKNetPriceLookupStrategy blueNetPriceLookupStrategy;
    @Mock
    private PriceDataFactory priceDataFactory;
    @Mock
    private CommercePriceService commercePriceService;
    @Mock
    private ProductService productService;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private Converter<MediaModel, ImageData> imageConverter;
    @Mock
    private VMKCategoryService categoryService;
    @Mock
    private CommerceCategoryService commerceCategoryService;
    @Mock(name = "variantOptionDataConverter")
    private Converter<VariantProductModel, VariantOptionData> variantOptionDataConverter;
    @Mock(name = "productListVariantOptionDataConverter")
    private Converter<VariantProductModel, VariantOptionData> productListVariantOptionDataConverter;
    @Mock
    private Populator<ProductData, VariantOptionData> vmkSolrProductDataVariantPopulator;

    private VMKProductFacadeImpl<ProductModel> blueProductFacade;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        blueProductFacade = new VMKProductFacadeImpl(blueNetPriceLookupStrategy, priceDataFactory, commercePriceService, imageConverter, categoryService, commerceCategoryService, variantOptionDataConverter, productListVariantOptionDataConverter, vmkSolrProductDataVariantPopulator);
        blueProductFacade.setProductService(productService);
        blueProductFacade.setCommonI18NService(commonI18NService);
    }

    @Test
    public void testGetPricesForProductOnlyContainsNETPrice() {
        ProductModel product = mock(ProductModel.class);
        CurrencyModel currency = mock(CurrencyModel.class);
        PriceData expectedPriceData = mock(PriceData.class);
        when(productService.getProductForCode("code")).thenReturn(product);
        when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
        when(blueNetPriceLookupStrategy.getNetPriceForProduct(product)).thenReturn(BigDecimal.TEN);
        when(priceDataFactory.create(PriceDataType.NET, BigDecimal.TEN, currency)).thenReturn(expectedPriceData);
        when(commercePriceService.getFromPriceForProduct(product)).thenReturn(null);

        PriceListData priceListData = blueProductFacade.getPricesForProduct("code");

        assertThat(priceListData.getPrices()).containsOnly(expectedPriceData);
    }

    @Test
    public void testGetPricesForProductOnlyContainsLISTPrice() {
        ProductModel product = mock(ProductModel.class);
        PriceData expectedPriceData = mock(PriceData.class);
        PriceInformation priceInformation = getPriceInformation();
        when(productService.getProductForCode("code")).thenReturn(product);
        when(blueNetPriceLookupStrategy.getNetPriceForProduct(product)).thenReturn(null);
        when(commercePriceService.getFromPriceForProduct(product)).thenReturn(priceInformation);
        when(priceDataFactory.create(PriceDataType.LIST, BigDecimal.valueOf(10d), "EUR")).thenReturn(expectedPriceData);

        PriceListData priceListData = blueProductFacade.getPricesForProduct("code");

        assertThat(priceListData.getPrices()).containsOnly(expectedPriceData);
    }

    @Test
    public void testGetPricesForProductOnlyContainsLISTAndNETPrice() {
        ProductModel product = mock(ProductModel.class);
        CurrencyModel currency = mock(CurrencyModel.class);
        PriceData expectedNetPriceData = mock(PriceData.class);
        PriceData expectedListPriceData = mock(PriceData.class);
        PriceInformation priceInformation = getPriceInformation();
        when(productService.getProductForCode("code")).thenReturn(product);
        when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
        when(blueNetPriceLookupStrategy.getNetPriceForProduct(product)).thenReturn(BigDecimal.TEN);
        when(priceDataFactory.create(PriceDataType.NET, BigDecimal.TEN, currency)).thenReturn(expectedNetPriceData);
        when(commercePriceService.getFromPriceForProduct(product)).thenReturn(priceInformation);
        when(priceDataFactory.create(PriceDataType.LIST, BigDecimal.valueOf(10d), "EUR")).thenReturn(expectedListPriceData);

        PriceListData priceListData = blueProductFacade.getPricesForProduct("code");

        assertThat(priceListData.getPrices()).contains(expectedNetPriceData, expectedListPriceData);
    }

    @Test
    public void testGetPricesForProductDoesNotContainPrices() {
        ProductModel product = mock(ProductModel.class);
        when(productService.getProductForCode("code")).thenReturn(product);
        when(blueNetPriceLookupStrategy.getNetPriceForProduct(product)).thenReturn(null);
        when(commercePriceService.getFromPriceForProduct(product)).thenReturn(null);

        PriceListData priceListData = blueProductFacade.getPricesForProduct("code");

        assertThat(priceListData.getPrices()).isEmpty();
    }

    @Test
    public void testEnrichProductDataWithSolrVariants_isGridView() {
        ProductData productData = mock(ProductData.class);
        ProductData variant = mock(ProductData.class);
        VanMarckeVariantProductModel solrVariantProduct = mock(VanMarckeVariantProductModel.class);
        VariantOptionData convertedVariant = mock(VariantOptionData.class);

        when(variant.getCode()).thenReturn("code");
        when(convertedVariant.getCode()).thenReturn("code");
        when(productData.getCode()).thenReturn("code");
        when(productService.getProductForCode("code")).thenReturn(solrVariantProduct);
        when(variantOptionDataConverter.convert(solrVariantProduct)).thenReturn(convertedVariant);

        blueProductFacade.enrichProductDataWithSolrVariants(productData, List.of(variant), false);

        ArgumentCaptor<List> argumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(productData, times(1)).setVariantOptions(List.of(convertedVariant));
        verify(productData, times(1)).setBaseOptions(argumentCaptor.capture());
        verify(vmkSolrProductDataVariantPopulator, times(1)).populate(variant, convertedVariant);
        List<BaseOptionData> values = argumentCaptor.getValue();
        Assert.assertEquals(List.of(convertedVariant), values.get(0).getOptions());
        Assert.assertEquals(convertedVariant, values.get(0).getSelected());
    }

    @Test
    public void testEnrichProductDataWithSolrVariants_isListView() {
        ProductData productData = mock(ProductData.class);
        ProductData variant = mock(ProductData.class);
        VanMarckeVariantProductModel solrVariantProduct = mock(VanMarckeVariantProductModel.class);
        VariantOptionData convertedVariant = mock(VariantOptionData.class);

        when(variant.getCode()).thenReturn("code");
        when(convertedVariant.getCode()).thenReturn("code");
        when(productData.getCode()).thenReturn("code");
        when(productService.getProductForCode("code")).thenReturn(solrVariantProduct);
        when(productListVariantOptionDataConverter.convert(solrVariantProduct)).thenReturn(convertedVariant);
        blueProductFacade.enrichProductDataWithSolrVariants(productData, List.of(variant), true);

        ArgumentCaptor<List> argumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(productData, times(1)).setVariantOptions(List.of(convertedVariant));
        verify(productData, times(1)).setBaseOptions(argumentCaptor.capture());
        verify(vmkSolrProductDataVariantPopulator, times(1)).populate(variant, convertedVariant);
        List<BaseOptionData> values = argumentCaptor.getValue();
        Assert.assertEquals(List.of(convertedVariant), values.get(0).getOptions());
        Assert.assertEquals(convertedVariant, values.get(0).getSelected());
    }

    @Test
    public void testEnrichProductDataWithBrandLogo() {
        // given
        ProductData productData = mock(ProductData.class);
        BrandCategoryModel brandCategoryModel = mock(BrandCategoryModel.class);
        BrandData brandData = mock(BrandData.class);
        when(productData.getBrand()).thenReturn(brandData);
        when(brandData.getCode()).thenReturn("brand");
        when(commerceCategoryService.getCategoryForCode("brand")).thenReturn(brandCategoryModel);
        MediaModel mediaModel = mock(MediaModel.class);
        ImageData imageData = mock(ImageData.class);
        when(categoryService.getLocalizedBrandLogoForFormat(brandCategoryModel, "SMALL")).thenReturn(Optional.of(mediaModel));
        when(imageConverter.convert(mediaModel)).thenReturn(imageData);

        // when
        blueProductFacade.enrichProductDataWithBrandLogo(productData);

        // then
        verify(brandData, times(1)).setLogo(imageData);
    }


    private PriceInformation getPriceInformation() {
        PriceInformation priceInformation = mock(PriceInformation.class);
        PriceValue priceValue = mock(PriceValue.class);
        when(priceInformation.getPriceValue()).thenReturn(priceValue);
        when(priceValue.getCurrencyIso()).thenReturn("EUR");
        when(priceValue.getValue()).thenReturn(10d);
        return priceInformation;
    }

}