package com.vanmarcke.facades.builder;

import com.vanmarcke.services.builder.ProductExportItemBuilder;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;

public class FeatureValueDataBuilder {

    private final FeatureValueData featureValue = new FeatureValueData();

    /**
     * Private constructor to hide the implicit default one.
     */
    private FeatureValueDataBuilder() {
    }

    /**
     * A new instance of the {@link ProductExportItemBuilder} is created and returned
     *
     * @return the created {@link ProductExportItemBuilder} instance
     */
    public static FeatureValueDataBuilder aFeatureValueData() {
        return new FeatureValueDataBuilder();
    }

    /**
     * Sets the given {@code value} on the current {@link FeatureValueData} instance.
     *
     * @param value the value
     * @return the current {@link ProductExportItemBuilder} instance for method chaining
     */
    public FeatureValueDataBuilder withValue(String value) {
        featureValue.setValue(value);
        return this;
    }

    /**
     * Returns the current {@link FeatureValueData} instance
     *
     * @return the current {@link FeatureValueData} instance
     */
    public FeatureValueData build() {
        return featureValue;
    }
}
