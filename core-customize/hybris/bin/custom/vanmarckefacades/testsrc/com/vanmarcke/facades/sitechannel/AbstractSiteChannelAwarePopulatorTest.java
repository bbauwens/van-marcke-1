package com.vanmarcke.facades.sitechannel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.site.BaseSiteService;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractSiteChannelAwarePopulatorTest {

    @Mock
    private BaseSiteService baseSiteService;

    private List<SiteChannel> siteChannels = Collections.singletonList(SiteChannel.B2B);

    DummyAbstractSiteChannelAwarePopulator populator;

    @Before
    public void setUp() throws Exception {
        populator = new DummyAbstractSiteChannelAwarePopulator();
        populator.setSiteChannels(siteChannels);
        populator.setBaseSiteService(baseSiteService);
    }

    @Test
    public void testShouldPopulate() {
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.B2B);

        Assertions
                .assertThat(populator.shouldPopulate(new Object()))
                .isTrue();
    }

    @Test
    public void testShouldPopulate_not() {
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.DIY);

        Assertions
                .assertThat(populator.shouldPopulate(new Object()))
                .isFalse();
    }

    class DummyAbstractSiteChannelAwarePopulator extends AbstractSiteChannelAwarePopulator {
        @Override
        protected void doPopulate(Object o, Object o2) {
            //do nothing
        }
    }
}