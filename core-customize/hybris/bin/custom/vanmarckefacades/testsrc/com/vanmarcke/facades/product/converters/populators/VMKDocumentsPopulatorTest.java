package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDocumentsPopulatorTest {

    private static final String CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String ATTRIBUTE = RandomStringUtils.randomAlphabetic(10);
    private static final Locale LOCALE_NL_BE = new Locale("nl_BE");
    private static final Locale LOCALE_NL = new Locale("nl");
    private static final Locale LOCALE_EN = new Locale("en");
    private static final Locale[] FALLBACK_LOCALES = {LOCALE_NL, LOCALE_EN};

    @Mock
    private Converter<MediaModel, ImageData> downloadsConverter;

    @Mock
    private ModelService modelService;

    @Mock
    private I18NService i18NService;

    @Spy
    @InjectMocks
    private VMKDocumentsPopulator populator;

    @Before
    public void setUp() {
        doReturn(modelService).when(populator).getModelService();
    }

    @Test
    public void testPopulate_notAVariantProduct() {
        ProductModel source = ProductModelMockBuilder
                .aBaseProduct(CODE, Locale.CANADA)
                .build();

        ProductData target = mock(ProductData.class);


        populator.populate(source, target);

        verifyZeroInteractions(target);
    }

    @Test
    public void testPopulate_noDocuments() {
        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .build();

        ProductData target = new ProductData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());

        populator.populate(source, target);

        Assertions
                .assertThat(target.isDocumentsVisible())
                .isFalse();
    }


    @Test
    public void testHasDocuments_withDocs() {
        MediaModel media = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withDOP(media)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(media).when(populator).getLocalizedMedia(source, ProductModel.DOP);

        when(downloadsConverter.convert(media))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.isDocumentsVisible())
                .isTrue();

        Assertions
                .assertThat(target.getDOP())
                .isEqualTo(imageData);
    }


    @Test
    public void testPopulateInstructionManuals() {
        MediaModel instructionManual = mock(MediaModel.class);
        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withInstructionManuals(singletonList(instructionManual))
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(singletonList(instructionManual)).when(populator).getLocalizedMediaCollection(source, ProductModel.INSTRUCTIONMANUALS);

        when(downloadsConverter.convertAll(singletonList(instructionManual)))
                .thenReturn(singletonList(imageData));

        populator.populate(source, target);

        Assertions
                .assertThat(target.getManuals())
                .containsExactly(imageData);

        verify(downloadsConverter).convertAll(singletonList(instructionManual));
    }

    @Test
    public void testPopulateCertificates() {
        MediaModel certificates = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withCertificates(singletonList(certificates))
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(singletonList(certificates)).when(populator).getLocalizedMediaCollection(source, ProductModel.CERTIFICATES);


        when(downloadsConverter.convertAll(singletonList(certificates)))
                .thenReturn(singletonList(imageData));

        populator.populate(source, target);

        Assertions
                .assertThat(target.getCertificates())
                .containsExactly(imageData);

        verify(downloadsConverter).convertAll(singletonList(certificates));
    }

    @Test
    public void testPopulateGeneralManuals() {
        MediaModel generalManual = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withGeneralManuals(singletonList(generalManual))
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(singletonList(generalManual)).when(populator).getLocalizedMediaCollection(source, ProductModel.GENERALMANUALS);


        when(downloadsConverter.convertAll(singletonList(generalManual)))
                .thenReturn(singletonList(imageData));

        populator.populate(source, target);

        Assertions
                .assertThat(target.getManuals())
                .containsExactly(imageData);

        verify(downloadsConverter).convertAll(singletonList(generalManual));
    }

    @Test
    public void testPopulateUserManuals() {
        MediaModel userManuals = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withUserManuals(singletonList(userManuals))
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(singletonList(userManuals)).when(populator).getLocalizedMediaCollection(source, ProductModel.USERMANUALS);

        when(downloadsConverter.convertAll(singletonList(userManuals)))
                .thenReturn(singletonList(imageData));

        populator.populate(source, target);

        Assertions
                .assertThat(target.getManuals())
                .containsExactly(imageData);

        verify(downloadsConverter).convertAll(singletonList(userManuals));
    }

    @Test
    public void testPopulateMaintenanceManuals() {
        MediaModel maintenanceManuals = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withMaintenanceManuals(singletonList(maintenanceManuals))
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(singletonList(maintenanceManuals)).when(populator).getLocalizedMediaCollection(source, ProductModel.MAINTENANCEMANUALS);

        when(downloadsConverter.convertAll(singletonList(maintenanceManuals)))
                .thenReturn(singletonList(imageData));

        populator.populate(source, target);

        Assertions
                .assertThat(target.getManuals())
                .containsExactly(imageData);

        verify(downloadsConverter).convertAll(singletonList(maintenanceManuals));
    }

    @Test
    public void testPopulateDOP() {
        MediaModel DOP = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withDOP(DOP)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(DOP).when(populator).getLocalizedMedia(source, ProductModel.DOP);

        when(downloadsConverter.convert(DOP))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getDOP())
                .isEqualTo(imageData);

        verify(downloadsConverter).convert(DOP);
    }

    @Test
    public void testPopulateNormalisation() {
        MediaModel normalisation = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withNormalisation(normalisation)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(normalisation).when(populator).getLocalizedMedia(source, ProductModel.NORMALISATION);

        when(downloadsConverter.convert(normalisation))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getNormalisation())
                .isEqualTo(imageData);

        verify(downloadsConverter).convert(normalisation);
    }

    @Test
    public void testPopulateTechDataSheet() {
        MediaModel techDataSheet = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withTechDataSheet(techDataSheet)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(techDataSheet).when(populator).getLocalizedMedia(source, ProductModel.TECH_DATA_SHEET);

        when(downloadsConverter.convert(techDataSheet))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getTechnicalDataSheet())
                .isEqualTo(imageData);

        verify(downloadsConverter).convert(techDataSheet);
    }

    @Test
    public void testPopulateSafetyDataSheet() {
        MediaModel safetyDataSheet = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withSafetyDataSheet(safetyDataSheet)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(safetyDataSheet).when(populator).getLocalizedMedia(source, ProductModel.SAFETY_DATA_SHEET);

        when(downloadsConverter.convert(safetyDataSheet))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getSafetyDataSheet())
                .isEqualTo(imageData);

        verify(downloadsConverter).convert(safetyDataSheet);
    }

    @Test
    public void testPopulateProductSpecificationSheet() {
        MediaModel productSpecificationSheet = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withProductSpecificationSheet(productSpecificationSheet)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(productSpecificationSheet).when(populator).getLocalizedMedia(source, ProductModel.PRODUCT_SPECIFICATION_SHEET);

        when(downloadsConverter.convert(productSpecificationSheet))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getProductSpecificationSheet())
                .isEqualTo(imageData);

        verify(downloadsConverter).convert(productSpecificationSheet);
    }

    @Test
    public void testPopulateWarranty() {
        MediaModel warranty = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withWarranty(warranty)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(warranty).when(populator).getLocalizedMedia(source, ProductModel.WARRANTY);

        when(downloadsConverter.convert(warranty))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getWarranty())
                .isEqualTo(imageData);

        verify(downloadsConverter).convert(warranty);
    }

    @Test
    public void testPopulateSparePartsList() {
        MediaModel sparePartsList = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withSparePartsList(sparePartsList)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(sparePartsList).when(populator).getLocalizedMedia(source, ProductModel.SPARE_PARTS_LIST);

        when(downloadsConverter.convert(sparePartsList))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getSparePartsList())
                .isEqualTo(imageData);

        verify(downloadsConverter).convert(sparePartsList);
    }

    @Test
    public void testPopulateEcoDataSheet() {
        MediaModel ecoDataSheet = mock(MediaModel.class);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withEcoDataSheet(ecoDataSheet)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());
        doReturn(ecoDataSheet).when(populator).getLocalizedMedia(source, ProductModel.ECO_DATA_SHEET);

        when(downloadsConverter.convert(ecoDataSheet))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getEcoDataSheet())
                .isEqualTo(imageData);

        verify(downloadsConverter).convert(ecoDataSheet);
    }

    @Test
    public void testPopulateBrandCatalogs() {
        BrandCategoryModel brandCategory = mock(BrandCategoryModel.class);
        MediaModel brandCatalog = mock(MediaModel.class);

        when(brandCategory.getBrandCatalog()).thenReturn(brandCatalog);

        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .withBrandCatalogs(singletonList(brandCategory))
                .withSupercategories(brandCategory)
                .build();

        ProductData target = new ProductData();
        ImageData imageData = new ImageData();

        doReturn(emptyList()).when(populator).getLocalizedMediaCollection(any(ProductModel.class), anyString());
        doReturn(null).when(populator).getLocalizedMedia(any(ProductModel.class), anyString());

        when(downloadsConverter.convert(brandCatalog))
                .thenReturn(imageData);

        populator.populate(source, target);

        Assertions
                .assertThat(target.getBrandCatalogs())
                .isEqualTo(singletonList(imageData));

        verify(downloadsConverter).convert(brandCatalog);
    }

    @Test
    public void testGetLocalizedMedia_noFallback() {
        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .build();

        MediaModel media = mock(MediaModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE)).thenReturn(media);

        MediaModel result = populator.getLocalizedMedia(source, ATTRIBUTE);

        Assertions
                .assertThat(result)
                .isEqualTo(media);

        verify(i18NService).getCurrentLocale();
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE);
        verifyNoMoreInteractions(i18NService, modelService);
    }

    @Test
    public void testGetLocalizedMedia_firstFallback() {
        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .build();

        MediaModel media = mock(MediaModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(i18NService.getFallbackLocales(LOCALE_NL_BE)).thenReturn(FALLBACK_LOCALES);
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE))
                .thenReturn(null);
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL))
                .thenReturn(media);

        Assertions
                .assertThat(populator.getLocalizedMedia(source, ATTRIBUTE))
                .isEqualTo(media);

        verify(i18NService, times(2)).getCurrentLocale();
        verify(i18NService).getFallbackLocales(LOCALE_NL_BE);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL);
        verifyNoMoreInteractions(i18NService, modelService);
    }

    @Test
    public void testGetLocalizedMedia_secondFallback() {
        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .build();

        MediaModel media = mock(MediaModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(i18NService.getFallbackLocales(LOCALE_NL_BE)).thenReturn(FALLBACK_LOCALES);
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE))
                .thenReturn(null);
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL))
                .thenReturn(null);
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_EN))
                .thenReturn(media);

        Assertions
                .assertThat(populator.getLocalizedMedia(source, ATTRIBUTE))
                .isEqualTo(media);

        verify(i18NService, times(2)).getCurrentLocale();
        verify(i18NService).getFallbackLocales(LOCALE_NL_BE);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_EN);
        verifyNoMoreInteractions(i18NService, modelService);
    }

    @Test
    public void testGetLocalizedMediaCollection_noFallback() {
        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .build();

        MediaModel media = mock(MediaModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE)).thenReturn(singletonList(media));

        Assertions
                .assertThat(populator.getLocalizedMediaCollection(source, ATTRIBUTE))
                .containsOnly(media);

        verify(i18NService).getCurrentLocale();
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE);
        verifyNoMoreInteractions(i18NService, modelService);
    }

    @Test
    public void testGetLocalizedMediaCollection_firstFallback() {
        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .build();

        MediaModel media = mock(MediaModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(i18NService.getFallbackLocales(LOCALE_NL_BE)).thenReturn(FALLBACK_LOCALES);
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE))
                .thenReturn(emptyList());
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL))
                .thenReturn(singletonList(media));

        Assertions
                .assertThat(populator.getLocalizedMediaCollection(source, ATTRIBUTE))
                .containsOnly(media);

        verify(i18NService, times(2)).getCurrentLocale();
        verify(i18NService).getFallbackLocales(LOCALE_NL_BE);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL);
        verifyNoMoreInteractions(i18NService, modelService);
    }

    @Test
    public void testGetLocalizedMediaCollection_secondFallback() {
        ProductModel source = VariantProductModelMockBuilder
                .aVariantProduct(CODE, Locale.CANADA)
                .build();

        MediaModel media = mock(MediaModel.class);

        when(i18NService.getCurrentLocale()).thenReturn(LOCALE_NL_BE);
        when(i18NService.getFallbackLocales(LOCALE_NL_BE)).thenReturn(FALLBACK_LOCALES);
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE))
                .thenReturn(emptyList());
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_NL))
                .thenReturn(emptyList());
        when(modelService.getAttributeValue(source, ATTRIBUTE, LOCALE_EN))
                .thenReturn(singletonList(media));

        Assertions
                .assertThat(populator.getLocalizedMediaCollection(source, ATTRIBUTE))
                .containsOnly(media);

        verify(i18NService, times(2)).getCurrentLocale();
        verify(i18NService).getFallbackLocales(LOCALE_NL_BE);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL_BE);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_NL);
        verify(modelService).getAttributeValue(source, ATTRIBUTE, LOCALE_EN);
        verifyNoMoreInteractions(i18NService, modelService);
    }
}