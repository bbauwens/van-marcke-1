package com.vanmarcke.facades.storesession.impl;

import com.vanmarcke.facades.search.VMKSearchFacade;
import com.vanmarcke.facades.site.data.BaseSiteData;
import com.vanmarcke.services.cms.VMKCmsSiteService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storesession.VMKStoreSessionService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKStoreSessionFacadeImplTest {

    @Mock
    private VMKStoreSessionService storeSessionService;
    @Mock
    private VMKCmsSiteService cmsSiteService;
    @Mock
    private VMKSearchFacade<PointOfServiceData> pointOfServiceSearchFacade;
    @Mock
    private VMKPointOfServiceService pointOfServiceService;

    private Converter<BaseSiteModel, BaseSiteData> siteConverter;
    private Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;

    private VMKStoreSessionFacadeImpl vmkStoreSessionFacade;

    @Before
    public void setUp() {
        siteConverter = mock(Converter.class);
        pointOfServiceConverter = mock(Converter.class);

        vmkStoreSessionFacade = new VMKStoreSessionFacadeImpl(cmsSiteService,
                siteConverter,
                pointOfServiceSearchFacade,
                pointOfServiceService,
                pointOfServiceConverter);

        vmkStoreSessionFacade.setStoreSessionService(storeSessionService);
    }

    @Test
    public void testGetAllSites() {
        List<CMSSiteModel> allSites = new ArrayList<>();
        List<BaseSiteData> convertedSites = new ArrayList<>();
        CMSSiteModel cmsSite = mock(CMSSiteModel.class);
        BaseSiteData baseSiteData = mock(BaseSiteData.class);
        allSites.add(cmsSite);
        convertedSites.add(baseSiteData);

        when(cmsSiteService.getSupportedSites(SiteChannel.B2B)).thenReturn(allSites);
        when(siteConverter.convertAll(allSites)).thenReturn(convertedSites);
        Collection<BaseSiteData> result = vmkStoreSessionFacade.getAllSites();

        assertThat(result).isNotNull().isNotEmpty().hasSize(1);
    }

    @Test
    public void testGetAllSites_noSites() {
        Collection<BaseSiteData> result = vmkStoreSessionFacade.getAllSites();

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void testGetCurrentSite() {
        CMSSiteModel baseSite = mock(CMSSiteModel.class);
        BaseSiteData baseSiteData = mock(BaseSiteData.class);

        when(cmsSiteService.getCurrentSite()).thenReturn(baseSite);
        when(siteConverter.convert(baseSite)).thenReturn(baseSiteData);
        BaseSiteData result = vmkStoreSessionFacade.getCurrentSite();

        assertThat(result).isNotNull();
    }

    @Test
    public void testSetCurrentSite() {
        vmkStoreSessionFacade.setCurrentSite("current-site-uid");

        verify(storeSessionService).setCurrentSite("current-site-uid");
    }

    @Test
    public void testGetAllStores() throws Exception {
        PointOfServiceData posData = mock(PointOfServiceData.class);

        when(pointOfServiceSearchFacade.search()).thenReturn(singletonList(posData));

        List<PointOfServiceData> result = vmkStoreSessionFacade.getAllStores();
        assertThat(result).containsExactly(posData);
    }

    @Test
    public void testGetCurrentStore() {
        PointOfServiceModel pointOfServiceModel = mock(PointOfServiceModel.class);
        PointOfServiceData pointOfServiceData = mock(PointOfServiceData.class);

        when(pointOfServiceService.getCurrentPointOfService()).thenReturn(pointOfServiceModel);

        when(pointOfServiceConverter.convert(pointOfServiceModel)).thenReturn(pointOfServiceData);

        PointOfServiceData result = vmkStoreSessionFacade.getCurrentStore();
        assertThat(result).isEqualTo(pointOfServiceData);
    }

    @Test
    public void testGetCurrentWhenNotSet() {
        PointOfServiceData result = vmkStoreSessionFacade.getCurrentStore();
        assertThat(result).isEqualTo(null);
    }

    @Test
    public void testSetCurrentStore() {
        vmkStoreSessionFacade.setCurrentStore("store");

        verify(storeSessionService).setCurrentStore("store");
    }
}