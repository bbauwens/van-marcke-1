package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.services.category.VMKCategoryService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCategoryPopulatorTest {

    private static final String MEDIA_FORMAT = "THUMBNAIL";
    private static final String CATEGORY = "category";
    private static final String BRAND = "brand";

    @Mock
    private Converter<CategoryModel, CategoryData> categoryConverter;
    @Mock
    private Converter<MediaModel, ImageData> imageConverter;
    @Mock
    private ProductService productService;
    @Mock
    private VMKCategoryService categoryService;

    @InjectMocks
    private VMKCategoryPopulator blueCategoryPopulator;

    @Before
    public void setup() {
        blueCategoryPopulator = new VMKCategoryPopulator(categoryConverter, imageConverter, productService, categoryService);
    }

    @Test
    public void testPopulate_withoutSubCategories() {
        BrandCategoryModel parent = mock(BrandCategoryModel.class);
        MediaModel thumbnail = mock(MediaModel.class);

        when(categoryService.getLocalizedBrandLogoForFormat(parent, MEDIA_FORMAT)).thenReturn(Optional.of(thumbnail));

        CategoryData result = new CategoryData();
        this.blueCategoryPopulator.populate(parent, result);

        assertThat(result.getChildren()).isEmpty();

        verify(this.categoryConverter).convert(parent, result);
    }

    @Test
    public void testPopulate_childHasProductsForCategory() {
        BrandCategoryModel parent = mock(BrandCategoryModel.class);
        BrandCategoryModel child = mock(BrandCategoryModel.class);
        MediaModel thumbnail = mock(MediaModel.class);
        ProductModel product = mock(ProductModel.class);

        when(parent.getCategories()).thenReturn(singletonList(child));
        when(productService.containsProductsForCategory(child)).thenReturn(true);

        when(categoryService.getLocalizedBrandLogoForFormat(parent, MEDIA_FORMAT)).thenReturn(Optional.of(thumbnail));
        when(categoryService.getLocalizedBrandLogoForFormat(child, MEDIA_FORMAT)).thenReturn(Optional.of(thumbnail));

        CategoryData result = new CategoryData();
        this.blueCategoryPopulator.populate(parent, result);

        assertThat(result.getChildren()).isNotEmpty();
        assertThat(result.getCategoryType()).isEqualTo(BRAND);

        verify(this.categoryConverter).convert(parent, result);
    }

    @Test
    public void testPopulate_childHasNoProductsForCategory() {
        BrandCategoryModel parent = mock(BrandCategoryModel.class);
        BrandCategoryModel child = mock(BrandCategoryModel.class);
        MediaModel thumbnail = mock(MediaModel.class);
        CategoryData result = new CategoryData();

        when(parent.getCategories()).thenReturn(singletonList(child));
        when(productService.containsProductsForCategory(child)).thenReturn(false);

        when(categoryService.getLocalizedBrandLogoForFormat(parent, MEDIA_FORMAT)).thenReturn(Optional.of(thumbnail));

        this.blueCategoryPopulator.populate(parent, result);

        assertThat(result.getChildren()).isEmpty();

        verify(this.categoryConverter).convert(parent, result);
    }

    @Test
    public void testPopulate_withoutThumbnail() {
        BrandCategoryModel parent = mock(BrandCategoryModel.class);
        BrandCategoryModel child = mock(BrandCategoryModel.class);
        ProductModel product = mock(ProductModel.class);

        when(parent.getCategories()).thenReturn(singletonList(child));
        when(productService.containsProductsForCategory(child)).thenReturn(true);

        CategoryData childData = mock(CategoryData.class);
        when(this.categoryConverter.convert(child)).thenReturn(childData);

        when(categoryService.getLocalizedBrandLogoForFormat(parent, MEDIA_FORMAT)).thenReturn(Optional.empty());
        when(categoryService.getLocalizedBrandLogoForFormat(child, MEDIA_FORMAT)).thenReturn(Optional.empty());

        CategoryData result = new CategoryData();
        this.blueCategoryPopulator.populate(parent, result);

        assertThat(result.getChildren()).isNotEmpty();

        verify(this.categoryConverter).convert(parent, result);

        verifyZeroInteractions(this.imageConverter);
    }

    @Test
    public void testPopulate_childHasProductsForCategory_Category() {
        CategoryModel parent = mock(CategoryModel.class);
        CategoryModel child = mock(CategoryModel.class);
        MediaModel picture = mock(MediaModel.class);

        when(parent.getPicture()).thenReturn(picture);
        when(child.getPicture()).thenReturn(picture);

        when(parent.getCategories()).thenReturn(singletonList(child));
        when(productService.containsProductsForCategory(child)).thenReturn(true);

        ImageData pictureData = new ImageData();
        when(imageConverter.convert(picture)).thenReturn(pictureData);

        CategoryData result = new CategoryData();
        blueCategoryPopulator.populate(parent, result);

        assertThat(result.getChildren()).isNotEmpty();
        assertThat(result.getImage()).isEqualTo(pictureData);
        assertThat(result.getCategoryType()).isEqualTo(CATEGORY);

        verify(categoryConverter).convert(parent, result);
        verifyZeroInteractions(categoryService);
    }
}