package com.vanmarcke.facades.order.impl;

import com.vanmarcke.facades.data.order.SavedCartData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKB2BOrderFacadeImplTest} class contains the unit tests for the {@link VMKB2BOrderFacadeImpl}
 * class.
 *
 * @author Cristi Stoica
 * @since 16-05-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKB2BOrderFacadeImplTest {

    @Mock
    private Converter<AbstractOrderModel, SavedCartData> savedCartConverter;

    @Mock
    private CustomerAccountService customerAccountService;

    @Mock
    private BaseStoreService baseStoreService;

    @InjectMocks
    private VMKB2BOrderFacadeImpl vmkb2BOrderFacade;

    @Captor
    private ArgumentCaptor<AbstractOrderModel> abstractOrderArgumentCaptor;

    @Before
    public void setup() {
        vmkb2BOrderFacade.setBaseStoreService(baseStoreService);
        vmkb2BOrderFacade.setCustomerAccountService(customerAccountService);
    }

    @Test
    public void testOrderConvertedToSavedCart() {
        final BaseStoreModel store = mock(BaseStoreModel.class);
        final String orderGuid = "test";
        final OrderModel order = mock(OrderModel.class);

        when(baseStoreService.getCurrentBaseStore()).thenReturn(store);
        when(customerAccountService.getOrderDetailsForGUID(orderGuid, store)).thenReturn(order);

        vmkb2BOrderFacade.getOrderDetailsForExport(orderGuid);
        verify(savedCartConverter).convert(abstractOrderArgumentCaptor.capture());
        assertThat(abstractOrderArgumentCaptor.getValue()).isEqualTo(order);
    }
}