package com.vanmarcke.facades.address.converters.populators;

import com.vanmarcke.cpi.data.ptv.ClassificationDescriptionEnum;
import com.vanmarcke.cpi.data.ptv.PTVResponseData;
import com.vanmarcke.cpi.data.ptv.ResultAddressData;
import com.vanmarcke.facades.addressvalidation.data.AddressValidationResponseData;
import com.vanmarcke.services.model.builder.CountryModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.assertj.core.util.Lists;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@code PTVAddressResponsePopulatorTest} class contains the unit tests for the {@link VMKPTVAddressResponsePopulator}
 * class.
 *
 * @author Christiaan Janssen
 * @since 07/02/2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPTVAddressResponsePopulatorTest {

    private static final Integer ERROR_CODE = RandomUtils.nextInt();
    private static final String ERROR_DESCRIPTION = RandomStringUtils.random(10);

    private static final String COUNTRY_ISO = RandomStringUtils.random(2);
    private static final String TOWN = RandomStringUtils.random(10);
    private static final String TOWN2 = RandomStringUtils.random(10);
    private static final String POSTAL_CODE = RandomStringUtils.random(10);
    private static final String LINE_1 = RandomStringUtils.random(10);
    private static final String LINE_2 = RandomStringUtils.random(10);

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private Converter<CountryModel, CountryData> countryConverter;

    @InjectMocks
    private VMKPTVAddressResponsePopulator ptvAddressResponsePopulator;

    @Test
    public void testPopulateWithoutExactMatch() {
        ResultAddressData firstAddress = new ResultAddressData();
        firstAddress.setCountry(COUNTRY_ISO);
        firstAddress.setCity(TOWN);
        firstAddress.setPostCode(POSTAL_CODE);

        ResultAddressData secondAddress = new ResultAddressData();
        secondAddress.setCountry(COUNTRY_ISO);
        secondAddress.setCity(TOWN);
        secondAddress.setCity2(TOWN2);
        secondAddress.setPostCode(POSTAL_CODE);
        secondAddress.setStreet(LINE_1);
        secondAddress.setHouseNumber(LINE_2);
        secondAddress.setTotalScore(98);

        PTVResponseData source = new PTVResponseData();
        source.setErrorCode(ERROR_CODE);
        source.setErrorDescription(ERROR_DESCRIPTION);
        source.setResultList(Lists.newArrayList(firstAddress, secondAddress));

        CountryModel countryModel = CountryModelMockBuilder
                .aCountry()
                .build();

        CountryData countryData = new CountryData();

        when(commonI18NService.getCountry(COUNTRY_ISO)).thenReturn(countryModel);
        when(countryConverter.convert(countryModel)).thenReturn(countryData);

        AddressValidationResponseData target = new AddressValidationResponseData();

        ptvAddressResponsePopulator.populate(source, target);

        Assertions
                .assertThat(target.getErrorCode())
                .isEqualTo(ERROR_CODE);

        Assertions
                .assertThat(target.getErrorDescription())
                .isEqualTo(ERROR_DESCRIPTION);

        Assertions
                .assertThat(target.getResultList())
                .hasSize(1);

        AddressData targetAddress = target.getResultList().get(0);

        Assertions
                .assertThat(targetAddress.getCountry())
                .isEqualTo(countryData);

        Assertions
                .assertThat(targetAddress.getTown())
                .isEqualTo(TOWN);

        Assertions
                .assertThat(targetAddress.getTown2())
                .isEqualTo(TOWN2);

        Assertions
                .assertThat(targetAddress.getPostalCode())
                .isEqualTo(POSTAL_CODE);

        Assertions
                .assertThat(targetAddress.getLine1())
                .isEqualTo(LINE_1);

        Assertions
                .assertThat(targetAddress.getLine2())
                .isEqualTo(LINE_2);

        Assertions
                .assertThat(target.isExactMatch())
                .isFalse();

        verify(commonI18NService).getCountry(COUNTRY_ISO);
        verify(countryConverter).convert(countryModel);
    }

    @Test
    public void testPopulateWithExactMatch_EXACT() {
        ResultAddressData firstAddress = new ResultAddressData();
        firstAddress.setCountry(COUNTRY_ISO);
        firstAddress.setCity(TOWN);
        firstAddress.setCity2(TOWN2);
        firstAddress.setPostCode(POSTAL_CODE);
        firstAddress.setStreet(LINE_1);
        firstAddress.setHouseNumber(LINE_2);
        firstAddress.setTotalScore(100);
        firstAddress.setClassificationDescription(ClassificationDescriptionEnum.EXACT);

        PTVResponseData source = new PTVResponseData();
        source.setErrorCode(ERROR_CODE);
        source.setErrorDescription(ERROR_DESCRIPTION);
        source.setResultList(Lists.newArrayList(firstAddress));

        CountryModel countryModel = CountryModelMockBuilder
                .aCountry()
                .build();

        CountryData countryData = new CountryData();

        when(commonI18NService.getCountry(COUNTRY_ISO)).thenReturn(countryModel);
        when(countryConverter.convert(countryModel)).thenReturn(countryData);

        AddressValidationResponseData target = new AddressValidationResponseData();

        ptvAddressResponsePopulator.populate(source, target);

        Assertions
                .assertThat(target.getErrorCode())
                .isEqualTo(ERROR_CODE);

        Assertions
                .assertThat(target.getErrorDescription())
                .isEqualTo(ERROR_DESCRIPTION);

        Assertions
                .assertThat(target.getResultList())
                .hasSize(1);

        AddressData targetAddress = target.getResultList().get(0);

        Assertions
                .assertThat(targetAddress.getCountry())
                .isEqualTo(countryData);

        Assertions
                .assertThat(targetAddress.getTown())
                .isEqualTo(TOWN);

        Assertions
                .assertThat(targetAddress.getTown2())
                .isEqualTo(TOWN2);

        Assertions
                .assertThat(targetAddress.getPostalCode())
                .isEqualTo(POSTAL_CODE);

        Assertions
                .assertThat(targetAddress.getLine1())
                .isEqualTo(LINE_1);

        Assertions
                .assertThat(targetAddress.getLine2())
                .isEqualTo(LINE_2);

        Assertions
                .assertThat(target.isExactMatch())
                .isTrue();

        verify(commonI18NService).getCountry(COUNTRY_ISO);
        verify(countryConverter).convert(countryModel);
    }

    @Test
    public void testPopulateWithExactMatch_UNIQUE() {
        ResultAddressData firstAddress = new ResultAddressData();
        firstAddress.setCountry(COUNTRY_ISO);
        firstAddress.setCity(TOWN);
        firstAddress.setCity2(TOWN2);
        firstAddress.setPostCode(POSTAL_CODE);
        firstAddress.setStreet(LINE_1);
        firstAddress.setHouseNumber(LINE_2);
        firstAddress.setTotalScore(100);
        firstAddress.setClassificationDescription(ClassificationDescriptionEnum.UNIQUE);

        PTVResponseData source = new PTVResponseData();
        source.setErrorCode(ERROR_CODE);
        source.setErrorDescription(ERROR_DESCRIPTION);
        source.setResultList(Lists.newArrayList(firstAddress));

        CountryModel countryModel = CountryModelMockBuilder
                .aCountry()
                .build();

        CountryData countryData = new CountryData();

        when(commonI18NService.getCountry(COUNTRY_ISO)).thenReturn(countryModel);
        when(countryConverter.convert(countryModel)).thenReturn(countryData);

        AddressValidationResponseData target = new AddressValidationResponseData();

        ptvAddressResponsePopulator.populate(source, target);

        Assertions
                .assertThat(target.getErrorCode())
                .isEqualTo(ERROR_CODE);

        Assertions
                .assertThat(target.getErrorDescription())
                .isEqualTo(ERROR_DESCRIPTION);

        Assertions
                .assertThat(target.getResultList())
                .hasSize(1);

        AddressData targetAddress = target.getResultList().get(0);

        Assertions
                .assertThat(targetAddress.getCountry())
                .isEqualTo(countryData);

        Assertions
                .assertThat(targetAddress.getTown())
                .isEqualTo(TOWN);

        Assertions
                .assertThat(targetAddress.getTown2())
                .isEqualTo(TOWN2);

        Assertions
                .assertThat(targetAddress.getPostalCode())
                .isEqualTo(POSTAL_CODE);

        Assertions
                .assertThat(targetAddress.getLine1())
                .isEqualTo(LINE_1);

        Assertions
                .assertThat(targetAddress.getLine2())
                .isEqualTo(LINE_2);

        Assertions
                .assertThat(target.isExactMatch())
                .isTrue();

        verify(commonI18NService).getCountry(COUNTRY_ISO);
        verify(countryConverter).convert(countryModel);
    }

    @Test
    public void testPopulateWithMultipleAddresses_WithExactMatch() {
        ResultAddressData firstAddress = new ResultAddressData();
        firstAddress.setCountry(COUNTRY_ISO);
        firstAddress.setCity(TOWN);
        firstAddress.setCity2(TOWN2);
        firstAddress.setPostCode(POSTAL_CODE);
        firstAddress.setStreet(LINE_1);
        firstAddress.setHouseNumber(LINE_2);
        firstAddress.setTotalScore(100);
        firstAddress.setClassificationDescription(ClassificationDescriptionEnum.EXACT);

        ResultAddressData secondAddress = new ResultAddressData();
        secondAddress.setCountry(COUNTRY_ISO);
        secondAddress.setCity(TOWN);
        secondAddress.setCity2(TOWN2);
        secondAddress.setPostCode(POSTAL_CODE);
        secondAddress.setStreet(LINE_1);
        secondAddress.setHouseNumber(LINE_2);
        secondAddress.setTotalScore(98);

        PTVResponseData source = new PTVResponseData();
        source.setErrorCode(ERROR_CODE);
        source.setErrorDescription(ERROR_DESCRIPTION);
        source.setResultList(Lists.newArrayList(firstAddress, secondAddress));

        CountryModel countryModel = CountryModelMockBuilder
                .aCountry()
                .build();

        CountryData countryData = new CountryData();

        when(commonI18NService.getCountry(COUNTRY_ISO)).thenReturn(countryModel);
        when(countryConverter.convert(countryModel)).thenReturn(countryData);

        AddressValidationResponseData target = new AddressValidationResponseData();

        ptvAddressResponsePopulator.populate(source, target);

        Assertions
                .assertThat(target.getErrorCode())
                .isEqualTo(ERROR_CODE);

        Assertions
                .assertThat(target.getErrorDescription())
                .isEqualTo(ERROR_DESCRIPTION);

        Assertions
                .assertThat(target.getResultList())
                .hasSize(1);

        AddressData targetAddress = target.getResultList().get(0);

        Assertions
                .assertThat(targetAddress.getCountry())
                .isEqualTo(countryData);

        Assertions
                .assertThat(targetAddress.getTown())
                .isEqualTo(TOWN);

        Assertions
                .assertThat(targetAddress.getTown2())
                .isEqualTo(TOWN2);

        Assertions
                .assertThat(targetAddress.getPostalCode())
                .isEqualTo(POSTAL_CODE);

        Assertions
                .assertThat(targetAddress.getLine1())
                .isEqualTo(LINE_1);

        Assertions
                .assertThat(targetAddress.getLine2())
                .isEqualTo(LINE_2);

        Assertions
                .assertThat(target.isExactMatch())
                .isTrue();

        verify(commonI18NService).getCountry(COUNTRY_ISO);
        verify(countryConverter).convert(countryModel);
    }
}