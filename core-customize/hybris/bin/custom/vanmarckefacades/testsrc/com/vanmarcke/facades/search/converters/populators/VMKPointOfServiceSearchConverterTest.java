package com.vanmarcke.facades.search.converters.populators;

import com.vanmarcke.facades.search.converters.VMKPointOfServiceSearchConverter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.solrfacetsearch.search.impl.SolrResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPointOfServiceSearchConverterTest extends AbstractSearchConverterTest {

    @InjectMocks
    private VMKPointOfServiceSearchConverter vmkPointOfServiceSearchConverter;

    @Test
    public void testConvert() {
        SolrResult source = mockSorlResult();

        when(document.getFirstValue("displayName_text")).thenReturn("display-name");
        when(document.getFirstValue("name_text")).thenReturn("name");
        when(document.getFirstValue("phone1_string")).thenReturn("phone1");

        PointOfServiceData result = vmkPointOfServiceSearchConverter.convert(source);

        assertThat(result.getDisplayName()).isEqualTo("display-name");
        assertThat(result.getName()).isEqualTo("name");
        assertThat(result.getAddress()).isNotNull();
        assertThat(result.getAddress().getPhone()).isEqualTo("phone1");
    }

}