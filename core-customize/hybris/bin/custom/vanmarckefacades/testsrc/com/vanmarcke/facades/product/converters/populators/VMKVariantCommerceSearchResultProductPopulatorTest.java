package com.vanmarcke.facades.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ProductStockStatus;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKVariantCommerceSearchResultProductPopulatorTest {

    @InjectMocks
    private VMKVariantCommerceSearchResultProductPopulator populator;

    @Test
    public void testPopulate_emptySource() {
        SearchResultValueData source = mock(SearchResultValueData.class);
        ProductData target = new ProductData();

        when(source.getVariants()).thenReturn(null);
        populator.populate(source, target);

        assertThat(target.getName()).isNull();
        assertThat(target.getNumberOfVariants()).isZero();
    }

    @Test
    public void testPopulate_nonEmptySource_emptyBaseProductName() {
        SearchResultValueData source = mock(SearchResultValueData.class);
        ProductData target = new ProductData();
        Map<String, Object> values = new HashMap<>();
        values.put("name", "variantName");

        when(source.getValues()).thenReturn(values);
        populator.populate(source, target);

        assertThat(target.getBaseProductName()).isEqualTo("variantName");
        assertThat(target.getNumberOfVariants()).isEqualTo(1);
    }

    @Test
    public void testPopulate_nonEmptySource_emptyValues() {
        SearchResultValueData source = mock(SearchResultValueData.class);
        ProductData target = new ProductData();
        Map<String, Object> values = new HashMap<>();
        values.put("baseProductUrl", "baseProductUrl");

        when(source.getValues()).thenReturn(values);
        populator.populate(source, target);

        assertThat(target.getUrl()).isNull();
        assertThat(target.getNumberOfVariants()).isEqualTo(1);
//        assertFalse(target.getHasAvailableDate());
    }

    @Test
    public void testPopulate_nonEmptySource_baseProductUrlWithVariants() {
        SearchResultValueData source = mock(SearchResultValueData.class);
        ProductData target = new ProductData();
        Map<String, Object> values = new HashMap<>();
        values.put("baseProductUrl", "baseProductUrl");

        when(source.getValues()).thenReturn(values);
        when(source.getVariants()).thenReturn(Collections.singletonList(mock(SearchResultValueData.class)));
        populator.populate(source, target);

        assertThat(target.getUrl()).isEqualTo("baseProductUrl");
        assertThat(target.getNumberOfVariants()).isEqualTo(2);
    }

    @Test
    public void testPopulate_nonEmptySource_withValues() {
        SearchResultValueData source = mock(SearchResultValueData.class);
        ProductData target = new ProductData();
        Map<String, Object> values = new HashMap<>();
        values.put("baseProductName", "baseProductName");
        values.put("inStockStatus", "LIMITED_STOCK");
        when(source.getValues()).thenReturn(values);
        when(source.getVariants()).thenReturn(Collections.singletonList(mock(SearchResultValueData.class)));
        populator.populate(source, target);

        assertThat(target.getBaseProductName()).isEqualTo("baseProductName");
        assertThat(target.getNumberOfVariants()).isEqualTo(2);
        assertEquals(ProductStockStatus.LIMITED_STOCK, target.getStockStatus());
    }
}