package com.vanmarcke.facades.address.validation.impl;

import com.vanmarcke.cpi.data.ptv.PTVAddressData;
import com.vanmarcke.cpi.data.ptv.PTVResponseData;
import com.vanmarcke.cpi.services.PTVXServerService;
import com.vanmarcke.facades.addressvalidation.data.AddressValidationResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAddressValidationFacadeImplTest {

    private static final String TOWN_NL = RandomStringUtils.random(10);
    private static final String TOWN_FR = RandomStringUtils.random(10);

    @Mock
    private PTVXServerService validationService;

    @Mock
    private Converter<AddressData, PTVAddressData> addressDataConverter;

    @Mock
    private Converter<PTVResponseData, AddressValidationResponseData> addressResponseDataConverter;

    @InjectMocks
    private VMKAddressValidationFacadeImpl addressValidationFacade;

    @Test
    public void testValidate_verifyFindAddressCall() {
        AddressData address = new AddressData();
        address.setTown(TOWN_FR);

        PTVAddressData ptvAddressData = new PTVAddressData();

        PTVResponseData ptvResponseData = new PTVResponseData();

        AddressData validatedAddress = new AddressData();
        validatedAddress.setTown(TOWN_NL);
        validatedAddress.setTown2(TOWN_FR);

        AddressValidationResponseData addressValidationResponseData = new AddressValidationResponseData();
        addressValidationResponseData.setResultList(Collections.singletonList(validatedAddress));

        when(addressDataConverter.convert(address)).thenReturn(ptvAddressData);

        when(validationService.findAddress(ptvAddressData)).thenReturn(ptvResponseData);

        when(addressResponseDataConverter.convert(ptvResponseData)).thenReturn(addressValidationResponseData);

        AddressValidationResponseData result = addressValidationFacade.validateAddress(address);

        Assertions
                .assertThat(result)
                .isEqualTo(addressValidationResponseData);

        Assertions
                .assertThat(address.getTown())
                .isEqualTo(TOWN_FR);
    }

}