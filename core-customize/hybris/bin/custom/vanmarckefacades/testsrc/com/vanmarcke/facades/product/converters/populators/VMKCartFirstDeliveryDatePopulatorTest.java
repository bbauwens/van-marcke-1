package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingEntryInfoResponseData;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.delivery.VMKFirstDateAvailabilityService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKCartFirstDeliveryDatePopulatorTest} class contains the unit tests for the {@link VMKCartFirstDeliveryDatePopulator}
 * class.
 *
 * @author Niels Raemaekers
 * @since 09-04-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartFirstDeliveryDatePopulatorTest {

    final String TEST_PRODUCT_NUMBER_1 = "ABC_001";
    final String TEST_PRODUCT_NUMBER_2 = "ABC_002";

    @Mock
    private VMKDeliveryInfoService deliveryInfoService;
    @Mock
    private CartModel cart;
    @Mock
    private ShippingDateInfoResponseData response;
    @Mock
    private ShippingEntryInfoResponseData responseResult;
    @Mock
    private VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService;
    @Mock
    private UserService userService;

    @InjectMocks
    private VMKCartFirstDeliveryDatePopulator populator;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        UserModel currentUser = mock(UserModel.class);
        when(userService.getCurrentUser()).thenReturn(currentUser);
        when(userService.isAnonymousUser(currentUser)).thenReturn(false);
    }

    @Test
    public void testPopulate_responseNull() {
        CartData cartData = setupCartData();

        when(deliveryInfoService.getShippingDateInformation(cart)).thenReturn(Optional.empty());
        populator.populate(cart, cartData);

        OrderEntryData result = cartData.getEntries().get(0);
        assertThat(result.getFirstPossibleDate()).isNull();
    }

    @Test
    public void testPopulate() {
        CartData cartData = setupCartData();
        Date deliveryDate = mock(Date.class);
        UserModel currentUser = mock(UserModel.class);

        when(deliveryInfoService.getShippingDateInformation(cart)).thenReturn(Optional.of(response));
        when(response.getResults()).thenReturn(Collections.singletonList(responseResult));

        when(responseResult.getProductNumber()).thenReturn(TEST_PRODUCT_NUMBER_1);
        when(responseResult.getDeliveryDate()).thenReturn(deliveryDate);
        when(vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(deliveryDate)).thenReturn(deliveryDate);
        populator.populate(cart, cartData);

        OrderEntryData result = cartData.getEntries().get(0);
        assertThat(result.getFirstPossibleDate()).isNotNull();
        assertThat(result.getFirstPossibleDate()).isEqualTo(deliveryDate);
    }

    @Test
    public void testPopulateWhenShippingResponseWithoutDeliveryDate() {
        //given
        CartData cartData = setupCartData();
        ShippingDateInfoResponseData shippingInfoResponse = new ShippingDateInfoResponseData();
        ShippingEntryInfoResponseData entry1 = new ShippingEntryInfoResponseData();
        entry1.setProductNumber(TEST_PRODUCT_NUMBER_1);
        entry1.setDeliveryDate(new Date());
        ShippingEntryInfoResponseData entry2 = new ShippingEntryInfoResponseData();
        entry1.setProductNumber(TEST_PRODUCT_NUMBER_2);
        entry1.setDeliveryDate(null);
        shippingInfoResponse.setResults(List.of(entry1, entry2));

        when(deliveryInfoService.getShippingDateInformation(cart)).thenReturn(Optional.of(shippingInfoResponse));
        when(vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(any(Date.class))).then(AdditionalAnswers.returnsFirstArg());

        //when
        Throwable thrown = catchThrowable(() -> {
            populator.populate(cart, cartData);
        });

        //then
        assertThat(thrown).isNull();
    }

    @Test
    public void testPopulateWhenShippingResponseWithNullResults() {
        //given
        CartData cartData = setupCartData();
        ShippingDateInfoResponseData shippingInfoResponse = new ShippingDateInfoResponseData();
        shippingInfoResponse.setResults(null);

        when(deliveryInfoService.getShippingDateInformation(cart)).thenReturn(Optional.of(shippingInfoResponse));
        when(vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(any(Date.class))).then(AdditionalAnswers.returnsFirstArg());

        //when
        Throwable thrown = catchThrowable(() -> {
            populator.populate(cart, cartData);
        });

        //then
        assertThat(thrown).isNull();
    }

    protected CartData setupCartData() {
        CartData cartData = new CartData();
        OrderEntryData entry = new OrderEntryData();
        ProductData product = mock(ProductData.class);
        entry.setProduct(product);

        cartData.setEntries(Collections.singletonList(entry));
        when(product.getCode()).thenReturn(TEST_PRODUCT_NUMBER_1);

        return cartData;
    }
}
