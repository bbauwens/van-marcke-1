package com.vanmarcke.facades.principal.user.impl;

import com.vanmarcke.services.b2b.VMKSSOUserService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKUserFacadeImplTest {

    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private UserService userService;
    @Mock
    private ModelService modelService;
    @Mock
    private VMKPointOfServiceService pointOfServiceService;
    @Mock
    private CartService cartService;
    @Mock
    private CommerceCartService commerceCartService;
    @Mock
    private VMKSSOUserService vmkssoUserService;

    @InjectMocks
    private VMKUserFacadeImpl vmkUserFacade;

    @Before
    public void setup() {
        vmkUserFacade.setUserService(userService);
        vmkUserFacade.setModelService(modelService);
        vmkUserFacade.setCartService(cartService);
    }

    @Captor
    private ArgumentCaptor<CommerceCartParameter> parameterArgumentCaptor;

    @Test
    public void testSyncSessionSite() {
        UserModel user = mock(UserModel.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);

        when(this.userService.getCurrentUser()).thenReturn(user);
        when(this.baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        this.vmkUserFacade.syncSessionSite();

        verify(this.modelService).save(user);
    }

    @Test
    public void testSyncSessionStore() {
        UserModel user = mock(UserModel.class);
        PointOfServiceModel store = mock(PointOfServiceModel.class);

        when(this.userService.getCurrentUser()).thenReturn(user);
        when(this.pointOfServiceService.getCurrentPointOfService()).thenReturn(store);
        this.vmkUserFacade.syncSessionStore();

        verify(this.modelService).save(user);
    }

    @Test
    public void testSaveNetPricePreference() throws Exception {
        final UserModel user = mock(UserModel.class);
        CartModel cartModel = mock(CartModel.class);

        when(this.userService.getCurrentUser()).thenReturn(user);

        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cartModel);

        this.vmkUserFacade.saveNetPricePreference(true);

        verify(modelService).save(user);
        verify(modelService).refresh(user);

        verify(commerceCartService).recalculateCart(parameterArgumentCaptor.capture());

        CommerceCartParameter parameterValue = parameterArgumentCaptor.getValue();
        assertThat(parameterValue.getCart()).isEqualTo(cartModel);
        assertThat(parameterValue.isEnableHooks()).isTrue();
    }

    @Test
    public void testSaveNetPricePreference_withCalculationException() throws Exception {
        final UserModel user = mock(UserModel.class);
        CartModel cartModel = mock(CartModel.class);

        when(user.getShowNetPrice()).thenReturn(true);

        when(this.userService.getCurrentUser()).thenReturn(user);

        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cartModel);

        doThrow(CalculationException.class).when(commerceCartService).recalculateCart(parameterArgumentCaptor.capture());

        this.vmkUserFacade.saveNetPricePreference(false);

        verify(modelService).save(user);
        verify(modelService).refresh(user);

        CommerceCartParameter parameterValue = parameterArgumentCaptor.getValue();
        assertThat(parameterValue.getCart()).isEqualTo(cartModel);
        assertThat(parameterValue.isEnableHooks()).isTrue();
    }

    @Test
    public void testGetNearestStoreForB2BCustomer_withoutBaseStore() {
        B2BCustomerModel customer = mock(B2BCustomerModel.class);
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        AddressModel billingAddress = mock(AddressModel.class);
        PointOfServiceModel pointOfService = mock(PointOfServiceModel.class);

        when(this.userService.getCurrentUser()).thenReturn(customer);
        when(customer.getDefaultB2BUnit()).thenReturn(b2BUnit);
        when(b2BUnit.getBillingAddress()).thenReturn(billingAddress);
        when(vmkssoUserService.getNearestStore(customer)).thenReturn(pointOfService);
        PointOfServiceModel result = vmkUserFacade.getNearestStoreForB2BCustomer();

        assertThat(result).isEqualTo(pointOfService);
        verify(userService).getCurrentUser();
        verify(vmkssoUserService).getNearestStore(customer);
    }

    @Test
    public void testGetNearestStoreForB2BCustomer_withBaseStore_noBillingAddress() {
        B2BCustomerModel customer = mock(B2BCustomerModel.class);
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        BaseStoreModel baseStore = mock(BaseStoreModel.class);

        when(this.userService.getCurrentUser()).thenReturn(customer);
        when(customer.getDefaultB2BUnit()).thenReturn(b2BUnit);
        when(b2BUnit.getBillingAddress()).thenReturn(null);

        PointOfServiceModel result = vmkUserFacade.getNearestStoreForB2BCustomer(baseStore);

        assertThat(result).isNull();
        verify(userService).getCurrentUser();
        verifyZeroInteractions(vmkssoUserService);
    }

    @Test
    public void testGetNearestStoreForB2BCustomer_withBaseStore_withBillingAddress() {
        B2BCustomerModel customer = mock(B2BCustomerModel.class);
        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);
        AddressModel billingAddress = mock(AddressModel.class);
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        PointOfServiceModel pointOfService = mock(PointOfServiceModel.class);

        when(this.userService.getCurrentUser()).thenReturn(customer);
        when(customer.getDefaultB2BUnit()).thenReturn(b2BUnit);
        when(b2BUnit.getBillingAddress()).thenReturn(billingAddress);
        when(vmkssoUserService.getNearestStore(customer, baseStore)).thenReturn(pointOfService);
        PointOfServiceModel result = vmkUserFacade.getNearestStoreForB2BCustomer(baseStore);

        assertThat(result).isEqualTo(pointOfService);
        verify(userService).getCurrentUser();
        verify(vmkssoUserService).getNearestStore(customer, baseStore);
    }
}
