package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cmsfacades.util.builder.DeliveryModeModelBuilder;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeliveryMethodCodePopulatorTest {

    private static final String DELIVERY_MODE_CODE = RandomStringUtils.randomAlphabetic(10);

    @InjectMocks
    private VMKDeliveryMethodCodePopulator populator;

    @Test
    public void testPopulate() {
        CartModel cart = mock(CartModel.class);
        DeliveryMethodForm form = new DeliveryMethodForm();

        DeliveryModeModel deliveryMode = DeliveryModeModelBuilder.aModel().withCode(DELIVERY_MODE_CODE).build();
        when(cart.getDeliveryMode()).thenReturn(deliveryMode);

        populator.populate(cart, form);

        assertThat(form.getDeliveryOption()).isEqualTo(DELIVERY_MODE_CODE);
    }

    @Test
    public void testPopulate_null() {
        CartModel cart = mock(CartModel.class);
        DeliveryMethodForm form = new DeliveryMethodForm();

        when(cart.getDeliveryMode()).thenReturn(null);

        populator.populate(cart, form);

        assertThat(form.getDeliveryOption()).isNull();
    }
}