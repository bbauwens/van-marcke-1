package com.vanmarcke.facades.search.converters.populators;

import com.vanmarcke.facades.search.converters.VMKPointOfServiceLocatorSearchConverter;
import com.vanmarcke.services.storelocator.pos.VMKDistanceFormatterService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.storelocator.data.TimeData;
import de.hybris.platform.solrfacetsearch.search.impl.SolrResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPointOfServiceLocatorSearchConverterTest extends AbstractSearchConverterTest {

    @Mock
    private Converter<Date, TimeData> timeDataConverter;

    @Mock
    private VMKDistanceFormatterService distanceFormatterService;

    @InjectMocks
    private VMKPointOfServiceLocatorSearchConverter vmkPointOfServiceLocatorSearchConverter;

    @Test
    public void testConvert() {
        SolrResult source = mockSorlResult();

        when(document.getFirstValue("displayName_text")).thenReturn("display-name");
        when(document.getFirstValue("name_text")).thenReturn("name");

        when(document.getFirstValue("streetName_string")).thenReturn("streetname");
        when(document.getFirstValue("streetNumber_string")).thenReturn("streetnumber");
        when(document.getFirstValue("phone1_string")).thenReturn("phone1");
        when(document.getFirstValue("postalCode_text")).thenReturn("postalcode");
        when(document.getFirstValue("town_text")).thenReturn("town");
        when(document.getFirstValue("country_string")).thenReturn("country");

        Date mondayOpeningTime = getTime(7, 00);
        Date mondayClosingTime = getTime(17, 30);
        when(document.getFirstValue("openingTime_monday_date")).thenReturn(mondayOpeningTime);
        when(document.getFirstValue("closingTime_monday_date")).thenReturn(mondayClosingTime);

        Date fridayOpeningTime = getTime(6, 30);
        Date fridayClosingTime = getTime(18, 30);
        when(document.getFirstValue("openingTime_friday_date")).thenReturn(fridayOpeningTime);
        when(document.getFirstValue("closingTime_friday_date")).thenReturn(fridayClosingTime);

        Date time = getTime(0, 00);
        when(document.getFirstValue("openingTime_sunday_date")).thenReturn(time);
        when(document.getFirstValue("closingTime_sunday_date")).thenReturn(time);

        TimeData mondayOpeningTimeData = mock(TimeData.class);
        TimeData mondayClosingTimeData = mock(TimeData.class);
        when(timeDataConverter.convert(mondayOpeningTime)).thenReturn(mondayOpeningTimeData);
        when(timeDataConverter.convert(mondayClosingTime)).thenReturn(mondayClosingTimeData);

        TimeData fridayOpeningTimeData = mock(TimeData.class);
        TimeData fridayClosingTimeData = mock(TimeData.class);
        when(timeDataConverter.convert(fridayOpeningTime)).thenReturn(fridayOpeningTimeData);
        when(timeDataConverter.convert(fridayClosingTime)).thenReturn(fridayClosingTimeData);

        when(document.getFirstValue("latitude_double")).thenReturn(1.0D);
        when(document.getFirstValue("longitude_double")).thenReturn(2.0D);

        when(document.getFirstValue("distance")).thenReturn(16.2F);

        when(distanceFormatterService.formatDistance(16.2F)).thenReturn("16.20");

        PointOfServiceData result = vmkPointOfServiceLocatorSearchConverter.convert(source);

        assertThat(result.getDisplayName()).isEqualTo("display-name");
        assertThat(result.getName()).isEqualTo("name");

        assertThat(result.getAddress()).isNotNull();
        assertThat(result.getAddress().getLine1()).isEqualTo("streetname");
        assertThat(result.getAddress().getLine2()).isEqualTo("streetnumber");
        assertThat(result.getAddress().getPhone()).isEqualTo("phone1");
        assertThat(result.getAddress().getPostalCode()).isEqualTo("postalcode");
        assertThat(result.getAddress().getTown()).isEqualTo("town");
        assertThat(result.getAddress().getCountry()).isNotNull();
        assertThat(result.getAddress().getCountry().getIsocode()).isEqualTo("country");

        assertThat(result.getOpeningHours()).isNotNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList()).hasSize(7);

        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(0).getWeekDay()).isEqualTo("SUNDAY");
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(0).getOpeningTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(0).getClosingTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(0).isClosed()).isTrue();

        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(1).getWeekDay()).isEqualTo("MONDAY");
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(1).getOpeningTime()).isEqualTo(mondayOpeningTimeData);
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(1).getClosingTime()).isEqualTo(mondayClosingTimeData);
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(1).isClosed()).isFalse();

        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(2).getWeekDay()).isEqualTo("TUESDAY");
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(2).getOpeningTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(2).getClosingTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(2).isClosed()).isTrue();

        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(3).getWeekDay()).isEqualTo("WEDNESDAY");
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(3).getOpeningTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(3).getClosingTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(3).isClosed()).isTrue();

        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(4).getWeekDay()).isEqualTo("THURSDAY");
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(4).getOpeningTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(4).getClosingTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(4).isClosed()).isTrue();

        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(5).getWeekDay()).isEqualTo("FRIDAY");
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(5).getOpeningTime()).isEqualTo(fridayOpeningTimeData);
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(5).getClosingTime()).isEqualTo(fridayClosingTimeData);
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(5).isClosed()).isFalse();

        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(6).getWeekDay()).isEqualTo("SATURDAY");
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(6).getOpeningTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(6).getClosingTime()).isNull();
        assertThat(result.getOpeningHours().getWeekDayOpeningList().get(6).isClosed()).isTrue();

        assertThat(result.getGeoPoint()).isNotNull();
        assertThat(result.getGeoPoint().getLatitude()).isEqualTo(1.0D);
        assertThat(result.getGeoPoint().getLongitude()).isEqualTo(2.0D);

        assertThat(result.getFormattedDistance()).isEqualTo("16.20");
    }

    private Date getTime(int hour, int minute) {
        return Date.from(LocalTime.of(hour, minute)
                .atDate(LocalDate.now())
                .atZone(ZoneId.of("UTC"))
                .toInstant());
    }

}