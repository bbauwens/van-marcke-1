package com.vanmarcke.facades.principal.b2bunit.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKB2BUnitPopulatorTest {

    @Mock
    B2BUnitModel b2BUnitModel;
    @InjectMocks
    private VMKB2BUnitPopulator blueB2BUnitPopulator;

    @Test
    public void populate() {
        B2BUnitData b2BUnitData = new B2BUnitData();
        when(b2BUnitModel.getCreditControllerEmail()).thenReturn("test@test.com");
        when(b2BUnitModel.getOrderManagerEmail()).thenReturn("test2@test.com");

        this.blueB2BUnitPopulator.populate(b2BUnitModel, b2BUnitData);
        assertThat(b2BUnitData.getCreditControllerEmail()).isEqualTo("test@test.com");
        assertThat(b2BUnitData.getOrderManagerEmail()).isEqualTo("test2@test.com");
    }
}