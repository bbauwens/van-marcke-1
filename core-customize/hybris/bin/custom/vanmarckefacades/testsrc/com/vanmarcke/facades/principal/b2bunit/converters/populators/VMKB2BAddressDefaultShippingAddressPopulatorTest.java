package com.vanmarcke.facades.principal.b2bunit.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKB2BAddressDefaultShippingAddressPopulatorTest {

    @InjectMocks
    private VMKB2BAddressDefaultShippingAddressPopulator blueB2BAddressDefaultShippingAddressPopulator;

    @Test
    public void testPopulate() {
        final AddressModel source = mock(AddressModel.class);
        final AddressData target = new AddressData();
        final B2BUnitModel owner = mock(B2BUnitModel.class);

        when(source.getOwner()).thenReturn(owner);
        when(owner.getShippingAddress()).thenReturn(source);
        this.blueB2BAddressDefaultShippingAddressPopulator.populate(source, target);

        assertThat(target.isDefaultAddress()).isTrue();
    }
}