package com.vanmarcke.facades.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.commerceservices.enums.SiteChannel.B2B;
import static de.hybris.platform.commerceservices.enums.SiteChannel.B2C;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductVariantBasicPopulatorTest {

    @Mock
    private BaseSiteService baseSiteService;

    @InjectMocks
    private VMKProductVariantBasicPopulator blueProductVariantBasicPopulator;

    @Before
    public void setup() {
        blueProductVariantBasicPopulator.setSiteChannels(singletonList(B2B));
    }

    @Test
    public void testPopulate_withB2BSite() {
        VariantProductModel source = mock(VariantProductModel.class);
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(baseSiteModel.getChannel()).thenReturn(B2B);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        when(source.getCode()).thenReturn("Product_Code");
        when(source.getName()).thenReturn("Product_Name");
        when(source.getDescription()).thenReturn("Product_Description");

        VariantOptionData target = new VariantOptionData();
        blueProductVariantBasicPopulator.populate(source, target);

        assertThat(target.getCode()).isEqualTo("Product_Code");
        assertThat(target.getName()).isEqualTo("Product_Name");
        assertThat(target.getDescription()).isEqualTo("Product_Description");
    }

    @Test
    public void testPopulate_withoutB2BSite() {
        VariantProductModel source = mock(VariantProductModel.class);
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(baseSiteModel.getChannel()).thenReturn(B2C);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        when(source.getName()).thenReturn("Product_Name");
        when(source.getDescription()).thenReturn("Product_Description");

        VariantOptionData target = new VariantOptionData();
        blueProductVariantBasicPopulator.populate(source, target);

        assertThat(target.getName()).isNullOrEmpty();
        assertThat(target.getDescription()).isNullOrEmpty();
    }
}