package com.vanmarcke.facades.delivery.methodform.populators;

import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.util.VMKDateUtils;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSelectedDeliveryDatePopulatorTest {

    @InjectMocks
    private VMKSelectedDeliveryDatePopulator populator;

    @Test
    public void testPopulate_shipping() {
        CartModel cart = mock(CartModel.class);
        DeliveryMethodForm pickupInfo = new DeliveryMethodForm();
        DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
        when(deliveryMode.getCode()).thenReturn("be-standard");
        Date date = VMKDateUtils.createDate(2020, 10, 3);
        when(cart.getDeliveryDate()).thenReturn(date);
        when(cart.getDeliveryMode()).thenReturn(deliveryMode);

        populator.populate(cart, pickupInfo);

        assertThat(pickupInfo.getSelectedPickupDate()).isEqualTo(null);
        assertThat(pickupInfo.getSelectedShippingDate()).isEqualTo("2020-10-03");

        verify(cart, times(2)).getDeliveryDate();
    }

    @Test
    public void testPopulate_pickup() {
        CartModel cart = mock(CartModel.class);
        DeliveryMethodForm pickupInfo = new DeliveryMethodForm();
        DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
        when(deliveryMode.getCode()).thenReturn("be-tec");
        Date date = VMKDateUtils.createDate(2020, 10, 3);
        when(cart.getDeliveryDate()).thenReturn(date);
        when(cart.getDeliveryMode()).thenReturn(deliveryMode);

        populator.populate(cart, pickupInfo);

        assertThat(pickupInfo.getSelectedPickupDate()).isEqualTo("2020-10-03");
        assertThat(pickupInfo.getSelectedShippingDate()).isEqualTo(null);

        verify(cart, times(2)).getDeliveryDate();
    }

    @Test
    public void testPopulate_noDate() {
        CartModel cart = mock(CartModel.class);
        DeliveryMethodForm pickupInfo = new DeliveryMethodForm();

        populator.populate(cart, pickupInfo);

        assertThat(pickupInfo.getSelectedPickupDate()).isNull();
        assertThat(pickupInfo.getSelectedShippingDate()).isNull();

        verify(cart).getDeliveryDate();
    }
}