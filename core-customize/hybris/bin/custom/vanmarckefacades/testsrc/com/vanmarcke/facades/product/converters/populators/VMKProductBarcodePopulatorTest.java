package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;

/**
 * The {@link VMKProductBarcodePopulatorTest} class contains the unit tests for the {@link VMKProductBarcodePopulator}
 * class.
 *
 * @author Christiaan Janssen
 * @since 10-01-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductBarcodePopulatorTest {

    private static final String BARCODE = RandomStringUtils.random(10);
    private static final String SUPPLIER_BARCODE = RandomStringUtils.random(10);

    @InjectMocks
    private VMKProductBarcodePopulator productBarcodePopulator;

    @Test
    public void testPopulate_withoutValues() {
        // given
        final ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(null, null)
                .build();

        final ProductData productData = new ProductData();

        // when
        productBarcodePopulator.populate(productModel, productData);

        // then
        assertThat(productData.getBarcode()).isNull();
        assertThat(productData.getSupplierBarcode()).isNull();
    }

    @Test
    public void testPopulate_withValues() {
        // given
        final ProductModel productModel = ProductModelMockBuilder
                .aBaseProduct(null, null)
                .withVanmarckeItemBarcode(BARCODE)
                .withVendorItemBarcode(SUPPLIER_BARCODE)
                .build();

        final ProductData productData = new ProductData();

        // when
        productBarcodePopulator.populate(productModel, productData);

        // then
        assertThat(productData.getBarcode()).isEqualTo(BARCODE);
        assertThat(productData.getSupplierBarcode()).isEqualTo(SUPPLIER_BARCODE);
    }
}