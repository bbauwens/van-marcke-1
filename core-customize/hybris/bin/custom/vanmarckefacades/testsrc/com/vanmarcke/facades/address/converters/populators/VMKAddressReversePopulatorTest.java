package com.vanmarcke.facades.address.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * The {@link VMKAddressReversePopulatorTest} class contain the unit tests for the {@link VMKAddressReversePopulator}
 * class.
 *
 * @author Christiaan Janssen
 * @since 23/09/2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAddressReversePopulatorTest {

    private static final String APARTMENT = RandomStringUtils.random(10);
    private static final String MOBILE = RandomStringUtils.random(10);

    @InjectMocks
    private VMKAddressReversePopulator addressReversePopulator;

    @Test
    public void testPopulate() {
        AddressData addressData = new AddressData();
        addressData.setApartment(APARTMENT);
        addressData.setMobile(MOBILE);

        AddressModel addressModel = new AddressModel();

        addressReversePopulator.populate(addressData, addressModel);

        Assertions
                .assertThat(addressModel.getAppartment())
                .isEqualTo(APARTMENT);

        Assertions
                .assertThat(addressModel.getPhone2())
                .isEqualTo(MOBILE);
    }
}