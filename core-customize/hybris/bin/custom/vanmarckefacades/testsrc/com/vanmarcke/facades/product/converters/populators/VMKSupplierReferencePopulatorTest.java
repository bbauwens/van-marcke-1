package com.vanmarcke.facades.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSupplierReferencePopulatorTest {

    private static final String SUPPLIER_ITEM_REF = RandomStringUtils.random(10);

    @InjectMocks
    private com.vanmarcke.facades.product.converters.populators.VMKSupplierReferencePopulator VMKSupplierReferencePopulator;

    @Test
    public void testPopulate() {
        ProductModel source = mock(ProductModel.class);
        ProductData target = new ProductData();

        when(source.getVendorItemNumber_current()).thenReturn(SUPPLIER_ITEM_REF);

        VMKSupplierReferencePopulator.populate(source, target);

        Assertions
                .assertThat(target.getSupplierReference())
                .isEqualTo(SUPPLIER_ITEM_REF);
    }

    @Test
    public void testPopulateWithPrivateSelClassification() {
        ProductModel source = mock(ProductModel.class);
        when(source.getSelClassification()).thenReturn("V");

        ProductData target = new ProductData();

        when(source.getVendorItemNumber_current()).thenReturn(SUPPLIER_ITEM_REF);

        VMKSupplierReferencePopulator.populate(source, target);

        Assertions
                .assertThat(target.getSupplierReference())
                .isNull();
    }

    @Test
    public void testPopulateWithCommoditySelClassification() {
        ProductModel source = mock(ProductModel.class);
        when(source.getSelClassification()).thenReturn("C");

        ProductData target = new ProductData();

        when(source.getVendorItemNumber_current()).thenReturn(SUPPLIER_ITEM_REF);

        VMKSupplierReferencePopulator.populate(source, target);

        Assertions
                .assertThat(target.getSupplierReference())
                .isNull();
    }
}