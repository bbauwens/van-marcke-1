package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAbstractOrderTechnicalSheetsAvailabilityPopulatorTest {

    @InjectMocks
    private VMKAbstractOrderTechnicalSheetsAvailabilityPopulator vmkAbstractOrderTechnicalSheetsAvailabilityPopulator;

    private ProductModel product_1;
    private ProductModel product_2;
    private AbstractOrderModel source = mock(AbstractOrderModel.class);
    private AbstractOrderData target = new AbstractOrderData();

    @Before
    public void setup() {
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel orderEntry_1 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel orderEntry_2 = mock(AbstractOrderEntryModel.class);
        this.product_1 = mock(ProductModel.class);
        this.product_2 = mock(ProductModel.class);
        orderEntries.add(orderEntry_1);
        orderEntries.add(orderEntry_2);

        when(this.source.getEntries()).thenReturn(orderEntries);
        when(orderEntry_1.getProduct()).thenReturn(this.product_1);
        when(orderEntry_2.getProduct()).thenReturn(this.product_2);
    }

    @Test
    public void testPopulate_noTechSheets() {
        this.vmkAbstractOrderTechnicalSheetsAvailabilityPopulator.populate(this.source, this.target);

        assertThat(this.target.getTechnicalSheetsAvailable()).isFalse();
    }

    @Test
    public void testPopulate_atLeastOneTechSheet() {
        final MediaModel techSheet = mock(MediaModel.class);

        when(this.product_2.getTech_data_sheet()).thenReturn(techSheet);
        this.vmkAbstractOrderTechnicalSheetsAvailabilityPopulator.populate(this.source, this.target);

        assertThat(this.target.getTechnicalSheetsAvailable()).isTrue();
    }
}