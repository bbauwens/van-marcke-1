package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.OrderModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAbstractOrderExternalCodePopulatorTest {

    @InjectMocks
    private VMKAbstractOrderExternalCodePopulator blueAbstractOrderExternalCodePopulator;

    @Test
    public void testPopulate() {
        OrderModel source = mock(OrderModel.class);
        AbstractOrderData target = new AbstractOrderData();

        when(source.getExternalCode()).thenReturn("W1234");
        blueAbstractOrderExternalCodePopulator.populate(source, target);

        assertThat(target.getExternalCode()).isEqualTo("W1234");
    }
}