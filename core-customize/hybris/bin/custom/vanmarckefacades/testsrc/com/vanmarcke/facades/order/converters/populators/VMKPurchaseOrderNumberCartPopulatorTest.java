package com.vanmarcke.facades.order.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPurchaseOrderNumberCartPopulatorTest {

    @InjectMocks
    private VMKPurchaseOrderNumberCartPopulator bluePurchaseOrderNumberCartPopulator;

    @Test
    public void populate() {
        final CartModel cartModel = mock(CartModel.class);

        when(cartModel.getPurchaseOrderNumber()).thenReturn("po-number");

        final CartData result = new CartData();
        bluePurchaseOrderNumberCartPopulator.populate(cartModel, result);

        assertThat(result.getPurchaseOrderNumber()).isEqualTo("po-number");
    }

}