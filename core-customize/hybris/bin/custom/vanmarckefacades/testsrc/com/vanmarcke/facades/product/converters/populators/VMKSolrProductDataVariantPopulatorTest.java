package com.vanmarcke.facades.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ProductStockStatus;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSolrProductDataVariantPopulatorTest extends TestCase {

    @Test
    public void testPopulate() {
        ProductData source = new ProductData();
        source.setCode("code");
        source.setName("name");
        source.setUrl("/url");
        source.setDescription("description");
        source.setPurchasable(true);
        source.setStockStatus(ProductStockStatus.NO_STOCK);
        VariantOptionData target = new VariantOptionData();

        VMKSolrProductDataVariantPopulator populator = new VMKSolrProductDataVariantPopulator();
        populator.populate(source, target);
        Assert.assertEquals("code", target.getCode());
        Assert.assertEquals("name", target.getName());
        Assert.assertEquals("/url", target.getUrl());
        Assert.assertEquals("description", target.getDescription());
        Assert.assertEquals(ProductStockStatus.NO_STOCK, target.getStockStatus());
    }
}