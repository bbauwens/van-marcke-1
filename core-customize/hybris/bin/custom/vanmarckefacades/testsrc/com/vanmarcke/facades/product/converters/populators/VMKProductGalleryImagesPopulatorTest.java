package com.vanmarcke.facades.product.converters.populators;

import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ImageFormatMapping;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductGalleryImagesPopulatorTest {

    private static final String CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String VARIANT_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String IMG_FORMAT1 = RandomStringUtils.randomAlphabetic(10);
    private static final String IMG_FORMAT2 = RandomStringUtils.randomAlphabetic(10);
    private static final String MEDIA_FORMAT_QUALIFIER = RandomStringUtils.randomAlphabetic(10);
    private static final String NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String ALT_TEXT = RandomStringUtils.randomAlphabetic(10);
    private static final String VIDEO = "VIDEO";
    private static final String OTHER_FORMAT = RandomStringUtils.randomAlphabetic(10);

    private ProductModel source;
    private ProductData target;

    @Mock
    private Converter<MediaModel, ImageData> imageConverter;

    @Mock
    private ImageFormatMapping imageFormatMapping;

    @Mock
    private MediaService mediaService;

    @Mock
    private MediaContainerService mediaContainerService;

    @Spy
    @InjectMocks
    private VMKProductGalleryImagesPopulator<ProductModel, ProductData> populator;

    @Before
    public void setUp() {
        source = ProductModelMockBuilder
                .aBaseProduct(CODE, Locale.GERMAN)
                .build();

        target = new ProductData();

        populator.setImageConverter(imageConverter);
        populator.setImageFormatMapping(imageFormatMapping);
        populator.setMediaService(mediaService);
        populator.setMediaContainerService(mediaContainerService);
    }

    @Test
    public void testPopulate() {
        ImageData imageData1 = new ImageData();
        List<ImageData> mediaList = Collections.list(imageData1);

        MediaContainerModel mediaContainer1 = mock(MediaContainerModel.class);
        MediaContainerModel mediaContainer2 = mock(MediaContainerModel.class);
        List<MediaContainerModel> mediaContainers = Collections.list(mediaContainer1, mediaContainer2);

        MediaModel media1 = mock(MediaModel.class);
        List<MediaModel> medias = Collections.list(media1);

        doNothing().when(populator).addMediaContainersInFormats(any(MediaContainerModel.class), anyInt(), anyListOf(ImageData.class));
        doNothing().when(populator).addMedia(any(MediaModel.class), anyInt(), anyListOf(ImageData.class));

        doReturn(mediaList).when(populator).getExistingImages(target);
        doReturn(mediaContainers).when(populator).getMediaContainers(source);
        doReturn(medias).when(populator).getMediaModels(source);

        populator.populate(source, target);

        verify(populator).getExistingImages(target);
        verify(populator).getMediaContainers(source);
        verify(populator).getMediaModels(source);
        verify(populator).addMediaContainersInFormats(mediaContainer1, 0, mediaList);
        verify(populator).addMediaContainersInFormats(mediaContainer2, 1, mediaList);
        verify(populator).addMedia(media1, 2, mediaList);
        verify(populator).setAltText(source, mediaList);

        assertThat(target.getImages()).isEqualTo(mediaList);
    }

    @Test
    public void testFallbackImages() {
        VariantProductModel variant = VariantProductModelMockBuilder.aVariantProduct(VARIANT_CODE, Locale.ENGLISH).build();
        when(source.getVariants()).thenReturn(singletonList(variant));
        doNothing().when(populator).populateGalleryImages(source, target);

        ImageData imageData1 = new ImageData();
        List<ImageData> mediaList = Collections.list(imageData1);

        MediaContainerModel mediaContainer1 = mock(MediaContainerModel.class);
        MediaContainerModel mediaContainer2 = mock(MediaContainerModel.class);
        List<MediaContainerModel> mediaContainers = Collections.list(mediaContainer1, mediaContainer2);

        MediaModel media1 = mock(MediaModel.class);
        List<MediaModel> medias = Collections.list(media1);

        doNothing().when(populator).addMediaContainersInFormats(any(MediaContainerModel.class), anyInt(), anyListOf(ImageData.class));
        doNothing().when(populator).addMedia(any(MediaModel.class), anyInt(), anyListOf(ImageData.class));

        doReturn(mediaList).when(populator).getExistingImages(target);
        doReturn(mediaContainers).when(populator).getMediaContainers(variant);
        doReturn(medias).when(populator).getMediaModels(variant);

        populator.populate(source, target);

        verify(populator).getExistingImages(target);
        verify(populator).getMediaContainers(variant);
        verify(populator).getMediaModels(variant);
        verify(populator).addMediaContainersInFormats(mediaContainer1, 0, mediaList);
        verify(populator).addMediaContainersInFormats(mediaContainer2, 1, mediaList);
        verify(populator).addMedia(media1, 2, mediaList);
        verify(populator).setAltText(variant, mediaList);
    }

    @Test
    public void testGetExistingImages() {
        ImageData imageData1 = new ImageData();
        List<ImageData> mediaList = Collections.list(imageData1);
        target.setImages(mediaList);

        List<ImageData> result = populator.getExistingImages(target);

        assertThat(result).containsExactly(imageData1);
    }

    @Test
    public void testGetExistingImages_noImgs() {
        List<ImageData> result = populator.getExistingImages(target);

        assertThat(result).isEmpty();
    }

    @Test
    public void testGetMediaContainers_filtered() {
        MediaContainerModel mediaContainer1 = mock(MediaContainerModel.class);
        MediaContainerModel mediaContainer2 = mock(MediaContainerModel.class);
        MediaContainerModel mediaContainer3 = mock(MediaContainerModel.class);
        List<MediaContainerModel> galleryImages = Collections.list(mediaContainer1, mediaContainer2);
        List<MediaContainerModel> techDrawings = Collections.list(mediaContainer2, mediaContainer3);

        doReturn(galleryImages).when(populator).getImages(source, ProductModel.GALLERYIMAGES);
        doReturn(techDrawings).when(populator).getImages(source, ProductModel.TECH_DRAWING);
        doReturn(emptyList()).when(populator).getImages(source, ProductModel.ECO_LABEL);

        List<MediaContainerModel> result = populator.getMediaContainers(source);

        verify(populator).getImages(source, ProductModel.GALLERYIMAGES);
        verify(populator).getImages(source, ProductModel.ECO_LABEL);
        verify(populator).getImages(source, ProductModel.TECH_DRAWING);

        assertThat(result)
                .containsExactly(mediaContainer1, mediaContainer2, mediaContainer3);
    }

    @Test
    public void testGetImages() {
        MediaContainerModel mediaContainer1 = mock(MediaContainerModel.class);
        MediaContainerModel mediaContainer2 = mock(MediaContainerModel.class);
        List<MediaContainerModel> images = Collections.list(mediaContainer1, mediaContainer2);

        doReturn(images).when(populator).getProductAttribute(source, ProductModel.GALLERYIMAGES);

        List<MediaContainerModel> result = populator.getImages(source, ProductModel.GALLERYIMAGES);

        verify(populator).getImages(source, ProductModel.GALLERYIMAGES);

        assertThat(result)
                .isEqualTo(images);
    }

    @Test
    public void testGetImages_null() {
        doReturn(null).when(populator).getProductAttribute(source, ProductModel.GALLERYIMAGES);

        List<MediaContainerModel> result = populator.getImages(source, ProductModel.GALLERYIMAGES);

        verify(populator).getImages(source, ProductModel.GALLERYIMAGES);

        assertThat(result)
                .isEmpty();
    }

    @Test
    public void testGetMediaModels() {
        MediaModel video = mock(MediaModel.class);
        List<MediaModel> medias = Collections.list(video);

        doReturn(medias).when(populator).getVideo(source);

        List<MediaModel> result = populator.getMediaModels(source);

        assertThat(result)
                .containsExactly(video);
    }

    @Test
    public void testGetVideo() {
        MediaModel video = mock(MediaModel.class);

        doReturn(video).when(populator).getProductAttribute(source, ProductModel.VIDEO);

        List<MediaModel> result = populator.getMediaModels(source);

        assertThat(result)
                .containsExactly(video);
    }

    @Test
    public void testAddMediaContainersInFormats_noMediaFormatQualifier() {
        populator.setImageFormats(singletonList(IMG_FORMAT1));

        MediaContainerModel container = mock(MediaContainerModel.class);

        when(imageFormatMapping.getMediaFormatQualifierForImageFormat(IMG_FORMAT1)).thenReturn(null);

        List<ImageData> medias = new ArrayList<>();

        populator.addMediaContainersInFormats(container, 0, medias);

        assertThat(medias).isEmpty();

        verify(populator, never()).getMediaFormat(anyString());
        verify(populator, never()).getMedia(any(MediaContainerModel.class), any(MediaFormatModel.class));
    }

    @Test
    public void testAddMediaContainersInFormats_noMediaFormatModel() {
        populator.setImageFormats(Collections.list(IMG_FORMAT1, IMG_FORMAT2));

        MediaContainerModel container = mock(MediaContainerModel.class);

        when(imageFormatMapping.getMediaFormatQualifierForImageFormat(IMG_FORMAT1)).thenReturn(MEDIA_FORMAT_QUALIFIER);
        when(imageFormatMapping.getMediaFormatQualifierForImageFormat(IMG_FORMAT2)).thenReturn(MEDIA_FORMAT_QUALIFIER);

        doReturn(null).when(populator).getMediaFormat(MEDIA_FORMAT_QUALIFIER);

        List<ImageData> medias = new ArrayList<>();

        populator.addMediaContainersInFormats(container, 0, medias);

        assertThat(medias).isEmpty();

        verify(populator, times(2)).getMediaFormat(MEDIA_FORMAT_QUALIFIER);
        verify(populator, never()).getMedia(eq(container), any(MediaFormatModel.class));
    }


    @Test
    public void testAddMediaContainersInFormats_noMediaModel() {
        populator.setImageFormats(Collections.list(IMG_FORMAT1, IMG_FORMAT2));

        MediaContainerModel container = mock(MediaContainerModel.class);

        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);
        doReturn(mediaFormat).when(populator).getMediaFormat(MEDIA_FORMAT_QUALIFIER);

        when(imageFormatMapping.getMediaFormatQualifierForImageFormat(IMG_FORMAT1)).thenReturn(MEDIA_FORMAT_QUALIFIER);
        when(imageFormatMapping.getMediaFormatQualifierForImageFormat(IMG_FORMAT2)).thenReturn(MEDIA_FORMAT_QUALIFIER);

        doReturn(null).when(populator).getMedia(container, mediaFormat);

        List<ImageData> medias = new ArrayList<>();

        populator.addMediaContainersInFormats(container, 0, medias);

        assertThat(medias).isEmpty();

        verify(populator, times(2)).getMediaFormat(MEDIA_FORMAT_QUALIFIER);
        verify(populator, times(2)).getMedia(container, mediaFormat);
    }

    @Test
    public void testAddMediaContainersInFormats_success() {
        populator.setImageFormats(Collections.list(IMG_FORMAT1, IMG_FORMAT2));

        MediaContainerModel container = mock(MediaContainerModel.class);

        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);
        doReturn(mediaFormat).when(populator).getMediaFormat(MEDIA_FORMAT_QUALIFIER);

        MediaModel mediaModel = mock(MediaModel.class);
        doReturn(mediaModel).when(populator).getMedia(container, mediaFormat);

        when(imageFormatMapping.getMediaFormatQualifierForImageFormat(IMG_FORMAT1)).thenReturn(MEDIA_FORMAT_QUALIFIER);
        when(imageFormatMapping.getMediaFormatQualifierForImageFormat(IMG_FORMAT2)).thenReturn(MEDIA_FORMAT_QUALIFIER);

        ImageData imageData1 = new ImageData();
        ImageData imageData2 = new ImageData();

        doReturn(imageData1).when(populator).convert(0, IMG_FORMAT1, mediaModel);
        doReturn(imageData2).when(populator).convert(0, IMG_FORMAT2, mediaModel);

        List<ImageData> medias = new ArrayList<>();

        populator.addMediaContainersInFormats(container, 0, medias);

        assertThat(medias).containsExactly(imageData1, imageData2);

        verify(populator, times(2)).getMediaFormat(MEDIA_FORMAT_QUALIFIER);
        verify(populator, times(2)).getMedia(container, mediaFormat);
        verify(populator).convert(0, IMG_FORMAT1, mediaModel);
        verify(populator).convert(0, IMG_FORMAT2, mediaModel);
    }

    @Test
    public void testAddMediaVideo() {
        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);
        when(mediaFormat.getQualifier()).thenReturn(VIDEO);

        MediaModel mediaModel = mock(MediaModel.class);
        when(mediaModel.getMediaFormat()).thenReturn(mediaFormat);

        ImageData imageData = new ImageData();

        List<ImageData> medias = new ArrayList<>();

        when(imageConverter.convert(mediaModel)).thenReturn(imageData);

        doReturn(imageData).when(populator).convert(0, VIDEO, mediaModel);

        populator.addMedia(mediaModel, 0, medias);

        assertThat(medias).containsExactly(imageData);

        verify(populator).convert(0, VIDEO, mediaModel);
        verify(populator, never()).convert(0, null, mediaModel);
    }

    @Test
    public void testAddMedia_differentMediaFormat() {
        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);
        when(mediaFormat.getName()).thenReturn(OTHER_FORMAT);

        MediaModel mediaModel = mock(MediaModel.class);
        when(mediaModel.getMediaFormat()).thenReturn(mediaFormat);

        ImageData imageData = new ImageData();

        List<ImageData> medias = new ArrayList<>();

        doReturn(imageData).when(populator).convert(0, null, mediaModel);

        populator.addMedia(mediaModel, 0, medias);

        assertThat(medias).containsExactly(imageData);

        verify(populator).convert(0, null, mediaModel);
        verify(populator, never()).convert(0, VIDEO, mediaModel);
    }

    @Test
    public void testAddMedia_noMediaFormat() {
        MediaModel mediaModel = mock(MediaModel.class);
        when(mediaModel.getMediaFormat()).thenReturn(null);

        ImageData imageData = new ImageData();

        List<ImageData> medias = new ArrayList<>();

        doReturn(imageData).when(populator).convert(0, null, mediaModel);

        populator.addMedia(mediaModel, 0, medias);

        assertThat(medias).containsExactly(imageData);

        verify(populator).convert(0, null, mediaModel);
        verify(populator, never()).convert(0, VIDEO, mediaModel);
    }

    @Test
    public void testSetAltText() {
        List<ImageData> mediaList = new ArrayList<>();
        ImageData imageData = new ImageData();
        mediaList.add(imageData);

        when(source.getName()).thenReturn(NAME);

        populator.setAltText(source, mediaList);

        assertThat(imageData.getAltText())
                .isEqualTo(NAME);
    }

    @Test
    public void testSetAltText_alreadySet() {
        List<ImageData> mediaList = new ArrayList<>();
        ImageData imageData = new ImageData();
        imageData.setAltText(ALT_TEXT);
        mediaList.add(imageData);


        populator.setAltText(source, mediaList);

        assertThat(imageData.getAltText())
                .isEqualTo(ALT_TEXT);
    }

    @Test
    public void testConvert() {
        MediaModel mediaModel = mock(MediaModel.class);
        ImageData imageData = new ImageData();

        when(imageConverter.convert(mediaModel)).thenReturn(imageData);

        ImageData result = populator.convert(2, IMG_FORMAT1, mediaModel);

        assertThat(result.getFormat()).isEqualTo(IMG_FORMAT1);
        assertThat(result.getImageType()).isEqualTo(ImageDataType.GALLERY);
        assertThat(result.getGalleryIndex()).isEqualTo(2);
        assertThat(result).isEqualTo(imageData);
    }

    @Test
    public void testGetMedia_mediaModel() {
        MediaModel mediaModel1 = mock(MediaModel.class);
        MediaModel mediaModel2 = mock(MediaModel.class);
        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);

        doReturn(mediaModel2).when(populator).getMediaModel(mediaModel1, mediaFormat);

        MediaModel result = populator.getMedia(mediaModel1, mediaFormat);

        assertThat(result).isEqualTo(mediaModel2);

        verify(populator, never()).getMediaContainer(any(), eq(mediaFormat));
        verify(populator).getMediaModel(mediaModel1, mediaFormat);
    }

    @Test
    public void testGetMedia_mediaContainer() {
        MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        MediaModel mediaModel = mock(MediaModel.class);
        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);

        doReturn(mediaModel).when(populator).getMediaContainer(mediaContainerModel, mediaFormat);

        MediaModel result = populator.getMedia(mediaContainerModel, mediaFormat);

        assertThat(result).isEqualTo(mediaModel);

        verify(populator, never()).getMediaModel(any(), eq(mediaFormat));
        verify(populator).getMediaContainer(mediaContainerModel, mediaFormat);
    }

    @Test
    public void testGetMediaModel() {
        MediaModel mediaModel1 = mock(MediaModel.class);
        MediaModel mediaModel2 = mock(MediaModel.class);
        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);

        when(mediaService.getMediaByFormat(mediaModel1, mediaFormat)).thenReturn(mediaModel2);

        MediaModel result = populator.getMediaModel(mediaModel1, mediaFormat);

        assertThat(result).isEqualTo(mediaModel2);

        verify(mediaService).getMediaByFormat(mediaModel1, mediaFormat);
    }

    @Test
    public void testGetMediaContainerModel() {
        MediaContainerModel mediaContainer = mock(MediaContainerModel.class);
        MediaModel mediaModel = mock(MediaModel.class);
        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);

        when(mediaContainerService.getMediaForFormat(mediaContainer, mediaFormat)).thenReturn(mediaModel);

        MediaModel result = populator.getMediaContainer(mediaContainer, mediaFormat);

        assertThat(result).isEqualTo(mediaModel);

        verify(mediaContainerService).getMediaForFormat(mediaContainer, mediaFormat);
    }

    @Test
    public void testGetMediaFormat() {
        MediaFormatModel mediaFormat = mock(MediaFormatModel.class);

        when(mediaService.getFormat(MEDIA_FORMAT_QUALIFIER)).thenReturn(mediaFormat);

        MediaFormatModel result = populator.getMediaFormat(MEDIA_FORMAT_QUALIFIER);

        assertThat(result).isEqualTo(mediaFormat);

        verify(mediaService).getFormat(MEDIA_FORMAT_QUALIFIER);
    }
}