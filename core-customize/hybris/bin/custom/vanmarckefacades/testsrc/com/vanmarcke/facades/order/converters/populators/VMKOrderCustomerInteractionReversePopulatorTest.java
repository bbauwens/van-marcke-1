package com.vanmarcke.facades.order.converters.populators;

import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.facades.customerinteraction.data.CustomerInteractionData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.services.B2BOrderService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.vanmarcke.core.enums.CustomerInteractionType.CREDIT_CONTROLLER;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKOrderCustomerInteractionReversePopulatorTest {

    @Mock
    private UserService userService;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private BaseStoreService baseStoreService;
    @Mock
    private B2BOrderService b2bOrderService;
    @InjectMocks
    private VMKOrderCustomerInteractionReversePopulator blueCustomerInteractionReversePopulator;

    @Test
    public void testPopulate() {
        final CustomerInteractionData source = mock(CustomerInteractionData.class);
        final OrderCustomerInteractionModel target = mock(OrderCustomerInteractionModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final UserModel userModel = mock(UserModel.class);
        final BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);
        final BaseStoreModel baseStoreModel = mock(BaseStoreModel.class);

        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);

        when(userService.getCurrentUser()).thenReturn(userModel);

        when(source.getOrderNumber()).thenReturn("1234");
        when(source.getType()).thenReturn(CREDIT_CONTROLLER);
        when(b2bOrderService.getOrderForCode("1234")).thenReturn(orderModel);
        when(source.getMessage()).thenReturn("some message");

        blueCustomerInteractionReversePopulator.populate(source, target);

        verify(target).setUser(userModel);
        verify(target).setStore(baseStoreModel);
        verify(target).setSite(baseSiteModel);
        verify(target).setType(CREDIT_CONTROLLER);
        verify(target).setOrder(orderModel);
        verify(target).setMessage("some message");
    }
}