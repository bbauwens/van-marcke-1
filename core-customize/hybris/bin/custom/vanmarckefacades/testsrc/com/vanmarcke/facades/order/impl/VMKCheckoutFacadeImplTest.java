package com.vanmarcke.facades.order.impl;

import com.vanmarcke.core.exception.NoSessionCartException;
import com.vanmarcke.facades.data.DeliveryInfoData;
import com.vanmarcke.facades.data.PickupInfoData;
import com.vanmarcke.facades.data.ShippingInfoData;
import com.vanmarcke.facades.delivery.VMKDeliveryInformationFacade;
import com.vanmarcke.facades.stock.VMKStockFacade;
import com.vanmarcke.services.delivery.VMKDeliveryMethodService;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.model.builder.CartEntryModelMockBuilder;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import com.vanmarcke.services.order.VMKCommerceCheckoutService;
import com.vanmarcke.services.order.payment.VMKPaymentInfoFactory;
import com.vanmarcke.services.order.payment.VMKPaymentTypeService;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bacceleratorservices.order.B2BCommerceCartService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.PICKUP;
import static com.vanmarcke.core.constants.VanmarckeCoreConstants.DeliveryMethods.SHIPPING;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKCheckoutFacadeImplTest} class contains the unit tests for the {@link VMKCheckoutFacadeImpl}
 * class.
 *
 * @author Christiaan Janssen
 * @since 06-07-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCheckoutFacadeImplTest {

    private static final String POS_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String PAYMENT_TYPE_CODE = "CARD";
    private static final String PO_NUMBER = RandomStringUtils.randomAlphabetic(10);
    private static final boolean CREDIT_WORTHY = RandomUtils.nextBoolean();
    private static final String ENTRY1 = RandomStringUtils.randomAlphabetic(10);
    private static final String ENTRY2 = RandomStringUtils.randomAlphabetic(10);
    private static final String ENTRIES = ENTRY1 + ", " + ENTRY2;

    private CartModel cart;

    @Mock
    private VMKPaymentInfoFactory paymentInfoFactory;

    @Mock
    private VMKPaymentTypeService paymentTypeService;

    @Mock
    private VMKCommerceCheckoutService checkoutService;

    @Mock
    private VMKDeliveryMethodService deliveryModeService;

    @Mock
    private VMKDeliveryInformationFacade deliveryInfoFacade;

    @Mock
    private ModelService modelService;

    @Mock
    private B2BCommerceCartService b2BCommerceCartService;

    @Mock
    private CartService cartService;

    @Mock
    private CartFacade cartFacade;

    @Mock
    private DeliveryService deliveryService;

    @Mock
    private Converter<CheckoutPaymentType, B2BPaymentTypeData> b2bPaymentTypeDataConverter;

    @Mock
    private VMKStockFacade vmkStockFacade;

    @Mock
    private VMKVariantProductService variantProductService;

    @Spy
    @InjectMocks
    private VMKCheckoutFacadeImpl checkoutFacade;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        checkoutFacade.setModelService(modelService);
        checkoutFacade.setCommerceCheckoutService(checkoutService);
        checkoutFacade.setB2bPaymentTypeDataConverter(b2bPaymentTypeDataConverter);
        checkoutFacade.setCommerceCartService(b2BCommerceCartService);
        checkoutFacade.setCartFacade(cartFacade);
        checkoutFacade.setDeliveryService(deliveryService);
        checkoutFacade.setCartService(cartService);

        cart = CartModelMockBuilder
                .aCart()
                .build();

        doReturn(cart).when(checkoutFacade).getCart();

    }

    @Test
    public void testGetFirstPossibleDeliveryDateForPickup() {
        PickupInfoData expectedResponse = new PickupInfoData();
        when(deliveryInfoFacade.getPickupInfo(cart, null)).thenReturn(expectedResponse);

        DeliveryInfoData actualResponse = checkoutFacade.getDeliveryInformation(PICKUP, null);

        assertThat(actualResponse)
                .isEqualTo(expectedResponse)
                .isInstanceOf(PickupInfoData.class)
                .isNotInstanceOf(ShippingInfoData.class);

        verify(deliveryInfoFacade).getPickupInfo(cart, null);
        verify(deliveryInfoFacade, never()).getShippingInfo(cart);
        verify(checkoutFacade).getCart();
    }

    @Test
    public void testGetFirstPossibleDeliveryDateForDelivery() {
        ShippingInfoData expectedResponse = new ShippingInfoData();
        when(deliveryInfoFacade.getShippingInfo(cart)).thenReturn(expectedResponse);

        DeliveryInfoData actualResponse = checkoutFacade.getDeliveryInformation(SHIPPING, null);

        assertThat(actualResponse)
                .isEqualTo(expectedResponse)
                .isInstanceOf(ShippingInfoData.class)
                .isNotInstanceOf(PickupInfoData.class);

        verify(deliveryInfoFacade, never()).getPickupInfo(cart, null);
        verify(deliveryInfoFacade).getShippingInfo(cart);
        verify(checkoutFacade).getCart();
    }


    @Test
    public void testGetAvailableDeliveryModes() {
        Map<String, Boolean> expected = new HashMap<>();
        when(deliveryModeService.getSupportedDeliveryMethods(cart)).thenReturn(expected);
        Map<String, Boolean> actual = checkoutFacade.getAvailableDeliveryMethods();

        assertThat(actual).isEqualTo(expected);
        verify(deliveryModeService).getSupportedDeliveryMethods(cart);
    }

    @Test
    public void testGetDeliveryModeMapping() {
        Map<String, String> expected = new HashMap<>();
        when(cartService.getSessionCart()).thenReturn(cart);
        when(deliveryModeService.getDeliveryMethodCodes(cart)).thenReturn(expected);

        Map<String, String> actual = checkoutFacade.getDeliveryMethodCodes();

        assertThat(actual).isEqualTo(expected);
        verify(deliveryModeService).getDeliveryMethodCodes(cart);
    }

    @Test
    public void testSaveDeliveryInformation() {
        DeliveryMethodForm form = new DeliveryMethodForm();

        checkoutFacade.saveDeliveryInformation(form);

        verify(checkoutService).setDeliveryDetailsOnCart(form);
    }

    @Test
    public void testBeforePlaceOrder() {
        checkoutFacade.beforePlaceOrder(cart);

        verify(cart).setStatus(OrderStatus.CREATED);
    }

    @Test
    public void testUpdateCheckoutCartWithCartNull() {
        CartData cartData = new CartData();

        doReturn(null).when(checkoutFacade).getCart();

        CartData actualCartData = checkoutFacade.updateCheckoutCart(cartData);

        assertThat(actualCartData)
                .isNull();

        verify(modelService, never()).save(any(CartModel.class));
    }

    @Test
    public void testUpdateCheckoutCart() {
        B2BPaymentTypeData paymentTypeData = new B2BPaymentTypeData();
        paymentTypeData.setCode(PAYMENT_TYPE_CODE);

        CartData cartData = new CartData();
        cartData.setPaymentType(paymentTypeData);
        cartData.setPurchaseOrderNumber(PO_NUMBER);

        CartModel cart = new CartModel();

        doReturn(cart).when(checkoutFacade).getCart();

        when(paymentTypeService.getSupportedPaymentTypeListForOrder(cart)).thenReturn(singletonList(CheckoutPaymentType.CARD));

        PaymentInfoModel expectedPaymentInfo = mock(PaymentInfoModel.class);

        when(paymentInfoFactory.createPaymentInfoForOrder(cart)).thenReturn(expectedPaymentInfo);
        when(cartFacade.getSessionCart()).thenReturn(cartData);

        AddressData deliveryAddress = new AddressData();

        doReturn(deliveryAddress).when(checkoutFacade).getDeliveryAddress();

        DeliveryModeData deliveryMode = new DeliveryModeData();

        doReturn(deliveryMode).when(checkoutFacade).getDeliveryMode();

        CartData actualCartData = checkoutFacade.updateCheckoutCart(cartData);

        assertThat(cart.getPaymentType())
                .isEqualTo(CheckoutPaymentType.CARD);

        assertThat(cart.getPaymentInfo())
                .isEqualTo(expectedPaymentInfo);

        assertThat(cart.getPurchaseOrderNumber())
                .isEqualTo(PO_NUMBER);

        assertThat(actualCartData)
                .isEqualTo(cartData);

        assertThat(cartData.getDeliveryAddress())
                .isEqualTo(deliveryAddress);

        assertThat(cartData.getDeliveryMode())
                .isEqualTo(deliveryMode);

        verify(paymentTypeService).getSupportedPaymentTypeListForOrder(cart);
        verify(paymentInfoFactory).createPaymentInfoForOrder(cart);
        verify(modelService).save(cart);
    }

    @Test
    public void testSetPaymentTypeForCartWithoutValidPaymentType() {
        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        when(paymentTypeService.getSupportedPaymentTypeListForOrder(cart)).thenReturn(singletonList(CheckoutPaymentType.CARD));

        expectedException.expect(EntityValidationException.class);
        expectedException.expectMessage("Payment type is not a valid value.");

        checkoutFacade.setPaymentTypeForCart(RandomStringUtils.random(10), cart);
    }

    @Test
    public void testGetPaymentTypesWithoutCart() {
        doReturn(null).when(checkoutFacade).getCart();

        List<B2BPaymentTypeData> actualPaymentModes = checkoutFacade.getPaymentTypes();

        assertThat(actualPaymentModes)
                .isEmpty();
    }

    @Test
    public void testGetPaymentTypes() {
        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        doReturn(cart).when(checkoutFacade).getCart();

        when(paymentTypeService.getSupportedPaymentTypeListForOrder(cart)).thenReturn(singletonList(CheckoutPaymentType.CARD));

        B2BPaymentTypeData paymentTypeData = new B2BPaymentTypeData();

        when(b2bPaymentTypeDataConverter.convertAll(singletonList(CheckoutPaymentType.CARD))).thenReturn(singletonList(paymentTypeData));

        List<B2BPaymentTypeData> actualPaymentModes = checkoutFacade.getPaymentTypes();

        assertThat(actualPaymentModes)
                .containsExactly(paymentTypeData);

        verify(paymentTypeService).getSupportedPaymentTypeListForOrder(cart);
    }

    @Test
    public void testSetFavoriteAlternativeStoreOnSessionCart() {
        checkoutFacade.setFavoriteAlternativeStoreOnSessionCart(POS_NAME);

        verify(checkoutService).setFavoriteAlternativeStoreOnSessionCart(POS_NAME);
    }

    @Test
    public void testGetInvalidCartEntries_empty() {
        when(checkoutService.getInvalidCartEntries()).thenReturn(Collections.emptyList());

        assertThat(checkoutFacade.getInvalidCartEntries())
                .isEmpty();

        verify(checkoutService).getInvalidCartEntries();
    }

    @Test
    public void testGetCart_exception() {
        doCallRealMethod().when(checkoutFacade).getCart();
        when(cartFacade.hasSessionCart()).thenReturn(false);
        expectedException.expect(NoSessionCartException.class);
        expectedException.expectMessage("No session cart was found.");
        checkoutFacade.getCart();
    }

    @Test
    public void testGetCart() {
        CartModel actual = checkoutFacade.getCart();

        assertThat(actual).isEqualTo(cart);
    }

    @Test
    public void testGetInvalidCartEntries_notEmpty() {
        List<String> invalidCartEntries = Arrays.asList(ENTRY1, ENTRY2);
        when(checkoutService.getInvalidCartEntries()).thenReturn(invalidCartEntries);

        assertThat(checkoutFacade.getInvalidCartEntries())
                .isEqualTo(ENTRIES);

        verify(checkoutService).getInvalidCartEntries();
    }

    @Test
    public void testRemoveInvalidCartEntries() {
        String invalidCartEntries = "1234, 5678";
        AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        ProductModel product1 = mock(ProductModel.class);
        ProductModel product2 = mock(ProductModel.class);

        when(cartService.getSessionCart()).thenReturn(cart);
        when(cart.getEntries()).thenReturn(Arrays.asList(entry1, entry2));
        when(entry1.getProduct()).thenReturn(product1);
        when(product1.getCode()).thenReturn("1234");
        when(entry2.getProduct()).thenReturn(product2);
        when(product2.getCode()).thenReturn("5678");
        checkoutFacade.removeInvalidCartEntries(invalidCartEntries);

        verify(modelService).removeAll(Arrays.asList(entry1, entry2));
        verify(modelService).refresh(cart);
    }

    @Test
    public void testGetSupportedDeliveryModes() {
        // This method is only here to max out the test coverage.
        checkoutFacade.getSupportedDeliveryModes();
    }

    @Test
    public void testGetDeliveryAddress() {
        // This method is only here to max out the test coverage.
        checkoutFacade.getDeliveryAddress();
    }

    @Test
    public void testGetDeliveryMode() {
        // This method is only here to max out the test coverage.
        checkoutFacade.getDeliveryMode();
    }

    @Test
    public void testGetDeliveryMethodForm() {
        // This method is only here to max out the test coverage.
        checkoutFacade.getDeliveryMethodForm();
    }

    @Test
    public void testAdjustQuantityForDiscontinuedProducts() throws CommerceCartModificationException {
        // given
        VariantProductModel product1 = VariantProductModelMockBuilder.aVariantProduct("product1", null)
                .build();
        VariantProductModel product2 = VariantProductModelMockBuilder.aVariantProduct("product2", null)
                .build();
        VariantProductModel product3 = VariantProductModelMockBuilder.aVariantProduct("product3", null)
                .build();
        VariantProductModel product4 = VariantProductModelMockBuilder.aVariantProduct("product4", null)
                .build();

        CartEntryModel cartEntry1 = CartEntryModelMockBuilder.anEntry()
                .withProduct(product1)
                .withQuantity(10L)
                .build();

        CartEntryModel cartEntry2 = CartEntryModelMockBuilder.anEntry()
                .withProduct(product2)
                .withQuantity(10L)
                .build();

        CartEntryModel cartEntry3 = CartEntryModelMockBuilder.anEntry()
                .withProduct(product3)
                .withQuantity(10L)
                .build();

        CartEntryModel cartEntry4 = CartEntryModelMockBuilder.anEntry()
                .withProduct(product4)
                .withQuantity(10L)
                .build();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withEntries(cartEntry1, cartEntry2, cartEntry3, cartEntry4)
                .build();

        when(vmkStockFacade.getAvailableEDCStock("product1")).thenReturn(9L);
        when(vmkStockFacade.getAvailableEDCStock("product2")).thenReturn(9L);
        when(vmkStockFacade.getAvailableEDCStock("product3")).thenReturn(9L);
        when(vmkStockFacade.getAvailableEDCStock("product4")).thenReturn(5L);
        when(variantProductService.isDiscontinuedProduct(product1)).thenReturn(true);
        when(variantProductService.isDiscontinuedProduct(product2)).thenReturn(false);
        when(variantProductService.isDiscontinuedProduct(product3)).thenReturn(true);
        when(variantProductService.isDiscontinuedProduct(product4)).thenReturn(false);
        when(variantProductService.isSellingOffAlmostNOS(product4)).thenReturn(true);
        when(cartEntry1.getEntryNumber()).thenReturn(1);
        when(cartEntry3.getEntryNumber()).thenReturn(3);
        when(cartEntry4.getEntryNumber()).thenReturn(4);
        doReturn(cart).when(checkoutFacade).getCart();

        // when
        String adjustedProducts = checkoutFacade.adjustQuantityForDiscontinuedProducts();

        // then
        Assert.assertEquals("product1,product3,product4", adjustedProducts);
        verify(cartFacade).updateCartEntry(1, 9);
        verify(cartFacade).updateCartEntry(3, 9);
        verify(cartFacade).updateCartEntry(4, 5);
    }

    protected Date createTestDate(String dateString) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(dateString);
    }
}
