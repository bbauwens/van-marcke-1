package com.vanmarcke.facades.product.converters.populators;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKFeaturePopulatorTest {

    private static final boolean BOOLEAN = RandomUtils.nextBoolean();
    private static final String VALUE = RandomStringUtils.randomAlphabetic(10);

    @Mock
    private LocalizedMessageFacade messageFacade;

    @Mock
    private I18NService i18NService;

    @InjectMocks
    private VMKFeaturePopulator vanmarckeFeaturePopulator;


    @Test
    public void testPopulate_withClassificationAttributeValue() {
        Feature feature = mock(Feature.class);
        ClassAttributeAssignmentModel classAttributeAssignment = mock(ClassAttributeAssignmentModel.class);
        ClassificationAttributeModel classificationAttribute = mock(ClassificationAttributeModel.class);
        List<FeatureValue> featureValues = new ArrayList<>();
        FeatureValue mockedFeatureValue = mock(FeatureValue.class);
        featureValues.add(mockedFeatureValue);
        ClassificationAttributeValueModel classificationAttributeValueModel = mock(ClassificationAttributeValueModel.class);
        when(mockedFeatureValue.getValue()).thenReturn(classificationAttributeValueModel);
        when(classificationAttributeValueModel.getCode()).thenReturn("FeatureValueCode");
        when(classificationAttributeValueModel.getName()).thenReturn("FeatureValueName");

        when(feature.getCode()).thenReturn("feature-code");
        when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignment);
        when(classAttributeAssignment.getClassificationAttribute()).thenReturn(classificationAttribute);
        when(classificationAttribute.getCode()).thenReturn("classification-attribute-code");
        when(feature.getValues()).thenReturn(featureValues);

        FeatureData result = new FeatureData();
        vanmarckeFeaturePopulator.populate(feature, result);

        assertThat(result.getCode()).isEqualTo("feature-code");
        assertThat(result.getClassificationCode()).isEqualTo("classification-attribute-code");
        assertThat(result.getFeatureValues()).isNotEmpty().hasSize(1);
        FeatureValueData featureValueData = result.getFeatureValues().iterator().next();
        assertThat(featureValueData.getCode()).isEqualTo("FeatureValueCode");
        assertThat(featureValueData.getValue()).isEqualTo("FeatureValueName");
    }

    @Test
    public void testPopulate_number() {
        Feature feature = mock(Feature.class);
        ClassAttributeAssignmentModel classAttributeAssignment = mock(ClassAttributeAssignmentModel.class);
        ClassificationAttributeModel classificationAttribute = mock(ClassificationAttributeModel.class);
        List<FeatureValue> featureValues = new ArrayList<>();
        FeatureValue mockedFeatureValue = mock(FeatureValue.class);
        featureValues.add(mockedFeatureValue);
        when(mockedFeatureValue.getValue()).thenReturn(3);

        when(feature.getCode()).thenReturn("feature-code");
        when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignment);
        when(classAttributeAssignment.getClassificationAttribute()).thenReturn(classificationAttribute);
        when(classificationAttribute.getCode()).thenReturn("classification-attribute-code");
        when(feature.getValues()).thenReturn(featureValues);

        FeatureData result = new FeatureData();
        vanmarckeFeaturePopulator.populate(feature, result);

        assertThat(result.getCode()).isEqualTo("feature-code");
        assertThat(result.getClassificationCode()).isEqualTo("classification-attribute-code");
        assertThat(result.getFeatureValues()).isNotEmpty().hasSize(1);
        FeatureValueData featureValueData = result.getFeatureValues().iterator().next();
        assertThat(featureValueData.getValue()).isEqualTo("3");
    }

    @Test
    public void testPopulate_boolean() {
        Feature feature = mock(Feature.class);
        ClassAttributeAssignmentModel classAttributeAssignment = mock(ClassAttributeAssignmentModel.class);
        ClassificationAttributeModel classificationAttribute = mock(ClassificationAttributeModel.class);
        List<FeatureValue> featureValues = new ArrayList<>();
        FeatureValue mockedFeatureValue = mock(FeatureValue.class);
        featureValues.add(mockedFeatureValue);
        when(mockedFeatureValue.getValue()).thenReturn(BOOLEAN);

        when(feature.getCode()).thenReturn("feature-code");
        when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignment);
        when(classAttributeAssignment.getClassificationAttribute()).thenReturn(classificationAttribute);
        when(classificationAttribute.getCode()).thenReturn("classification-attribute-code");
        when(feature.getValues()).thenReturn(featureValues);

        when(i18NService.getCurrentLocale()).thenReturn(Locale.CHINA);
        when(messageFacade.getMessageForCodeAndLocale("facet.boolean." + String.valueOf(BOOLEAN).toLowerCase(), Locale.CHINA)).thenReturn(VALUE);

        FeatureData result = new FeatureData();
        vanmarckeFeaturePopulator.populate(feature, result);

        assertThat(result.getCode()).isEqualTo("feature-code");
        assertThat(result.getClassificationCode()).isEqualTo("classification-attribute-code");
        assertThat(result.getFeatureValues()).isNotEmpty().hasSize(1);
        FeatureValueData featureValueData = result.getFeatureValues().iterator().next();
        assertThat(featureValueData.getValue()).isEqualTo(VALUE);
    }

}