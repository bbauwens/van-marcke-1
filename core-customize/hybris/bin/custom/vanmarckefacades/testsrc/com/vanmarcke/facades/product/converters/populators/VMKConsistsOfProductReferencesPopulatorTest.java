package com.vanmarcke.facades.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.HashSet;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKConsistsOfProductReferencesPopulatorTest {

    @Mock
    Converter<ProductReferenceModel, ProductReferenceData> blueProductReferenceConsistsOfConverter;

    @InjectMocks
    VMKConsistsOfProductReferencesPopulator blueConsistsOfProductReferencesPopulator;

    @Test
    public void testConvert() {
        final ProductModel source = mock(ProductModel.class);
        final ProductData target = new ProductData();
        final ProductReferenceModel consistsOf = mock(ProductReferenceModel.class);
        final ProductReferenceModel upSelling = mock(ProductReferenceModel.class);
        final ProductReferenceModel crossSelling = mock(ProductReferenceModel.class);
        final ProductReferenceData consistsOfData = mock(ProductReferenceData.class);
        final Collection productReferences = new HashSet();
        productReferences.add(consistsOf);
        productReferences.add(upSelling);
        productReferences.add(crossSelling);

        when(source.getProductReferences()).thenReturn(productReferences);
        when(consistsOf.getReferenceType()).thenReturn(ProductReferenceTypeEnum.CONSISTS_OF);
        when(upSelling.getReferenceType()).thenReturn(ProductReferenceTypeEnum.UPSELLING);
        when(crossSelling.getReferenceType()).thenReturn(ProductReferenceTypeEnum.CROSSELLING);
        when(this.blueProductReferenceConsistsOfConverter.convert(consistsOf)).thenReturn(consistsOfData);
        this.blueConsistsOfProductReferencesPopulator.populate(source, target);

        assertThat(target.getConsistsOfReferences()).isNotNull().isNotEmpty().hasSize(1).containsExactly(consistsOfData);
    }
}