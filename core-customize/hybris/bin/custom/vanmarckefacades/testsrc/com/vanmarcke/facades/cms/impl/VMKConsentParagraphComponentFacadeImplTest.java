package com.vanmarcke.facades.cms.impl;

import com.vanmarcke.core.model.ConsentParagraphComponentModel;
import com.vanmarcke.facades.cms.ConsentParagraphData;
import com.vanmarcke.services.consent.VMKConsentParagraphComponentService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKConsentParagraphComponentFacadeImplTest} class contains the unit tests for the
 * {@link VMKConsentParagraphComponentFacadeImpl} class.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKConsentParagraphComponentFacadeImplTest {

    @Mock
    private VMKConsentParagraphComponentService consentParagraphComponentService;

    @Mock
    private Converter<ConsentParagraphComponentModel, ConsentParagraphData> consentParagraphConverter;

    @InjectMocks
    private VMKConsentParagraphComponentFacadeImpl consentParagraphComponentFacade;

    @Test
    public void testGetConsentParagraphs() {
        ConsentParagraphComponentModel paragraphModel = mock(ConsentParagraphComponentModel.class);

        when(consentParagraphComponentService.getConsentParagraphs()).thenReturn(Collections.singletonList(paragraphModel));

        ConsentParagraphData expectedParagraph = new ConsentParagraphData();

        when(consentParagraphConverter.convertAll(Collections.singletonList(paragraphModel))).thenReturn(Collections.singletonList(expectedParagraph));

        List<ConsentParagraphData> actualParagraphs = consentParagraphComponentFacade.getConsentParagraphs();

        Assertions
                .assertThat(actualParagraphs)
                .containsExactly(expectedParagraph);

        verify(consentParagraphComponentService).getConsentParagraphs();
        verify(consentParagraphConverter).convertAll(Collections.singletonList(paragraphModel));
    }
}