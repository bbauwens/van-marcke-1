/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.vanmarcke.occaddon.controllers;

import com.vanmarcke.occaddon.constants.VanmarckeoccaddonWebConstants;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.request.mapping.annotation.ApiVersion;
import de.hybris.platform.commerceservices.request.mapping.annotation.RequestMappingOverride;
import de.hybris.platform.commercewebservicescommons.dto.product.ProductWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.mapping.FieldSetLevelHelper;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.EnumSet;


/**
 * Controller REST resource which replaces/extends some Product URIs
 */
@RestController
@RequestMapping(value = "/{baseSiteId}/products")
@ApiVersion("v2")
@Api(tags = "Products")
public class VMKB2BProductsController extends VMKBaseController {

    private static final EnumSet<ProductOption> PRODUCT_OPTIONS_SET = EnumSet.of(ProductOption.BASIC,
            ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
            ProductOption.CATEGORIES, ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL,
            ProductOption.DOCUMENTS, ProductOption.BRAND, ProductOption.PROMOTIONS,
            ProductOption.REFERENCE_CONSISTS_OF, ProductOption.SUPPLIER_REFERENCE, ProductOption.BARCODES);

    @Resource(name = "cwsProductFacade")
    private ProductFacade productFacade;

    @Secured({VanmarckeoccaddonWebConstants.ROLE_CUSTOMERGROUP, VanmarckeoccaddonWebConstants.ROLE_CUSTOMERMANAGERGROUP, VanmarckeoccaddonWebConstants.ROLE_TRUSTED_CLIENT})
    @RequestMapping(value = "/{productCode}", method = RequestMethod.GET)
    @RequestMappingOverride(priorityProperty = "99")
    @CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
    @Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,#productCode,#fields)")
    @ResponseBody
    @ApiOperation(nickname = "getProduct", value = "Get product details.", notes = "Returns details of a single product according to a product code.")
    @ApiBaseSiteIdParam
    public ProductWsDTO getProductByCode(
            @ApiParam(value = "The product code", required = true) @PathVariable final String productCode,
            @ApiFieldsParam @RequestParam(required = false, defaultValue = FieldSetLevelHelper.DEFAULT_LEVEL) final String fields) {

        final ProductData product = productFacade.getProductForCodeAndOptions(productCode, PRODUCT_OPTIONS_SET);
        return dataMapper.map(product, ProductWsDTO.class, fields);
    }
}
