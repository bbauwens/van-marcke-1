package com.vanmarcke.occaddon.filter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The {@link VMKAbstractUrlMatchingFilter} class is used to overwrite the
 * {@code de.hybris.platform.ycommercewebservices.v2.filter.AbstractUrlMatchingFilter} class.
 *
 * @author Christiaan Janssen
 * @since 13-91-2021
 */
public abstract class VMKAbstractUrlMatchingFilter extends OncePerRequestFilter {

    private static final String BASE_SITES_ENDPOINT_PATTERN = "^(/basesites)(/[a-zA-Z0-9-]+)?$";

    /**
     * Checks whether the {@code request} matches the given {@code regexp}.
     *
     * @param request the HTTP request
     * @param regexp  the regular expression
     * @return {@code true} in case the {@code request} matches the given {@code regexp}, {@code false} otherwise
     */
    protected boolean matchesUrl(HttpServletRequest request, String regexp) {
        Matcher matcher = getMatcher(request, regexp);
        return matcher.find();
    }

    /**
     * Returns the UID of the base site for the given {@code request}.
     * <p>
     * Returns {@code null} in case the request matches the {@code /basesites/*}.
     *
     * @param request the HTTP request
     * @param regexp  the regular expression
     * @return the UID of the base site
     */
    protected String getBaseSiteValue(HttpServletRequest request, String regexp) {
        Matcher matcher = getMatcher(request, BASE_SITES_ENDPOINT_PATTERN);
        if (matcher.matches()) {
            return null;
        }

        matcher = getMatcher(request, regexp);
        if (matcher.find()) {
            return matcher.group().substring(1);
        }
        return null;
    }

    /**
     * Returns the value for the given {@code request} and {@code regexp}.
     *
     * @param request the HTTP request
     * @param regexp  the regular expresion of the value to find
     * @return the value
     */
    protected String getValue(HttpServletRequest request, String regexp) {
        Matcher matcher = getMatcher(request, regexp);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    /**
     * Returns the value for the given {@code request}, {@code regexp}, and {@code groupName}.
     *
     * @param request   the HTTP request
     * @param regexp    the regular expresion
     * @param groupName the group name of the value to find
     * @return the value
     */
    protected String getValue(HttpServletRequest request, String regexp, String groupName) {
        Matcher matcher = getMatcher(request, regexp);
        if (matcher.find()) {
            return matcher.group(groupName);
        }
        return null;
    }

    /**
     * Returns a {@link Matcher} for the given {@code request} and {@code regexp}.
     *
     * @param request the HTTP request o retrieve the path from
     * @param regexp  the regular expression
     * @return the {@link Matcher} instance
     */
    protected Matcher getMatcher(HttpServletRequest request, String regexp) {
        Pattern pattern = Pattern.compile(regexp);
        String path = getPath(request);
        return pattern.matcher(path);
    }

    /**
     * Returns the path for the given {@code request}.
     *
     * @param request the HTTP request
     * @return the path
     */
    protected String getPath(HttpServletRequest request) {
        return StringUtils.defaultString(request.getPathInfo());
    }
}
