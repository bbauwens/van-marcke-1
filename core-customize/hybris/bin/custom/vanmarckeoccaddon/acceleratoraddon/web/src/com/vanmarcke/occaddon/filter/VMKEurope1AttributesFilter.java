package com.vanmarcke.occaddon.filter;

import com.vanmarcke.core.model.CommerceGroupModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static de.hybris.platform.europe1.constants.Europe1Constants.PARAMS.UPG;
import static de.hybris.platform.europe1.constants.Europe1Constants.PARAMS.UTG;

/**
 * Custom implementation for de.hybris.platform.ycommercewebservices.filter.Europe1AttributesFilter by overriding the bean.
 * Adjusts the functionality for setting the UserTaxGroup and in addition sets the UserPriceGroup on the session.
 *
 * @author marteto
 * @see de.hybris.platform.ycommercewebservices.filter.Europe1AttributesFilter
 * @see com.vanmarcke.storefront.filters.VMKPriceListFilter
 * @deprecated In the future, we might to make sure that the UPG and PPG are coming directly from the current user instead of the site.
 */
@Deprecated
public class VMKEurope1AttributesFilter extends OncePerRequestFilter {

    private final SessionService sessionService;
    private final CMSSiteService cmsSiteService;

    /**
     * Constructor
     *
     * @param sessionService The sessionService
     * @param cmsSiteService The cmsSiteService
     */
    public VMKEurope1AttributesFilter(SessionService sessionService, CMSSiteService cmsSiteService) {
        this.sessionService = sessionService;
        this.cmsSiteService = cmsSiteService;
    }

    /**
     * Finds the CommerceGroup for the current CMSSite and sets the UPG and PPG on the session
     *
     * @param request     The request
     * @param response    The response
     * @param filterChain The filter chain
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {
        CommerceGroupModel commerceGroup = getCommerceGroupForCurrentSite();
        if (commerceGroup != null) {
            setUserPriceGroupOnSession(commerceGroup);
            setUserTaxGroupOnSession(commerceGroup);
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Get the CommerceGroup from the current CMSSite
     *
     * @return The CommerceGroupModel or null
     */
    protected CommerceGroupModel getCommerceGroupForCurrentSite() {
        CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
        return currentSite != null ? currentSite.getCommerceGroup() : null;
    }

    /**
     * Set the UserPriceGroup from the given CommerceGroup on the session
     *
     * @param commerceGroup The CommerceGroup
     */
    protected void setUserPriceGroupOnSession(CommerceGroupModel commerceGroup) {
        final UserPriceGroup userPriceGroup = commerceGroup.getUserPriceGroup();
        if (userPriceGroup != null) {
            sessionService.setAttribute(UPG, userPriceGroup);
        }
    }

    /**
     * Set the UserTaxGroup from the given CommerceGroup on the session
     *
     * @param commerceGroup The CommerceGroup
     */
    protected void setUserTaxGroupOnSession(CommerceGroupModel commerceGroup) {
        final UserTaxGroup userTaxGroup = commerceGroup.getUserTaxGroup();
        if (userTaxGroup != null) {
            sessionService.setAttribute(UTG, userTaxGroup);
        }
    }
}
