package com.vanmarcke.occaddon.controllers;

import com.google.common.collect.Lists;
import com.vanmarcke.services.exception.PickupDeliveryDateException;
import com.vanmarcke.services.exception.UnsupportedDeliveryDateException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commercewebservicescommons.dto.search.pagedata.PaginationWsDTO;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.dto.error.ErrorListWsDTO;
import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.mapping.DataMapper;
import de.hybris.platform.webservicescommons.mapping.FieldSetLevelHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import static de.hybris.platform.webservicescommons.util.YSanitizer.sanitize;

/**
 * The {@link VMKBaseController} class defines the exception handler to be used by all controllers. Extending
 * controllers can add or overwrite the exception handler if needed.
 * <p>
 * Duplicate of de.hybris.platform.ycommercewebservices.v2.controller.BaseController as vanmarckeoccaddon does not
 * depend on ycommercewebservice
 *
 * @author Christiaan Janssen
 * @since 12-01-2021
 */
@Controller
public class VMKBaseController {

    private static final Logger LOG = LoggerFactory.getLogger(VMKBaseController.class);

    protected static final String DEFAULT_PAGE_SIZE = "20";
    protected static final String DEFAULT_CURRENT_PAGE = "0";
    protected static final String BASIC_FIELD_SET = FieldSetLevelHelper.BASIC_LEVEL;
    protected static final String DEFAULT_FIELD_SET = FieldSetLevelHelper.DEFAULT_LEVEL;
    protected static final String HEADER_TOTAL_COUNT = "X-Total-Count";
    protected static final String EXCEPTION_MESSAGE = "An exception occurred!";
    protected static final String INVALID_REQUEST_BODY_ERROR_MESSAGE = "Request body is invalid or missing";

    protected static final String API_COMPATIBILITY_B2C_CHANNELS = "api.compatibility.b2c.channels";

    @Resource(name = "dataMapper")
    protected DataMapper dataMapper;

    /**
     * Handles the {@link ModelNotFoundException} and returns a proper error message.
     *
     * @param ex the exception
     * @return the error message
     */
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler({ModelNotFoundException.class})
    public ErrorListWsDTO handleModelNotFoundException(final Exception ex) {
        LOG.debug(EXCEPTION_MESSAGE, ex);
        return handleErrorInternal(UnknownIdentifierException.class.getSimpleName(), ex.getMessage());
    }

    /**
     * Handles the {@link DuplicateUidException} and returns a proper error message.
     *
     * @param ex the exception
     * @return the error message
     */
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler({DuplicateUidException.class})
    public ErrorListWsDTO handleDuplicateUidException(final DuplicateUidException ex) {
        LOG.debug(EXCEPTION_MESSAGE, ex);
        return handleErrorInternal(DuplicateUidException.class.getSimpleName(), ex.getMessage());
    }

    /**
     * Handles the {@link HttpMessageNotReadableException} and returns a proper error message.
     *
     * @param ex the exception
     * @return the error message
     */
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ErrorListWsDTO handleHttpMessageNotReadableException(final Exception ex) {
        LOG.debug(INVALID_REQUEST_BODY_ERROR_MESSAGE, ex);
        return handleErrorInternal(HttpMessageNotReadableException.class.getSimpleName(), INVALID_REQUEST_BODY_ERROR_MESSAGE);
    }

    /**
     * Handles the {@link UnsupportedDeliveryDateException} and returns a proper error message.
     *
     * @param ex the exception
     * @return the error message
     */
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler({UnsupportedDeliveryDateException.class, PickupDeliveryDateException.class})
    public ErrorListWsDTO handleDeliveryDateExceptions(final Exception ex) {
        LOG.debug(EXCEPTION_MESSAGE, ex);
        return handleErrorInternal(ex.getClass().getSimpleName(), ex.getMessage());
    }

    /**
     * Creates user friendly error for the given {@code type} and {@code message}.
     *
     * @param type    the type of error
     * @param message the error message
     * @return the user friendly error
     */
    protected ErrorListWsDTO handleErrorInternal(final String type, final String message) {
        final ErrorListWsDTO errorListDto = new ErrorListWsDTO();
        final ErrorWsDTO error = new ErrorWsDTO();
        error.setType(type.replace("Exception", "Error"));
        error.setMessage(sanitize(message));
        errorListDto.setErrors(Lists.newArrayList(error));
        return errorListDto;
    }

    /**
     * Validates the given {@code object} using the {@code validator}.
     *
     * @param object     the object to validate
     * @param objectName the name of the object to validate
     * @param validator  the validator to use
     */
    protected void validate(final Object object, final String objectName, final Validator validator) {
        final Errors errors = new BeanPropertyBindingResult(object, objectName);
        validator.validate(object, errors);
        if (errors.hasErrors()) {
            throw new WebserviceValidationException(errors);
        }
    }

    /**
     * Adds pagination field to the 'fields' parameter
     *
     * @param fields the pagination fields
     * @return fields with pagination
     */
    protected String addPaginationField(final String fields) {
        String fieldsWithPagination = fields;

        if (StringUtils.isNotBlank(fieldsWithPagination)) {
            fieldsWithPagination += ",";
        }
        fieldsWithPagination += "pagination";

        return fieldsWithPagination;
    }

    /**
     * Sets the total header count on the {@code response}.
     *
     * @param response      the HTTP response
     * @param paginationDto the pagination data
     */
    protected void setTotalCountHeader(final HttpServletResponse response, final PaginationWsDTO paginationDto) {
        if (paginationDto != null && paginationDto.getTotalResults() != null) {
            response.setHeader(HEADER_TOTAL_COUNT, String.valueOf(paginationDto.getTotalResults()));
        }
    }

    /**
     * Sets the total header count on the {@code response}.
     *
     * @param response      the HTTP response
     * @param paginationDto the pagination data
     */
    protected void setTotalCountHeader(final HttpServletResponse response, final PaginationData paginationDto) {
        if (paginationDto != null) {
            response.setHeader(HEADER_TOTAL_COUNT, String.valueOf(paginationDto.getTotalNumberOfResults()));
        }
    }
}
