package com.vanmarcke.occaddon.validators;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.CartData;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * The {@link VMKB2BPlaceOrderCartValidator} class is a copy of the {@code B2BPlaceOrderCartValidator} class.
 * <p>
 * We've removed the validation of the cost center, because we do not use it.
 *
 * @author Christiaan Janssen
 * @since 27-08-2021
 */
public class VMKB2BPlaceOrderCartValidator implements Validator {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(final Class<?> clazz) {
        return CartData.class.equals(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final Object target, final Errors errors) {
        final CartData cart = (CartData) target;

        if (!cart.isCalculated()) {
            errors.reject("cart.notCalculated");
        }

        if (cart.getDeliveryAddress() == null) {
            errors.reject("cart.deliveryAddressNotSet");
        }

        if (cart.getDeliveryMode() == null) {
            errors.reject("cart.deliveryModeNotSet");
        }

        if (CheckoutPaymentType.CARD.getCode().equals(cart.getPaymentType().getCode())) {
            if (cart.getPaymentInfo() == null) {
                errors.reject("cart.paymentInfoNotSet");
            }
        }

        if (cart.getDeliveryDate() == null) {
            errors.reject("cart.deliveryDateNotSet");
        }
    }
}
