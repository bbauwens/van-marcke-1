package com.vanmarcke.occaddon.constants;

/**
 * Global class for all Vanmarckeoccaddon web constants. You can add global constants for your extension into this class.
 */
public final class VanmarckeoccaddonWebConstants {

    public static final String ROLE_CUSTOMERGROUP = "ROLE_CUSTOMERGROUP";
    public static final String ROLE_TRUSTED_CLIENT = "ROLE_TRUSTED_CLIENT";
    public static final String ROLE_CUSTOMERMANAGERGROUP = "ROLE_CUSTOMERMANAGERGROUP";

    /**
     * private constructor to hide the default implicit one.
     */
    private VanmarckeoccaddonWebConstants() {
        //empty to avoid instantiating this constant class
    }
}
