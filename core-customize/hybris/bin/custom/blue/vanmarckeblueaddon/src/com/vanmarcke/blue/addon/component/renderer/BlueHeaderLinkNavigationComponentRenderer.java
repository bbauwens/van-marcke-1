package com.vanmarcke.blue.addon.component.renderer;

import com.vanmarcke.core.model.HeaderLinkNavigationComponentModel;

/**
 * Custom Header Link Navigation Component Renderer
 */
public class BlueHeaderLinkNavigationComponentRenderer<C extends HeaderLinkNavigationComponentModel> extends AbstractBlueComponentRenderer<C> {

}