package com.vanmarcke.blue.addon.component.renderer;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;

/**
 * Custom Mini Cart Component Renderer
 */
public class BlueMiniCartComponentRenderer<C extends MiniCartComponentModel> extends AbstractBlueComponentRenderer<C> {

}