package com.vanmarcke.blue.addon.facades.impl;

import com.vanmarcke.blue.addon.facades.BlueCheckoutFlowFacade;
import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.facades.delivery.VMKDeliveryInformationFacade;
import com.vanmarcke.facades.order.impl.VMKCheckoutFacadeImpl;
import com.vanmarcke.facades.stock.VMKStockFacade;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import com.vanmarcke.services.delivery.VMKDeliveryMethodService;
import com.vanmarcke.services.order.VMKCommerceCheckoutService;
import com.vanmarcke.services.order.payment.VMKPaymentInfoFactory;
import com.vanmarcke.services.order.payment.VMKPaymentTypeService;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.platform.acceleratorservices.checkout.pci.CheckoutPciStrategy;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

/**
 * Default implementation of {@link BlueCheckoutFlowFacade}
 */
public class DefaultBlueCheckoutFlowFacade extends VMKCheckoutFacadeImpl implements BlueCheckoutFlowFacade {

    private final CheckoutPciStrategy checkoutPciStrategy;
    private final VMKCommerceGroupService commerceGroupService;

    public DefaultBlueCheckoutFlowFacade(VMKPaymentInfoFactory paymentInfoFactory,
                                         VMKPaymentTypeService paymentTypeService,
                                         VMKCommerceCheckoutService checkoutService,
                                         VMKDeliveryMethodService deliveryMethodService,
                                         VMKDeliveryInformationFacade deliveryInfoFacade,
                                         CheckoutPciStrategy checkoutPciStrategy,
                                         VMKCommerceGroupService commerceGroupService,
                                         VMKStockFacade vmkStockFacade,
                                         VMKVariantProductService variantProductService) {
        super(paymentInfoFactory,
                paymentTypeService,
                checkoutService,
                deliveryMethodService,
                deliveryInfoFacade,
                vmkStockFacade,
                variantProductService);

        this.checkoutPciStrategy = checkoutPciStrategy;
        this.commerceGroupService = commerceGroupService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNoDeliveryPointOfService() {
        CartData cartData = getCheckoutCart();
        return hasPickUpItems() && (cartData == null || cartData.getDeliveryPointOfService() == null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPickUpItems() {
        CartModel cart = getCart();
        return cart != null && cart.getDeliveryMode() instanceof PickUpDeliveryModeModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasShippingItems() {
        CartModel cart = getCart();
        return cart != null && cart.getDeliveryMode() instanceof ZoneDeliveryModeModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasValidCountry() {
        CartModel cart = getCart();
        CommerceGroupModel group = commerceGroupService.getCurrentCommerceGroup();
        if (cart != null && cart.getPaymentAddress() != null && group != null) {
            CountryModel currentCommerceGroupCountry = group.getCountry();
            CountryModel paymentCountry = cart.getPaymentAddress().getCountry();

            return currentCommerceGroupCountry.equals(paymentCountry);
        }
        return Boolean.FALSE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CheckoutPciOptionEnum getSubscriptionPciOption() {
        return checkoutPciStrategy.getSubscriptionPciOption();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CartData getCheckoutCart() {
        // This method is overridden for testing purposes.
        return super.getCheckoutCart();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CartModel getCart() {
        // This method is overridden for testing purposes.
        return super.getCart();
    }
}
