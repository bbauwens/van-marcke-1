package com.vanmarcke.blue.addon.facades;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;

/**
 * Extends the default {@link CheckoutFlowFacade}
 */
public interface BlueCheckoutFlowFacade extends CheckoutFlowFacade {

    /**
     * Checks if there is no delivery point of service
     *
     * @return true if there is no delivery point of service
     */
    boolean hasNoDeliveryPointOfService();

    /**
     * Checks if the cart has a valid country
     *
     * @return true if the country is valid for the cart
     */
    boolean hasValidCountry();
}