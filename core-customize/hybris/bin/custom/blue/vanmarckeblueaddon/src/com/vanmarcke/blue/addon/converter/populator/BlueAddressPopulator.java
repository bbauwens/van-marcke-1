package com.vanmarcke.blue.addon.converter.populator;

import com.vanmarcke.blue.addon.form.BlueAddressForm;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;

/**
 * The {@link BlueAddressPopulator} class is used to populate {@link BlueAddressForm} instances with the data of the
 * {@link AddressData} instances.
 *
 * @author Christiaan Janssen
 * @since 23-09-2020
 */
public class BlueAddressPopulator implements Populator<AddressData, BlueAddressForm> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(AddressData source, BlueAddressForm target) {
        target.setAddressId(source.getId());
        target.setTitleCode(source.getTitleCode());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setLine1(source.getLine1());
        target.setLine2(source.getLine2());
        target.setApartment(source.getApartment());
        target.setTownCity(source.getTown());
        target.setPostcode(source.getPostalCode());
        target.setPhone(source.getPhone());
        target.setMobile(source.getMobile());
        target.setCountryIso(source.getCountry().getIsocode());
        target.setSaveInAddressBook(source.isVisibleInAddressBook());
        target.setShippingAddress(source.isShippingAddress());
        target.setBillingAddress(source.isBillingAddress());
        target.setDefaultAddress(source.isDefaultAddress());

        if (source.getRegion() != null) {
            target.setRegionIso(source.getRegion().getIsocode());
        }
    }
}
