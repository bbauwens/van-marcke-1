<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="account-section-header">
    <spring:theme code="text.account.addressBook"/>

    <c:if test="${cmsSite.channel != 'DIY'}">
        <ycommerce:testId code="addressBook_addNewAddress_button">
            <div class="account-section-header-add pull-right">
                <a href="add-address" class="">
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;<spring:theme
                        code="text.account.addressBook.addAddress"/>
                </a>
            </div>
        </ycommerce:testId>
    </c:if>
</div>

<div class="account-addressbook account-list vmb-addressbook">
    <c:if test="${empty addressData}">
        <div class="account-section-content content-empty">
            <spring:theme code="text.account.addressBook.noSavedAddresses"/>
        </div>
    </c:if>

    <c:if test="${not empty addressData}">
        <div class="account-cards card-select">
            <div class="row">
                <c:forEach items="${addressData}" var="address">
                    <div class="col-xs-12 col-sm-6 col-md-4 card">
                        <div class="pull-left">
                            <ul id="vmbAddressData-${fn:escapeXml(address.id)}">
                                <li>
                                    <strong>${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}&nbsp;
                                        <c:choose>
                                            <c:when test="${address.defaultAddress}">
                                                <span class="glyphicon glyphicon-star" title="<spring:theme code="text.default"/>"></span>
                                            </c:when>
                                            <c:otherwise>
                                                <ycommerce:testId code="addressBook_isDefault_button">
                                                    <a class="" href="set-default-address/${fn:escapeXml(address.id)}"
                                                       title="<spring:theme code="text.set.default"/>">
                                                        <span class="glyphicon glyphicon-star-empty"></span>
                                                    </a>
                                                </ycommerce:testId>
                                            </c:otherwise>
                                        </c:choose>
                                    </strong>
                                </li>
                                <li>${fn:escapeXml(address.line1)}&nbsp;${fn:escapeXml(address.line2)}&nbsp;${fn:escapeXml(address.apartment)}</li>
                                <li>${fn:escapeXml(address.postalCode)}&nbsp;${fn:escapeXml(address.town)}</li>
                                <li> ${fn:escapeXml(address.country.name)}</li>
                                <li>${fn:escapeXml(address.phone)}</li>
                                <li>${fn:escapeXml(address.mobile)}</li>
                            </ul>
                        </div>
                        <c:if test="${cmsSite.channel != 'DIY'}">
                            <div class="account-cards-actions pull-left">
                                <ycommerce:testId code="addressBook_editAddress_button">
                                    <a class="action-links" href="edit-address/${fn:escapeXml(address.id)}">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </ycommerce:testId>
                                <ycommerce:testId code="addressBook_removeAddress_button">
                                    <a class="action-links removeAddressFromBookButton" href="#"
                                       data-address-id="${fn:escapeXml(address.id)}">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </ycommerce:testId>
                            </div>
                        </c:if>
                    </div>
                </c:forEach>
            </div>

            <div class="vmb-modal fade" id="vmbDeleteAddressModal" tabindex="-1" role="dialog" aria-labelledby="vmbDeleteAddressModalTitle">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="vmbDeleteAddressModalTitle"><spring:theme code="text.address.delete.popup.title" /></h4>
                        </div>
                        <div class="modal-body">
                            <spring:theme code="text.address.remove.following"/>
                            <div class="address">
                                    <%--- Created by acc.address.js --%>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><spring:theme code="text.button.cancel"/></button>
                            <a href="#" class="btn btn-default" id="btnDeleteSubmit" data-url="<c:url value="remove-address/"/>">
                                <spring:theme code="text.address.delete"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </c:if>
</div>