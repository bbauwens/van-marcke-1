

(function($) {
    'use strict';

    //Click function on the table so that user can browse trough variants
    $(".vmb-facets-reset__btn").click(function(event) {
        event.preventDefault();

        var url = window.location.href.split('?');
        var baseUrl = url[0];

        if(url.length > 1) {
            var params = url[1].split('&');
            for(var i = 0; i < params.length; i++) {
                if(params[i].startsWith("q")) {
                    params.splice(i, 1);
                }
            }
            baseUrl = baseUrl + "?" + params.join("&");
        }

        window.location.href = baseUrl;

        event.stopPropagation();
    });

})(jQuery);