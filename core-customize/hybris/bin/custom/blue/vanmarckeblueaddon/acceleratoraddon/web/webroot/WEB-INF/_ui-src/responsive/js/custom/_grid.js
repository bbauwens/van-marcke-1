
(function($) {
    'use strict';

    if ($(".vmb-grid").length > 0) {

        resizeGridTiles(".vmb-grid");

        $(window).onDelayed('resize', 50, function () {
            resizeGridTiles(".vmb-grid");
        });
    }

})(jQuery);


//Function to create a reponsive css grid that is compatible with IE
function resizeGridTiles(container){
    var tiles = $(container);

    var gridSize = 275;
    if(tiles.data("gridSize") !== undefined){
        gridSize = parseInt(tiles.data("gridSize"));
    }

    var testEl = document.createElement('div');
    // detect support
    var supports_grid = typeof testEl.style.grid === 'string' || typeof testEl.style.msGridColumn === 'string';

    $.each(tiles, function(){
        resizeGridTilesSingle($(this), supports_grid, gridSize);
    });
}

//Helper function for resizeGridTiles
function resizeGridTilesSingle(containerElm, supports_grid, gridSize) {

    var tileElements = containerElm.children().not("script");
    var gridStructure = [];

    //calc how many items the grid can contain
    var columnCount = Math.floor(containerElm.width()/gridSize);

    if(supports_grid){

        if(columnCount > 1){

            if(columnCount > 4){
                columnCount = 4;
            }

            var ieCss = "minmax("+gridSize+"px, 1fr)";

            for(var i = 1; i < columnCount; i++){
                ieCss +=" 15px minmax("+gridSize+"px, 1fr)";
            }

            var ieCssRows = "auto";

            for(var i = 1; i < Math.ceil(tileElements.length/columnCount); i++){
                ieCssRows +=" 15px auto";
            }

            containerElm.css("-ms-grid-columns", ieCss);
            containerElm.css("-ms-grid-rows", ieCssRows);
            containerElm.css("grid-template-columns", "repeat("+ columnCount +", minmax("+gridSize+"px, 1fr))");


            gridStructure = buildGridStructure(tileElements.length, columnCount);

            $.each(tileElements, function(i) {
                $(this).css({"-ms-grid-column":  gridStructure[i].column, "-ms-grid-row": gridStructure[i].row});
            });

        }else{
            containerElm.css("-ms-grid-columns", "1fr");
            containerElm.css("grid-template-columns", "1fr");

            var ieCssRows = "auto";

            for(var i = 1; i < tileElements.length; i++){
                ieCssRows +=" 15px auto";
            }

            containerElm.css("-ms-grid-rows", ieCssRows);

            $.each(tileElements, function(i) {
                $(this).css("-ms-grid-column", "1").css("-ms-grid-row", ""+ (1 + (i*2)));
            });
        }

    }else{
        containerElm.css("margin", "0 15px 15px 0").css("max-width", gridSize + "px");
    }

}

//Create an array structure for the CSS Grid
function buildGridStructure(total, columnCount){

    var gridStructure = [];

    for(var r = 1; r <= (Math.ceil(total/columnCount)*2); r+= 2){
        for(var i = 1; i < (columnCount*2); i += 2){
            gridStructure.push({"column" : ""+i, "row": ""+r});
        }
    }

    return gridStructure;
}