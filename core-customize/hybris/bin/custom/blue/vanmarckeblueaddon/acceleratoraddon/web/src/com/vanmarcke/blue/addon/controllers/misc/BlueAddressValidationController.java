package com.vanmarcke.blue.addon.controllers.misc;

import com.vanmarcke.facades.address.validation.VMKAddressValidationFacade;
import com.vanmarcke.facades.addressvalidation.data.AddressValidationResponseData;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.commercefacades.user.data.AddressData;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Views.Fragments.AddressValidation.ADDRESS_VALIDATION_POPUP;

/**
 * Controller for Address Validation functionality, which is not specific to a page.
 */
@Controller
public class BlueAddressValidationController extends AbstractController {

    @Resource(name = "vmkAddressValidationFacade")
    private VMKAddressValidationFacade addressValidationFacade;

    /**
     * Endpoint to validate a provided address.
     *
     * @param addressData address to be validated
     * @param model       model to put the result on
     * @return view to use for rendering
     */
    @PostMapping(value = "/av", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String validateAddress(@RequestBody AddressData addressData, Model model) {
        AddressValidationResponseData responseData = addressValidationFacade.validateAddress(addressData);

        model.addAttribute("originalAddressData", addressData);
        model.addAttribute("exactMatch", responseData.isExactMatch());

        if (responseData.getErrorCode() != null && responseData.getErrorCode() != 0) {
            model.addAttribute("validationErrorDescription", responseData.getErrorDescription());
            model.addAttribute("validationErrorCode", responseData.getErrorCode());
        } else {
            model.addAttribute("validationResult", responseData.getResultList());
        }

        return ADDRESS_VALIDATION_POPUP;
    }

}
