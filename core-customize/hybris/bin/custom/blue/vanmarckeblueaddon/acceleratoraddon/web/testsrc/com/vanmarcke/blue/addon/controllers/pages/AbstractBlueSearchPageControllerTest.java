package com.vanmarcke.blue.addon.controllers.pages;

import com.vanmarcke.blue.addon.constants.VanmarckeblueaddonWebConstants.ProductListViewType;
import com.vanmarcke.facades.product.VMKProductFacade;
import com.vanmarcke.facades.search.VMKSolrProductSearchFacade;
import com.vanmarcke.facades.search.data.VariantSearchStateData;
import com.vanmarcke.storefront.security.cookie.EnhancedCookieGenerator;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.FilterQueryOperator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractBlueSearchPageControllerTest {

    @Mock
    private EnhancedCookieGenerator plpListViewCookieGenerator;

    @Mock
    private CommerceCategoryService commerceCategoryService;

    @Mock
    private VMKSolrProductSearchFacade<ProductData> productSearchFacade;

    @Mock
    private VMKProductFacade vmkProductFacade;

    @InjectMocks
    private AbstractBlueSearchPageController abstractBlueSearchPageController;

    @Captor
    private ArgumentCaptor<VariantSearchStateData> variantSearchStateDataArgumentCaptor;

    @Test
    public void testGetAndStorePLPViewType_newViewType() {
        ProductListViewType viewType = ProductListViewType.List;
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        ProductListViewType newViewType = abstractBlueSearchPageController.getAndStorePLPViewType(viewType, request, response);
        verify(plpListViewCookieGenerator, times(1)).addCookie(response, ProductListViewType.List.name());
        Assert.assertEquals(ProductListViewType.List, newViewType);
    }

    @Test
    public void testGetAndStorePLPViewType_viewTypeSession() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        Cookie cookie = mock(Cookie.class);
        when(cookie.getName()).thenReturn("view-type");
        when(cookie.getValue()).thenReturn("List");
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});
        ProductListViewType viewType = abstractBlueSearchPageController.getAndStorePLPViewType(null, request, response);
        verify(cookie, atLeast(1)).getName();
        Assert.assertEquals(ProductListViewType.List, viewType);
    }

    @Test
    public void testGetAndStorePLPViewType_noViewType() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        when(request.getCookies()).thenReturn(new Cookie[0]);
        ProductListViewType viewType = abstractBlueSearchPageController.getAndStorePLPViewType(null, request, response);
        verify(plpListViewCookieGenerator, times(1)).addCookie(response, ProductListViewType.Grid.name());
        Assert.assertEquals(ProductListViewType.Grid, viewType);
    }

    @Test
    public void testPopulateProductVariants() {
        // given
        ProductSearchPageData<SearchStateData, ProductData> searchPageData = mock(ProductSearchPageData.class);
        ProductData productData = mock(ProductData.class);
        ProductSearchPageData<SearchStateData, ProductData> variantsSearchPageData = mock(ProductSearchPageData.class);
        String searchQuery = "searchQuery";
        ProductData solrVariant = mock(ProductData.class);
        when(variantsSearchPageData.getResults()).thenReturn(List.of(solrVariant));
        when(searchPageData.getResults()).thenReturn(List.of(productData));
        when(productData.getBaseProduct()).thenReturn("baseProduct");
        when(productSearchFacade.searchSolrProductVariants(any(VariantSearchStateData.class))).thenReturn(variantsSearchPageData);

        // when
        abstractBlueSearchPageController.populateProductVariants(searchPageData, searchQuery, "01-02-02");

        // then
        verify(productSearchFacade, times(1)).searchSolrProductVariants(variantSearchStateDataArgumentCaptor.capture());
        Assert.assertEquals("searchQuery", variantSearchStateDataArgumentCaptor.getValue().getQuery().getValue());
        Assert.assertEquals("baseProductCode", variantSearchStateDataArgumentCaptor.getValue().getQuery().getFilterQueries().get(0).getKey());
        Assert.assertEquals("baseProduct", variantSearchStateDataArgumentCaptor.getValue().getQuery().getFilterQueries().get(0).getValues().iterator().next());
        Assert.assertEquals(FilterQueryOperator.AND, variantSearchStateDataArgumentCaptor.getValue().getQuery().getFilterQueries().get(0).getOperator());
        verify(vmkProductFacade, times(1)).enrichProductDataWithSolrVariants(productData, List.of(solrVariant), true);
        verify(vmkProductFacade, times(1)).enrichProductDataWithBrandLogo(productData);
    }


}