<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/cart" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>

<%-- Verified that there's a pre-existing bug regarding the setting of showTax; created issue  --%>
<div class="row">
    <div class="col-xs-12 col-md-4 col-lg-4">
        <div class="cart-voucher">
            <cart:cartVoucher cartData="${cartData}"/>
        </div>
    </div>
    <div class="col-xs-12 col-md-2 col-lg-4">

    </div>
    <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="cart-totals vmb-cart-totals">
            <c:if test="${cmsSite.channel != 'DIY' || !isAnonymousUser}">
                <cart:cartTotals cartData="${cartData}" showTax="true"/>
            </c:if>

            <c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
            <div class="vmb-cart-totals__checkout">
                <a href="${fn:escapeXml(checkoutUrl)}" class="btn btn-primary">
                    <spring:theme code="checkout.checkout"/>
                </a>
            </div>
        </div>
    </div>
</div>