<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

dataLayer.push(
<json:object>
    <json:property name="event" value="detail"/>
    <json:object name="ecommerce">
        <json:property name="currencyCode" value="${currentCurrency.isocode}"/>
        <json:object name="detail">
            <json:array name="products">
                <json:object>
                    <json:property name="name" value="${fn:escapeXml(product.analyticsData.name)}"/>
                    <json:property name='id'>${product.code}</json:property>
                    <json:property name="price" value="${product.price.value}"/>
                    <json:property name="category" value="${product.analyticsData.category}"/>
                    <json:property name="brand" value="${product.analyticsData.brand}"/>
                </json:object>
            </json:array>
        </json:object>
    </json:object>
</json:object>
);