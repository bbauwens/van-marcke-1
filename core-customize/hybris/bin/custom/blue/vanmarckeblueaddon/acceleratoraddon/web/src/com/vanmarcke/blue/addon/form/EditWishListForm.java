package com.vanmarcke.blue.addon.form;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.List;

/**
 * Form used to update properties of a wish list.
 */
public class EditWishListForm {

    private String name;
    private String description;
    private List<OrderEntryData> entries;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OrderEntryData> getEntries() {
        return entries;
    }

    public void setEntries(List<OrderEntryData> entries) {
        this.entries = entries;
    }
}
