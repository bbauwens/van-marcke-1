package com.vanmarcke.blue.addon.interceptors;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * Handler to disable favorite store in checkout
 */
public class BlueFavoriteStoreBeforeViewHandler implements BeforeViewHandler {

    private List<String> pagesToDisableFavoriteStore;

    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView) throws Exception {
        if (pagesToDisableFavoriteStore.stream().anyMatch(page -> request.getRequestURI().contains(page))) {
            modelAndView.addObject("disableFavoriteStore", true);
        }
    }

    protected List<String> getPagesToDisableFavoriteStore() {
        return pagesToDisableFavoriteStore;
    }

    @Required
    public void setPagesToDisableFavoriteStore(final List<String> pagesToDisableFavoriteStore) {
        this.pagesToDisableFavoriteStore = pagesToDisableFavoriteStore;
    }
}
