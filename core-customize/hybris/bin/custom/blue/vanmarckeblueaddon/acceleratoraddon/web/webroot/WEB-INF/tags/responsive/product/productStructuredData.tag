<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="productStructuredData" required="true" type="java.lang.String" %>


<c:if test="${not empty productStructuredData}">
<script type="application/ld+json">
${productStructuredData}
</script>
</c:if>