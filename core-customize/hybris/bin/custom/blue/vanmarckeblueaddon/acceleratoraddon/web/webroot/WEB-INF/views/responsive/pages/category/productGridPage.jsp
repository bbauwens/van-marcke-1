<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/storepickup" %>
<template:page pageTitle="${pageTitle}">

    <cms:pageSlot position="Section1" var="feature" element="div" class="product-grid-section1-slot">
        <cms:component component="${feature}" element="div"
                       class="yComponentWrapper map product-grid-section1-component"/>
    </cms:pageSlot>

    <div class="row">
        <cms:pageSlot position="BlueGridPageTopContentSlot" var="content" limit="1">
            <cms:component component="${content}" element="div" class="col-xs-12"/>
        </cms:pageSlot>
    </div>

    <div class="row vmb-plp-wrapper">
        <div class="col-xs-3 vmb-plp-facets">
            <cms:pageSlot position="ProductLeftRefinements" var="feature" element="div"
                          class="product-grid-left-refinements-slot">
                <cms:component component="${feature}" element="div"
                               class="yComponentWrapper product-grid-left-refinements-component"/>
            </cms:pageSlot>
        </div>
        <div class="col-sm-12 col-md-9 vmb-plp-products">
            <cms:pageSlot position="ProductGridSlot" var="feature" element="div" class="product-grid-right-result-slot">
                <cms:component component="${feature}" element="div"
                               class="product__list--wrapper yComponentWrapper product-grid-right-result-component"/>
            </cms:pageSlot>
        </div>
    </div>
    <div class="row">
        <cms:pageSlot position="BlueGridPageBottom1LeftContentSlot" var="content" limit="1">
            <cms:component component="${content}" element="div" class="col-xs-12 col-sm-6"/>
        </cms:pageSlot>
        <cms:pageSlot position="BlueGridPageBottom1RightContentSlot" var="content" limit="1">
            <cms:component component="${content}" element="div" class="col-xs-12 col-sm-6"/>
        </cms:pageSlot>
    </div>
    <div class="row">
        <cms:pageSlot position="BlueGridPageBottom2ContentSlot" var="content" limit="1">
            <cms:component component="${content}" element="div" class="col-xs-12"/>
        </cms:pageSlot>
    </div>
    <storepickup:pickupStorePopup/>
</template:page>