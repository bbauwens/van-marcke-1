ACC.product = {

    _autoload: [
        "bindToAddToCartForm",
        "enableStorePickupButton",
        "enableVariantSelectors",
        "bindFacets"
    ],

    bindFacets: function () {
        $(document).on("click", ".js-show-facets", function (e) {
            e.preventDefault();

            $("#vmbMobileRefineModal .modal-body").html($(".js-product-facet").clone(true, true));
            $("#vmbMobileRefineModal .modal-body .js-product-facet").removeClass("hidden-sm hidden-xs");

            $("#vmbMobileRefineModal").modal()
                .on('hidden.bs.modal', function (e) {
                    $("#vmbMobileRefineModal .modal-body").empty();
                });
        });

        $(document).on("click", "#vmbMobileRefineModal .js-product-facet .js-facet-name", function (e) {
            e.preventDefault();
            $(this).parent().toggleClass("active");
        });

        $('.js-facet-name').click( function (e) {
            target = $($(this).data('target'));
            if (target.is(":visible")) {
                $(this).children('span').removeClass('glyphicon-chevron-up');
                $(this).children('span').addClass('glyphicon-chevron-down');
                target.slideUp();
            } else {
                $(this).children('span').removeClass('glyphicon-chevron-down');
                $(this).children('span').addClass('glyphicon-chevron-up');
                target.slideDown();
            }
        });

        $('.vmb-switch-plp-view').click(function (e) {
            viewSelector = $(this).children('span');
            existingViewType = $(this).data('view-type');

            if (existingViewType === 'List' && $(this).attr('id') === 'list-view-button') {
                return;
            }

            if (existingViewType === 'Grid' && $(this).attr('id')  === 'grid-view-button') {
                return;
            }

            if (existingViewType === 'List') {
                window.location.replace(ACC.product.addPLPViewQueryParam(window.location.href, "viewType=Grid"));
            } else {
                window.location.replace(ACC.product.addPLPViewQueryParam(window.location.href, "viewType=List"));
            }
        });
    },

    addPLPViewQueryParam: function(url, param) {
        if (url.endsWith('#')) {
            url = url.slice(0, -1);
        }
        if (!url.includes('?')) {
            return url + "?" + param;
        } else if (url.includes('viewType')) {
            return url.replace(/viewType=(Grid|List)/, param);
        } else {
            return url + "&" + param;
        }
    },

    enableAddToCartButton: function () {
        $('.js-enable-btn').each(function () {
            if (!($(this).hasClass('outOfStock') || $(this).hasClass('out-of-stock'))) {
                $(this).prop("disabled", false);
            }
        });
    },

    enableVariantSelectors: function () {
        $('.variant-select').prop("disabled", false);
    },

    bindToAddToCartForm: function () {
        var addToCartForm = $('.add_to_cart_form');
        addToCartForm.ajaxForm({
            beforeSubmit: ACC.product.showRequest,
            success: ACC.product.displayAddToCartPopup
        });
        setTimeout(function () {
            $ajaxCallEvent = true;
        }, 2000);
    },
    showRequest: function (arr, $form, options) {
        if ($ajaxCallEvent) {
            $ajaxCallEvent = false;
            return true;
        }
        return false;

    },

    bindToAddToCartStorePickUpForm: function () {
        var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
        addToCartStorePickUpForm.ajaxForm({success: ACC.product.displayAddToCartPopup});
    },

    enableStorePickupButton: function () {
        $('.js-pickup-in-store-button').prop("disabled", false);
    },

    displayAddToCartPopup: function (cartResult, statusText, xhr, formElement) {
        $ajaxCallEvent = true;
        $('#addToCartLayer').remove();
        if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
            ACC.minicart.updateMiniCartDisplay();
        }

        if (cartResult && cartResult.cartAnalyticsData) {
            for (var i = 0; i < cartResult.cartAnalyticsData.length; i++) {
                var entry = cartResult.cartAnalyticsData[i];

                var code = entry.productCode;
                var quantity = entry.quantity;

                if (code && quantity > 0) {
                    var data = {
                        "cartCode": entry.cartCode,
                        "productCode": entry.productCode,
                        "quantity": entry.quantity,
                        "productPrice": entry.productPostPrice,
                        "productName": entry.productName
                    };
                    ACC.track.trackAddToCart(code, quantity, data);
                }
            }
        }
    }
};

$(document).ready(function () {
    $ajaxCallEvent = true;
    ACC.product.enableAddToCartButton();
});