<%@ tag import="com.vanmarcke.services.util.URIUtils" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hideHeaderLinks" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/nav" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/common/header" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="currentUrl" value='<%=URIUtils.normalize(request.getAttribute("javax.servlet.forward.request_uri").toString())%>'/>

<header class="js-mainHeader vmb-main-header vmb-main-header--checkout">
    <div class="vmb-site-logo col-xs-12 col-md-1">
        <button class="btn vmb-toggle-sm-navigation hidden-md hidden-lg" type="button">
            <span class="glyphicon glyphicon-align-justify"></span>
        </button>
        <a class="logo" href="<c:url value="/"/>"></a>
    </div>
    <div class="vmb-site-nav col-xs-12 col-md-11">
        <nav class="navigation navigation--top hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <div class="nav__left"></div>
                    <div class="nav__right">
                        <ul class="nav__links nav__links--account">
                            <c:if test="${uiExperienceOverride}">
                                <li class="backToMobileLink">
                                    <c:url value="/_s/ui-experience?level=" var="backToMobileStoreUrl"/>
                                    <a href="${fn:escapeXml(backToMobileStoreUrl)}">
                                        <spring:theme code="text.backToMobileStore"/>
                                    </a>
                                </li>
                            </c:if>
                            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                <c:set var="maxNumberChars" value="25"/>
                                <c:if test="${fn:length(user.firstName) gt maxNumberChars}">
                                    <c:set target="${user}" property="firstName"
                                           value="${fn:substring(user.firstName, 0, maxNumberChars)}..."/>
                                </c:if>
                                <li class="logged_in js-logged_in js-myAccount-toggle vmb-my-account"
                                    data-toggle="collapse" data-parent=".nav__right">
                                    <ycommerce:testId code="header_LoggedUser">
                                        <spring:theme code="header.welcome"
                                                      arguments="${user.firstName},${user.lastName}"/>
                                    </ycommerce:testId>
                                    <span class="glyphicon glyphicon-chevron-down"></span>
                                    <cms:pageSlot position="HeaderLinks" var="link">
                                        <cms:component component="${link}" element=""/>
                                    </cms:pageSlot>
                                </li>
                            </sec:authorize>
                            <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                                <li class="liOffcanvas">
                                    <ycommerce:testId code="header_Login_link">
                                        <c:choose>
                                            <c:when test="${disableSmartEdit}">
                                                <c:set value="/elisionsamlsinglesignon/saml${currentUrl}" var="loginUrl"/>
                                            </c:when>
                                            <c:otherwise>
                                                <c:set value="/login" var="loginUrl"/>
                                            </c:otherwise>
                                        </c:choose>
                                        <a href="${fn:escapeXml(loginUrl)}">
                                            <spring:theme code="header.link.login"/>
                                        </a>
                                    </ycommerce:testId>
                                </li>
                            </sec:authorize>
                            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                <li class="liOffcanvas">
                                    <ycommerce:testId code="header_signOut">
                                        <c:choose>
                                            <c:when test="${disableSmartEdit}">
                                                <c:set value="/elisionsamlsinglesignon/saml/logout" var="logoutUrl"/>
                                            </c:when>
                                            <c:otherwise>
                                                <c:set value="/logout" var="logoutUrl"/>
                                            </c:otherwise>
                                        </c:choose>
                                        <a href="${fn:escapeXml(logoutUrl)}">
                                            <spring:theme code="header.link.logout"/>
                                        </a>
                                    </ycommerce:testId>
                                </li>
                            </sec:authorize>
                        </ul>
                        <header:languageSelector languages="${languages}" currentLanguage="${currentLanguage}"/>
                    </div>
                </div>
        </nav>
        <%-- a hook for the my account links in desktop/wide desktop--%>
        <div class="hidden-xs hidden-sm js-secondaryNavAccount collapse" id="accNavComponentDesktopOne">
            <ul class="nav__links">
            </ul>
        </div>
        <div class="hidden-xs hidden-sm js-secondaryNavCompany collapse" id="accNavComponentDesktopTwo">
            <ul class="nav__links js-nav__links">
            </ul>
        </div>
        <nav class="navigation navigation--middle js-navigation--middle">
            <div class="container-fluid">
                <div class="row">

                </div>
                <div class="row desktop__nav">
                    <div class="nav__left col-xs-12 col-sm-7">
                        <div class="row">
                            <div class="col-sm-2 hidden-xs visible-sm">

                            </div>
                            <div class="col-sm-10 col-md-12 vmb-main-header__quicknav">
                                <nav class="navigation navigation--bottom js_navigation--bottom js-enquire-offcanvas-navigation"
                                     role="navigation">
                                    <div class="vmb-mobile-logo hidden-md hidden-lg">
                                        <div class="vmb-site-logo">
                                            <a class="logo" href="<c:url value="/"/>"></a>
                                        </div>
                                    </div>
                                    <div class="vmb-mobile-account hidden-md hidden-lg">
                                        <button type="button" class="btn"><spring:theme
                                                code="header.link.account"/><span
                                                class="glyphicon glyphicon-triangle-bottom"></span></button>
                                        <ul class="vmb-mobile-account__nav">
                                            <%--Dynamically generated by 'header.js"--%>
                                            <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                                                <li>
                                                    <c:set value="/elisionsamlsinglesignon/saml${currentUrl}" var="loginUrl"/>
                                                    <a href="${fn:escapeXml(loginUrl)}">
                                                        <spring:theme code="header.link.login"/>
                                                    </a>
                                                </li>
                                            </sec:authorize>
                                            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                                <li>
                                                    <c:set value="/elisionsamlsinglesignon/saml/logout" var="logoutUrl"/>
                                                    <a href="${fn:escapeXml(logoutUrl)}">
                                                        <spring:theme code="header.link.logout"/>
                                                    </a>
                                                </li>
                                            </sec:authorize>
                                        </ul>
                                    </div>
                                    <div class="vmb-favorite-store__mobile">
                                        <%--Dynamically generated by 'header.js"--%>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <a id="skiptonavigation"></a>
    </div>
</header>