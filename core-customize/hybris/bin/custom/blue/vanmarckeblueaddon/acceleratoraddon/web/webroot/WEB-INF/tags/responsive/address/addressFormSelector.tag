<%@ attribute name="supportedCountries" required="true" type="java.util.List" %>
<%@ attribute name="regions" required="true" type="java.util.List" %>
<%@ attribute name="country" required="false" type="java.lang.String" %>
<%@ attribute name="cancelUrl" required="false" type="java.lang.String" %>
<%@ attribute name="addressBook" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<c:if test="${not empty deliveryAddresses}">
    <a href="#" type="button" class="vmb-btn-address-book vmb-address-selector"><spring:theme
            code="checkout.checkout.multi.deliveryAddress.viewAddressBook"/></a>
    <div id="addressbook">
        <spring:url var="selectDeliveryAddressUrl" value="{contextPath}/checkout/multi/delivery-address/select"
                    htmlEscape="false">
            <spring:param name="contextPath" value="${request.contextPath}"/>
        </spring:url>
        <c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
            <div class="addressEntry">
                <form action="${fn:escapeXml(selectDeliveryAddressUrl)}" method="GET">
                    <input type="hidden" name="selectedAddressCode" value="${fn:escapeXml(deliveryAddress.id)}"/>
                    <ul>
                        <li>
                            <strong>
                                <c:if test="${not empty deliveryAddress.title}">${fn:escapeXml(deliveryAddress.title)}&nbsp;</c:if>
                                <c:choose>
                                    <c:when test="${not empty deliveryAddress.firstName or not empty deliveryAddress.lastName}">
                                        ${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}
                                    </c:when>
                                    <c:otherwise>
                                        ${fn:escapeXml(deliveryAddress.companyName)}
                                    </c:otherwise>
                                </c:choose>
                            </strong>
                            <br>
                                ${fn:escapeXml(deliveryAddress.line1)}&nbsp;
                                ${fn:escapeXml(deliveryAddress.line2)}
                            <br>
                                ${fn:escapeXml(deliveryAddress.town)}
                            <c:if test="${not empty deliveryAddress.region.name}">
                                &nbsp;${fn:escapeXml(deliveryAddress.region.name)}
                            </c:if>
                            <br>
                                ${fn:escapeXml(deliveryAddress.country.name)}&nbsp;
                                ${fn:escapeXml(deliveryAddress.postalCode)}
                        </li>
                    </ul>
                    <button type="submit" class="btn btn-default">
                        <spring:theme code="checkout.multi.deliveryAddress.useThisAddress"/>
                    </button>
                </form>
            </div>
        </c:forEach>
    </div>
</c:if>
<c:if test="${empty addressFormEnabled or addressFormEnabled}">
    <a href="#" type="button" class="vmb-btn-new-address vmb-address-selector"><spring:theme
            code="checkout.checkout.multi.deliveryAddress.newAddress"/></a>

    <div class="vmb-checkout-new-address">

        <form:form method="post" modelAttribute="addressForm">
            <form:hidden path="addressId" class="add_edit_delivery_address_id"
                         status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}"/>
            <form:hidden path="countryIso"/>
            <div id="i18nAddressForm" class="i18nAddressForm">
                <address:blueAddressFormElements/>
            </div>
            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                <div class="checkbox">
                    <c:choose>
                        <c:when test="${showSaveToAddressBook}">
                            <formElement:formCheckbox idKey="saveAddressInMyAddressBook"
                                                      labelKey="checkout.summary.deliveryAddress.saveAddressInMyAddressBook"
                                                      path="saveInAddressBook" inputCSS="add-address-left-input"
                                                      labelCSS="add-address-left-label" mandatory="true"/>
                        </c:when>
                        <c:when test="${not addressBookEmpty && not isDefaultAddress}">
                            <ycommerce:testId code="editAddress_defaultAddress_box">
                                <formElement:formCheckbox idKey="defaultAddress"
                                                          labelKey="address.default" path="defaultAddress"
                                                          inputCSS="add-address-left-input"
                                                          labelCSS="add-address-left-label" mandatory="true"/>
                            </ycommerce:testId>
                        </c:when>
                    </c:choose>
                </div>
            </sec:authorize>
            <div id="addressform_button_panel" class="form-actions">
                <c:choose>
                    <c:when test="${edit eq true && not addressBook}">
                        <ycommerce:testId code="multicheckout_saveAddress_button">
                            <button
                                    class="positive right change_address_button show_processing_message"
                                    type="submit">
                                <spring:theme code="checkout.multi.saveAddress"/>
                            </button>
                        </ycommerce:testId>
                    </c:when>
                    <c:when test="${addressBook eq true}">
                        <div class="accountActions">
                            <div class="row">
                                <div class="col-sm-6 col-sm-push-6 accountButtons">
                                    <ycommerce:testId code="editAddress_saveAddress_button">
                                        <button class="btn btn-primary btn-block change_address_button show_processing_message"
                                                type="submit">
                                            <spring:theme code="text.button.save"/>
                                        </button>
                                    </ycommerce:testId>
                                </div>
                                <div class="col-sm-6 col-sm-pull-6 accountButtons">
                                    <ycommerce:testId code="editAddress_cancelAddress_button">
                                        <c:url value="${cancelUrl}" var="cancel"/>
                                        <a class="btn btn-block btn-default" href="${fn:escapeXml(cancel)}">
                                            <spring:theme code="text.button.cancel"/>
                                        </a>
                                    </ycommerce:testId>
                                </div>
                            </div>
                        </div>
                    </c:when>
                </c:choose>
            </div>
        </form:form>

        <div class="text-right">
            <button id="addressSubmit" type="button" class="btn btn-default checkout-next"><spring:theme
                    code="checkout.multi.deliveryAddress.continue"/></button>
        </div>
    </div>
</c:if>