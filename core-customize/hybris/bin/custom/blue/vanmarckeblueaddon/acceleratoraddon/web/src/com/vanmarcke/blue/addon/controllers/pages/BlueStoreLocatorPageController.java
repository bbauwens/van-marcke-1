package com.vanmarcke.blue.addon.controllers.pages;

import com.vanmarcke.facades.search.VMKPointOfServiceLocatorSearchFacade;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorservices.store.data.UserLocationData;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.StorefinderBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.StoreFinderForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Views.Pages.StoreFinder.STORE_FINDER_SEARCH_PAGE;

/**
 * Controller for store locator search and detail pages.
 */

@Controller
@RequestMapping(value = "/store-finder")
public class BlueStoreLocatorPageController extends AbstractSearchPageController {

    private static final Logger LOG = Logger.getLogger(BlueStoreLocatorPageController.class);

    private static final String STORE_FINDER_CMS_PAGE_LABEL = "/store-finder";
    private static final String GOOGLE_API_KEY_ID = "googleApiKey";
    private static final String GOOGLE_API_VERSION = "googleApiVersion";

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "storefinderBreadcrumbBuilder")
    private StorefinderBreadcrumbBuilder storefinderBreadcrumbBuilder;

    @Resource(name = "vmkPointOfServiceLocatorSearchFacade")
    private VMKPointOfServiceLocatorSearchFacade pointOfServiceLocatorSearchFacade;

    @Resource(name = "customerLocationService")
    private CustomerLocationService customerLocationService;


    @ModelAttribute("googleApiVersion")
    public String getGoogleApiVersion() {
        return this.configurationService.getConfiguration().getString(GOOGLE_API_VERSION);
    }

    @ModelAttribute("googleApiKey")
    public String getGoogleApiKey(final HttpServletRequest request) {
        final String googleApiKey = getHostConfigService().getProperty(GOOGLE_API_KEY_ID, request.getServerName());
        if (StringUtils.isEmpty(googleApiKey)) {
            LOG.warn("No Google API key found for server: " + request.getServerName());
        }
        return googleApiKey;
    }

    // Method to get the empty search form
    @RequestMapping(method = RequestMethod.GET)
    public String getStoreFinderPage(final Model model) throws CMSItemNotFoundException {
        setUpStoreFinderForm(model);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, this.storefinderBreadcrumbBuilder.getBreadcrumbs());
        storeCmsPageInModel(model, getStoreFinderPage());
        setUpMetaDataForContentPage(model, (ContentPageModel) getStoreFinderPage());
        return getViewForPage(model);
    }

    @RequestMapping(method = RequestMethod.GET, params = "q")
    public String findStores(@RequestParam(value = "page", defaultValue = "0") final int page, // NOSONAR
                             @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                             @RequestParam(value = "sort", required = false) final String sortCode,
                             @RequestParam(value = "q") final String locationQuery,
                             @RequestParam(value = "latitude", required = false) final Double latitude,
                             @RequestParam(value = "longitude", required = false) final Double longitude, final StoreFinderForm storeFinderForm,
                             final Model model, final BindingResult bindingResult) throws CMSItemNotFoundException {
        final String sanitizedSearchQuery = XSSFilterUtil.filter(locationQuery);
        GeoPoint geoPoint = null;
        if (latitude != null && longitude != null) {
            geoPoint = new GeoPoint();
            geoPoint.setLatitude(latitude);
            geoPoint.setLongitude(longitude);
        }
        if (StringUtils.isNotBlank(sanitizedSearchQuery)) {
            setUpMetaData(sanitizedSearchQuery, model);
            setUpPageTitle(sanitizedSearchQuery, model);
        }
        setUpStoreFinderForm(model);
        setUpSearchResults(geoPoint, sanitizedSearchQuery, createPageableData(page, getStoreLocatorPageSize(), sortCode, showMode), model);

        storeCmsPageInModel(model, getStoreFinderPage());
        return STORE_FINDER_SEARCH_PAGE;
    }

    // setup methods to populate the model
    protected void setUpMetaData(final String locationQuery, final Model model) {
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(locationQuery);
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getSiteName()
                + " "
                + getMessageSource().getMessage("storeFinder.meta.description.results", null, "storeFinder.meta.description.results",
                getI18nService().getCurrentLocale()) + " " + locationQuery);
        super.setUpMetaData(model, metaKeywords, metaDescription);
    }

    protected void setUpNoResultsErrorMessage(final Model model, final StoreFinderSearchPageData<PointOfServiceData> searchResult) {
        if (searchResult.getResults().isEmpty()) {
            GlobalMessages.addErrorMessage(model, "storelocator.error.no.results.subtitle");
        }
    }

    protected void setUpPageData(final Model model, final StoreFinderSearchPageData<PointOfServiceData> searchResult,
                                 final List<Breadcrumb> breadCrumbsList) {
        populateModel(model, searchResult, ShowMode.Page);
        model.addAttribute("locationQuery", StringEscapeUtils.escapeHtml(searchResult.getLocationText()));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbsList);
    }

    protected void setUpSearchResults(final GeoPoint geoPoint, final String searchQuery, final PageableData pageableData, final Model model) {
        final StoreFinderSearchPageData<PointOfServiceData> searchResult = this.pointOfServiceLocatorSearchFacade.search(searchQuery, geoPoint, pageableData);

        final GeoPoint newGeoPoint = new GeoPoint();
        newGeoPoint.setLatitude(searchResult.getSourceLatitude());
        newGeoPoint.setLongitude(searchResult.getSourceLongitude());

        updateLocalUserPreferences(newGeoPoint, searchResult.getLocationText());
        setUpPageData(model, searchResult, this.storefinderBreadcrumbBuilder.getBreadcrumbsForCurrentPositionSearch());
        setUpPosition(model, newGeoPoint);
        setUpNoResultsErrorMessage(model, searchResult);
    }

    protected void setUpPosition(final Model model, final GeoPoint geoPoint) {
        model.addAttribute("geoPoint", geoPoint);
    }

    protected void updateLocalUserPreferences(final GeoPoint geoPoint, final String location) {
        final UserLocationData userLocationData = new UserLocationData();
        userLocationData.setSearchTerm(location);
        userLocationData.setPoint(geoPoint);
        this.customerLocationService.setUserLocation(userLocationData);
    }

    protected void setUpStoreFinderForm(final Model model) {
        final StoreFinderForm storeFinderForm = new StoreFinderForm();
        model.addAttribute("storeFinderForm", storeFinderForm);
    }

    protected void setUpPageTitle(final String searchText, final Model model) {
        storeContentPageTitleInModel(
                model,
                getPageTitleResolver().resolveContentPageTitle(
                        getMessageSource().getMessage("storeFinder.meta.title", null, "storeFinder.meta.title",
                                getI18nService().getCurrentLocale())
                                + " " + searchText));
    }

    protected AbstractPageModel getStoreFinderPage() throws CMSItemNotFoundException {
        return getContentPageForLabelOrId(STORE_FINDER_CMS_PAGE_LABEL);
    }

    /**
     * Get the default search page size.
     *
     * @return the number of results per page, <tt>0</tt> (zero) indicated 'default' size should be used
     */
    protected int getStoreLocatorPageSize() {
        return getSiteConfigService().getInt("storefront.storelocator.pageSize", 0);
    }
}
