<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>

<%--    todo: remove   --%>
<%@ attribute name="stock" required="true" type="de.hybris.platform.commercefacades.product.data.StockData" %>

<%@ attribute name="type" required="true" type="java.lang.String" %>
<%@ attribute name="code" required="true" type="java.lang.String" %>

<c:set value="" var="stockIcon"/>
<c:set value="" var="stockCount"/>
<c:set value="" var="stockText"/>
<c:set value="" var="stockId"/>

<c:choose>
    <%--    EDC stock indication    --%>
    <c:when test="${'warehouse'.equals(type)}">
        <c:set value="vmb_stock_edc_print.png" var="printIcon"/>
        <c:set value="vmb-js-stock-update update-icon-edc" var="stockIcon"/>
        <c:set value="vmb-stock-edc-${code}" var="stockId"/>
    </c:when>
    <%--    TEC stock indication: Show actual stock count    --%>
    <c:when test="${'storeCount'.equals(type)}">
        <c:set value="vmb_stock_tec_print.png" var="printIcon"/>
        <c:set value="vmb-js-stock-update update-count-tec" var="stockIcon"/>
        <c:set value="vmb-stock-tec-${code}" var="stockId"/>
    </c:when>
    <%--    TEC stock indication: Show stock presence    --%>
    <c:when test="${'store'.equals(type)}">
        <c:set value="vmb_stock_tec_print.png" var="printIcon"/>
        <c:set value="vmb-js-stock-update update-icon-tec" var="stockIcon"/>
        <c:set value="vmb-stock-tec-${code}" var="stockId"/>
    </c:when>
</c:choose>

<img src="/_ui/responsive/theme-vanmarckeblue/images/${printIcon}" class="vmb-stock-icon-print">
<div title="${stockText}" id="${stockId}" class="vmb-stock-status ${stockIcon}" data-code="${code}">
    ${stockCount}
    <img src="/_ui/responsive/common/images/3dots.gif" alt="loader" id="${stockId}-spinner">
</div>
<c:if test="${!'storeCount'.equals(type)}"><span class="vmb-stock-text hidden-md hidden-lg">${stockText}</span></c:if>
<c:if test="${!'warehouse'.equals(type)}"><format:nostock cssClass="hidden-md hidden-lg hidden-print"></format:nostock></c:if>
