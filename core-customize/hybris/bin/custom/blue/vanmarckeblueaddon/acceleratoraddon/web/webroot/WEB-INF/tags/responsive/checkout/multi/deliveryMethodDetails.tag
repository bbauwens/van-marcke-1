<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryMethod" required="true" type="de.hybris.platform.commercefacades.order.data.DeliveryModeData" %>
<%@ attribute name="isSelected" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<div class="vmb-delivery-option">
	<form:radiobutton path="deliveryOption" id="${deliveryMethod.code}" label="${fn:escapeXml(deliveryMethod.name)}" value="${deliveryMethod.code}"/>&nbsp;<span class="delivery-option__price">(<format:price priceData="${deliveryMethod.deliveryCost}" displayFreeForZero="TRUE"/>)</span>
</div>