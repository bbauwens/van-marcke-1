<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/common/footer" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="currentDate" class="java.util.Date"/>
<c:set var="inclBtw" value="false"/>

<c:if test="${component.visible}">
    <div class="container-fluid hidden-print">
        <div class="footer__top">
            <div class="row">
                <div class="footer__left col-xs-12 col-sm-12 col-md-9">
                    <div class="row">
                        <c:forEach items="${component.navigationNode.children}" var="childLevel1">
                            <c:forEach items="${childLevel1.children}" step="${component.wrapAfter}" varStatus="i">
                                <div class="footer__nav--container col-xs-12 col-sm-3">
                                    <c:if test="${component.wrapAfter > i.index}">
                                        <div class="title">${fn:escapeXml(childLevel1.title)}</div>
                                    </c:if>
                                    <ul class="footer__nav--links">
                                        <c:forEach items="${childLevel1.children}" var="childLevel2" begin="${i.index}"
                                                   end="${i.index + component.wrapAfter - 1}">
                                            <c:forEach items="${childLevel2.entries}" var="childlink">
                                                <cms:component component="${childlink.item}" evaluateRestriction="true"
                                                               element="li" class="footer__link"/>
                                            </c:forEach>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </c:forEach>
                        </c:forEach>
                    </div>
                </div>
                <div class="footer__right col-xs-12 col-md-3">

                    <cms:pageSlot position="footerparagraph" var="feature" class="row" elememnt="div">
                        <cms:component component="${feature}" element="div" class="footer__nav--container col-xs-12"/>
                    </cms:pageSlot>

                    <c:if test="${showLanguageCurrency}">
                        <div class="row">
                            <div class="col-xs-6 col-md-6 footer__dropdown hidden-lg hidden-md">
                                <footer:languageSelector languages="${languages}" currentLanguage="${currentLanguage}"/>
                            </div>
                            <div class="col-xs-6 col-md-6 footer__dropdown hidden-lg hidden-md">
                                <footer:siteSelector allSites="${allSites}" currentSite="${currentSite}"/>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>

    <div class="footer__bottom">
        <div class="footer__copyright">
            <div class="container">
                <fmt:formatDate value="${currentDate}" pattern="yyyy" var="currentYear"/>
                <spring:message code="footer.copyright" arguments="${currentYear}"/> - <spring:message code="${inclBtw ? 'footer.copyright.btw.incl' : 'footer.copyright.btw.excl'}"/>
            </div>
        </div>
    </div>
</c:if>