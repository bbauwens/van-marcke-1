<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="account-orderdetail account-consignment">
    <ycommerce:testId code="orderDetail_itemList_section">
        <c:choose>
            <c:when test="${orderData.isSplitted}">
                <c:forEach var="ibmOrderData" items="${orderData.ibmSubOrders}">
                    <order:checkoutOrderDetails order="${orderData}" ibmOrder="${ibmOrderData}"/>
                    <c:if test="${not empty orderData.entries}">
                        <order:checkoutOrderEntries order="${orderData}" ibmOrder="${ibmOrderData}"/>
                    </c:if>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <order:checkoutOrderDetails order="${orderData}"/>
                <c:if test="${not empty orderData.entries}">
                    <order:checkoutOrderEntries order="${orderData}"/>
                </c:if>
            </c:otherwise>
        </c:choose>
    </ycommerce:testId>
</div>