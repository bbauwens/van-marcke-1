package com.vanmarcke.blue.addon.controllers.util;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueControllerUtilsTest {

    @Test
    public void testWithExistingMetaTags() {
        Model model = mock(Model.class);

        MetaElementData metaElement = new MetaElementData();
        metaElement.setName("metaKeyForExistingEntry");
        metaElement.setContent("metaValueForExistingEntry");

        List<MetaElementData> metadataList = new LinkedList<>();
        metadataList.add(metaElement);

        String key = "metaKeyForNewEntry";
        String value = "metaValueForNewEntry";

        when(model.getAttribute("metatags")).thenReturn(metadataList);
        BlueControllerUtils.addAttributeToMetaTags(model, key, value);

        List<MetaElementData> result = (List<MetaElementData>)(model.getAttribute("metatags"));

        assertNotNull(result);
        assertTrue(result.contains(metaElement));
        assertEquals(2, result.size());
    }

}
