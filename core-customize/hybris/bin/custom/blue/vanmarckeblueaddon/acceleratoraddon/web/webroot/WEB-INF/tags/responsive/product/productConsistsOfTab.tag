<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="gtm" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/analytics/gtm" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tab-details">
    <table class="table vmb-consists-of-table">
        <thead>
        <tr>
            <th class="sku"><spring:theme code="pdp.varianttable.sku"/></th>
            <th class="name"><spring:theme code="pdp.varianttable.name"/></th>
            <th class="quantity"><spring:theme code="pdp.varianttable.pieces"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${product.consistsOfReferences}" var="consistsof" varStatus="loopIndex" >
                <tr class="vmb-gtm-product-data"
                    data-code="${fn:escapeXml(consistsof.target.code)}"
                    data-name="${fn:escapeXml(consistsof.target.name)}"
                    data-category="${fn:escapeXml(consistsof.target.analyticsData.category)}"
                    data-brand="${fn:escapeXml(consistsof.target.analyticsData.brand)}"
                    data-url="${consistsof.target.url}"
                    data-list="Productreferences ${fn:escapeXml(consistsof.referenceType)}"
                    data-position="${loopIndex.index + 1}"
                >
                    <c:choose>
                        <c:when test="${consistsof.target.purchasable}">
                            <td class="sku" data-label="<spring:theme code="pdp.varianttable.sku" />"><a class="vmb-gtm-product-item" href="<c:url value="${consistsof.target.url}"/>">${consistsof.target.code}</a></td>
                            <td class="name" data-label="<spring:theme code="pdp.varianttable.name" />"><a class="vmb-gtm-product-item" href="<c:url value="${consistsof.target.url}"/>">${consistsof.target.name}</a></td>
                        </c:when>
                        <c:otherwise>
                            <td class="sku" data-label="<spring:theme code="pdp.varianttable.sku" />">${consistsof.target.code}</td>
                            <td class="name" data-label="<spring:theme code="pdp.varianttable.name" />">${consistsof.target.name}</td>
                        </c:otherwise>
                    </c:choose>
                    <td class="quantity" data-label="<spring:theme code="pdp.varianttable.pieces" />">${consistsof.quantity}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

<gtm:productConsistsOf/>