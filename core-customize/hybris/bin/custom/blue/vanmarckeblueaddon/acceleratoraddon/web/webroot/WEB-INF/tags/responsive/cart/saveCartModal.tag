<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="actionUrl" required="true" type="java.lang.String" %>
<%@ attribute name="titleKey" required="true" type="java.lang.String" %>
<%@ attribute name="messageKey" required="false" type="java.lang.String" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:set var="loc_val">
	<spring:message code="basket.save.cart.max.chars"/>
</c:set>

<input type="hidden" id="localized_val" name="localized_val" value="${loc_val}"/>
<div class="vmb-modal fade" id="saveCart" tabindex="-1" role="dialog" aria-labelledby="saveCartLabel">
	<div class="modal-dialog" role="document">
		<form:form action="${actionUrl}" id="saveCartForm" modelAttribute="saveCartForm" autocomplete="off"
				   cssClass="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="saveCartLabel"><spring:theme code="${titleKey}"/></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<c:if test="${not empty messageKey}">
						<div class="legend"><spring:theme code="${messageKey}"/></div>
					</c:if>

					<label class="control-label" for="name">
						<spring:theme code="basket.save.cart.name"/>
					</label>
					<form:input cssClass="form-control" id="saveCartName" path="name" maxlength="255"/>
					<div class="help-block right-cartName" id="remain">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="description">
						<spring:theme code="basket.save.cart.description"/>
					</label>
					<form:textarea cssClass="form-control" id="saveCartDescription" path="description" maxlength="255"/>
					<div class="help-block" id="remainTextArea">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-link" id="cancelSaveCartButton" data-dismiss="modal"><spring:theme
						code="basket.save.cart.action.cancel"/></button>
				<button type="submit" class="btn btn-primary" id="saveCartButton"><spring:theme
						code="basket.save.cart.action.save"/></button>
			</div>
		</form:form>
	</div>
</div>

