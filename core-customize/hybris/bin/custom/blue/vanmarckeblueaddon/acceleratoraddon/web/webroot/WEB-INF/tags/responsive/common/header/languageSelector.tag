<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="languages" required="true" type="java.util.Collection" %>
<%@ attribute name="currentLanguage" required="true" type="de.hybris.platform.commercefacades.storesession.data.LanguageData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:if test="${fn:length(languages) > 1}">
	<spring:url value="/_s/language" var="setLanguageActionUrl"/>
	<form:form action="${setLanguageActionUrl}" method="post" cssClass="vmb-main-header__langSelector">
		<ul>
			<c:forEach items="${languages}" var="lang">
				<li>
					<c:choose>
						<c:when test="${lang.isocode == currentLanguage.isocode}">
							<button type="submit" name="code" value="${fn:escapeXml(lang.isocode)}" lang="${fn:escapeXml(fn:substring(lang.isocode,0, 2))}" disabled>${fn:escapeXml(lang.nativeName)}</button>
						</c:when>
						<c:otherwise>
							<button type="submit" name="code" value="${fn:escapeXml(lang.isocode)}" lang="${fn:escapeXml(fn:substring(lang.isocode,0, 2))}">${fn:escapeXml(lang.nativeName)}</button>
						</c:otherwise>
					</c:choose>
				</li>
			</c:forEach>
		</ul>
	</form:form>
</c:if>