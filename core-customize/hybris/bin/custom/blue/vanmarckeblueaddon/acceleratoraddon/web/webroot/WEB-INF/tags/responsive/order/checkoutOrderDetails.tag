<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ attribute name="ibmOrder" required="false" type="de.hybris.platform.commercefacades.order.data.IBMOrderData" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="hasPickupItems" value="${order.pickupItemsQuantity > 0}"/>
<c:set var="hasShippedItems" value="${order.deliveryItemsQuantity > 0}"/>

<c:set var="address" value="${order.deliveryAddress}"/>
<c:set var="name" value="${order.b2bCustomerData.name}"/>
<c:set var="tecAddress" value="${order.deliveryPointOfService.address}"/>
<c:set var="tecName" value="TEC ${order.deliveryPointOfService.displayName}"/>

<h2 class="vmb-checkout-confirmation__subtitle"><spring:theme code="text.account.order.info"/></h2>

<div class="vmb-checkout-confirmation__details">
    <c:if test="${order.isSplitted}">
        <div class="vmb-checkout-confirmation__details--ordersplit">
            <div class="order-number">
                <div class="label-order"><spring:theme code="text.account.orderHistory.orderNumber"/></div>
                <div class="value-order">${fn:escapeXml(ibmOrder.code)}</div>
            </div>
            <div class="order-delivery-date">
                <div class="label-order">
                    <c:if test="${fn:endsWith(order.deliveryMode.code, 'standard')}">
                        <spring:theme code="order.delivery.date"/>
                    </c:if>
                    <c:if test="${fn:endsWith(order.deliveryMode.code, 'tec')}">
                        <spring:theme code="order.pickup.date"/>
                    </c:if>
                </div>
                <div class="value-order">
                    <fmt:formatDate value="${ibmOrder.orderEntries.get(0).deliveryPickupDate}" pattern="dd-MM-yyyy"/>
                </div>
            </div>
        </div>
    </c:if>

    <div class="vmb-checkout-confirmation__details--shipping">
        <div class="order-shipping-method">
            <div class="label-order"><spring:theme code="text.shippingMethod"/></div>
            <div class="value-order">${fn:escapeXml(order.deliveryMode.name)}<br>${fn:escapeXml(order.deliveryMode.description)}
            </div>
        </div>
        <c:choose>
            <c:when test="${hasShippedItems}">
                <div class="order-ship-to">
                    <div class="label-order"><spring:theme code="text.account.order.shipto"/></div>
                    <div class="value-order"><address:formattedAddress address="${order.deliveryAddress}"
                                                                       name="${name}"/></div>
                </div>
                <c:if test="${not empty order.deliveryComment}">
                    <div class="order-note">
                        <div class="label-order"><spring:theme code="checkout.summary.delivery.note"/></div>
                        <div class="value-order">${order.deliveryComment}</div>
                    </div>
                </c:if>
            </c:when>
            <c:when test="${hasPickupItems}">
                <div class="order-ship-to">
                    <div class="label-order"><spring:theme code="basket.page.title.pickupFrom"/></div>
                    <div class="value-order"><address:formattedAddress address="${tecAddress}" name="${tecName}"/></div>
                </div>
            </c:when>
        </c:choose>
    </div>
    <c:if test="${not empty order.paymentInfo}">
        <div class="vmb-checkout-confirmation__details--payment">
            <div class="order-billing-address">
                <order:billingAddressItem order="${order}"/>
            </div>
            <div class="order-payment-data">
                <order:paymentDetailsItem order="${order}"/>
            </div>
        </div>
    </c:if>
</div>
