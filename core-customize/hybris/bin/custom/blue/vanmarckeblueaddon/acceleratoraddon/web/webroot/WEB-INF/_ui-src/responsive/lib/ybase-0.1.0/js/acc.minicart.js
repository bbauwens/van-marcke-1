ACC.minicart = {

	_autoload: [
		"bindMiniCart"
	],

	bindMiniCart: function(){

        $(document).on("click",".js-mini-cart-link", function(e){
            e.preventDefault();
            var url = $(this).data("miniCartUrl");

            if(url){

                $('body').append("<div class='vmb-mini-cart-wrapper'><div class='vmb-mini-cart-bg'></div><div class='vmb-mini-cart-container'></div></div>");

                $.get(url, function(data) {

                    $(".vmb-mini-cart-container").html(data);
                    $(".vmb-mini-cart-container").parent().addClass('show');

                    // prevent body from scroll when mini cart is open
                    $("body").css("overflow", "hidden");

                    if($("#_asm").length > 0){
                        $(".vmb-mini-cart-container").css("top", $("#_asm").height());
                    }

                    // Close function
                    $(".js-vmb-mini-cart-close, .vmb-mini-cart-bg").on("click", function(e) {
                        e.preventDefault();
                        $(".vmb-mini-cart-wrapper").removeClass('show');
                        $("body").css("overflow", "");
                        setTimeout(function() {
                            $(".vmb-mini-cart-wrapper").remove();
                        }, 800);
                    });
                }).fail(function() {
                    window.location.href = directCartUrl;
                });

            }

        });

        $(".js-ASM-collapseBtn").click(function () {
            $(".vmb-mini-cart-container").css("top", $("#_asm").height());
        });
    },

    updateMiniCartDisplay: function(){
        var cartItems = $(".js-mini-cart-link").data("miniCartItemsText");
        var miniCartRefreshUrl = $(".js-mini-cart-link").data("miniCartRefreshUrl");
        $.ajax({
            url: miniCartRefreshUrl,
            cache: false,
            type: 'GET',
            success: function(jsonData){
                $(".js-mini-cart-link .js-mini-cart-count").html(jsonData.miniCartCount);
                $(".js-mini-cart-link .js-mini-cart-price").html(jsonData.miniCartPrice);
            }
        });
    }

};