package com.vanmarcke.blue.addon.validation;

import com.vanmarcke.blue.addon.form.EditWishListForm;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAddToWishListFormValidatorTest {

    @InjectMocks
    private VMKAddToWishListFormValidator vmkAddToWishListFormValidator;

    @Test
    public void testValidate() {
        String name = "This is a test with-all_correcté 01234";
        String description = "This, is a correct-tèst_!? \n dsfd....t estgmé@ weiß  :;()*%€/ \\ <>,.?”'!@&-_+=";
        EditWishListForm form = mock(EditWishListForm.class);
        Errors errors = mock(Errors.class);

        when(form.getName()).thenReturn(name);
        when(form.getDescription()).thenReturn(description);
        vmkAddToWishListFormValidator.validate(form, errors);

        verify(errors, never()).rejectValue(anyString(), anyString());
    }

    @Test
    public void testValidate_notValid() {
        String name = "This, is not a correct-tèst_!? \n dsfd....t estgmé@";
        String description = "#$ testing with wrong description";
        EditWishListForm form = mock(EditWishListForm.class);
        Errors errors = mock(Errors.class);

        when(form.getName()).thenReturn(name);
        when(form.getDescription()).thenReturn(description);
        vmkAddToWishListFormValidator.validate(form, errors);

        verify(errors).rejectValue("newWishlistName", "wishlist.validation-error.pattern-name");
        verify(errors).rejectValue("newWishlistDescription", "wishlist.validation-error.pattern-description");
    }
}
