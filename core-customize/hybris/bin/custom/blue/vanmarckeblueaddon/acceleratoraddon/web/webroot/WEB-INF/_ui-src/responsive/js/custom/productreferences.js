(function ($) {
    'use strict';

    if ($(".vmb-reference-component").length > 0) {

        $('.vmb-reference-component .swiper-container').each(function (index) {

            var swipeClass = "s" + index;
            $(this).addClass(swipeClass);

            var mySwiper = new Swiper("." + swipeClass, {

                watchOverflow: true,
                spaceBetween: 15,

                // If we need pagination
                pagination: {
                    el: "." + swipeClass + ' .swiper-pagination',
                    clickable: true
                },

                // Navigation arrows
                navigation: {
                    nextEl: $(this).closest('.vmb-reference-component').find('.swiper-button-next')[0],
                    prevEl: $(this).closest('.vmb-reference-component').find('.swiper-button-prev')[0]
                },
                'on': {
                    'init': function () {
                        $(this.el).closest('.vmb-reference-component').find('.swiper-button-next').removeClass('hidden');
                        $(this.el).closest('.vmb-reference-component').find('.swiper-button-prev').removeClass('hidden');
                    }
                }

            });
            $(window).resize(function () {
                var width = $(".swiper-container").width();
                mySwiper.params.slidesPerView = calculateSlidesPerView(width);
                mySwiper.update();
            });
            $(window).trigger('resize')

           function calculateSlidesPerView(width) {
                if (width > 800) {
                    return 4;
                } else if (width > 590) {
                    return 3;
                } else if (width > 320) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });

    }
})(jQuery);