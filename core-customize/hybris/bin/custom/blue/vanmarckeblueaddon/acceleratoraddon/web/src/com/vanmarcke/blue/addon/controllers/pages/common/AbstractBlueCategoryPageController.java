package com.vanmarcke.blue.addon.controllers.pages.common;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.blue.addon.constants.VanmarckeblueaddonWebConstants.ProductListViewType;
import com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants;
import com.vanmarcke.blue.addon.controllers.pages.AbstractBlueSearchPageController;
import com.vanmarcke.blue.addon.controllers.util.BlueControllerUtils;
import com.vanmarcke.services.helper.HreflangHelper;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.Config;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.vanmarcke.blue.addon.constants.VanmarckeblueaddonWebConstants.META_TAG_PAGE_DESCRIPTION;
import static com.vanmarcke.blue.addon.constants.VanmarckeblueaddonWebConstants.META_TAG_PAGE_TITLE;

public class AbstractBlueCategoryPageController extends AbstractBlueSearchPageController {

    private static final Logger LOG = Logger.getLogger(AbstractBlueCategoryPageController.class);

    /**
     * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it contains on or more '.' characters. Please see
     * https://jira.springsource.org/browse/SPR-6164 for a discussion on the issue and future resolution.
     */
    protected static final String CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/{categoryCode:.*}";
    protected static final String PRODUCT_GRID_PAGE = "category/productGridPage";

    private static final String MAX_CATEGORY_PAGE_LEVEL = "categorypage.levels.max";

    @Resource(name = "searchBreadcrumbBuilder")
    private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

    @Resource(name = "categoryModelUrlResolver")
    private UrlResolver<CategoryModel> categoryModelUrlResolver;

    @Resource(name = "customerLocationService")
    private CustomerLocationService customerLocationService;

    @Resource(name = "cmsPreviewService")
    private CMSPreviewService cmsPreviewService;

    @Resource(name = "hreflangHelper")
    private HreflangHelper hreflangHelper;

    @Resource(name = "localizedMessageFacade")
    private LocalizedMessageFacade localizedMessageFacade;

    @Resource(name = "vmkPageTitleResolver")
    private PageTitleResolver pageTitleResolver;

    @Resource(name = "siteBaseUrlResolutionService")
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @ExceptionHandler(UnknownIdentifierException.class)
    public String handleUnknownIdentifierException(UnknownIdentifierException exception, HttpServletRequest request) {
        request.setAttribute("message", exception.getMessage());
        return FORWARD_PREFIX + "/404";
    }

    protected String performSearchAndGetAjaxResults(String categoryCode,
                                                    String searchQuery,
                                                    int page,
                                                    ShowMode showMode,
                                                    String sortCode,
                                                    final Model model) {
        CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);

        CategoryPageModel categoryPage = getCategoryPage(category);

        CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, searchQuery, page, showMode,
                sortCode, categoryPage);

        ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData;
        try {
            categorySearch.doSearch();
            searchPageData = categorySearch.getSearchPageData();
            populateProductVariants(searchPageData, searchQuery, categoryCode);
        } catch (ConversionException e) {
            searchPageData = createEmptySearchResult(categoryCode);
        }

        model.addAttribute("searchPageData", searchPageData);
        model.addAttribute("categoryName", category.getName());
        model.addAttribute("categoryCode", category.getCode());
        return VanmarckeblueaddonControllerConstants.Views.Fragments.Product.PRODUCT_LIST;
    }


    /**
     * Performs the search in Solr and returns the result page.
     *
     * @param categoryCode the category code
     * @param searchQuery  the search query
     * @param page         the page
     * @param showMode     the show mode
     * @param sortCode     the sort code
     * @param model        the model
     * @param request      the request
     * @param response     the response
     * @return the result page
     * @throws UnsupportedEncodingException in case the request URL cannot be decoded
     */
    protected String performSearchAndGetResultsPage(String categoryCode,
                                                    String searchQuery,
                                                    int page,
                                                    ShowMode showMode,
                                                    String sortCode,
                                                    ProductListViewType viewType,
                                                    Model model,
                                                    HttpServletRequest request,
                                                    HttpServletResponse response) throws UnsupportedEncodingException {
        CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);

        String redirection = checkRequestUrl(request, response, getCategoryModelUrlResolver().resolve(category));
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        CategoryPageModel categoryPage = getCategoryPage(category);

        CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, searchQuery, page, showMode,
                sortCode, categoryPage);

        ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData;

        // determine correct list view depending on the cookie stored value
        viewType = getAndStorePLPViewType(viewType, request, response);

        try {
            categorySearch.doSearch();
            searchPageData = categorySearch.getSearchPageData();
            if (ProductListViewType.List == viewType) {
                populateProductVariants(searchPageData, searchQuery, categoryCode);
            }
        } catch (ConversionException e) {
            searchPageData = createEmptySearchResult(categoryCode);
        }

        if (!isValidLevel(categoryCode) || CollectionUtils.isEmpty(searchPageData.getSubCategories())) {
            try {
                // Overwrite to product grid page.
                categoryPage = (CategoryPageModel) getCmsPageService().getPageForId("productGrid");
            } catch (CMSItemNotFoundException e) {
                LOG.error(e);
            }
        }

        storeCmsPageInModel(model, categoryPage);
        storeContinueUrl(request);

        populateModel(model, searchPageData, showMode);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getSearchBreadcrumbBuilder().getBreadcrumbs(categoryCode, searchPageData));
        model.addAttribute("showCategoriesOnly", categorySearch.isShowCategoriesOnly());
        model.addAttribute("categoryName", category.getName());
        model.addAttribute("categoryCode", category.getCode());
        model.addAttribute("categoryDescription", category.getDescription());
        model.addAttribute("pageType", PageType.CATEGORY.name());
        model.addAttribute("userLocation", getCustomerLocationService().getUserLocation());
        model.addAttribute("viewType", viewType);

        updatePageTitle(category, model);

        RequestContextData requestContextData = getRequestContextData(request);
        requestContextData.setCategory(category);
        requestContextData.setSearch(searchPageData);

        if (searchQuery != null) {
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
            model.addAttribute("searchQuery", searchQuery);
        }

        String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(
                category.getKeywords().stream().map(KeywordModel::getKeyword).collect(Collectors.toSet()));
        String metaDescription = MetaSanitizerUtil.sanitizeDescription(
                MessageFormat.format(localizedMessageFacade.getMessageForCodeAndCurrentLocale(META_TAG_PAGE_DESCRIPTION), category.getName()));
        String metaTile = MetaSanitizerUtil.sanitizeDescription(
                MessageFormat.format(localizedMessageFacade.getMessageForCodeAndCurrentLocale(META_TAG_PAGE_TITLE), category.getName()));
        setUpMetaData(model, metaKeywords, metaDescription);
        BlueControllerUtils.addAttributeToMetaTags(model, "title", metaTile);

        String canonicalCategory = getSiteBaseUrlResolutionService()
                .getWebsiteUrlForSite(getBaseSiteService().getCurrentBaseSite(), true, getCategoryModelUrlResolver().resolve(category));
        model.addAttribute("canonicalLink", canonicalCategory);
        return getViewPage(categoryPage);
    }

    /**
     * Creates empty search results in case {@code doSearch} throws an exception in order to avoid stacktrace on storefront.
     *
     * @param categoryCode category code
     * @return created {@link ProductCategorySearchPageData}
     */
    protected ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> createEmptySearchResult(
            String categoryCode) {
        ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = new ProductCategorySearchPageData<>();
        searchPageData.setResults(Collections.emptyList());
        searchPageData.setPagination(createEmptyPagination());
        searchPageData.setCategoryCode(categoryCode);
        return searchPageData;
    }

    protected FacetRefinement<SearchStateData> performSearchAndGetFacets(String categoryCode, String searchQuery,
                                                                         int page, ShowMode showMode, String sortCode) {
        ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = populateSearchPageData(
                categoryCode, searchQuery, page, showMode, sortCode);
        List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(),
                convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs()));
        FacetRefinement<SearchStateData> refinement = new FacetRefinement<>();
        refinement.setFacets(facets);
        refinement.setCount(searchPageData.getPagination().getTotalNumberOfResults());
        refinement.setBreadcrumbs(searchPageData.getBreadcrumbs());

        return refinement;
    }

    protected SearchResultsData<ProductData> performSearchAndGetResultsData(String categoryCode, String searchQuery,
                                                                            int page, ShowMode showMode, String sortCode) {
        ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = populateSearchPageData(
                categoryCode, searchQuery, page, showMode, sortCode);
        SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
        searchResultsData.setResults(searchPageData.getResults());
        searchResultsData.setPagination(searchPageData.getPagination());

        return searchResultsData;
    }


    protected ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> populateSearchPageData(
            String categoryCode, String searchQuery, int page, ShowMode showMode, String sortCode) {
        CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);
        CategoryPageModel categoryPage = getCategoryPage(category);
        CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, searchQuery, page, showMode,
                sortCode, categoryPage);
        categorySearch.doSearch();

        return categorySearch.getSearchPageData();
    }


    protected CategoryPageModel getDefaultCategoryPage() {
        try {
            return this.getCmsPageService().getPageForCategory(null);
        } catch (CMSItemNotFoundException ignore) // NOSONAR
        {
            // Ignore
        }
        return null;
    }

    protected boolean categoryHasDefaultPage(CategoryPageModel categoryPage) {
        return Boolean.TRUE.equals(categoryPage.getDefaultPage());
    }

    protected CategoryPageModel getCategoryPage(CategoryModel category) {
        try {
            return this.getCmsPageService().getPageForCategory(category, getCMSPreviewService().getPagePreviewCriteria());
        } catch (CMSItemNotFoundException ignore) // NOSONAR
        {
            // Ignore
        }
        return null;
    }

    protected void updatePageTitle(CategoryModel category, Model model) {
        storeContentPageTitleInModel(model, getPageTitleResolver().resolveCategoryPageTitle(category));
    }

    protected String getViewPage(CategoryPageModel categoryPage) {
        if (categoryPage != null) {
            String targetPage = getViewForPage(categoryPage);
            if (targetPage != null && !targetPage.isEmpty()) {
                return targetPage;
            }
        }
        return PAGE_ROOT + PRODUCT_GRID_PAGE;
    }

     /**
     *
     * @param categoryCode category code
     * @return map with the hreflang links
     */
    protected Map<String, String> getCategoryHreflangLinks(String categoryCode) {
        CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);
        return getHreflangHelper().generateHreflangMap(category);
    }


    protected class CategorySearchEvaluator {

        private String categoryCode;
        private SearchQueryData searchQueryData = new SearchQueryData();
        private int page;
        private ShowMode showMode;
        private String sortCode;
        private CategoryPageModel categoryPage;
        private boolean showCategoriesOnly;
        private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData;

        public CategorySearchEvaluator(String categoryCode, String searchQuery, int page, ShowMode showMode,
                                       String sortCode, CategoryPageModel categoryPage) {
            this.categoryCode = categoryCode;
            this.searchQueryData.setValue(searchQuery);
            this.page = page;
            this.showMode = showMode;
            this.sortCode = sortCode;
            this.categoryPage = categoryPage;
        }

        public void doSearch() {
            this.showCategoriesOnly = false;
            if (this.searchQueryData.getValue() == null) {
                // Direct category link without filtering
                this.searchPageData = getProductSearchFacade().categorySearch(this.categoryCode);
                if (this.categoryPage != null) {
                    this.showCategoriesOnly = !categoryHasDefaultPage(this.categoryPage)
                            && CollectionUtils.isNotEmpty(this.searchPageData.getSubCategories());
                }
            } else {
                // We have some search filtering
                if (this.categoryPage == null) {
                    // Load the default category page
                    this.categoryPage = getDefaultCategoryPage();
                }

                SearchStateData searchState = new SearchStateData();
                searchState.setQuery(this.searchQueryData);

                PageableData pageableData = createPageableData(this.page, getSearchPageSize(), this.sortCode, this.showMode);
                this.searchPageData = getProductSearchFacade().categorySearch(this.categoryCode, searchState, pageableData);
            }
            //Encode SearchPageData
            this.searchPageData = (ProductCategorySearchPageData) encodeSearchPageData(this.searchPageData);
        }

        public int getPage() {
            return this.page;
        }

        public CategoryPageModel getCategoryPage() {
            return this.categoryPage;
        }

        public boolean isShowCategoriesOnly() {
            return this.showCategoriesOnly;
        }

        public ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> getSearchPageData() {
            return this.searchPageData;
        }
    }

    /**
     * Checks whether the level of the given {@code categoryCode} is valid.
     *
     * @param categoryCode the category's unique identifier
     * @return {@code true} in case the level of the given {@code categoryCode} is valid, {@code false} otherwise
     */
    private boolean isValidLevel(String categoryCode) {
        int currentLevel = searchBreadcrumbBuilder.getBreadcrumbs(categoryCode, null, false).size();
        return Config.getInt(MAX_CATEGORY_PAGE_LEVEL, 3) > currentLevel;
    }


    /**
     * @return the searchBreadcrumbBuilder
     */
    public SearchBreadcrumbBuilder getSearchBreadcrumbBuilder() {
        return this.searchBreadcrumbBuilder;
    }

    /**
     * @return the categoryModelUrlResolver
     */
    public UrlResolver<CategoryModel> getCategoryModelUrlResolver() {
        return this.categoryModelUrlResolver;
    }

    /**
     * @return the customerLocationService
     */
    public CustomerLocationService getCustomerLocationService() {
        return this.customerLocationService;
    }

    /**
     * @return the cmsPreviewService
     */
    public CMSPreviewService getCMSPreviewService() {
        return this.cmsPreviewService;
    }

    /**
     * @return the hreflangHelper
     */
    public HreflangHelper getHreflangHelper() {
        return hreflangHelper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PageTitleResolver getPageTitleResolver() {
        return pageTitleResolver;
    }

    /**
     * @return the siteBaseUrlResolutionService
     */
    public SiteBaseUrlResolutionService getSiteBaseUrlResolutionService() {
        return siteBaseUrlResolutionService;
    }
}
