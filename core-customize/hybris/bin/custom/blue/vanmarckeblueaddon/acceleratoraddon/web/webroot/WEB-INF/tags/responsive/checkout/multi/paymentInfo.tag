<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="paymentInfo" required="true" type="de.hybris.platform.commercefacades.order.data.CCPaymentInfoData" %>
<%@ attribute name="showPaymentInfo" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/checkout/multi" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:if test="${not empty cartData.paymentType and (empty showPaymentInfo or showPaymentInfo)}">
    <div class="checkout-order-summary-list checkout-order-summary-list--payment">
        <div class="checkout-order-summary-list-heading">
            <h4><spring:theme code="checkout.multi.payment" text="Payment:"/></h4>
            <div class="data">
                <div>
                    <h5><spring:theme code="checkout.multi.paymentType"/></h5>
                    <div>
                            ${cartData.paymentType.displayName}
                        <c:if test="${not empty cartData.purchaseOrderNumber}"> <br/><span><spring:theme code="checkout.multi.paymentMethod.purchase.order.number"/>:</span> ${fn:escapeXml(cartData.purchaseOrderNumber)}</c:if>
                    </div>
                </div>
                <c:if test="${not empty paymentInfo.billingAddress}">
                    <div>
                        <h5><spring:theme code="checkout.checkout.multi.paymentAddress"/></h5>
                        <multi-checkout:paymentAddress cartData="${cartData}"/>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</c:if>