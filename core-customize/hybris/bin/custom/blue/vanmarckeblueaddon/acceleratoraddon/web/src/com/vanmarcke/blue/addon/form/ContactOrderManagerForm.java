package com.vanmarcke.blue.addon.form;

/**
 * Form object for registration
 */
public class ContactOrderManagerForm {

    private String type;
    private String orderNumber;
    private String email;
    private String contactComment;

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getOrderNumber() {
        return this.orderNumber;
    }

    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getContactComment() {
        return this.contactComment;
    }

    public void setContactComment(final String contactComment) {
        this.contactComment = contactComment;
    }
}
