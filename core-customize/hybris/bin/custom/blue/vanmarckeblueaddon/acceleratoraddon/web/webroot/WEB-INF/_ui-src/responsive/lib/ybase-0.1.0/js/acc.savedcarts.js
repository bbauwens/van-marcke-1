ACC.savedcarts = {

    _autoload: [
        ["bindRestoreSavedCartClick", $(".js-restore-saved-cart").length != 0],
        ["bindDeleteSavedCartLink", $('.js-delete-saved-cart').length != 0],
        ["bindDeleteConfirmLink", $('.js-savedcart_delete_confirm').length != 0],
        ["bindSaveCartForm", $(".js-save-cart-link").length != 0 || $(".js-update-saved-cart").length != 0],
        ["bindUpdateUploadingSavedCarts", $(".js-uploading-saved-carts-update").length != 0]
    ],

    $savedCartRestoreBtn: {},
    $currentCartName: {},

    bindRestoreSavedCartClick: function () {
        $(".js-restore-saved-cart").click(function (event) {

            event.preventDefault();
            var cartId = $(this).data('savedcart-id');
            var url = ACC.config.encodedContextPath + '/my-account/saved-carts/' + cartId + '/restore';

            $.get(url).done(function (data) {
                $("body").append(data);

                $("#vmbRestoreSavedCartModal").modal()
                    .on('hidden.bs.modal', function (e) {
                        $("#vmbRestoreSavedCartModal").remove();
                    });

                ACC.savedcarts.bindRestoreModalHandlers();
                ACC.savedcarts.bindPostRestoreSavedCartLink();
            });
        });
    },

    bindRestoreModalHandlers: function () {

        ACC.savedcarts.$savedCartRestoreBtn = $('.js-save-cart-restore-btn');
        ACC.savedcarts.$currentCartName = $('.js-current-cart-name');

        $(document).on("click", "#keepRestoredCart", function () {
            $("#activeCartName").prop("disabled", false);
            $("#activeCartName").parent().slideDown();
        });

        $(document).on("click", "#preventSaveActiveCart", function () {
            $("#activeCartName").parent().slideUp();
            $("#activeCartName").prop("disabled", true);
        });

        ACC.savedcarts.$currentCartName.on('focus', function (event) {
            $('.js-restore-current-cart-form').removeClass('has-error');
            $('.js-restore-error-container').html('');
        });

        ACC.savedcarts.$currentCartName.on('blur', function (event) {
            if (this.value == "" && this.value.length === 0) {
                ACC.savedcarts.$savedCartRestoreBtn.attr('disabled', 'disabled');
            } else {
                ACC.savedcarts.$savedCartRestoreBtn.removeAttr('disabled');
            }
        });
    },

    bindPostRestoreSavedCartLink: function () {

        $(document).on("click", '.js-save-cart-restore-btn', function (event) {
            event.preventDefault();

            var cartName = $('#activeCartName').val();
            var url = $(this).data('restore-url');
            var preventSaveCartChecked = $('.js-prevent-save-active-cart').prop('checked');
            var postData = {
                preventSaveActiveCart: preventSaveCartChecked !== undefined ? preventSaveCartChecked : false,
                keepRestoredCart: false,
                cartName: cartName
            };

            $.post({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: postData,
                beforeSend: function () {
                    $('#vmbRestoreSavedCartModal').modal('hide');
                    showLoader();
                },
                success: function (response) {
                    var url = ACC.config.encodedContextPath + "/cart";
                    window.location.replace(url);
                },
                error: function (xht, textStatus, ex) {
                    $('.js-restore-current-cart-form').addClass('has-error');
                    $('.js-restore-error-container').html(ex);
                },
                complete: function () {
                    hideLoader();
                }
            });
        });
    },

    bindDeleteSavedCartLink: function () {
        $(document).on("click", '.js-delete-saved-cart', function (event) {
            event.preventDefault();
            var cartId = $(this).data('savedcart-id');
            var cartName = $(this).data('savedcart-name');

            $("#vmbConfirmSavedCartDeleteCode").val(cartId);
            $("#vmbConfirmSavedCartDeleteName").html(cartName);

            $("#vmbConfirmSavedCartDeleteModal").modal();
        });
    },

    bindDeleteConfirmLink: function () {
        $(document).on("click", '.js-savedcart_delete_confirm', function (event) {
            event.preventDefault();
            $(this).attr("disabled", true);
            var cartId = $("#vmbConfirmSavedCartDeleteCode").val();
            var url = ACC.config.encodedContextPath + '/my-account/saved-carts/' + cartId + '/delete';
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function (response) {
                    var url = ACC.config.encodedContextPath + "/my-account/saved-carts";
                    window.location.replace(url);
                }
            });
        });
    },

    bindSaveCartForm: function () {
        ACC.savedcarts.charactersLeftInit();
        var showSaveCartFormCallback = function () {

            $("#saveCart").modal()
                .on('hidden.bs.modal', function (e) {
                    document.getElementById("saveCartForm").reset();
                    $('#saveCartName').parent().removeClass("has-error");
                    ACC.savedcarts.charactersLeftInit();
                });
        };

        $(document).on("click", ".js-save-cart-link, .js-update-saved-cart", function (e) {
            e.preventDefault();
            ACC.common.checkAuthenticationStatusBeforeAction(showSaveCartFormCallback);
        });


        $('#saveCartName').keyup(function () {
            // limit the text length
            var maxchars = 255;
            var value = $('#localized_val').attr('value');
            var tlength = $(this).val().length;

            if (tlength > 0) {
                $(this).parent().removeClass("has-error");
            }

            remain = maxchars - parseInt(tlength);
            $('#remain').text(value + ' : ' + remain);
        });

        $('#saveCartDescription').keyup(function () {
            var maxchars = 255;
            var value = $('#localized_val').attr('value');
            var tlength = $(this).val().length;
            remain = maxchars - parseInt(tlength);
            $('#remainTextArea').text(value + ' : ' + remain);
        });

        $(document).on("click", '#saveCart #saveCartButton', function (e) {
            e.preventDefault();

            if (!_.isEmpty($('#saveCartName').val())) {
                $('#saveCartForm').submit();
                $('#saveCart').modal('hide');
            } else {
                $('#saveCartName').parent().addClass("has-error");
            }

        });
    },

    charactersLeftInit: function () {

        $('#remain').text($('#localized_val').attr('value') + ' : ' + ( 255 - $('#saveCartName').val().length));
        $('#remainTextArea').text($('#localized_val').attr('value') + ' : ' + (255 - $('#saveCartDescription').val().length));
    },


    bindUpdateUploadingSavedCarts: function () {
        var cartIdRowMapping = $(".js-uploading-saved-carts-update").data("idRowMapping");
        var refresh = $(".js-uploading-saved-carts-update").data("refreshCart");
        if (cartIdRowMapping && refresh) {
            var interval = $(".js-uploading-saved-carts-update").data("refreshInterval");
            var arrCartIdAndRow = cartIdRowMapping.split(',');
            var mapCartRow = new Object();
            var cartCodes = [];
            for (i = 0; i < arrCartIdAndRow.length; i++) {
                var arrValue = arrCartIdAndRow[i].split(":");
                if (arrValue != "") {
                    mapCartRow[arrValue[0]] = arrValue[1];
                    cartCodes.push(arrValue[0]);
                }
            }

            if (cartCodes.length > 0) {
                setTimeout(function () {
                    ACC.savedcarts.refreshWorker(cartCodes, mapCartRow, interval)
                }, interval);
            }
        }
    },


    refreshWorker: function (cartCodes, mapCartRow, interval) {
        $.ajax({
            dataType: "json",
            url: ACC.config.encodedContextPath + '/my-account/saved-carts/uploadingCarts',
            data: {
                cartCodes: cartCodes
            },
            type: "GET",
            traditional: true,
            success: function (data) {
                if (data != undefined) {
                    var hidden = "hidden";
                    var rowId = "#row-";
                    for (i = 0; i < data.length; i++) {
                        var cart = data[i];

                        var index = $.inArray(cart.code, cartCodes);
                        if (index > -1) {
                            cartCodes.splice(index, 1)
                        }
                        var rowIdIndex = mapCartRow[cart.code];
                        if (rowIdIndex != undefined) {
                            var rowSelector = rowId + rowIdIndex;
                            $(rowSelector + " .js-saved-cart-name").removeClass("not-active");
                            $(rowSelector + " .js-saved-cart-date").removeClass(hidden);
                            $(rowSelector + " .js-file-importing").remove();
                            $(rowSelector + " .js-saved-cart-description").text(cart.description);
                            var numberOfItems = cart.entries.length;
                            $(rowSelector + " .js-saved-cart-number-of-items").text(numberOfItems);
                            $(rowSelector + " .js-saved-cart-total").text(cart.totalPrice.formattedValue);
                            if (numberOfItems > 0) {
                                $(rowSelector + " .js-restore-saved-cart").removeClass(hidden);
                                $(rowSelector + " .js-export-saved-cart").removeClass(hidden);
                            }
                            $(rowSelector + " .js-delete-saved-cart").removeClass(hidden);
                        }
                    }
                };

                if (cartCodes.length > 0) {
                    setTimeout(function () {
                        ACC.savedcarts.refreshWorker(cartCodes, mapCartRow, interval)
                    }, interval);
                }
            }
        })
    }
};