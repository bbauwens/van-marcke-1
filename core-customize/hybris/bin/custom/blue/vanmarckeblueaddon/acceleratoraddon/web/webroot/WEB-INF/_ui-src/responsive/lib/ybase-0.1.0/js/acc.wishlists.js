ACC.wishlists = {

    _autoload: [
        ["bindRestoreWishListClick", $(".js-restore-wish-list").length != 0],
        ["bindExportBasketClick", $(".js-export-basket").length != 0],
        ["bindDeleteWishListLink", $('.js-delete-wish-list').length != 0],
        ["bindDeleteConfirmLink", $('.js-wishlist_delete_confirm').length != 0],
        ["bindWishListForm", $(".js-add-products-to-wish-list").length != 0],
        ["bindEditWishListForm", $(".js-update-wish-list").length != 0],
        ["bindUpdateUploadingWishLists", $(".js-uploading-wish-lists-update").length != 0],
        ["bindAddToWishListFromPDPClick", $(".js-add-to-wishlist").length != 0],
        ["bindAddToWishListFromCartClick", $(".js-add-to-wishlist-from-cart").length != 0],
        ["showRemoveUnavailableProductsModal", $('.js-delete-wish-list').length != 0 || $(".js-update-wish-list").length != 0 || $(".js-restore-wish-list").length != 0]
    ],

    $wishListRestoreBtn: {},
    $currentCartName: {},

    addProductsToWishlist: function (orderProducts) {
        if (orderProducts.length <= 0) {
            ACC.wishlists.displayGlobalAlert({type: 'warning', messageId: 'wishlist-no-products-selected-message'});
            return;
        }

        var url = ACC.config.encodedContextPath + '/my-account/saved-baskets/add';

        // we have multiple products selected
        $.get(url).done(function (data) {
            $("body").append(data);

            var modalBody = $.templates("#wishListAddToWishlistSelectionTemplate").render({
                products: orderProducts
            });

            $("#vmbAddToWishlistWishListModal .js-add-to-wishlist-product-selection").html(modalBody);

            $("#vmbAddToWishlistWishListModal").modal()
                .on('hidden.bs.modal', function (e) {
                    $("#vmbAddToWishlistWishListModal").remove();
                });

            ACC.wishlists.bindAddToWishListModalHandlers();
            ACC.wishlists.bindPostAddToWishListLink();
            ACC.wishlists.bindWishListForm();
        });
    },

    bindAddToWishListFromPDPClick: function () {
        $(".js-add-to-wishlist").click(function (event) {
            event.preventDefault();

            ACC.wishlists.clearGlobalAlerts();

            var orderProducts = [];

            $.each($(".vmb-product-quantity"), function () {

                if (!(_.isEmpty($(this).val())) && parseInt($(this).val()) > 0) {
                    orderProducts.push({
                        'productCode': "" + $(this).data('code'),
                        'quantity': parseInt($(this).val())
                    });
                }
            });
            ACC.wishlists.addProductsToWishlist(orderProducts);
        });

    },

    bindAddToWishListFromCartClick: function () {
        $(".js-add-to-wishlist-from-cart").click(function (event) {
            event.preventDefault();

            ACC.wishlists.clearGlobalAlerts();

            var orderProducts = [];

            // collect all selected products and quantities
            $(".js-select-all-products-wish-list-item:checkbox:checked").each(function () {
                // find out the quantity for this element
                var targetClass = $(this).data('wishlist-target-class');
                // get the quantity
                var quantity = $("." + targetClass).val();
                if (!quantity) {
                    quantity = 1;
                }

                orderProducts.push({
                    'productCode': "" + $(this).data('code'),
                    'quantity': quantity
                });
            });

            ACC.wishlists.addProductsToWishlist(orderProducts);
        });
        ACC.wishlists.bindCheckboxListeners();
        $(".js-select-all-products-wish-list").change();
    },

    bindAddToWishListModalHandlers: function () {

        $(document).on("click", ".js-add-to-wishlist-option", function () {
            $(".js-add-to-wishlist-option-new-body").slideUp();
        });

        $(document).on("click", ".js-add-to-wishlist-option-new", function () {
            $('.js-add-to-wishlist-option-new-body').slideDown();
            $('.js-add-to-wishlist-option-existing-body').slideUp();
            ACC.wishlists.validateWishlistName($('#newWishlistName'));
            ACC.wishlists.validateWishlistDescription($('#newWishListDescription'));
            ACC.wishlists.enableOrDisableWishlistSaveButton($('#saveBasketButton'));
        });

        $(document).on("click", ".js-add-to-wishlist-option-existing", function () {
            $('.js-add-to-wishlist-option-existing-body').slideDown();
            $('.js-add-to-wishlist-option-new-body').slideUp();
            ACC.wishlists.checkExistingWishlist();
        });
    },

    showRemoveUnavailableProductsModal: function () {
        if ($('#brokenWishlistName').val() !== "") {
            $("#vmbUnavailableProductsModal").modal();

            $('#js-wishlist-delete-unavailable').click(function (event) {

                var wishlistId = $('#brokenWishlistName').val();
                var actionType =  $('#actionType').val();
                $("#vmbUnavailableProductsModal")
                url = ACC.config.encodedContextPath + '/my-account/saved-baskets/remove-unavailable';
                $.post({
                    type: 'POST',
                    url: url,
                    data: {wishlistName : wishlistId},
                    success: function (data, textStatus, request) {
                        $("#vmbUnavailableProductsModal").modal('hide');
                        switch (actionType) {
                            case "restore":
                                $.get({
                                    url: ACC.config.encodedContextPath + '/my-account/saved-baskets/' + wishlistId + '/restore'
                                }).done(function (data) {
                                    $("body").append(data);

                                    $("#vmbRestoreWishListModal").modal()
                                        .on('hidden.bs.modal', function (e) {
                                            $("#vmbRestoreWishListModal").remove();
                                            $('.modal-backdrop').remove();
                                            window.location.reload();

                                        });
                                    ACC.wishlists.bindRestoreModalHandlers();
                                    ACC.wishlists.bindPostRestoreWishListLink();
                                });
                                break;
                            case "export":
                                    let url = ACC.config.encodedContextPath + '/export-basket?basketName=' + wishlistId
                                        + '&basketType=WISHLIST';

                                    $.get(url)
                                        .done(function (data) {
                                            $("body").append(data);

                                            $("#vmbExportWishListModal").modal()
                                                .on('hidden.bs.modal', function (e) {
                                                    $("#vmbExportWishListModal").remove();
                                                    window.location.reload();
                                                });

                                            ACC.wishlists.bindExportModalHandlers();
                                            ACC.wishlists.bindPostExportWishListLink();

                                        });
                                break;

                            default:
                                var wishlistUrl = ACC.config.encodedContextPath + "/my-account/saved-baskets/" + wishlistId;
                                window.location.replace(wishlistUrl);
                        }
                    }
                });
            });
        }
    },

    bindPostAddToWishListLink: function () {
        $("#saveBasketButton").on("click", function (event) {
            event.preventDefault();
            let url = $("#addToWishListForm").prop("action");
            $.post({
                type   : 'POST',
                url    : url,
                data   : $('#addToWishListForm').serialize(),
                success: function(data, textStatus, request) {
                    var wishListUrl = ACC.config.encodedContextPath + request.getResponseHeader('wishListUrl');
                    var successMessage = request.getResponseHeader('successMessage');
                    ACC.wishlists.displayGlobalCustomAlert("<div class=\"alert alert-info alert-dismissable getAccAlert\">\n" +
                            "        <button class=\"close closeAccAlert\" aria-hidden=\"true\" data-dismiss=\"alert\" type=\"button\">&times;</button>" +
                            "        <a href=\"" + wishListUrl + "\">" + successMessage + "</a>" +
                            "    </div>");

                    $('#vmbAddToWishlistWishListModal').modal('hide');
                    $('#vmbRestoreWishListModal').modal('hide');

                },
                error  : function(xhr, textStatus, request) {
                    let errorMessage = JSON.parse(xhr.responseText);
                    ACC.wishlists.displayGlobalAlert({
                        type: 'error',
                        message: errorMessage
                    });
                }
            });
            $('#vmbAddToWishlistWishListModal').modal('hide');
            $('#vmbRestoreWishListModal').modal('hide');
        });
    },

    bindCheckboxListeners: function () {
        $(".js-select-all-products-wish-list").change(function () {
            // set all checkboxes to the same value as the current one
            var checked = this.checked;

            // Iterate each checkbox
            $('.js-select-all-products-wish-list-item').prop('checked', checked);
            $('.js-select-all-products-wish-list-item').change();
        });

        $(".js-select-all-products-wish-list-item").change(function () {
        var checked = this.checked;
            var targetClass = $(this).data('wishlist-target-class');
            if (!this.checked) {
                $('.js-select-all-products-wish-list').prop('checked', checked);
            }
            // update the values
            $("." + targetClass).attr("data-wishlist-selected", checked);
        });
    },
    bindRestoreWishListClick: function () {
        ACC.wishlists.bindCheckboxListeners();

        $(".js-restore-wish-list").click(function (event) {
            event.preventDefault();

            ACC.wishlists.clearGlobalAlerts();

            var productCodes = []
            // collect all selected products
            $(".js-select-all-products-wish-list-item:checkbox:checked").each(function () {
                productCodes.push($(this).data('code'));
            });

            if (productCodes.length <= 0 && !$(this).hasClass("js-restore-wish-list-all")) {
                ACC.wishlists.displayGlobalAlert({type: 'warning', messageId: 'wishlist-no-products-selected-message'});

                return;
            }

            var cartId = $(this).data('wishlist-id');
            var url = ACC.config.encodedContextPath + '/my-account/saved-baskets/' + cartId + '/restore';

            $.get({
                url: url,
                data: $.param({productCodes: productCodes}, true)
            }).done(function (data) {
                $("body").append(data);

                $("#vmbRestoreWishListModal").modal()
                    .on('hidden.bs.modal', function (e) {
                        $("#vmbRestoreWishListModal").remove();
                    });

                ACC.wishlists.bindRestoreModalHandlers();
                ACC.wishlists.bindPostRestoreWishListLink();
            }).fail(function (response) {
                $('#brokenWishlistName').val(cartId);
                $('#actionType').val("restore");
                ACC.wishlists.showRemoveUnavailableProductsModal();
            })
        });
    },

    bindExportBasketClick: function () {
        $(".js-export-basket").click(function (event) {

            event.preventDefault();
            let basketName = $(this).data('basket-name');
            let basketType = $(this).data('basket-type');

            let url = ACC.config.encodedContextPath + '/export-basket?basketName=' + basketName
                                                    + '&basketType=' + basketType;

            $.get(url)
                .done(function (data) {
                $("body").append(data);

                $("#vmbExportWishListModal").modal()
                    .on('hidden.bs.modal', function (e) {
                        $("#vmbExportWishListModal").remove();
                    });

                ACC.wishlists.bindExportModalHandlers();
                ACC.wishlists.bindPostExportWishListLink();
            }).fail(function (response) {
                $('#brokenWishlistName').val(basketName);
                $('#actionType').val("export");
                ACC.wishlists.showRemoveUnavailableProductsModal();
            })
        });
    },


    bindExportModalHandlers: function () {

        $(document).on("click", "#exportWishListAsPdf", function () {
            $("#exportWishListAsPdfBody").slideDown();
            $("#exportWishListAsExcelBody").slideUp();
        });

        $(document).on("click", "#exportWishListAsExcel", function () {
            $("#exportWishListAsPdfBody").slideUp();
            $("#exportWishListAsExcelBody").slideDown();
        });

        $(document).on("click", ".btn-do-export", function (e) {
            e.preventDefault();
            // Coding
            $('#exportSavedBasketForm').submit();
            $('#vmbExportWishListModal').modal('hide');
            return false;
        });

        // MAke sure PDF Export is activated
        $('#exportWishListAsPdf').click();
    },

    bindPostExportWishListLink: function () {
        $(document).on("click", '.js-wish-list-export-btn', function (event) {
            event.preventDefault();

            $('#restoreWishListForm').submit();
        });
    },

    bindRestoreModalHandlers: function () {

        ACC.wishlists.$wishListRestoreBtn = $('.js-wish-list-restore-btn');
        ACC.wishlists.$currentCartName = $('.js-current-cart-name');
        ACC.wishlists.$newWishListDescription = $('#newWishListDescription');
        ACC.wishlists.slideUpRestoreWishlistForm();

        ACC.wishlists.$currentCartName.keyup(function() {
            ACC.wishlists.validateWishlistName(ACC.wishlists.$currentCartName);
            ACC.wishlists.enableOrDisableWishlistSaveButton(ACC.wishlists.$wishListRestoreBtn);
        })

        ACC.wishlists.$newWishListDescription.keyup(function() {
            ACC.wishlists.validateWishlistDescription(ACC.wishlists.$newWishListDescription);
            ACC.wishlists.enableOrDisableWishlistSaveButton(ACC.wishlists.$wishListRestoreBtn);
        });


        $(document).on("click", "#keepRestoredCart", function () {
            $("#activeCartName").prop("disabled", false);
            $("#activeCartName").parent().slideDown();
            $("#newWishListDescription").parent().slideDown();
            ACC.wishlists.validateWishlistName(ACC.wishlists.$currentCartName);
            ACC.wishlists.validateWishlistDescription(ACC.wishlists.$newWishListDescription);
            ACC.wishlists.enableOrDisableWishlistSaveButton(ACC.wishlists.$wishListRestoreBtn);
        });

        ACC.wishlists.$currentCartName.focus(function(event) {
            ACC.wishlists.validateWishlistName(ACC.wishlists.$currentCartName);
        });


        $(document).on("click", "#preventSaveActiveCart", function () {
            ACC.wishlists.slideUpRestoreWishlistForm();
        });

        $(document).on("click", "#preventMergeActiveCart", function () {
            ACC.wishlists.slideUpRestoreWishlistForm();
        });
    },

    slideUpRestoreWishlistForm: function () {
        $("#activeCartName").parent().slideUp();
        $("#newWishListDescription").parent().slideUp();
        $("#activeCartName").prop("disabled", true);
        ACC.wishlists.$wishListRestoreBtn.removeAttr('disabled');
    },

    bindPostRestoreWishListLink: function () {

        $(document).on("click", '.js-wish-list-restore-btn', function (event) {
            event.preventDefault();

            var cartName = $('#activeCartName').val();
            var newWishListDescription = $('#newWishListDescription').val();
            var url = $(this).data('restore-url');

            // collect the product codes
            var products = [];
            $("input[name='productCodes']").each(function (index, input) {
                var productCode = input.value;
                var targetClass = 'wishlist_check_' + productCode;
                var quantity = $("." + targetClass).val();

                products.push({
                    'productCode': "" + productCode,
                    'quantity': quantity
                });
            });


            // done
            var preventWishListChecked = $('.js-prevent-save-active-cart').prop('checked');
            var preventMergedWishListChecked = $('.js-prevent-merge-active-cart').prop('checked');
            var postData = {
                preventSaveActiveCart: preventWishListChecked !== undefined ? preventWishListChecked : false,
                preventMergeActiveCart: preventMergedWishListChecked !== undefined ? preventMergedWishListChecked : false,
                keepRestoredCart: false,
                cartName: cartName,
                newWishListDescription: newWishListDescription,
                wishlistProducts: products
            };

            $.post({
                url: url,
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(postData),
                beforeSend: function () {
                    showLoader();
                },
            }).done(function() {
                var url = ACC.config.encodedContextPath + "/cart";
                window.location.replace(url);
            }).fail(function(response) {
                var errorMessage = response.responseText;
                ACC.wishlists.displayGlobalAlert({
                    type: 'error',
                    message: errorMessage
                });
            }).always(function() {
                hideLoader();
            });

            $('#vmbRestoreWishListModal').modal('hide');
        });
    },

    bindDeleteWishListLink: function () {
        $(document).on("click", '.js-delete-wish-list', function (event) {
            event.preventDefault();
            var cartId = $(this).data('wishlist-id');
            var cartName = $(this).data('wishlist-name');

            $("#vmbConfirmWishListDeleteCode").val(cartId);
            $("#vmbConfirmWishListDeleteName").html(cartName);

            $("#vmbConfirmWishListDeleteModal").modal();
        });
    },

    bindDeleteConfirmLink: function () {
        $(document).on("click", '.js-wishlist_delete_confirm', function (event) {
            event.preventDefault();
            $(this).attr("disabled", true);
            var cartId = $("#vmbConfirmWishListDeleteCode").val();
            var url = ACC.config.encodedContextPath + '/my-account/saved-baskets/' + cartId + '/delete';
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function (response) {
                    var url = ACC.config.encodedContextPath + "/my-account/saved-baskets";
                    window.location.replace(url);
                }
            });
        });
    },

    checkExistingWishlist: function () {
        var existingWishlist = $('input[name="wishListAddTo"]:checked').val();
        if (undefined !== existingWishlist && existingWishlist.length !== 0) {
            $('#saveBasketButton').prop('disabled', false);
        }
    },

    bindWishListForm: function () {
        $('.js-add-to-wishlist-option').click(function() {
            ACC.wishlists.checkExistingWishlist();
        });

        if(undefined !== $('#newWishlistName').val() && $('#newWishlistName').val().length == 0) {
            $('#saveBasketButton').prop('disabled', true);
        }

        $('#newWishlistName').keyup(function() {
            ACC.wishlists.validateWishlistName($('#newWishlistName'));
            ACC.wishlists.enableOrDisableWishlistSaveButton($('#saveBasketButton'));
        })

        $('#newWishListDescription').keyup(function() {
            ACC.wishlists.validateWishlistDescription($('#newWishListDescription'));
            ACC.wishlists.enableOrDisableWishlistSaveButton($('#saveBasketButton'));
        });

        $(document).on("click", '#wishList #wishListButton', function(e) {
            e.preventDefault();

            if(!_.isEmpty($('#wishListName').val())) {
                $('#wishListForm').submit();
                $('#wishList').modal('hide');
            } else {
                $('#wishListName').parent().addClass("has-error");
            }

        });

    },

    bindEditWishListForm: function () {

        $('.js-wishlist-delete-item').click(function () {
            var targetInput = '.' + $(this).data('wishlist-target-class');

            // set quantity to 0
            $(targetInput).val('0');
        })

        if (undefined !== $('#wishListName').val() && $('#wishListName').val().length == 0) {
            $('#saveBasketButton').prop('disabled', true);
        }

        $('#wishListName').keyup(function() {
            ACC.wishlists.validateWishlistName($('#wishListName'));
            ACC.wishlists.enableOrDisableWishlistSaveButton($('#saveBasketButton'));
        });

        $('#wishListDescription').keyup(function() {
            ACC.wishlists.validateWishlistDescription($('#wishListDescription'));
            ACC.wishlists.enableOrDisableWishlistSaveButton($('#saveBasketButton'));
        });
    },

    charactersLeftInit: function() {
        $('#remain').text($('#localized_val').attr('value') + ' : ' + (255 - $('#wishListName').val().length));
        $('#remainTextArea').text($('#localized_val').attr('value') + ' : ' + (255 - $('#wishListDescription').val().length));
    },

    validateWishlistName: function(domElement) {
        //Validate value through Regex
        var nameRegex = /^[ A-Za-z0-9\u00C0-\u00ff_-]*$/;
        var nameResult = nameRegex.test(domElement.val());

        if(nameResult == false) {
            var value = $('#localized_error_name').attr('value');
            $('#validate-error-name').text(value);
        } else {
            $('#validate-error-name').text("");
        }

        var tlength = domElement.val().length;
        if(tlength > 0 && nameResult == true) {
            domElement.parent().removeClass("has-error");
        } else {
            domElement.parent().addClass("has-error");
        }
    },

    validateWishlistDescription: function(domElement) {
        // limit the text length
        var maxchars = VMB.constraints.savedCartDescriptionFieldLimit;
        var value = $('#localized_val').attr('value');
        var tlength = domElement.val().length;
        remain = maxchars - parseInt(tlength);
        $('#remainTextArea').text(value + ' : ' + remain);

        //Validate value through Regex
        var descriptionRegex = /^[ A-Za-z0-9\u00C0-\u00ff?!_\n@ :;()*%€\/\<\\>,."”'@&+=-]*$/;
        var descriptionResult = descriptionRegex.test(domElement.val());

        if(descriptionResult == false) {
            var value = $('#localized_error_description').attr('value');
            $('#validate-error-description').text(value);
            domElement.parent().addClass("has-error");
        } else {
            domElement.parent().removeClass("has-error");
            $('#validate-error-description').text("");
        }
    },

    enableOrDisableWishlistSaveButton: function(saveButton) {
        var errors = $("div.has-error").length;
        if(errors > 0) {
            saveButton.prop('disabled', true);
        } else {
            saveButton.prop('disabled', false);
        }
    },

    bindUpdateUploadingWishLists: function () {
        var cartIdRowMapping = $(".js-uploading-wish-lists-update").data("idRowMapping");
        var refresh = $(".js-uploading-wish-lists-update").data("refreshCart");
        if (cartIdRowMapping && refresh) {
            var interval = $(".js-uploading-wish-lists-update").data("refreshInterval");
            var arrCartIdAndRow = cartIdRowMapping.split(',');
            var mapCartRow = new Object();
            var cartCodes = [];
            for (i = 0; i < arrCartIdAndRow.length; i++) {
                var arrValue = arrCartIdAndRow[i].split(":");
                if (arrValue != "") {
                    mapCartRow[arrValue[0]] = arrValue[1];
                    cartCodes.push(arrValue[0]);
                }
            }

            if (cartCodes.length > 0) {
                setTimeout(function () {
                    ACC.wishlists.refreshWorker(cartCodes, mapCartRow, interval)
                }, interval);
            }
        }
    },

    refreshWorker: function (cartCodes, mapCartRow, interval) {
        $.ajax({
            dataType: "json",
            url: ACC.config.encodedContextPath + '/my-account/saved-baskets/uploadingCarts',
            data: {
                cartCodes: cartCodes
            },
            type: "GET",
            traditional: true,
            success: function (data) {
                if (data != undefined) {
                    var hidden = "hidden";
                    var rowId = "#row-";
                    for (i = 0; i < data.length; i++) {
                        var cart = data[i];

                        var index = $.inArray(cart.code, cartCodes);
                        if (index > -1) {
                            cartCodes.splice(index, 1)
                        }
                        var rowIdIndex = mapCartRow[cart.code];
                        if (rowIdIndex != undefined) {
                            var rowSelector = rowId + rowIdIndex;
                            $(rowSelector + " .js-wish-list-name").removeClass("not-active");
                            $(rowSelector + " .js-wish-list-date").removeClass(hidden);
                            $(rowSelector + " .js-file-importing").remove();
                            $(rowSelector + " .js-wish-list-description").text(cart.description);
                            var numberOfItems = cart.entries.length;
                            $(rowSelector + " .js-wish-list-number-of-items").text(numberOfItems);
                            $(rowSelector + " .js-wish-list-total").text(cart.totalPrice.formattedValue);
                            if (numberOfItems > 0) {
                                $(rowSelector + " .js-restore-wish-list").removeClass(hidden);
                                $(rowSelector + " .js-export-basket").removeClass(hidden);
                            }
                            $(rowSelector + " .js-delete-wish-list").removeClass(hidden);
                        }
                    }
                }

                if (cartCodes.length > 0) {
                    setTimeout(function () {
                        ACC.wishlists.refreshWorker(cartCodes, mapCartRow, interval)
                    }, interval);
                }
            }
        })
    },

    displayGlobalAlert: function (options) {
        ACC.wishlists.clearGlobalAlerts();

        if ($(".global-alerts").length === 0) {
            $("body").prepend("<div class='global-alerts'></div>");
        }

        var alertTemplateSelector;

        switch (options.type) {
            case 'error':
                alertTemplateSelector = '#global-alert-danger-template';
                break;
            case 'warning':
                alertTemplateSelector = '#global-alert-warning-template';
                break;
            default:
                alertTemplateSelector = '#global-alert-info-template';
        }

        if (typeof options.message != 'undefined') {
            $('.global-alerts').append($(alertTemplateSelector).tmpl({message: options.message}));
        }

        if (typeof options.messageId != 'undefined') {
            $('.global-alerts').append($(alertTemplateSelector).tmpl({message: $('#' + options.messageId).text()}));
        }

        $(".closeAccAlert").on("click", function () {
            $(this).parent('.getAccAlert').remove();
        });
    },

    displayGlobalCustomAlert: function (message) {
        ACC.wishlists.clearGlobalAlerts();

        if ($(".global-alerts").length === 0) {
            $("body").prepend("<div class='global-alerts'></div>");
        }

        $('.global-alerts').append(message);

        $(".closeAccAlert").on("click", function () {
            $(this).parent('.getAccAlert').remove();
        });
    },

    clearGlobalAlerts: function () {
        $('.global-alerts').empty();
    }
};
