<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/nav" %>
<%@ taglib prefix="event" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/event" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row no-margin">
    <div class="col-xs-10 col-xs-push-1 col-sm-offset-1 col-sm-push-0 col-sm-10 col-md-offset-0 col-md-3 col-lg-2 vmb-image-container">
        <product:productImagePanel galleryImages="${galleryImages}"/>
    </div>
    <div class="col-xs-10 col-xs-push-1 col-sm-push-0 col-sm-offset-1 col-sm-10 col-md-offset-0 col-md-9 col-lg-10 no-padding">
        <div class="vmb-product-main-info">
            <h1 class="product-title">${fn:escapeXml(product.name)}</h1>
            <div class="description">${ycommerce:sanitizeHTML(product.summary)}</div>
            <div class="product-logo-filters-container">
                <c:if test="${not empty product.brand.logo}">
                    <div class="vmb-product-brandLogo">
                        <img src="${product.brand.logo.url}" alt="brand-logo"/>
                    </div>
                </c:if>
                <nav:productDetailAppliedFilters pageData="${searchPageData}"/>
            </div>

            <product:productPromotionSection product="${product}"/>
            <product:productVariantTable/>
        </div>
    </div>
    <event:updateStock/>
</div>