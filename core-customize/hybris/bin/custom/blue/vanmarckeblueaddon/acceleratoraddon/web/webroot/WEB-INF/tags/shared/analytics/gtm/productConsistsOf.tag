<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<script type="text/javascript">

    <c:set var="prodPosition" value="${1}" />

    dataLayer.push(
        <json:object>
            <json:property name="event" value="impressions"/>
            <json:object name="ecommerce">
                <json:property name="currencyCode" value="${currentCurrency.isocode}"/>
                <json:array name="impressions" items="${product.consistsOfReferences}" var="productRef">
                    <json:object>
                        <json:property name='name'>${productRef.target.analyticsData.name}</json:property>
                        <json:property name='id'>${productRef.target.code}</json:property>
                        <json:property name="price" value="${productRef.target.price.value}"/>
                        <json:property name="category" value=""/>
                        <json:property name="list" value="Productreferences ${productRef.referenceType}"/>
                        <json:property name="position" value="${prodPosition}"/>
                    </json:object>
                    <c:set var="prodPosition" value="${prodPosition + 1}" />
                </json:array>
            </json:object>
        </json:object>
    );
</script>