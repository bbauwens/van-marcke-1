<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>

<c:if test="${empty favoriteStore}">
    <span data-toggle="tooltip" data-placement="top" title="<spring:theme code="text.stockstatus.unknown" htmlEscape="false"/>" class="glyphicon glyphicon-question-sign vmb-stock-info ${cssClass}"></span>
</c:if>