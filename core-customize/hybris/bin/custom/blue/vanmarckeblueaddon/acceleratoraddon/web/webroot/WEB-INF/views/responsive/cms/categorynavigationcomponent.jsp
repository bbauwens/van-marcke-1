<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:if test="${component.visible}">
    <ul class="vmb-category-menu">
        <c:forEach items="${component.navigationNode.children}" var="rootLevel">
            <c:if test="${rootLevel.uid eq currentSite.startNavigationNode && not empty rootLevel.children}">
                <c:forEach items="${rootLevel.children}" var="childLevel1">
                    <c:if test="${not empty childLevel1.visibleChildren}">
                        <li class="childlevel1 childlevel1-has__sub js-enquire-has-sub">
                            <c:forEach items="${childLevel1.entries}" var="childlink1">
                                <cms:component component="${childlink1.item}" evaluateRestriction="true" element="span" class="childlevel1__link"/>
                            </c:forEach>
                            <c:if test="${not empty childLevel1.visibleChildren}"><span class="glyphicon glyphicon-triangle-right"></span>
                                <div class="sub-menu">
                                    <c:forEach items="${childLevel1.visibleChildren}" var="childLevel2">
                                        <div class="childlevel2">
                                            <c:forEach items="${childLevel2.entries}" var="childlink2">
                                                <div class="childlevel2__link">
                                                    <cms:component component="${childlink2.item}" evaluateRestriction="true" element=""/>
                                                    <c:if test="${not empty childLevel2.visibleChildren}">
                                                        <span class="glyphicon glyphicon-triangle-right hidden-lg hidden-md"></span>
                                                    </c:if>
                                                </div>
                                            </c:forEach>
                                            <c:if test="${not empty childLevel2.visibleChildren}">
                                                <ul class="childlevel3">
                                                    <c:forEach items="${childLevel2.visibleChildren}" var="childLevel3" varStatus="i">
                                                        <c:forEach items="${childLevel3.entries}" var="childlink3">
                                                            <cms:component component="${childlink3.item}" evaluateRestriction="true" element="li" class="childlevel3__link"/>
                                                        </c:forEach>
                                                    </c:forEach>
                                                </ul>
                                            </c:if>
                                        </div>
                                    </c:forEach>
                                </div>
                            </c:if>
                        </li>
                    </c:if>
                </c:forEach>
            </c:if>
        </c:forEach>
    </ul>
</c:if>