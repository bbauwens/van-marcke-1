package com.vanmarcke.blue.addon.controllers.cms;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.cmsfacades.util.builder.ProductModelBuilder;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueProductReferencesComponentControllerTest {

    private static final String BASE_PRODUCT_PREFIX = "BP_";
    private static final String PRODUCT_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String COMPONENT_TITLE = RandomStringUtils.randomAlphabetic(10);
    private static final int RANDOM_NUMBER = new Random().nextInt(10);
    private static final List<ProductOption> PRODUCT_OPTIONS = singletonList(ProductOption.BASIC);

    @Mock
    private ProductFacade productFacade;

    @Spy
    @InjectMocks
    private BlueProductReferencesComponentController controller;

    @Test
    public void testFillModelWithNoProduct() {
        Model model = mock(Model.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        ProductReferencesComponentModel component = mock(ProductReferencesComponentModel.class);
        RequestContextData requestContextData = mock(RequestContextData.class);

        when(requestContextData.getProduct()).thenReturn(null);
        doReturn(requestContextData).when(controller).getRequestContextData(request);

        controller.fillModel(request, model, component);

        verifyZeroInteractions(model, productFacade);
    }

    @Test
    public void testFillModelWithBaseProduct() {
        Model model = mock(Model.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        ProductReferencesComponentModel component = mock(ProductReferencesComponentModel.class);
        RequestContextData requestContextData = mock(RequestContextData.class);
        List<ProductReferenceTypeEnum> enumList = new ArrayList<>();
        List<ProductReferenceData> productReferences = new ArrayList<>();

        ProductModel product = ProductModelBuilder.aModel().withCode(BASE_PRODUCT_PREFIX + PRODUCT_CODE).build();

        when(component.getProductReferenceTypes()).thenReturn(enumList);
        when(component.getMaximumNumberProducts()).thenReturn(RANDOM_NUMBER);
        when(component.getTitle()).thenReturn(COMPONENT_TITLE);
        when(requestContextData.getProduct()).thenReturn(product);
        when(productFacade.getProductReferencesForCode(PRODUCT_CODE, enumList, PRODUCT_OPTIONS, RANDOM_NUMBER)).thenReturn(productReferences);

        doReturn(requestContextData).when(controller).getRequestContextData(request);

        controller.fillModel(request, model, component);

        verify(productFacade).getProductReferencesForCode(PRODUCT_CODE, enumList, PRODUCT_OPTIONS, RANDOM_NUMBER);
        verify(model).addAttribute("title", COMPONENT_TITLE);
        verify(model).addAttribute("productReferences", productReferences);
    }

    @Test
    public void testFillModelWithVariantProduct() {
        Model model = mock(Model.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        ProductReferencesComponentModel component = mock(ProductReferencesComponentModel.class);
        RequestContextData requestContextData = mock(RequestContextData.class);
        List<ProductReferenceTypeEnum> enumList = new ArrayList<>();
        List<ProductReferenceData> productReferences = new ArrayList<>();

        ProductModel product = ProductModelBuilder.aModel().withCode(PRODUCT_CODE).build();

        when(component.getProductReferenceTypes()).thenReturn(enumList);
        when(component.getMaximumNumberProducts()).thenReturn(RANDOM_NUMBER);
        when(component.getTitle()).thenReturn(COMPONENT_TITLE);
        when(requestContextData.getProduct()).thenReturn(product);
        when(productFacade.getProductReferencesForCode(PRODUCT_CODE, enumList, PRODUCT_OPTIONS, RANDOM_NUMBER)).thenReturn(productReferences);

        doReturn(requestContextData).when(controller).getRequestContextData(request);

        controller.fillModel(request, model, component);

        verify(productFacade).getProductReferencesForCode(PRODUCT_CODE, enumList, PRODUCT_OPTIONS, RANDOM_NUMBER);
        verify(model).addAttribute("title", COMPONENT_TITLE);
        verify(model).addAttribute("productReferences", productReferences);
    }
}