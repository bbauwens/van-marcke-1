<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <cms:pageSlot position="BlueCategoryContent1" var="content1" limit="1">
        <cms:component component="${content1}" element="div" class="col-xs-12"/>
    </cms:pageSlot>
</div>

<div class="row">
    <cms:pageSlot position="BlueCategoryContent2" var="content2" limit="1">
        <cms:component component="${content2}" element="div" class="col-sm-12 col-md-6"/>
    </cms:pageSlot>
    <cms:pageSlot position="BlueCategoryContent3" var="content3" limit="1">
        <cms:component component="${content3}" element="div" class="col-sm-12 col-md-6"/>
    </cms:pageSlot>
</div>

<div class="row">
    <cms:pageSlot position="BlueCategoryContent4" var="content4" limit="1">
        <cms:component component="${content4}" element="div" class="col-xs-12"/>
    </cms:pageSlot>
</div>