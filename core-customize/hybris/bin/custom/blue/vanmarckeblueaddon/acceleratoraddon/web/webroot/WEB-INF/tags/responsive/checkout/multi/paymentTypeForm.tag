<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<ycommerce:testId code="checkoutStepTwo">
    <div class="step-body-form">


        <c:if test="${not isCreditWorthy}">
            <div class="alert alert-info">
                <c:choose>
                    <c:when test="${isCreditLimitExceeded}">
                        <spring:theme code="text.checkout.payment.d1.credit.info"/>
                    </c:when>
                    <c:otherwise>
                        <spring:theme code="checkout.multi.paymentMethod.accountPayment.unavailable"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </c:if>

        <c:if test="${isCreditLimitWarning eq true}">
            <div class="alert alert-info">
                <spring:theme code="text.checkout.payment.d2.credit.info"/>
            </div>
        </c:if>
        <form:form id="selectPaymentTypeForm" modelAttribute="paymentTypeForm"
                   action="${request.contextPath}/checkout/multi/payment-type/choose" method="post">
            <div class="radiobuttons_paymentselection">
                <c:forEach items="${paymentTypes}" var="paymentType">
                    <div class="radiobuttons_paymentselection__type">
                        <form:radiobutton path="paymentType" id="PaymentTypeSelection_${paymentType.code}"
                                          value="${paymentType.code}" label="${paymentType.displayName}"/>
                        <c:choose>
                            <c:when test="${paymentType.code eq 'CARD'}">
                                <c:if test="${not empty paymentModes}">
                                    <div class="radiobuttons_paymentselection__icons">
                                        <c:forEach var="paymentMode" items="${paymentModes}">
                                            <c:if test="${not empty paymentMode.thumbnail && not empty paymentMode.thumbnail.url}">
                                                <img class="radiobuttons_paymentselection__icon" src="${paymentMode.thumbnail.url}" alt="${paymentMode.code}"/>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                </c:if>
                            </c:when>
                            <c:when test="${paymentType.code eq 'ACCOUNT'}">
                            </c:when>
                        </c:choose>
                    </div>
                </c:forEach>
            </div>

            <div class="text-right">
                <button id="choosePaymentType_continue_button" type="submit" class="btn btn-default">
                    <spring:theme code="checkout.multi.paymentType.continue"/>
                </button>
            </div>
        </form:form>
    </div>
</ycommerce:testId>