package com.vanmarcke.blue.addon.checkout.steps.validation.impl;

import com.vanmarcke.blue.addon.facades.BlueCheckoutFlowFacade;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueResponsivePaymentCheckoutStepValidatorTest {

    @Mock
    private BlueCheckoutFlowFacade checkoutFlowFacade;

    @InjectMocks
    private BlueResponsivePaymentCheckoutStepValidator responsivePaymentCheckoutStepValidator;

    @Test
    public void testValidateOnEnter_withInvalidCart() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(false);

        ValidationResults result = responsivePaymentCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.REDIRECT_TO_CART);

        verify(checkoutFlowFacade, never()).hasNoDeliveryAddress();
        verify(checkoutFlowFacade, never()).hasNoDeliveryMode();
        verify(checkoutFlowFacade, never()).hasNoDeliveryPointOfService();
    }

    @Test
    public void testValidateOnEnter_withoutDeliveryAddress() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(true);

        when(checkoutFlowFacade.hasNoDeliveryAddress()).thenReturn(true);

        ValidationResults result = responsivePaymentCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.REDIRECT_TO_DELIVERY_METHOD);

        verify(checkoutFlowFacade, never()).hasNoDeliveryMode();
        verify(checkoutFlowFacade, never()).hasNoDeliveryPointOfService();
    }

    @Test
    public void testValidateOnEnter_withoutDeliveryMode() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(true);

        when(checkoutFlowFacade.hasNoDeliveryAddress()).thenReturn(false);

        when(checkoutFlowFacade.hasNoDeliveryMode()).thenReturn(true);

        ValidationResults result = responsivePaymentCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.REDIRECT_TO_DELIVERY_METHOD);

        verify(checkoutFlowFacade, never()).hasNoDeliveryPointOfService();
    }

    @Test
    public void testValidateOnEnter_withoutDeliveryPointOfService() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(true);

        when(checkoutFlowFacade.hasNoDeliveryAddress()).thenReturn(false);

        when(checkoutFlowFacade.hasNoDeliveryMode()).thenReturn(false);

        when(checkoutFlowFacade.hasNoDeliveryPointOfService()).thenReturn(true);

        ValidationResults result = responsivePaymentCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.REDIRECT_TO_DELIVERY_METHOD);
    }

    @Test
    public void testValidateOnEnter() {
        RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);

        when(checkoutFlowFacade.hasValidCart()).thenReturn(true);

        when(checkoutFlowFacade.hasNoDeliveryAddress()).thenReturn(false);

        when(checkoutFlowFacade.hasNoDeliveryMode()).thenReturn(false);

        when(checkoutFlowFacade.hasNoDeliveryPointOfService()).thenReturn(false);

        ValidationResults result = responsivePaymentCheckoutStepValidator.validateOnEnter(redirectAttributes);
        assertThat(result).isEqualTo(ValidationResults.SUCCESS);
    }
}