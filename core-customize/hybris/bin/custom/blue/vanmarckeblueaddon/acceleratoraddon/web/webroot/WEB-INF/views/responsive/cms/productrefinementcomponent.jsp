<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div id="product-facet" class="hidden-sm hidden-xs product__facet js-product-facet">
    <nav:facetNavAppliedFilters pageData="${searchPageData}"/>
    <nav:facetNavRefinements pageData="${searchPageData}"/>
</div>
<div class="vmb-modal fade" id="vmbMobileRefineModal" tabindex="-1" role="dialog" aria-labelledby="vmbMobileRefineModalTitle">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="vmbMobileRefineModalTitle"><spring:theme code="search.nav.selectRefinements.title"/></h4>
            </div>
            <div class="modal-body">
                <%--- Created by acc.product.js --%>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><spring:theme code="popup.close"/></button>
                <button class="btn btn-default vmb-facets-reset__btn">
                    <spring:message code="product.list.facets.reset"/>
                </button>
            </div>
        </div>
    </div>
</div>