package com.vanmarcke.blue.addon.controllers.util;

import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import org.springframework.ui.Model;

import java.util.LinkedList;
import java.util.List;

/**
 * Holds common controller methods
 *
 * @author Giani Ifrim
 * @since 15-07-2021
 */
public class BlueControllerUtils {

    /**
     * Add attribute to existing model meta tags or add meta tag to the model if not.
     *
     * @param model target model
     * @param key meta tag name
     * @param value meta tag content
     */
    public static void addAttributeToMetaTags(final Model model, final String key, final String value)
    {
        MetaElementData metaElement = new MetaElementData();
        metaElement.setName(key);
        metaElement.setContent(value);

        if (model.getAttribute("metatags") != null) {
            ((List) model.getAttribute("metatags")).add(metaElement);
        } else {
            final List<MetaElementData> metadata = new LinkedList<>();
            metadata.add(metaElement);
            model.addAttribute("metatags", metadata);
        }
    }
}
