ACC.track = {
    trackAddToCart: function (products, quantity) {
        if (isTrackingEnabled()) {
            window.mediator.publish('trackAddToCart', {
                products: products,
                quantity: quantity
            });
        }
    },
    trackRemoveFromCart: function (product, initialCartQuantity) {
        if (isTrackingEnabled()) {
            window.mediator.publish('trackRemoveFromCart', {
                product: product,
                initialCartQuantity: initialCartQuantity
            });
        }
    },

    trackUpdateCart: function (product, initialCartQuantity, newCartQuantity) {
        if (isTrackingEnabled()) {
            if (initialCartQuantity > newCartQuantity) {
                //REMOVE
                ACC.track.trackRemoveFromCart(product, initialCartQuantity - newCartQuantity);
            } else {
                //ADD

                if (!_.isArray(product)) {
                    product = [product];
                }

                ACC.track.trackAddToCart(product, newCartQuantity - initialCartQuantity);
            }
        }
    },

    trackShowReviewClick: function (productCode) {
        if (isTrackingEnabled()) {
            window.mediator.publish('trackShowReviewClick', {});
        }
    },

    productClick: function (actionField, product, callbackUrl) {
        if (isTrackingEnabled()) {
            window.mediator.publish('trackProductClick', {
                product: product,
                actionField: actionField,
                callback: callbackUrl,
            });
        }
    },

    searchResult: function (products, searchTerm) {
        if (isTrackingEnabled()) {
            if (products.length > 0) {
                window.mediator.publish('trackSearch', {
                    products: products,
                    searchTerm: searchTerm
                });
            } else {
                window.mediator.publish('trackSearchNoResult', {
                    searchTerm: searchTerm
                });
            }
        }
    },

    searchClick: function (searchTerm) {
        if (isTrackingEnabled()) {
            window.mediator.publish('trackSearchClick', {
                searchTerm: searchTerm
            });
        }
    },

    checkoutStep: function (step, checkoutOption) {
        if (isTrackingEnabled()) {
            window.mediator.publish('trackCheckoutStep', {
                step: step,
                checkoutOption: checkoutOption
            });
        }
    }
}
;

(function ($) {
    'use strict';
    $(document).on("click", ".vmb-gtm-product-item", function () {
        var dataContainer = $(this).closest(".vmb-gtm-product-data");

        var product = {
            'id': dataContainer.data("code").toString(),
            'name': dataContainer.data("name").toString(),
            'position': dataContainer.data("position"),
            'category': dataContainer.data("category"),
            'brand': dataContainer.data('brand')
        };

        if (!_.isEmpty(dataContainer.data("price"))) {
            product.price = dataContainer.data("price");
        }

        ACC.track.productClick(dataContainer.data('list'), product, window.location.origin + dataContainer.data("url"));

    });

    $(document).on("click", ".vmb-gtm-search", function () {
        ACC.track.searchClick($(this).data("term"));
    });
})(jQuery);

/**
 * Return whether the user has allowed the use of analytical tracking.
 *
 * @returns {boolean} true if tracking is enabled
 */
function isTrackingEnabled() {
    var consents = getCookie('CONSENTS');

    if (!consents) {
        return false;
    }

    var trackingEnabled = false;

    for (var i = 0; i < consents.length; i++) {
        if (consents[i].consentType === 'ANALYTICAL_COOKIES' && consents[i].optType === 'OPT_IN') {
            trackingEnabled = true;
        }
    }
    return trackingEnabled;
}

/**
 * Returns the cookie value for the cookie with the given {@code name}
 *
 * @param name the cookie name
 * @return {any} the cookie value
 */
function getCookie(name) {
    var consents = undefined;

    if (!$.cookie(name) || typeof $.cookie(name) === 'undefined') {
        return consents;
    }

    try {
        var decodedUri = decodeURI($.cookie(name));
        if (decodedUri) {
            consents = JSON.parse(decodedUri);
        }
    } catch (e) {
        console.error('error decoding cookie')
    }

    return consents;
}