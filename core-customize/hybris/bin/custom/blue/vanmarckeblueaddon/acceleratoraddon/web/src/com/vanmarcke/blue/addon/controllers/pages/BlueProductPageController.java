package com.vanmarcke.blue.addon.controllers.pages;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.google.common.collect.Maps;
import com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants;
import com.vanmarcke.blue.addon.controllers.util.BlueControllerUtils;
import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.facades.product.VMKProductFacade;
import com.vanmarcke.facades.search.VMKSolrProductSearchFacade;
import com.vanmarcke.facades.search.builder.VariantSearchStateDataBuilder;
import com.vanmarcke.facades.search.data.VariantSearchStateData;
import com.vanmarcke.services.helper.HreflangHelper;
import com.vanmarcke.services.product.VMKStructuredDataService;
import de.hybris.platform.acceleratorfacades.futurestock.FutureStockFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.FutureStockForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.*;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.FilterQueryOperator;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.Config;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.*;
import java.util.Map.Entry;

import static com.vanmarcke.blue.addon.constants.VanmarckeblueaddonWebConstants.META_TAG_PAGE_DESCRIPTION;
import static com.vanmarcke.blue.addon.constants.VanmarckeblueaddonWebConstants.META_TAG_PAGE_TITLE;
import static java.util.Collections.emptyMap;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections4.MapUtils.isNotEmpty;

/**
 * Controller for product details page
 */
@Controller
@RequestMapping(value = "/**/p")
public class BlueProductPageController extends BlueSearchPageController {

    private static final Logger LOG = Logger.getLogger(BlueProductPageController.class);

    /**
     * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it contains one or more '.' characters. Please see
     * https://jira.springsource.org/browse/SPR-6164 for a discussion on the issue and future resolution.
     */
    private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";

    private static final String FUTURE_STOCK_ENABLED = "storefront.products.futurestock.enabled";
    private static final String STOCK_SERVICE_UNAVAILABLE = "basket.page.viewFuture.unavailable";
    private static final String NOT_MULTISKU_ITEM_ERROR = "basket.page.viewFuture.not.multisku";
    private static final String PRODUCT_NOT_AVAILABLE = "product.pdp.not.available.country";

    @Resource(name = "productModelUrlResolver")
    private UrlResolver<ProductModel> productModelUrlResolver;

    @Resource(name = "productDataUrlResolver")
    private UrlResolver<ProductData> productDataUrlResolver;

    @Resource(name = "productVariantFacade")
    private ProductFacade productFacade;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "productBreadcrumbBuilder")
    private ProductBreadcrumbBuilder productBreadcrumbBuilder;

    @Resource(name = "cmsPageService")
    private CMSPageService cmsPageService;

    @Resource(name = "futureStockFacade")
    private FutureStockFacade futureStockFacade;

    @Resource(name = "productSearchFacade")
    private VMKSolrProductSearchFacade<ProductData> productSearchFacade;

    @Resource(name = "blueProductFacade")
    private VMKProductFacade vmkProductFacade;

    @Resource(name = "hreflangHelper")
    private HreflangHelper hreflangHelper;

    @Resource(name = "localizedMessageFacade")
    private LocalizedMessageFacade localizedMessageFacade;

    @Resource(name = "vmkPageTitleResolver")
    private PageTitleResolver pageTitleResolver;

    @Resource(name = "siteBaseUrlResolutionService")
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Resource(name = "productStructuredDataService")
    private VMKStructuredDataService<ProductData> productStructuredDataService;

    @GetMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN)
    public String productDetail(@PathVariable("productCode") String encodedProductCode,
                                @RequestParam(value = "q", required = false) final String searchQuery,
                                @RequestParam(value = "text", required = false) final String textSearch,
                                @RequestParam(value = "category", required = false) final String categoryCode,
                                Model model,
                                RedirectAttributes redirectAttributes,
                                HttpServletRequest request,
                                HttpServletResponse response)
            throws CMSItemNotFoundException, UnsupportedEncodingException {

        String productCode = decodeWithScheme(encodedProductCode, UTF_8);

        try {
            ProductModel product = productService.getProductForCode(productCode);

            String redirection = checkRequestUrl(request, response, productModelUrlResolver.resolve(product));
            if (StringUtils.isNotEmpty(redirection)) {
                return redirection;
            }

            updatePageTitle(productCode, model);

            String baseProductCode = product instanceof VanMarckeVariantProductModel ? ((VanMarckeVariantProductModel) product).getBaseProduct().getCode() : product.getCode();

            String finalQuery = searchQuery;
            if (searchQuery == null && textSearch != null) {
                finalQuery = textSearch;
            }

            // perform solr search
            ProductSearchPageData<SearchStateData, ProductData> searchPageData = searchSolrProductData(finalQuery, baseProductCode, categoryCode);
            List<ProductData> solrVariants = Collections.emptyList();

            if (searchPageData != null) {
                solrVariants = searchPageData.getResults();
                cleanupBreadcrumbsUrls(searchPageData, "/p/" + encodedProductCode);
                setRemoveFreeTextUrl(searchPageData, "/p/" + encodedProductCode, searchQuery);
                model.addAttribute("searchPageData", searchPageData);
            }

            final ProductData productData = populateProductDetailForDisplay(productCode, model, request, null, solrVariants);

            model.addAttribute("pageType", PageType.PRODUCT.name());
            model.addAttribute("futureStockEnabled", Config.getBoolean(FUTURE_STOCK_ENABLED, false));

            final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
            final String metaDescription = MetaSanitizerUtil.sanitizeDescription(MessageFormat.format(localizedMessageFacade.getMessageForCodeAndCurrentLocale(META_TAG_PAGE_DESCRIPTION), product.getName()));
            final String metaTile = MetaSanitizerUtil.sanitizeDescription(MessageFormat.format(localizedMessageFacade.getMessageForCodeAndCurrentLocale(META_TAG_PAGE_TITLE), product.getName()));

            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_FOLLOW);
            setUpMetaData(model, metaKeywords, metaDescription);
            model.addAttribute("hreflanglinks", getHreflangHelper().generateHreflangMap(product));

            String canonicalProduct = getSiteBaseUrlResolutionService()
                    .getWebsiteUrlForSite(getBaseSiteService().getCurrentBaseSite(), true, productModelUrlResolver.resolve(product));
            model.addAttribute("canonicalLink", canonicalProduct);
            model.addAttribute("productStructuredData", getProductStructuredDataService().createStructuredDataJson(productData));

            BlueControllerUtils.addAttributeToMetaTags(model, "title", metaTile);

            return getViewForPage(model);
        } catch (UnknownIdentifierException ex) {
            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, PRODUCT_NOT_AVAILABLE);
            return REDIRECT_PREFIX + ROOT;
        }
    }

    private ProductSearchPageData<SearchStateData, ProductData> searchSolrProductData(String searchQuery, String baseProductCode, String categoryCode) {
        ProductSearchPageData<SearchStateData, ProductData> searchPageData;
        try {
            VariantSearchStateData variantSearchStateData = new VariantSearchStateDataBuilder()
                    .withContext(SearchQueryContext.PDP)
                    .withSearchQuery(searchQuery)
                    .withCategoryCode(categoryCode)
                    .withFilterQuery("baseProductCode", baseProductCode, FilterQueryOperator.AND)
                    .build();

            searchPageData = encodeSearchPageData(this.productSearchFacade.searchSolrProductVariants(variantSearchStateData));
            LOG.info("Found " + searchPageData.getResults().size() + " results in solr for base product: " + baseProductCode);
            return searchPageData;
        } catch (final ConversionException e) // NOSONAR
        {
            // nothing to do - the exception is logged in SearchSolrQueryPopulator
        }

        return null;
    }

    @GetMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/orderForm")
    public String productOrderForm(@PathVariable("productCode") String encodedProductCode, Model model, HttpServletRequest request) throws CMSItemNotFoundException {
        String productCode = decodeWithScheme(encodedProductCode, UTF_8);
        List<ProductOption> extraOptions = Collections.singletonList(ProductOption.URL);

        ProductData productData = productFacade.getProductForCodeAndOptions(productCode, extraOptions);
        updatePageTitle(productCode, model);

        populateProductDetailForDisplay(productCode, model, request, extraOptions, Collections.emptyList());

        if (!model.containsAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT)) {
            return REDIRECT_PREFIX + productDataUrlResolver.resolve(productData);
        }

        return VanmarckeblueaddonControllerConstants.Views.Pages.Product.ORDER_FORM;
    }

    @GetMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/zoomImages")
    public String showZoomImages(@PathVariable("productCode") String encodedProductCode,
                                 @RequestParam(value = "galleryPosition", required = false) String galleryPosition, Model model) {
        String productCode = decodeWithScheme(encodedProductCode, UTF_8);
        ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
                Collections.singleton(ProductOption.GALLERY));
        List<Map<String, ImageData>> images = getGalleryImages(productData);
        populateProductData(productData, model);
        if (galleryPosition != null) {
            try {
                model.addAttribute("zoomImageUrl", images.get(Integer.parseInt(galleryPosition)).get("zoom").getUrl());
            } catch (IndexOutOfBoundsException | NumberFormatException ex) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(ex);
                }
                model.addAttribute("zoomImageUrl", "");
            }
        }
        return VanmarckeblueaddonControllerConstants.Views.Fragments.Product.ZOOM_IMAGES_POPUP;
    }

    @GetMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/quickView")
    public String showQuickView(@PathVariable("productCode") String encodedProductCode, Model model,
                                HttpServletRequest request) {
        String productCode = decodeWithScheme(encodedProductCode, UTF_8);
        ProductModel productModel = productService.getProductForCode(productCode);
        ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
                Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
                        ProductOption.CATEGORIES, ProductOption.VARIANT_FULL));

        sortVariantOptionData(productData);
        populateProductData(productData, model);
        getRequestContextData(request).setProduct(productModel);

        return VanmarckeblueaddonControllerConstants.Views.Fragments.Product.QUICK_VIEW_POPUP;
    }

    @GetMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/futureStock")
    public String productFutureStock(@PathVariable("productCode") String encodedProductCode, Model model, HttpServletRequest request) throws CMSItemNotFoundException {
        String productCode = decodeWithScheme(encodedProductCode, UTF_8);
        boolean futureStockEnabled = Config.getBoolean(FUTURE_STOCK_ENABLED, false);
        if (futureStockEnabled) {
            List<FutureStockData> futureStockList = futureStockFacade.getFutureAvailability(productCode);
            if (futureStockList == null) {
                GlobalMessages.addErrorMessage(model, STOCK_SERVICE_UNAVAILABLE);
            } else if (futureStockList.isEmpty()) {
                GlobalMessages.addInfoMessage(model, "product.product.details.future.nostock");
            }

            populateProductDetailForDisplay(productCode, model, request, Collections.emptyList(), Collections.emptyList());
            model.addAttribute("futureStocks", futureStockList);

            return VanmarckeblueaddonControllerConstants.Views.Fragments.Product.FUTURE_STOCK_POPUP;
        } else {
            return VanmarckeblueaddonControllerConstants.Views.Pages.Error.ERROR_NOT_FOUND_PAGE;
        }
    }

    @PostMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/grid/skusFutureStock", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Map<String, Object> productSkusFutureStock(FutureStockForm form) {
        String productCode = form.getProductCode();
        List<String> skus = form.getSkus();
        boolean futureStockEnabled = Config.getBoolean(FUTURE_STOCK_ENABLED, false);

        Map<String, Object> result = new HashMap<>();
        if (futureStockEnabled && isNotEmpty(skus) && StringUtils.isNotBlank(productCode)) {
            Map<String, List<FutureStockData>> futureStockData = futureStockFacade
                    .getFutureAvailabilityForSelectedVariants(productCode, skus);

            if (futureStockData == null) {
                // future availability service is down, we show this to the user
                result = Maps.newHashMap();
                String errorMessage = getMessageSource().getMessage(NOT_MULTISKU_ITEM_ERROR, null,
                        getI18nService().getCurrentLocale());
                result.put(NOT_MULTISKU_ITEM_ERROR, errorMessage);
            } else {
                for (Entry<String, List<FutureStockData>> entry : futureStockData.entrySet()) {
                    result.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return result;
    }

    @ExceptionHandler(UnknownIdentifierException.class)
    public String handleUnknownIdentifierException(UnknownIdentifierException exception, HttpServletRequest request) {
        request.setAttribute("message", exception.getMessage());
        return FORWARD_PREFIX + "/404";
    }

    protected void updatePageTitle(String productCode, Model model) {
        storeContentPageTitleInModel(model, getPageTitleResolver().resolveProductPageTitle(productCode));
    }

    protected ProductData populateProductDetailForDisplay(String productCode, Model model, HttpServletRequest request,
                                                          List<ProductOption> extraOptions, List<ProductData> solrVariants) throws CMSItemNotFoundException {
        ProductModel productModel = productService.getProductForCode(productCode);

        getRequestContextData(request).setProduct(productModel);

        List<ProductOption> options = new ArrayList<>(Arrays.asList(ProductOption.BASIC,
                ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
                ProductOption.CATEGORIES, ProductOption.CLASSIFICATION,
                ProductOption.DOCUMENTS, ProductOption.BRAND, ProductOption.PROMOTIONS,
                ProductOption.REFERENCE_CONSISTS_OF, ProductOption.SUPPLIER_REFERENCE, ProductOption.BARCODES));

        if (extraOptions != null) {
            options.addAll(extraOptions);
        }

        ProductData productData = productFacade.getProductForCodeAndOptions(productCode, options);

        if (!CollectionUtils.isEmpty(solrVariants)) {
            vmkProductFacade.enrichProductDataWithSolrVariants(productData, solrVariants, false);
        }

        sortVariantOptionData(productData);
        storeCmsPageInModel(model, getPageForProduct(productCode));

        populateProductData(productData, model);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, productBreadcrumbBuilder.getBreadcrumbs(productCode));

        model.addAttribute("distinctiveFeaturesHeaders", getDefiningFeatureHeaders(productData));

        if (isNotEmpty(productData.getVariantMatrix())) {
            model.addAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT, isNotEmpty(productData.getVariantMatrix()));
        }

        return productData;
    }

    private Map<String, String> getDefiningFeatureHeaders(ProductData productData) {
        Map<String, FeatureData> definingAttributes = null;

        if (isNotEmpty(productData.getVariantOptions())) {
            definingAttributes = productData.getVariantOptions().get(0).getDefiningAttributes();
        } else if (isNotEmpty(productData.getBaseOptions()) && isNotEmpty(productData.getBaseOptions().get(0).getOptions())) {
            definingAttributes = productData.getBaseOptions().get(0).getOptions().get(0).getDefiningAttributes();
        }

        if (isNotEmpty(definingAttributes)) {
            // keep order of defining features from backend!
            Map<String, String> definingFeatureHeaders = new LinkedHashMap<>();
            definingAttributes.forEach((key, value) -> definingFeatureHeaders.put(key, value.getName()));
            return definingFeatureHeaders;
        } else {
            return emptyMap();
        }
    }

    protected void populateProductData(ProductData productData, Model model) {
        model.addAttribute("galleryImages", getGalleryImages(productData));
        model.addAttribute("product", productData);
        if (productData.getConfigurable()) {
            List<ConfigurationInfoData> configurations = productFacade.getConfiguratorSettingsForCode(productData.getCode());
            if (isNotEmpty(configurations)) {
                model.addAttribute("configuratorType", configurations.get(0).getConfiguratorType());
            }
        }
    }

    protected void sortVariantOptionData(ProductData productData) {
        if (isNotEmpty(productData.getBaseOptions())) {
            for (BaseOptionData baseOptionData : productData.getBaseOptions()) {
                if (isNotEmpty(baseOptionData.getOptions())) {
                    baseOptionData.getOptions().sort(Comparator.comparing(VariantOptionData::getCode));
                }
            }
        }

        if (isNotEmpty(productData.getVariantOptions())) {
            productData.getVariantOptions().sort(Comparator.comparing(VariantOptionData::getCode));
        }
    }

    protected List<Map<String, ImageData>> getGalleryImages(ProductData productData) {
        List<Map<String, ImageData>> galleryImages = new ArrayList<>();
        if (isNotEmpty(productData.getImages())) {
            List<ImageData> images = new ArrayList<>();
            for (ImageData image : productData.getImages()) {
                if (ImageDataType.GALLERY.equals(image.getImageType())) {
                    images.add(image);
                }
            }
            images.sort(Comparator.comparing(ImageData::getGalleryIndex));

            if (isNotEmpty(images)) {
                addFormatsToGalleryImages(galleryImages, images);
            }
        }
        return galleryImages;
    }

    protected void addFormatsToGalleryImages(List<Map<String, ImageData>> galleryImages, List<ImageData> images) {
        int currentIndex = images.get(0).getGalleryIndex();
        Map<String, ImageData> formats = new HashMap<>();
        for (ImageData image : images) {
            if (currentIndex != image.getGalleryIndex()) {
                galleryImages.add(formats);
                formats = new HashMap<>();
                currentIndex = image.getGalleryIndex();
            }
            formats.put(image.getFormat(), image);
        }
        if (!formats.isEmpty()) {
            galleryImages.add(formats);
        }
    }

    protected AbstractPageModel getPageForProduct(String productCode) throws CMSItemNotFoundException {
        ProductModel productModel = productService.getProductForCode(productCode);
        return cmsPageService.getPageForProduct(productModel);
    }

    protected void cleanupBreadcrumbsUrls(ProductSearchPageData<SearchStateData, ProductData> searchPageData, String newRoot) {
        searchPageData.getBreadcrumbs().forEach(breadcrumbData -> {
            String queryParams = StringUtils.substringAfter(breadcrumbData.getRemoveQuery().getUrl(), "?");
            breadcrumbData.getRemoveQuery().setUrl(newRoot + '?' + queryParams);
        });
    }

    protected void setRemoveFreeTextUrl(ProductSearchPageData<SearchStateData, ProductData> searchPageData, String newRoot, String queryString) {
        if (StringUtils.isNotBlank(searchPageData.getFreeTextSearch())) {
            if (queryString != null && queryString.contains(searchPageData.getFreeTextSearch())) {
                queryString = StringUtils.remove(queryString, searchPageData.getFreeTextSearch());
                try {
                    searchPageData.setFreeTextSearchRemoveUrl(newRoot + "?q=" + URLEncoder.encode(queryString, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    LOG.error("Unsupported encoding (UTF-8). Fallback to html escaping.", e);
                    searchPageData.setFreeTextSearchRemoveUrl(newRoot + "?q=" + StringEscapeUtils.escapeHtml(queryString));
                }
            } else {
                searchPageData.setFreeTextSearchRemoveUrl(newRoot);
            }
        }
    }

    /**
     * @return the hreflangHelper
     */
    public HreflangHelper getHreflangHelper() {
        return hreflangHelper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PageTitleResolver getPageTitleResolver() {
        return pageTitleResolver;
    }

    /**
     * @return the siteBaseUrlResolutionService
     */
    public SiteBaseUrlResolutionService getSiteBaseUrlResolutionService() {
        return siteBaseUrlResolutionService;
    }

    /**
     * @return the productStructuredDataService
     */
    public VMKStructuredDataService<ProductData> getProductStructuredDataService() {
        return productStructuredDataService;
    }
}
