<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<%@ attribute name="name" required="true" type="java.lang.String" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:if test="${not empty address}">
    <c:if test="${ not empty name }">
        ${fn:escapeXml(name)}
    </c:if>
    <br/>
    <c:if test="${ not empty address.line1 }">
        ${fn:escapeXml(address.line1)}&nbsp;
    </c:if>
        <c:if test="${ not empty address.line2 }">
            ${fn:escapeXml(address.line2)}
        </c:if>
    <c:if test="${ not empty address.apartment }">&nbsp;-&nbsp;${fn:escapeXml(address.apartment)}
    </c:if>
        <br/>
        <c:if test="${ not empty address.postalCode }">
            ${fn:escapeXml(address.postalCode)}&nbsp;
        </c:if>
        <c:if test="${not empty address.town }">
            ${fn:escapeXml(address.town)}&nbsp;
        </c:if>
        <c:if test="${ not empty address.country.isocode }">
            (${fn:escapeXml(address.country.isocode)})
        </c:if>
        <br/>
        <c:if test="${ not empty address.phone }">
            ${fn:escapeXml(address.phone)}
        </c:if>
        <br/>
        <c:if test="${ not empty address.mobile }">
            ${fn:escapeXml(address.mobile)}
        </c:if>
</c:if>
