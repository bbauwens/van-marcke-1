<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/address" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/my-account/address-book" var="addressBookUrl" htmlEscape="false"/>
<c:choose>
    <c:when test="${edit eq true }">
        <c:set var="headline"><spring:theme code="text.account.addressBook.updateAddress"/></c:set>
    </c:when>
    <c:otherwise>
        <c:set var="headline"><spring:theme code="text.account.addressBook.addAddress"/></c:set>
    </c:otherwise>
</c:choose>
<div class="container vmb-addressbook-edit">
    <div class="back-link">
        <button type="button" class="addressBackBtn" data-back-to-addresses="${addressBookUrl}">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </button>
        <span class="label">${headline}</span>
    </div>
    <div class="row">
        <div class="vmb-addressbook-edit__form col-xs-12">
            <address:myAccountAddressForm addressBook="true" cancelUrl="/my-account/address-book"/>
        </div>
    </div>
</div>
