<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="tab-details">
    <ycommerce:testId code="productDetails_content_label">
        <p>
                ${ycommerce:sanitizeHTML(product.description)}
        </p>
        <c:if test="${not empty product.supplierReference}">
            <p>
                <spring:message code="product.details.tab.supplier.ref"/>
                &nbsp; ${ycommerce:sanitizeHTML(product.supplierReference)}
            </p>
        </c:if>
    </ycommerce:testId>
</div>