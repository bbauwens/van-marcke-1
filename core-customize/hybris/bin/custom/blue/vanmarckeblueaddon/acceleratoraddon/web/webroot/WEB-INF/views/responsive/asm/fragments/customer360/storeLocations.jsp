<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/addons/assistedservicestorefront/store" %>

<store:storeListForm/>

<script>
    ASM.storefinder.autoLoad("${fragmentData}");
    load(0);
    function load(i){
        if (ASM.storefinder.coords.latitude && ASM.storefinder.coords.longitude) {
            ASM.storefinder.getInitStoreData(null, ASM.storefinder.coords.latitude, ASM.storefinder.coords.longitude);
            window.dispatchEvent(new Event("resize"));
            return;
        }
        if (i < 10) {
            setTimeout(function() {load(i++)}, 200);
        }
    }
</script>