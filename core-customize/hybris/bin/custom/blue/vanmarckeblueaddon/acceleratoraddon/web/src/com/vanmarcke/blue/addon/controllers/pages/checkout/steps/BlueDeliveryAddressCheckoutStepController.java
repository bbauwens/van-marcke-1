package com.vanmarcke.blue.addon.controllers.pages.checkout.steps;

import com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants;
import com.vanmarcke.blue.addon.form.BlueAddressForm;
import com.vanmarcke.facades.principal.b2bunit.VMKB2BUnitFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.DeliveryAddressData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_DELIVERY_ADDRESS_PAGE;

@Controller
@RequestMapping(value = "/checkout/multi/delivery-address")
public class BlueDeliveryAddressCheckoutStepController extends AbstractCheckoutStepController {

    private static final String DELIVERY_METHOD = "delivery-method";
    private static final String SHOW_SAVE_TO_ADDRESS_BOOK_ATTR = "showSaveToAddressBook";

    @Resource(name = "addressDataUtil")
    private AddressDataUtil addressDataUtil;

    @Resource(name = "b2bUnitFacade")
    private VMKB2BUnitFacade b2bUnitFacade;

    @Resource(name = "blueReverseAddressConverter")
    private Converter<BlueAddressForm, AddressData> addressReverseConverter;

    @Override
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException, CommerceCartModificationException {
        //This will no longer be used since this step was merged into the delivery-method step
        return null;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @RequireHardLogIn
    public String add(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
                      final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        final CartData cartData = getCheckoutFacade().getCheckoutCart();
        populateCommonModelAttributes(model, cartData, addressForm);

        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
            return ADD_EDIT_DELIVERY_ADDRESS_PAGE;
        }

        final AddressData newAddress = this.addressDataUtil.convertToAddressData(addressForm);

        processAddressVisibilityAndDefault(addressForm, newAddress);

        // Verify the address data.
        final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade().verifyAddressData(newAddress);
        final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
                model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
                "checkout.multi.address.updated");

        if (addressRequiresReview) {
            return ADD_EDIT_DELIVERY_ADDRESS_PAGE;
        }

        this.b2bUnitFacade.addDeliveryAddressToUnit(newAddress);

        final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
        // Set the new address as the selected checkout delivery address
        getCheckoutFacade().setDeliveryAddress(newAddress);
        if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook()) { // temporary address should be removed
            this.b2bUnitFacade.removeDeliveryAddress(previousSelectedAddress);
        }

        // Set the new address as the selected checkout delivery address
        getCheckoutFacade().setDeliveryAddress(newAddress);

        return getCheckoutStep().nextStep();
    }

    protected void processAddressVisibilityAndDefault(final AddressForm addressForm, final AddressData newAddress) {
        if (addressForm.getSaveInAddressBook() != null) {
            newAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
            if (addressForm.getSaveInAddressBook().booleanValue() && CollectionUtils.isEmpty(this.b2bUnitFacade.getB2BUnitAddressBook())) {
                newAddress.setDefaultAddress(true);
            }
        } else if (getCheckoutCustomerStrategy().isAnonymousCheckout()) {
            newAddress.setDefaultAddress(true);
            newAddress.setVisibleInAddressBook(true);
        }
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    @RequireHardLogIn
    public String editAddressForm(@RequestParam("editAddressCode") final String editAddressCode, final Model model,
                                  final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
        if (getCheckoutStep().checkIfValidationErrors(validationResults)) {
            return getCheckoutStep().onValidation(validationResults);
        }

        AddressData addressData = null;
        if (StringUtils.isNotEmpty(editAddressCode)) {
            addressData = getCheckoutFacade().getDeliveryAddressForCode(editAddressCode);
        }

        final AddressForm addressForm = new AddressForm();
        final boolean hasAddressData = addressData != null;
        if (hasAddressData) {
            this.addressDataUtil.convert(addressData, addressForm);
        }

        final CartData cartData = getCheckoutFacade().getCheckoutCart();
        populateCommonModelAttributes(model, cartData, addressForm);

        if (addressData != null) {
            model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
        }

        return ADD_EDIT_DELIVERY_ADDRESS_PAGE;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @RequireHardLogIn
    public String edit(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
                       final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        final CartData cartData = getCheckoutFacade().getCheckoutCart();
        populateCommonModelAttributes(model, cartData, addressForm);

        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
            return ADD_EDIT_DELIVERY_ADDRESS_PAGE;
        }

        final AddressData newAddress = this.addressDataUtil.convertToAddressData(addressForm);

        processAddressVisibility(addressForm, newAddress);

        newAddress.setDefaultAddress(CollectionUtils.isEmpty(this.b2bUnitFacade.getB2BUnitAddressBook())
                || this.b2bUnitFacade.getB2BUnitAddressBook().size() == 1 || Boolean.TRUE.equals(addressForm.getDefaultAddress()));

        // Verify the address data.
        final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
                .verifyAddressData(newAddress);
        final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
                model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
                "checkout.multi.address.updated");

        if (addressRequiresReview) {
            if (StringUtils.isNotEmpty(addressForm.getAddressId())) {
                final AddressData addressData = getCheckoutFacade().getDeliveryAddressForCode(addressForm.getAddressId());
                if (addressData != null) {
                    model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
                    model.addAttribute("edit", Boolean.TRUE);
                }
            }

            return ADD_EDIT_DELIVERY_ADDRESS_PAGE;
        }

        this.b2bUnitFacade.editDeliveryAddress(newAddress);
        getCheckoutFacade().setDeliveryModeIfAvailable();
        getCheckoutFacade().setDeliveryAddress(newAddress);

        return getCheckoutStep().nextStep();
    }

    protected void processAddressVisibility(final AddressForm addressForm, final AddressData newAddress) {

        if (addressForm.getSaveInAddressBook() == null) {
            newAddress.setVisibleInAddressBook(true);
        } else {
            newAddress.setVisibleInAddressBook(Boolean.TRUE.equals(addressForm.getSaveInAddressBook()));
        }
    }

    @RequestMapping(value = "/remove", method =
            {RequestMethod.GET, RequestMethod.POST})
    @RequireHardLogIn
    public String removeAddress(@RequestParam("addressCode") final String addressCode, final RedirectAttributes redirectModel,
                                final Model model) throws CMSItemNotFoundException {
        final AddressData addressData = new AddressData();
        addressData.setId(addressCode);
        this.b2bUnitFacade.removeDeliveryAddress(addressData);
        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
                "account.confirmation.address.removed");
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        model.addAttribute("addressForm", new AddressForm());

        return getCheckoutStep().currentStep();
    }

    /**
     * Creates an address for the given {@code addressForm}.
     *
     * @param addressForm the address information
     * @return the created address
     */
    @PostMapping(value = "/select", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequireHardLogIn
    public ResponseEntity<DeliveryAddressData> doSelectSuggestedAddress(BlueAddressForm addressForm) {
        AddressData selectedAddress = addressReverseConverter.convert(addressForm);

        if (Boolean.TRUE.equals(addressForm.getEditAddress())) {
            b2bUnitFacade.editDeliveryAddress(selectedAddress);
        } else {
            b2bUnitFacade.addDeliveryAddressToUnit(selectedAddress);
        }
        String currentDeliveryMode = getCheckoutFacade().getCheckoutCart().getDeliveryMode() != null ? getCheckoutFacade().getCheckoutCart().getDeliveryMode().getCode() : null;
        setDeliveryAddress(selectedAddress, true);
        DeliveryAddressData deliveryAddressData = getDeliveryAddressData(selectedAddress, currentDeliveryMode);

        return ResponseEntity.ok().body(deliveryAddressData);
    }

    /**
     * This method gets called when the "Use this Address" button is clicked. It sets the selected delivery address on the checkout facade - if it has changed, and reloads the page highlighting the
     * selected delivery address.
     *
     * @param selectedAddressCode - the id of the delivery address.
     * @return the selected address
     */
    @RequestMapping(value = "/select/{selectedAddressCode}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @RequireHardLogIn
    public ResponseEntity<DeliveryAddressData> doSelectDeliveryAddress(@PathVariable(value = "selectedAddressCode", required = true) String selectedAddressCode,
                                                                       RedirectAttributes redirectAttributes) {
        if (StringUtils.isNotBlank(selectedAddressCode)) {
            AddressData selectedAddressData = getCheckoutFacade().getDeliveryAddressForCode(selectedAddressCode);
            boolean hasSelectedAddressData = selectedAddressData != null;


            if (hasSelectedAddressData) {
                String currentDeliveryMode = getCheckoutFacade().getCheckoutCart().getDeliveryMode() != null ? getCheckoutFacade().getCheckoutCart().getDeliveryMode().getCode() : null;
                DeliveryAddressData deliveryAddressData = getDeliveryAddressData(selectedAddressData, currentDeliveryMode);
                deliveryAddressData.setIsCartRecalculated(true);
                return ResponseEntity.ok().body(deliveryAddressData);
            }
        }
        return ResponseEntity.notFound().build();
    }

    /**
     *  get new delivery address data including new delivery mode
     *
     * @param selectedAddressData the selected address data
     * @param currentDeliveryMode the current delivery mode
     * @return the new delivery address data, including cart recalculation information if needed
     */
    private DeliveryAddressData getDeliveryAddressData(AddressData selectedAddressData, String currentDeliveryMode) {
        DeliveryAddressData deliveryAddressData = new DeliveryAddressData();
        try {
            BeanUtils.copyProperties(deliveryAddressData, selectedAddressData);
        } catch (InvocationTargetException | IllegalAccessException e) {
            // do nothing
        }

        setDeliveryAddress(selectedAddressData, false);
        String newDeliveryMode = getCheckoutFacade().getCheckoutCart().getDeliveryMode() != null ? getCheckoutFacade().getCheckoutCart().getDeliveryMode().getCode() : null;
        deliveryAddressData.setZoneDeliveryMode(newDeliveryMode);
        if (currentDeliveryMode != null && !currentDeliveryMode.equals(newDeliveryMode)) {
            deliveryAddressData.setIsCartRecalculated(true);
        }
        return deliveryAddressData;
    }


    protected void setDeliveryAddress(final AddressData selectedAddressData, boolean isNewAddress) {
        final AddressData cartCheckoutDeliveryAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
        if (isNewAddress || isAddressIdChanged(cartCheckoutDeliveryAddress, selectedAddressData)) {
            getCheckoutFacade().setDeliveryAddress(selectedAddressData);
            if (cartCheckoutDeliveryAddress != null && !cartCheckoutDeliveryAddress.isVisibleInAddressBook()) { // temporary address should be removed
                this.b2bUnitFacade.removeDeliveryAddress(cartCheckoutDeliveryAddress);
            }
        }
    }

    @RequestMapping(value = "/back", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String back(final RedirectAttributes redirectAttributes) {
        return getCheckoutStep().previousStep();
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String next(final RedirectAttributes redirectAttributes) {
        return getCheckoutStep().nextStep();
    }

    protected String getBreadcrumbKey() {
        return "checkout.multi." + getCheckoutStep().getProgressBarId() + ".breadcrumb";
    }

    protected CheckoutStep getCheckoutStep() {
        return getCheckoutStep(DELIVERY_METHOD);
    }

    protected void populateCommonModelAttributes(final Model model, final CartData cartData, final AddressForm addressForm)
            throws CMSItemNotFoundException {
        model.addAttribute("cartData", cartData);
        model.addAttribute("addressForm", addressForm);
        model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
        model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
        model.addAttribute("addressFormEnabled", Boolean.TRUE);
        model.addAttribute("removeAddressEnabled", Boolean.TRUE);
        model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.TRUE);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(getBreadcrumbKey()));
        model.addAttribute("metaRobots", "noindex,nofollow");
        if (StringUtils.isNotBlank(addressForm.getCountryIso())) {
            model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
            model.addAttribute("country", addressForm.getCountryIso());
        }
        prepareDataForPage(model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setCheckoutStepLinksForModel(model, getCheckoutStep());
    }
}
