(function ($) {
    'use strict';

    if ($('.vmb-saved-cart-details').length > 0) {
        $(".savedCartBackBtn").click(function (event) {
            event.preventDefault();
            var ref = document.referrer;

            if (ref !== undefined) {
                var refSplit = ref.split('/');

                if (refSplit[refSplit.length - 1].startsWith('saved-carts')) {
                    window.location.href = document.referrer;
                    return;
                }
            }

            window.location.href = $(this).data('baseHref');
        });
        window.mediator.publish('updateStockIndication', '');
    }

})(jQuery);