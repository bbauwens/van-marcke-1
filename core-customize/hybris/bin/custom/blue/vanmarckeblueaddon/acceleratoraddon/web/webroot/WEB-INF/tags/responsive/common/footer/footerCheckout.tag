<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/common/footer" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="currentDate" class="java.util.Date"/>
<c:set var="inclBtw" value="false"/>

<footer class="vmb-footer vmb-footer--checkout">

    <div class="vmb-footer-checkout-top">
        <cms:pageSlot position="CheckoutFooter" var="feature">
            <cms:component component="${feature}" element="div" class="footer__nav--container col-xs-12"/>
        </cms:pageSlot>
    </div>

    <div class="footer__bottom">
        <div class="footer__copyright">
            <ul>
                <li>
                    <a href="<spring:message code="checkout.multi.footer.saleterms.link"/>" target="_blank"><spring:message code="checkout.multi.footer.saleterms.text"/></a>
                </li>
                <li>
                    <a href="<spring:message code="checkout.multi.footer.privacy.link"/>" target="_blank"><spring:message code="checkout.multi.footer.privacy.text"/></a>
                </li>
                <li>
                    <a href="<spring:message code="checkout.multi.footer.cookies.link"/>" target="_blank"><spring:message code="checkout.multi.footer.cookies.text"/></a>
                </li>
                <li>
                    <fmt:formatDate value="${currentDate}" pattern="yyyy" var="currentYear"/>
                    <spring:message code="checkout.multi.footer.copyright" arguments="${currentYear}"/>
                </li>
                <li>
                    <spring:message code="checkout.multi.footer.address"/>
                </li>
                <li>
                    <spring:message code="checkout.multi.footer.vat"/>
                </li>
            </ul>
        </div>
    </div>
</footer>

