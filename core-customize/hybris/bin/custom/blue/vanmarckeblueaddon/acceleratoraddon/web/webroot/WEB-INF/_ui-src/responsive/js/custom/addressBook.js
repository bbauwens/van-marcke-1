(function ($) {
    'use strict';

    if ($(".vmb-account-new-address").length > 0) {
        $(".vmb-account-new-address__submit").click(function () {
            cleanUpFormErrors("#addressForm");

            var addressForm = $("#addressForm");
            if (addressForm.valid()) {

                $(this)
                    .prop("disabled", true)
                    .addClass("btn--loading");

                var addressData = {
                    line1: addressForm.find("#address-line1").val(),
                    line2: addressForm.find("#address-line2").val(),
                    apartment: addressForm.find("#address-apartment").val(),
                    town: addressForm.find("#address-townCity").val(),
                    postalCode: addressForm.find("#address-postcode").val(),
                    country: {
                        isocode: addressForm.find("#address-country").val(),
                    }
                };

                VMB.addressValidation.validate(addressData).then(function (addressResult) {
                    var addressForm = $("#addressForm");
                    addressForm.find("#address-line1").val(addressResult.line1);
                    addressForm.find("#address-line2").val(addressResult.line2);
                    addressForm.find("#address-townCity").val(addressResult.town);
                    addressForm.find("#address-postcode").val(addressResult.postalCode);
                    addressForm.submit();
                })['catch'](function (error) {
                    if (error === VMB.addressValidation.CLOSED) {
                        $(".vmb-account-new-address__submit")
                            .prop("disabled", false)
                            .removeClass("btn--loading");
                        return true;
                    }

                    $("#addressForm").submit();
                });
            }
        });
    }
})(jQuery);
