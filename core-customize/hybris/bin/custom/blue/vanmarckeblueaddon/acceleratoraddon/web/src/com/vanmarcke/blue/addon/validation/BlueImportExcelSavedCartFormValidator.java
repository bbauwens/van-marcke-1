package com.vanmarcke.blue.addon.validation;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ImportCSVSavedCartForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

/**
 * This class implements the {@link Validator} interface. It validates excel files that are being imported.
 *
 * @author Tom van den Berg
 * @since 13-05-2020
 */
public class BlueImportExcelSavedCartFormValidator implements Validator {

    private static final String IMPORT_CSV_FILE_MAX_SIZE_BYTES_KEY = "import.csv.file.max.size.bytes";
    private static final String XLS_FILE_FIELD = "xlsFile";
    private static final String XLS_FILE_EXTENSION = ".xls";
    private static final String XLSX_FILE_EXTENSION = ".xlsx";
    private static final String APP_XLS_CONTENT_TYPE = "application/vnd.ms-excel";
    private static final String APP_XLSX_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    private final SiteConfigService siteConfigService;

    /**
     * Provides an instance of the {@code BlueImportExcelSavedCartFormValidator}.
     *
     * @param siteConfigService the site config service
     */
    public BlueImportExcelSavedCartFormValidator(SiteConfigService siteConfigService) {
        this.siteConfigService = siteConfigService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return ImportCSVSavedCartForm.class.equals(aClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(Object target, Errors errors) {
        if (target instanceof ImportCSVSavedCartForm) {
            ImportCSVSavedCartForm importCSVSavedCartForm = (ImportCSVSavedCartForm) target;
            MultipartFile xlsFile = importCSVSavedCartForm.getCsvFile();

            if (xlsFile == null || xlsFile.isEmpty()) {
                errors.rejectValue(XLS_FILE_FIELD, "import.csv.savedCart.fileRequired");
                return;
            }

            String fileContentType = xlsFile.getContentType();
            String fileName = xlsFile.getOriginalFilename();

            if (!hasCorrectContentType(fileContentType) || fileName == null || !hasCorrectExtension(fileName)) {
                errors.rejectValue(XLS_FILE_FIELD, "import.xls.savedCart.fileExcelRequired");
                return;
            }

            if (xlsFile.getSize() > getFileMaxSize()) {
                errors.rejectValue(XLS_FILE_FIELD, "import.csv.savedCart.fileMaxSizeExceeded");
            }
        }
    }

    /**
     * Retrieves the max file size for carts that are being imported.
     *
     * @return the maximum file size
     */
    protected long getFileMaxSize() {
        return siteConfigService.getLong(IMPORT_CSV_FILE_MAX_SIZE_BYTES_KEY, 0);
    }


    /**
     * Checks whether the imported file is an Excel file
     *
     * @param fileContentType the file content type
     * @return boolean
     */
    private boolean hasCorrectContentType(String fileContentType) {
        return APP_XLS_CONTENT_TYPE.equalsIgnoreCase(fileContentType) || APP_XLSX_CONTENT_TYPE.equalsIgnoreCase(fileContentType);
    }

    /**
     * Checks whether the imported file has the correct XLS or XLSX extension.
     *
     * @param fileName the file name
     * @return boolean
     */
    private boolean hasCorrectExtension(String fileName) {
        return fileName.toLowerCase().endsWith(XLS_FILE_EXTENSION) || fileName.toLowerCase().endsWith(XLSX_FILE_EXTENSION);
    }
}
