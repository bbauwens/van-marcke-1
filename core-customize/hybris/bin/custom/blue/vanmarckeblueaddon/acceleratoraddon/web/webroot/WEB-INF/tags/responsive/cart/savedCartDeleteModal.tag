<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="vmb-modal fade" id="vmbConfirmSavedCartDeleteModal" tabindex="-1" role="dialog" aria-labelledby="vmbConfirmSavedCartDeleteModalTitle">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="vmbConfirmSavedCartDeleteModalTitle"><spring:theme code='text.account.savedcart.delete.popuptitle'/></h4>
			</div>
			<div class="modal-body">
				<p>
					<spring:theme code="text.account.savedcart.delete.msg"/>
				</p>
				<p>
					<span><spring:theme code="text.account.savedcart.cart.name"/>:</span>&nbsp;<strong id="vmbConfirmSavedCartDeleteName"></strong>
				</p>
				<input type="hidden" value="" id="vmbConfirmSavedCartDeleteCode">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal"><spring:theme code="text.button.cancel"/></button>
				<button type="button" class="btn btn-default js-savedcart_delete_confirm">
					<spring:theme code="general.delete.button"/>
				</button>
			</div>
		</div>
	</div>
</div>
