package com.vanmarcke.blue.addon.controllers.pages;

import com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants;
import com.vanmarcke.facades.cart.VMKSaveCartFacade;
import com.vanmarcke.services.order.cart.conversion.VMKCsvExcelConversionService;
import de.hybris.platform.acceleratorfacades.csv.CsvFacade;
import de.hybris.platform.acceleratorfacades.ordergridform.OrderGridFormFacade;
import de.hybris.platform.acceleratorfacades.product.data.ReadOnlyOrderGridData;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.enums.ImportStatus;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RestoreSaveCartForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.SaveCartForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RestoreSaveCartFormValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.SaveCartFormValidator;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartParameterData;
import de.hybris.platform.commercefacades.order.data.CommerceSaveCartResultData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Controller for saved carts page
 */
@Controller
@RequestMapping("/my-account/saved-carts")
public class BlueAccountSavedCartsPageController extends AbstractSearchPageController {

    private static final String MY_ACCOUNT_SAVED_CARTS_URL = "/my-account/saved-carts";
    private static final String REDIRECT_TO_SAVED_CARTS_PAGE = REDIRECT_PREFIX + MY_ACCOUNT_SAVED_CARTS_URL;

    private static final String SAVED_CARTS_CMS_PAGE = "saved-carts";
    private static final String SAVED_CART_DETAILS_CMS_PAGE = "savedCartDetailsPage";

    private static final String SAVED_CART_CODE_PATH_VARIABLE_PATTERN = "{cartCode:.*}";

    private static final String REFRESH_UPLOADING_SAVED_CART = "refresh.uploading.saved.cart";
    private static final String REFRESH_UPLOADING_SAVED_CART_INTERVAL = "refresh.uploading.saved.cart.interval";

    private static final Logger LOG = Logger.getLogger(BlueAccountSavedCartsPageController.class);

    @Resource(name = "accountBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "saveCartFacade")
    private VMKSaveCartFacade saveCartFacade;

    @Resource(name = "orderGridFormFacade")
    private OrderGridFormFacade orderGridFormFacade;

    @Resource(name = "saveCartFormValidator")
    private SaveCartFormValidator saveCartFormValidator;

    @Resource(name = "cartFacade")
    private CartFacade cartFacade;

    @Resource(name = "restoreSaveCartFormValidator")
    private RestoreSaveCartFormValidator restoreSaveCartFormValidator;

    @Resource(name = "defaultVMKCsvFacade")
    private CsvFacade csvFacade;

    @Resource(name = "defaultExcelService")
    private VMKCsvExcelConversionService excelService;

    @RequestMapping(method = RequestMethod.GET)
    @RequireHardLogIn
    public String savedCarts(@RequestParam(value = "page", defaultValue = "0") final int page,
                             @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                             @RequestParam(value = "sort", required = false) final String sortCode, final Model model)
            throws CMSItemNotFoundException {
        // Handle paged search results
        final PageableData pageableData = createPageableData(page, 5, sortCode, showMode);
        final SearchPageData<CartData> searchPageData = this.saveCartFacade.getSavedCartsForCurrentUser(pageableData, null);
        populateModel(model, searchPageData, showMode);

        model.addAttribute("refreshSavedCart", getSiteConfigService().getBoolean(REFRESH_UPLOADING_SAVED_CART, false));
        model.addAttribute("refreshSavedCartInterval", getSiteConfigService().getLong(REFRESH_UPLOADING_SAVED_CART_INTERVAL, 0));

        storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_CARTS_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_CARTS_CMS_PAGE));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, this.accountBreadcrumbBuilder.getBreadcrumbs("text.account.savedCarts"));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/" + SAVED_CART_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    @RequireHardLogIn
    public String savedCart(@PathVariable("cartCode") final String cartCode, final Model model,
                            final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        try {
            final CommerceSaveCartParameterData parameter = new CommerceSaveCartParameterData();
            parameter.setCartId(cartCode);

            final CommerceSaveCartResultData resultData = this.saveCartFacade.getCartForCodeAndCurrentUser(parameter);
            final CartData cartData = resultData.getSavedCartData();
            if (ImportStatus.PROCESSING.equals(cartData.getImportStatus())) {
                return REDIRECT_TO_SAVED_CARTS_PAGE;
            }
            model.addAttribute("savedCartData", cartData);

            final SaveCartForm saveCartForm = new SaveCartForm();
            saveCartForm.setDescription(cartData.getDescription());
            saveCartForm.setName(cartData.getName());
            model.addAttribute("saveCartForm", saveCartForm);

            final List<Breadcrumb> breadcrumbs = this.accountBreadcrumbBuilder.getBreadcrumbs(null);
            breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_SAVED_CARTS_URL, getMessageSource().getMessage("text.account.savedCarts",
                    null, getI18nService().getCurrentLocale()), null));
            breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.savedCart.savedCartBreadcrumb",
                    new Object[]
                            {cartData.getName()}, "Saved Cart {0}", getI18nService().getCurrentLocale()), null));
            model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);

        } catch (final CommerceSaveCartException e) {
            LOG.warn("Attempted to load a saved cart that does not exist or is not visible", e);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found", null);
            return REDIRECT_TO_SAVED_CARTS_PAGE;
        }
        storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_CART_DETAILS_CMS_PAGE));
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
        model.addAttribute("pageType", PageType.SAVEDCART.name());
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_CART_DETAILS_CMS_PAGE));
        return getViewForPage(model);
    }

    @RequestMapping(value = "/uploadingCarts", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @RequireHardLogIn
    public List<CartData> getUploadingSavedCarts(@RequestParam("cartCodes") final List<String> cartCodes)
            throws CommerceSaveCartException {
        final List<CartData> result = new ArrayList<>();
        for (final String cartCode : cartCodes) {
            final CommerceSaveCartParameterData parameter = new CommerceSaveCartParameterData();
            parameter.setCartId(cartCode);

            final CommerceSaveCartResultData resultData = this.saveCartFacade.getCartForCodeAndCurrentUser(parameter);
            final CartData cartData = resultData.getSavedCartData();

            if (ImportStatus.COMPLETED.equals(cartData.getImportStatus())) {
                result.add(cartData);
            }
        }

        return result;
    }

    @RequestMapping(value = "/" + SAVED_CART_CODE_PATH_VARIABLE_PATTERN + "/getReadOnlyProductVariantMatrix", method = RequestMethod.GET)
    @RequireHardLogIn
    public String getProductVariantMatrixForResponsive(@PathVariable("cartCode") final String cartCode,
                                                       @RequestParam("productCode") final String productCode, final Model model, final RedirectAttributes redirectModel) {
        try {
            final CommerceSaveCartParameterData parameter = new CommerceSaveCartParameterData();
            parameter.setCartId(cartCode);

            final CommerceSaveCartResultData resultData = this.saveCartFacade.getCartForCodeAndCurrentUser(parameter);
            final CartData cartData = resultData.getSavedCartData();

            final Map<String, ReadOnlyOrderGridData> readOnlyMultiDMap = this.orderGridFormFacade.getReadOnlyOrderGridForProductInOrder(
                    productCode, Arrays.asList(ProductOption.BASIC, ProductOption.CATEGORIES), cartData);
            model.addAttribute("readOnlyMultiDMap", readOnlyMultiDMap);

            return VanmarckeblueaddonControllerConstants.Views.Fragments.Checkout.READ_ONLY_EXPANDED_ORDER_FORM;
        } catch (final CommerceSaveCartException e) {
            LOG.warn("Attempted to load a saved cart that does not exist or is not visible", e);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found", null);
            return REDIRECT_TO_SAVED_CARTS_PAGE + "/" + cartCode;
        }
    }

    @RequestMapping(value = "/" + SAVED_CART_CODE_PATH_VARIABLE_PATTERN + "/edit", method = RequestMethod.POST)
    @RequireHardLogIn
    public String savedCartEdit(@PathVariable("cartCode") final String cartCode, final SaveCartForm form,
                                final BindingResult bindingResult, final RedirectAttributes redirectModel) throws CommerceSaveCartException {
        this.saveCartFormValidator.validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            for (final ObjectError error : bindingResult.getAllErrors()) {
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, error.getCode());
            }
            redirectModel.addFlashAttribute("saveCartForm", form);
        } else {
            final CommerceSaveCartParameterData commerceSaveCartParameterData = new CommerceSaveCartParameterData();
            commerceSaveCartParameterData.setCartId(cartCode);
            commerceSaveCartParameterData.setName(form.getName());
            commerceSaveCartParameterData.setDescription(form.getDescription());
            commerceSaveCartParameterData.setEnableHooks(false);
            try {
                final CommerceSaveCartResultData saveCartData = this.saveCartFacade.saveCart(commerceSaveCartParameterData);
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
                        "text.account.saveCart.edit.success", new Object[]
                                {saveCartData.getSavedCartData().getName()});
            } catch (final CommerceSaveCartException csce) {
                LOG.error(csce.getMessage(), csce);
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
                        "text.account.saveCart.edit.error", new Object[]
                                {form.getName()});
            }
        }
        return REDIRECT_TO_SAVED_CARTS_PAGE + "/" + cartCode;
    }

    @RequestMapping(value = "/{cartId}/restore", method = RequestMethod.GET)
    @RequireHardLogIn
    public String restoreSaveCartForId(@PathVariable(value = "cartId") final String cartId, final Model model)
            throws CommerceSaveCartException {
        CommerceSaveCartParameterData parameters = new CommerceSaveCartParameterData();
        parameters.setCartId(cartId);

        CommerceSaveCartResultData commerceSaveCartResultData = this.saveCartFacade.getCartSummary(parameters);

        boolean hasSessionCart = this.cartFacade.hasEntries();
        model.addAttribute("hasSessionCart", hasSessionCart);
        if (hasSessionCart) {
            model.addAttribute("autoGeneratedName", System.currentTimeMillis());
        }
        model.addAttribute(commerceSaveCartResultData);
        return VanmarckeblueaddonControllerConstants.Views.Fragments.Account.SAVED_CART_RESTORE_POPUP;
    }

    @RequireHardLogIn
    @RequestMapping(value = "/{cartId}/restore", method = RequestMethod.POST)
    public @ResponseBody
    String postRestoreSaveCartForId(@PathVariable(value = "cartId") final String cartId,
                                    final RestoreSaveCartForm restoreSaveCartForm, final BindingResult bindingResult) throws CommerceSaveCartException {
        try {
            this.restoreSaveCartFormValidator.validate(restoreSaveCartForm, bindingResult);
            if (bindingResult.hasErrors()) {
                return getMessageSource().getMessage(bindingResult.getFieldError().getCode(), null,
                        getI18nService().getCurrentLocale());
            }

            if (restoreSaveCartForm.getCartName() != null && !restoreSaveCartForm.isPreventSaveActiveCart()
                    && this.cartFacade.hasEntries()) {
                final CommerceSaveCartParameterData commerceSaveActiveCart = new CommerceSaveCartParameterData();
                commerceSaveActiveCart.setCartId(this.cartFacade.getSessionCart().getCode());
                commerceSaveActiveCart.setName(restoreSaveCartForm.getCartName());
                commerceSaveActiveCart.setEnableHooks(true);
                this.saveCartFacade.saveCart(commerceSaveActiveCart);
            }

            final CommerceSaveCartParameterData commerceSaveCartParameterData = new CommerceSaveCartParameterData();
            commerceSaveCartParameterData.setCartId(cartId);
            commerceSaveCartParameterData.setEnableHooks(true);
            if (restoreSaveCartForm.isKeepRestoredCart()) {
                this.saveCartFacade.cloneSavedCart(commerceSaveCartParameterData);
            }
            this.saveCartFacade.restoreSavedCart(commerceSaveCartParameterData);
            getSessionService().setAttribute(WebConstants.CART_RESTORATION, null);
        } catch (final CommerceSaveCartException ex) {
            LOG.error("Error while restoring the cart for cartId " + cartId + " because of " + ex);
            return getMessageSource().getMessage("text.restore.savedcart.error", null, getI18nService().getCurrentLocale());
        }
        return String.valueOf(HttpStatus.OK);
    }

    @RequestMapping(value = "/{cartId}/delete", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    @RequireHardLogIn
    public @ResponseBody
    String deleteSaveCartForId(@PathVariable(value = "cartId") final String cartId)
            throws CommerceSaveCartException {
        try {
            final CommerceSaveCartParameterData parameters = new CommerceSaveCartParameterData();
            parameters.setCartId(cartId);
            this.saveCartFacade.flagForDeletion(cartId);
        } catch (final CommerceSaveCartException ex) {
            LOG.error("Error while deleting the saved cart with cartId " + cartId + " because of " + ex);
            return getMessageSource().getMessage("text.delete.savedcart.error", null, getI18nService().getCurrentLocale());
        }
        return String.valueOf(HttpStatus.OK);
    }

    @RequestMapping(value = "/{cartId}/technicalsheets", method = RequestMethod.GET)
    @RequireHardLogIn
    public void downloadTechnicalDatasheets(@PathVariable final String cartId, final HttpServletResponse response) throws IOException {
        final InputStream is = this.saveCartFacade.exportTechnicalDataSheetsForSavedCartId(cartId);
        if (is != null) {
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            response.setHeader("Content-Disposition", "attachment; filename=" + cartId + ".pdf");
            try {
                IOUtils.copy(is, response.getOutputStream());
                response.flushBuffer();
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
    }

    @RequestMapping(value = "/{cartId}/export", method = RequestMethod.GET)
    public ResponseEntity<ByteArrayResource> downloadExcel(@PathVariable("cartId") String cartId) {
        try (StringWriter csvWriter = new StringWriter()) {
            try {
                List<String> cartHeaders = new ArrayList<>();
                cartHeaders.add(getMessageSource().getMessage("basket.export.cart.item.sku", null, getI18nService().getCurrentLocale()));
                cartHeaders.add(getMessageSource().getMessage("basket.export.cart.item.quantity", null, getI18nService().getCurrentLocale()));
                cartHeaders.add(getMessageSource().getMessage("basket.export.cart.item.name", null, getI18nService().getCurrentLocale()));
                cartHeaders.add(getMessageSource().getMessage("basket.export.cart.item.price", null, getI18nService().getCurrentLocale()));
                cartHeaders.add(getMessageSource().getMessage("basket.export.cart.item.total-price", null, getI18nService().getCurrentLocale()));

                CommerceSaveCartParameterData parameters = new CommerceSaveCartParameterData();
                parameters.setCartId(cartId);
                CommerceSaveCartResultData commerceSaveCartResultData = saveCartFacade.getCartForCodeAndCurrentUser(parameters);

                HttpHeaders httpHeader = new HttpHeaders();
                httpHeader.setContentType(new MediaType("application", "force-download"));
                httpHeader.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=cart.xls");

                if (commerceSaveCartResultData.getSavedCartData() != null) {
                    CartData cartData = commerceSaveCartResultData.getSavedCartData();
                    csvFacade.generateCsvFromCart(cartHeaders, true, cartData, csvWriter);

                    byte[] xlsOutput = excelService.convertCsvToXls(new StringReader(csvWriter.toString()));

                    return new ResponseEntity<>(new ByteArrayResource(xlsOutput), httpHeader, HttpStatus.CREATED);
                }
            } catch (IOException | CommerceSaveCartException e) {
                LOG.error(e.getMessage(), e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (IOException ioException) {
            LOG.error(ioException.getMessage());
        }
        return null;
    }

}
