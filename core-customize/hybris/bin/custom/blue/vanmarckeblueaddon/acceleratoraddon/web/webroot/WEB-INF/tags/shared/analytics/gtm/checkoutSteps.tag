<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="step" required="true" type="java.lang.Integer" %>
<%@ attribute name="option" required="false" type="java.lang.String" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    dataLayer.push({
        'event': 'checkout',
        ecommerce:
        <json:object>
            <json:property name="currencyCode" value="${currentCurrency.isocode}"/>
            <json:object name="checkout">
                <json:object name="actionField">
                    <json:property name="step" value="${step}"/>
                    <c:if test="${not empty option}">
                        <json:property name="option" value="${option}"/>
                    </c:if>
                </json:object>
                <json:array name="products" items="${cartData.entries}" var="orderEntry">
                    <json:object>
                        <json:property name="name" value="${orderEntry.product.analyticsData.name}"/>
                        <json:property name='id'>${orderEntry.product.code}</json:property>
                        <json:property name="quantity" value="${orderEntry.quantity}"/>
                        <json:property name="price" value="${orderEntry.basePrice.value}"/>
                        <json:property name="category" value="${orderEntry.product.analyticsData.category}"/>
                        <json:property name="brand" value="${orderEntry.product.analyticsData.brand}"/>
                    </json:object>
                </json:array>
            </json:object>
        </json:object>
    });
</script>