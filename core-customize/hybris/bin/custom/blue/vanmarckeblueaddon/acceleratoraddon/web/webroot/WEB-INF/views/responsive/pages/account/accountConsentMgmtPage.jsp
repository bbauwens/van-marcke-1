<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="consent-page">
    <div class="account-section-header">
        <spring:theme code="text.account.consent.consentManagement"/>
    </div>

    <div class="consent-overview">
        <c:forEach items="${consentParagraphs}" var="paragraph">
            <c:set var="hideSwitch" value=""/>
            <c:set var="toggleState" value=""/>
            <c:set var="toggleClass" value=""/>

            <c:if test="${paragraph.editable eq false}">
                <c:set var="toggleState" value=" checked disabled"/>
                <c:set var="toggleClass" value=" consent-overview__item__checkbox-disabled"/>
            </c:if>

            <c:if test="${empty paragraph.consentType}">
                <c:set var="hideSwitch" value=" hidden"/>
            </c:if>

            <c:if test="${paragraph.editable eq true and not empty paragraph.consentType}">
                <c:forEach items="${consents}" var="consent">
                    <c:if test="${paragraph.consentType eq consent.consentType and consent.optType eq 'OPT_IN'}">
                        <c:set var="toggleState" value="checked"/>
                    </c:if>
                </c:forEach>
            </c:if>

            <div class="row consent-overview__item">
                <input type="checkbox" class="consent-overview__item__checkbox ${toggleClass} ${hideSwitch}"
                       id="consent-checkbox-${paragraph.consentType}" aria-label="checkboxLabel" ${toggleState}/>
                <label for="consent-checkbox-${paragraph.consentType}">${paragraph.title}</label>
                <div class="consent-overview__item__content">${paragraph.content}</div>
            </div>
        </c:forEach>

        <div class="consent-overview__button">
            <button id="save-consents-button" type="button" class="btn btn-primary btn-lg btn-block">
                <spring:theme code="consent.button.accept.settings"/>
            </button>
        </div>
    </div>
</div>