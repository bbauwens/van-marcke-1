package com.vanmarcke.blue.addon.validation;

import com.vanmarcke.blue.addon.form.AddToWishListForm;
import org.springframework.validation.Errors;

/**
 * The {@link VMKAddToWishListFormValidator} class is used to validate {@link AddToWishListForm} instances.
 *
 * @author Niels Raemaekers
 * @since 20-05-2021
 */
public class VMKAddToWishListFormValidator extends VMKAbstractWishListFormValidator {

    private static final String PATTERN_NAME = "^[ A-Za-z0-9\\x{00C0}-\\x{00ff}_-]*$";
    private static final String FIELD_NAME = "newWishlistName";
    private static final String PATTERN_DESCRIPTION = "^[ A-Za-z0-9\\x{00C0}-\\x{00ff}?!_\\n@:;()*%€\\/\\<\\\\>,.\"”'@&+=-]*$";
    private static final String FIELD_DESCRIPTION = "newWishlistDescription";

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return AddToWishListForm.class.equals(aClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(Object object, Errors errors) {
        AddToWishListForm wishListForm = (AddToWishListForm) object;

        validateString(errors, wishListForm.getNewWishlistName(), FIELD_NAME, PATTERN_NAME);
        validateString(errors, wishListForm.getNewWishlistDescription(), FIELD_DESCRIPTION, PATTERN_DESCRIPTION);
    }
}
