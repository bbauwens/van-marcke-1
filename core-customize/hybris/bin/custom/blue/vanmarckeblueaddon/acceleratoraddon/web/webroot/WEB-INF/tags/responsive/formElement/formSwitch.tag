<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="checked" required="true" type="java.lang.Boolean" %>
<%@ attribute name="idKey" required="true" type="java.lang.String" %>
<%@ attribute name="labelKey" required="false" type="java.lang.String" %>
<%@ attribute name="labelArg" required="false" type="java.lang.String" %>
<%@ attribute name="itemValue" required="false" type="java.lang.String" %>
<%@ attribute name="containerCss" required="false" type="java.lang.String" %>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="vmb-switch__container ${fn:escapeXml(containerCss)}">
    <c:if test="${not empty labelKey}">
        <label class="vmb-switch__label" for="${idKey}"><spring:theme code="${labelKey}" arguments="${labelArg}"/> <c:if test="${mandatory}"> <span class="text-danger">*</span> </c:if></label>
    </c:if>
    <div class="vmb-switch">
        <input type="checkbox" id="${idKey}" class="vmb-switch__checkbox" value="${itemValue}" <c:if test="${checked}"> checked </c:if> <c:if test="${mandatory}"> required </c:if> <c:if test="${disabled}"> disabled </c:if> >
        <div class="vmb-switch__knobs" data-yes="<spring:theme code="text.switch.yes"/>" data-no="<spring:theme code="text.switch.no"/>">
            <span></span>
        </div>
        <div class="vmb-switch__layer"></div>
    </div>
</div>