<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/cart" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:url value="/my-account/saved-carts/" var="savedCartsLink" htmlEscape="false"/>
<c:set var="searchUrl" value="/my-account/saved-carts?sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>

<div class="vmb-saved-cart-overview">
    <h1><spring:theme code="text.account.savedCarts"/></h1>

    <c:if test="${empty searchPageData.results}">
        <div class="account-section-content content-empty">
            <ycommerce:testId code="savedCarts_noOrders_label">
                <spring:theme code="text.account.savedCarts.noSavedCarts"/>
            </ycommerce:testId>
        </div>
    </c:if>

    <c:if test="${not empty searchPageData.results}">
        <div class="account-section-content">

            <nav:pagination top="true" msgKey="text.account.savedCarts.page" showCurrentPageInfo="true"
                            hideRefineButton="true"
                            supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
                            searchPageData="${searchPageData}" searchUrl="${searchUrl}"
                            numberPagesShown="${numberPagesShown}"/>

            <div class="account-overview-table saved__carts__overview--table">
                <c:set var="cartIdRowMapping" value=''/>
                <table class="table vmb-saved-cart-overview__table">
                    <thead>
                    <tr>
                        <th><spring:theme code="text.account.savedCart.name"/></th>
                        <th><spring:theme code="text.account.savedCart.dateSaved"/></th>
                        <th><spring:theme code="text.account.savedCart.description"/></th>
                        <th><spring:theme code="text.account.savedCart.qty"/></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${searchPageData.results}" var="savedCart" varStatus="loop">
                        <c:choose>
                            <c:when test="${savedCart.importStatus eq 'PROCESSING' }">
                                <c:set var="importCartIsProcessing" value="true"/>
                                <c:set var="cartIdRowMapping"
                                       value="${cartIdRowMapping}${fn:escapeXml(savedCart.code)}:${loop.index},"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="importCartIsProcessing" value="false"/>
                            </c:otherwise>
                        </c:choose>
                        <tr id="row-${loop.index}">
                            <td class="saved-cart-name"
                                data-label="<spring:theme code='text.account.savedCart.name' javaScriptEscape="true" htmlEscape="true"/>:">
                                <ycommerce:testId code="savedCarts_name_link">
                                    <a href="${savedCartsLink}${ycommerce:encodeUrl(savedCart.code)}"
                                       class="responsive-table-link js-saved-cart-name ${importCartIsProcessing ? 'not-active' : '' }">
                                            ${fn:escapeXml(savedCart.name)}
                                    </a>
                                </ycommerce:testId>
                            </td>
                            <td class="responsive-table-cell"
                                data-label="<spring:theme code='text.account.savedCart.dateSaved' javaScriptEscape="true" htmlEscape="true" />:">
                                    <span class="js-saved-cart-date ${importCartIsProcessing ? 'hidden' : '' }">
                                        <ycommerce:testId code="savedCarts_created_label">
                                            <fmt:formatDate value="${savedCart.saveTime}" dateStyle="medium"
                                                            timeStyle="short"
                                                            type="both"/>
                                        </ycommerce:testId>
                                    </span>
                            </td>
                            <td class="responsive-table-cell saved-cart-description"
                                data-label="<spring:theme code='text.account.savedCart.description' javaScriptEscape="true" htmlEscape="true"/>:">
                                <ycommerce:testId code="savedCarts_description_label">
                                        <span class="js-saved-cart-description">
                                            <c:choose>
                                                <c:when test="${importCartIsProcessing}">
                                                    <span class="file-importing js-file-importing">
                                                        <img src="${commonResourcePath}/images/3dots.gif" width="25"
                                                             height="25"/>
                                                    </span>
                                                </c:when>
                                                <c:otherwise>
                                                    ${fn:escapeXml(savedCart.description)}
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                                </ycommerce:testId>
                            </td>
                            <td data-label="<spring:theme code='text.account.savedCart.qty' javaScriptEscape="true" htmlEscape="true"/>:">
                                <ycommerce:testId code="savedCarts_noOfItems_label">
                                        <span class="js-saved-cart-number-of-items">
                                            <c:if test="${importCartIsProcessing eq false}">
                                                ${savedCart.totalUnitCount}
                                            </c:if>
                                        </span>
                                </ycommerce:testId>
                            </td>
                            <spring:url value="/my-account/saved-carts/${savedCart.code}/export" var="exportUrl"
                                        htmlEscape="false"/>
                            <td class="restore-item-column">
                                <ycommerce:testId code="savedCarts_restore_link">
                                    <a href="#"
                                       class="js-restore-saved-cart restore-item-link ${importCartIsProcessing || savedCart.totalUnitCount < 1 ? 'hidden' : '' }"
                                       data-savedcart-id="${fn:escapeXml(savedCart.code)}">
                                        <span><spring:theme code='text.account.savedCart.restore'/></span>
                                    </a>
                                </ycommerce:testId>
                                &nbsp;
                                <ycommerce:testId code="savedCarts_export_link">
                                    <a href="${exportUrl}"
                                       class="js-export-saved-cart restore-item-link ${importCartIsProcessing || savedCart.totalUnitCount < 1 ? 'hidden' : '' }">
                                        <span><spring:theme code="basket.export.csv.file"/></span>
                                    </a>
                                </ycommerce:testId>

                            </td>
                            <td class="remove-item-column">
                                <ycommerce:testId code="savedCarts_delete_link">
                                    <a href="#"
                                       class="js-delete-saved-cart remove-item-link ${importCartIsProcessing ? 'hidden' : '' }"
                                       data-savedcart-id="${fn:escapeXml(savedCart.code)}"
                                       data-savedcart-name="${fn:escapeXml(savedCart.name)}">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </ycommerce:testId>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="js-uploading-saved-carts-update" data-id-row-mapping="${cartIdRowMapping}"
                     data-refresh-cart="${refreshSavedCart}"
                     data-refresh-interval="${refreshSavedCartInterval}"></div>
            </div>

            <nav:pagination top="false" msgKey="text.account.savedCarts.page" showCurrentPageInfo="true"
                            hideRefineButton="true"
                            supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
                            searchPageData="${searchPageData}" searchUrl="${searchUrl}"
                            numberPagesShown="${numberPagesShown}"/>
        </div>

        <cart:savedCartDeleteModal/>
    </c:if>

    <div class="text-right">
        <a class="btn btn-default" href="<c:url value="/import/csv/saved-cart"/>"><span
                class="glyphicon glyphicon-import"></span>&nbsp;<spring:theme code="import.csv.savedCart.title"/></a>
    </div>
</div>