<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/nav" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="event" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/event" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<h1 class="vmb-plp-title">${categoryName}</h1>

<input type="hidden" id="search-query-input" value="${searchPageData.currentQuery.url}"/>
<c:choose>
    <c:when test="${viewType eq 'List'}">
        <nav:pagination top="true" supportShowPaged="false" supportShowAll="true" searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}" numberPagesShown="${numberPagesShown}"/>

        <c:forEach items="${searchPageData.results}" var="product" varStatus="status">
            <product:productListerListViewItem product="${product}"
                                               list="${categoryName}"
                                               categoryCode="${categoryCode}"
                                               position="${status.index + 1}"
                                               plvVariantsCount="${plvVariantsCount}"/>
        </c:forEach>
        <event:updateStock/>
        <div class="col-xs-12 col-sm-12 col-md-9 vmb-product-variant-addtocart" id="sticky-buttons">
                <div class="product-action-buttons">
                    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                        <button type="button" class="btn btn-primary js-add-to-wishlist">
                            <spring:theme code="text.saveForLater"/>
                        </button>
                    </sec:authorize>
                    <button type="button" class="btn btn-default vmb-add-to-cart" onclick="addToCartMulti()">
                        <span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;<spring:theme code="text.addToCart"/>
                    </button>
                </div>
                <div class="scroll-to-top-wrap">
                    <div class="scroll-to-top-button" onclick="ACC.plp.onScrollToTopClick()">
                        <span class="glyphicon glyphicon-chevron-up"></span>
                    </div>
                </div>
        </div>

        <common:globalMessagesTemplates/>
    </c:when>
    <c:when test="${viewType eq 'Grid'  || viewType == null}">
        <nav:pagination top="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}" numberPagesShown="${numberPagesShown}"/>
        <div class="vmb-grid vmb-product-container">
            <c:forEach items="${searchPageData.results}" var="product" varStatus="status">
                <product:productListerGridItem product="${product}"
                                               list="${categoryName}"
                                               categoryCode="${categoryCode}"
                                               position="${status.index + 1}" />
            </c:forEach>
        </div>
        <nav:pagination top="false"  supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"  searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}"  numberPagesShown="${numberPagesShown}"/>
    </c:when>
</c:choose>



</div>


