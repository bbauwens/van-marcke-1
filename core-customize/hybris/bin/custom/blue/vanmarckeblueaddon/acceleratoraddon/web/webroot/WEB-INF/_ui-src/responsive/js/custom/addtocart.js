function addToCartMulti() {

    var orderProducts = [];

    $.each($(".vmb-product-quantity"), function () {

        if (!(_.isEmpty($(this).val())) && parseInt($(this).val()) > 0) {
            orderProducts.push({
                'productCode': "" + $(this).data('code'),
                'quantity': parseInt($(this).val())
            });
        }

    });

    if (orderProducts.length > 0) {
        showLoader();

        $.ajax({
            method: "POST",
            contentType: "application/json",
            url: VMB.urls.addToCartMulti,
            data: JSON.stringify(orderProducts)
        }).done(function (response) {
            if (!_.isEmpty(response) && _.isArray(response)) {

                //clear inputs
                $(".vmb-product-quantity").each(function () {
                    $(this).val('');
                    //reset old value back to 1
                    this.oldValue = 1;
                });

                //render cart items
                var modalBodyHtml = $.templates("#vmbAddToCartItemTemplate").render(setProductImage(response));

                $("#vmbAddToCartModal .modal-body").html(modalBodyHtml);

                hideLoader();
                $("#vmbAddToCartModal").modal();

                //Update cart button
                if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
                    ACC.minicart.updateMiniCartDisplay();
                }

                //GTM Add to cart
                var products = [];
                for (var i = 0; i < response.length; i++) {
                    products.push(
                        {
                            'id': response[i].entry.product.code,
                            'name': response[i].entry.product.analyticsData.name,
                            'price': response[i].entry.basePrice.value,
                            'category': response[i].entry.product.analyticsData.category,
                            'brand': response[i].entry.product.analyticsData.brand,
                            'quantity': response[i].quantityAdded,
                        }
                    );
                }

                ACC.track.trackAddToCart(products);

            } else {
                $("#vmbAddToCartModal .modal-body").html($.templates("#vmbAddToCartErrorTemplate").render());
                hideLoader();
                $("#vmbAddToCartModal").modal();
            }
        }).fail(function (response) {
            $("#vmbAddToCartModal .modal-body").html($.templates("#vmbAddToCartErrorTemplate").render());
            hideLoader();
            $("#vmbAddToCartModal").modal();
        });
    }
}

// Helper Function to set the product image on the object
function setProductImage(data) {

    for (var i = 0; i < data.length; i++) {
        data[i].productImage = "";

        if (data[i].entry.hasOwnProperty("product") && data[i].entry.product.hasOwnProperty("images") && _.isArray(data[i].entry.product.images)) {
            var prodImages = _.filter(data[i].entry.product.images, {'format': 'product'});
            if (_.isArray(prodImages) && prodImages.length > 0 && prodImages[0].hasOwnProperty("url") && !_.isEmpty(prodImages[0].url)) {
                data[i].productImage = prodImages[0].url;
            }
        }
    }

    return data;
}


(function ($) {
    'use strict';

    $(".vmb-product-quantity:not(:disabled)").inputFilter(function (value) {
        return /^\d*$/.test(value);
    });

})(jQuery);
