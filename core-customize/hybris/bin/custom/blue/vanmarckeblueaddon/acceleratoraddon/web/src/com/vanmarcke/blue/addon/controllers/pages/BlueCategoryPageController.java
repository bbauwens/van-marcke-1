package com.vanmarcke.blue.addon.controllers.pages;

import com.vanmarcke.blue.addon.constants.VanmarckeblueaddonWebConstants.ProductListViewType;
import com.vanmarcke.blue.addon.controllers.pages.common.AbstractBlueCategoryPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

/**
 * Controller for a category page
 */
@Controller
@RequestMapping(value = "/**/c")
public class BlueCategoryPageController extends AbstractBlueCategoryPageController {

    @GetMapping(value = "/{categoryCode}/products", produces = MediaType.APPLICATION_JSON_VALUE)
    protected String getProducts(@PathVariable("categoryCode") final String categoryCode, // NOSONAR
                                 @RequestParam(value = "q", required = false) final String searchQuery,
                                 @RequestParam(value = "page", defaultValue = "0") final int page,
                                 @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                 @RequestParam(value = "sort", required = false) final String sortCode,
                                 final Model model) {
        return performSearchAndGetAjaxResults(categoryCode, searchQuery, page, showMode, sortCode, model);
    }


    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String category(@PathVariable("categoryCode") final String categoryCode, // NOSONAR
                           @RequestParam(value = "q", required = false) final String searchQuery,
                           @RequestParam(value = "page", defaultValue = "0") final int page,
                           @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                           @RequestParam(value = "sort", required = false) final String sortCode,
                           @RequestParam(value = "viewType", required = false) final ProductListViewType viewType, final Model model,
                           final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException {
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_FOLLOW);
        model.addAttribute("hreflanglinks", getCategoryHreflangLinks(categoryCode));
        return performSearchAndGetResultsPage(categoryCode, searchQuery, page, showMode, sortCode, viewType, model, request, response);
    }

    @ResponseBody
    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/facets", method = RequestMethod.GET)
    public FacetRefinement<SearchStateData> getFacets(@PathVariable("categoryCode") final String categoryCode,
                                                      @RequestParam(value = "q", required = false) final String searchQuery,
                                                      @RequestParam(value = "page", defaultValue = "0") final int page,
                                                      @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                                      @RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException {
        return performSearchAndGetFacets(categoryCode, searchQuery, page, showMode, sortCode);
    }

    @ResponseBody
    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/results", method = RequestMethod.GET)
    public SearchResultsData<ProductData> getResults(@PathVariable("categoryCode") final String categoryCode,
                                                     @RequestParam(value = "q", required = false) final String searchQuery,
                                                     @RequestParam(value = "page", defaultValue = "0") final int page,
                                                     @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                                     @RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException {
        return performSearchAndGetResultsData(categoryCode, searchQuery, page, showMode, sortCode);
    }
}
