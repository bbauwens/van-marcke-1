package com.vanmarcke.blue.addon.controllers.cms;

import com.vanmarcke.core.model.ClientServiceParagraphComponentModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Actions.Cms.CLIENT_SERVICE_PARAGRAPH_COMPONENT;

/**
 * ClientServiceParagraphComponentController
 *
 * @author Niels Raemaekers
 * @since 02-06-2020
 */
@Controller("ClientServiceParagraphComponentController")
@RequestMapping(value = CLIENT_SERVICE_PARAGRAPH_COMPONENT)
public class BlueClientServiceParagraphComponentController extends AbstractBlueCMSComponentController<ClientServiceParagraphComponentModel> {

    @Override
    protected void fillModel(HttpServletRequest request, Model model, ClientServiceParagraphComponentModel component) {
        model.addAttribute("componentTitle", component.getTitle());
        model.addAttribute("componentContent", component.getContent());
        model.addAttribute("componentTelephone", component.getTelephone());
        model.addAttribute("componentTelephoneFrontend", component.getTelephoneFrontend());
        model.addAttribute("componentEmail", component.getEmail());
        model.addAttribute("screenTakeoverUrl", component.getScreenTakeoverUrl());
    }
}
