package com.vanmarcke.blue.addon.controllers.pages.checkout.steps;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.blue.addon.form.BlueAddressForm;
import com.vanmarcke.core.helper.CheckoutDateHelper;
import com.vanmarcke.facades.forms.validation.DeliveryMethodFormValidator;
import com.vanmarcke.facades.order.VMKCheckoutFacade;
import com.vanmarcke.facades.order.validation.VMKOrderValidator;
import com.vanmarcke.facades.principal.b2bunit.VMKB2BUnitFacade;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

import static com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_METHOD_PAGE;
import static com.vanmarcke.core.constants.VanmarckeCoreConstants.REQUEST_SEPARATE_INVOICE;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

@Controller
@RequestMapping(value = "/checkout/multi/delivery-method")
public class BlueDeliveryMethodCheckoutStepController extends AbstractCheckoutStepController {

    private static final String DELIVERY_METHOD = "delivery-method";
    private static final String SHOW_SAVE_TO_ADDRESS_BOOK_ATTR = "showSaveToAddressBook";

    private static final String REDIRECT_CART_URL = REDIRECT_PREFIX + "/cart";

    @Resource
    private LocalizedMessageFacade messageFacade;

    @Resource
    private DeliveryMethodFormValidator deliveryMethodFormValidator;

    @Resource
    private VMKStoreSessionFacade storeSessionFacade;

    @Resource
    private VMKOrderValidator orderValidator;

    @Resource(name = "vmkCheckoutFacade")
    private VMKCheckoutFacade checkoutFacade;

    @Resource(name = "vmkB2BUnitFacade")
    private VMKB2BUnitFacade b2BUnitFacade;

    @Resource
    private CheckoutDateHelper checkoutDateHelper;

    @RequestMapping(value = "/choose", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    @PreValidateCheckoutStep(checkoutStep = DELIVERY_METHOD)
    public String enterStep(Model model, RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        String errorMessage = orderValidator.checkCartForErrors();
        if (isNotEmpty(errorMessage)) {
            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, errorMessage);
            return REDIRECT_CART_URL;
        }

        CartData cartData = checkoutFacade.getCheckoutCart();

        populateCommonModelAttributes(model, cartData);

        return CHOOSE_DELIVERY_METHOD_PAGE;
    }

    @RequestMapping(value = "/back", method = RequestMethod.GET)
    @RequireHardLogIn
    @Override
    public String back(final RedirectAttributes redirectAttributes) {
        return getCheckoutStep().previousStep();
    }

    @RequestMapping(value = "/next", method = {RequestMethod.GET, RequestMethod.POST})
    @RequireHardLogIn
    public String next(RedirectAttributes redirectModel, DeliveryMethodForm deliveryMethodForm, BindingResult bindingResult) {
        deliveryMethodFormValidator.validate(deliveryMethodForm, bindingResult);
        getSessionService().setAttribute(REQUEST_SEPARATE_INVOICE, deliveryMethodForm.getRequestSeparateInvoice());

        if (bindingResult.hasErrors()) {
            String errorMessage = messageFacade.getMessageForCodeAndCurrentLocale(bindingResult.getAllErrors().get(0).getCode());
            return redirectToCurrentStepWithError(redirectModel, errorMessage);
        }

        checkoutFacade.saveDeliveryInformation(deliveryMethodForm);
        return getCheckoutStep().nextStep();
    }

    protected CheckoutStep getCheckoutStep() {
        return getCheckoutStep(DELIVERY_METHOD);
    }

    protected String getBreadcrumbKey() {
        return "checkout.multi." + getCheckoutStep().getProgressBarId() + ".breadcrumb";
    }

    protected void populateCommonModelAttributes(final Model model, final CartData cartData)
            throws CMSItemNotFoundException {
        model.addAttribute("cartData", cartData);
        model.addAttribute("addressForm", createAddressForm());
        model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
        model.addAttribute("noAddress", getCheckoutFlowFacade().hasNoDeliveryAddress());
        model.addAttribute("addressFormEnabled", Boolean.TRUE);
        model.addAttribute("removeAddressEnabled", Boolean.TRUE);
        model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.TRUE);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(getBreadcrumbKey()));
        model.addAttribute("metaRobots", "noindex,nofollow");
        model.addAttribute("country", storeSessionFacade.getCurrentSite().getCountry().getIsocode());

        model.addAttribute("availableDeliveryModes", checkoutFacade.getAvailableDeliveryMethods());
        model.addAttribute("deliveryModeMapping", checkoutFacade.getDeliveryMethodCodes());

        DeliveryMethodForm deliveryMethodForm = checkoutFacade.getDeliveryMethodForm();
        model.addAttribute("deliveryMethodForm", deliveryMethodForm);

        model.addAttribute("requiredYardReference", b2BUnitFacade.mustAddReference());
        model.addAttribute("changeSeparateInvoiceReference", b2BUnitFacade.mandatorySeparateInvoiceReference());
        model.addAttribute("lastPossibleDate", checkoutDateHelper.getCheckoutLastPossibleDateForCart(cartData));

        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryMethod.breadcrumb"));
        model.addAttribute("metaRobots", "noindex,nofollow");
        prepareDataForPage(model);
        storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
        setCheckoutStepLinksForModel(model, getCheckoutStep());
    }

    /**
     * Sends a redirect to the current checkout step with the given error {@code messageKey}.
     *
     * @param redirectModel the redirect model
     * @param messageKey    the message key
     * @return the redirect url
     */
    protected String redirectToCurrentStepWithError(final RedirectAttributes redirectModel, final String messageKey) {
        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, messageKey);
        return getCheckoutStep().currentStep();
    }

    /**
     * Creates a {@link BlueAddressForm} with the current country iso code.
     *
     * @return the address form
     */
    protected BlueAddressForm createAddressForm() {
        BlueAddressForm addressForm = new BlueAddressForm();
        CountryData currentCountry = storeSessionFacade.getCurrentSite().getCountry();
        addressForm.setCountryIso(currentCountry.getIsocode());
        return addressForm;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String next(RedirectAttributes redirectAttributes) {
        //not implemented
        throw new NotImplementedException();
    }
}
