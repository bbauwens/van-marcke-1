package com.vanmarcke.blue.addon.validation;

import com.vanmarcke.blue.addon.form.AddToWishListForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

/**
 * The {@link VMKAbstractWishListFormValidator} class is used to validate {@link AddToWishListForm} instances.
 *
 * @author Niels Raemaekers
 * @since 20-05-2021
 */
public class VMKAbstractWishListFormValidator implements Validator {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKAbstractWishListFormValidator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return Object.class.equals(aClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(Object object, Errors errors) {
        //do not use, use implementations instead
    }

    protected void validateString(Errors errors, String value, String field, String regexPattern) {
        final Pattern pattern = Pattern.compile(regexPattern);
        if (!pattern.matcher(value).matches()) {
            errors.rejectValue(field, "wishlist.validation-error.pattern-" + field);
            LOGGER.error("Error while validating the field '{}' due to not matching the regex pattern", field);
        }
    }
}
