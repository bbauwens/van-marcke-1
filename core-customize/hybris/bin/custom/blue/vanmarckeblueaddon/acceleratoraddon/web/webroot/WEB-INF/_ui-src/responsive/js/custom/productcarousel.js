(function($) {
    'use strict';

    if($(".vmb-product-carousel-component").length > 0){

        var pcSwipers = [];

        $('.vmb-product-carousel-component .swiper-container').each(function (index) {

            var swipeClass = "pcs" + index;
            $(this).addClass(swipeClass);

            var options = {
                watchOverflow: true,
                spaceBetween: 15,
                preloadImages: false,
                lazy: true,
                watchSlidesVisibility: true,

                pagination: {
                    el: "." + swipeClass + ' .swiper-pagination',
                    clickable: true
                },

                navigation: {
                    nextEl: $(this).closest('.vmb-product-carousel-component').find('.swiper-button-next')[0],
                        prevEl: $(this).closest('.vmb-product-carousel-component').find('.swiper-button-prev')[0]
                },

                'on': {
                    'init': function () {
                        $(this.el).closest('.vmb-product-carousel-component').find('.swiper-button-next').removeClass('hidden');
                        $(this.el).closest('.vmb-product-carousel-component').find('.swiper-button-prev').removeClass('hidden');
                    }
                }
            };

            var watchPc = false;

            if($(this).data("carousel") === "one"){
                options.slidesPerView = 1;
                options.slidesPerGroup = 1;
                options.centeredSlides = true;
            }

            if($(this).data("carousel") === "allVisible"){

                var slidesCount = Math.floor( $(this).innerWidth() / 270);

                options.slidesPerView = slidesCount;
                options.slidesPerGroup = slidesCount;
                watchPc = true;
            }

            var pcSwiper = new Swiper ("." + swipeClass, options);

            if(watchPc){
                pcSwipers.push(pcSwiper);
            }
        });

        $(window).onDelayed('resize', 50, function () {

            for (var i = 0; i < pcSwipers.length; i++){
                var pcs = pcSwipers[i];

                var slidesCount = Math.floor(pcs.size / 270);

                if(slidesCount !== pcs.params.slidesPerView){
                    pcs.params.slidesPerView =  slidesCount;
                    pcs.params.slidesPerGroup =  slidesCount;
                    pcs.update();
                }
            }

        });

    }

})(jQuery);