<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="list" required="false" type="java.lang.String" %>
<%@ attribute name="position" required="false" type="java.lang.Integer" %>
<%@ attribute name="categoryCode" required="false" type="java.lang.String" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="${product.url}" var="productUrl"/>
<c:set value="${not empty product.potentialPromotions}" var="hasPromotion"/>

<c:set var="productSearchQueryUrl" value="${productUrl}"/>

<c:choose>
    <c:when test="${not empty searchQuery && not empty searchText}">
        <c:set var="productSearchQueryUrl" value="${productUrl}?q=${ycommerce:encodeUrl(searchQuery)}&text=${ycommerce:encodeUrl(searchText)}"/>
    </c:when>
    <c:when test="${not empty searchQuery && empty searchText}">
        <c:set var="productSearchQueryUrl" value="${productUrl}?q=${ycommerce:encodeUrl(searchQuery)}"/>
    </c:when>
    <c:otherwise>
        <c:set var="productSearchQueryUrl" value="${productUrl}?text=${ycommerce:encodeUrl(searchText)}"/>
    </c:otherwise>
</c:choose>

<c:if test="${not empty categoryCode}">
    <c:set var="productSearchQueryUrl" value="${productSearchQueryUrl}&category=${ycommerce:encodeUrl(categoryCode)}"/>
</c:if>


<div class="vmb-product-item vmb-gtm-product-data"
     data-list="${list}"
     data-code="${fn:escapeXml(product.code)}"
     data-name="${fn:escapeXml(product.name)}"
     data-category="${fn:escapeXml(product.analyticsData.category)}"
     data-brand="${fn:escapeXml(product.analyticsData.brand)}"
     data-url="${productSearchQueryUrl}"
     data-position="${position}">


    <a class="vmb-product-item__thumb vmb-gtm-product-item" href="${productSearchQueryUrl}"
       title="${fn:escapeXml(product.name)}">
        <product:productPrimaryImage product="${product}" format="product"/>
    </a>
    <div class="vmb-product-item__content">
        <div class="vmb-product-item__details">
            <a class="vmb-product-item__name" href="${productSearchQueryUrl}">
                <c:out escapeXml="false" value="${ycommerce:sanitizeHTML(product.baseProductName)}"/>
            </a>
        </div>
        <a class="btn btn-default vmb-product-item__variants vmb-gtm-product-item" href="${productSearchQueryUrl}"><spring:theme
                code="productTile.numberOfVariants" arguments="${product.numberOfVariants}"/></a>

    </div>
</div>