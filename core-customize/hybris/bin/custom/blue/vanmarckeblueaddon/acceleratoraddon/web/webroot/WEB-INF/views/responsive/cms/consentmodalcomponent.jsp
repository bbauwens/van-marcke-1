<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="cookieModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 id="cookieModalLabel"><spring:theme code="consent.request.header.title"/></h2>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4">
                            <c:forEach items="${consentParagraphs}" var="paragraph">
                                <div class="row manage-consent-item" data-consenttype="${paragraph.consentType}">
                                        ${paragraph.title}
                                </div>
                            </c:forEach>
                        </div>

                        <c:forEach items="${consentParagraphs}" var="paragraph" varStatus="loop">
                            <c:set var="hideSwitch" value=""/>
                            <c:set var="toggleState" value=""/>
                            <c:set var="toggleClass" value=""/>

                            <c:if test="${paragraph.editable eq false}">
                                <c:set var="toggleState" value=" checked disabled"/>
                                <c:set var="toggleClass" value=" material-switch-disabled"/>
                            </c:if>

                            <c:if test="${empty paragraph.consentType}">
                                <c:set var="hideSwitch" value=" hidden"/>
                            </c:if>

                            <div class="col-sm-8 consent-content ${loop.index eq 0 ? "" : "hidden"}"
                                 id="consent-content-${paragraph.consentType}">
                                <div class="row">
                                    <div class="material-switch ${toggleClass} pull-right ${hideSwitch}"
                                         id="consent-content-toggle">
                                        <input id="consent-toggle-${paragraph.consentType}"
                                               type="checkbox" ${toggleState}/>
                                        <label for="consent-toggle-${paragraph.consentType}"
                                               class="label-primary"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <p class="consent-content-paragraph">${paragraph.content}</p>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="cookieModalConsentAll" type="button" class="btn btn-primary btn-lg btn-block"
                        data-dismiss="modal"><spring:theme code="consent.button.accept.all"/>
                </button>
                <button id="cookieModalConsentSettings" type="button" class="btn btn-primary btn-lg btn-block"
                        data-dismiss="modal"><spring:theme code="consent.button.accept.settings"/>
                </button>
            </div>
        </div>
    </div>
</div>