<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="index" required="false" type="java.lang.Integer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/grid" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/order" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%--
    Represents single cart item on cart page
 --%>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="errorStatus" value="<%= de.hybris.platform.catalog.enums.ProductInfoStatus.valueOf(\"ERROR\") %>"/>
<c:set var="entryNumberHtml" value="${fn:escapeXml(entry.entryNumber)}"/>
<c:set var="productCodeHtml" value="${fn:escapeXml(entry.product.code)}"/>
<c:set var="quantityHtml" value="${fn:escapeXml(entry.quantity)}"/>
<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>

<c:if test="${empty index}">
    <c:set property="index" value="${entryNumber}"/>
</c:if>

<input type="hidden" value="${cmsSite.channel == 'DIY'}" id="isDIY"/>

<c:if test="${not empty entry}">

    <c:set var="showEditableGridClass" value=""/>
    <c:url value="${entry.product.url}" var="productUrl"/>

    <%-- product image --%>
    <td class="vmb-cartitem__image">
        <a class="vmb-gtm-product-item" href="${fn:escapeXml(productUrl)}"><product:productPrimaryImage
                product="${entry.product}" format="thumbnail"/></a>
    </td>

    <%-- product name, code, promotions --%>
    <td class="vmb-cartitem__info">
        <a class="vmb-gtm-product-item" href="${fn:escapeXml(productUrl)}"><span
                class="name">${fn:escapeXml(entry.product.name)}</span></a>

        <div class="code"><spring:theme code="basket.page.entry.sku"/>: ${productCodeHtml}</div>

        <span class="text-info hidden" id="vmb-unknown-availability-message-${entry.product.code}"><spring:theme
                code="text.stock.outOfStock"/></span>

        <c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry)}">
            <c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
                <c:set var="displayed" value="false"/>
                <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                    <c:if test="${not displayed && ycommerce:isConsumedByEntry(consumedEntry,entry) && not empty promotion.description}">
                        <c:set var="displayed" value="true"/>

                        <div class="promo">
                            <ycommerce:testId code="cart_potentialPromotion_label">
                                ${ycommerce:sanitizeHTML(promotion.description)}
                            </ycommerce:testId>
                        </div>
                    </c:if>
                </c:forEach>
            </c:forEach>
        </c:if>
        <c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntryOrOrderEntryGroup(cartData, entry)}">
            <c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
                <c:set var="displayed" value="false"/>
                <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                    <c:if test="${not displayed && ycommerce:isConsumedByEntry(consumedEntry,entry) }">
                        <c:set var="displayed" value="true"/>
                        <div class="promo">
                            <ycommerce:testId code="cart_appliedPromotion_label">
                                ${ycommerce:sanitizeHTML(promotion.description)}
                            </ycommerce:testId>
                        </div>
                    </c:if>
                </c:forEach>
            </c:forEach>
        </c:if>
    </td>

    <c:set var="wishlistCheckIdx" value="wishlist_check_${entry.entryNumber}"/>

    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
        <td class="vmb-cartitem__wishlist" style="vertical-align: middle;" data-label="<spring:theme code="basket.page.qty"/>:">
            <input type="checkbox"
                    class="js-select-all-products-wish-list-item"
                    data-code="${entry.product.code}"
                    data-wishlist-target-class="${wishlistCheckIdx}">
        </td>
    </sec:authorize>

    <%-- quantity --%>
    <td class="vmb-cartitem__quantity" data-label="<spring:theme code="basket.page.qty"/>:">
        <c:url value="/cart/update" var="cartUpdateFormAction"/>
        <c:set var="cartDataJson"
               value='{"cartCode" : "${ycommerce:encodeJSON(cartData.code)}","productPostPrice":"${ycommerce:encodeJSON(entry.basePrice.value)}","productName":"${ycommerce:encodeJSON(entry.product.name)}"}'></c:set>
        <form:form id="updateCartForm${entry.entryNumber}" action="${cartUpdateFormAction}" method="post"
                   modelAttribute="updateQuantityForm${entry.entryNumber}"
                   class="js-qty-form${entry.entryNumber}"
                   data-cart="${fn:escapeXml(cartDataJson)}">
            <input type="hidden" name="entryNumber" value="${entryNumberHtml}"/>
            <input type="hidden" name="productCode" value="${productCodeHtml}"/>
            <input type="hidden" name="initialQuantity" value="${quantityHtml}"/>

            <form:label cssClass="visible-xs visible-sm" path="quantity" for="quantity${entry.entryNumber}"></form:label>
            <form:input cssClass="form-control js-update-entry-quantity-input vmb-product-quantity ${wishlistCheckIdx}" disabled="${not entry.updateable}" type="number" size="1" id="quantity_${entry.entryNumber}" path="quantity"
                    data-code="${entry.product.code}"
                    data-wishlist-selected='false'/>
        </form:form>
    </td>

    <%-- stock EDC --%>
    <td class="vmb-cartitem__stock-warehouse">
        <format:stock type="warehouse" stock="${entry.product.stock}" code="${entry.product.code}"/>
    </td>

    <c:if test="${cmsSite.channel != 'DIY'}">
        <%-- stock TEC --%>
        <td class="vmb-cartitem__stock-tec">
            <format:stock type="storeCount" stock="${entry.product.stock}" code="${entry.product.code}"/>
        </td>

        <%-- price --%>
        <td class="vmb-cartitem__price" data-label="<spring:theme code="basket.page.price" arguments="${currentCurrency.symbol}"/>:">
            <format:price priceData="${entry.basePrice}" displayFreeForZero="true"/>
        </td>
    </c:if>

    <c:if test="${!isAnonymousUser and (user.showNetPrice or cmsSite.channel == 'DIY')}">
        <%-- netto --%>
        <td class="vmb-cartitem__nettoprice vmb-netto-price-display"
            data-label="<spring:theme code="basket.page.price.netto" arguments="${currentCurrency.symbol}"/>:">
            <format:price priceData="${entry.netPriceData}" displayFreeForZero="true"/>
        </td>
    </c:if>

    <c:if test="${cmsSite.channel != 'DIY' or !isAnonymousUser}">
        <%-- total --%>
        <td class="vmb-cartitem__total js-item-total" data-label="<spring:theme code="basket.page.total"/>:">
            <format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/>
        </td>
    </c:if>
    <%-- first possible delivery date --%>
    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
        <td class="vmb-cartitem__deliveryDate">
            <c:choose>
                <c:when test="${not empty entry.firstPossibleDate}">
                    <fmt:formatDate value="${entry.firstPossibleDate}" pattern="dd-MM-yyyy"/>
                </c:when>
                <c:otherwise>
                    <spring:theme code="text.quote.description.not.applicable"/>
                </c:otherwise>
            </c:choose>
        </td>
    </sec:authorize>

    <%-- menu icon --%>
    <td class="vmb-cartitem__menu">
        <c:if test="${entry.updateable}">
            <form:form id="cartEntryActionForm" action="" method="post"/>
            <c:url value="/cart/entry/execute/REMOVE" var="entryActionUrl"/>
            <button class="btn btn-link js-execute-entry-action-button" id="actionEntry_${entryNumberHtml}"
                    data-entry-action-url="${fn:escapeXml(entryActionUrl)}"
                    data-entry-action="REMOVE"
                    data-entry-product-code="${productCodeHtml}"
                    data-entry-product-name="${entry.product.name}"
                    data-entry-product-price="${entry.basePrice.value}"
                    data-entry-product-category="${entry.product.analyticsData.category}"
                    data-entry-product-brand="${entry.product.analyticsData.brand}"
                    data-entry-initial-quantity="${quantityHtml}"
                    data-action-entry-numbers="${fn:escapeXml(entryNumberHtml)}">
                <span class="glyphicon glyphicon-remove"></span><span class="remove-text"><spring:theme code="basket.page.entry.action.REMOVE"/></span>
            </button>
        </c:if>
    </td>
</c:if>
