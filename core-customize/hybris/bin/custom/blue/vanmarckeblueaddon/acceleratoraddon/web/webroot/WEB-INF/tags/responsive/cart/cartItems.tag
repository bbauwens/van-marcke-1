<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/cart" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/shared/format" %>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/wishlist" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="errorStatus" value="<%= de.hybris.platform.catalog.enums.ProductInfoStatus.valueOf(\"ERROR\") %>"/>
<sec:authorize var="isAnonymousUser" access="hasAnyRole('ROLE_ANONYMOUS')"/>

<table class="table table-condensed vmb-cart-table">
    <thead>
    <tr>
        <th class="vmb-cartitem__image"></th>
        <th class="vmb-cartitem__info"><spring:theme code="basket.page.item"/></th>
        <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
            <th class="vmb-cartitem__wishlist js-add-to-wishlist-cart">
                <input type="checkbox" class="js-select-all-products-wish-list"><spring:theme code="basket.page.savedCart"/>
            </th>
        </sec:authorize>
        <th class="vmb-cartitem__quantity"><spring:theme code="basket.page.qty"/></th>
        <th class="vmb-cartitem__stock-warehouse" data-toggle="tooltip" data-placement="top"
            title="<spring:theme code="text.stockstatus.icon.warehouse" htmlEscape="false"/>"></th>
        <c:if test="${cmsSite.channel != 'DIY'}">
            <th class="vmb-cartitem__stock-tec"><format:tecIcon/><format:nostock/></th>
            <th class="vmb-cartitem__price"><spring:theme code="basket.page.price"
                                                          arguments="${currentCurrency.symbol}"/></th>
        </c:if>
        <c:if test="${!isAnonymousUser and (user.showNetPrice or cmsSite.channel == 'DIY')}">
            <th class="vmb-cartitem__nettoprice vmb-netto-price-display">
                <spring:theme code="basket.page.price.netto" arguments="${currentCurrency.symbol}"/></th>
        </c:if>
        <c:if test="${cmsSite.channel != 'DIY' or !isAnonymousUser}">
            <th class="vmb-cartitem__total">
                <spring:theme code="basket.page.total"/>
            </th>
        </c:if>
        <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
            <th class="vmb-cartitem__deliveryDate"><spring:theme code="basket.page.first.possible.date"/></th>
        </sec:authorize>
        <th class="vmb-cartitem__remove"></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${cartData.rootGroups}" var="group" varStatus="loop">
        <cart:rootEntryGroup cartData="${cartData}" entryGroup="${group}"/>
    </c:forEach>
    </tbody>
</table>

<div style="display: none">
    <span id="wishlist-no-products-selected-message">
        <spring:theme code="basket.page.savedCart.add.error"/>
    </span>
</div>

<product:productOrderFormJQueryTemplates/>
<storepickup:pickupStorePopup/>
<wishlist:wishListAddWarningModal/>
<common:globalMessagesTemplates/>
