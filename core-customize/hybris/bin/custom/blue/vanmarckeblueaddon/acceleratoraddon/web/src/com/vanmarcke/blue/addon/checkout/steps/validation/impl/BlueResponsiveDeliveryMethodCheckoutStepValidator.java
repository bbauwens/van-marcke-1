package com.vanmarcke.blue.addon.checkout.steps.validation.impl;

import com.vanmarcke.blue.addon.facades.BlueCheckoutFlowFacade;
import com.vanmarcke.facades.storesession.VMKStoreSessionFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.site.BaseSiteService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.addFlashMessage;

public class BlueResponsiveDeliveryMethodCheckoutStepValidator extends AbstractCheckoutStepValidator {

    private static final Logger LOGGER = Logger.getLogger(BlueResponsiveDeliveryMethodCheckoutStepValidator.class);

    private VMKStoreSessionFacade storeSessionFacade;
    private BaseSiteService baseSiteService;


    @Override
    public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes) {
        if (!((BlueCheckoutFlowFacade) getCheckoutFlowFacade()).hasValidCountry()) {
            addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "checkout.multi.country.invalid");
            return ValidationResults.REDIRECT_TO_CART;
        }

        if (!getCheckoutFlowFacade().hasValidCart()) {
            LOGGER.info("Missing, empty or unsupported cart");
            return ValidationResults.REDIRECT_TO_CART;
        }

        if (storeSessionFacade.getCurrentStore() == null && !baseSiteService.getCurrentBaseSite().getChannel().equals(SiteChannel.DIY)) {
            addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "checkout.multi.store.notprovided");
            return ValidationResults.REDIRECT_TO_CART;
        }

        if (getCheckoutFacade().hasShippingItems() && getCheckoutFlowFacade().hasNoDeliveryAddress()) {
            addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "checkout.multi.deliveryAddress.notprovided");
            return ValidationResults.REDIRECT_TO_CART;
        }

        return ValidationResults.SUCCESS;
    }

    @Required
    public void setStoreSessionFacade(final VMKStoreSessionFacade storeSessionFacade) {
        this.storeSessionFacade = storeSessionFacade;
    }

    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}