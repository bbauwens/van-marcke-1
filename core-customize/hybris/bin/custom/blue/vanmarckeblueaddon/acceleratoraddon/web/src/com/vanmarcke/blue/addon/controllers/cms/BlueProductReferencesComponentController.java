/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.vanmarcke.blue.addon.controllers.cms;

import com.vanmarcke.blue.addon.controllers.VanmarckeblueaddonControllerConstants;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static java.util.Collections.singletonList;


/**
 * Controller for CMS ProductReferencesComponent
 */
@Controller("ProductReferencesComponentController")
@RequestMapping(value = VanmarckeblueaddonControllerConstants.Actions.Cms.PRODUCT_REFERENCE_COMPONENT)
public class BlueProductReferencesComponentController extends AbstractBlueCMSComponentController<ProductReferencesComponentModel> {

    private static final String BASE_PRODUCT_PREFIX = "BP_";
    private static final List<ProductOption> PRODUCT_OPTIONS = singletonList(ProductOption.BASIC);

    @Resource(name = "productFacade")
    private ProductFacade productFacade;

    /**
     * {@inheritDoc}
     */
    @Override
    protected RequestContextData getRequestContextData(HttpServletRequest request) {
        // This method has been overridden for testing purposes
        return super.getRequestContextData(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fillModel(HttpServletRequest request, Model model, ProductReferencesComponentModel component) {

        ProductModel currentProduct = getRequestContextData(request).getProduct();

        if (currentProduct != null) {
            String currentProductCode = trimCurrentProductCode(currentProduct);

            List<ProductReferenceData> productReferences = productFacade.getProductReferencesForCode(currentProductCode,
                    component.getProductReferenceTypes(), PRODUCT_OPTIONS, component.getMaximumNumberProducts());

            model.addAttribute("title", component.getTitle());
            model.addAttribute("productReferences", productReferences);
        }
    }

    /**
     * If the product code of the given product has a base product prefix, it gets trimmed.
     *
     * @param currentProduct the product
     * @return the trimmed product code
     */
    private String trimCurrentProductCode(ProductModel currentProduct) {
        String productCode = currentProduct.getCode();
        if (productCode.startsWith(BASE_PRODUCT_PREFIX)) {
            productCode = productCode.substring(3);
        }
        return productCode;
    }
}