package com.vanmarcke.blue.addon.converter.populator;

import com.vanmarcke.blue.addon.form.BlueAddressForm;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@link BlueReverseAddressPopulatorTest} class contains the unit tests for the {@link BlueReverseAddressPopulator}
 * class.
 *
 * @author Christiaan Janssen
 * @since 22-09-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BlueReverseAddressPopulatorTest {

    private static final String ADDRESS_ID = RandomStringUtils.random(10);
    private static final String TITLE_CODE = RandomStringUtils.random(10);
    private static final String FIRST_NAME = RandomStringUtils.random(10);
    private static final String LAST_NAME = RandomStringUtils.random(10);
    private static final String LINE1 = RandomStringUtils.random(10);
    private static final String LINE2 = RandomStringUtils.random(10);
    private static final String APARTMENT = RandomStringUtils.random(10);
    private static final String TOWN = RandomStringUtils.random(10);
    private static final String POSTAL_CODE = RandomStringUtils.random(10);
    private static final String PHONE = RandomStringUtils.random(10);
    private static final String MOBILE = RandomStringUtils.random(10);
    private static final String COUNTRY_ISO = RandomStringUtils.random(10);
    private static final String REGION_ISO = RandomStringUtils.random(10);

    @Mock
    private I18NFacade i18NFacade;

    @InjectMocks
    private BlueReverseAddressPopulator addressPopulator;

    @Test
    public void testPopulate() {
        BlueAddressForm form = new BlueAddressForm();
        form.setAddressId(ADDRESS_ID);
        form.setTitleCode(TITLE_CODE);
        form.setFirstName(FIRST_NAME);
        form.setLastName(LAST_NAME);
        form.setLine1(LINE1);
        form.setLine2(LINE2);
        form.setApartment(APARTMENT);
        form.setTownCity(TOWN);
        form.setPostcode(POSTAL_CODE);
        form.setPhone(PHONE);
        form.setMobile(MOBILE);
        form.setCountryIso(COUNTRY_ISO);
        form.setRegionIso(REGION_ISO);
        form.setDefaultAddress(Boolean.TRUE);


        CountryData country = new CountryData();

        when(i18NFacade.getCountryForIsocode(COUNTRY_ISO)).thenReturn(country);

        RegionData region = new RegionData();

        when(i18NFacade.getRegion(COUNTRY_ISO, REGION_ISO)).thenReturn(region);

        AddressData address = new AddressData();

        addressPopulator.populate(form, address);

        Assertions
                .assertThat(address.getId())
                .isEqualTo(ADDRESS_ID);

        Assertions
                .assertThat(address.getTitleCode())
                .isEqualTo(TITLE_CODE);

        Assertions
                .assertThat(address.getFirstName())
                .isEqualTo(FIRST_NAME);

        Assertions
                .assertThat(address.getLastName())
                .isEqualTo(LAST_NAME);

        Assertions
                .assertThat(address.getLine1())
                .isEqualTo(LINE1);

        Assertions
                .assertThat(address.getLine2())
                .isEqualTo(LINE2);

        Assertions
                .assertThat(address.getApartment())
                .isEqualTo(APARTMENT);

        Assertions
                .assertThat(address.getTown())
                .isEqualTo(TOWN);

        Assertions
                .assertThat(address.getPostalCode())
                .isEqualTo(POSTAL_CODE);

        Assertions
                .assertThat(address.isBillingAddress())
                .isFalse();

        Assertions
                .assertThat(address.isShippingAddress())
                .isTrue();

        Assertions
                .assertThat(address.getPhone())
                .isEqualTo(PHONE);

        Assertions
                .assertThat(address.getMobile())
                .isEqualTo(MOBILE);

        Assertions
                .assertThat(address.getCountry())
                .isEqualTo(country);

        Assertions
                .assertThat(address.getRegion())
                .isEqualTo(region);

        Assertions
                .assertThat(address.isDefaultAddress())
                .isTrue();

        Assertions
                .assertThat(address.isVisibleInAddressBook())
                .isFalse();

        verify(i18NFacade).getCountryForIsocode(COUNTRY_ISO);
        verify(i18NFacade).getRegion(COUNTRY_ISO, REGION_ISO);
    }
}