package com.vanmarcke.services.order.cart.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import com.vanmarcke.services.util.VMKDateUtils;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKTecCollectReservationMessageServiceImplTest {

    private static final String TEC_COLLECT_MESSAGE_KEY = "checkout.tec.collect.message";
    private static final String TEC_COLLECT_MESSAGE = RandomStringUtils.randomAlphabetic(10);

    private static final LocalDate MONDAY_TODAY = LocalDate.of(2020, 10, 5);
    private static final LocalDate FRIDAY_TODAY = LocalDate.of(2020, 10, 9);
    private static final LocalDate SATURDAY_TODAY = LocalDate.of(2020, 10, 10);

    private static final Date MONDAY = VMKDateUtils.createDate(2020, 10, 5);
    private static final Date TUESDAY = VMKDateUtils.createDate(2020, 10, 6);
    private static final Date TUESDAY_NEXT = VMKDateUtils.createDate(2020, 10, 13);
    private static final Date WEDNESDAY = VMKDateUtils.createDate(2020, 10, 7);
    private static final Date FRIDAY = VMKDateUtils.createDate(2020, 10, 9);
    private static final Date SATURDAY = VMKDateUtils.createDate(2020, 10, 10);

    private CartModel cart;
    private PointOfServiceModel pos;

    @Mock
    private LocalizedMessageFacade messageFacade;
    @Mock
    private VMKOpeningScheduleService openingScheduleService;

    @Spy
    @InjectMocks
    private VMKTecCollectReservationMessageServiceImpl tecCollectReservationMessageService;

    @Before
    public void setUp() throws Exception {
        pos = PointOfServiceModelMockBuilder.aPointOfService().build();
        cart = CartModelMockBuilder.aCart().withDeliveryPointOfService(pos).build();
    }

    @Test
    public void testShouldShowTecCollectMessage_noPickupDate_NO() {
        assertThat(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, null, true)).isEmpty();
        verifyZeroInteractions(openingScheduleService, messageFacade);
    }

    @Test
    public void testShouldShowTecCollectMessage_tecCollectNotPossible_NO() {
        assertThat(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, MONDAY, false)).isEmpty();

        verifyZeroInteractions(openingScheduleService, messageFacade);
    }

    @Test
    public void testShouldShowTecCollectMessage_MondayAndTuesday_NO() {
        doReturn(MONDAY_TODAY).when(tecCollectReservationMessageService).getTecCollectDate();

        assertThat(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, TUESDAY, true)).isEmpty();

        verifyZeroInteractions(openingScheduleService, messageFacade);
    }

    @Test
    public void testShouldShowTecCollectMessage_FridayAndSaturday_saturdayClosed_NO() {
        doReturn(FRIDAY_TODAY).when(tecCollectReservationMessageService).getTecCollectDate();

        when(openingScheduleService.isStoreOpenOnSaturday(pos)).thenReturn(false);

        assertThat(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, SATURDAY, true)).isEmpty();

        verifyZeroInteractions(messageFacade);
        verify(openingScheduleService).isStoreOpenOnSaturday(pos);
    }

    @Test
    public void testShouldShowTecCollectMessage_FridayAndSaturday_saturdayOpen_NO() {
        doReturn(FRIDAY_TODAY).when(tecCollectReservationMessageService).getTecCollectDate();

        when(messageFacade.getMessageForCodeAndLocale(TEC_COLLECT_MESSAGE_KEY, LocaleUtils.toLocale("en")))
                .thenReturn(TEC_COLLECT_MESSAGE);

        when(openingScheduleService.isStoreOpenOnSaturday(pos)).thenReturn(true);

        assertThat(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, SATURDAY, true)).isEmpty();

        verifyZeroInteractions(messageFacade);
        verify(openingScheduleService).isStoreOpenOnSaturday(pos);
    }

    @Test
    public void testShouldShowTecCollectMessage_SaturdayAndMonday_saturdayOpen_NO() {
        doReturn(SATURDAY_TODAY).when(tecCollectReservationMessageService).getTecCollectDate();

        when(messageFacade.getMessageForCodeAndLocale(TEC_COLLECT_MESSAGE_KEY, LocaleUtils.toLocale("en")))
                .thenReturn(TEC_COLLECT_MESSAGE);

        when(openingScheduleService.isStoreOpenOnSaturday(pos)).thenReturn(true);

        assertThat(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, MONDAY, true)).isEmpty();

        verifyZeroInteractions(messageFacade);
    }

    @Test
    public void testShouldShowTecCollectMessage_SaturdayAndMonday_saturdayClosed_NO() {
        doReturn(SATURDAY_TODAY).when(tecCollectReservationMessageService).getTecCollectDate();

        when(messageFacade.getMessageForCodeAndLocale(TEC_COLLECT_MESSAGE_KEY, LocaleUtils.toLocale("en")))
                .thenReturn(TEC_COLLECT_MESSAGE);

        when(openingScheduleService.isStoreOpenOnSaturday(pos)).thenReturn(false);

        assertThat(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, MONDAY, true)).isEmpty();

        verifyZeroInteractions(messageFacade);
    }

    @Test
    public void testShouldShowTecCollectMessage_FridayAndTuesday_saturdayOpen_YES() {
        doReturn(FRIDAY_TODAY).when(tecCollectReservationMessageService).getTecCollectDate();
        when(openingScheduleService.isStoreOpenOnSaturday(pos)).thenReturn(true);

        when(messageFacade.getMessageForCodeAndCurrentLocale(TEC_COLLECT_MESSAGE_KEY))
                .thenReturn(TEC_COLLECT_MESSAGE);

        assertThat(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, TUESDAY_NEXT, true)).isEqualTo(TEC_COLLECT_MESSAGE);

        verify(cart).getDeliveryPointOfService();
        verify(openingScheduleService).isStoreOpenOnSaturday(pos);
        verify(messageFacade).getMessageForCodeAndCurrentLocale(TEC_COLLECT_MESSAGE_KEY);
    }

    @Test
    public void testShouldShowTecCollectMessage_MondayAndWednesday_YES() {
        doReturn(MONDAY_TODAY).when(tecCollectReservationMessageService).getTecCollectDate();

        when(messageFacade.getMessageForCodeAndCurrentLocale(TEC_COLLECT_MESSAGE_KEY))
                .thenReturn(TEC_COLLECT_MESSAGE);

        assertThat(tecCollectReservationMessageService.generateTecCollectReservationMessage(cart, WEDNESDAY, true)).isEqualTo(TEC_COLLECT_MESSAGE);

        verifyZeroInteractions(openingScheduleService);
        verify(messageFacade).getMessageForCodeAndCurrentLocale(TEC_COLLECT_MESSAGE_KEY);
    }

}