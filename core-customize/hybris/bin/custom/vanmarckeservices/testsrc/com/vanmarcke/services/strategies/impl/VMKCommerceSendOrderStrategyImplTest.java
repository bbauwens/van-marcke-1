package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.core.enums.IBMOrderStatus;
import com.vanmarcke.core.model.IBMOrderModel;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PlaceOrderInfoResponseData;
import com.vanmarcke.cpi.data.order.PlaceOrderResponseData;
import com.vanmarcke.cpi.data.order.PlaceOrdersInfoResponseData;
import com.vanmarcke.cpi.services.ESBPlaceOrderService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.ExportStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Set;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceSendOrderStrategyImplTest {

    @Mock
    private Converter<AbstractOrderModel, OrderRequestData> vmkPlaceOrderRequestConverter;

    @Mock
    private ESBPlaceOrderService esbPlaceOrderService;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private VMKCommerceSendOrderStrategyImpl defaultVMKCommerceSendOrderStrategy;

    @Test
    public void testSendOrder_noResponse() {
        OrderModel order = mock(OrderModel.class);
        OrderRequestData orderRequestData = mock(OrderRequestData.class);

        when(vmkPlaceOrderRequestConverter.convert(order)).thenReturn(orderRequestData);
        when(esbPlaceOrderService.getPlaceOrderInformation(orderRequestData)).thenReturn(null);
        OrderModel result = defaultVMKCommerceSendOrderStrategy.sendOrder(order);

        verify(vmkPlaceOrderRequestConverter).convert(order);
        verify(esbPlaceOrderService).getPlaceOrderInformation(orderRequestData);
        verify(modelService).save(order);
        verify(order).setExportStatus(ExportStatus.NOTEXPORTED);
        assertThat(result).isEqualTo(order);
    }

    @Test
    public void testSendOrder_ResponseHasNoData() {
        OrderModel order = mock(OrderModel.class);
        OrderRequestData orderRequestData = mock(OrderRequestData.class);
        PlaceOrderResponseData response = mock(PlaceOrderResponseData.class);

        when(vmkPlaceOrderRequestConverter.convert(order)).thenReturn(orderRequestData);
        when(esbPlaceOrderService.getPlaceOrderInformation(orderRequestData)).thenReturn(response);
        when(response.getData()).thenReturn(null);
        OrderModel result = defaultVMKCommerceSendOrderStrategy.sendOrder(order);

        verify(vmkPlaceOrderRequestConverter).convert(order);
        verify(esbPlaceOrderService).getPlaceOrderInformation(orderRequestData);
        verify(modelService).save(order);
        verify(order).setExportStatus(ExportStatus.NOTEXPORTED);
        assertThat(result).isEqualTo(order);
    }

    @Test
    public void testSendOrder_ResponseHasData_Error() {
        OrderModel order = mock(OrderModel.class);
        OrderRequestData orderRequestData = mock(OrderRequestData.class);
        PlaceOrderResponseData response = mock(PlaceOrderResponseData.class);
        PlaceOrdersInfoResponseData data = mock(PlaceOrdersInfoResponseData.class);

        when(vmkPlaceOrderRequestConverter.convert(order)).thenReturn(orderRequestData);
        when(esbPlaceOrderService.getPlaceOrderInformation(orderRequestData)).thenReturn(response);
        when(response.getData()).thenReturn(data);
        when(data.getOrders()).thenReturn(null);
        OrderModel result = defaultVMKCommerceSendOrderStrategy.sendOrder(order);

        verify(vmkPlaceOrderRequestConverter).convert(order);
        verify(esbPlaceOrderService).getPlaceOrderInformation(orderRequestData);
        verify(modelService).save(order);
        verify(order).setExportStatus(ExportStatus.NOTEXPORTED);
        assertThat(result).isEqualTo(order);
    }

    @Test
    public void testSendOrder_ResponseHasData_ProductID() {
        OrderModel order = mock(OrderModel.class);
        OrderRequestData orderRequestData = mock(OrderRequestData.class);
        PlaceOrderResponseData response = mock(PlaceOrderResponseData.class);
        PlaceOrdersInfoResponseData data = mock(PlaceOrdersInfoResponseData.class);
        PlaceOrderInfoResponseData orderData = mock(PlaceOrderInfoResponseData.class);
        IBMOrderModel ibmOrderModel = mock(IBMOrderModel.class);
        when(modelService.create(IBMOrderModel.class)).thenReturn(ibmOrderModel);
        when(vmkPlaceOrderRequestConverter.convert(order)).thenReturn(orderRequestData);
        when(esbPlaceOrderService.getPlaceOrderInformation(orderRequestData)).thenReturn(response);
        when(response.getData()).thenReturn(data);
        when(data.getOrders()).thenReturn(List.of(orderData));
        when(orderData.getId()).thenReturn("W1234");
        when(orderData.isInError()).thenReturn(false);
        when(ibmOrderModel.getCode()).thenReturn("W1234");
        when(order.getIbmOrders()).thenReturn(Set.of(ibmOrderModel));
        when(ibmOrderModel.getOrderStatus()).thenReturn(IBMOrderStatus.SENT_TO_ERP);

        OrderModel result = defaultVMKCommerceSendOrderStrategy.sendOrder(order);

        verify(vmkPlaceOrderRequestConverter).convert(order);
        verify(esbPlaceOrderService).getPlaceOrderInformation(orderRequestData);
        verify(modelService).save(ibmOrderModel);
        verify(ibmOrderModel).setCode("W1234");
        verify(order).setIbmOrders(Set.of(ibmOrderModel));
        verify(modelService).save(order);
        verify(order).setExportStatus(ExportStatus.EXPORTED);
        verify(order).setExternalCode("W1234");
        verify(order).setStatus(OrderStatus.COMPLETED);
        assertThat(result).isEqualTo(order);
    }
}