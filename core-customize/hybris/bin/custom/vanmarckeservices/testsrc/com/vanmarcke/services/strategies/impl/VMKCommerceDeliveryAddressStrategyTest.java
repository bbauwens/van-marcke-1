package com.vanmarcke.services.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceDeliveryModeValidationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceDeliveryAddressStrategyTest {

    @Mock
    private ModelService modelService;
    @Mock
    private DeliveryService deliveryService;
    @Mock
    private CommerceCartCalculationStrategy commerceCartCalculationStrategy;
    @Mock
    private CommerceDeliveryModeValidationStrategy commerceDeliveryModeValidationStrategy;

    @InjectMocks
    VMKCommerceDeliveryAddressStrategy vmkCommerceDeliveryAddressStrategy;

    @Test
    public void testStoreDeliveryAddress() {
        // given
        CommerceCheckoutParameter commerceCheckoutParameter = mock(CommerceCheckoutParameter.class);
        CartModel cartModel = mock(CartModel.class);
        CartEntryModel cartEntryModel = mock(CartEntryModel.class);
        AddressModel addressModel = mock(AddressModel.class);
        ZoneDeliveryModeModel zoneDeliveryMode = mock(ZoneDeliveryModeModel.class);
        ZoneDeliveryModeModel oldZoneDeliveryMode = mock(ZoneDeliveryModeModel.class);

        when(cartModel.getDeliveryMode()).thenReturn(oldZoneDeliveryMode);
        when(cartModel.getEntries()).thenReturn(List.of(cartEntryModel));
        when(commerceCheckoutParameter.getAddress()).thenReturn(addressModel);
        when(commerceCheckoutParameter.getCart()).thenReturn(cartModel);
        when(deliveryService.getSupportedDeliveryModeListForOrder(cartModel)).thenReturn(List.of(zoneDeliveryMode));
        when(deliveryService.getSupportedDeliveryAddressesForOrder(cartModel, false)).thenReturn(List.of(addressModel));
        when(zoneDeliveryMode.getCode()).thenReturn("be-standard");
        when(oldZoneDeliveryMode.getCode()).thenReturn("be-alternative");

        // when
        vmkCommerceDeliveryAddressStrategy.storeDeliveryAddress(commerceCheckoutParameter);

        // then
        verify(cartModel).setDeliveryAddress(addressModel);
        verify(cartModel).setDeliveryMode(zoneDeliveryMode);
        verify(modelService, times(2)).save(cartModel);
        verify(cartEntryModel).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testStoreDeliveryAddress_nullCurrentDeliveryMode() {
        // given
        CommerceCheckoutParameter commerceCheckoutParameter = mock(CommerceCheckoutParameter.class);
        CartModel cartModel = mock(CartModel.class);
        CartEntryModel cartEntryModel = mock(CartEntryModel.class);
        AddressModel addressModel = mock(AddressModel.class);
        ZoneDeliveryModeModel zoneDeliveryMode = mock(ZoneDeliveryModeModel.class);

        when(cartModel.getDeliveryMode()).thenReturn(null);
        when(cartModel.getEntries()).thenReturn(List.of(cartEntryModel));
        when(commerceCheckoutParameter.getAddress()).thenReturn(addressModel);
        when(commerceCheckoutParameter.getCart()).thenReturn(cartModel);
        when(deliveryService.getSupportedDeliveryModeListForOrder(cartModel)).thenReturn(List.of(zoneDeliveryMode));
        when(deliveryService.getSupportedDeliveryAddressesForOrder(cartModel, false)).thenReturn(List.of(addressModel));
        when(zoneDeliveryMode.getCode()).thenReturn("be-standard");

        // when
        vmkCommerceDeliveryAddressStrategy.storeDeliveryAddress(commerceCheckoutParameter);

        // then
        verify(cartModel).setDeliveryAddress(addressModel);
        verify(cartModel).setDeliveryMode(zoneDeliveryMode);
        verify(modelService, times(2)).save(cartModel);
        verify(cartEntryModel).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testStoreDeliveryAddress_noChange() {
        // given
        CommerceCheckoutParameter commerceCheckoutParameter = mock(CommerceCheckoutParameter.class);
        CartModel cartModel = mock(CartModel.class);
        CartEntryModel cartEntryModel = mock(CartEntryModel.class);
        AddressModel addressModel = mock(AddressModel.class);
        ZoneDeliveryModeModel zoneDeliveryMode = mock(ZoneDeliveryModeModel.class);
        ZoneDeliveryModeModel oldZoneDeliveryMode = mock(ZoneDeliveryModeModel.class);

        when(cartModel.getDeliveryMode()).thenReturn(oldZoneDeliveryMode);
        when(cartModel.getEntries()).thenReturn(List.of(cartEntryModel));
        when(commerceCheckoutParameter.getAddress()).thenReturn(addressModel);
        when(commerceCheckoutParameter.getCart()).thenReturn(cartModel);
        when(deliveryService.getSupportedDeliveryModeListForOrder(cartModel)).thenReturn(List.of(zoneDeliveryMode));
        when(deliveryService.getSupportedDeliveryAddressesForOrder(cartModel, false)).thenReturn(List.of(addressModel));
        when(zoneDeliveryMode.getCode()).thenReturn("be-standard");
        when(oldZoneDeliveryMode.getCode()).thenReturn("be-standard");

        // when
        vmkCommerceDeliveryAddressStrategy.storeDeliveryAddress(commerceCheckoutParameter);

        // then
        verify(cartModel).setDeliveryAddress(addressModel);
        verify(cartEntryModel, times(0)).setDeliveryMode(any(ZoneDeliveryModeModel.class));
        verify(modelService, times(2)).save(cartModel);
        verify(cartEntryModel, times(0)).setCalculated(anyBoolean());
    }


}