package com.vanmarcke.services.util;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
public class URIUtilsTest {

    @Test
    public void testNormalize() {
        String result = URIUtils.normalize("/nl_BE/c/cat-name/p/A06002");
        assertThat(result).isEqualTo("/nl_BE/c/cat-name/p/A06002");
    }

    @Test
    public void testNormalize_withRepeatedSlashes() {
        String result = URIUtils.normalize("/////nl_BE////c///p//A06002");
        assertThat(result).isEqualTo("/nl_BE/c/p/A06002");
    }

}