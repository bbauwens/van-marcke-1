package com.vanmarcke.services.order.credit.impl;

import com.vanmarcke.cpi.data.order.CreditCheckInfoResponseData;
import com.vanmarcke.cpi.data.order.CreditCheckResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.ptv.CreditCheckInfoResponseEnum;
import com.vanmarcke.cpi.services.ESBCreditCheckService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCreditCheckServiceImplTest {

    @Mock
    private ESBCreditCheckService esbCreditCheckService;

    @Mock
    private Converter<AbstractOrderModel, OrderRequestData> orderRequestConverter;

    @Mock
    private ModelService modelService;

    @Spy
    @InjectMocks
    private VMKCreditCheckServiceImpl vmkCreditCheckService;

    @Test
    public void testValidateCreditworthinessForCart_notCreditWorthy() {
        final CartModel cart = mock(CartModel.class);
        final CreditCheckResponseData creditCheckResponseData = mock(CreditCheckResponseData.class);
        final CreditCheckInfoResponseData creditCheckData = mock(CreditCheckInfoResponseData.class);

        when(creditCheckResponseData.getData()).thenReturn(creditCheckData);
        when(creditCheckData.getCreditworthy()).thenReturn(CreditCheckInfoResponseEnum.NOT_CREDIT_WORTHY);

        OrderRequestData orderRequest = mock(OrderRequestData.class);
        when(orderRequestConverter.convert(cart)).thenReturn(orderRequest);

        when(this.esbCreditCheckService.getCreditCheckInformation(orderRequest)).thenReturn(creditCheckResponseData);

        boolean result = this.vmkCreditCheckService.validateCreditworthinessForCart(cart);
        assertThat(result).isFalse();
        verify(cart, times(1)).setCreditLimitWarning(false);
        verify(cart, times(1)).setCreditLimitExceeded(false);
        verify(cart, never()).setCreditLimitWarning(true);
        verify(cart, never()).setCreditLimitExceeded(true);
    }

    @Test
    public void testValidateCreditworthinessForCart_creditWorthy() {
        final CartModel cart = mock(CartModel.class);
        final CreditCheckResponseData creditCheckResponseData = mock(CreditCheckResponseData.class);
        final CreditCheckInfoResponseData creditCheckData = mock(CreditCheckInfoResponseData.class);

        when(creditCheckResponseData.getData()).thenReturn(creditCheckData);
        when(creditCheckData.getCreditworthy()).thenReturn(CreditCheckInfoResponseEnum.CREDIT_WORTHY);

        OrderRequestData orderRequest = mock(OrderRequestData.class);
        when(orderRequestConverter.convert(cart)).thenReturn(orderRequest);

        when(this.esbCreditCheckService.getCreditCheckInformation(orderRequest)).thenReturn(creditCheckResponseData);

        boolean result = this.vmkCreditCheckService.validateCreditworthinessForCart(cart);
        assertThat(result).isTrue();
        verify(cart, times(1)).setCreditLimitWarning(false);
        verify(cart, times(1)).setCreditLimitExceeded(false);
        verify(cart, never()).setCreditLimitWarning(true);
        verify(cart, never()).setCreditLimitExceeded(true);
    }

    @Test
    public void testValidateCreditworthinessForCart_limitExceeded_positive() {
        final CartModel cart = mock(CartModel.class);
        final CreditCheckResponseData creditCheckResponseData = mock(CreditCheckResponseData.class);
        final CreditCheckInfoResponseData creditCheckData = mock(CreditCheckInfoResponseData.class);
        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);

        when(creditCheckResponseData.getData()).thenReturn(creditCheckData);
        when(creditCheckData.getCreditworthy()).thenReturn(CreditCheckInfoResponseEnum.CREDIT_LIMIT_EXCEEDED);

        OrderRequestData orderRequest = mock(OrderRequestData.class);
        when(orderRequestConverter.convert(cart)).thenReturn(orderRequest);

        when(deliveryMode.getCode()).thenReturn("test-standard");
        when(cart.getDeliveryMode()).thenReturn(deliveryMode);

        when(this.esbCreditCheckService.getCreditCheckInformation(orderRequest)).thenReturn(creditCheckResponseData);
        doReturn(2L).when(vmkCreditCheckService).calculateWorkingDaysBetween(any(), any());

        when(cart.getDeliveryDate()).thenReturn(new Date());

        boolean result = this.vmkCreditCheckService.validateCreditworthinessForCart(cart);
        assertThat(result).isTrue();
        verify(cart, times(1)).setCreditLimitWarning(false);
        verify(cart, times(1)).setCreditLimitExceeded(false);
        verify(cart, times(1)).setCreditLimitWarning(true);
        verify(cart, times(1)).setCreditLimitExceeded(true);
    }

    @Test
    public void testValidateCreditworthinessForCart_limitExceeded_negative() {
        final CartModel cart = mock(CartModel.class);
        final CreditCheckResponseData creditCheckResponseData = mock(CreditCheckResponseData.class);
        final CreditCheckInfoResponseData creditCheckData = mock(CreditCheckInfoResponseData.class);
        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);

        when(creditCheckResponseData.getData()).thenReturn(creditCheckData);
        when(creditCheckData.getCreditworthy()).thenReturn(CreditCheckInfoResponseEnum.CREDIT_LIMIT_EXCEEDED);

        OrderRequestData orderRequest = mock(OrderRequestData.class);
        when(orderRequestConverter.convert(cart)).thenReturn(orderRequest);

        when(deliveryMode.getCode()).thenReturn("test-standard");
        when(cart.getDeliveryMode()).thenReturn(deliveryMode);

        when(this.esbCreditCheckService.getCreditCheckInformation(orderRequest)).thenReturn(creditCheckResponseData);
        doReturn(0L).when(vmkCreditCheckService).calculateWorkingDaysBetween(any(), any());

        boolean result = this.vmkCreditCheckService.validateCreditworthinessForCart(cart);
        assertThat(result).isFalse();
        verify(cart, times(1)).setCreditLimitWarning(false);
        verify(cart, times(1)).setCreditLimitExceeded(false);
        verify(cart, times(1)).setCreditLimitExceeded(true);
        verify(cart, never()).setCreditLimitWarning(true);
    }
}