package com.vanmarcke.services.model.builder;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code CartEntryModelMockBuilder} class can be used to create a mock implementation of the
 * {@link CartEntryModel} class.
 *
 * @author Christiaan Janssen
 * @since 08-07-2020
 */
public class CartEntryModelMockBuilder extends AbstractOrderEntryModelMockBuilder<CartEntryModelMockBuilder, CartEntryModel> {

    /**
     * Creates a new instance of the {@link CartEntryModelMockBuilder} class.
     */
    private CartEntryModelMockBuilder() {
        super(CartEntryModelMockBuilder.class);
    }

    /**
     * The entry point to the {@link CartEntryModelMockBuilder}.
     *
     * @return the {@link CartEntryModelMockBuilder} instance for method chaining
     */
    public static CartEntryModelMockBuilder anEntry() {
        return new CartEntryModelMockBuilder();
    }

    /**
     * Creates a mock implementation of the {@link CartEntryModel} class.
     *
     * @return the {@link CartEntryModel} instance
     */
    public CartEntryModel build() {
        CartEntryModel entry = mock(CartEntryModel.class);
        when(entry.getQuantity()).thenReturn(quantity);
        when(entry.getOrder()).thenReturn((CartModel) order);
        when(entry.getProduct()).thenReturn(product);
        return entry;
    }
}
