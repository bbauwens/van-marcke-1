package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.core.helper.CheckoutDateHelper;
import com.vanmarcke.cpi.data.order.EntryInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storelocator.schedule.VMKOpeningScheduleService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPickupInfoServiceImplTest {

    private static final Date FUTURE_DATE = DateTime.now().plusDays(3).toDate();
    private static final Date TOMORROW_DATE = DateTime.now().plusDays(1).toDate();
    private static final Date PAST_DATE = DateTime.now().minusDays(3).toDate();

    private CartModel cart;
    private PickupDateInfoResponseData responseData;

    @Mock
    private VMKPointOfServiceService pointOfServiceService;
    @Mock
    private VMKDeliveryInfoServiceImpl deliveryInfoService;
    @Mock
    private CheckoutDateHelper checkoutDateHelper;
    @Mock
    private VMKFirstDateAvailabilityServiceImpl vmkFirstDateAvailabilityService;
    @Mock
    private VMKOpeningScheduleService openingScheduleService;

    @Spy
    @InjectMocks
    private VMKPickupInfoServiceImpl pickupInfoService;

    @Before
    public void setUp() throws Exception {
        cart = CartModelMockBuilder
                .aCart()
                .build();

        responseData = new PickupDateInfoResponseData();
    }

    @Test
    public void testIsPickupPossibleWithoutPickupMethods() {
        doReturn(Optional.empty()).when(pickupInfoService).getFirstPossiblePickupDate(cart);
        doReturn(false).when(pickupInfoService).isTecCollectPossible(cart);
        when(pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart)).thenReturn(null);

        assertThat(pickupInfoService.isPickupPossible(cart)).isFalse();

        verify(pointOfServiceService).getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart);
    }

    @Test
    public void testIsPickupPossibleWithTecCollectPossible() {
        doReturn(Optional.empty()).when(pickupInfoService).getFirstPossiblePickupDate(cart);
        doReturn(true).when(pickupInfoService).isTecCollectPossible(cart);
        when(pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart)).thenReturn(null);

        assertThat(pickupInfoService.isPickupPossible(cart)).isTrue();

        verify(pointOfServiceService, never()).getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart);
    }

    @Test
    public void testIsPickupPossibleWithPickupDate() {
        doReturn(Optional.of(new Date())).when(pickupInfoService).getFirstPossiblePickupDate(cart);

        assertThat(pickupInfoService.isPickupPossible(cart)).isTrue();

        verify(pickupInfoService, never()).isTecCollectPossible(cart);
        verify(pointOfServiceService, never()).getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart);
    }

    @Test
    public void testIsPickupPossibleWithOpenAlternativeStores() {
        doReturn(Optional.empty()).when(pickupInfoService).getFirstPossiblePickupDate(cart);
        doReturn(false).when(pickupInfoService).isTecCollectPossible(cart);
        when(pointOfServiceService.getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart))
                .thenReturn(singletonList(mock(PointOfServiceModel.class)));

        assertThat(pickupInfoService.isPickupPossible(cart)).isTrue();

        verify(pointOfServiceService).getAlternativeTECStoresWhichAreCurrentlyOpenForCart(cart);

    }

    @Test
    public void testGetFirstPossiblePickupDate() {
        //given
        PickupDateInfoResponseData response = createPickupDateResponseWithPickupDates(TOMORROW_DATE, FUTURE_DATE);
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(response));
        when(vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(any(Date.class))).then(AdditionalAnswers.returnsFirstArg());
        when(checkoutDateHelper.isValidDate(any(Date.class))).thenReturn(true);

        //when
        final Optional<Date> result = pickupInfoService.getFirstPossiblePickupDate(cart);

        //then
        assertThat(result).hasValue(TOMORROW_DATE);
    }

    @Test
    public void testGetFirstPossiblePickupDateWithoutResponse() {
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.empty());

        assertThat(pickupInfoService.getFirstPossiblePickupDate(cart)).isEmpty();
    }

    @Test
    public void testGetFirstPossiblePickupDateWhenPickupDateInvalid() {
        //given
        PickupDateInfoResponseData response = createPickupDateResponseWithPickupDates(PAST_DATE);
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(response));
        when(vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(any(Date.class))).then(AdditionalAnswers.returnsFirstArg());

        //when
        final Optional<Date> result = pickupInfoService.getFirstPossiblePickupDate(cart);

        //then
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetFirstPossiblePickupDateWhenPickupDateNull() {
        //given
        PickupDateInfoResponseData response = createPickupDateResponseWithPickupDates(TOMORROW_DATE, FUTURE_DATE, null);
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(response));

        //when
        final Optional<Date> result = pickupInfoService.getFirstPossiblePickupDate(cart);

        //then
        assertThat(result).isEmpty();
        verifyZeroInteractions(vmkFirstDateAvailabilityService);
        verifyZeroInteractions(checkoutDateHelper);
    }

    @Test
    public void testIsTecCollectPossibleWithNullResponse() {
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.empty());

        assertThat(pickupInfoService.isTecCollectPossible(cart)).isFalse();
    }

    @Test
    public void testIsTecCollectPossibleWithNoPos() {
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.empty());

        assertThat(pickupInfoService.isTecCollectPossible(cart)).isFalse();
    }

    @Test
    public void testIsTecCollectPossibleWithNoTecCollectDate() {
        responseData.setExpressPickupDate(null);
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(responseData));
        assertThat(pickupInfoService.isTecCollectPossible(cart)).isFalse();
    }

    @Test
    public void testIsTecCollectPossibleWithPosClosed() {
        PointOfServiceModel pos = mock(PointOfServiceModel.class);
        when(cart.getDeliveryPointOfService()).thenReturn(pos);

        responseData.setExpressPickupDate(FUTURE_DATE);
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(responseData));
        assertThat(pickupInfoService.isTecCollectPossible(cart)).isFalse();

        when(openingScheduleService.isStoreOpenOnGivenDate(pos, FUTURE_DATE)).thenReturn(false);

        assertThat(pickupInfoService.isTecCollectPossible(cart)).isFalse();
    }

    private PickupDateInfoResponseData createPickupDateResponseWithPickupDates(final Date... pickupDates) {
        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        final List<EntryInfoResponseData> responseEntries = Arrays.stream(pickupDates)
                .map(pickupDate -> {
                    EntryInfoResponseData entry = new EntryInfoResponseData();
                    entry.setPickupDate(pickupDate);
                    return entry;
                })
                .collect(Collectors.toList());
        response.setProducts(responseEntries);
        return response;
    }
}
