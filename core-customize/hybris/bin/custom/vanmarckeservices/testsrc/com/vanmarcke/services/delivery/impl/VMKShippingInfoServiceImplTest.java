package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.core.helper.CheckoutDateHelper;
import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingEntryInfoResponseData;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKShippingInfoServiceImplTest {

    private static final Date TODAY_DATE = DateTime.now().toDate();
    private static final Date TOMORROW_DATE = DateTime.now().plusDays(1).toDate();
    private static final Date FUTURE_DATE = DateTime.now().plusDays(3).toDate();

    private CartModel cart;

    @Mock
    private VMKDeliveryInfoService deliveryInfoService;
    @Mock
    private CheckoutDateHelper checkoutDateHelper;
    @Mock
    private VMKFirstDateAvailabilityServiceImpl vmkFirstDateAvailabilityService;
    @Spy
    @InjectMocks
    private VMKShippingInfoServiceImpl shippingInfoService;

    @Before
    public void setUp() throws Exception {
        cart = CartModelMockBuilder
                .aCart()
                .build();
    }

    @Test
    public void testIsShippingPossibleWhenFirstShippingDateIsNotFound() {
        //given
        doReturn(Optional.empty()).when(shippingInfoService).getFirstPossibleShippingDate(cart);

        //when
        final boolean result = shippingInfoService.isShippingPossible(cart);

        //then
        assertThat(result).isFalse();
    }

    @Test
    public void testIsShippingPossibleWhenFirstShippingDateIsFound() {
        //given
        doReturn(Optional.of(FUTURE_DATE)).when(shippingInfoService).getFirstPossibleShippingDate(cart);

        //when
        final boolean result = shippingInfoService.isShippingPossible(cart);

        //then
        assertThat(result).isTrue();
    }

    @Test
    public void testGetFirstPossibleShippingDateWhenNoShippingDateResponse() {
        //given
        when(deliveryInfoService.getShippingDateInformation(cart)).thenReturn(Optional.empty());

        //when
        final Optional<Date> result = shippingInfoService.getFirstPossibleShippingDate(cart);

        //then
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetFirstPossibleShippingDateWhenDateIsInvalid() {
        //given
        ShippingDateInfoResponseData response = createShippingDateResponseWithDeliveryDates(TOMORROW_DATE);
        when(deliveryInfoService.getShippingDateInformation(cart)).thenReturn(Optional.of(response));
        when(vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(any(Date.class))).then(AdditionalAnswers.returnsFirstArg());
        when(checkoutDateHelper.isValidDate(any(Date.class))).thenReturn(false);

        //when
        final Optional<Date> result = shippingInfoService.getFirstPossibleShippingDate(cart);

        //then
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetFirstPossibleShippingDate() {
        //given
        ShippingDateInfoResponseData response = createShippingDateResponseWithDeliveryDates(TOMORROW_DATE, FUTURE_DATE);
        when(deliveryInfoService.getShippingDateInformation(cart)).thenReturn(Optional.of(response));
        when(vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(any(Date.class))).then(AdditionalAnswers.returnsFirstArg());
        when(checkoutDateHelper.isValidDate(any(Date.class))).thenReturn(true);

        //when
        final Optional<Date> result = shippingInfoService.getFirstPossibleShippingDate(cart);

        //then
        assertThat(result).hasValue(TOMORROW_DATE);
    }

    @Test
    public void testGetFirstPossibleShippingDateForCompleteCartWhenDeliveryDateNull() {
        //given
        ShippingDateInfoResponseData response = createShippingDateResponseWithDeliveryDates(TOMORROW_DATE, FUTURE_DATE, null);
        when(deliveryInfoService.getShippingDateInformation(cart)).thenReturn(Optional.of(response));

        //when
        final Date result = shippingInfoService.getFirstPossibleShippingDateForCompleteCart(cart);

        //then
        assertThat(result).isNull();
    }

    @Test
    public void testGetFirstPossibleShippingDateForCompleteCart() {
        //given
        ShippingDateInfoResponseData response = createShippingDateResponseWithDeliveryDates(TODAY_DATE, TOMORROW_DATE, FUTURE_DATE);
        when(deliveryInfoService.getShippingDateInformation(cart)).thenReturn(Optional.of(response));
        when(vmkFirstDateAvailabilityService.getFirstValidNoClosingDate(any(Date.class))).then(AdditionalAnswers.returnsFirstArg());
        when(checkoutDateHelper.isValidDate(any(Date.class))).thenReturn(true);

        //when
        final Date result = shippingInfoService.getFirstPossibleShippingDateForCompleteCart(cart);

        //then
        assertThat(result).isEqualTo(FUTURE_DATE);
    }

    private ShippingDateInfoResponseData createShippingDateResponseWithDeliveryDates(final Date... deliveryDates) {
        ShippingDateInfoResponseData response = new ShippingDateInfoResponseData();
        final List<ShippingEntryInfoResponseData> responseEntries = Arrays.stream(deliveryDates)
                .map(deliveryDate -> {
                    ShippingEntryInfoResponseData entry = new ShippingEntryInfoResponseData();
                    entry.setDeliveryDate(deliveryDate);
                    return entry;
                })
                .collect(Collectors.toList());
        response.setResults(responseEntries);
        return response;
    }
}
