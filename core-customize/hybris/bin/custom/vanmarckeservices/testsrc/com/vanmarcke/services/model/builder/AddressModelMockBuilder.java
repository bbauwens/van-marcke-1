package com.vanmarcke.services.model.builder;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code AddressModelMockBuilder} class can be used to create a mock implementation of the
 * {@link AddressModel} class.
 *
 * @author Christiaan Janssen
 * @since 07-07-2020
 */
public class AddressModelMockBuilder {

    private String apartment;
    private String town;
    private String phone2;
    private CountryModel country;

    /**
     * Creates a new instance of the {@link AddressModelMockBuilder} class.
     */
    private AddressModelMockBuilder() {
    }

    /**
     * The entry point to the {@link AddressModelMockBuilder}.
     *
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public static AddressModelMockBuilder anAddress() {
        return new AddressModelMockBuilder();
    }

    /**
     * Sets the {@code apartment} on the current {@link AddressModelMockBuilder} instance
     *
     * @param apartment the apartment
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public AddressModelMockBuilder withApartment(String apartment) {
        this.apartment = apartment;
        return this;
    }

    /**
     * Sets the {@code town} on the current {@link AddressModelMockBuilder} instance
     *
     * @param town the town
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public AddressModelMockBuilder withTown(String town) {
        this.town = town;
        return this;
    }

    /**
     * Sets the {@code phone2} on the current {@link AddressModelMockBuilder} instance
     *
     * @param phone2 the cell phone number
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public AddressModelMockBuilder withPhone2(String phone2) {
        this.phone2 = phone2;
        return this;
    }

    /**
     * Sets the {@code country} on the current {@link AddressModelMockBuilder} instance
     *
     * @param country the country
     * @return the {@link AddressModelMockBuilder} instance for method chaining
     */
    public AddressModelMockBuilder withCountry(CountryModel country) {
        this.country = country;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link AddressModel} class.
     *
     * @return the {@link AddressModel} instance
     */
    public AddressModel build() {
        AddressModel address = mock(AddressModel.class);
        when(address.getAppartment()).thenReturn(apartment);
        when(address.getTown()).thenReturn(town);
        when(address.getPhone2()).thenReturn(phone2);
        when(address.getCountry()).thenReturn(country);
        return address;
    }
}
