package com.vanmarcke.services.model.builder;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code VariantProductModelMockBuilder} class can be used to create a mock implementation of the
 * {@link VariantProductModel} class.
 *
 * @author Christiaan Janssen
 * @since 29-04-2020
 */
public class VariantProductModelMockBuilder {

    private final String code;
    private final Locale locale;
    private final List<CategoryModel> supercategories;
    private final Map<Locale, List<MediaModel>> instructionManuals;
    private final Map<Locale, List<MediaModel>> generalManuals;
    private final Map<Locale, List<MediaModel>> userManuals;
    private final Map<Locale, List<MediaModel>> maintenanceManuals;
    private final Map<Locale, List<MediaModel>> certificates;
    private final Map<Locale, MediaModel> DOP;
    private final Map<Locale, MediaModel> normalisation;
    private final Map<Locale, MediaModel> techDataSheet;
    private final Map<Locale, MediaModel> safetyDataSheet;
    private final Map<Locale, MediaModel> productSpecificationSheet;
    private final Map<Locale, MediaModel> warranty;
    private final Map<Locale, MediaModel> sparePartsList;
    private final Map<Locale, MediaModel> ecoDataSheet;
    private final Map<Locale, List<CategoryModel>> brandCatalogs;

    private String itemType;
    private ProductModel baseProduct;
    private List<MediaContainerModel> techDrawings;
    private List<MediaContainerModel> galleryImages;

    /**
     * Creates a new instance of the {@link VariantProductModelMockBuilder} class.
     *
     * @param code   the product code
     * @param locale the local
     */
    private VariantProductModelMockBuilder(String code, Locale locale) {
        this.code = code;
        this.locale = locale;
        this.supercategories = new ArrayList<>();
        this.instructionManuals = new HashMap<>();
        this.generalManuals = new HashMap<>();
        this.userManuals = new HashMap<>();
        this.maintenanceManuals = new HashMap<>();
        this.certificates = new HashMap<>();
        this.DOP = new HashMap<>();
        this.normalisation = new HashMap<>();
        this.techDataSheet = new HashMap<>();
        this.safetyDataSheet = new HashMap<>();
        this.productSpecificationSheet = new HashMap<>();
        this.warranty = new HashMap<>();
        this.sparePartsList = new HashMap<>();
        this.ecoDataSheet = new HashMap<>();
        this.brandCatalogs = new HashMap<>();
    }

    /**
     * The entry point to the {@link VariantProductModelMockBuilder}.
     *
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public static VariantProductModelMockBuilder aVariantProduct() {
        return new VariantProductModelMockBuilder(null, null);
    }

    /**
     * The entry point to the {@link VariantProductModelMockBuilder}.
     *
     * @param code the product code
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public static VariantProductModelMockBuilder aVariantProduct(String code, Locale locale) {
        return new VariantProductModelMockBuilder(code, locale);
    }

    /**
     * Sets the {@code baseProduct} on the current {@link VariantProductModelMockBuilder} instance
     *
     * @param baseProduct the variant's base product
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withBaseProduct(ProductModel baseProduct) {
        this.baseProduct = baseProduct;
        return this;
    }

    /**
     * Sets the given {@code supercategories} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param supercategories the super categories
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withSupercategories(CategoryModel... supercategories) {
        this.supercategories.addAll(Arrays.asList(supercategories));
        return this;
    }

    /**
     * Sets the given gallery images on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param galleryImages the gallery images
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withGalleryImages(List<MediaContainerModel> galleryImages) {
        this.galleryImages = galleryImages;
        return this;
    }

    /**
     * Sets the given technical drawings on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param techDrawings the technical drawings
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withTechDrawings(List<MediaContainerModel> techDrawings) {
        this.techDrawings = techDrawings;
        return this;
    }

    /**
     * Sets the given {@code instruction manuals} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param instructionManuals the instruction manuals
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withInstructionManuals(List<MediaModel> instructionManuals) {
        this.instructionManuals.put(locale, instructionManuals);
        return this;
    }

    /**
     * Sets the given {@code general manuals} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param generalManuals the general manuals
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withGeneralManuals(List<MediaModel> generalManuals) {
        this.generalManuals.put(locale, generalManuals);
        return this;
    }

    /**
     * Sets the given {@code certificates} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param certificates the certificates
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withCertificates(List<MediaModel> certificates) {
        this.certificates.put(locale, certificates);
        return this;
    }

    /**
     * Sets the given {@code user manuals} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param userManuals the user manuals
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withUserManuals(List<MediaModel> userManuals) {
        this.userManuals.put(locale, userManuals);
        return this;
    }

    /**
     * Sets the given {@code maintenance manuals} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param maintenanceManuals the maintenance manuals
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withMaintenanceManuals(List<MediaModel> maintenanceManuals) {
        this.maintenanceManuals.put(locale, maintenanceManuals);
        return this;
    }

    /**
     * Sets the given {@code DOP} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param DOP the DOP
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withDOP(MediaModel DOP) {
        this.DOP.put(locale, DOP);
        return this;
    }

    /**
     * Sets the given {@code normalisation} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param normalisation the normalisation
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withNormalisation(MediaModel normalisation) {
        this.normalisation.put(locale, normalisation);
        return this;
    }

    /**
     * Sets the given {@code techDataSheet} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param techDataSheet the techDataSheet
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withTechDataSheet(MediaModel techDataSheet) {
        this.techDataSheet.put(locale, techDataSheet);
        return this;
    }

    /**
     * Sets the given {@code safetyDataSheet} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param safetyDataSheet the safetyDataSheet
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withSafetyDataSheet(MediaModel safetyDataSheet) {
        this.safetyDataSheet.put(locale, safetyDataSheet);
        return this;
    }

    /**
     * Sets the given {@code productSpecificationSheet} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param productSpecificationSheet the productSpecificationSheet
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withProductSpecificationSheet(MediaModel productSpecificationSheet) {
        this.productSpecificationSheet.put(locale, productSpecificationSheet);
        return this;
    }

    /**
     * Sets the given {@code warranty} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param warranty the warranty
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withWarranty(MediaModel warranty) {
        this.warranty.put(locale, warranty);
        return this;
    }

    /**
     * Sets the given {@code sparePartsList} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param sparePartsList the sparePartsList
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withSparePartsList(MediaModel sparePartsList) {
        this.sparePartsList.put(locale, sparePartsList);
        return this;
    }

    /**
     * Sets the given {@code ecoDataSheet} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param ecoDataSheet the ecoDataSheet
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withEcoDataSheet(MediaModel ecoDataSheet) {
        this.ecoDataSheet.put(locale, ecoDataSheet);
        return this;
    }

    /**
     * Sets the given {@code brandCatalogs} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param brandCatalogs the brandCatalogs
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withBrandCatalogs(List<CategoryModel> brandCatalogs) {
        this.brandCatalogs.put(locale, brandCatalogs);
        return this;
    }

    /**
     * Sets the given {@code itemType} on the current {@link VariantProductModelMockBuilder} instance.
     *
     * @param itemType the item type
     * @return the {@link VariantProductModelMockBuilder} instance for method chaining
     */
    public VariantProductModelMockBuilder withItemType(String itemType) {
        this.itemType = itemType;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link VariantProductModel} class.
     *
     * @return the {@link VariantProductModel} instance
     */
    public VariantProductModel build() {
        VariantProductModel product = mock(VariantProductModel.class);
        when(product.getCode()).thenReturn(code);
        when(product.getBaseProduct()).thenReturn(baseProduct);
        when(product.getSupercategories()).thenReturn(supercategories);
        when(product.getGalleryImages()).thenReturn(galleryImages);
        when(product.getTech_drawing()).thenReturn(techDrawings);
        when(product.getInstructionManuals()).thenReturn(instructionManuals.get(locale));
        when(product.getMaintenanceManuals()).thenReturn(maintenanceManuals.get(locale));
        when(product.getGeneralManuals()).thenReturn(generalManuals.get(locale));
        when(product.getUserManuals()).thenReturn(userManuals.get(locale));
        when(product.getCertificates()).thenReturn(certificates.get(locale));
        when(product.getDOP()).thenReturn(DOP.get(locale));
        when(product.getNormalisation()).thenReturn(normalisation.get(locale));
        when(product.getTech_data_sheet()).thenReturn(techDataSheet.get(locale));
        when(product.getSafety_data_sheet()).thenReturn(safetyDataSheet.get(locale));
        when(product.getProduct_specification_sheet()).thenReturn(productSpecificationSheet.get(locale));
        when(product.getWarranty()).thenReturn(warranty.get(locale));
        when(product.getSpare_parts_list()).thenReturn(sparePartsList.get(locale));
        when(product.getEco_data_sheet()).thenReturn(ecoDataSheet.get(locale));
        when(product.getItemtype()).thenReturn(itemType);
        return product;
    }
}
