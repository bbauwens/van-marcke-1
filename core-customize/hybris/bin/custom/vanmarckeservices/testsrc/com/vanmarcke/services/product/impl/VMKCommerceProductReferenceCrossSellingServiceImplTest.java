package com.vanmarcke.services.product.impl;

import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commerceservices.product.data.ReferenceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import org.apache.commons.lang.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceProductReferenceCrossSellingServiceImplTest {

    private static final String CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String CODE2 = RandomStringUtils.randomAlphabetic(10);
    private static final String CODE3 = RandomStringUtils.randomAlphabetic(10);
    private static final ProductReferenceTypeEnum PRODUCT_REF = ProductReferenceTypeEnum.CROSSELLING;

    @Mock
    private VMKCrossSellingLookupServiceImpl lookupService;
    @Mock
    private ProductService productService;
    @InjectMocks
    VMKCommerceProductReferenceCrossSellingServiceImpl crossSellingService;

    @Test
    public void testGetProductReferencesForCode_NoResults() {
        List<String> productList = Collections.emptyList();
        when(lookupService.getCrossSellingProducts(CODE)).thenReturn(productList);

        Assertions
                .assertThat(crossSellingService.getProductReferencesForCode(CODE, PRODUCT_REF, 5))
                .isEmpty();

        verifyZeroInteractions(productService);
    }

    @Test
    public void testGetProductReferencesForCode_NoLimit() {
        List<String> productList = Collections.emptyList();
        when(lookupService.getCrossSellingProducts(CODE)).thenReturn(productList);

        Assertions
                .assertThat(crossSellingService.getProductReferencesForCode(CODE, PRODUCT_REF, null))
                .isEmpty();

        verifyZeroInteractions(productService);
    }

    @Test
    public void testGetProductReferencesForCode_LimitTest() {
        List<String> productList = Arrays.asList(CODE2, CODE3);
        when(lookupService.getCrossSellingProducts(CODE)).thenReturn(productList);

        Assertions
                .assertThat(crossSellingService.getProductReferencesForCode(CODE, PRODUCT_REF, 1))
                .hasSize(1);

        verify(productService).getProductForCode(CODE2);
        verify(productService, times(0)).getProductForCode(CODE3);
    }

    @Test
    public void testGetProductReferencesForCode_SUCCES() {
        List<String> productList = Arrays.asList(CODE2, CODE3);
        when(lookupService.getCrossSellingProducts(CODE)).thenReturn(productList);

        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(CODE3, Locale.JAPAN)
                .build();

        when(productService.getProductForCode(CODE3)).thenReturn(product);

        List<ReferenceData<ProductReferenceTypeEnum, ProductModel>> result =
                crossSellingService.getProductReferencesForCode(CODE, PRODUCT_REF, 2);
        Assertions
                .assertThat(result)
                .hasSize(2);

        Assertions
                .assertThat(result.get(1).getTarget())
                .isEqualTo(product);

        Assertions
                .assertThat(result.get(1).getReferenceType())
                .isEqualTo(PRODUCT_REF);

        verify(productService).getProductForCode(CODE2);
        verify(productService).getProductForCode(CODE3);
    }
}