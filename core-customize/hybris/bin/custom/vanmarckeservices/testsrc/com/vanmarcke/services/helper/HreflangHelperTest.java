package com.vanmarcke.services.helper;

import com.vanmarcke.services.search.solrfacetsearch.provider.VMKModelUrlResolver;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * * The {@link HreflangHelperTest} contains the unit tests for the {@link HreflangHelper} class.
 *
 * @author Giani Ifrim
 * @since 12-07-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class HreflangHelperTest {

    private static final String FULL_ROOT_URL = "https://blue.vanmarcke.com/nl_BE";
    private static final String ROOT_URL = "https://blue.vanmarcke.com/";

    @Mock
    BaseSiteService baseSiteService;

    @Mock
    SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Mock
    private CommonI18NService commonI18NService;

    @SuppressWarnings("unchecked")
    VMKModelUrlResolver<ItemModel> categoryModelUrlResolver = (VMKModelUrlResolver<ItemModel>) mock(VMKModelUrlResolver.class);

    @SuppressWarnings("unchecked")
    VMKModelUrlResolver<ItemModel> productModelUrlResolver = (VMKModelUrlResolver<ItemModel>) mock(VMKModelUrlResolver.class);

    @InjectMocks
    private HreflangHelper hreflangHelper;

    private final List<VMKModelUrlResolver<ItemModel>> resolvers = new ArrayList<>();
    private final Map<String, String> locales = new HashMap<>();

    @Before
    public void setup() {
        locales.put("nl-be", "nl_BE");
        locales.put("fr-be", "fr_BE");
        locales.put("de-be", "de_BE");
        locales.put("fr-fr", "fr_FR");
        locales.put("fr-lu", "fr_LU");
        locales.put("x-default", "nl_BE");
        hreflangHelper.setHreflangLocales(locales);

        CMSSiteModel site = mock(CMSSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(site);
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteService.getCurrentBaseSite(), true, "/")).thenReturn(FULL_ROOT_URL);
    }

    @Test
    public void testHelperForCategory() {

        CategoryModel categoryModel = mock(CategoryModel.class);

        resolvers.add(categoryModelUrlResolver);
        resolvers.add(productModelUrlResolver);
        hreflangHelper.setResolvers(resolvers);

        when(categoryModelUrlResolver.canResolve(categoryModel)).thenReturn(true);
        when(productModelUrlResolver.canResolve(categoryModel)).thenReturn(false);
        when(commonI18NService.getLocaleForIsoCode(anyString())).thenReturn(Locale.ENGLISH);
        when(categoryModelUrlResolver.resolveInternal(categoryModel, Locale.ENGLISH)).thenReturn("/category-path/c/000001");

        Map<String, String> result = hreflangHelper.generateHreflangMap(categoryModel);

        assertThat(result.size(), is(locales.size()));
        assertThat(result, IsMapContaining.hasEntry("nl-be", ROOT_URL + "nl_BE/category-path/c/000001"));
        assertThat(result, IsMapContaining.hasEntry("fr-be", ROOT_URL + "fr_BE/category-path/c/000001"));
        assertThat(result, IsMapContaining.hasEntry("de-be", ROOT_URL + "de_BE/category-path/c/000001"));
        assertThat(result, IsMapContaining.hasEntry("fr-fr", ROOT_URL + "fr_FR/category-path/c/000001"));
        assertThat(result, IsMapContaining.hasEntry("fr-lu", ROOT_URL + "fr_LU/category-path/c/000001"));
        assertThat(result, IsMapContaining.hasEntry("x-default", ROOT_URL + "nl_BE/category-path/c/000001"));
    }

    @Test
    public void testHelperForProduct() {

        ProductModel productModel = mock(ProductModel.class);

        resolvers.add(categoryModelUrlResolver);
        resolvers.add(productModelUrlResolver);
        hreflangHelper.setResolvers(resolvers);

        when(categoryModelUrlResolver.canResolve(productModel)).thenReturn(false);
        when(productModelUrlResolver.canResolve(productModel)).thenReturn(true);
        when(commonI18NService.getLocaleForIsoCode(anyString())).thenReturn(Locale.ENGLISH);
        when(productModelUrlResolver.resolveInternal(productModel, Locale.ENGLISH)).thenReturn("/category-path/product-name/p/000001");

        Map<String, String> result = hreflangHelper.generateHreflangMap(productModel);

        assertThat(result.size(), is(locales.size()));
        assertThat(result, IsMapContaining.hasEntry("nl-be", ROOT_URL + "nl_BE/category-path/product-name/p/000001"));
        assertThat(result, IsMapContaining.hasEntry("fr-be", ROOT_URL + "fr_BE/category-path/product-name/p/000001"));
        assertThat(result, IsMapContaining.hasEntry("de-be", ROOT_URL + "de_BE/category-path/product-name/p/000001"));
        assertThat(result, IsMapContaining.hasEntry("fr-fr", ROOT_URL + "fr_FR/category-path/product-name/p/000001"));
        assertThat(result, IsMapContaining.hasEntry("fr-lu", ROOT_URL + "fr_LU/category-path/product-name/p/000001"));
        assertThat(result, IsMapContaining.hasEntry("x-default", ROOT_URL + "nl_BE/category-path/product-name/p/000001"));
    }
}
