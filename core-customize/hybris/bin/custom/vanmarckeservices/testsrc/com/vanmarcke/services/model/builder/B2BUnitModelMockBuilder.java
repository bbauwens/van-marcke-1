package com.vanmarcke.services.model.builder;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code B2BUnitModelMockBuilder} class can be used to create a mock implementation of the
 * {@link B2BUnitModel} class.
 *
 * @author Christiaan Janssen
 * @since 29-04-2020
 */
public class B2BUnitModelMockBuilder {

    private final String uid;

    private AddressModel shippingAddress;
    private AddressModel billingAddress;

    /**
     * Creates a new instance of the {@link B2BUnitModelMockBuilder} class.
     *
     * @param uid the unique identifier
     */
    private B2BUnitModelMockBuilder(String uid) {
        this.uid = uid;
    }

    /**
     * The entry point to the {@link B2BUnitModelMockBuilder}.
     *
     * @param uid the unique identifier
     * @return the {@link B2BUnitModelMockBuilder} instance for method chaining
     */
    public static B2BUnitModelMockBuilder aB2BUnit(String uid) {
        return new B2BUnitModelMockBuilder(uid);
    }

    /**
     * Sets the {@code address} on the current {@link B2BUnitModelMockBuilder} instance
     *
     * @param address the shipping address
     * @return the {@link B2BUnitModelMockBuilder} instance for method chaining
     */
    public B2BUnitModelMockBuilder withShippingAddress(AddressModel address) {
        this.shippingAddress = address;
        return this;
    }

    /**
     * Sets the {@code address} on the current {@link B2BUnitModelMockBuilder} instance
     *
     * @param address the billing address
     * @return the {@link B2BUnitModelMockBuilder} instance for method chaining
     */
    public B2BUnitModelMockBuilder withBillingAddress(AddressModel address) {
        this.billingAddress = address;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link B2BUnitModel} class.
     *
     * @return the {@link B2BUnitModel} instance
     */
    public B2BUnitModel build() {
        B2BUnitModel b2bUnit = mock(B2BUnitModel.class);
        when(b2bUnit.getUid()).thenReturn(uid);
        when(b2bUnit.getShippingAddress()).thenReturn(shippingAddress);
        when(b2bUnit.getBillingAddress()).thenReturn(billingAddress);
        return b2bUnit;
    }
}
