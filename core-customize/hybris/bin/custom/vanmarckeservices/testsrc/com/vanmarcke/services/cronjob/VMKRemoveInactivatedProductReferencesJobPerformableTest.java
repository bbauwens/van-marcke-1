package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.VMKRemoveInactivatedProductReferencesCronJobModel;
import com.vanmarcke.core.references.VMKProductReferencesDao;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKRemoveInactivatedProductReferencesJobPerformableTest {

    @Mock
    private VMKProductReferencesDao vmkProductReferencesDao;
    @Mock
    private ModelService modelService;
    @InjectMocks
    private VMKRemoveInactivatedProductReferencesJobPerformable vmkRemoveInactivatedProductReferencesJobPerformable;

    @Test
    public void perform() {
        VMKRemoveInactivatedProductReferencesCronJobModel vmkRemoveInactivatedProductReferencesCronJob = mock(VMKRemoveInactivatedProductReferencesCronJobModel.class);
        ProductReferenceModel inactiveReference = mock(ProductReferenceModel.class);
        List<ProductReferenceModel> allInactiveReferences = Collections.singletonList(inactiveReference);

        when(vmkProductReferencesDao.findAllInactiveProductReferences()).thenReturn(allInactiveReferences);
        PerformResult result = vmkRemoveInactivatedProductReferencesJobPerformable.perform(vmkRemoveInactivatedProductReferencesCronJob);

        verify(vmkProductReferencesDao).findAllInactiveProductReferences();
        verify(modelService).removeAll(allInactiveReferences);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }
}