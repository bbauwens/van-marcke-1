package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.ProductImageInheritanceCronJobModel;
import com.vanmarcke.services.product.VMKProductImageInheritanceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKProductImageInheritanceJobPerformableTest {

    @Mock
    private ModelService modelService;

    @Mock
    private VMKProductImageInheritanceService productImageInheritanceService;

    @Mock
    private TimeService timeService;

    @InjectMocks
    private VMKProductImageInheritanceJobPerformable productImageInheritanceJobPerformable;

    @Test
    public void testPerform() throws Exception {
        Date currentTime = new Date();
        when(timeService.getCurrentTime()).thenReturn(currentTime);

        Date lastSuccessfulTime = mock(Date.class);
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        ProductImageInheritanceCronJobModel productImageInheritanceCronJob = mock(ProductImageInheritanceCronJobModel.class);
        when(productImageInheritanceCronJob.getCatalogVersion()).thenReturn(catalogVersion);
        when(productImageInheritanceCronJob.getLastSuccessfulTime()).thenReturn(lastSuccessfulTime);

        PerformResult result = productImageInheritanceJobPerformable.perform(productImageInheritanceCronJob);
        assertThat(result).isNotNull();
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

        verify(productImageInheritanceService).updateProductImageInheritance(catalogVersion, lastSuccessfulTime);
        verify(productImageInheritanceCronJob).setLastSuccessfulTime(currentTime);
        verify(modelService).save(productImageInheritanceCronJob);
    }

    @Test
    public void testPerform_withError() throws Exception {
        Date currentTime = new Date();
        when(timeService.getCurrentTime()).thenReturn(currentTime);

        Date lastSuccessfulTime = mock(Date.class);
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        ProductImageInheritanceCronJobModel productImageInheritanceCronJob = mock(ProductImageInheritanceCronJobModel.class);
        when(productImageInheritanceCronJob.getCatalogVersion()).thenReturn(catalogVersion);
        when(productImageInheritanceCronJob.getLastSuccessfulTime()).thenReturn(lastSuccessfulTime);

        doThrow(Exception.class).when(productImageInheritanceService).updateProductImageInheritance(catalogVersion, lastSuccessfulTime);

        PerformResult result = productImageInheritanceJobPerformable.perform(productImageInheritanceCronJob);
        assertThat(result).isNotNull();
        assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);

        verify(productImageInheritanceCronJob, never()).setLastSuccessfulTime(any());
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testPerform_withoutLastSuccessfullTime() throws Exception {
        Date currentTime = new Date();
        when(timeService.getCurrentTime()).thenReturn(currentTime);

        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        ProductImageInheritanceCronJobModel productImageInheritanceCronJob = mock(ProductImageInheritanceCronJobModel.class);
        when(productImageInheritanceCronJob.getCatalogVersion()).thenReturn(catalogVersion);

        PerformResult result = productImageInheritanceJobPerformable.perform(productImageInheritanceCronJob);
        assertThat(result).isNotNull();
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

        verify(productImageInheritanceService).updateProductImageInheritance(catalogVersion, null);
        verify(productImageInheritanceCronJob).setLastSuccessfulTime(currentTime);
        verify(modelService).save(productImageInheritanceCronJob);
    }

}