package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ProductStockStatus;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKStockStatusValueResolverTest extends AbstractValueResolverTest {

    @Mock
    private InputDocument inputDocument;
    @Mock
    private IndexerBatchContext indexerBatchContext;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private VMKVariantProductService variantProductService;

    @InjectMocks
    VMKStockStatusValueResolver vmkStockStatusValueResolver;

    @Test
    public void testValue_IN_STOCK() throws FieldValueProviderException {
        VanMarckeVariantProductModel productModel = mock(VanMarckeVariantProductModel.class);
        when(variantProductService.isNOSProduct(productModel)).thenReturn(Boolean.FALSE);
        when(variantProductService.isDiscontinuedOutOfStockProduct(productModel)).thenReturn(Boolean.FALSE);
        when(variantProductService.isSoonInStockProduct(productModel)).thenReturn(Boolean.FALSE);
        vmkStockStatusValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), productModel);
        verify(inputDocument).addField(indexedProperty, ProductStockStatus.IN_STOCK.toString(), null);
    }

    @Test
    public void testValue_NO_STOCK() throws FieldValueProviderException {
        VanMarckeVariantProductModel productModel = mock(VanMarckeVariantProductModel.class);
        when(variantProductService.isNOSProduct(productModel)).thenReturn(Boolean.TRUE);
        vmkStockStatusValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), productModel);
        verify(inputDocument).addField(indexedProperty, ProductStockStatus.NO_STOCK.toString(), null);
        verify(variantProductService, times(0)).isDiscontinuedProduct(productModel);
        verify(productModel, times(0)).getAvailabilityDate();
    }

    @Test
    public void testValue_LIMITED_STOCK() throws FieldValueProviderException {
        VanMarckeVariantProductModel productModel = mock(VanMarckeVariantProductModel.class);
        when(variantProductService.isNOSProduct(productModel)).thenReturn(Boolean.FALSE);
        when(variantProductService.isDiscontinuedOutOfStockProduct(productModel)).thenReturn(Boolean.TRUE);
        vmkStockStatusValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), productModel);
        verify(inputDocument).addField(indexedProperty, ProductStockStatus.LIMITED_STOCK.toString(), null);
        verify(productModel, times(0)).getAvailabilityDate();
    }

    @Test
    public void testValue_SOON_IN_STOCK() throws FieldValueProviderException {
        VanMarckeVariantProductModel productModel = mock(VanMarckeVariantProductModel.class);
        when(variantProductService.isNOSProduct(productModel)).thenReturn(Boolean.FALSE);
        when(variantProductService.isDiscontinuedOutOfStockProduct(productModel)).thenReturn(Boolean.FALSE);
        when(variantProductService.isSoonInStockProduct(productModel)).thenReturn(Boolean.TRUE);
        when(productModel.getAvailabilityDate()).thenReturn(null);
        vmkStockStatusValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), productModel);
        verify(inputDocument).addField(indexedProperty, ProductStockStatus.SOON_IN_STOCK.toString(), null);
        verify(variantProductService, times(1)).isDiscontinuedOutOfStockProduct(productModel);
        verify(variantProductService, times(1)).isNOSProduct(productModel);
        verify(variantProductService, times(1)).isSoonInStockProduct(productModel);
    }
}