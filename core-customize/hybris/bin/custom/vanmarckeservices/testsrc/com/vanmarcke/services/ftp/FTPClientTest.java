package com.vanmarcke.services.ftp;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class FTPClientTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private FTPClient ftpClient;

    @Mock
    private org.apache.commons.net.ftp.FTPClient ftp;

    @Before
    public void setUp() throws Exception {
        ftpClient = new FTPClient("ftp-host", "ftp-user", "ftp-pwd", "ftp-remote-path") {

            @Override
            protected org.apache.commons.net.ftp.FTPClient getFTPClient() {
                return ftp;
            }
        };
    }

    @Test
    public void testSend() throws Exception {
        when(ftp.getReplyCode()).thenReturn(200);

        InputStream inputStream = new ByteArrayInputStream("file-content".getBytes());
        ftpClient.send(inputStream, "file-name");

        verify(ftp).connect("ftp-host", 21);
        verify(ftp).login("ftp-user", "ftp-pwd");
        verify(ftp).enterLocalPassiveMode();
        verify(ftp).storeFile(eq("ftp-remote-path/file-name"), any(FileInputStream.class));
        verify(ftp).disconnect();
    }

    @Test
    public void testSend_withConnectionFailure() throws Exception {
        expectedException.expect(IOException.class);

        when(ftp.getReplyCode()).thenReturn(400);

        InputStream inputStream = new ByteArrayInputStream("file-content".getBytes());
        ftpClient.send(inputStream, "file-name");

        verify(ftp).connect("ftp-host", 21);
        verify(ftp, never()).login("ftp-user", "ftp-pwd");
        verify(ftp, never()).enterLocalPassiveMode();
        verify(ftp, never()).storeFile(any(), any());
        verify(ftp).disconnect();
    }

}