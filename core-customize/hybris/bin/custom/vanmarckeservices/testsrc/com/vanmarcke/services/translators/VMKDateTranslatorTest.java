package com.vanmarcke.services.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.impex.jalo.header.AbstractDescriptor;
import de.hybris.platform.impex.jalo.header.StandardColumnDescriptor;
import de.hybris.platform.jalo.Item;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDateTranslatorTest {

    @Mock
    private Item item;
    @Mock
    private StandardColumnDescriptor columnDescription;
    @Mock
    private AbstractDescriptor.DescriptorParams descriptionParams;

    @Spy
    private VMKDateTranslator translator;

    @Before
    public void setUp() {
        doReturn(columnDescription).when(translator).getColumnDescriptor();
        when(columnDescription.getDescriptorData()).thenReturn(descriptionParams);
    }

    @Test
    public void testImportValue_withEmptyValue() {
        Object result = translator.importValue(null, item);

        assertThat(result).isInstanceOf(String.class);
        assertThat((String) result).isEmpty();
    }

    @Test
    public void testImportValue_withDateFormatParameter() {
        when(this.descriptionParams.getModifier("dateFormat")).thenReturn("HH:mm");

        Object result = translator.importValue("8:58", item);

        assertThat(result).isInstanceOf(Date.class);
        LocalDateTime localDateTime = ((Date) result).toInstant().atZone(ZoneId.of("UTC")).toLocalDateTime();
        assertThat(localDateTime.getHour()).isEqualTo(8);
        assertThat(localDateTime.getMinute()).isEqualTo(58);
    }

    @Test
    public void testImportValue_withoutDateFormatParameter() {
        Object result = translator.importValue("2019-08-05", item);

        assertThat(result).isInstanceOf(Date.class);
        LocalDateTime localDateTime = ((Date) result).toInstant().atZone(ZoneId.of("UTC")).toLocalDateTime();
        assertThat(localDateTime.getYear()).isEqualTo(2019);
        assertThat(localDateTime.getMonth()).isEqualTo(Month.AUGUST);
        assertThat(localDateTime.getDayOfMonth()).isEqualTo(5);
        assertThat(localDateTime.getHour()).isEqualTo(0);
        assertThat(localDateTime.getMinute()).isEqualTo(0);
    }

    @Test
    public void testImportValue_withoutInvalidDateExpressionValue() {
        Object result = translator.importValue("8:58", item);

        assertThat(result).isInstanceOf(String.class);
        assertThat((String) result).isEmpty();
    }

    @Test
    public void testExportValue() {
        Date date = Date.from(LocalDateTime.of(2019, 8, 4, 20, 45, 16).toInstant(ZoneOffset.of("+02:00")));

        String result = translator.exportValue(date);
        assertThat(result).isEqualTo("Sun Aug 04 20:45:16 CEST 2019");
    }

    @Test
    public void testExportValue_withoutValue() {
        String result = translator.exportValue(null);
        assertThat(result).isEmpty();
    }

}