package com.vanmarcke.services.util;

import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ClientUtilsTest {

    private static final String HEADER_NAME = "X-FORWARDED-FOR";
    private static final String HEADER = RandomStringUtils.randomAlphabetic(10);

    @Test
    public void testGetClientIp_requestNull() {
        String result = ClientUtils.getClientIp(null);
        Assertions
                .assertThat(result)
                .isEqualTo("");
    }

    @Test
    public void testGetClientIp_HeaderNotNull() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(HEADER_NAME)).thenReturn(HEADER);

        String result = ClientUtils.getClientIp(request);
        Assertions
                .assertThat(result)
                .isEqualTo(HEADER);
    }

    @Test
    public void testGetClientIp_HeaderNull() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(HEADER_NAME)).thenReturn(null);
        when(request.getRemoteAddr()).thenReturn(HEADER);

        String result = ClientUtils.getClientIp(request);
        Assertions
                .assertThat(result)
                .isEqualTo(HEADER);

        verify(request).getHeader(HEADER_NAME);
        verify(request).getRemoteAddr();
    }

    @Test
    public void testGetClientIp_HeaderEmpty() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(HEADER_NAME)).thenReturn("");
        when(request.getRemoteAddr()).thenReturn(HEADER);

        String result = ClientUtils.getClientIp(request);
        Assertions
                .assertThat(result)
                .isEqualTo(HEADER);

        verify(request).getHeader(HEADER_NAME);
        verify(request).getRemoteAddr();
    }
}