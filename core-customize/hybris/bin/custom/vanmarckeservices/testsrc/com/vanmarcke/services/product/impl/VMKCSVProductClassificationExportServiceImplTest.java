package com.vanmarcke.services.product.impl;

import com.vanmarcke.core.constants.VanmarckeCoreConstants;
import com.vanmarcke.core.product.VMKPagedVariantProductDao;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.*;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.LocaleUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCSVProductClassificationExportServiceImplTest {

    @Mock
    private ClassificationService classificationService;
    @Mock
    private VMKPagedVariantProductDao pagedVariantProductDao;
    @Spy
    private VMKCSVProductClassificationExportServiceImpl exportService;
    @Captor
    private ArgumentCaptor<Map<Integer, String>> parametersCaptor;

    @Before
    public void setup() {
        exportService.setClassificationService(classificationService);
        exportService.setPagedVariantProductDao(pagedVariantProductDao);
    }

    @Test
    public void testGenerateProductClassificationFeed() {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final ClassificationSystemVersionModel systemVersion = mock(ClassificationSystemVersionModel.class);
        final Date lastModifiedTime = mock(Date.class);
        final SearchPageData resultPageData = mock(SearchPageData.class);
        final VariantProductModel variantProductModel = mock(VariantProductModel.class);
        final FeatureList features = mock(FeatureList.class);
        final Feature feature = mock(Feature.class);
        final ClassAttributeAssignmentModel attributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        final PaginationData paginationData = mock(PaginationData.class);
        final FeatureValue value1 = mock(FeatureValue.class);
        final FeatureValue value2 = mock(FeatureValue.class);
        final ClassificationClassModel classificationClassModel = mock(ClassificationClassModel.class);
        final ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);

        when(pagedVariantProductDao.findProductsByCatalogVersionApprovalStatusAndDate(eq(catalogVersion), eq(ArticleApprovalStatus.APPROVED), any(), any())).thenReturn(resultPageData);
        when(resultPageData.getResults()).thenReturn(singletonList(variantProductModel));
        when(variantProductModel.getCode()).thenReturn("productCode");
        when(classificationService.getFeatures(variantProductModel)).thenReturn(features);
        when(features.getFeatures()).thenReturn(singletonList(feature));

        when(feature.getClassAttributeAssignment()).thenReturn(attributeAssignmentModel);
        when(attributeAssignmentModel.getSystemVersion()).thenReturn(systemVersion);
        when(attributeAssignmentModel.getClassificationClass()).thenReturn(classificationClassModel);
        when(attributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
        when(classificationAttributeModel.getCode()).thenReturn("attributeCode");
        when(classificationClassModel.getCode()).thenReturn("classCode");

        when(feature.getValues()).thenReturn(asList(value1, value2));

        when(value1.getValue()).thenReturn(5);
        when(value2.getValue()).thenReturn(3);

        when(resultPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(1);
        doReturn("csvLines").when(exportService).buildCsvLineInternal(parametersCaptor.capture());

        exportService.generateProductClassificationFeed(catalogVersion, systemVersion, lastModifiedTime);

        int index = 1;
        for (final Map<Integer, String> fields : parametersCaptor.getAllValues()) {
            assertThat(fields).includes(entry(0, "productCode"));
            assertThat(fields).includes(entry(1, "classCode"));
            assertThat(fields).includes(entry(2, "attributeCode"));
            assertThat(fields).includes(entry(3, String.valueOf(index)));
            if (index == 1) {
                assertThat(fields).includes(entry(4, "5"));
            } else {
                assertThat(fields).includes(entry(4, "3"));
            }
            index++;
        }
    }

    @Test
    public void testGenerateProductClassificationFeed_withComposedFeatureCode() {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final ClassificationSystemVersionModel systemVersion = mock(ClassificationSystemVersionModel.class);
        final Date lastModifiedTime = mock(Date.class);
        final SearchPageData resultPageData = mock(SearchPageData.class);
        final VariantProductModel variantProductModel = mock(VariantProductModel.class);
        final FeatureList features = mock(FeatureList.class);
        final Feature feature = mock(Feature.class);
        final ClassAttributeAssignmentModel attributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        final PaginationData paginationData = mock(PaginationData.class);
        final FeatureValue value1 = mock(FeatureValue.class);
        final FeatureValue value2 = mock(FeatureValue.class);
        final ClassificationClassModel classificationClassModel = mock(ClassificationClassModel.class);
        final ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);

        when(pagedVariantProductDao.findProductsByCatalogVersionApprovalStatusAndDate(eq(catalogVersion), eq(ArticleApprovalStatus.APPROVED), any(), any())).thenReturn(resultPageData);
        when(resultPageData.getResults()).thenReturn(singletonList(variantProductModel));
        when(variantProductModel.getCode()).thenReturn("productCode");
        when(classificationService.getFeatures(variantProductModel)).thenReturn(features);
        when(features.getFeatures()).thenReturn(singletonList(feature));

        when(feature.getClassAttributeAssignment()).thenReturn(attributeAssignmentModel);
        when(attributeAssignmentModel.getSystemVersion()).thenReturn(systemVersion);
        when(attributeAssignmentModel.getClassificationClass()).thenReturn(classificationClassModel);
        when(attributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
        when(classificationAttributeModel.getCode()).thenReturn("classCode_attributeCode");
        when(classificationClassModel.getCode()).thenReturn("classCode");

        when(feature.getValues()).thenReturn(asList(value1, value2));

        when(value1.getValue()).thenReturn(5);
        when(value2.getValue()).thenReturn(3);

        when(resultPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(1);
        doReturn("csvLines").when(exportService).buildCsvLineInternal(parametersCaptor.capture());

        exportService.generateProductClassificationFeed(catalogVersion, systemVersion, lastModifiedTime);

        int index = 1;
        for (final Map<Integer, String> fields : parametersCaptor.getAllValues()) {
            assertThat(fields).includes(entry(0, "productCode"));
            assertThat(fields).includes(entry(1, "classCode"));
            assertThat(fields).includes(entry(2, "attributeCode"));
            assertThat(fields).includes(entry(3, String.valueOf(index)));
            if (index == 1) {
                assertThat(fields).includes(entry(4, "5"));
            } else {
                assertThat(fields).includes(entry(4, "3"));
            }
            index++;
        }
    }

    @Test
    public void testCreateFullEtimProductExportLines_noFeatures() {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final ClassificationSystemVersionModel systemVersion = mock(ClassificationSystemVersionModel.class);
        final ClassificationSystemModel classificationSystemModel = mock(ClassificationSystemModel.class);
        final SearchPageData resultPageData = mock(SearchPageData.class);
        final VariantProductModel variantProductModel = mock(VariantProductModel.class);
        final FeatureList features = mock(FeatureList.class);
        final Feature feature = mock(Feature.class);
        final ClassAttributeAssignmentModel attributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        final PaginationData paginationData = mock(PaginationData.class);
        final FeatureValue value1 = mock(FeatureValue.class);
        final FeatureValue value2 = mock(FeatureValue.class);
        final ClassificationClassModel classificationClassModel = mock(ClassificationClassModel.class);
        final ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);

        final Locale localeNL = LocaleUtils.toLocale(VanmarckeCoreConstants.Languages.ISO_CODE_NL);
        final Locale localeFR = LocaleUtils.toLocale(VanmarckeCoreConstants.Languages.ISO_CODE_FR);

        when(pagedVariantProductDao.findProductsByCatalogVersionAndApprovalStatus(eq(catalogVersion), eq(ArticleApprovalStatus.APPROVED), any())).thenReturn(resultPageData);
        when(resultPageData.getResults()).thenReturn(singletonList(variantProductModel));
        when(variantProductModel.getCode()).thenReturn("productCode");
        when(variantProductModel.getAs400_ShortDescription(localeNL)).thenReturn("desc_nl");
        when(variantProductModel.getAs400_ShortDescription(localeFR)).thenReturn("desc_fr");
        when(variantProductModel.getCode()).thenReturn("productCode");
        when(classificationService.getFeatures(variantProductModel)).thenReturn(features);
        when(features.getFeatures()).thenReturn(singletonList(feature));

        when(feature.getClassAttributeAssignment()).thenReturn(attributeAssignmentModel);
        when(attributeAssignmentModel.getSystemVersion()).thenReturn(systemVersion);
        when(systemVersion.getCatalog()).thenReturn(classificationSystemModel);
        when(classificationSystemModel.getId()).thenReturn(VanmarckeCoreConstants.ETIM_CLASSIFICATION);

        when(attributeAssignmentModel.getClassificationClass()).thenReturn(classificationClassModel);
        when(attributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
        when(classificationAttributeModel.getCode()).thenReturn("classCode_attributeCode");
        when(classificationAttributeModel.getName(localeNL)).thenReturn("className_attributeName_nl");
        when(classificationAttributeModel.getName(localeFR)).thenReturn("className_attributeName_fr");
        when(classificationClassModel.getCode()).thenReturn("classCode");
        when(classificationClassModel.getName(localeNL)).thenReturn("className_nl");
        when(classificationClassModel.getName(localeFR)).thenReturn("className_fr");

        when(feature.getValues()).thenReturn(EMPTY_LIST);

        when(resultPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(1);
        doReturn("csvLines").when(exportService).buildCsvLineInternal(parametersCaptor.capture());

        exportService.createFullEtimProductExportLines(catalogVersion);

        for (final Map<Integer, String> fields : parametersCaptor.getAllValues()) {
            assertThat(fields).includes(entry(0, "productCode"));
            assertThat(fields).includes(entry(1, "desc_nl"));
            assertThat(fields).includes(entry(2, "desc_fr"));
            assertThat(fields).includes(entry(3, "classCode"));
            assertThat(fields).includes(entry(4, "className_nl"));
            assertThat(fields).includes(entry(5, "className_fr"));
            assertThat(fields).includes(entry(6, "classCode_attributeCode"));
            assertThat(fields).includes(entry(7, "className_attributeName_nl"));
            assertThat(fields).includes(entry(8, "className_attributeName_fr"));
        }
    }

    @Test
    public void testCreateFullEtimProductExportLines_withFeatures() {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final ClassificationSystemVersionModel systemVersion = mock(ClassificationSystemVersionModel.class);
        final ClassificationSystemModel classificationSystemModel = mock(ClassificationSystemModel.class);
        final SearchPageData resultPageData = mock(SearchPageData.class);
        final VariantProductModel variantProductModel = mock(VariantProductModel.class);
        final FeatureList features = mock(FeatureList.class);
        final Feature feature = mock(Feature.class);
        final ClassAttributeAssignmentModel attributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        final PaginationData paginationData = mock(PaginationData.class);
        final FeatureValue value1 = mock(FeatureValue.class);
        final FeatureValue value2 = mock(FeatureValue.class);
        final ClassificationClassModel classificationClassModel = mock(ClassificationClassModel.class);
        final ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);

        final Locale localeNL = LocaleUtils.toLocale(VanmarckeCoreConstants.Languages.ISO_CODE_NL);
        final Locale localeFR = LocaleUtils.toLocale(VanmarckeCoreConstants.Languages.ISO_CODE_FR);

        when(pagedVariantProductDao.findProductsByCatalogVersionAndApprovalStatus(eq(catalogVersion), eq(ArticleApprovalStatus.APPROVED), any())).thenReturn(resultPageData);
        when(resultPageData.getResults()).thenReturn(singletonList(variantProductModel));
        when(variantProductModel.getCode()).thenReturn("productCode");
        when(variantProductModel.getAs400_ShortDescription(localeNL)).thenReturn("desc_nl");
        when(variantProductModel.getAs400_ShortDescription(localeFR)).thenReturn("desc_fr");
        when(variantProductModel.getCode()).thenReturn("productCode");
        when(classificationService.getFeatures(variantProductModel)).thenReturn(features);
        when(features.getFeatures()).thenReturn(singletonList(feature));

        when(feature.getClassAttributeAssignment()).thenReturn(attributeAssignmentModel);
        when(attributeAssignmentModel.getSystemVersion()).thenReturn(systemVersion);
        when(systemVersion.getCatalog()).thenReturn(classificationSystemModel);
        when(classificationSystemModel.getId()).thenReturn(VanmarckeCoreConstants.ETIM_CLASSIFICATION);

        when(attributeAssignmentModel.getClassificationClass()).thenReturn(classificationClassModel);
        when(attributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
        when(classificationAttributeModel.getCode()).thenReturn("classCode_attributeCode");
        when(classificationAttributeModel.getName(localeNL)).thenReturn("className_attributeName_nl");
        when(classificationAttributeModel.getName(localeFR)).thenReturn("className_attributeName_fr");
        when(classificationClassModel.getCode()).thenReturn("classCode");
        when(classificationClassModel.getName(localeNL)).thenReturn("className_nl");
        when(classificationClassModel.getName(localeFR)).thenReturn("className_fr");

        when(feature.getValues()).thenReturn(asList(value1, value2));

        when(value1.getValue()).thenReturn(100);
        when(value2.getValue()).thenReturn(Boolean.TRUE);

        when(resultPageData.getPagination()).thenReturn(paginationData);
        when(paginationData.getNumberOfPages()).thenReturn(1);
        doReturn("csvLines").when(exportService).buildCsvLineInternal(parametersCaptor.capture());

        exportService.createFullEtimProductExportLines(catalogVersion);

        int index=1;
        for (final Map<Integer, String> fields : parametersCaptor.getAllValues()) {
            assertThat(fields).includes(entry(0, "productCode"));
            assertThat(fields).includes(entry(1, "desc_nl"));
            assertThat(fields).includes(entry(2, "desc_fr"));
            assertThat(fields).includes(entry(3, "classCode"));
            assertThat(fields).includes(entry(4, "className_nl"));
            assertThat(fields).includes(entry(5, "className_fr"));
            assertThat(fields).includes(entry(6, "classCode_attributeCode"));
            assertThat(fields).includes(entry(7, "className_attributeName_nl"));
            assertThat(fields).includes(entry(8, "className_attributeName_fr"));

            if (index == 1) {
                assertThat(fields).includes(entry(9, String.valueOf(index)));
                assertThat(fields).includes(entry(10, "100"));
            } else {
                assertThat(fields).includes(entry(9, String.valueOf(index)));
                assertThat(fields).includes(entry(10, "Y"));
            }
            index++;
        }
    }
}
