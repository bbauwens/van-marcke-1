package com.vanmarcke.services.builder;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.classification.features.UnlocalizedFeature;

public class FeatureBuilder {

    private FeatureBuilder() {
    }

    public static FeatureBuilder aFeature() {
        return new FeatureBuilder();
    }

    public Feature build(ClassAttributeAssignmentModel classAttributeAssignmentModel, FeatureValue featureValue) {
        return new UnlocalizedFeature(classAttributeAssignmentModel, featureValue);
    }
}
