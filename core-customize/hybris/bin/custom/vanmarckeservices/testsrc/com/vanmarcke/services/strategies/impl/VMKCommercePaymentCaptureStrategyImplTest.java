package com.vanmarcke.services.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;

import static de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
import static de.hybris.platform.payment.dto.TransactionStatus.REVIEW;
import static de.hybris.platform.payment.enums.PaymentTransactionType.AUTHORIZATION;
import static de.hybris.platform.payment.enums.PaymentTransactionType.CAPTURE;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommercePaymentCaptureStrategyImplTest {

    @Mock
    private ModelService modelService;
    @Mock
    private PaymentService paymentService;
    @InjectMocks
    private VMKCommercePaymentCaptureStrategyImpl defaultVMKCommercePaymentCaptureStrategy;

    @Test
    public void testCapturePaymentAmount_withoutAuthorization() {
        CartModel cartModel = mock(CartModel.class);
        PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        when(paymentTransactionModel.getEntries()).thenReturn(singletonList(paymentTransactionEntryModel));

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        PaymentTransactionEntryModel result = defaultVMKCommercePaymentCaptureStrategy.capturePaymentAmount(cartModel);
        assertThat(result).isNull();

        verifyZeroInteractions(modelService);
        verifyZeroInteractions(paymentService);
    }

    @Test
    public void testCapturePaymentAmount_withInvoice() {
        CartModel cartModel = mock(CartModel.class);
        InvoicePaymentInfoModel paymentInfoModel = mock(InvoicePaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        PaymentTransactionEntryModel entryModel = mock(PaymentTransactionEntryModel.class);
        CurrencyModel currencyModel = mock(CurrencyModel.class);

        when(paymentTransactionEntryModel.getType()).thenReturn(AUTHORIZATION);
        when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(ACCEPTED.name());
        when(paymentTransactionEntryModel.getPaymentTransaction()).thenReturn(paymentTransactionModel);
        when(paymentTransactionEntryModel.getAmount()).thenReturn(BigDecimal.TEN);
        when(paymentTransactionEntryModel.getCurrency()).thenReturn(currencyModel);

        when(paymentTransactionModel.getEntries()).thenReturn(singletonList(paymentTransactionEntryModel));

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        when(paymentService.getNewPaymentTransactionEntryCode(paymentTransactionModel, CAPTURE)).thenReturn("entry-code");

        when(modelService.create(PaymentTransactionEntryModel.class)).thenReturn(entryModel);

        PaymentTransactionEntryModel result = defaultVMKCommercePaymentCaptureStrategy.capturePaymentAmount(cartModel);
        assertThat(result).isEqualTo(entryModel);

        verify(entryModel).setType(CAPTURE);
        verify(entryModel).setCode("entry-code");
        verify(entryModel).setAmount(BigDecimal.TEN);
        verify(entryModel).setCurrency(currencyModel);
        verify(entryModel).setTime(any(Date.class));
        verify(entryModel).setPaymentTransaction(paymentTransactionModel);
        verify(entryModel).setTransactionStatus("ACCEPTED");
        verify(entryModel).setTransactionStatusDetails("SUCCESFULL");

        verify(modelService).save(entryModel);
        verify(modelService).refresh(paymentTransactionModel);

        verify(paymentService, never()).capture(any(PaymentTransactionModel.class));
    }

    @Test
    public void testCapturePaymentAmount_withCard() {
        CartModel cartModel = mock(CartModel.class);
        PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        PaymentTransactionEntryModel entryModel = mock(PaymentTransactionEntryModel.class);

        when(paymentTransactionEntryModel.getType()).thenReturn(AUTHORIZATION);
        when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(ACCEPTED.name());
        when(paymentTransactionEntryModel.getPaymentTransaction()).thenReturn(paymentTransactionModel);

        when(paymentTransactionModel.getEntries()).thenReturn(singletonList(paymentTransactionEntryModel));

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        when(paymentService.capture(paymentTransactionModel)).thenReturn(entryModel);

        PaymentTransactionEntryModel result = defaultVMKCommercePaymentCaptureStrategy.capturePaymentAmount(cartModel);
        assertThat(result).isEqualTo(entryModel);

        verify(modelService, never()).create(PaymentTransactionEntryModel.class);
        verify(modelService, never()).save(entryModel);
        verify(modelService).refresh(paymentTransactionModel);

        verify(paymentService, never()).getNewPaymentTransactionEntryCode(paymentTransactionModel, CAPTURE);
    }

    @Test
    public void testCapturePaymentAmount_withCardAndCaptured() {
        CartModel cartModel = mock(CartModel.class);
        PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        when(paymentTransactionEntryModel.getType()).thenReturn(CAPTURE);
        when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(ACCEPTED.name());
        when(paymentTransactionEntryModel.getPaymentTransaction()).thenReturn(paymentTransactionModel);

        when(paymentTransactionModel.getEntries()).thenReturn(singletonList(paymentTransactionEntryModel));

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        PaymentTransactionEntryModel result = defaultVMKCommercePaymentCaptureStrategy.capturePaymentAmount(cartModel);
        assertThat(result).isEqualTo(paymentTransactionEntryModel);

        verifyZeroInteractions(modelService);

        verifyZeroInteractions(paymentService);
    }

    @Test
    public void testCapturePaymentAmount_withCardAndPending() {
        CartModel cartModel = mock(CartModel.class);
        PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        when(paymentTransactionEntryModel.getType()).thenReturn(CAPTURE);
        when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(REVIEW.name());
        when(paymentTransactionEntryModel.getPaymentTransaction()).thenReturn(paymentTransactionModel);

        when(paymentTransactionModel.getEntries()).thenReturn(singletonList(paymentTransactionEntryModel));

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        PaymentTransactionEntryModel result = defaultVMKCommercePaymentCaptureStrategy.capturePaymentAmount(cartModel);
        assertThat(result).isEqualTo(paymentTransactionEntryModel);

        verifyZeroInteractions(modelService);

        verifyZeroInteractions(paymentService);
    }

    @Test
    public void testCapturePaymentAmount_withCardAndAdaptedException() {
        CartModel cartModel = mock(CartModel.class);
        PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        when(paymentTransactionEntryModel.getType()).thenReturn(AUTHORIZATION);
        when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(ACCEPTED.name());
        when(paymentTransactionEntryModel.getPaymentTransaction()).thenReturn(paymentTransactionModel);

        when(paymentTransactionModel.getEntries()).thenReturn(singletonList(paymentTransactionEntryModel));

        when(cartModel.getPaymentTransactions()).thenReturn(singletonList(paymentTransactionModel));
        when(cartModel.getPaymentInfo()).thenReturn(paymentInfoModel);

        when(paymentService.capture(paymentTransactionModel)).thenThrow(AdapterException.class);

        PaymentTransactionEntryModel result = defaultVMKCommercePaymentCaptureStrategy.capturePaymentAmount(cartModel);
        assertThat(result).isNull();

        verifyZeroInteractions(modelService);
    }

    @Test
    public void testIsPaymentCapturedForOrder() {
        final OrderModel order = mock(OrderModel.class);
        PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntry = mock(PaymentTransactionEntryModel.class);

        when(order.getPaymentTransactions()).thenReturn(singletonList(paymentTransaction));
        when(paymentTransaction.getEntries()).thenReturn(singletonList(paymentTransactionEntry));
        when(paymentTransactionEntry.getType()).thenReturn(CAPTURE);
        when(paymentTransactionEntry.getTransactionStatus()).thenReturn(ACCEPTED.toString());
        boolean result = defaultVMKCommercePaymentCaptureStrategy.isPaymentCapturedForOrder(order);

        assertThat(result).isEqualTo(Boolean.TRUE);
    }

    @Test
    public void testIsPaymentCapturedForOrder_NotFullyCaptured() {
        final OrderModel order = mock(OrderModel.class);
        PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntry = mock(PaymentTransactionEntryModel.class);

        when(order.getPaymentTransactions()).thenReturn(singletonList(paymentTransaction));
        when(paymentTransaction.getEntries()).thenReturn(singletonList(paymentTransactionEntry));
        when(paymentTransactionEntry.getType()).thenReturn(PaymentTransactionType.PARTIAL_CAPTURE);
        when(paymentTransactionEntry.getTransactionStatus()).thenReturn(ACCEPTED.toString());
        boolean result = defaultVMKCommercePaymentCaptureStrategy.isPaymentCapturedForOrder(order);

        assertThat(result).isEqualTo(Boolean.FALSE);
    }
}