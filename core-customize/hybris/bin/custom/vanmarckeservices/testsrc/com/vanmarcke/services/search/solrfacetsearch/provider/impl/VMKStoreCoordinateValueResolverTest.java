package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKStoreCoordinateValueResolverTest extends AbstractValueResolverTest {

    @InjectMocks
    VMKStoreCoordinateValueResolver storeCoordinateValueResolver;
    @Mock
    private InputDocument inputDocument;
    @Mock
    private IndexerBatchContext indexerBatchContext;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private PointOfServiceModel posModel;
    @Mock
    private ModelService modelService;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test_latitudeHasValue() throws FieldValueProviderException {
        when(indexedProperty.getName()).thenReturn("latitude");
        when(modelService.getAttributeValue(posModel, "latitude")).thenReturn(5d);
        storeCoordinateValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);
        verify(inputDocument).addField(indexedProperty, 5.0, null);
    }

    @Test
    public void test_longitudeHasValue() throws FieldValueProviderException {
        when(indexedProperty.getName()).thenReturn("longitude");
        when(modelService.getAttributeValue(posModel, "longitude")).thenReturn(5d);
        storeCoordinateValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);
        verify(inputDocument).addField(indexedProperty, 5.0, null);
    }

    @Test
    public void test_latitudeIsNull() throws FieldValueProviderException {
        when(indexedProperty.getName()).thenReturn("latitude");
        when(modelService.getAttributeValue(posModel, "latitude")).thenReturn(null);
        storeCoordinateValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);
        verify(inputDocument).addField(indexedProperty, 0.0, null);
    }

    @Test
    public void test_longitudeIsNull() throws FieldValueProviderException {
        when(indexedProperty.getName()).thenReturn("longitude");
        when(modelService.getAttributeValue(posModel, "longitude")).thenReturn(null);
        storeCoordinateValueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), posModel);
        verify(inputDocument).addField(indexedProperty, 0.0, null);
    }
}
