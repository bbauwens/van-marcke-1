package com.vanmarcke.services.product.impl;

import com.vanmarcke.facades.product.data.BrandData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * The {@link VMKProductStructuredDataServiceTest} class contains the unit test for the
 * {@link VMKProductStructuredDataService} class.
 *
 * @author Giani Ifrim
 * @since 27-07-2021
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductStructuredDataServiceTest {

    private static final String BRAND_NAME = "brandNameValue";
    private static final String CATEGORY = "categoryValue";
    private static final String GTIN = "gtinValue";
    private static final String MPN = "mpnValue";
    private static final String PRODUCT_ID = "productIdValue";
    private static final String SKU = "skuValue";
    private static final String DESC = "descValue";
    private static final String ID = "idValue";
    private static final String NAME = "nameValue";
    private static final String URL = "/p/000000";
    private static final String FULL_URL = "https://blue.vanmarcke.com/nl_BE" + URL;
    private static final String IMG_URL = "/img1.jpg";
    private static final String AGGREGATE_RATING = "aggregateRating";

    @Mock
    BaseSiteService baseSiteService;

    @InjectMocks
    VMKProductStructuredDataService vmkProductStructuredDataService;

    @Mock
    SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    ProductData productData = new ProductData();

    @Before
    public void setUp() {
        BrandData brandData = mock(BrandData.class);
        CMSSiteModel site = mock(CMSSiteModel.class);

        when(brandData.getName()).thenReturn(BRAND_NAME);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(site);
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteService.getCurrentBaseSite(), true, URL)).thenReturn(FULL_URL);

        productData.setBrand(brandData);
        productData.setSuperCategory(CATEGORY);
        productData.setGtin(GTIN);
        productData.setMpn(MPN);
        productData.setProductId(PRODUCT_ID);
        productData.setCode(SKU);
        productData.setSummary(DESC);
        productData.setIdentifier(ID);
        productData.setName(NAME);
        productData.setUrl(URL);
        productData.setThumbnail(IMG_URL);

    }

    @Test
    public void createJsonFromProductWithAllAttributes() {
        String result = vmkProductStructuredDataService.createStructuredDataJson(productData);

        assertThat(result, containsString(BRAND_NAME));
        assertThat(result, containsString(CATEGORY));
        assertThat(result, containsString(GTIN));
        assertThat(result, containsString(MPN));
        assertThat(result, containsString(PRODUCT_ID));
        assertThat(result, containsString(SKU));
        assertThat(result, containsString(DESC));
        assertThat(result, containsString(ID));
        assertThat(result, containsString(NAME));
        assertThat(result, containsString(FULL_URL));
        assertThat(result, containsString(IMG_URL));
        assertThat(result, containsString(AGGREGATE_RATING));
    }

    @Test
    public void createJsonFromProductWithEmptyAttributes() {
        productData.setCode(null);
        productData.setSummary("");
        productData.setIdentifier("");

        String result = vmkProductStructuredDataService.createStructuredDataJson(productData);

        assertThat(result, containsString(BRAND_NAME));
        assertThat(result, containsString(CATEGORY));
        assertThat(result, containsString(GTIN));
        assertThat(result, containsString(MPN));
        assertThat(result, containsString(PRODUCT_ID));
        assertThat(result, containsString(NAME));
        assertThat(result, containsString(FULL_URL));
        assertThat(result, containsString(IMG_URL));
        assertThat(result, containsString(AGGREGATE_RATING));

        assertThat(result, not(containsString(SKU)));
        assertThat(result, not(containsString(DESC)));
        assertThat(result, not(containsString(ID)));
    }
}
