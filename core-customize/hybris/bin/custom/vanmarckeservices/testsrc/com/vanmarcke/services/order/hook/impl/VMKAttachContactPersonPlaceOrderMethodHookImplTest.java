package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.core.model.VanmarckeContactModel;
import com.vanmarcke.core.user.VMKContactPersonDao;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAttachContactPersonPlaceOrderMethodHookImplTest {

    @Mock
    private VMKContactPersonDao contactPersonDao;
    @Mock
    private ModelService modelService;
    @InjectMocks
    private VMKAttachContactPersonPlaceOrderMethodHookImpl defaultVMKAttachReturnContactPlaceOrderMethodHook;

    @Test
    public void testBeforeSubmitOrder_withPostalCodeAndCountry() throws Exception {
        CommerceCheckoutParameter parameter = mock(CommerceCheckoutParameter.class);
        CommerceOrderResult result = mock(CommerceOrderResult.class);
        OrderModel order = mock(OrderModel.class);
        AddressModel billingAddress = mock(AddressModel.class);
        CountryModel country = mock(CountryModel.class);
        VanmarckeContactModel returnContact = mock(VanmarckeContactModel.class);

        when(result.getOrder()).thenReturn(order);
        when(order.getPaymentAddress()).thenReturn(billingAddress);
        when(billingAddress.getPostalcode()).thenReturn("3500");
        when(billingAddress.getCountry()).thenReturn(country);
        when(contactPersonDao.findContactPersonForCountryAndPostalcode(country, "3500")).thenReturn(returnContact);
        defaultVMKAttachReturnContactPlaceOrderMethodHook.beforeSubmitOrder(parameter, result);

        verify(order).setContactPerson(returnContact);
        verify(modelService).save(order);
        verify(contactPersonDao).findContactPersonForCountryAndPostalcode(country, "3500");
    }

    @Test
    public void testBeforeSubmitOrder_withOnlyCountry() throws Exception {
        CommerceCheckoutParameter parameter = mock(CommerceCheckoutParameter.class);
        CommerceOrderResult result = mock(CommerceOrderResult.class);
        OrderModel order = mock(OrderModel.class);
        AddressModel billingAddress = mock(AddressModel.class);
        CountryModel country = mock(CountryModel.class);
        VanmarckeContactModel returnContact = mock(VanmarckeContactModel.class);

        when(result.getOrder()).thenReturn(order);
        when(order.getPaymentAddress()).thenReturn(billingAddress);
        when(billingAddress.getCountry()).thenReturn(country);
        when(country.getDefaultReturnContact()).thenReturn(returnContact);
        defaultVMKAttachReturnContactPlaceOrderMethodHook.beforeSubmitOrder(parameter, result);

        verify(order).setContactPerson(returnContact);
        verify(modelService).save(order);
        verifyZeroInteractions(contactPersonDao);
    }

    @Test
    public void testBeforeSubmitOrder_withoutCountry() throws Exception {
        CommerceCheckoutParameter parameter = mock(CommerceCheckoutParameter.class);
        CommerceOrderResult result = mock(CommerceOrderResult.class);
        OrderModel order = mock(OrderModel.class);
        AddressModel billingAddress = mock(AddressModel.class);

        when(result.getOrder()).thenReturn(order);
        when(order.getPaymentAddress()).thenReturn(billingAddress);
        defaultVMKAttachReturnContactPlaceOrderMethodHook.beforeSubmitOrder(parameter, result);

        verify(order).setContactPerson(null);
        verify(modelService).save(order);
        verifyZeroInteractions(contactPersonDao);
    }
}