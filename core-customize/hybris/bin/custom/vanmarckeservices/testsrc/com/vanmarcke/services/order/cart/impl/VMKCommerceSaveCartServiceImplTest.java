package com.vanmarcke.services.order.cart.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.*;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceSaveCartServiceImplTest {

    @Mock
    private CommerceSaveCartStrategy commerceSaveCartStrategy;
    @Mock
    private CommerceCloneSavedCartStrategy commerceCloneSavedCartStrategy;
    @Mock
    private CommerceCartRestorationStrategy commerceCartRestorationStrategy;
    @InjectMocks
    private VMKCommerceSaveCartServiceImpl defaultVMKCommerceSaveCartService;

    @Captor
    private ArgumentCaptor<CommerceCartParameter> commerceCartParameterArgumentCaptor;

    @Test
    public void testSaveCart_currentSessionCart() throws Exception {
        final CommerceSaveCartResult saveCartResult = mock(CommerceSaveCartResult.class);
        final CommerceSaveCartResult clonedCartResult = mock(CommerceSaveCartResult.class);
        final CartModel cartModel = mock(CartModel.class);
        final CartModel clonedCartModel = mock(CartModel.class);

        when(clonedCartResult.getSavedCart()).thenReturn(clonedCartModel);

        final CommerceSaveCartParameter parameter = new CommerceSaveCartParameter();
        parameter.setEnableHooks(true);
        parameter.setCart(cartModel);

        when(this.commerceSaveCartStrategy.saveCart(parameter)).thenReturn(saveCartResult);
        when(cartModel.getSavedBy()).thenReturn(null);
        when(cartModel.getSaveTime()).thenReturn(null);
        when(this.commerceCloneSavedCartStrategy.cloneSavedCart(parameter)).thenReturn(clonedCartResult);
        when(this.commerceCartRestorationStrategy.restoreCart(this.commerceCartParameterArgumentCaptor.capture())).thenReturn(any(CommerceCartRestoration.class));
        final CommerceSaveCartResult result = this.defaultVMKCommerceSaveCartService.saveCart(parameter);
        assertThat(result).isEqualTo(saveCartResult);

        assertThat(this.commerceCartParameterArgumentCaptor.getValue().getCart()).isEqualTo(clonedCartModel);
        assertThat(this.commerceCartParameterArgumentCaptor.getValue().isEnableHooks()).isTrue();
    }

    @Test
    public void testSaveCart_editSavedCart() throws Exception {
        final CommerceSaveCartResult saveCartResult = mock(CommerceSaveCartResult.class);
        final CartModel cartModel = mock(CartModel.class);

        final CommerceSaveCartParameter parameter = new CommerceSaveCartParameter();
        parameter.setCart(cartModel);

        when(this.commerceSaveCartStrategy.saveCart(parameter)).thenReturn(saveCartResult);
        when(cartModel.getSavedBy()).thenReturn(mock(UserModel.class));
        when(cartModel.getSaveTime()).thenReturn(mock(Date.class));
        final CommerceSaveCartResult result = this.defaultVMKCommerceSaveCartService.saveCart(parameter);
        assertThat(result).isEqualTo(saveCartResult);

        verifyZeroInteractions(this.commerceCloneSavedCartStrategy);
    }

    @Test
    public void testRestoreSavedCart() throws Exception {
        final CommerceSaveCartResult clonedCartResult = mock(CommerceSaveCartResult.class);
        final CartModel clonedCartModel = mock(CartModel.class);
        final CommerceCartRestoration commerceCartRestoration = mock(CommerceCartRestoration.class);

        when(clonedCartResult.getSavedCart()).thenReturn(clonedCartModel);

        final CommerceSaveCartParameter parameter = new CommerceSaveCartParameter();
        parameter.setEnableHooks(true);

        when(this.commerceCloneSavedCartStrategy.cloneSavedCart(parameter)).thenReturn(clonedCartResult);

        when(this.commerceCartRestorationStrategy.restoreCart(this.commerceCartParameterArgumentCaptor.capture())).thenReturn(commerceCartRestoration);

        final CommerceCartRestoration result = this.defaultVMKCommerceSaveCartService.restoreSavedCart(parameter);
        assertThat(result).isEqualTo(commerceCartRestoration);

        assertThat(this.commerceCartParameterArgumentCaptor.getValue().getCart()).isEqualTo(clonedCartModel);
        assertThat(this.commerceCartParameterArgumentCaptor.getValue().isEnableHooks()).isTrue();
    }

    @Test(expected = CommerceSaveCartException.class)
    public void testRestoreSavedCart_withCommerceSaveCartException() throws Exception {
        final CommerceSaveCartParameter parameter = mock(CommerceSaveCartParameter.class);
        final CommerceSaveCartResult clonedCartResult = mock(CommerceSaveCartResult.class);
        final CartModel clonedCartModel = mock(CartModel.class);

        when(clonedCartResult.getSavedCart()).thenReturn(clonedCartModel);

        when(this.commerceCloneSavedCartStrategy.cloneSavedCart(parameter)).thenReturn(clonedCartResult);

        when(this.commerceCartRestorationStrategy.restoreCart(any(CommerceCartParameter.class))).thenThrow(CommerceCartRestorationException.class);

        this.defaultVMKCommerceSaveCartService.restoreSavedCart(parameter);
    }

}