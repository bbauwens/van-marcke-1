package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.elision.hybris.l10n.model.LocalizedMessageModel;
import com.elision.hybris.l10n.services.LocalizedMessageService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKEDCStockIndicatorFacetValueDisplayNameProviderTest {

    @Mock
    private SearchQuery searchQuery;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private LocalizedMessageService localizedMessageService;

    private static final String EDC_STOCK_VALUE_IN_STOCK = "facet.edc.stock.value.in.stock";
    private static final String EDC_STOCK_VALUE_NO_STOCK = "facet.edc.stock.value.no.stock";
    private static final String EDC_STOCK_VALUE_NOS = "facet.edc.stock.value.nos.product";

    @InjectMocks
    private VMKEDCStockIndicatorFacetValueDisplayNameProvider vmkEDCStockIndicatorFacetValueDisplayNameProvider;

    @Test
    public void testGetDisplayName_Stock() {
        String originalValue = "STOCK";
        LocalizedMessageModel localizedMessage = mock(LocalizedMessageModel.class);

        when(localizedMessageService.getLocalizedMessageForKey(EDC_STOCK_VALUE_IN_STOCK)).thenReturn(localizedMessage);
        when(localizedMessage.getMessage()).thenReturn("In stock");
        String result = vmkEDCStockIndicatorFacetValueDisplayNameProvider.getDisplayName(searchQuery, indexedProperty, originalValue);

        assertThat(result).isEqualTo("In stock");
        verify(localizedMessageService).getLocalizedMessageForKey(EDC_STOCK_VALUE_IN_STOCK);
        verify(localizedMessageService, never()).getLocalizedMessageForKey(EDC_STOCK_VALUE_NO_STOCK);
        verify(localizedMessageService, never()).getLocalizedMessageForKey(EDC_STOCK_VALUE_NOS);
    }

    @Test
    public void testGetDisplayName_OutOfStock() {
        String originalValue = "NOSTOCK";
        LocalizedMessageModel localizedMessage = mock(LocalizedMessageModel.class);

        when(localizedMessageService.getLocalizedMessageForKey(EDC_STOCK_VALUE_NO_STOCK)).thenReturn(localizedMessage);
        when(localizedMessage.getMessage()).thenReturn("No stock");
        String result = vmkEDCStockIndicatorFacetValueDisplayNameProvider.getDisplayName(searchQuery, indexedProperty, originalValue);

        assertThat(result).isEqualTo("No stock");
        verify(localizedMessageService, never()).getLocalizedMessageForKey(EDC_STOCK_VALUE_IN_STOCK);
        verify(localizedMessageService).getLocalizedMessageForKey(EDC_STOCK_VALUE_NO_STOCK);
        verify(localizedMessageService, never()).getLocalizedMessageForKey(EDC_STOCK_VALUE_NOS);
    }

    @Test
    public void testGetDisplayName_NOSProduct() {
        String originalValue = "NOSPRODUCT";
        LocalizedMessageModel localizedMessage = mock(LocalizedMessageModel.class);

        when(localizedMessageService.getLocalizedMessageForKey(EDC_STOCK_VALUE_NOS)).thenReturn(localizedMessage);
        when(localizedMessage.getMessage()).thenReturn("On order");
        String result = vmkEDCStockIndicatorFacetValueDisplayNameProvider.getDisplayName(searchQuery, indexedProperty, originalValue);

        assertThat(result).isEqualTo("On order");
        verify(localizedMessageService, never()).getLocalizedMessageForKey(EDC_STOCK_VALUE_IN_STOCK);
        verify(localizedMessageService, never()).getLocalizedMessageForKey(EDC_STOCK_VALUE_NO_STOCK);
        verify(localizedMessageService).getLocalizedMessageForKey(EDC_STOCK_VALUE_NOS);
    }
}
