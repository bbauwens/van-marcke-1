package com.vanmarcke.services.model.builder;

import com.vanmarcke.core.model.CommerceGroupModel;
import de.hybris.platform.core.model.c2l.CountryModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link CommerceGroupModelMockBuilder} class can be used to create a mock implementation of the
 * {@link CommerceGroupModel} class.
 *
 * @author Christiaan Janssen
 * @since 08-07-2020
 */
public class CommerceGroupModelMockBuilder {

    private CountryModel country;

    /**
     * Creates a new instance of the {@link CommerceGroupModelMockBuilder} class.
     */
    private CommerceGroupModelMockBuilder() {
    }

    /**
     * The entry point to the {@link CommerceGroupModelMockBuilder}.
     *
     * @return the {@link CommerceGroupModelMockBuilder} instance for method chaining
     */
    public static CommerceGroupModelMockBuilder aCommerceGroup() {
        return new CommerceGroupModelMockBuilder();
    }

    /**
     * Sets the {@code country} on the current {@link CommerceGroupModelMockBuilder} instance
     *
     * @param country the country
     * @return the {@link CommerceGroupModelMockBuilder} instance for method chaining
     */
    public CommerceGroupModelMockBuilder withCountry(CountryModel country) {
        this.country = country;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link CommerceGroupModel} class.
     *
     * @return the {@link CountryModel} instance
     */
    public CommerceGroupModel build() {
        CommerceGroupModel group = mock(CommerceGroupModel.class);
        when(group.getCountry()).thenReturn(country);
        return group;
    }
}
