package com.vanmarcke.services.storesession.impl;

import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.i18n.LanguageResolver;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKStoreSessionServiceImplTest {

    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private LanguageResolver languageResolver;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private VMKPointOfServiceService pointOfServiceService;

    @InjectMocks
    private VMKStoreSessionServiceImpl vmkStoreSessionService;

    @Test
    public void testSetCurrentSite() {
        final String uid = "blue-be";
        final BaseSiteModel baseSite = mock(BaseSiteModel.class);
        final LanguageModel language = mock(LanguageModel.class);

        when(this.baseSiteService.getBaseSiteForUID(uid)).thenReturn(baseSite);
        when(baseSite.getDefaultLanguage()).thenReturn(language);
        when(language.getIsocode()).thenReturn("en");
        when(this.languageResolver.getLanguage("en")).thenReturn(language);
        this.vmkStoreSessionService.setCurrentSite(uid);

        verify(this.baseSiteService).setCurrentBaseSite(baseSite, true);
        verify(this.commonI18NService).setCurrentLanguage(language);
    }

    @Test
    public void testSetCurrentStore() {
        PointOfServiceModel pointOfServiceModel = mock(PointOfServiceModel.class);

        when(pointOfServiceService.getPointOfServiceForName("store-uid")).thenReturn(pointOfServiceModel);

        vmkStoreSessionService.setCurrentStore("store-uid");

        verify(pointOfServiceService).setCurrentPointOfService(pointOfServiceModel);
    }

}