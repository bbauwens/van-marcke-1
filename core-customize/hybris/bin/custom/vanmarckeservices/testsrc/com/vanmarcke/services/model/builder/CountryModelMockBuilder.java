package com.vanmarcke.services.model.builder;

import de.hybris.platform.core.model.c2l.CountryModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code CountryModelMockBuilder} class can be used to create a mock implementation of the
 * {@link CountryModel} class.
 *
 * @author Christiaan Janssen
 * @since 07-07-2020
 */
public class CountryModelMockBuilder {

    private String isoCode;

    /**
     * Creates a new instance of the {@link CountryModelMockBuilder} class.
     */
    private CountryModelMockBuilder() {
    }

    /**
     * The entry point to the {@link CountryModelMockBuilder}.
     *
     * @return the {@link CountryModelMockBuilder} instance for method chaining
     */
    public static CountryModelMockBuilder aCountry() {
        return new CountryModelMockBuilder();
    }

    /**
     * Sets the {@code isoCode} on the current {@link CountryModelMockBuilder} instance
     *
     * @param isoCode the ISO code
     * @return the {@link CountryModelMockBuilder} instance for method chaining
     */
    public CountryModelMockBuilder withISOCode(String isoCode) {
        this.isoCode = isoCode;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link CountryModel} class.
     *
     * @return the {@link CountryModel} instance
     */
    public CountryModel build() {
        CountryModel country = mock(CountryModel.class);
        when(country.getIsocode()).thenReturn(isoCode);
        return country;
    }
}
