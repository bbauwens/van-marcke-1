package com.vanmarcke.services.consent.impl;

import com.vanmarcke.core.cms.VMKConsentParagraphComponentDAO;
import com.vanmarcke.core.model.ConsentParagraphComponentModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKConsentParagraphComponentServiceImplTest} class contains the unit tests for the
 * {@link VMKConsentParagraphComponentServiceImpl} class.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKConsentParagraphComponentServiceImplTest {

    @Mock
    private VMKConsentParagraphComponentDAO consentParagraphComponentDAO;

    @Mock
    private CatalogVersionService catalogVersionService;

    @InjectMocks
    private VMKConsentParagraphComponentServiceImpl consentParagraphComponentService;

    @Test
    public void testGetConsentParagraphs() {
        CatalogVersionModel cv = mock(CatalogVersionModel.class);

        when(catalogVersionService.getCatalogVersion("globalBlueContentCatalog", "Online")).thenReturn(cv);

        ConsentParagraphComponentModel expectedParagraph = mock(ConsentParagraphComponentModel.class);

        when(consentParagraphComponentDAO.findAll(cv)).thenReturn(Collections.singletonList(expectedParagraph));

        List<ConsentParagraphComponentModel> actualParagraphs = consentParagraphComponentService.getConsentParagraphs();

        Assertions
                .assertThat(actualParagraphs)
                .containsExactly(expectedParagraph);

        verify(catalogVersionService).getCatalogVersion("globalBlueContentCatalog", "Online");
        verify(consentParagraphComponentDAO).findAll(cv);
    }
}