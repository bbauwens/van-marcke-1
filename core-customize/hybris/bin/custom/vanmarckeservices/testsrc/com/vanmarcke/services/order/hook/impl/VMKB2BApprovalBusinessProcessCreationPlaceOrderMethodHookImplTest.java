package com.vanmarcke.services.order.hook.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

@UnitTest
public class VMKB2BApprovalBusinessProcessCreationPlaceOrderMethodHookImplTest {

    private VMKB2BApprovalBusinessProcessCreationPlaceOrderMethodHookImpl hook;

    @Before
    public void setUp() {
        hook = new VMKB2BApprovalBusinessProcessCreationPlaceOrderMethodHookImpl();
    }

    @Test
    public void afterPlaceOrderDoesNothing() {
        CommerceCheckoutParameter commerceCheckoutParameter = mock(CommerceCheckoutParameter.class);
        CommerceOrderResult commerceOrderResult = mock(CommerceOrderResult.class);

        hook.afterPlaceOrder(commerceCheckoutParameter, commerceOrderResult);

        verifyZeroInteractions(commerceCheckoutParameter);
        verifyZeroInteractions(commerceOrderResult);
    }

    @Test
    public void beforePlaceOrderDoesNothing() {
        CommerceCheckoutParameter commerceCheckoutParameter = mock(CommerceCheckoutParameter.class);

        hook.beforePlaceOrder(commerceCheckoutParameter);

        verifyZeroInteractions(commerceCheckoutParameter);
    }

    @Test
    public void beforeSubmitOrderDoesNothing() {
        CommerceCheckoutParameter commerceCheckoutParameter = mock(CommerceCheckoutParameter.class);
        CommerceOrderResult commerceOrderResult = mock(CommerceOrderResult.class);

        hook.beforeSubmitOrder(commerceCheckoutParameter, commerceOrderResult);

        verifyZeroInteractions(commerceCheckoutParameter);
        verifyZeroInteractions(commerceOrderResult);
    }
}