package com.vanmarcke.services.product.price.impl;

import com.vanmarcke.cpi.data.price.ProductPriceResponseData;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import com.vanmarcke.services.product.price.VMKPriceLookupService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceCalculationData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.*;

/**
 * The {@code VMKPriceServiceImplTest} class contains the unit tests for the {@link VMKPriceServiceImpl} class.
 *
 * @author Joris Cryns, Christiaan Janssen
 * @since 27-06-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPriceServiceImplTest {

    private static final String B2B_UNIT_UID = RandomStringUtils.randomAlphabetic(10);
    private static final String BASE_PRODUCT_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String VARIANT_PRODUCT_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String CURRENCY_ISO = RandomStringUtils.randomAlphabetic(10);
    private static final BigDecimal PRICE = BigDecimal.valueOf(RandomUtils.nextLong());
    private static final Integer VAT = Integer.valueOf(RandomUtils.nextInt());
    private static final Double PRICE2_DOUBLE = RandomUtils.nextDouble();
    private static final BigDecimal PRICE2 = BigDecimal.valueOf(PRICE2_DOUBLE);

    @Mock
    private VMKPriceLookupService priceLookupService;

    @Mock
    private CommercePriceService commercePriceService;

    @InjectMocks
    private VMKPriceServiceImpl vmkPriceService;

    @Test
    public void testGetPricingWithMatchingResult() {
        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(BASE_PRODUCT_CODE, null)
                .build();

        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(VARIANT_PRODUCT_CODE, Locale.CANADA)
                .withBaseProduct(product)
                .build();

        ProductPriceResponseData responseData = new ProductPriceResponseData();
        responseData.setProductCode(VARIANT_PRODUCT_CODE);
        responseData.setPrice(PRICE);
        responseData.setVat(VAT);

        when(priceLookupService.getPricing(product, B2B_UNIT_UID)).thenReturn(Collections.singletonList(responseData));

        PriceCalculationData expectedPrice = vmkPriceService.getPrice(variant, B2B_UNIT_UID);

        Assertions
                .assertThat(expectedPrice.getPriceValue())
                .isEqualByComparingTo(PRICE);

        Assertions
                .assertThat(expectedPrice.getVat())
                .isEqualTo(VAT);

        verify(priceLookupService).getPricing(product, B2B_UNIT_UID);
        verifyZeroInteractions(commercePriceService);
    }

    @Test
    public void testGetPricingWithNoMatchingResult() {
        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(VARIANT_PRODUCT_CODE, Locale.CANADA)
                .build();

        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(BASE_PRODUCT_CODE, null)
                .withVariants(variant)
                .build();

        when(variant.getBaseProduct()).thenReturn(product);

        PriceInformation priceInformation = new PriceInformation(new PriceValue(CURRENCY_ISO, PRICE2_DOUBLE, false));

        when(priceLookupService.getPricing(product, B2B_UNIT_UID)).thenReturn(List.of());
        when(commercePriceService.getWebPriceForProduct(variant)).thenReturn(priceInformation);

        PriceCalculationData expectedPrice = vmkPriceService.getPrice(variant, B2B_UNIT_UID);

        Assertions
                .assertThat(expectedPrice.getPriceValue())
                .isEqualByComparingTo(PRICE2);

        verify(priceLookupService).getPricing(product, B2B_UNIT_UID);
        verify(commercePriceService).getWebPriceForProduct(variant);
    }
}