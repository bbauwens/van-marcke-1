package com.vanmarcke.services.suggestion.impl;

import com.vanmarcke.core.suggestion.impl.VMKSimpleSuggestionDaoImpl;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import junit.framework.Assert;
import org.apache.commons.lang.math.NumberUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * JUnit test suite for {@link VMKSimpleSuggestionServiceImplTest}
 */
@UnitTest
public class VMKSimpleSuggestionServiceImplTest {

    @Mock
    private VMKSimpleSuggestionDaoImpl simpleSuggestionDao;
    private VMKSimpleSuggestionServiceImpl defaultSimpleSuggestionService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        defaultSimpleSuggestionService = new VMKSimpleSuggestionServiceImpl();
        defaultSimpleSuggestionService.setSimpleSuggestionDao(simpleSuggestionDao);
    }

    @Test
    public void testGetReferencedProductsForBoughtCategory() {
        final UserModel user = mock(UserModel.class);
        final CategoryModel category = mock(CategoryModel.class);

        final Integer limit = NumberUtils.INTEGER_ONE;
        final boolean excludePurchased = true;
        final List<ProductModel> result = Collections.emptyList();
        final ProductReferenceTypeEnum type = ProductReferenceTypeEnum.FOLLOWUP;
        given(simpleSuggestionDao.findProductsRelatedToPurchasedProductsByCategory(category, user, type, excludePurchased, limit))
                .willReturn(result);

        final List<ProductModel> actual = defaultSimpleSuggestionService.getReferencesForPurchasedInCategory(category, user, type,
                excludePurchased, limit);
        Assert.assertEquals(result, actual);
    }

    @Test
    public void getReferencesForProducts() {
        List<ProductModel> expected = new ArrayList<>();
        when(simpleSuggestionDao.findProductsRelatedToProducts(anyList(), anyList(), any(UserModel.class), anyBoolean(), anyInt())).thenReturn(expected);

        List<ProductModel> actual = defaultSimpleSuggestionService.getReferencesForProducts(null, null, null, true, null);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void getReferencesForPurchasedInCategory() {
        List<ProductModel> expected = new ArrayList<>();
        when(simpleSuggestionDao.findProductsRelatedToPurchasedProductsByCategory(any(CategoryModel.class), anyList(), any(UserModel.class), anyBoolean(), anyInt())).thenReturn(expected);

        List<ProductModel> actual = defaultSimpleSuggestionService.getReferencesForPurchasedInCategory(null, new ArrayList<>(), null, true, null);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void getReferencesForProductsReturnsEmptyListWhenProductsIsEmpty() {
        List<ProductModel> actual = defaultSimpleSuggestionService.getReferencesForProducts(new ArrayList<>(), null, null, true, null);
        assertThat(actual).isEmpty();
    }
}
