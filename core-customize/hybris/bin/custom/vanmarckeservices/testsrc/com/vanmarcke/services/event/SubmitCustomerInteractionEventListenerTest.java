package com.vanmarcke.services.event;

import com.vanmarcke.core.model.CustomerInteractionProcessModel;
import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.services.event.events.SubmitCustomerInteractionEvent;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubmitCustomerInteractionEventListenerTest {

    @Mock
    private ModelService modelService;
    @Mock
    private BusinessProcessService businessProcessService;
    @InjectMocks
    private SubmitCustomerInteractionEventListener submitCustomerInteractionEventListener;

    @Test
    public void testOnSiteEvent() {
        final SubmitCustomerInteractionEvent event = mock(SubmitCustomerInteractionEvent.class);
        final OrderCustomerInteractionModel customerInteraction = mock(OrderCustomerInteractionModel.class);
        final OrderModel order = mock(OrderModel.class);
        final CustomerInteractionProcessModel customerInteractionProcess = mock(CustomerInteractionProcessModel.class);

        when(event.getCustomerInteraction()).thenReturn(customerInteraction);
        when(customerInteraction.getOrder()).thenReturn(order);
        when(order.getCode()).thenReturn("1234");
        when(businessProcessService.createProcess(any(String.class), eq("blue-customer-interaction-process"))).thenReturn(customerInteractionProcess);
        submitCustomerInteractionEventListener.onSiteEvent(event);

        verify(modelService).save(customerInteractionProcess);
        verify(businessProcessService).startProcess(customerInteractionProcess);
    }

    @Test
    public void testGetSiteChannelForEvent() {
        final SubmitCustomerInteractionEvent event = mock(SubmitCustomerInteractionEvent.class);
        final OrderCustomerInteractionModel customerInteraction = mock(OrderCustomerInteractionModel.class);
        final OrderModel order = mock(OrderModel.class);
        final BaseSiteModel baseSite = mock(BaseSiteModel.class);

        when(event.getCustomerInteraction()).thenReturn(customerInteraction);
        when(customerInteraction.getOrder()).thenReturn(order);
        when(customerInteraction.getSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.B2B);
        final SiteChannel result = submitCustomerInteractionEventListener.getSiteChannelForEvent(event);

        assertThat(result).isEqualTo(SiteChannel.B2B);
    }
}