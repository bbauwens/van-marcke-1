package com.vanmarcke.services.order.cart.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartFactory;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@UnitTest
public class VMKCartFactoryFactoryImplTest {

    private BaseSiteService baseSiteService;
    private Map<SiteChannel, CartFactory> factoryConfigurationMap;
    private CartFactory defaultConfiguration;
    private CartFactory b2bCartFactory;

    private VMKCartFactoryFactoryImpl defaultVMKCartFactoryFactory;

    @Before
    public void setUp() {
        baseSiteService = mock(BaseSiteService.class);
        defaultConfiguration = mock(CartFactory.class);
        b2bCartFactory = mock(CartFactory.class);
        factoryConfigurationMap = new HashMap<>();
        factoryConfigurationMap.put(SiteChannel.B2B, b2bCartFactory);
        defaultVMKCartFactoryFactory = new VMKCartFactoryFactoryImpl();
        defaultVMKCartFactoryFactory.setBaseSiteService(baseSiteService);
        defaultVMKCartFactoryFactory.setDefaultConfiguration(defaultConfiguration);
        defaultVMKCartFactoryFactory.setFactoryConfigurationMap(factoryConfigurationMap);
    }

    @Test
    public void createCartReturnsCartFactoryFromMap() {
        CartModel expected = mock(CartModel.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.B2B);
        when(b2bCartFactory.createCart()).thenReturn(expected);

        CartModel actual = defaultVMKCartFactoryFactory.createCart();

        verifyZeroInteractions(defaultConfiguration);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void createCartReturnsDefaultCartFactory() {
        CartModel expected = mock(CartModel.class);
        BaseSiteModel baseSite = mock(BaseSiteModel.class);
        when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSite);
        when(baseSite.getChannel()).thenReturn(SiteChannel.B2C);
        when(defaultConfiguration.createCart()).thenReturn(expected);

        CartModel actual = defaultVMKCartFactoryFactory.createCart();

        verifyZeroInteractions(b2bCartFactory);
        assertThat(actual).isEqualTo(expected);
    }
}