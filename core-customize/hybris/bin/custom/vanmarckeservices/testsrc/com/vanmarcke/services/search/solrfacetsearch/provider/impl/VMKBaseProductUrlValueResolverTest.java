package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKBaseProductUrlValueResolverTest extends AbstractValueResolverTest {

    @Mock
    private InputDocument inputDocument;
    @Mock
    private IndexerBatchContext indexerBatchContext;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private ProductModel productModel;
    @Mock
    private VariantProductModel variantProductModel;
    @Mock
    private UrlResolver<ProductModel> urlResolver;

    @InjectMocks
    private VMKBaseProductUrlValueResolver valueResolver;

    @Test
    public void testAddValueWhenBaseProduct() throws FieldValueProviderException {
        when(urlResolver.resolve(productModel)).thenReturn("baseProductURL");
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), productModel);

        verify(inputDocument).addField(indexedProperty, "baseProductURL", null);
    }

    @Test
    public void testAddValueWhenVariantProduct() throws FieldValueProviderException {
        when(variantProductModel.getBaseProduct()).thenReturn(productModel);
        when(urlResolver.resolve(productModel)).thenReturn("baseProductURL");
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), variantProductModel);

        verify(inputDocument).addField(indexedProperty, "baseProductURL", null);
    }

    @Test
    public void testAddValueWhenNull() throws FieldValueProviderException {
        when(variantProductModel.getBaseProduct()).thenReturn(null);
        valueResolver.resolve(inputDocument, indexerBatchContext, Collections.singletonList(indexedProperty), variantProductModel);

        verifyZeroInteractions(inputDocument);
    }
}