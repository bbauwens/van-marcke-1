package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.services.strategies.VMKCommercePaymentCaptureStrategy;
import com.vanmarcke.services.strategies.VMKCommerceSendOrderStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSendOrderPlaceOrderMethodHookImplTest {

    @Mock
    private ModelService modelService;
    @Mock
    private VMKCommerceSendOrderStrategy vmkCommerceSendOrderStrategy;
    @Mock
    private VMKCommercePaymentCaptureStrategy vmkCommercePaymentCaptureStrategy;
    @InjectMocks
    private VMKSendOrderPlaceOrderMethodHookImpl defaultVMKSendOrderPlaceOrderMethodHook;

    @Test
    public void testBeforeSubmitOrder() throws Exception {
        OrderModel order = mock(OrderModel.class);
        CommerceCheckoutParameter parameter = mock(CommerceCheckoutParameter.class);
        CommerceOrderResult result = mock(CommerceOrderResult.class);

        when(result.getOrder()).thenReturn(order);
        when(vmkCommercePaymentCaptureStrategy.isPaymentCapturedForOrder(order)).thenReturn(Boolean.TRUE);
        defaultVMKSendOrderPlaceOrderMethodHook.beforeSubmitOrder(parameter, result);

        verify(vmkCommercePaymentCaptureStrategy).isPaymentCapturedForOrder(order);
        verify(vmkCommerceSendOrderStrategy).sendOrder(order);
    }

    @Test
    public void testBeforeSubmitOrder_PaymentNotYetCaptured() throws Exception {
        OrderModel order = mock(OrderModel.class);
        CommerceCheckoutParameter parameter = mock(CommerceCheckoutParameter.class);
        CommerceOrderResult result = mock(CommerceOrderResult.class);

        when(result.getOrder()).thenReturn(order);
        when(vmkCommercePaymentCaptureStrategy.isPaymentCapturedForOrder(order)).thenReturn(Boolean.FALSE);
        defaultVMKSendOrderPlaceOrderMethodHook.beforeSubmitOrder(parameter, result);

        verify(vmkCommercePaymentCaptureStrategy).isPaymentCapturedForOrder(order);
        verifyZeroInteractions(vmkCommerceSendOrderStrategy);
    }

    @Test
    public void testBeforePlaceOrder_cartNull() throws InvalidCartException {
        CommerceCheckoutParameter parameter = mock(CommerceCheckoutParameter.class);
        defaultVMKSendOrderPlaceOrderMethodHook.beforePlaceOrder(parameter);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testBeforePlaceOrder() throws InvalidCartException {
        CommerceCheckoutParameter parameter = mock(CommerceCheckoutParameter.class);
        CartModel cart = mock(CartModel.class);
        when(parameter.getCart()).thenReturn(cart);

        defaultVMKSendOrderPlaceOrderMethodHook.beforePlaceOrder(parameter);

        verify(cart).setStatus(OrderStatus.ORDER_CREATION_IN_PROGRESS);
        verify(modelService).save(cart);
    }
}