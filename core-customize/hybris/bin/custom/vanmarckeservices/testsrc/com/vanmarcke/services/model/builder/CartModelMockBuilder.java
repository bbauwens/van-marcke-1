package com.vanmarcke.services.model.builder;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code CartModelMockBuilder} class can be used to create a mock implementation of the
 * {@link CartModel} class.
 *
 * @author Christiaan Janssen
 * @since 06-07-2020
 */
public class CartModelMockBuilder extends AbstractOrderModelMockBuilder<CartModelMockBuilder, CartModel> {

    PointOfServiceModel deliveryPointOfService;
    DeliveryModeModel deliveryMode;

    /**
     * Creates a new instance of the {@link CartModelMockBuilder} class.
     */
    private CartModelMockBuilder() {
        super(CartModelMockBuilder.class);
    }

    /**
     * The entry point to the {@link CartModelMockBuilder}.
     *
     * @return the {@link CartModelMockBuilder} instance for method chaining
     */
    public static CartModelMockBuilder aCart() {
        return new CartModelMockBuilder();
    }

    /**
     * Sets the {@code deliveryPointOfService} on the current {@link CartModelMockBuilder} instance
     *
     * @param deliveryPointOfService the delivery point of service
     * @return the {@link CartModelMockBuilder} instance for method chaining
     */
    public CartModelMockBuilder withDeliveryPointOfService(PointOfServiceModel deliveryPointOfService) {
        this.deliveryPointOfService = deliveryPointOfService;
        return this;
    }

    /**
     * Sets the {@code deliveryMode} on the current {@link CartModelMockBuilder} instance
     *
     * @param deliveryMode the delivery mode
     * @return the {@link CartModelMockBuilder} instance for method chaining
     */
    public CartModelMockBuilder withDeliveryMode(DeliveryModeModel deliveryMode) {
        this.deliveryMode = deliveryMode;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link CartModel} class.
     *
     * @return the {@link CartModel} instance
     */
    public CartModel build() {
        CartModel cart = mock(CartModel.class);
        when(cart.getCode()).thenReturn(code);
        when(cart.getGuid()).thenReturn(guid);
        when(cart.getUser()).thenReturn(user);
        when(cart.getCurrency()).thenReturn(currency);
        when(cart.getUnit()).thenReturn(unit);
        when(cart.getNet()).thenReturn(net);
        when(cart.getDeliveryAddress()).thenReturn(deliveryAddress);
        when(cart.getPaymentAddress()).thenReturn(paymentAddress);
        when(cart.getDeliveryPointOfService()).thenReturn(deliveryPointOfService);
        when(cart.getDeliveryMode()).thenReturn(deliveryMode);
        when(cart.getEntries()).thenReturn(entries);
        when(cart.getSite()).thenReturn(site);
        return cart;
    }
}
