package com.vanmarcke.services.product.price.impl;

import com.vanmarcke.cpi.data.price.PriceRequestData;
import com.vanmarcke.cpi.data.price.PriceResponseData;
import com.vanmarcke.cpi.data.price.ProductPriceResponseData;
import com.vanmarcke.cpi.services.ESBPriceService;
import com.vanmarcke.services.model.builder.ProductModelMockBuilder;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPriceLookupServiceImplTest {

    private static final String B2B_UNIT_UID = RandomStringUtils.randomAlphabetic(10);
    private static final String BASE_PRODUCT_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String VARIANT_PRODUCT_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final BigDecimal PRICE = BigDecimal.valueOf(RandomUtils.nextLong());

    @Mock
    private ESBPriceService esbPriceService;

    @InjectMocks
    private VMKPriceLookupServiceImpl vmkPriceLookupService;

    @Captor
    private ArgumentCaptor<PriceRequestData> captor;

    @Test
    public void testGetPricingWithNullResponse() {
        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(VARIANT_PRODUCT_CODE, Locale.CANADA)
                .build();

        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(BASE_PRODUCT_CODE, null)
                .withVariants(variant)
                .build();

        when(esbPriceService.getPriceInformation(captor.capture())).thenReturn(null);

        List<ProductPriceResponseData> expectedPrices = vmkPriceLookupService.getPricing(product, B2B_UNIT_UID);

        Assertions
                .assertThat(expectedPrices)
                .isEmpty();

        PriceRequestData requestCaptor = captor.getValue();

        Assertions
                .assertThat(requestCaptor)
                .isNotNull();

        Assertions
                .assertThat(requestCaptor.getCustomerID())
                .isEqualTo(B2B_UNIT_UID);

        Assertions
                .assertThat(requestCaptor.getProductIDs())
                .containsExactly(VARIANT_PRODUCT_CODE);

        verify(esbPriceService).getPriceInformation(any(PriceRequestData.class));
    }

    @Test
    public void testGetPricingWithoutPricing() {
        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(VARIANT_PRODUCT_CODE, Locale.CANADA)
                .build();

        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(BASE_PRODUCT_CODE, null)
                .withVariants(variant)
                .build();

        PriceResponseData response = new PriceResponseData();

        when(esbPriceService.getPriceInformation(captor.capture())).thenReturn(response);

        List<ProductPriceResponseData> expectedPrices = vmkPriceLookupService.getPricing(product, B2B_UNIT_UID);

        Assertions
                .assertThat(expectedPrices)
                .isEmpty();

        PriceRequestData request = captor.getValue();

        Assertions
                .assertThat(request)
                .isNotNull();

        Assertions
                .assertThat(request.getCustomerID())
                .isEqualTo(B2B_UNIT_UID);

        Assertions
                .assertThat(request.getProductIDs())
                .containsExactly(VARIANT_PRODUCT_CODE);

        verify(esbPriceService).getPriceInformation(any(PriceRequestData.class));
    }

    @Test
    public void testGetPricingWithMatchingEntryForVariant() {
        VariantProductModel variant = VariantProductModelMockBuilder
                .aVariantProduct(VARIANT_PRODUCT_CODE, Locale.CANADA)
                .build();

        ProductModel product = ProductModelMockBuilder
                .aBaseProduct(BASE_PRODUCT_CODE, null)
                .withVariants(variant)
                .build();

        ProductPriceResponseData price = new ProductPriceResponseData();
        price.setProductCode(VARIANT_PRODUCT_CODE);
        price.setPrice(PRICE);

        PriceResponseData response = new PriceResponseData();
        response.setPricing(Collections.singletonList(price));

        when(esbPriceService.getPriceInformation(captor.capture())).thenReturn(response);

        List<ProductPriceResponseData> expectedPrices = vmkPriceLookupService.getPricing(product, B2B_UNIT_UID);

        Assertions
                .assertThat(expectedPrices)
                .containsExactly(price);

        PriceRequestData request = captor.getValue();

        Assertions
                .assertThat(request)
                .isNotNull();

        Assertions
                .assertThat(request.getCustomerID())
                .isEqualTo(B2B_UNIT_UID);

        Assertions
                .assertThat(request.getProductIDs())
                .containsExactly(VARIANT_PRODUCT_CODE);

        verify(esbPriceService).getPriceInformation(any(PriceRequestData.class));
    }
}