package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.services.delivery.VMKPickupInfoService;
import com.vanmarcke.services.delivery.VMKShippingInfoService;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeliveryMethodServiceImplTest {

    private static final String PICKUP_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final String SHIPPING_CODE = RandomStringUtils.randomAlphabetic(10);
    private static final boolean PICKUP_POSSIBLE = RandomUtils.nextBoolean();
    private static final boolean SHIPPING_POSSIBLE = RandomUtils.nextBoolean();

    private CartModel cart;
    private ZoneDeliveryModeModel shipping;
    private PickUpDeliveryModeModel pickup;
    private List<DeliveryModeModel> supportedDeliveryMethods;

    @Mock
    private DeliveryService deliveryService;

    @Mock
    private VMKPickupInfoService pickupInfoService;

    @Mock
    private VMKShippingInfoService shippingInfoService;

    @InjectMocks
    private VMKDeliveryMethodServiceImpl deliveryMethodService;

    @Before
    public void setUp() throws Exception {
        cart = CartModelMockBuilder
                .aCart()
                .build();

        shipping = mock(ZoneDeliveryModeModel.class);
        pickup = mock(PickUpDeliveryModeModel.class);

        supportedDeliveryMethods = Arrays.asList(shipping, pickup);

        when(deliveryService.getSupportedDeliveryModeListForOrder(cart))
                .thenReturn(supportedDeliveryMethods);
    }

    @Test
    public void testGetSupportedDeliveryMethods() {
        when(pickupInfoService.isPickupPossible(cart)).thenReturn(PICKUP_POSSIBLE);
        when(shippingInfoService.isShippingPossible(cart)).thenReturn(SHIPPING_POSSIBLE);

        HashMap<String, Boolean> actualDeliveryModes = deliveryMethodService.getSupportedDeliveryMethods(cart);

        assertThat(actualDeliveryModes).contains(
                entry("PICKUP", PICKUP_POSSIBLE),
                entry("DELIVERY", SHIPPING_POSSIBLE));
    }

    @Test
    public void testGetDeliveryMethodCodes() {
        when(shipping.getCode()).thenReturn(SHIPPING_CODE);
        when(pickup.getCode()).thenReturn(PICKUP_CODE);

        Map<String, String> actualMapping = deliveryMethodService.getDeliveryMethodCodes(cart);

        assertThat(actualMapping).contains(
                entry("PICKUP", PICKUP_CODE),
                entry("DELIVERY", SHIPPING_CODE));
    }

    @Test
    public void testGetDeliveryMethodCodes_oneNullValue() {
        when(shipping.getCode()).thenReturn(SHIPPING_CODE);
        supportedDeliveryMethods = singletonList(shipping);


        Map<String, String> actualMapping = deliveryMethodService.getDeliveryMethodCodes(cart);

        assertThat(actualMapping).contains(entry("DELIVERY", SHIPPING_CODE));
    }

    @Test
    public void testGetSupportDeliveryMethodsForCart() {
        Map<Boolean, List<DeliveryModeModel>> result = deliveryMethodService.getSupportDeliveryMethodsForCart(cart);

        assertThat(result.get(true)).containsExactly(shipping);
        assertThat(result.get(false)).containsExactly(pickup);
    }
}