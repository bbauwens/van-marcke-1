package com.vanmarcke.services.factories.impl;

import com.vanmarcke.services.basestore.VMKBaseStoreService;
import com.vanmarcke.services.product.VMKStockService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.store.BaseStoreModel;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.vanmarcke.services.constants.VanmarckeservicesConstants.ERPSystems.IBM;
import static com.vanmarcke.services.constants.VanmarckeservicesConstants.ERPSystems.S4HANA;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKStockServiceFactoryImplTest {

    @Mock
    private VMKBaseStoreService baseStoreService;
    @Mock
    private Map<String, VMKStockService> factoryConfigurationMap;
    @Mock
    private VMKStockService defaultStockService;
    @Mock
    private VMKStockService ibmStockService;
    @Mock
    private VMKStockService sapStockService;

    private VMKStockServiceFactoryImpl stockServiceFactory;

    @Before
    public void setUp() {
        stockServiceFactory = new VMKStockServiceFactoryImpl(baseStoreService, factoryConfigurationMap, defaultStockService);
        when(factoryConfigurationMap.getOrDefault(IBM, defaultStockService)).thenReturn(ibmStockService);
        when(factoryConfigurationMap.getOrDefault(S4HANA, defaultStockService)).thenReturn(sapStockService);
    }

    @Test
    public void testGetStockService_S4Hana() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStore);
        when(baseStoreService.isSAPBaseStore(baseStore)).thenReturn(true);

        Assertions
                .assertThat(stockServiceFactory.getStockService())
                .isEqualTo(sapStockService);

        verify(baseStoreService).getCurrentBaseStore();
        verify(baseStoreService).isSAPBaseStore(baseStore);
    }

    @Test
    public void testGetStockService_IBM() {
        BaseStoreModel baseStore = mock(BaseStoreModel.class);
        when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStore);
        when(baseStoreService.isSAPBaseStore(baseStore)).thenReturn(false);

        Assertions
                .assertThat(stockServiceFactory.getStockService())
                .isEqualTo(ibmStockService);

        verify(baseStoreService).getCurrentBaseStore();
        verify(baseStoreService).isSAPBaseStore(baseStore);
    }
}