package com.vanmarcke.services.action;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

@UnitTest
public class VMKDummyActionTest {

    private VMKDummyAction vmkDummyAction;

    @Before
    public void setUp() {
        vmkDummyAction = new VMKDummyAction();
    }

    @Test
    public void executeAction() {
        BusinessProcessModel businessProcess = mock(BusinessProcessModel.class);

        AbstractSimpleDecisionAction.Transition actual = vmkDummyAction.executeAction(businessProcess);

        verifyZeroInteractions(businessProcess);
        assertThat(actual).isEqualTo(AbstractSimpleDecisionAction.Transition.OK);
    }
}