package com.vanmarcke.services.externaltax.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class VMKAcceleratorDetermineExternalTaxStrategyTest {

    private VMKAcceleratorDetermineExternalTaxStrategy acceleratorDetermineExternalTaxStrategy;

    @Before
    public void setUp() {
        acceleratorDetermineExternalTaxStrategy = new VMKAcceleratorDetermineExternalTaxStrategy();
    }

    @Test
    public void shouldCalculateExternalTaxes() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        when(abstractOrderModel.getNet()).thenReturn(true);
        when(abstractOrderModel.getDeliveryAddress()).thenReturn(mock(AddressModel.class));
        when(abstractOrderModel.getDeliveryMode()).thenReturn(mock(DeliveryModeModel.class));

        boolean actual = acceleratorDetermineExternalTaxStrategy.shouldCalculateExternalTaxes(abstractOrderModel);

        assertThat(actual).isTrue();
    }

    @Test
    public void shouldCalculateExternalTaxesReturnsFalseWhenNetIsFalse() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        when(abstractOrderModel.getNet()).thenReturn(false);
        when(abstractOrderModel.getDeliveryAddress()).thenReturn(mock(AddressModel.class));
        when(abstractOrderModel.getDeliveryMode()).thenReturn(mock(DeliveryModeModel.class));

        boolean actual = acceleratorDetermineExternalTaxStrategy.shouldCalculateExternalTaxes(abstractOrderModel);

        assertThat(actual).isFalse();
    }

    @Test
    public void shouldCalculateExternalTaxesReturnsFalseWhenDeliveryModeIsNull() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        when(abstractOrderModel.getNet()).thenReturn(true);
        when(abstractOrderModel.getDeliveryAddress()).thenReturn(mock(AddressModel.class));
        when(abstractOrderModel.getDeliveryMode()).thenReturn(null);

        boolean actual = acceleratorDetermineExternalTaxStrategy.shouldCalculateExternalTaxes(abstractOrderModel);

        assertThat(actual).isFalse();
    }

    @Test
    public void shouldCalculateExternalTaxesReturnsFalseWhenDeliveryAddressIsNull() {
        AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        when(abstractOrderModel.getNet()).thenReturn(true);
        when(abstractOrderModel.getDeliveryAddress()).thenReturn(null);
        when(abstractOrderModel.getDeliveryMode()).thenReturn(mock(DeliveryModeModel.class));

        boolean actual = acceleratorDetermineExternalTaxStrategy.shouldCalculateExternalTaxes(abstractOrderModel);

        assertThat(actual).isFalse();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldCalculateExternalTaxesThrowsIllegalArgumentExceptionWhenInputIsNull() {
        acceleratorDetermineExternalTaxStrategy.shouldCalculateExternalTaxes(null);
    }
}