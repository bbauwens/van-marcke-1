package com.vanmarcke.services.order.impl;

import com.vanmarcke.cpi.data.order.EntryInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.model.builder.CartEntryModelMockBuilder;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import com.vanmarcke.services.model.builder.VariantProductModelMockBuilder;
import com.vanmarcke.services.product.VMKVariantProductService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKCommerceCheckoutServiceImplTest} class contains the unit tests for the
 * {@link VMKCommerceCheckoutServiceImpl} class.
 *
 * @author Tom van den Berg, Christiaan Janssen
 * @since 06-07-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCommerceCheckoutServiceImplTest {

    private static final String POS_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String SKU = RandomStringUtils.randomAlphabetic(10);
    private static final String ANOTHER_SKU = RandomStringUtils.randomAlphabetic(10);

    private static final String CODE_SKU_L = "L";
    private static final String CODE_SKU_X = "X";
    private static final String CODE_SKU_STAR = "*";

    private static final Date NOW = new Date();

    @Mock
    private VMKPointOfServiceService pointOfServiceService;

    @Mock
    private CartService cartService;

    @Mock
    private Converter<DeliveryMethodForm, CartModel> deliveryInformationCartConverter;

    @Mock
    private CommerceCartCalculationStrategy commerceCartCalculationStrategy;

    @Mock
    private ModelService modelService;

    @Mock
    private UserService userService;

    @Mock
    private VMKDeliveryInfoService deliveryInfoService;

    @Mock
    private VMKVariantProductService vmkVariantProductService;

    @Spy
    @InjectMocks
    private VMKCommerceCheckoutServiceImpl checkoutService;

    @Captor
    private ArgumentCaptor<CommerceCartParameter> captor;

    @Before
    public void setUp() {
        checkoutService.setModelService(modelService);
    }

    @Test
    public void testSetDeliveryDetails() {
        DeliveryMethodForm form = new DeliveryMethodForm();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        doReturn(cart).when(checkoutService).getCart();

        checkoutService.setDeliveryDetailsOnCart(form);

        verify(deliveryInformationCartConverter).convert(form, cart);
        verify(modelService).save(cart);
        verify(modelService).refresh(cart);
        verify(commerceCartCalculationStrategy).calculateCart(captor.capture());

        CommerceCartParameter actualParameter = captor.getValue();

        assertThat(actualParameter.isEnableHooks()).isTrue();

        assertThat(actualParameter.getCart()).isEqualTo(cart);
    }

    @Test
    public void testSetFavoriteAlternativeStoreOnSessionCartWithPointOfService() {
        CartModel cart = new CartModel();

        doReturn(cart).when(checkoutService).getCart();

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        when(pointOfServiceService.getPointOfServiceForName(POS_NAME)).thenReturn(pos);

        checkoutService.setFavoriteAlternativeStoreOnSessionCart(POS_NAME);

        assertThat(cart.getDeliveryPointOfService()).isEqualTo(pos);

        verifyZeroInteractions(userService);
        verify(pointOfServiceService).getPointOfServiceForName(POS_NAME);
        verify(modelService).save(cart);
    }

    @Test
    public void testSetFavoriteAlternativeStoreOnSessionCartWithoutPointOfService() {
        CartModel cart = new CartModel();
        UserModel user = new UserModel();
        PointOfServiceModel pos = mock(PointOfServiceModel.class);
        user.setSessionStore(pos);

        doReturn(cart).when(checkoutService).getCart();
        when(userService.getCurrentUser()).thenReturn(user);

        checkoutService.setFavoriteAlternativeStoreOnSessionCart(null);

        assertThat(cart.getDeliveryPointOfService()).isEqualTo(pos);

        verify(userService).getCurrentUser();
        verify(pointOfServiceService, never()).getPointOfServiceForName(anyString());
        verify(modelService).save(cart);
    }

    @Test
    public void testGetCartWithSessionCart() {
        when(cartService.hasSessionCart()).thenReturn(true);

        CartModel expectedCart = CartModelMockBuilder
                .aCart()
                .build();

        when(cartService.getSessionCart()).thenReturn(expectedCart);

        CartModel actualCart = checkoutService.getCart();

        assertThat(actualCart).isEqualTo(expectedCart);

        verify(cartService).hasSessionCart();
        verify(cartService).getSessionCart();
    }

    @Test
    public void testGetCartWithoutSessionCart() {
        when(cartService.hasSessionCart()).thenReturn(false);

        CartModel actualCart = checkoutService.getCart();

        assertThat(actualCart).isNull();

        verify(cartService).hasSessionCart();
        verify(cartService, never()).getSessionCart();
    }

    @Test
    public void testGetInvalidCartEntries_noSessionCart() {
        when(cartService.hasSessionCart()).thenReturn(false);

        assertThat(checkoutService.getInvalidCartEntries()).isEmpty();
    }

    @Test
    public void testGetInvalidCartEntries_noProducts() {
        when(cartService.hasSessionCart()).thenReturn(true);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        when(cartService.getSessionCart()).thenReturn(cart);

        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        response.setExpressPickupDate(new Date());

        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(response));

        List<String> result = checkoutService.getInvalidCartEntries();

        assertThat(result).isEmpty();
    }

    @Test
    public void testGetInvalidCartEntries_withTecCollectDate() {
        when(cartService.hasSessionCart()).thenReturn(true);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        when(cartService.getSessionCart()).thenReturn(cart);

        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        response.setExpressPickupDate(new Date());

        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(response));

        List<String> result = checkoutService.getInvalidCartEntries();

        assertThat(result).isEmpty();
    }

    @Test
    public void testGetInvalidCartEntries() {
        when(cartService.hasSessionCart()).thenReturn(true);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        when(cartService.getSessionCart()).thenReturn(cart);

        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        EntryInfoResponseData entry1 = new EntryInfoResponseData();
        EntryInfoResponseData entry2 = new EntryInfoResponseData();
        entry1.setCodeSku(CODE_SKU_L);
        entry1.setProductNumber(SKU);
        entry1.setPickupDate(NOW);
        entry2.setCodeSku(CODE_SKU_X);
        entry2.setPickupDate(NOW);

        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(response));

        response.setProducts(asList(entry1, entry2));

        List<String> result = checkoutService.getInvalidCartEntries();

        assertThat(result).containsExactly(SKU);
    }

    @Test
    public void testGetInvalidCartEntries_withNullPickupDate_withXSku() {
        when(cartService.hasSessionCart()).thenReturn(true);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        when(cartService.getSessionCart()).thenReturn(cart);

        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        EntryInfoResponseData entry1 = new EntryInfoResponseData();
        EntryInfoResponseData entry2 = new EntryInfoResponseData();
        entry1.setProductNumber(SKU);
        entry1.setPickupDate(null);
        entry1.setCodeSku(CODE_SKU_STAR);
        entry2.setCodeSku(CODE_SKU_X);
        entry2.setPickupDate(NOW);

        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(response));

        response.setProducts(asList(entry1, entry2));

        List<String> result = checkoutService.getInvalidCartEntries();

        assertThat(result).containsExactly(SKU);
    }

    @Test
    public void testGetInvalidCartEntries_withNullPickupDate_withStarSku() {
        when(cartService.hasSessionCart()).thenReturn(true);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        when(cartService.getSessionCart()).thenReturn(cart);

        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        EntryInfoResponseData entry1 = new EntryInfoResponseData();
        EntryInfoResponseData entry2 = new EntryInfoResponseData();
        entry1.setProductNumber(SKU);
        entry1.setPickupDate(null);
        entry1.setCodeSku(CODE_SKU_STAR);
        entry2.setCodeSku(CODE_SKU_L);
        entry2.setProductNumber(SKU);
        entry2.setPickupDate(NOW);

        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn(Optional.of(response));

        response.setProducts(asList(entry1, entry2));

        List<String> result = checkoutService.getInvalidCartEntries();

        assertThat(result).hasSameElementsAs(asList(SKU));
    }

    @Test
    public void testGetInvalidCartEntries_withNullPickupDate_withLSku() {
        when(cartService.hasSessionCart()).thenReturn(true);

        CartModel cart = CartModelMockBuilder
                .aCart()
                .build();

        when(cartService.getSessionCart()).thenReturn(cart);

        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        EntryInfoResponseData entry1 = new EntryInfoResponseData();
        EntryInfoResponseData entry2 = new EntryInfoResponseData();
        entry1.setProductNumber(SKU);
        entry1.setCodeSku(CODE_SKU_STAR);
        entry1.setEnoughStock(false);
        entry2.setCodeSku(CODE_SKU_L);
        entry2.setProductNumber(ANOTHER_SKU);
        entry2.setPickupDate(NOW);

        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn((Optional.of(response)));

        response.setProducts(asList(entry1, entry2));

        List<String> result = checkoutService.getInvalidCartEntries();

        assertThat(result).contains(SKU, ANOTHER_SKU);
    }


    @Test
    public void testValidEntrywithNullPickupDate_adjustableStockSellingOff() {
        when(cartService.hasSessionCart()).thenReturn(true);
        VariantProductModel productModel = VariantProductModelMockBuilder
                .aVariantProduct(SKU, null)
                .build();

        CartEntryModel cartEntryModel = CartEntryModelMockBuilder
                .anEntry()
                .withProduct(productModel)
                .build();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withEntries(cartEntryModel)
                .build();

        when(cartService.getSessionCart()).thenReturn(cart);

        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        EntryInfoResponseData entry1 = new EntryInfoResponseData();
        EntryInfoResponseData entry2 = new EntryInfoResponseData();
        entry1.setProductNumber(SKU);
        entry1.setPickupDate(null);
        entry1.setCodeSku(CODE_SKU_STAR);
        entry1.setEnoughStock(false);

        when(vmkVariantProductService.isSellingOffAlmostNOS(productModel)).thenReturn(true);
        when(vmkVariantProductService.getAvailableEDCStock(productModel)).thenReturn(10L);
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn((Optional.of(response)));

        response.setProducts(asList(entry1, entry2));

        List<String> result = checkoutService.getInvalidCartEntries();

        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void testValidEntrywithNullPickupDate_adjustableStockDiscontinued() {
        when(cartService.hasSessionCart()).thenReturn(true);
        VariantProductModel productModel = VariantProductModelMockBuilder
                .aVariantProduct(SKU, null)
                .build();

        CartEntryModel cartEntryModel = CartEntryModelMockBuilder
                .anEntry()
                .withProduct(productModel)
                .build();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withEntries(cartEntryModel)
                .build();

        when(cartService.getSessionCart()).thenReturn(cart);

        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        EntryInfoResponseData entry1 = new EntryInfoResponseData();
        EntryInfoResponseData entry2 = new EntryInfoResponseData();
        entry1.setProductNumber(SKU);
        entry1.setPickupDate(null);
        entry1.setCodeSku(CODE_SKU_STAR);
        entry1.setEnoughStock(false);

        when(vmkVariantProductService.isDiscontinuedProduct(productModel)).thenReturn(true);
        when(vmkVariantProductService.getAvailableEDCStock(productModel)).thenReturn(10L);
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn((Optional.of(response)));

        response.setProducts(asList(entry1, entry2));

        List<String> result = checkoutService.getInvalidCartEntries();

        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void testGetInvalidCartEntrY_withNullPickupDate_noEDCStock() {
        when(cartService.hasSessionCart()).thenReturn(true);
        VariantProductModel productModel = VariantProductModelMockBuilder
                .aVariantProduct(SKU, null)
                .build();

        CartEntryModel cartEntryModel = CartEntryModelMockBuilder
                .anEntry()
                .withProduct(productModel)
                .build();

        CartModel cart = CartModelMockBuilder
                .aCart()
                .withEntries(cartEntryModel)
                .build();

        when(cartService.getSessionCart()).thenReturn(cart);

        PickupDateInfoResponseData response = new PickupDateInfoResponseData();
        EntryInfoResponseData entry1 = new EntryInfoResponseData();
        EntryInfoResponseData entry2 = new EntryInfoResponseData();
        entry1.setProductNumber(SKU);
        entry1.setPickupDate(null);
        entry1.setCodeSku(CODE_SKU_STAR);
        entry1.setEnoughStock(false);

        when(vmkVariantProductService.isDiscontinuedProduct(productModel)).thenReturn(true);
        when(vmkVariantProductService.getAvailableEDCStock(productModel)).thenReturn(0L);
        when(deliveryInfoService.getPickupDateInformation(cart)).thenReturn((Optional.of(response)));

        response.setProducts(asList(entry1, entry2));

        List<String> result = checkoutService.getInvalidCartEntries();

        assertThat(result).contains(SKU);

    }
}