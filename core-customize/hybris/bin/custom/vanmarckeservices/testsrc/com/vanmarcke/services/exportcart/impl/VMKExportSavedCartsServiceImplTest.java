package com.vanmarcke.services.exportcart.impl;

import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.vanmarcke.facades.data.order.SavedCartData;
import com.vanmarcke.services.util.PdfUtils;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.media.MediaService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKExportSavedCartsServiceImplTest {

    public static final String TEST_CODE = "test_code";
    public static final String VANMARCKE_BLUE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA = "vanmarcke-blue-wishlist-pdf-export";
    public static final String VANMARCKE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA = "vanmarcke-wishlist-pdf-export";

    @Mock
    private ProductService productService;

    @Mock
    private MediaService mediaService;

    @Mock
    private PdfUtils pdfUtils;

    @InjectMocks
    @Spy
    VMKExportSavedCartsServiceImpl vmkExportSavedCartsService;

    @Test
    public void testPopulateTechnicalDataSheets() {
        // given
        SavedCartData savedCartData = mock(SavedCartData.class);
        OrderEntryData orderEntryData = mock(OrderEntryData.class);
        when(savedCartData.getEntries()).thenReturn(List.of(orderEntryData));
        ProductData productData = mock(ProductData.class);
        ProductModel productModel = mock(ProductModel.class);
        when(productData.getCode()).thenReturn(TEST_CODE);
        when(orderEntryData.getProduct()).thenReturn(productData);
        when(productService.getProductForCode(TEST_CODE)).thenReturn(productModel);
        MediaModel mediaModel = mock(MediaModel.class);
        when(mediaModel.getMime()).thenReturn(MediaType.APPLICATION_PDF_VALUE);
        when(mediaService.getStreamFromMedia(mediaModel)).thenReturn(mock(InputStream.class));
        when(productModel.getTech_data_sheet()).thenReturn(mediaModel);

        // when
        vmkExportSavedCartsService.populateTechnicalDataSheets(savedCartData);

        // then
        verify(productService).getProductForCode(TEST_CODE);
        verify(productModel).getTech_data_sheet();
        verify(mediaModel).getMime();
        verify(mediaService, times(1)).getStreamFromMedia(mediaModel);
    }

    @Test
    public void testGetWidthsWithShowImagesAndShowNet() {
        float[] widths = vmkExportSavedCartsService.getWidths(true, true);
        Assert.assertEquals(7, widths.length);
        Assert.assertEquals(3, widths[2], 0.1);
    }

    @Test
    public void testGetWidthsWithShowImagesAndNoShowNet() {
        float[] widths = vmkExportSavedCartsService.getWidths(true, false);
        Assert.assertEquals(6, widths.length);
        Assert.assertEquals(3, widths[2], 0.1);
    }

    @Test
    public void testGetWidthsWithNoShowImagesAndShowNet() {
        float[] widths = vmkExportSavedCartsService.getWidths(false, true);
        Assert.assertEquals(6, widths.length);
        Assert.assertEquals(3, widths[1], 0.1);
    }


    @Test
    public void testGetWidthsWithNoShowImagesAndNoShowNet() {
        float[] widths = vmkExportSavedCartsService.getWidths(false, false);
        Assert.assertEquals(5, widths.length);
        Assert.assertEquals(3, widths[1], 0.1);
    }

    @Test
    public void testCreatePdfHeader() throws IOException {
        // given
        Document document = mock(Document.class);
        SavedCartData savedCartData = mock(SavedCartData.class);
        UserModel userModel = mock(UserModel.class);

        MediaModel headerImgBlue = mock(MediaModel.class);
        MediaModel headerImg = mock(MediaModel.class);
        when(mediaService.getMedia(VANMARCKE_BLUE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA)).thenReturn(headerImgBlue);
        when(mediaService.getMedia(VANMARCKE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA)).thenReturn(headerImg);
        byte[] mockByteArray = new byte[]{};
        Image image = mock(Image.class);
        when(mediaService.getDataFromMedia(headerImgBlue)).thenReturn(mockByteArray);
        when(mediaService.getDataFromMedia(headerImg)).thenReturn(mockByteArray);
        doReturn(image).when(vmkExportSavedCartsService).getImage(mockByteArray);

        // when
        vmkExportSavedCartsService.createPdfHeader(document, savedCartData, userModel);

        // then
        verify(mediaService).getMedia(VANMARCKE_BLUE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA);
        verify(mediaService).getMedia(VANMARCKE_HEADER_SAVEDCART_PDF_EXPORT_MEDIA);
        verify(vmkExportSavedCartsService, times(2)).getImage(any());
        verify(userModel, times(1)).getUid();
        verify(savedCartData, times(1)).getName();
    }

    @Test
    public void testAddImageWithShowImages() throws IOException {
        // given
        PdfPTable pdfTable = mock(PdfPTable.class);
        ProductModel productModel = mock(ProductModel.class);
        Image image = mock(Image.class);
        MediaModel thumbnail = mock(MediaModel.class);
        when(productModel.getThumbnail()).thenReturn(thumbnail);
        byte[] mockByteArray = new byte[]{};

        when(mediaService.getDataFromMedia(thumbnail)).thenReturn(mockByteArray);

        doReturn(image).when(vmkExportSavedCartsService).getImage(mockByteArray);

        // when
        vmkExportSavedCartsService.addImage(pdfTable, true, productModel);

        // then
        verify(pdfTable).addCell(any(PdfPCell.class));
        verify(productModel).getThumbnail();
        verify(mediaService).getDataFromMedia(thumbnail);
    }
}