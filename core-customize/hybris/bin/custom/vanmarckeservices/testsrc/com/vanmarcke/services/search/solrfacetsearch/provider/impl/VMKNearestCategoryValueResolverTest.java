package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKNearestCategoryValueResolverTest extends AbstractValueResolverTest {

    @Mock
    private InputDocument inputDocument;
    @Mock
    private IndexerBatchContext indexerBatchContext;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private CategoryService categoryService;
    @InjectMocks
    private VMKNearestCategoryValueResolver vmkNearestCategoryValueResolver;

    @Test
    public void testAddFieldValuesWhenProductModel() throws FieldValueProviderException {
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        when(categoryService.getCategoryPathForProduct(productModel, CategoryModel.class)).thenReturn(singletonList(categoryModel));
        when(categoryModel.getName()).thenReturn("categoryName");
        vmkNearestCategoryValueResolver.resolve(inputDocument, indexerBatchContext, singletonList(indexedProperty), productModel);
        verify(inputDocument).addField(eq(indexedProperty), eq("categoryName"), anyString());
    }

    @Test
    public void testAddFieldValuesWhenVariantModel() throws FieldValueProviderException {
        VariantProductModel variantProductModel = mock(VariantProductModel.class);
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        when(variantProductModel.getBaseProduct()).thenReturn(productModel);
        when(categoryService.getCategoryPathForProduct(productModel, CategoryModel.class)).thenReturn(singletonList(categoryModel));
        when(categoryModel.getName()).thenReturn("categoryName");
        vmkNearestCategoryValueResolver.resolve(inputDocument, indexerBatchContext, singletonList(indexedProperty), productModel);
        verify(inputDocument).addField(eq(indexedProperty), eq("categoryName"), anyString());
    }

    @Test
    public void testAddFieldValuesWhenMultipleCategories() throws FieldValueProviderException {
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);
        CategoryModel categoryModel2 = mock(CategoryModel.class);
        when(categoryService.getCategoryPathForProduct(productModel, CategoryModel.class)).thenReturn(asList(categoryModel1, categoryModel2));
        when(categoryModel2.getCode()).thenReturn("categoryCode");
        vmkNearestCategoryValueResolver.resolve(inputDocument, indexerBatchContext, singletonList(indexedProperty), productModel);
        verify(inputDocument).addField(eq(indexedProperty), eq("categoryCode"), anyString());
    }
}