package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.ProductExportCronJobModel;
import com.vanmarcke.services.ftp.FTPClient;
import com.vanmarcke.services.product.VMKProductExportService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKProductExportJobPerformableTest} class contains the unit tests for the {@link VMKProductExportJobPerformable}
 * class.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 30/08/2018
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductExportJobPerformableTest {

    @Mock
    private VMKProductExportService productExportService;

    @Mock
    private FTPClient ftpClient;

    @InjectMocks
    private VMKProductExportJobPerformable productExportJob;

    @Test
    public void testPerform() throws JAXBException, IOException, XMLStreamException {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        ProductExportCronJobModel job = mock(ProductExportCronJobModel.class);
        when(job.getCatalogVersion()).thenReturn(catalogVersion);

        File file = new File("test-file.xml");
        file.createNewFile();

        when(productExportService.generateProductFeed(catalogVersion, "product-feed-hybris.xml")).thenReturn(file);

        PerformResult result = productExportJob.perform(job);

        Assertions
                .assertThat(result)
                .isNotNull();

        Assertions
                .assertThat(result.getResult())
                .isEqualTo(CronJobResult.SUCCESS);

        Assertions
                .assertThat(result.getStatus())
                .isEqualTo(CronJobStatus.FINISHED);

        verify(ftpClient).send(any(InputStream.class), eq("product-feed-hybris.xml"));
    }

    @Test
    public void testPerformWithError() throws JAXBException, IOException, XMLStreamException {
        CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

        ProductExportCronJobModel job = mock(ProductExportCronJobModel.class);
        when(job.getCatalogVersion()).thenReturn(catalogVersion);

        IOException exception = mock(IOException.class);

        when(productExportService.generateProductFeed(catalogVersion, "product-feed-hybris.xml")).thenThrow(exception);

        PerformResult result = productExportJob.perform(job);

        Assertions
                .assertThat(result)
                .isNotNull();

        Assertions
                .assertThat(result.getResult())
                .isEqualTo(CronJobResult.ERROR);

        Assertions
                .assertThat(result.getStatus())
                .isEqualTo(CronJobStatus.ABORTED);

        verify(ftpClient, never()).send(any(InputStream.class), anyString());
    }
}