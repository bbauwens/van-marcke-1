package com.vanmarcke.services.action;

import com.vanmarcke.services.strategies.VMKCommercePaymentCaptureStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.core.enums.OrderStatus.PAYMENT_CAPTURED;
import static de.hybris.platform.core.enums.OrderStatus.PAYMENT_NOT_CAPTURED;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKValidatePaymentCaptureStatusWithRetryActionTest {

    @Mock
    private VMKCommercePaymentCaptureStrategy vmkCommercePaymentCaptureStrategy;
    @Mock
    private ModelService modelService;
    @InjectMocks
    private VMKValidatePaymentCaptureStatusWithRetryAction vmkValidatePaymentCaptureStatusWithRetryAction;

    @Test
    public void testExecuteAction() {
        OrderProcessModel process = mock(OrderProcessModel.class);
        OrderModel order = mock(OrderModel.class);

        when(process.getOrder()).thenReturn(order);
        when(vmkCommercePaymentCaptureStrategy.isPaymentCapturedForOrder(order)).thenReturn(Boolean.TRUE);
        Transition result = vmkValidatePaymentCaptureStatusWithRetryAction.executeAction(process);

        verify(vmkCommercePaymentCaptureStrategy).isPaymentCapturedForOrder(order);
        verify(order).setStatus(PAYMENT_CAPTURED);
        verify(modelService).save(order);
        assertThat(result).isEqualTo(Transition.OK);
    }

    @Test
    public void testExecuteAction_PaymentNotYetCaptured() {
        OrderProcessModel process = mock(OrderProcessModel.class);
        OrderModel order = mock(OrderModel.class);
        PaymentTransactionEntryModel paymentTransactionEntry = mock(PaymentTransactionEntryModel.class);

        when(process.getOrder()).thenReturn(order);
        when(vmkCommercePaymentCaptureStrategy.isPaymentCapturedForOrder(order)).thenReturn(Boolean.FALSE);
        when(vmkCommercePaymentCaptureStrategy.capturePaymentAmount(order)).thenReturn(paymentTransactionEntry);
        when(vmkCommercePaymentCaptureStrategy.isPaymentCapturedForEntry(paymentTransactionEntry)).thenReturn(Boolean.TRUE);
        Transition result = vmkValidatePaymentCaptureStatusWithRetryAction.executeAction(process);

        verify(vmkCommercePaymentCaptureStrategy).isPaymentCapturedForOrder(order);
        verify(vmkCommercePaymentCaptureStrategy).capturePaymentAmount(order);
        verify(vmkCommercePaymentCaptureStrategy).isPaymentCapturedForEntry(paymentTransactionEntry);
        verify(order).setStatus(PAYMENT_CAPTURED);
        verify(modelService).save(order);
        assertThat(result).isEqualTo(Transition.OK);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteAction_PaymentNotYetCaptured_RecaptureFailed() {
        OrderProcessModel process = mock(OrderProcessModel.class);
        OrderModel order = mock(OrderModel.class);
        PaymentTransactionEntryModel paymentTransactionEntry = mock(PaymentTransactionEntryModel.class);

        when(process.getOrder()).thenReturn(order);
        when(vmkCommercePaymentCaptureStrategy.isPaymentCapturedForOrder(order)).thenReturn(Boolean.FALSE);
        when(vmkCommercePaymentCaptureStrategy.capturePaymentAmount(order)).thenReturn(paymentTransactionEntry);
        when(vmkCommercePaymentCaptureStrategy.isPaymentCapturedForEntry(paymentTransactionEntry)).thenReturn(Boolean.FALSE);
        vmkValidatePaymentCaptureStatusWithRetryAction.executeAction(process);

        verify(vmkCommercePaymentCaptureStrategy).isPaymentCapturedForOrder(order);
        verify(vmkCommercePaymentCaptureStrategy).capturePaymentAmount(order);
        verify(vmkCommercePaymentCaptureStrategy).isPaymentCapturedForEntry(paymentTransactionEntry);
        verify(order).setStatus(PAYMENT_NOT_CAPTURED);
        verify(modelService).save(order);
    }
}