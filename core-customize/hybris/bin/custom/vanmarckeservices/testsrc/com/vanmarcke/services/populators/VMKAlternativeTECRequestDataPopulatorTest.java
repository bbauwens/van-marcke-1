package com.vanmarcke.services.populators;

import com.vanmarcke.cpi.data.order.AlternativeTECLineItemRequestData;
import com.vanmarcke.cpi.data.order.AlternativeTECRequestData;
import com.vanmarcke.services.b2b.VMKB2BUnitService;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAlternativeTECRequestDataPopulatorTest {

    @Mock
    private VMKBaseStoreService vmkBaseStoreService;

    @Mock
    private VMKB2BUnitService b2bUnitService;

    @InjectMocks
    private VMKAlternativeTECRequestDataPopulator vmkAlternativeTECRequestDataPopulator;

    @Test
    public void testPopulate() {
        Map<String, Long> source = new HashMap<>();
        source.put("1234", 1L);
        AlternativeTECRequestData target = new AlternativeTECRequestData();
        PointOfServiceModel pointOfService = mock(PointOfServiceModel.class);
        BaseStoreModel baseStore = mock(BaseStoreModel.class);

        when(vmkBaseStoreService.getCurrentBaseStore()).thenReturn(baseStore);
        when(baseStore.getPointsOfService()).thenReturn(Collections.singletonList(pointOfService));
        when(pointOfService.getName()).thenReturn("EA");
        when(b2bUnitService.getCustomerB2BUnitID()).thenReturn("V99909");
        vmkAlternativeTECRequestDataPopulator.populate(source, target);

        verify(vmkBaseStoreService).getCurrentBaseStore();
        assertThat(target.getStoreIDs()).isNotEmpty().containsExactly("EA");
        assertThat(target.getLineItems()).isNotEmpty();
        assertThat(target.getCustomerID()).isEqualTo("V99909");
        AlternativeTECLineItemRequestData resultLine = target.getLineItems().get(0);
        assertThat(resultLine.getProductID()).isEqualTo("1234");
        assertThat(resultLine.getQuantity()).isEqualTo(1L);
    }
}
