package com.vanmarcke.services.model.builder;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code UserModelMockBuilder} class can be used to create a mock implementation of the
 * {@link UserModel} class.
 *
 * @author Christiaan Janssen
 * @since 20-04-2020
 */
public class UserModelMockBuilder<S extends UserModelMockBuilder, T extends UserModel> {

    protected final S myself;

    protected String uid;
    private PointOfServiceModel sessionStore;
    private boolean showNetPrice;

    /**
     * Creates a new instance of the {@link UserModelMockBuilder} class.
     *
     * @param type the type
     */
    protected UserModelMockBuilder(Class<S> type) {
        myself = type.cast(this);
    }

    /**
     * The entry point to the {@link UserModelMockBuilder}.
     *
     * @return the {@link UserModelMockBuilder} instance for method chaining
     */
    public static UserModelMockBuilder aUser() {
        return new UserModelMockBuilder(UserModelMockBuilder.class);
    }

    /**
     * Sets the {@code code} on the current {@link UserModelMockBuilder} instance
     *
     * @param uid the user's unique identifier
     * @return the {@link UserModelMockBuilder} instance for method chaining
     */
    public UserModelMockBuilder withUID(String uid) {
        this.uid = uid;
        return this;
    }

    /**
     * Sets the {@code code} on the current {@link UserModelMockBuilder} instance
     *
     * @param sessionStore the session store
     * @return the {@link UserModelMockBuilder} instance for method chaining
     */
    public UserModelMockBuilder withSessionStore(PointOfServiceModel sessionStore) {
        this.sessionStore = sessionStore;
        return this;
    }

    /**
     * Sets the showNetPrice on the the current {@link UserModelMockBuilder} instance
     * @param showNetPrice the show net price
     * @return the {@link UserModelMockBuilder} instance for method chaining
     */
    public UserModelMockBuilder withShowNetPrice(boolean showNetPrice) {
        this.showNetPrice = showNetPrice;
        return this;
    }


    /**
     * Creates a mock implementation of the {@link UserModel} class.
     *
     * @return the {@link UserModel} instance
     */
    public UserModel build() {
        UserModel user = mock(UserModel.class);
        when(user.getUid()).thenReturn(uid);
        when(user.getSessionStore()).thenReturn(sessionStore);
        when(user.getShowNetPrice()).thenReturn(showNetPrice);
        return user;
    }
}
