package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateRequestData;
import com.vanmarcke.cpi.data.order.PickupDateResponseData;
import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateResponseData;
import com.vanmarcke.cpi.services.ESBPickupDateService;
import com.vanmarcke.cpi.services.ESBShippingDateService;
import com.vanmarcke.services.model.builder.CartModelMockBuilder;
import com.vanmarcke.services.request.VMKYRequestService;
import com.vanmarcke.services.request.impl.VMKYRequestServiceImpl;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKDeliveryInfoServiceImplTest {

    private CartModel cart;
    private PickupDateInfoResponseData pickupResponseData;
    private ShippingDateInfoResponseData shippingResponseData;
    private PickupDateResponseData pickupResponse;
    private ShippingDateResponseData shippingResponse;

    @Mock
    private ESBPickupDateService esbPickupDateService;
    @Mock
    private ESBShippingDateService esbShippingDateService;

    @Mock
    private Converter<AbstractOrderModel, PickupDateRequestData> pickupDateRequestConverter;
    @Mock
    private Converter<AbstractOrderModel, ShippingDateRequestData> shippingDateRequestConverter;

    private VMKYRequestService yRequestService = new VMKYRequestServiceImpl();

    private VMKDeliveryInfoServiceImpl deliveryInfoService;

    @Before
    public void setUp() throws Exception {
        deliveryInfoService = spy(new VMKDeliveryInfoServiceImpl(
                esbPickupDateService,
                esbShippingDateService,
                yRequestService, pickupDateRequestConverter,
                shippingDateRequestConverter));
        cart = CartModelMockBuilder.aCart().build();

        pickupResponseData = new PickupDateInfoResponseData();
        pickupResponse = new PickupDateResponseData();
        shippingResponseData = new ShippingDateInfoResponseData();
        shippingResponse = new ShippingDateResponseData();
    }

    @Test
    public void testGetPickupDateInfo_validResponse() {
        PickupDateRequestData request = new PickupDateRequestData();
        pickupResponse.setData(pickupResponseData);
        when(pickupDateRequestConverter.convert(cart)).thenReturn(request);
        when(esbPickupDateService.getPickupDateInformation(request)).thenReturn(pickupResponse);

        assertThat(deliveryInfoService.getPickupDateInformation(cart)).hasValue(pickupResponseData);
    }

    @Test
    public void testGetPickupDateInfo_invalidResponse() {
        PickupDateRequestData request = new PickupDateRequestData();
        when(pickupDateRequestConverter.convert(cart)).thenReturn(request);
        when(esbPickupDateService.getPickupDateInformation(request)).thenReturn(pickupResponse);

        assertThat(deliveryInfoService.getPickupDateInformation(cart)).isEmpty();
    }

    @Test
    public void testIsValidPickResponse_responseNull() {
        PickupDateRequestData request = new PickupDateRequestData();
        when(pickupDateRequestConverter.convert(cart)).thenReturn(request);
        when(esbPickupDateService.getPickupDateInformation(request)).thenReturn(null);

        assertThat(deliveryInfoService.getPickupDateInformation(cart)).isEmpty();
    }

    @Test
    public void testGetShippingInfo_validResponse() {
        ShippingDateRequestData request = new ShippingDateRequestData();
        shippingResponse.setData(shippingResponseData);
        when(shippingDateRequestConverter.convert(cart)).thenReturn(request);
        when(esbShippingDateService.getShippingDateInformation(request)).thenReturn(shippingResponse);

        assertThat(deliveryInfoService.getShippingDateInformation(cart)).hasValue(shippingResponseData);
    }

    @Test
    public void testIsValidShippingResponse_responseNull() {
        ShippingDateRequestData request = new ShippingDateRequestData();
        when(shippingDateRequestConverter.convert(cart)).thenReturn(request);
        when(esbShippingDateService.getShippingDateInformation(request)).thenReturn(null);

        assertThat(deliveryInfoService.getShippingDateInformation(cart)).isEmpty();
    }

    @Test
    public void testIsValidShippingResponse_dataNull() {
        ShippingDateRequestData request = new ShippingDateRequestData();
        when(shippingDateRequestConverter.convert(cart)).thenReturn(request);
        when(esbShippingDateService.getShippingDateInformation(request)).thenReturn(shippingResponse);

        assertThat(deliveryInfoService.getShippingDateInformation(cart)).isEmpty();
    }
}
