package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.ProductClassificationExportCronJobModel;
import com.vanmarcke.facades.data.ProductExportFeedResult;
import com.vanmarcke.services.ftp.FTPClient;
import com.vanmarcke.services.product.VMKProductClassificationExportService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class VMKProductClassificationExportJobPerformableTest {

    @Mock
    private VMKProductClassificationExportService productClassificationExportService;
    @Mock
    private FTPClient ftpClient;

    @InjectMocks
    private VMKProductClassificationExportJobPerformable productClassificationExportJobPerformable;

    @Test
    public void testPerform() throws Exception {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final ClassificationSystemVersionModel classificationSystemVersion = mock(ClassificationSystemVersionModel.class);
        final Date lastModifiedTime = mock(Date.class);
        final ProductClassificationExportCronJobModel productClassificationExportCronJob = mock(ProductClassificationExportCronJobModel.class);
        final ProductExportFeedResult productExportFeedResult = mock(ProductExportFeedResult.class);
        when(productClassificationExportCronJob.getCatalogVersion()).thenReturn(catalogVersion);
        when(productClassificationExportCronJob.getSystemVersion()).thenReturn(classificationSystemVersion);
        when(productClassificationExportCronJob.getLastModifiedTime()).thenReturn(lastModifiedTime);
        when(productClassificationExportCronJob.getFeedName()).thenReturn("etim-product-feed-hybris.csv");
        when(productExportFeedResult.getFeed()).thenReturn("etim-product-feed-content");

        when(productClassificationExportService.generateProductClassificationFeed(catalogVersion, classificationSystemVersion, lastModifiedTime)).thenReturn(productExportFeedResult);

        final PerformResult result = productClassificationExportJobPerformable.perform(productClassificationExportCronJob);
        assertThat(result).isNotNull();
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

        verify(ftpClient).send(any(ByteArrayInputStream.class), eq("etim-product-feed-hybris.csv"));
    }

    @Test
    public void testPerform_withError() throws Exception {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final ClassificationSystemVersionModel classificationSystemVersion = mock(ClassificationSystemVersionModel.class);
        final Date lastModifiedTime = mock(Date.class);
        final ProductClassificationExportCronJobModel productClassificationExportCronJob = mock(ProductClassificationExportCronJobModel.class);
        when(productClassificationExportCronJob.getCatalogVersion()).thenReturn(catalogVersion);
        when(productClassificationExportCronJob.getSystemVersion()).thenReturn(classificationSystemVersion);
        when(productClassificationExportCronJob.getLastModifiedTime()).thenReturn(lastModifiedTime);
        when(productClassificationExportCronJob.getFeedName()).thenReturn("etim-product-feed-hybris.csv");

        when(productClassificationExportService.generateProductClassificationFeed(catalogVersion, classificationSystemVersion, lastModifiedTime)).thenThrow(Exception.class);

        final PerformResult result = productClassificationExportJobPerformable.perform(productClassificationExportCronJob);
        assertThat(result).isNotNull();
        assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);

        verifyZeroInteractions(ftpClient);
    }

    @Test
    public void testPerform_withEmptyProductFeed() throws Exception {
        final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
        final ClassificationSystemVersionModel classificationSystemVersion = mock(ClassificationSystemVersionModel.class);
        final Date lastModifiedTime = mock(Date.class);
        final ProductClassificationExportCronJobModel productClassificationExportCronJob = mock(ProductClassificationExportCronJobModel.class);
        final ProductExportFeedResult productExportFeedResult = mock(ProductExportFeedResult.class);
        when(productClassificationExportCronJob.getCatalogVersion()).thenReturn(catalogVersion);
        when(productClassificationExportCronJob.getSystemVersion()).thenReturn(classificationSystemVersion);
        when(productClassificationExportCronJob.getLastModifiedTime()).thenReturn(lastModifiedTime);
        when(productClassificationExportCronJob.getFeedName()).thenReturn("etim-product-feed-hybris.csv");
        when(productExportFeedResult.getFeed()).thenReturn("");

        when(productClassificationExportService.generateProductClassificationFeed(catalogVersion, classificationSystemVersion, lastModifiedTime)).thenReturn(productExportFeedResult);

        final PerformResult result = productClassificationExportJobPerformable.perform(productClassificationExportCronJob);
        assertThat(result).isNotNull();
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

        verifyZeroInteractions(ftpClient);
    }
}