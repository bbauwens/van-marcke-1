package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.helper.ProductAndCategoryHelper;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductModelUrlResolverTest {

    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private CommerceCategoryService commerceCategoryService;
    @Mock
    private ProductAndCategoryHelper productAndCategoryHelper;
    @InjectMocks
    private VMKProductModelUrlResolver productModelUrlResolver;

    @Before
    public void setup() {
        productModelUrlResolver.setDefaultPattern("/{category-path}/{product-name}/p/{product-code}");
    }

    @Test
    public void testResolveInternal() {
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);

        when(categoryModel1.getName()).thenReturn("parent-name1");

        when(productModel.getSupercategories()).thenReturn(singletonList(categoryModel));

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(singletonList(categoryModel1)));
        when(productAndCategoryHelper.isValidProductCategory(categoryModel)).thenReturn(Boolean.TRUE);

        when(productAndCategoryHelper.getBaseProduct(productModel)).thenReturn(productModel);

        when(productModel.getCode()).thenReturn("product-code");
        when(productModel.getName()).thenReturn("product-name");

        String result = productModelUrlResolver.resolveInternal(productModel);

        assertThat(result).isEqualTo("/parent-name1/product-name/p/product-code");
    }

    @Test
    public void testResolveInternal_withoutCategoriesAndwithEmptyProductName() {
        ProductModel productModel = mock(ProductModel.class);

        when(productModel.getCode()).thenReturn("product-code");

        when(productAndCategoryHelper.getBaseProduct(productModel)).thenReturn(productModel);

        String result = productModelUrlResolver.resolveInternal(productModel);

        assertThat(result).isEqualTo("/c/p/product-code");
    }

    @Test
    public void testResolveInternal_withoutCategories() {
        ProductModel productModel = mock(ProductModel.class);

        when(productModel.getCode()).thenReturn("product-code");
        when(productModel.getName()).thenReturn("product-name");

        when(productAndCategoryHelper.getBaseProduct(productModel)).thenReturn(productModel);

        String result = productModelUrlResolver.resolveInternal(productModel);

        assertThat(result).isEqualTo("/c/product-name/p/product-code");
    }

    @Test
    public void testResolveInternal_withEmptyCategoryNames() {
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);

        when(productModel.getSupercategories()).thenReturn(singletonList(categoryModel));

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(singletonList(categoryModel1)));

        when(productAndCategoryHelper.getBaseProduct(productModel)).thenReturn(productModel);

        when(productModel.getCode()).thenReturn("product-code");
        when(productModel.getName()).thenReturn("product-name");

        String result = productModelUrlResolver.resolveInternal(productModel);

        assertThat(result).isEqualTo("/c/product-name/p/product-code");
    }

    @Test
    public void testResolveInternal_withEmptyCategoryNamesAndEmptyProductName() {
        CategoryModel categoryModel = mock(CategoryModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);

        ProductModel productModel = mock(ProductModel.class);

        when(productModel.getSupercategories()).thenReturn(singletonList(categoryModel));

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(singletonList(categoryModel1)));

        when(productAndCategoryHelper.getBaseProduct(productModel)).thenReturn(productModel);

        when(productModel.getCode()).thenReturn("product-code");

        String result = productModelUrlResolver.resolveInternal(productModel);

        assertThat(result).isEqualTo("/c/p/product-code");
    }

    @Test
    public void testResolveInternalLocalized() {
        ProductModel productModel = mock(ProductModel.class);
        CategoryModel categoryModel = mock(CategoryModel.class);
        CategoryModel categoryModel1 = mock(CategoryModel.class);

        when(categoryModel1.getName()).thenReturn("parent-name1");
        when(categoryModel1.getName(Locale.ENGLISH)).thenReturn("parent-name1-en");
        when(productModel.getSupercategories()).thenReturn(singletonList(categoryModel));

        when(commerceCategoryService.getPathsForCategory(categoryModel)).thenReturn(singletonList(singletonList(categoryModel1)));
        when(productAndCategoryHelper.isValidProductCategory(categoryModel)).thenReturn(Boolean.TRUE);

        when(productAndCategoryHelper.getBaseProduct(productModel)).thenReturn(productModel);

        when(productModel.getCode()).thenReturn("product-code");
        when(productModel.getName(Locale.ENGLISH)).thenReturn("product-name-en");

        String result = productModelUrlResolver.resolveInternal(productModel, Locale.ENGLISH);

        assertThat(result).isEqualTo("/parent-name1-en/product-name-en/p/product-code");
    }

}