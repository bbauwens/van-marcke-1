package com.vanmarcke.services.action;

import be.elision.mandrillextension.service.MandrillService;
import com.vanmarcke.email.converters.impl.MandrillOrderConfirmationConverter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSendOrderConfirmationEmailActionTest {

    private static final String LANGUAGE_ISO = "en";
    private static final String TEMPLATE = RandomStringUtils.random(10);
    private static final String EMAIL = RandomStringUtils.random(10);
    private static final String ORDER_MANAGER_EMAIL = RandomStringUtils.random(10);

    @Mock
    private ModelService modelService;

    @Mock
    private MandrillService mandrillService;

    @Mock
    private MandrillOrderConfirmationConverter mandrillOrderConfirmationConverter;

    @InjectMocks
    private VMKSendOrderConfirmationEmailAction vmkSendOrderConfirmationEmailAction;

    @Test
    public void testExecuteAction_notDIY() throws Exception {
        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(LANGUAGE_ISO);

        BaseSiteModel site = mock(BaseSiteModel.class);
        when(site.getChannel()).thenReturn(SiteChannel.B2B);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getOrderConfirmationEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);

        CustomerModel customer = mock(CustomerModel.class);
        when(customer.getUid()).thenReturn(EMAIL);

        OrderModel order = mock(OrderModel.class);
        when(order.getLanguage()).thenReturn(language);
        when(order.getSite()).thenReturn(site);
        when(order.getStore()).thenReturn(store);
        when(order.getUser()).thenReturn(customer);

        OrderProcessModel process = mock(OrderProcessModel.class);
        when(process.getOrder()).thenReturn(order);

        Transition result = vmkSendOrderConfirmationEmailAction.executeAction(process);

        assertThat(result).isEqualTo(Transition.OK);

        String[] toArray = new String[]{EMAIL};

        verify(order).setStatus(OrderStatus.CONFIRMATION_SENT);
        verify(mandrillService).send(TEMPLATE, order, mandrillOrderConfirmationConverter, toArray, null, null);
        verify(modelService).save(order);
    }

    @Test
    public void testExecuteAction_DIY() throws Exception {
        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(LANGUAGE_ISO);

        BaseSiteModel site = mock(BaseSiteModel.class);
        when(site.getChannel()).thenReturn(SiteChannel.DIY);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getOrderConfirmationEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);
        when(store.getOrderManagerEmail()).thenReturn(ORDER_MANAGER_EMAIL);
        CustomerModel customer = mock(CustomerModel.class);
        when(customer.getUid()).thenReturn(EMAIL);

        OrderModel order = mock(OrderModel.class);
        when(order.getLanguage()).thenReturn(language);
        when(order.getSite()).thenReturn(site);
        when(order.getStore()).thenReturn(store);
        when(order.getUser()).thenReturn(customer);

        OrderProcessModel process = mock(OrderProcessModel.class);
        when(process.getOrder()).thenReturn(order);

        Transition result = vmkSendOrderConfirmationEmailAction.executeAction(process);

        assertThat(result).isEqualTo(Transition.OK);

        String[] toArray = new String[]{EMAIL};
        String[] bccArray = new String[]{ORDER_MANAGER_EMAIL};

        verify(order).setStatus(OrderStatus.CONFIRMATION_SENT);
        verify(mandrillService).send(TEMPLATE, order, mandrillOrderConfirmationConverter, toArray, null, bccArray);
        verify(modelService).save(order);
    }

    @Test
    public void testExecuteAction_withFailure() throws Exception {
        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(LANGUAGE_ISO);

        BaseSiteModel site = mock(BaseSiteModel.class);
        when(site.getChannel()).thenReturn(SiteChannel.B2B);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getOrderConfirmationEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);

        CustomerModel customer = mock(CustomerModel.class);
        when(customer.getUid()).thenReturn(EMAIL);

        OrderModel order = mock(OrderModel.class);
        when(order.getLanguage()).thenReturn(language);
        when(order.getSite()).thenReturn(site);
        when(order.getStore()).thenReturn(store);
        when(order.getUser()).thenReturn(customer);

        OrderProcessModel process = mock(OrderProcessModel.class);
        when(process.getOrder()).thenReturn(order);

        String[] toArray = new String[]{EMAIL};

        when(mandrillService.send(TEMPLATE, order, mandrillOrderConfirmationConverter, toArray, null, null)).thenThrow(Exception.class);

        Transition result = vmkSendOrderConfirmationEmailAction.executeAction(process);

        assertThat(result).isEqualTo(Transition.NOK);

        verify(order).setStatus(OrderStatus.CONFIRMATION_NOT_SENT);
        verify(modelService).save(order);
    }
}