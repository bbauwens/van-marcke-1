package com.vanmarcke.services.model.builder;

import com.vanmarcke.core.enums.ConsentType;
import com.vanmarcke.core.enums.OptType;
import com.vanmarcke.core.model.VMKConsentModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@code VMKConsentModelMockBuilder} class can be used to create a mock implementation of the
 * {@link VMKConsentModel} class.
 *
 * @author Christiaan Janssen
 * @since 20-04-2020
 */
public class VMKConsentModelMockBuilder {

    private ConsentType consentType;
    private OptType optType;

    /**
     * Creates a new instance of the {@link VMKConsentModelMockBuilder} class.
     */
    private VMKConsentModelMockBuilder() {
    }

    /**
     * The entry point to the {@link VMKConsentModelMockBuilder}.
     *
     * @return the {@link VMKConsentModelMockBuilder} instance for method chaining
     */
    public static VMKConsentModelMockBuilder aConsent() {
        return new VMKConsentModelMockBuilder();
    }

    /**
     * Sets the {@code consentType} on the current {@link VMKConsentModelMockBuilder} instance
     *
     * @param consentType the consent type
     * @return the {@link VMKConsentModelMockBuilder} instance for method chaining
     */
    public VMKConsentModelMockBuilder withConsentType(ConsentType consentType) {
        this.consentType = consentType;
        return this;
    }

    /**
     * Sets the {@code optType} on the current {@link VMKConsentModelMockBuilder} instance
     *
     * @param optType the opt type
     * @return the {@link VMKConsentModelMockBuilder} instance for method chaining
     */
    public VMKConsentModelMockBuilder withOptType(OptType optType) {
        this.optType = optType;
        return this;
    }

    /**
     * Creates a mock implementation of the {@link VMKConsentModel} class.
     *
     * @return the {@link VMKConsentModel} instance
     */
    public VMKConsentModel build() {
        VMKConsentModel consent = mock(VMKConsentModel.class);
        when(consent.getConsentType()).thenReturn(consentType);
        when(consent.getOptType()).thenReturn(optType);
        return consent;
    }
}
