package com.vanmarcke.services.builder;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemModel;

import java.util.Set;

public class ClassificationSystemModelBuilder {

    private String id;
    private Set<CatalogVersionModel> catalogVersions;
    private CatalogVersionModel activeCatalogVersion;

    /**
     * Private constructor to hide the implicit default one.
     */
    private ClassificationSystemModelBuilder() {
    }

    /**
     * A new instance of the {@link ClassificationSystemModelBuilder} is created and returned
     *
     * @return the created {@link ClassificationSystemModelBuilder} instance
     */
    public static ClassificationSystemModelBuilder aCatalogModel() {
        return new ClassificationSystemModelBuilder();
    }

    /**
     * Sets the given {@code classificationSystemModel} on the current {@link ClassificationSystemModelBuilder} instance.
     *
     * @param id the id
     * @return the current {@link ProductExportItemBuilder} instance for method chaining
     */
    public ClassificationSystemModelBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public ClassificationSystemModelBuilder withCatalogVersions(final Set<CatalogVersionModel> catalogVersions) {
        this.catalogVersions = catalogVersions;
        return this;
    }

    public ClassificationSystemModelBuilder withActiveCatalogVersion(final CatalogVersionModel activeCatalogVersion) {
        this.activeCatalogVersion = activeCatalogVersion;
        return this;
    }

    /**
     * Returns the current {@link catalog} instance
     *
     * @return the current {@link catalog} instance
     */
    public ClassificationSystemModel build() {
        final ClassificationSystemModel classificationSystemModel = new ClassificationSystemModel();
        classificationSystemModel.setActiveCatalogVersion(activeCatalogVersion);
        classificationSystemModel.setCatalogVersions(catalogVersions);
        classificationSystemModel.setId(id);
        return classificationSystemModel;
    }
}
