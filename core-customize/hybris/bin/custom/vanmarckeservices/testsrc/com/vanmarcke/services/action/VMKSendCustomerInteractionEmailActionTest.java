package com.vanmarcke.services.action;

import be.elision.mandrillextension.service.MandrillService;
import com.vanmarcke.core.enums.CustomerInteractionStatus;
import com.vanmarcke.core.model.CustomerInteractionModel;
import com.vanmarcke.core.model.CustomerInteractionProcessModel;
import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.core.model.VanmarckeContactModel;
import com.vanmarcke.email.converters.impl.MandrillCustomerInteractionConverter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static com.vanmarcke.core.enums.CustomerInteractionType.CREDIT_CONTROLLER;
import static com.vanmarcke.core.enums.CustomerInteractionType.ORDER_MANAGER;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKSendCustomerInteractionEmailActionTest {

    private static final String LANGUAGE_ISO = "en";
    private static final String TEMPLATE = RandomStringUtils.random(10);
    private static final String EMAIL = RandomStringUtils.random(10);
    private static final String CUSTOMER_UID = RandomStringUtils.random(10);

    @Mock
    private ModelService modelService;

    @Mock
    private MandrillService mandrillService;

    @Mock
    private MandrillCustomerInteractionConverter mandrillCustomerInteractionConverter;

    @InjectMocks
    private VMKSendCustomerInteractionEmailAction vmkSendCustomerInteractionEmailAction;

    @Test
    public void testExecuteAction_CreditController() throws Exception {
        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(LANGUAGE_ISO);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getCreditControllerEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);
        when(store.getCreditControllerEmail()).thenReturn(EMAIL);

        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);

        CustomerModel customer = mock(CustomerModel.class);
        when(customer.getUid()).thenReturn(CUSTOMER_UID);

        OrderModel order = mock(OrderModel.class);
        when(order.getLanguage()).thenReturn(language);
        when(order.getStore()).thenReturn(store);
        when(order.getUnit()).thenReturn(b2BUnit);
        when(order.getUser()).thenReturn(customer);

        OrderCustomerInteractionModel customerInteraction = mock(OrderCustomerInteractionModel.class);
        when(customerInteraction.getOrder()).thenReturn(order);

        CustomerInteractionProcessModel process = mock(CustomerInteractionProcessModel.class);
        when(process.getCustomerInteraction()).thenReturn(customerInteraction);
        when(customerInteraction.getType()).thenReturn(CREDIT_CONTROLLER);

        AbstractSimpleDecisionAction.Transition result = vmkSendCustomerInteractionEmailAction.executeAction(process);

        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.OK);

        verify(mandrillService).send(TEMPLATE, customerInteraction, mandrillCustomerInteractionConverter, EMAIL);
        verify(mandrillService).send(TEMPLATE, customerInteraction, mandrillCustomerInteractionConverter, CUSTOMER_UID);

        verify(store, never()).getOrderManagerEmailTemplate();
        verify(store, never()).getOrderManagerEmail();

        verify(customerInteraction).setStatus(CustomerInteractionStatus.SENT);

        verify(modelService).save(customerInteraction);
    }

    @Test
    public void testExecuteAction_FallbackToOrderManager() throws Exception {
        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(LANGUAGE_ISO);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getOrderManagerEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);
        when(store.getOrderManagerEmail()).thenReturn(EMAIL);

        VanmarckeContactModel contactPerson = mock(VanmarckeContactModel.class);

        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);

        CustomerModel customer = mock(CustomerModel.class);
        when(customer.getUid()).thenReturn(CUSTOMER_UID);

        OrderModel order = mock(OrderModel.class);
        when(order.getLanguage()).thenReturn(language);
        when(order.getStore()).thenReturn(store);
        when(order.getContactPerson()).thenReturn(contactPerson);
        when(order.getUnit()).thenReturn(b2BUnit);
        when(order.getUser()).thenReturn(customer);

        OrderCustomerInteractionModel customerInteraction = mock(OrderCustomerInteractionModel.class);
        when(customerInteraction.getOrder()).thenReturn(order);
        when(customerInteraction.getType()).thenReturn(ORDER_MANAGER);

        CustomerInteractionProcessModel process = mock(CustomerInteractionProcessModel.class);
        when(process.getCustomerInteraction()).thenReturn(customerInteraction);

        AbstractSimpleDecisionAction.Transition result = vmkSendCustomerInteractionEmailAction.executeAction(process);

        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.OK);

        verify(mandrillService).send(TEMPLATE, customerInteraction, mandrillCustomerInteractionConverter, EMAIL);
        verify(mandrillService).send(TEMPLATE, customerInteraction, mandrillCustomerInteractionConverter, CUSTOMER_UID);

        verify(store, never()).getCreditControllerEmailTemplate();
        verify(store, never()).getCreditControllerEmail();

        verify(customerInteraction).setStatus(CustomerInteractionStatus.SENT);

        verify(modelService).save(customerInteraction);
    }

    @Test
    public void testExecuteAction_ContactPerson() throws Exception {
        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(LANGUAGE_ISO);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getOrderManagerEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);
        when(store.getOrderManagerEmail()).thenReturn(EMAIL);

        VanmarckeContactModel contactPerson = mock(VanmarckeContactModel.class);
        when(contactPerson.getEmail()).thenReturn(EMAIL);

        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);

        CustomerModel customer = mock(CustomerModel.class);
        when(customer.getUid()).thenReturn(CUSTOMER_UID);

        OrderModel order = mock(OrderModel.class);
        when(order.getLanguage()).thenReturn(language);
        when(order.getStore()).thenReturn(store);
        when(order.getUnit()).thenReturn(b2BUnit);
        when(order.getContactPerson()).thenReturn(contactPerson);
        when(order.getUser()).thenReturn(customer);

        OrderCustomerInteractionModel customerInteraction = mock(OrderCustomerInteractionModel.class);
        when(customerInteraction.getOrder()).thenReturn(order);
        when(customerInteraction.getType()).thenReturn(ORDER_MANAGER);

        CustomerInteractionProcessModel process = mock(CustomerInteractionProcessModel.class);
        when(process.getCustomerInteraction()).thenReturn(customerInteraction);

        AbstractSimpleDecisionAction.Transition result = vmkSendCustomerInteractionEmailAction.executeAction(process);

        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.OK);

        verify(mandrillService).send(TEMPLATE, customerInteraction, mandrillCustomerInteractionConverter, EMAIL);
        verify(mandrillService).send(TEMPLATE, customerInteraction, mandrillCustomerInteractionConverter, CUSTOMER_UID);

        verify(store, never()).getCreditControllerEmailTemplate();
        verify(store, never()).getCreditControllerEmail();

        verify(customerInteraction).setStatus(CustomerInteractionStatus.SENT);

        verify(modelService).save(customerInteraction);
    }

    @Test
    public void testExecuteAction_withFailure() throws Exception {
        LanguageModel language = mock(LanguageModel.class);
        when(language.getIsocode()).thenReturn(LANGUAGE_ISO);

        BaseStoreModel store = mock(BaseStoreModel.class);
        when(store.getOrderManagerEmailTemplate(Locale.ENGLISH)).thenReturn(TEMPLATE);
        when(store.getOrderManagerEmail()).thenReturn(EMAIL);

        B2BUnitModel b2BUnit = mock(B2BUnitModel.class);

        CustomerModel customer = mock(CustomerModel.class);
        when(customer.getUid()).thenReturn(CUSTOMER_UID);

        OrderModel order = mock(OrderModel.class);
        when(order.getLanguage()).thenReturn(language);
        when(order.getStore()).thenReturn(store);
        when(order.getUnit()).thenReturn(b2BUnit);
        when(order.getUser()).thenReturn(customer);

        OrderCustomerInteractionModel customerInteraction = mock(OrderCustomerInteractionModel.class);
        when(customerInteraction.getOrder()).thenReturn(order);
        when(customerInteraction.getType()).thenReturn(ORDER_MANAGER);

        CustomerInteractionProcessModel process = mock(CustomerInteractionProcessModel.class);
        when(process.getCustomerInteraction()).thenReturn(customerInteraction);

        when(mandrillService.send(anyString(), any(CustomerInteractionModel.class), any(MandrillCustomerInteractionConverter.class), anyString())).thenThrow(Exception.class);

        AbstractSimpleDecisionAction.Transition result = vmkSendCustomerInteractionEmailAction.executeAction(process);

        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.NOK);

        verify(customerInteraction).setStatus(CustomerInteractionStatus.NOT_SENT);

        verify(modelService).save(customerInteraction);
    }

    @Test
    public void testExecuteAction_withoutOrderCustomerInteraction() {
        CustomerInteractionProcessModel process = mock(CustomerInteractionProcessModel.class);
        CustomerInteractionModel customerInteraction = mock(CustomerInteractionModel.class);

        when(process.getCustomerInteraction()).thenReturn(customerInteraction);

        AbstractSimpleDecisionAction.Transition result = vmkSendCustomerInteractionEmailAction.executeAction(process);
        assertThat(result).isEqualTo(AbstractSimpleDecisionAction.Transition.NOK);

        verifyZeroInteractions(mandrillService);

        verify(customerInteraction).setStatus(CustomerInteractionStatus.NOT_SENT);

        verify(modelService).save(customerInteraction);
    }
}