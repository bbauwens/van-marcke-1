package com.vanmarcke.services.action;

import com.vanmarcke.services.strategies.VMKCommerceSendOrderStrategy;
import de.hybris.platform.core.enums.ExportStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import static de.hybris.platform.core.enums.ExportStatus.EXPORTED;
import static de.hybris.platform.core.enums.ExportStatus.NOTEXPORTED;
import static de.hybris.platform.core.enums.OrderStatus.COMPLETED;
import static de.hybris.platform.core.enums.OrderStatus.PROCESSING_ERROR;

/**
 * Action which will trigger the event for sending the order to AS400 if not done before
 */
public class VMKSendOrderToAS400Action extends AbstractSimpleDecisionAction<OrderProcessModel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKSendOrderToAS400Action.class);

    private VMKCommerceSendOrderStrategy vmkCommerceSendOrderStrategy;

    /**
     * {@inheritDoc}
     */
    @Override
    public Transition executeAction(final OrderProcessModel process) throws RetryLaterException {
        OrderModel order = process.getOrder();
        try {
            order = vmkCommerceSendOrderStrategy.sendOrder(order);
            if (ExportStatus.EXPORTED.equals(order.getExportStatus())) {
                return Transition.OK;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        throw new RetryLaterException("Issue sending to AS400, attempting to send again in 5 minutes");
    }

    @Required
    public void setVmkCommerceSendOrderStrategy(VMKCommerceSendOrderStrategy vmkCommerceSendOrderStrategy) {
        this.vmkCommerceSendOrderStrategy = vmkCommerceSendOrderStrategy;
    }
}