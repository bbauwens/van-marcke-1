package com.vanmarcke.services.product.impl;

import com.vanmarcke.facades.product.data.StockListData;
import com.vanmarcke.services.constants.VITCalculatedStatusConstants;
import com.vanmarcke.services.constants.VITDeliveryMethodConstants;
import com.vanmarcke.services.product.VMKStockService;
import com.vanmarcke.services.product.VMKVariantProductService;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

/**
 * This class provides the implementation for the {@link VMKVariantProductService}.
 *
 * @author Tom van den Berg
 * @since 11-3-2020
 */
public class VMKVariantProductServiceImpl implements VMKVariantProductService {

    private final static String EDC_WAREHOUSE = "EDC";

    private final VMKStockService stockService;
    /**
     * Provides an implementation of VMKVariantProductService
     * @param stockService
     */
    public VMKVariantProductServiceImpl(VMKStockService stockService) {
        this.stockService = stockService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPotentiallyInStock(VariantProductModel variantProduct) {
        return !(VITDeliveryMethodConstants.CROSS_DOCK.equals(variantProduct.getDeliveryMethod()) ||
                (VITDeliveryMethodConstants.NO_STOCK.equals(variantProduct.getDeliveryMethod())
                        && VITCalculatedStatusConstants.NOT_AVAILABLE.equals(variantProduct.getCalculatedStatus())));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSellingOffAlmostNOS(VariantProductModel variantProduct) {
        return (VITDeliveryMethodConstants.NO_STOCK.equals(variantProduct.getDeliveryMethod())
                && VITCalculatedStatusConstants.SELLING_OFF.equals(variantProduct.getCalculatedStatus()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isNOSProduct(VariantProductModel variantProduct) {
        return !isPotentiallyInStock(variantProduct) ||
                (isSellingOffAlmostNOS(variantProduct) && getAvailableEDCStock(variantProduct) == 0L);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDiscontinuedProduct(VariantProductModel variantProduct) {
        return (StringUtils.isEmpty(variantProduct.getDeliveryMethod()) &&
                (VITCalculatedStatusConstants.NOT_AVAILABLE.equals(variantProduct.getCalculatedStatus())
                        || VITCalculatedStatusConstants.SELLING_OFF.equals(variantProduct.getCalculatedStatus())));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDiscontinuedOutOfStockProduct(VariantProductModel variantProduct) {
        return isDiscontinuedProduct(variantProduct) && getAvailableEDCStock(variantProduct) == 0L;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSoonInStockProduct(VariantProductModel product) {
        return StringUtils.isEmpty(product.getAvailabilityDate());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPurchasable(VariantProductModel variantProduct) {
        return !isNOSProduct(variantProduct)
                && !isSoonInStockProduct(variantProduct)
                && !isDiscontinuedOutOfStockProduct(variantProduct);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getAvailableEDCStock(ProductModel product) {
        StockListData currentEDCStock = stockService.getStockForProductAndWarehouses(product, new String[]{});
        if (currentEDCStock != null && !CollectionUtils.isEmpty(currentEDCStock.getStock())) {
            return currentEDCStock.getStock().stream()
                    .filter(stockData -> EDC_WAREHOUSE.equals(stockData.getWareHouse()))
                    .findFirst()
                    .map(StockData::getStockLevel)
                    .orElse(0L);
        }

        return 0L;
    }
}
