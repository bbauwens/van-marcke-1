package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.constants.VanmarckeCoreConstants;
import com.vanmarcke.core.model.FullEtimProductExportCronJobModel;
import com.vanmarcke.facades.data.ProductExportFeedResult;
import com.vanmarcke.services.ftp.FTPClient;
import com.vanmarcke.services.product.price.VMKFullEtimProductExportService;
import com.vanmarcke.services.util.VMKExportUtils;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Job which exports product classifications in CSV format.
 *
 * @author Giani Ifrim
 * @since 30-08-2021
 */
public class VMKFullEtimProductExportJobPerformable extends AbstractJobPerformable<FullEtimProductExportCronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(VMKFullEtimProductExportJobPerformable.class);

    private final VMKFullEtimProductExportService vmkFullEtimProductExportService;
    private final FTPClient ftpClient;

    public VMKFullEtimProductExportJobPerformable(VMKFullEtimProductExportService vmkFullEtimProductExportService, FTPClient ftpClient) {
        this.vmkFullEtimProductExportService = vmkFullEtimProductExportService;
        this.ftpClient = ftpClient;
    }

    @Override
    public PerformResult perform(FullEtimProductExportCronJobModel fullEtimProductExportCronJobModel) {
        ProductExportFeedResult result = vmkFullEtimProductExportService.createFullEtimProductExportLines(fullEtimProductExportCronJobModel.getCatalogVersion());

        if (StringUtils.isBlank(result.getFeed())) {
            LOG.warn("No products to export!");
            setLastRunTime(fullEtimProductExportCronJobModel, result);
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        }

        try (InputStream inputStream = IOUtils.toInputStream(result.getFeed(), "UTF8")) {
            ftpClient.send(inputStream, VMKExportUtils.getFormattedFileName(VanmarckeCoreConstants.Export.FULL_ETIM_PRODUCT_EXPORT_FILE_NAME));
            setLastRunTime(fullEtimProductExportCronJobModel, result);
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        } catch (IOException e) {
            LOG.error("Failed to export to FTP location: ", e);
        } catch (Exception e) {
            LOG.error("Exception occured when runnning fullEtimProductExportCronJob: ", e);
        }

        return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
    }

    /**
     * This method updates the lastSuccessfulTime from the cronjob
     *                          with the one from the export result.
     *
     * @param fullEtimProductExportCronJobModel target cronjob
     * @param result export result
     */
    private void setLastRunTime(FullEtimProductExportCronJobModel fullEtimProductExportCronJobModel, ProductExportFeedResult result) {
        if (result.getLastModifiedTime() != null) {
            fullEtimProductExportCronJobModel.setLastSuccessfulTime(result.getLastModifiedTime());
            super.modelService.save(fullEtimProductExportCronJobModel);
        }
    }
}
