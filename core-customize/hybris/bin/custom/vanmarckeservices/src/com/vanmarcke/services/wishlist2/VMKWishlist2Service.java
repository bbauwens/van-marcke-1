package com.vanmarcke.services.wishlist2;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.core.servicelayer.data.SortData;
import de.hybris.platform.wishlist2.Wishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.springframework.lang.NonNull;

public interface VMKWishlist2Service extends Wishlist2Service {

    /**
     * Returns the {@link Wishlist2Model} instance for the current user and the given {@code name}.
     *
     * @param name the name
     * @return the {@link Wishlist2Model} instance
     */
    Wishlist2Model getWishlistForCurrentUserAndName(String name);

    /**
     * Returns the {@link Wishlist2Model} instance for the current user and the given {@code name}.
     *
     * @param user the user
     * @param name the name
     * @return the {@link Wishlist2Model} instance
     */
    Wishlist2Model findWishListByUserAndName(@NonNull UserModel user, @NonNull String name);

    /**
     * Creates the {@link Wishlist2Model} instance for the current user and the given {@code name} and
     * {@code description}.
     *
     * @param name        the name
     * @param description the description
     * @return the {@link Wishlist2Model} instance
     */
    Wishlist2Model createWishlistForCurrentUserAndNameAndDescription(final String name, final String description);

    /**
     * Removes the {@link Wishlist2Model} instance for the current user and the given {@code name}.
     *
     * @param name the name
     */
    void removeWishlistForCurrentUserAndName(String name);

    /**
     * Returns the {@link Wishlist2EntryModel} instance for the current user and the given {@code pk}.
     *
     * @param pk the primary key
     * @return the {@link Wishlist2EntryModel} instance
     */
    Wishlist2EntryModel getWishlistEntryForCurrentUserAndPK(String pk);

    /**
     * Removes the {@link Wishlist2EntryModel} instance for the given {@code pk}.
     *
     * @param pk the primary key
     */
    void removeWishlistEntryForPK(String pk);

    /**
     * Search for wishlists
     *
     * @param paginationData Pagination information
     * @param sortData       Sorting information
     * @return Resulting wish lists
     */
    SearchPageData<Wishlist2Model> getWishlists(@NonNull final PaginationData paginationData, @NonNull final SortData sortData);

    /**
     * Convert the specified cart to wishlist
     *
     * @param wishListName       The name of the wishlist to create
     * @param wishListDescription   The description of the wishlist to create
     * @param abstractOrderModel the order to export to wishlist
     * @param userModel          the user
     * @return The wishlist converted from the cart
     */
    Wishlist2Model convertCartToWishlist(String wishListName, String wishListDescription, AbstractOrderModel abstractOrderModel, UserModel userModel);

    /**
     * Add required fields to the wishlist instance
     *
     * @param wishlist2Model Wishlist instance
     * @param wishListName   the name
     * @param userModel      the user
     */
    void addRequiredWishListFields(final Wishlist2Model wishlist2Model, final String wishListName, final UserModel userModel);

    /**
     * Add all required fields to the wish list entry
     *
     * @param wishlist2EntryModel Wish list entry
     * @param productModel        Product
     * @param quantity            Quantity
     */
    void addRequiredWishListEntryFields(final Wishlist2EntryModel wishlist2EntryModel, final ProductModel productModel, final int quantity);

    /**
     *  Add product from upload file to wishlist entry
     *
     * @param productCode           product code to be uploaded to wishlist
     * @param qty                   requested quantity to be added to wishlist
     * @param wishlist2Model        wishlist model to be updated with the product entry
     */
    void uploadProductToWishlist(String productCode, Long qty, Wishlist2Model wishlist2Model);

    /**
     * Retrieve the number of wishlits for the specified customer.
     *
     * @param user User to fetch the wishlists for
     * @return Number of wish lists for the user
     */
    Integer getWishListsCountForUser(UserModel user);
}