package com.vanmarcke.services.translators;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import org.apache.commons.lang3.time.FastDateFormat;

import java.text.ParseException;
import java.util.TimeZone;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class VMKDateTranslator extends AbstractValueTranslator {

    private static final String PARAM_DATE_FORMAT = "dateFormat";
    private static final String PARAM_TIME_ZONE = "timeZone";

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    private static final String DEFAULT_TIME_ZONE = "UTC";

    @Override
    public Object importValue(final String valueExpr, final Item toItem) throws JaloInvalidParameterException {
        clearStatus();

        final String dateFormat = defaultIfEmpty(getColumnDescriptor().getDescriptorData().getModifier(PARAM_DATE_FORMAT), DEFAULT_DATE_FORMAT);
        final String timeZone = defaultIfEmpty(getColumnDescriptor().getDescriptorData().getModifier(PARAM_TIME_ZONE), DEFAULT_TIME_ZONE);

        if (isNotEmpty(valueExpr)) {
            FastDateFormat fastDateFormat = FastDateFormat.getInstance(dateFormat, TimeZone.getTimeZone(timeZone));
            try {
                return fastDateFormat.parse(valueExpr);
            } catch (ParseException e) {
                // nothing to do here
            }
        }
        return EMPTY;
    }

    @Override
    public String exportValue(final Object value) throws JaloInvalidParameterException {
        return value == null ? EMPTY : value.toString();
    }
}