package com.vanmarcke.services.order.cart.conversion.impl;

import com.vanmarcke.services.constants.VanmarckeservicesConstants.Export;
import com.vanmarcke.services.order.cart.conversion.VMKCsvExcelConversionService;
import de.hybris.platform.util.CSVReader;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;

/**
 * This class provides methods to convert carts from XLSX to CSV and vica versa.
 *
 * @author Tom van den Berg
 * @since 12-05-2020
 */
public class VMKCsvExcelConversionServiceImpl implements VMKCsvExcelConversionService {

    private static final Logger LOGGER = Logger.getLogger(VMKCsvExcelConversionServiceImpl.class);
    public static final String DEFAULT_CURRENCY_FORMAT = "€\\ #.#,00;€\\ -#.#,00";
    public static final String DEFAULT_CURRENCY_FORMAT_2 = "€\\ #,##0.00;€\\ -#,##0.00";

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] convertCsvToXls(StringReader csv) {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
             Workbook workbook = new HSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("cart");
            processCsvRows(csv, sheet);
            workbook.write(outputStream);
            return outputStream.toByteArray();

        } catch (IOException e) {
            LOGGER.debug("Error exporting cart to excel file", e);
        }
        return ArrayUtils.EMPTY_BYTE_ARRAY;
    }

    @Override
    public byte[] convertCsvToXls(StringReader csv, int[] currencyColumns) {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
             Workbook workbook = new HSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("cart");
            processCsvRows(csv, sheet);
            processCurrencyCell(currencyColumns, workbook, sheet);
            workbook.write(outputStream);
            return outputStream.toByteArray();

        } catch (IOException e) {
            LOGGER.debug("Error exporting cart to excel file", e);
        }
        return ArrayUtils.EMPTY_BYTE_ARRAY;
    }

    /**
     * add formatting to currency cells
     *
     * @param currencyColumns columns containing currency (double) values
     * @param workbook        workbook
     * @param sheet           sheet containing the rows
     */
    protected void processCurrencyCell(int[] currencyColumns, Workbook workbook, Sheet sheet) {
        int index = 0;
        for (Row row : sheet) {
            if (index > 0) {
                for (int i : currencyColumns) {
                    Cell cell = row.getCell(i);
                    try {
                        cell.setCellValue(Double.parseDouble(cell.getStringCellValue()));
                    } catch (NumberFormatException e) {
                        cell.setCellValue(cell.getStringCellValue());
                    }
                    CellStyle cs = workbook.createCellStyle();
                    DataFormat df = workbook.createDataFormat();
                    cs.setDataFormat(df.getFormat(DEFAULT_CURRENCY_FORMAT_2));
                    cell.setCellStyle(cs);
                }
            }

            index++;
        }
    }

    /**
     * Processes the rows in the given {@link Sheet}.
     * <p>
     * It reads the data from the given {@link StringReader}.
     *
     * @param csv   the string reader
     * @param sheet the sheet
     */
    protected void processCsvRows(StringReader csv, Sheet sheet) {
        CSVReader reader = getCSVReader(csv);
        int rowNumber = 0;

        while (reader.readNextLine()) {
            processCsvRow(sheet, reader, rowNumber++);
        }
    }

    /**
     * Processes each row in the given {@link Sheet}.
     * <p>
     * It reads the data from the given {@link StringReader} based on the given {@code row number}.
     *
     * @param sheet     the sheet
     * @param reader    the CSV reader
     * @param rowNumber the row number
     */
    protected void processCsvRow(Sheet sheet, CSVReader reader, int rowNumber) {
        Row currentRow = sheet.createRow(rowNumber);

        Map<Integer, String> line = reader.getLine();

        for (int i = 0; i < line.size(); i++) {
            if (i == Export.Excel.NAME_COLUMN_NUMBER) {
                sheet.autoSizeColumn(i);
            }
            currentRow.createCell(i).setCellValue(line.get(i));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream convertXlsToCsv(MultipartFile sheet) {
        StringBuilder data = new StringBuilder();
        try (Workbook workBook = WorkbookFactory.create(sheet.getInputStream())) {
            processRows(data, workBook);
            return new ByteArrayInputStream(data.toString().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * Processes the rows in the given {@link Workbook}.
     *
     * @param data     the string builder
     * @param workBook the work book
     */
    protected void processRows(StringBuilder data, Workbook workBook) {
        for (Row row : workBook.getSheetAt(0)) {
            Iterator<Cell> cellIterator = row.cellIterator();
            processCells(data, cellIterator);
            data.append('\n');
        }
    }

    /**
     * Processes the cells in the given {@link Iterator<Cell>}.
     *
     * @param data         the string builder
     * @param cellIterator the cell iterator
     */
    protected void processCells(StringBuilder data, Iterator<Cell> cellIterator) {
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            processCell(data, cell);
        }
    }

    /**
     * Processes the given {@link Cell}.
     *
     * @param data the string builder
     * @param cell the cell
     */
    protected void processCell(StringBuilder data, Cell cell) {
        CellType type = cell.getCellTypeEnum();
        if (type == CellType.BOOLEAN) {
            data.append(cell.getBooleanCellValue());
        } else if (type == CellType.NUMERIC) {
            data.append((int) cell.getNumericCellValue());
        } else if (type == CellType.STRING) {
            data.append(cell.getStringCellValue());
        } else if (type == CellType.BLANK) {
        } else {
            data.append(cell + "");
        }
        data.append(";");
    }

    protected CSVReader getCSVReader(StringReader csv) {
        return new CSVReader(csv);
    }
}
