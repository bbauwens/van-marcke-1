package com.vanmarcke.services.cms.restriction.evaluator;

import com.vanmarcke.services.model.cms.restriction.VMKChannelRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.site.BaseSiteService;

public class VMKChannelRestrictionEvaluator implements CMSRestrictionEvaluator<VMKChannelRestrictionModel> {

    private final BaseSiteService baseSiteService;

    public VMKChannelRestrictionEvaluator(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    @Override
    public boolean evaluate(VMKChannelRestrictionModel vmkChannelRestrictionModel, RestrictionData restrictionData) {
        return !(baseSiteService.getCurrentBaseSite().getChannel() == vmkChannelRestrictionModel.getChannel());
    }
}
