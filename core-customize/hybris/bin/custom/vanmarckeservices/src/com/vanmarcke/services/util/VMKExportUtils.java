package com.vanmarcke.services.util;

import com.vanmarcke.core.constants.VanmarckeCoreConstants;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class VMKExportUtils {

    /**
     * Get the file name with current date and extension
     *
     * @param fileName the original fileName
     * @return the file name with current date
     */
    public static String getFormattedFileName(String fileName) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(VanmarckeCoreConstants.Export.EXPORT_DATE_TIME_SUFFIX_FORMAT);
        LocalDate localDate = LocalDate.now();
        return fileName + VanmarckeCoreConstants.Export.EXPORT_HYPHEN + dtf.format(localDate) + VanmarckeCoreConstants.Export.EXPORT_EXTENSION;
    }
}
