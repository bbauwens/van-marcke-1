package com.vanmarcke.services.factories.impl;

import com.vanmarcke.services.factories.VMKAbstractSiteChannelAwareFactory;
import de.hybris.platform.commerceservices.strategies.DeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;

/**
 * implementation for the DeliveryAddressesLookupStrategy
 */
public class VMKDeliveryAddressLookupStrategyFactoryImpl extends VMKAbstractSiteChannelAwareFactory<DeliveryAddressesLookupStrategy> implements DeliveryAddressesLookupStrategy {

    @Override
    public List<AddressModel> getDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder, final boolean visibleAddressesOnly) {
        return get().getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
    }
}
