package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang3.StringUtils;

/**
 * Resolver for nearest categories.
 */
public class VMKNearestCategoryValueResolver extends AbstractValueResolver<ProductModel, Object, Object> {

    private CategoryService categoryService;

    public VMKNearestCategoryValueResolver(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument inputDocument, IndexerBatchContext indexerBatchContext, IndexedProperty indexedProperty,
                                  ProductModel source, ValueResolverContext<Object, Object> valueResolverContext) throws FieldValueProviderException {

        ProductModel productModel = source;
        if (productModel instanceof VariantProductModel) {
            productModel = ((VariantProductModel) productModel).getBaseProduct();
        }

        if (productModel != null) {
            String category = categoryService.getCategoryPathForProduct(productModel, CategoryModel.class).stream()
                    .reduce((c1, c2) -> c2)
                    .map(c -> StringUtils.isNotBlank(c.getName()) ? c.getName() : c.getCode())
                    .orElse(null);

            if (StringUtils.isNotBlank(category)) {
                inputDocument.addField(indexedProperty, category, valueResolverContext.getFieldQualifier());
            }
        }
    }
}
