package com.vanmarcke.services.order.payment.keygenerator;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import java.util.UUID;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;

public class VMKPaymentInfoKeyGenerator implements KeyGenerator {

    private static final String KEY_FORMAT = "%s-%s";

    @Override
    public Object generate() {
        return UUID.randomUUID().toString();
    }

    @Override
    public Object generateFor(final Object o) {
        checkArgument(o instanceof AbstractOrderModel, "Parameter o must be of type de.hybris.platform.core.model.order.AbstractOrderModel");

        final AbstractOrderModel order = (AbstractOrderModel) o;
        return format(KEY_FORMAT, order.getCode(), generate());
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("A reset of payment info key generator is not supported.");
    }
}