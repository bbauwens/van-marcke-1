package com.vanmarcke.services.constants;


/**
 * This class lists the VIT Delivery Method codes.
 * <p>
 * These codes returned from VIT state the delivery methods of a particular product.
 *
 * @since 3-3-2020
 * @author Tom van den Berg
 */
public final class VITDeliveryMethodConstants {

    public static final String NO_STOCK = "N";
    public static final String CROSS_DOCK = "X";

    /**
     * Hides the default implicit constructor.
     */
    private VITDeliveryMethodConstants() {}
}
