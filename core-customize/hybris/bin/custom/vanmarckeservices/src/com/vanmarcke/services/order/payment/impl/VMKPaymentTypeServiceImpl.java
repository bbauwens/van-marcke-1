package com.vanmarcke.services.order.payment.impl;

import com.vanmarcke.services.order.credit.VMKCreditCheckService;
import com.vanmarcke.services.order.payment.VMKPaymentTypeService;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.enumeration.EnumerationService;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType.ACCOUNT;

/**
 * Default implementation for {@link VMKPaymentTypeService}
 */
public class VMKPaymentTypeServiceImpl implements VMKPaymentTypeService {

    private final EnumerationService enumerationService;
    private final VMKCreditCheckService vmkCreditCheckService;

    /**
     * Creates a new instance of the {@link VMKPaymentTypeServiceImpl} class.
     *
     * @param enumerationService    the enumerationService
     * @param vmkCreditCheckService the credit check service
     */
    public VMKPaymentTypeServiceImpl(final EnumerationService enumerationService,
                                     final VMKCreditCheckService vmkCreditCheckService) {
        this.enumerationService = enumerationService;
        this.vmkCreditCheckService = vmkCreditCheckService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CheckoutPaymentType> getSupportedPaymentTypeListForOrder(final AbstractOrderModel order) {
        checkArgument(order != null, "parameter order cannot be null");
        final List<CheckoutPaymentType> checkoutPaymentTypes = enumerationService.getEnumerationValues(CheckoutPaymentType._TYPECODE);
        if (!vmkCreditCheckService.validateCreditworthinessForCart(order)) {
            checkoutPaymentTypes.removeIf(e -> e.equals(ACCOUNT));
        }
        return checkoutPaymentTypes;
    }
}