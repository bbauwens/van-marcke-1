package com.vanmarcke.services.odata.schema;

import de.hybris.platform.integrationservices.model.DescriptorFactory;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemAttributeModel;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.odata2services.odata.schema.utils.SchemaUtils;
import org.apache.olingo.odata2.api.edm.provider.Association;
import org.apache.olingo.odata2.api.edm.provider.AssociationEnd;

import static de.hybris.platform.odata2services.odata.schema.utils.SchemaUtils.buildAssociationName;
import static de.hybris.platform.odata2services.odata.schema.utils.SchemaUtils.toFullQualifiedName;

public class VMKLocalizedOneToOneAssociationGenerator extends VMKUnlocalizedOneToOneAssociationGenerator {

    public VMKLocalizedOneToOneAssociationGenerator(DescriptorFactory integrationServicesDescriptorFactory) {
        super(integrationServicesDescriptorFactory);
    }

    @Override
    public boolean isApplicable(final TypeAttributeDescriptor attributeDescriptor) {
        return attributeDescriptor != null && !attributeDescriptor.isPrimitive() && hasOneLocalizedElement(attributeDescriptor);
    }

    @Override
    public Association generate(final TypeAttributeDescriptor attributeDescriptor) {
        final String sourceTypeCode = getSourceRole(attributeDescriptor);
        final String targetTypeCode = getTargetRole(attributeDescriptor);
        return new Association()
                .setName(getAssociationName(attributeDescriptor))
                .setEnd1(new AssociationEnd()
                        .setType(toFullQualifiedName(sourceTypeCode))
                        .setRole(sourceTypeCode)
                        .setMultiplicity(getSourceCardinality(attributeDescriptor)))
                .setEnd2(new AssociationEnd()
                        .setType(toFullQualifiedName(getTargetType(attributeDescriptor)))
                        .setRole(targetTypeCode)
                        .setMultiplicity(getTargetCardinality(attributeDescriptor)));
    }

    @Override
    public String getAssociationName(final IntegrationObjectItemAttributeModel itemAttribute) {
        return getAssociationName(asDescriptor(itemAttribute));
    }

    @Override
    public String getAssociationName(final TypeAttributeDescriptor attributeDescriptor) {
        return buildAssociationName(getSourceRole(attributeDescriptor), attributeDescriptor.getAttributeName());
    }

    @Override
    public String getSourceRole(final IntegrationObjectItemAttributeModel itemAttribute) {
        return getSourceRole(asDescriptor(itemAttribute));
    }

    @Override
    public String getSourceRole(final TypeAttributeDescriptor attributeDescriptor) {
        final String itemCode = attributeDescriptor.getTypeDescriptor().getItemCode();
        return SchemaUtils.localizedEntityName(itemCode);
    }

    private boolean hasOneLocalizedElement(final TypeAttributeDescriptor attributeDescriptor) {
        return !attributeDescriptor.isCollection() && attributeDescriptor.isLocalized();
    }
}
