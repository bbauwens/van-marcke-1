package com.vanmarcke.services.factories;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public abstract class VMKAbstractSiteChannelAwareFactory<T> {

    private BaseSiteService baseSiteService;
    private Map<SiteChannel, T> factoryConfigurationMap;
    private T defaultConfiguration;

    protected T get() {
        BaseSiteModel baseSite = baseSiteService.getCurrentBaseSite();
        return get(baseSite);
    }

    protected T get(final BaseSiteModel baseSite) {
        validateParameterNotNullStandardMessage("baseSite", baseSite);
        final T configuration = MapUtils.getObject(factoryConfigurationMap, baseSite.getChannel());
        return configuration != null ? configuration : defaultConfiguration;
    }

    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    @Required
    public void setFactoryConfigurationMap(final Map<SiteChannel, T> factoryConfigurationMap) {
        this.factoryConfigurationMap = factoryConfigurationMap;
    }

    @Required
    public void setDefaultConfiguration(final T defaultConfiguration) {
        this.defaultConfiguration = defaultConfiguration;
    }

}