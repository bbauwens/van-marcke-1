package com.vanmarcke.services.product.impl;

import com.vanmarcke.core.product.VMKPagedVariantProductDao;
import com.vanmarcke.services.data.ProductExportItem;
import com.vanmarcke.services.helper.StreamingMarshallerHelper;
import com.vanmarcke.services.product.VMKProductExportService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static de.hybris.platform.catalog.enums.ArticleApprovalStatus.APPROVED;
import static de.hybris.platform.servicelayer.search.paginated.util.PaginatedSearchUtils.createSearchPageDataWithPaginationAndSorting;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

/**
 * The {@link VMKProductExportServiceImpl} class implements the business logic to export product information.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 27/08/2018
 */
public class VMKProductExportServiceImpl implements VMKProductExportService {

    private static final Logger LOG = Logger.getLogger(VMKProductExportServiceImpl.class);

    private static final int PAGE_SIZE = 1000;

    private final VMKPagedVariantProductDao pagedVariantProductDao;
    private final Converter<VariantProductModel, ProductExportItem> productExportConverter;

    /**
     * Creates a new instance of the {@link VMKProductExportServiceImpl} class.
     *
     * @param pagedVariantProductDao the variant product DAO
     * @param productExportConverter the product export converter
     */
    public VMKProductExportServiceImpl(VMKPagedVariantProductDao pagedVariantProductDao,
                                       Converter<VariantProductModel, ProductExportItem> productExportConverter) {
        this.pagedVariantProductDao = pagedVariantProductDao;
        this.productExportConverter = productExportConverter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File generateProductFeed(CatalogVersionModel catalogVersion, String path) throws IOException, JAXBException, XMLStreamException {
        validateParameterNotNull(catalogVersion, "catalog version cannot be null");

        StreamingMarshallerHelper<ProductExportItem> marshaller = new StreamingMarshallerHelper<>(ProductExportItem.class);
        marshaller.open(path, "items");

        Map<String, String> sortParams = new HashMap<>();
        sortParams.put("code", "asc");

        int currentPage = 0;
        int numberOfPages;
        do {
            SearchPageData<VariantProductModel> searchPageData = createSearchPageDataWithPaginationAndSorting(PAGE_SIZE, currentPage, true, sortParams);
            SearchPageData<VariantProductModel> resultPageData = pagedVariantProductDao.findProductsByCatalogVersionAndApprovalStatus(catalogVersion, APPROVED, searchPageData);

            if (CollectionUtils.isNotEmpty(resultPageData.getResults())) {
                resultPageData.getResults()
                        .stream()
                        .map(productExportConverter::convert)
                        .forEach(t -> {
                            try {
                                marshaller.write(t, "item");
                            } catch (JAXBException e) {
                                LOG.error(e.getMessage(), e);
                            }
                        });
            }

            currentPage++;
            numberOfPages = resultPageData.getPagination().getNumberOfPages();
        } while (currentPage < numberOfPages);

        marshaller.close();

        return new File(path);
    }
}