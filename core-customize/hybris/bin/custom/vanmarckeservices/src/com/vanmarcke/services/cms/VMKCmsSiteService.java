package com.vanmarcke.services.cms;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteChannel;

import java.util.List;

/**
 * Custom interface of CMSSiteService
 */
public interface VMKCmsSiteService extends CMSSiteService {

    /**
     * Gets all the supported sites for the given {@link SiteChannel}
     *
     * @param channel the site channel
     * @return a list with supported CMSSiteModels or an empty list
     */
    List<CMSSiteModel> getSupportedSites(SiteChannel channel);
}
