package com.vanmarcke.services.cronjob;

import com.vanmarcke.core.model.ProductExportCronJobModel;
import com.vanmarcke.services.ftp.FTPClient;
import com.vanmarcke.services.product.VMKProductExportService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * The job which will generate an xml feed for all products and send it via FTP to Channable.
 *
 * @author Taki Korovessis, Christiaan Janssen
 * @since 30/08/2018
 */
public class VMKProductExportJobPerformable extends AbstractJobPerformable<ProductExportCronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(VMKProductExportJobPerformable.class);

    private static final String FILENAME_PRODUCT_FEED = "product-feed-hybris.xml";

    private final VMKProductExportService productExportService;
    private final FTPClient ftpClient;

    /**
     * Creates a new instance of the {@link VMKProductExportJobPerformable} class.
     *
     * @param productExportService the product export service
     * @param ftpClient            the FTP client
     */
    public VMKProductExportJobPerformable(VMKProductExportService productExportService,
                                          FTPClient ftpClient) {
        this.productExportService = productExportService;
        this.ftpClient = ftpClient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PerformResult perform(ProductExportCronJobModel productExportCronJob) {
        try {
            File file = productExportService.generateProductFeed(productExportCronJob.getCatalogVersion(), FILENAME_PRODUCT_FEED);
            if (file != null) {
                InputStream is = FileUtils.openInputStream(file);
                ftpClient.send(is, FILENAME_PRODUCT_FEED);
                IOUtils.closeQuietly(is);
                FileUtils.deleteQuietly(file);
            }
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        } catch (IOException | JAXBException | XMLStreamException e) {
            LOG.error("Exception occurred during the generation of the product feed", e);
        }
        return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
    }
}