package com.vanmarcke.services.order.cart;

import com.vanmarcke.services.exception.PickupDeliveryDateException;
import com.vanmarcke.services.exception.UnsupportedDeliveryDateException;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;

import java.util.Date;
import java.util.List;

/**
 * Custom interface for the CommerceCartService
 */
public interface VMKCommerceCartService extends CommerceCartService {

    /**
     * Adds to the (existing) {@link CartModel} the (existing) {@link ProductModel} in the given {@link UnitModel} and
     * with the given <code>quantity</code>. If in the cart already an entry with the given product and given unit exists
     * the given <code>quantity</code> is added to the the quantity of this cart entry unless <code>forceNewEntry</code>
     * is set to true. After this the cart is calculated.
     *
     * @param parameterList - A parameter object containing all attributes needed for add to cart
     *                      <p>
     *                      {@link CommerceCartParameter#cart} - The user's cart in session
     *                      {@link CommerceCartParameter#pointOfService} - The store object for pick up in store items (only needs
     *                      to be passed in if you are adding an item to pick up {@link CommerceCartParameter#product} - The
     *                      {@link ProductModel} to add {@link CommerceCartParameter#quantity} - The quantity to add
     *                      {@link CommerceCartParameter#unit} - The UnitModel of the product @see
     *                      {@link de.hybris.platform.core.model.product.ProductModel#getUnit()}
     *                      {@link CommerceCartParameter#createNewEntry} - The flag for creating a new
     *                      {@link de.hybris.platform.core.model.order.CartEntryModel}
     *                      </P>
     * @return the cart modification data that includes a statusCode and the actual quantity added to the cart
     * @throws CommerceCartModificationException if the <code>product</code> is a base product OR the quantity is less than 1 or no usable unit was
     *                                           found (only when given <code>unit</code> is also <code>null</code>) or any other reason the cart could
     *                                           not be modified.
     */
    List<CommerceCartModification> addToCart(List<CommerceCartParameter> parameterList) throws CommerceCartMergingException;

    /**
     * Update the mode for the cart
     *
     * @param parameter the parameter object containing all attributes needed for update the mode for the cart
     */
    void updateCartCheckoutMode(CommerceCartParameter parameter);

    /**
     * Sets the {@code requestedDeliveryDate} for the given {@code parameter}.
     *
     * @param parameter             the parameter holding that {@link CartModel} that will be updated
     * @param requestedDeliveryDate the requested delivery date
     * @throws UnsupportedDeliveryDateException in case the requested delivery date is invalid
     */
    void setRequestedDeliveryDate(final CommerceCartParameter parameter,
                                  final Date requestedDeliveryDate) throws UnsupportedDeliveryDateException, PickupDeliveryDateException;
}