package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.services.strategies.VMKNavigationBuilderStrategy;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.product.ProductService;
import org.springframework.cache.annotation.Cacheable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

public class VMKNavigationBuilderStrategyImpl implements VMKNavigationBuilderStrategy {

    private final ProductService productService;

    /**
     * Creates a new instance of {@link VMKNavigationBuilderStrategyImpl}
     *
     * @param productService the productService
     */
    public VMKNavigationBuilderStrategyImpl(final ProductService productService) {
        this.productService = productService;
    }

    /**
     * {@inheritDoc}
     */
    @Cacheable(value = "mainMenuNavigationNodes", key = "{#baseSite.uid, #cmsNavigationNodeModel.uid}", cacheManager = "cmsNavigationNodeCacheManager", unless = "#result.size() == 0")
    @Override
    public List<CMSNavigationNodeModel> buildNavigation(final BaseSiteModel baseSite, final CMSNavigationNodeModel cmsNavigationNodeModel) {
        final List<CMSNavigationNodeModel> visibleChildren = new ArrayList<>();
        if (isNotEmpty(cmsNavigationNodeModel.getChildren())) {
            cmsNavigationNodeModel.getChildren().forEach(e -> addChildNavigationNodeIfChildContainCategoriesWithProducts(e, visibleChildren));
        }
        return visibleChildren;
    }

    /**
     * Check if child nodes have entries that contain a category which have at least one product somewhere down the line
     *
     * @param childNode       the current child node
     * @param visibleChildren the list of visible children
     */
    private void addChildNavigationNodeIfChildContainCategoriesWithProducts(CMSNavigationNodeModel childNode, List<CMSNavigationNodeModel> visibleChildren) {
        final List<CMSNavigationEntryModel> navigationEntries = childNode.getEntries();
        if (isNotEmpty(navigationEntries)) {
            final Optional<CMSLinkComponentModel> result = navigationEntries.stream()
                    .map(CMSNavigationEntryModel::getItem)
                    .filter(CMSLinkComponentModel.class::isInstance)
                    .map(CMSLinkComponentModel.class::cast)
                    .filter(e -> e.getCategory() != null)
                    .filter(e -> productService.containsProductsForCategory(e.getCategory()))
                    .findAny();

            if (result.isPresent()) {
                visibleChildren.add(childNode);
            }
        }
    }

}