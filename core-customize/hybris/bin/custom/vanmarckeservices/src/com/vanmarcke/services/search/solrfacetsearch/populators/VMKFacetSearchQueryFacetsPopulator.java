package com.vanmarcke.services.search.solrfacetsearch.populators;

import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQueryFacetsPopulator;
import org.apache.solr.client.solrj.SolrQuery;

/**
 * The {@code VMKFacetSearchQueryFacetsPopulator} class is used to overwrite the OOTB
 * {@link FacetSearchQueryFacetsPopulator} class.
 * <p>
 * The reason for this is that the category search did not work properly, because the number of facets returned was
 * incorrect.
 *
 * @author Christiaan Janssen
 * @since 02-04-2020
 */
public class VMKFacetSearchQueryFacetsPopulator extends FacetSearchQueryFacetsPopulator {

    private final Integer facetLimit;

    /**
     * Creates a new instance of the {@link VMKFacetSearchQueryFacetsPopulator} class.
     *
     * @param facetLimit the number of facet values to display
     */
    public VMKFacetSearchQueryFacetsPopulator(Integer facetLimit) {
        this.facetLimit = facetLimit;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(SearchQueryConverterData source, SolrQuery target) {
        super.populate(source, target);

        populateFacetLimit(target);
    }

    /**
     * Populates the facet limit.
     *
     * @param target the Solr query
     */
    private void populateFacetLimit(SolrQuery target) {
        target.setFacetLimit(facetLimit);
    }
}
