package com.vanmarcke.services.order.impl;

import com.vanmarcke.cpi.data.order.EntryInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.forms.delivery.DeliveryMethodForm;
import com.vanmarcke.services.order.VMKCommerceCheckoutService;
import com.vanmarcke.services.product.VMKVariantProductService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import de.hybris.platform.b2bacceleratorservices.order.impl.DefaultB2BCommerceCheckoutService;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The {@link VMKCommerceCheckoutServiceImpl} class implements the business logic that enables the checkout process.
 *
 * @author Niels Raemaekers, Taki Korovessis, Tom van den Berg, Christiaan Janssen
 * @since 09-08-2019
 */
public class VMKCommerceCheckoutServiceImpl extends DefaultB2BCommerceCheckoutService implements VMKCommerceCheckoutService {

    private final CartService cartService;
    private final VMKPointOfServiceService pointOfServiceService;
    private final VMKDeliveryInfoService deliveryInfoService;
    private final UserService userService;
    private final VMKVariantProductService variantProductService;

    private final Converter<DeliveryMethodForm, CartModel> deliveryInformationCartConverter;
    private final CommerceCartCalculationStrategy commerceCartCalculationStrategy;

    /**
     * Provides an instance of the {@code VMKCommerceCheckoutServiceImpl}.
     * @param pointOfServiceService            the point of service service
     * @param cartService                      the cart service
     * @param deliveryInfoService              the pickup info service
     * @param deliveryInformationCartConverter the delivery method form to cartmodel converter
     * @param commerceCartCalculationStrategy  the commerce cart calculation strategy
     * @param userService                      the user service
     * @param variantProductService            the product variant service
     */
    public VMKCommerceCheckoutServiceImpl(VMKPointOfServiceService pointOfServiceService,
                                          CartService cartService,
                                          VMKDeliveryInfoService deliveryInfoService,
                                          Converter<DeliveryMethodForm, CartModel> deliveryInformationCartConverter,
                                          CommerceCartCalculationStrategy commerceCartCalculationStrategy,
                                          UserService userService,
                                          VMKVariantProductService variantProductService) {
        this.pointOfServiceService = pointOfServiceService;
        this.cartService = cartService;
        this.deliveryInfoService = deliveryInfoService;
        this.deliveryInformationCartConverter = deliveryInformationCartConverter;
        this.commerceCartCalculationStrategy = commerceCartCalculationStrategy;
        this.userService = userService;
        this.variantProductService = variantProductService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDeliveryDetailsOnCart(DeliveryMethodForm form) {
        CartModel cart = getCart();
        if (cart != null) {
            deliveryInformationCartConverter.convert(form, cart);
            getModelService().save(cart);
            getModelService().refresh(cart);
            CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
            commerceCartParameter.setEnableHooks(true);
            commerceCartParameter.setCart(cart);
            commerceCartCalculationStrategy.calculateCart(commerceCartParameter);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFavoriteAlternativeStoreOnSessionCart(String name) {
        CartModel cart = getCart();
        if (cart != null) {
            cart.setDeliveryPointOfService(
                    name == null
                            ? userService.getCurrentUser().getSessionStore()
                            : pointOfServiceService.getPointOfServiceForName(name));
        }
        getModelService().save(cart);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getInvalidCartEntries() {
        final CartModel cart = getCart();

        if (cart != null) {
            return deliveryInfoService.getPickupDateInformation(cart)
                    .map(PickupDateInfoResponseData::getProducts)
                    .map(entryInfos -> getInvalidEntries(entryInfos, cart))
                    .orElse(Collections.emptyList());
        }
        return Collections.emptyList();
    }

    private List<String> getInvalidEntries(List<EntryInfoResponseData> entries, CartModel cart) {
        return entries.stream()
                .filter(product -> isInvalidEntry(product, cart))
                .map(EntryInfoResponseData::getProductNumber)
                .collect(Collectors.toList());
    }

    private boolean isInvalidEntry(EntryInfoResponseData entry, CartModel cartModel) {
        return ("L".equals(entry.getCodeSku()) && !(isEntryProductStockAdjustable(entry, cartModel))) ||
                (entry.getPickupDate() == null && "*".equals(entry.getCodeSku())
                && !isEntryProductStockAdjustable(entry, cartModel));
    }
    /**
     * Get the current cart
     *
     * @return the current cart
     */
    protected CartModel getCart() {
        return cartService.hasSessionCart() ? cartService.getSessionCart() : null;
    }

    /**
     * returns true if product has enough stock or stock is adjustable to a lower amount
     *
     * @param entry the entry payload from the pickup date service
     * @param cartModel the cart
     * @return true if product has stock or adjustable stock
     */
    protected boolean isEntryProductStockAdjustable(EntryInfoResponseData entry, CartModel cartModel) {
        boolean enoughStock = entry.isEnoughStock();

        Optional<ProductModel> entryProduct = cartModel.getEntries()
                .stream().filter(cartEntry -> cartEntry.getProduct().getCode().equals(entry.getProductNumber()))
                .map(AbstractOrderEntryModel::getProduct)
                .findFirst();

        if (entryProduct.isPresent()) {
            VariantProductModel product = (VariantProductModel) entryProduct.get();
            return enoughStock || ((variantProductService.isSellingOffAlmostNOS(product) || variantProductService.isDiscontinuedProduct(product))
                    && !(variantProductService.getAvailableEDCStock(product) == 0L));

        }

        return enoughStock;
    }
}
