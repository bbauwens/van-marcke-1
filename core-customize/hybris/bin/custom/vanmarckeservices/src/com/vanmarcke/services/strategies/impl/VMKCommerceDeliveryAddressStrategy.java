package com.vanmarcke.services.strategies.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceDeliveryAddressStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.util.Optional;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

/**
 * Custom strategy to allow setting of new delivery mode also
 *
 * @author Cristi Stoica
 * @since 07-10-2021
 */
public class VMKCommerceDeliveryAddressStrategy extends DefaultCommerceDeliveryAddressStrategy {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean storeDeliveryAddress(final CommerceCheckoutParameter parameter)
    {

        final CartModel cartModel = parameter.getCart();
        final AddressModel addressModel = parameter.getAddress();
        final boolean flagAsDeliveryAddress = parameter.isIsDeliveryAddress();

        validateParameterNotNull(cartModel, "Cart model cannot be null");
        getModelService().refresh(cartModel);

        final UserModel user = cartModel.getUser();
        getModelService().refresh(user);

        cartModel.setDeliveryAddress(addressModel);

        // Check that the address model belongs to the same user as the cart
        if (isValidDeliveryAddress(cartModel, addressModel))
        {
            getModelService().save(cartModel);

            if (addressModel != null && flagAsDeliveryAddress && !Boolean.TRUE.equals(addressModel.getShippingAddress()))
            {
                // Flag the address as a delivery address
                addressModel.setShippingAddress(Boolean.TRUE);
                getModelService().save(addressModel);
            }

            Optional<DeliveryModeModel> zoneDeliveryMode = getDeliveryService().getSupportedDeliveryModeListForOrder(cartModel)
                    .stream()
                    .filter(deliveryModeModel -> deliveryModeModel instanceof ZoneDeliveryModeModel)
                    .findFirst();

            if (zoneDeliveryMode.isPresent()) {
                DeliveryModeModel currentZoneDelivery = cartModel.getDeliveryMode();
                cartModel.setDeliveryMode(zoneDeliveryMode.get());
                getModelService().save(cartModel);

                if (currentZoneDelivery == null || !currentZoneDelivery.getCode().equals(zoneDeliveryMode.get().getCode())) {
                    cartModel.getEntries().forEach(entry -> entry.setCalculated(Boolean.FALSE));
                }
            }

            final CommerceCartParameter calculateCartParameter = new CommerceCartParameter();
            calculateCartParameter.setEnableHooks(true);
            calculateCartParameter.setCart(cartModel);
            getCommerceCartCalculationStrategy().calculateCart(calculateCartParameter);

            // verify if the current delivery mode is still valid for this address
            getCommerceDeliveryModeValidationStrategy().validateDeliveryMode(parameter);
            getModelService().refresh(cartModel);

            return true;
        }

        return false;
    }
}
