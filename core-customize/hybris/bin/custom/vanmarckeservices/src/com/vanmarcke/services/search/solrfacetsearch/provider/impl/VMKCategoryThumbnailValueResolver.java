package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import org.apache.commons.lang.StringUtils;

/**
 * Value resolvers for category thumbnails.
 *
 * @author Joris Cryns
 * @since 15-7-2019
 */
public class VMKCategoryThumbnailValueResolver extends AbstractValueResolver<CategoryModel, Object, Object> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument document,
                                  IndexerBatchContext context,
                                  IndexedProperty indexedProperty,
                                  CategoryModel categoryModel,
                                  ValueResolverContext<Object, Object> valueResolverContext) throws FieldValueProviderException {

        if (categoryModel.getThumbnail() != null && StringUtils.isNotBlank(categoryModel.getThumbnail().getURL())) {
            document.addField(indexedProperty, categoryModel.getThumbnail().getURL());
        }
    }
}
