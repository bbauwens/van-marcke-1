package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.services.strategies.VMKUpdateCartCheckoutModeStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

/**
 * The {@link VMKUpdateCartCheckoutModeStrategyImpl} class implements the business logic to update the cart's checkout mode.
 *
 * @author Taki Korovessis, Niels Raemaekers, Christiaan Janssen
 * @since 02-10-2019
 */
public class VMKUpdateCartCheckoutModeStrategyImpl extends AbstractBusinessService implements VMKUpdateCartCheckoutModeStrategy {

    private final CommerceCartCalculationStrategy commerceCartCalculationStrategy;

    /**
     * Creates a new instance of the {@link VMKUpdateCartCheckoutModeStrategyImpl} class.
     *
     * @param commerceCartCalculationStrategy the cart calculation strategy
     */
    public VMKUpdateCartCheckoutModeStrategyImpl(CommerceCartCalculationStrategy commerceCartCalculationStrategy) {
        this.commerceCartCalculationStrategy = commerceCartCalculationStrategy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateCartCheckoutMode(CommerceCartParameter parameter) {
        CartModel cart = parameter.getCart();

        validateParameterNotNull(cart, "Cart model cannot be null");

        if (cart.getCheckoutMode() == null || !cart.getCheckoutMode().equals(parameter.getCheckoutMode())) {
            cart.setCheckoutMode(parameter.getCheckoutMode());
            refresh(parameter, cart);
        } else if (!parameter.getCheckoutMode() && !cart.getCheckoutMode()) {
            cart.setDeliveryPointOfService(null);
            refresh(parameter, cart);
        }
    }

    /**
     * Saves, refreshes and reculculates the given {@code cart}.
     *
     * @param parameter the parameter
     * @param cart      the cart
     */
    private void refresh(CommerceCartParameter parameter, CartModel cart) {
        getModelService().save(cart);
        getModelService().refresh(cart);
        commerceCartCalculationStrategy.recalculateCart(parameter);
    }
}