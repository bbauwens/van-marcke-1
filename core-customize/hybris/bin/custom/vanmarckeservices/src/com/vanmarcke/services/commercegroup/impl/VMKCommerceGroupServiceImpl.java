package com.vanmarcke.services.commercegroup.impl;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import org.springframework.beans.factory.annotation.Required;

/**
 * Default implementation of the {@link VMKCommerceGroupService}
 */
public class VMKCommerceGroupServiceImpl implements VMKCommerceGroupService {

    private CMSSiteService cmsSiteService;

    /**
     * {@inheritDoc}
     */
    @Override
    public CommerceGroupModel getCurrentCommerceGroup() {
        final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
        return currentSite != null ? currentSite.getCommerceGroup() : null;
    }

    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }
}