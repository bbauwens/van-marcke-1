package com.vanmarcke.services.product.impl;

import com.vanmarcke.core.constants.VanmarckeCoreConstants;
import com.vanmarcke.core.product.VMKPagedVariantProductDao;
import com.vanmarcke.facades.data.ProductExportFeedResult;
import com.vanmarcke.services.product.VMKProductClassificationExportService;
import com.vanmarcke.services.product.price.VMKFullEtimProductExportService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.LocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;
import java.util.stream.IntStream;

import static de.hybris.platform.catalog.enums.ArticleApprovalStatus.APPROVED;
import static de.hybris.platform.servicelayer.search.paginated.util.PaginatedSearchUtils.createSearchPageDataWithPagination;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.util.CSVConstants.*;
import static de.hybris.platform.util.CSVUtils.buildCsvLine;
import static java.lang.String.valueOf;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.ArrayUtils.getLength;
import static org.apache.commons.lang3.StringUtils.split;

/**
 * Service which exports product classifications in CSV format.
 *
 * @author Joris Cryns
 * @since 23-8-2019
 */
public class VMKCSVProductClassificationExportServiceImpl implements VMKProductClassificationExportService, VMKFullEtimProductExportService {

    private static final Logger LOG = LoggerFactory.getLogger(VMKCSVProductClassificationExportServiceImpl.class);

    private static final String CSV_HEADER = "#;vanmarckeitemnumber;class id;attribute;attribute index;value";
    private static final String CSV_HEADER_FULL_ETIM = "sku;AS400 description nl;AS400 description fr;etim class code;etim class name nl;etim class name fr;" +
            "etim feature code;etim feature name nl;etim feature name fr;etim feature index;etim value;etim value name nl;etim value name fr;";

    private static final int PAGE_SIZE = 1000;
    private final Locale localeNL = LocaleUtils.toLocale(VanmarckeCoreConstants.Languages.ISO_CODE_NL);
    private final Locale localeFR = LocaleUtils.toLocale(VanmarckeCoreConstants.Languages.ISO_CODE_FR);

    private ClassificationService classificationService;
    private VMKPagedVariantProductDao pagedVariantProductDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductExportFeedResult generateProductClassificationFeed(final CatalogVersionModel catalogVersion, final ClassificationSystemVersionModel systemVersion, final Date lastModifiedTime) {
        validateParameterNotNull(catalogVersion, "catalog version cannot be null");
        validateParameterNotNull(catalogVersion, "system version cannot be null");

        int currentPage = 0, numberOfPages;

        final ProductExportFeedResult productExportFeedResult = new ProductExportFeedResult();

        final StringBuilder result = new StringBuilder(CSV_HEADER).append(HYBRIS_LINE_SEPARATOR);
        do {
            final SearchPageData<VariantProductModel> searchPageData = createSearchPageDataWithPagination(PAGE_SIZE, currentPage, true);
            //TODO: remove catalogVersion and APPROVED status. Use personalization rules instead.
            final SearchPageData<VariantProductModel> resultPageData = pagedVariantProductDao.findProductsByCatalogVersionApprovalStatusAndDate(catalogVersion, APPROVED, lastModifiedTime, searchPageData);

            if (isNotEmpty(resultPageData.getResults())) {
                productExportFeedResult.setLastModifiedTime(resultPageData.getResults().get(0).getModifiedtime());
                for (final VariantProductModel v : resultPageData.getResults()) {
                    final FeatureList featureList = classificationService.getFeatures(v);
                    featureList.getFeatures()
                            .stream()
                            .filter(f -> systemVersion.equals(f.getClassAttributeAssignment().getSystemVersion()))
                            .filter(f -> isNotEmpty(f.getValues()))
                            .forEach(f -> IntStream.range(0, f.getValues().size())
                                    .boxed()
                                    .forEach(i -> {
                                        result.append(HYBRIS_FIELD_SEPARATOR);

                                        final Map<Integer, String> fields = new HashMap<>();
                                        fields.put(0, v.getCode());
                                        fields.put(1, f.getClassAttributeAssignment().getClassificationClass().getCode());
                                        fields.put(2, format(f.getClassAttributeAssignment().getClassificationAttribute()));
                                        fields.put(3, valueOf(i + 1));
                                        fields.put(4, format(f.getValues().get(i)));

                                        result.append(buildCsvLineInternal(fields));

                                        result.append(HYBRIS_LINE_SEPARATOR);
                                    }));
                }
            }

            currentPage++;
            numberOfPages = resultPageData.getPagination().getNumberOfPages();
        } while (currentPage < numberOfPages);

        productExportFeedResult.setFeed(result.toString());
        return productExportFeedResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductExportFeedResult createFullEtimProductExportLines(final CatalogVersionModel catalogVersion) {
        validateParameterNotNull(catalogVersion, "catalog version cannot be null");

        int currentPage = 0, numberOfPages;
        final ProductExportFeedResult productExportFeedResult = new ProductExportFeedResult();
        final StringBuilder result = new StringBuilder(CSV_HEADER_FULL_ETIM).append(HYBRIS_LINE_SEPARATOR);

        do {
            final SearchPageData<VariantProductModel> searchPageData =
                    createSearchPageDataWithPagination(PAGE_SIZE, currentPage, true);

            final SearchPageData<VariantProductModel> resultPageData =
                    pagedVariantProductDao.findProductsByCatalogVersionAndApprovalStatus(catalogVersion, APPROVED, searchPageData);

            if (isNotEmpty(resultPageData.getResults())) {

                for (final VariantProductModel variant : resultPageData.getResults()) {
                    final List<Feature> features = classificationService.getFeatures(variant).getFeatures();

                    features
                            .stream()
                            .filter(f -> f.getClassAttributeAssignment().getSystemVersion().getCatalog().getId().equals(VanmarckeCoreConstants.ETIM_CLASSIFICATION))
                            .filter(feature -> feature.getClassAttributeAssignment().getSystemVersion().getActive())
                            .forEach(f -> {
                                if (isNotEmpty(f.getValues())) {
                                    for (int i = 0; i < f.getValues().size(); i++) {
                                        final Map<Integer, String> fields = new HashMap<>();
                                        exportClassificationData(variant, f, fields);

                                        fields.put(9, valueOf(i + 1));
                                        fields.put(10, format(f.getValues().get(i)));

                                        if (f.getValues().get(i).getValue() instanceof ClassificationAttributeValueModel) {
                                            fields.put(11, (((ClassificationAttributeValueModel) f.getValues().get(i).getValue()).getName(localeNL)));
                                            fields.put(12, (((ClassificationAttributeValueModel) f.getValues().get(i).getValue()).getName(localeFR)));
                                        }

                                        result.append(buildCsvLineInternal(fields));
                                        result.append(HYBRIS_LINE_SEPARATOR);
                                    }

                                } else {
                                    final Map<Integer, String> fields = new HashMap<>();
                                    exportClassificationData(variant, f, fields);

                                    result.append(buildCsvLineInternal(fields));
                                    result.append(HYBRIS_LINE_SEPARATOR);
                                }
                            });
                }
            }

            currentPage++;
            numberOfPages = resultPageData.getPagination().getNumberOfPages();
        } while (currentPage < numberOfPages);

        if (StringUtils.countMatches(result.toString(), HYBRIS_LINE_SEPARATOR) > 1) {
            productExportFeedResult.setFeed(result.toString());
        }
        return productExportFeedResult;
    }

    /**
     * This method will add the classification data to the map containing the export information.
     *
     * @param variant product variant
     * @param f classification feature
     * @param fields map containing the export information for the given variant
     */
    private void exportClassificationData(VariantProductModel variant, Feature f, Map<Integer, String> fields) {
        fields.put(0, variant.getCode());
        fields.put(1, variant.getAs400_ShortDescription(localeNL));
        fields.put(2, variant.getAs400_ShortDescription(localeFR));
        fields.put(3, f.getClassAttributeAssignment().getClassificationClass().getCode());
        fields.put(4, f.getClassAttributeAssignment().getClassificationClass().getName(localeNL));
        fields.put(5, f.getClassAttributeAssignment().getClassificationClass().getName(localeFR));
        fields.put(6, f.getClassAttributeAssignment().getClassificationAttribute().getCode());
        fields.put(7, f.getClassAttributeAssignment().getClassificationAttribute().getName(localeNL));
        fields.put(8, f.getClassAttributeAssignment().getClassificationAttribute().getName(localeFR));
    }

    /**
     * This method has been created so JUnit tests can spy this, not requiring a running instance (integration test)
     */
    protected String buildCsvLineInternal(final Map<Integer, String> fields) {
        return buildCsvLine(fields, HYBRIS_FIELD_SEPARATOR, HYBRIS_QUOTE_CHARACTER, HYBRIS_LINE_SEPARATOR);
    }

    /**
     * Formats the classification attribute.
     *
     * @param classificationAttribute the classification attribute
     * @return the formatted attribute
     */
    protected String format(final ClassificationAttributeModel classificationAttribute) {
        final String[] splitCode = split(classificationAttribute.getCode(), "_");
        return getLength(splitCode) > 1 ? splitCode[1] : classificationAttribute.getCode();
    }

    /**
     * Formats the feature value.
     *
     * @param value the feature value
     * @return the formatted feature value
     */
    protected String format(final FeatureValue value) {
        final String formattedValue;
        if (value.getValue() instanceof ClassificationAttributeValueModel) {
            formattedValue = ((ClassificationAttributeValueModel) value.getValue()).getCode();
        } else if (NumberUtils.isNumber(valueOf(value.getValue()))) {
            formattedValue = valueOf(value.getValue()).replaceAll("\\.0*$", "");
        } else if (value.getValue() instanceof Boolean) {
            formattedValue = BooleanUtils.isTrue((Boolean) value.getValue()) ? "Y" : "N";
        } else {
            formattedValue = value.getValue().toString();
        }
        return formattedValue;
    }

    @Required
    public void setClassificationService(final ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    @Required
    public void setPagedVariantProductDao(final VMKPagedVariantProductDao pagedVariantProductDao) {
        this.pagedVariantProductDao = pagedVariantProductDao;
    }
}