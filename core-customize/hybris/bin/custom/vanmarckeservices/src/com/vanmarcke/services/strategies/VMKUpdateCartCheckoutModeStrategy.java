package com.vanmarcke.services.strategies;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;

/**
 * Abstraction for strategy updating the checkout mode.
 */
@FunctionalInterface
public interface VMKUpdateCartCheckoutModeStrategy {

    /**
     * Updates the checkout mode for the cart
     *
     * @param parameter the cart parameter
     */
    void updateCartCheckoutMode(CommerceCartParameter parameter);
}