package com.vanmarcke.services.search.solrfacetsearch.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchResponseSubCategoriesPopulator;
import de.hybris.platform.solrfacetsearch.search.Facet;
import de.hybris.platform.solrfacetsearch.search.FacetValue;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The {@code VMKSearchResponseSubCategoriesPopulator} class is used to populate the subcategories in the search
 * response.
 * <p>
 * We've overwritten the {@link SearchResponseSubCategoriesPopulator} instance because we want to sort the categories by
 * their code instead of their name.
 *
 * @author Christiaan Janssen
 * @since 20-04-2020
 */
public class VMKSearchResponseSubCategoriesPopulator extends SearchResponseSubCategoriesPopulator {

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<CategoryModel> buildSubCategories(String categoryCode, SearchResult solrSearchResult) {
        if (solrSearchResult == null || solrSearchResult.getNumberOfResults() <= 0 || StringUtils.isEmpty(categoryCode)) {
            return null;
        }

        Facet categoryPathFacet = solrSearchResult.getFacet("categoryPath");
        if (categoryPathFacet == null) {
            return null;
        }

        CategoryModel currentCategory = getCommerceCategoryService().getCategoryForCode(categoryCode);
        if (currentCategory == null) {
            return null;
        }

        List<CategoryModel> subCategories = new ArrayList<>();
        for (String subCategoryCode : extractSubcategoryCodes(categoryPathFacet, currentCategory)) {
            subCategories.add(getCommerceCategoryService().getCategoryForCode(subCategoryCode));
        }

        subCategories.sort(VMKCategoryComparator.INSTANCE);

        return subCategories;
    }

    /**
     * Extracts the subcategory codes of the {@code currentCategory} from the given {@code categoryPathFacet}.
     *
     * @param categoryPathFacet the cateogry path facet
     * @param currentCategory   the current category
     * @return the subcategory codes
     */
    private Set<String> extractSubcategoryCodes(Facet categoryPathFacet, CategoryModel currentCategory) {
        Set<String> prefixFilters = getPathsForCategory(currentCategory);

        Set<String> subCategoryCodes = new HashSet<>();
        for (FacetValue facetValue : categoryPathFacet.getFacetValues()) {
            String subCategoryPath = extractCategorySubPath(prefixFilters, facetValue.getName());
            if (subCategoryPath != null && !subCategoryPath.isEmpty()) {
                subCategoryCodes.add(subCategoryPath);
            }
        }
        return subCategoryCodes;
    }

    /**
     * The {@code VMKCategoryComparator} is used to compare {@link CategoryModel} instances.
     * <p>
     * We've overwritten the {@link CategoryComparator} instance because we want to compare the category's code instead
     * of its name.
     */
    private static class VMKCategoryComparator extends CategoryComparator {

        protected static final VMKCategoryComparator INSTANCE = new VMKCategoryComparator();

        /**
         * {@inheritDoc}
         */
        @Override
        protected int compareInstances(CategoryModel a, CategoryModel b) {
            return compareValues(a.getCode(), b.getCode(), false);
        }
    }
}
