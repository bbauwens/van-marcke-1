package com.vanmarcke.services.order.payment;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;

/**
 * Service which provides functionality around available and supported payment types.
 */
public interface VMKPaymentTypeService {

    /**
     * Gets the supported payment types for the given order
     *
     * @param order the order
     * @return a list of supported payment types
     */
    List<CheckoutPaymentType> getSupportedPaymentTypeListForOrder(AbstractOrderModel order);
}