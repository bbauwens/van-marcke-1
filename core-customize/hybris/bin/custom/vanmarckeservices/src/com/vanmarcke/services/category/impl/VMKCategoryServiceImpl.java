package com.vanmarcke.services.category.impl;

import com.vanmarcke.core.model.BrandCategoryModel;
import com.vanmarcke.services.category.VMKCategoryService;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.media.MediaService;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

/**
 * Implements methods for Categories and Brands.
 *
 * @author Tom van den Berg
 * @since 25-08-2020
 */
public class VMKCategoryServiceImpl implements VMKCategoryService {

    private final I18NService i18NService;
    private final MediaService mediaService;

    /**
     * Provides an instance of the DefaultVMKCategoryService.
     *
     * @param i18NService  the I18N service
     * @param mediaService the media service
     */
    public VMKCategoryServiceImpl(I18NService i18NService, MediaService mediaService) {
        this.i18NService = i18NService;
        this.mediaService = mediaService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<MediaModel> getLocalizedBrandLogoForFormat(BrandCategoryModel brand, String mediaFormat) {
        List<MediaContainerModel> containers = getLocalizedBrandLogo(brand);

        if (CollectionUtils.isNotEmpty(containers)) {
            return getBrandLogoForFormat(mediaFormat, containers);
        }
        return Optional.empty();
    }

    /**
     * Retrieves the brand logo for the given {@link BrandCategoryModel} and the current locale.
     * <p>
     * If it finds nothing it will try the fall back locales.
     *
     * @param brandCategory the brand
     * @return the list of brand logos
     */
    protected List<MediaContainerModel> getLocalizedBrandLogo(BrandCategoryModel brandCategory) {
        List<MediaContainerModel> containers = brandCategory.getBrandLogo(i18NService.getCurrentLocale());

        if (containers.isEmpty()) {
            containers = getLocalizedBrandLogoWithFallbackLocale(brandCategory);
        }

        return containers;
    }

    /**
     * Extracts the brandlogos for the given {@link BrandCategoryModel} based on the fallback locales.
     *
     * @param brandCategory the brand
     * @return the list of brand logos
     */
    protected List<MediaContainerModel> getLocalizedBrandLogoWithFallbackLocale(BrandCategoryModel brandCategory) {
        Locale[] fallBackLanguages = i18NService.getFallbackLocales(i18NService.getCurrentLocale());

        List<MediaContainerModel> containers = new ArrayList<>();

        for (Locale fallBackLanguage : fallBackLanguages) {
            containers.addAll(brandCategory.getBrandLogo(fallBackLanguage));

            if (isNotEmpty(containers)) {
                break;
            }
        }

        return containers;
    }

    /**
     * Retrieves the brand logo for the given media format from the given list of {@link MediaContainerModel} instances.
     *
     * @param mediaFormat the media format
     * @param containers  the list of media containers
     * @return an optional media model
     */
    protected Optional<MediaModel> getBrandLogoForFormat(String mediaFormat, List<MediaContainerModel> containers) {
        MediaFormatModel mediaFormatModel = mediaService.getFormat(mediaFormat);
        MediaModel media = null;
        try {
            media = mediaService.getMediaByFormat(containers.get(0), mediaFormatModel);
        } catch (ModelNotFoundException e) {
            //do nothing
        }
        return Optional.ofNullable(media);
    }
}
