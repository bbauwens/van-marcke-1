package com.vanmarcke.services.product.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commerceservices.product.CommerceProductReferenceService;
import de.hybris.platform.commerceservices.product.data.ReferenceData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Factory for CommerceProductReferenceService
 */
public class VMKCommerceProductReferenceServiceFactory implements CommerceProductReferenceService<ProductReferenceTypeEnum, ProductModel> {

    private CommerceProductReferenceService defaultCommerceProductReferenceService;
    private CommerceProductReferenceService vmkCrossSellCommerceProductReferenceService;

    /**
     * Constructor for VMKCommerceProductReferenceServiceFactory
     *
     * @param defaultCommerceProductReferenceService      the defaultCommerceProductReferenceService
     * @param vmkCrossSellCommerceProductReferenceService the vmkCrossSellCommerceProductReferenceService
     */
    public VMKCommerceProductReferenceServiceFactory(final CommerceProductReferenceService defaultCommerceProductReferenceService, final CommerceProductReferenceService vmkCrossSellCommerceProductReferenceService) {
        this.defaultCommerceProductReferenceService = defaultCommerceProductReferenceService;
        this.vmkCrossSellCommerceProductReferenceService = vmkCrossSellCommerceProductReferenceService;
    }

    /**
     * @deprecated since forever
     */
    @Override
    @Deprecated
    public List<ReferenceData<ProductReferenceTypeEnum, ProductModel>> getProductReferencesForCode(final String code, final ProductReferenceTypeEnum referenceType, final Integer limit) {
        return get(referenceType).getProductReferencesForCode(code, referenceType, limit);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ReferenceData<ProductReferenceTypeEnum, ProductModel>> getProductReferencesForCode(final String code, final List<ProductReferenceTypeEnum> referenceTypes, final Integer limit) {
        final List<ReferenceData<ProductReferenceTypeEnum, ProductModel>> referenceData = new ArrayList<>();

        referenceTypes.stream()
                .map(ref -> get(ref).getProductReferencesForCode(code, Collections.singletonList(ref), limit))
                .forEach(ref -> referenceData.addAll(ref));

        return referenceData;
    }

    /**
     * Gets the correct implementation of the CommerceProductReferenceService based on the referenceType
     *
     * @param referenceType the given referenceType
     * @return correct implementation of the CommerceProductReferenceService
     */
    protected CommerceProductReferenceService get(final ProductReferenceTypeEnum referenceType) {
        if (referenceType.equals(ProductReferenceTypeEnum.CROSSELLING)) {
            return this.vmkCrossSellCommerceProductReferenceService;
        }

        return this.defaultCommerceProductReferenceService;
    }
}
