package com.vanmarcke.services.b2b;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;

/**
 * Custom interface for B2BUnitService
 *
 * @author Joris Cryns, Niels Raemaekers, Tom Martens
 * @since 07-07-2019
 */
public interface VMKB2BUnitService extends B2BUnitService<B2BUnitModel, B2BCustomerModel> {

    /**
     * Get the {@link B2BUnitModel} for current {@link de.hybris.platform.core.model.user.UserModel}
     *
     * @return the B2BUnit for User if found, else <code>null</code> is returned.
     */
    B2BUnitModel getB2BUnitModelForCurrentUser();

    /**
     * Get the Customer's B2B unit ID or fallback to the current basestore fallback customer id
     *
     * @return the customer's B2B Unit ID
     */
    String getCustomerB2BUnitID();

    /**
     * Get the Customer's B2B unit ID or fallback to the current basestore fallback customer id
     *
     * @param userModel the user for which we need the parent b2b unit
     * @return the customer's B2B Unit ID
     */
    String getCustomerB2BUnitID(UserModel userModel);

    /**
     * Sets the default address entry
     *
     * @param b2BUnit      the B2B Unit
     * @param addressModel the address model to be set as default one
     */
    void setDefaultDeliveryAddressEntry(B2BUnitModel b2BUnit, AddressModel addressModel);

    /**
     * Check whether the customer must add a reference
     *
     * @return the decision
     */
    boolean mustAddReference();

    /**
     * Decisions if the customer must add the reference for a separate invoice
     *
     * @return the decision
     */
    boolean mandatorySeparateInvoiceReference();
}
