package com.vanmarcke.services.strategies;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

/**
 * Strategy which provides functionality around payment capturing.
 */
public interface VMKCommercePaymentCaptureStrategy {

    /**
     * Capture the payment amount for the specified order
     *
     * @param order the order
     * @return {@link PaymentTransactionEntryModel} from the capture result
     */
    PaymentTransactionEntryModel capturePaymentAmount(AbstractOrderModel order);

    /**
     * Check if the payment has been captured for the given order
     *
     * @param order the given order
     * @return decision whether the payment has been captured or not
     */
    boolean isPaymentCapturedForOrder(OrderModel order);

    /**
     * Check if the payment has been captured for the given entry
     *
     * @param lastEntry the given entry
     * @return decision whether the payment has been captured or not
     */
    boolean isPaymentCapturedForEntry(PaymentTransactionEntryModel lastEntry);
}