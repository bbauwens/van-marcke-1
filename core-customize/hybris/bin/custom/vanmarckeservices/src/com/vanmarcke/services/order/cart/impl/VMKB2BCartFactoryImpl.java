package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.services.order.cart.VMKB2BCartFactory;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commerceservices.order.impl.CommerceCartFactory;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

public class VMKB2BCartFactoryImpl extends CommerceCartFactory implements VMKB2BCartFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKB2BCartFactoryImpl.class);

    private final B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;
    private final ModelService modelService;
    private final I18NService i18nService;

    /**
     * Constructor
     *
     * @param b2bUnitService The b2bUnitService
     * @param modelService   The modelService
     * @param i18nService    The i18nService
     */
    public VMKB2BCartFactoryImpl(B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService, ModelService modelService, I18NService i18nService) {
        this.b2bUnitService = b2bUnitService;
        this.modelService = modelService;
        this.i18nService = i18nService;
    }

    @Override
    public CartModel createCart() {
        CartModel cart = super.createCart();
        postProcessCart(cart);

        return cart;
    }

    @Override
    public boolean isB2BCart(final CartModel cart) {
        return cart.getUser() instanceof B2BCustomerModel;
    }

    private void postProcessCart(final CartModel cart) {
        cart.setLocale(i18nService.getCurrentLocale().toString());
        cart.setStatus(OrderStatus.CREATED);
        cart.setPaymentType(CheckoutPaymentType.ACCOUNT);
        if (isB2BCart(cart)) {
            final B2BUnitModel unit = b2bUnitService.getParent((B2BCustomerModel) cart.getUser());
            Assert.notNull(unit,
                    String.format("No B2BUnit associated to cart %s created by %s", cart.getCode(), cart.getUser().getUid()));
            cart.setUnit(unit);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("Setting B2BUnit %s on Cart %s created by %s", unit.getUid(), cart.getCode(), cart.getUser()));
            }
        }
        modelService.save(cart);
    }

}
