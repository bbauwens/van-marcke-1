package com.vanmarcke.services.request;

import java.util.function.Function;

public interface VMKYRequestService {

    /**
     * Get request attribute for given key.
     *
     * @param key attribute key
     * @param <T> target type
     * @return value
     */
    <T> T getAttribute(VMKYRequestKey key);

    /**
     * Set request attribute.
     *
     * @param key   attribute key
     * @param value attribute value
     */
    void setAttribute(VMKYRequestKey key, Object value);

    /**
     * Remove request attribute for given key.
     *
     * @param key attribute key
     */
    void removeAttribute(VMKYRequestKey key);

    /**
     * Get request attribute for given key. If not exist put value provided by the function in request cache.
     *
     * @param key      attribute key
     * @param function value provider function
     * @param <T>      target type
     * @return value
     */
    <T> T computeIfAbsent(VMKYRequestKey key, Function<VMKYRequestKey, T> function);
}
