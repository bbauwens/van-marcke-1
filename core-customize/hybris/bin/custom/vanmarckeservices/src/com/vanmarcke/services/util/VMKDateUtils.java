package com.vanmarcke.services.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;

/**
 * Provides util methods for dates.
 *
 * @author Tom van den Berg
 * @since 28-08-2020
 */
public class VMKDateUtils {

    /**
     * Creates a {@link Date} object from the given year, month and day
     *
     * @param year  the year
     * @param month the month
     * @param day   the day
     * @return a date object
     */
    public static Date createDate(int year, int month, int day) {
        return Date.from(LocalDate.of(year, month, day).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Creates a formatted date as a String from the given year, month and day
     *
     * @param year  the year
     * @param month the month
     * @param day   the day
     * @return a formatted date string
     */
    public static String createDateString(int year, int month, int day) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyy-MM-dd");
        return LocalDate.of(year, month, day).format(dateFormatter);
    }

    /**
     * Creates a formatted date as a String from the {@link Date}.
     *
     * @param date the date object
     * @return a formatted date string
     */
    public static String createDateString(Date date) {
        return new SimpleDateFormat("yyy-MM-dd").format(date);
    }

    /**
     * Converts the given {@link Date} instance to a {@link LocalDate} instance
     *
     * @param date the {@link Date} instance
     * @return the {@link LocalDate} instance
     */
    public static LocalDate createLocalDate(Date date) {
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    /**
     * Returns difference in days between from and to dates.
     *
     * @param fromDate from date
     * @param toDate   to date
     * @return days difference
     */
    public static int daysBetween(final Date fromDate, final Date toDate) {
        return (int) ChronoUnit.DAYS.between(fromDate.toInstant(), toDate.toInstant());
    }

    /**
     * Returns today's date.
     *
     * @return the date
     */
    public static Date getTodaysDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static long calendarDaysBetween(final Date fromDate, final Date toDate) {
        final long milliSecondsPerDay = 60 * 60 * 24 * 1000;

        final Calendar dateStartCal = Calendar.getInstance();
        dateStartCal.setTime(fromDate);
        dateStartCal.set(Calendar.HOUR_OF_DAY, 23);
        dateStartCal.set(Calendar.MINUTE, 59);
        dateStartCal.set(Calendar.SECOND, 59);
        dateStartCal.set(Calendar.MILLISECOND, 999);

        final Calendar dateEndCal = Calendar.getInstance();
        dateEndCal.setTime(toDate);
        dateEndCal.set(Calendar.HOUR_OF_DAY, 0);
        dateEndCal.set(Calendar.MINUTE, 0);
        dateEndCal.set(Calendar.SECOND, 0);
        dateEndCal.set(Calendar.MILLISECOND, 0);

        return (dateEndCal.getTimeInMillis() - dateStartCal.getTimeInMillis()) / milliSecondsPerDay;

    }

    /**
     * Check if a given date is a week day
     * @param date the date to be checked
     * @return true if it is M, T, W, T, F
     */
    public static boolean isWeekDay(LocalDate date) {
        return !(SATURDAY.equals(date.getDayOfWeek()) ||
                SUNDAY.equals(date.getDayOfWeek()));
    }

    /**
     * Calculate the number of working days between the given dates
     *
     * @param start the start date
     * @param end the end date
     * @return the number of days excluding start date, end date
     *                                      and weekend days
     */
    public static long getWorkingDaysBetween(Date start, Date end) {
        final long totalNumberOfDaysBetween = calendarDaysBetween(start, end);
        long weekDaysBetween = 0;

        LocalDate startLocalDate = createLocalDate(start);

        for (int i = 1; i <= totalNumberOfDaysBetween; i++) {
            if (isWeekDay(startLocalDate.plusDays(i))) {
                weekDaysBetween++;
            }
        }

        return weekDaysBetween;
    }

    /**
     * Private constructor to hide the implicit constructor.
     */
    private VMKDateUtils() {
    }

}
