package com.vanmarcke.services.odata.processor;

import de.hybris.platform.odata2services.odata.processor.writer.CallbackWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.olingo.odata2.api.ODataCallback;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.ep.EntityProviderWriteProperties;
import org.apache.olingo.odata2.api.ep.callback.WriteCallbackContext;
import org.apache.olingo.odata2.api.ep.callback.WriteEntryCallbackContext;
import org.apache.olingo.odata2.api.ep.callback.WriteEntryCallbackResult;
import org.apache.olingo.odata2.api.ep.callback.WriteFeedCallbackContext;
import org.apache.olingo.odata2.api.ep.callback.WriteFeedCallbackResult;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.apache.olingo.odata2.api.exception.ODataApplicationException;
import org.apache.olingo.odata2.api.uri.ExpandSelectTreeNode;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class VMKCallbackWriter extends CallbackWriter {
    private static final Logger LOG = LogManager.getLogger();

    @Override
    public WriteFeedCallbackResult retrieveFeedResult(final WriteFeedCallbackContext context) throws ODataApplicationException {
        final WriteFeedCallbackResult result = new WriteFeedCallbackResult();
        result.setInlineProperties(properties(context));
        result.setFeedData(getFeedEntries(context));
        return result;
    }

    @Override
    public WriteEntryCallbackResult retrieveEntryResult(WriteEntryCallbackContext context) throws ODataApplicationException {
        final WriteEntryCallbackResult result = new WriteEntryCallbackResult();
        result.setInlineProperties(properties(context));
        result.setEntryData(getNavPropertyEntityData(context));
        return result;
    }

    private EntityProviderWriteProperties properties(final WriteCallbackContext context) {
        return EntityProviderWriteProperties
                .fromProperties(context.getCurrentWriteProperties())
                .expandSelectTree(context.getCurrentExpandSelectTreeNode())
                .callbacks(populateCallbacks(context.getCurrentExpandSelectTreeNode()))
                .build();
    }

    private Map<String, ODataCallback> populateCallbacks(final ExpandSelectTreeNode expandSelectTree) {
        final Map<String, ODataCallback> callbacks = new HashMap<>();
        expandSelectTree.getLinks().forEach((propName, expandTreeNode) -> callbacks.put(propName, new VMKCallbackWriter()));
        return callbacks;
    }

    private List<Map<String, Object>> getFeedEntries(final WriteFeedCallbackContext context) throws ODataApplicationException {
        try {
            final ODataFeed entries = (ODataFeed) context.getEntryData().get(context.getNavigationProperty().getName());
            return entries == null ? Collections.emptyList() : entries.getEntries()
                    .stream()
                    .map(ODataEntry::getProperties)
                    .collect(Collectors.toList());
        } catch (final EdmException e) {
            LOG.error("Encountered error while trying to get navigation property name from {}", context.getNavigationProperty(), e);
            throw new ODataApplicationException("Could not $expand navigation property.", Locale.ENGLISH, e);
        }
    }

    private Map<String, Object> getNavPropertyEntityData(final WriteEntryCallbackContext context) throws ODataApplicationException {
        try {
            if (context.getEntryData().containsKey(context.getNavigationProperty().getName())) {
                final Object navProperty = context.getEntryData().get(context.getNavigationProperty().getName());

                if (navProperty instanceof Collection) {
                    return ((Collection<?>) navProperty).stream()
                            .filter(ODataEntry.class::isInstance)
                            .map(ODataEntry.class::cast)
                            .flatMap(oDataEntry -> oDataEntry.getProperties().entrySet().stream())
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                } else {
                    return ((ODataEntry) navProperty).getProperties();
                }

            } else {
                return null;
            }
        } catch (final EdmException e) {
            LOG.error("Encountered error while trying to get navigation property name from {}", context.getNavigationProperty(), e);
            throw new ODataApplicationException("Could not $expand navigation property.", Locale.ENGLISH, e);
        }
    }
}
