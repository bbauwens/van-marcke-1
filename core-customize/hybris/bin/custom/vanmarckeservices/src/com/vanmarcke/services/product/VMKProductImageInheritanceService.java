package com.vanmarcke.services.product;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.Date;

/**
 * Service providing functionality regarding the product's image inheritance.
 */
@FunctionalInterface
public interface VMKProductImageInheritanceService {

    /**
     * Updates the product image inheritance since the specified {@link Date} for the given {@CatalogVerionModel}.
     *
     * @param catalogVersion the catalog version
     * @param date           the date
     */
    void updateProductImageInheritance(CatalogVersionModel catalogVersion, Date date);

}