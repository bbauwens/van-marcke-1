package com.vanmarcke.services.product.price.impl;

import com.vanmarcke.cpi.data.builder.PriceRequestDataBuilder;
import com.vanmarcke.cpi.data.price.PriceRequestData;
import com.vanmarcke.cpi.data.price.PriceResponseData;
import com.vanmarcke.cpi.data.price.ProductPriceResponseData;
import com.vanmarcke.cpi.services.ESBPriceService;
import com.vanmarcke.services.product.price.VMKPriceLookupService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.cache.annotation.Cacheable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implements the {@link VMKPriceLookupService}. It provides methods to process price requests.
 *
 * @author Tom van den Berg
 * @since 10-06-2020
 */
public class VMKPriceLookupServiceImpl implements VMKPriceLookupService {

    private final ESBPriceService esbPriceService;

    /**
     * Provides an instance of the {@code DefaultBluePriceLookupService}.
     *
     * @param esbPriceService the ESB price service
     */
    public VMKPriceLookupServiceImpl(ESBPriceService esbPriceService) {
        this.esbPriceService = esbPriceService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(value = "priceInformation", key = "{#product.code, #b2bUnitUid}", cacheManager = "vanMarckeCacheManager")
    public List<ProductPriceResponseData> getPricing(ProductModel product, String b2bUnitUid) {
        PriceRequestData request = PriceRequestDataBuilder
                .aPriceRequest(b2bUnitUid, getVariantCodes(product))
                .build();

        PriceResponseData response = esbPriceService.getPriceInformation(request);

        List<ProductPriceResponseData> pricing = new ArrayList<>();
        if (response != null && CollectionUtils.isNotEmpty(response.getPricing())) {
            pricing = response.getPricing();
        }
        return pricing;
    }

    /**
     * Returns the codes of the given {@code product}'s variants.
     *
     * @param product the product
     * @return the codes of the variants
     */
    private List<String> getVariantCodes(ProductModel product) {
        return product.getVariants()
                .stream()
                .map(VariantProductModel::getCode)
                .collect(Collectors.toList());
    }

}
