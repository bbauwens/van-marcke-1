package com.vanmarcke.services.storesession;

import de.hybris.platform.commerceservices.storesession.StoreSessionService;

/**
 * Custom interface for StoreSessionService
 */
public interface VMKStoreSessionService extends StoreSessionService {

    /**
     * Sets the current site in the session
     *
     * @param uid the uid for the site
     */
    void setCurrentSite(String uid);

    /**
     * Sets the current favorite store in the session
     *
     * @param uid the uid for the store
     */
    void setCurrentStore(String uid);
}