package com.vanmarcke.services.validation.impl;

import com.vanmarcke.cpi.data.order.CartValidationInternalResponseData;
import com.vanmarcke.cpi.data.order.CartValidationResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.services.ESBCartValidationService;
import com.vanmarcke.services.MessageData;
import com.vanmarcke.services.validation.VMKCartValidationService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

/**
 * The {@code DefaultVMKCartValidationService} class implements the business logic that can be used to validate the
 * shopping cart.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 03-02-2020
 */
public class VMKCartValidationServiceImpl implements VMKCartValidationService {

    private final Converter<AbstractOrderModel, OrderRequestData> vmkCartValidationRequestConverter;
    private final ESBCartValidationService esbCartValidationService;

    /**
     * Creates a new instance of the {@link VMKCartValidationServiceImpl} class.
     *
     * @param vmkCartValidationRequestConverter validation request converter
     * @param esbCartValidationService          the ESB validation service
     */
    public VMKCartValidationServiceImpl(Converter<AbstractOrderModel, OrderRequestData> vmkCartValidationRequestConverter,
                                        ESBCartValidationService esbCartValidationService) {
        this.vmkCartValidationRequestConverter = vmkCartValidationRequestConverter;
        this.esbCartValidationService = esbCartValidationService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageData isCartValid(CartModel cart) {
        OrderRequestData request = vmkCartValidationRequestConverter.convert(cart);
        CartValidationResponseData response = esbCartValidationService.getCartValidationInformation(request);
        return getErrorMessage(response);
    }

    /**
     * Converts the given {@code response} to a validation message code.
     *
     * @param response the validation response
     * @return the validation message code
     */
    private MessageData getErrorMessage(CartValidationResponseData response) {
        MessageData messageData = null;
        if (response == null || response.getData() == null) {
            messageData = new MessageData();
            messageData.setKey("cart.validation-error.failed");
        } else {
            for (final CartValidationInternalResponseData suborder : emptyIfNull(response.getData().getOrders())) {
                if (suborder.isInError()) {
                    if (StringUtils.isEmpty(suborder.getErrorCode())) {
                        messageData = suborder.getProducts()
                                .stream()
                                .filter(entry -> entry.getErrorCode() != null)
                                .findFirst()
                                .map(entry -> getErrorMessage(entry.getErrorCode(), entry.getProductID()))
                                .orElse(null);
                        break;
                    } else {
                        messageData = getErrorMessage(suborder.getErrorCode(), null);
                    }
                }
            }
        }
        return messageData;
    }

    /**
     * Returns the error message for the given {@code errorCode}.
     * <p>
     * If the {@code productCode} is provided, this will be used in the error message.
     *
     * @param errorCode   the error code
     * @param productCode the product's unique identifier
     * @return the error message
     */
    protected MessageData getErrorMessage(String errorCode, String productCode) {
        final MessageData messageData = new MessageData();
        switch (errorCode) {
            case "001":
                messageData.setKey("cart.validation-error.representative-not-allowed");
                break;
            case "002":
                messageData.setKey("cart.validation-error.customer-not-allowed");
                break;
            case "016":
                messageData.setKey("cart.validation-error.article-not-allowed");
                messageData.setAttributes(Collections.singletonList(productCode));
                break;
            case "018":
                messageData.setKey("cart.validation-error.invalid-price");
                messageData.setAttributes(Collections.singletonList(productCode));
                break;
            case "019":
                messageData.setKey("cart.validation-error.quantity-invalid");
                messageData.setAttributes(Collections.singletonList(productCode));
                break;
            case "026":
                messageData.setKey("cart.validation-error.no-confirmation");
                break;
            case "027":
                messageData.setKey("cart.validation-error.no-vam");
                break;
            case "028":
                messageData.setKey("cart.validation-error.shipping-address-different-from-vam");
                break;
            case "029":
                messageData.setKey("cart.validation-error.pos-not-allowed");
                break;
            case "030":
                messageData.setKey("cart.validation-error.pos-missing");
                break;
            case "031":
                messageData.setKey("cart.validation-error.yard-reference-missing");
                break;
            case "032":
                messageData.setKey("cart.validation-error.order-amount-exceeded");
                break;
            case "033":
                messageData.setKey("cart.validation-error.credit-line-exceeded");
                break;
            case "034":
                messageData.setKey("cart.validation-error.cash-on-delivery-not-allowed");
                break;
            default:
                messageData.setKey("cart.validation-error.failed");
        }
        return messageData;
    }
}
