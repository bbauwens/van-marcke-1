package com.vanmarcke.services.delivery;

import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Date;
import java.util.Optional;

/**
 * Defines methods for retrieving pickup related information.
 *
 * @author Tom van den Berg
 * @since 31-12-2020
 */
public interface VMKPickupInfoService {

    /**
     * Determine whether pickup is possible for the given cart and supported delivery methods.
     *
     * @param cart the cart
     * @return true if possible
     */
    boolean isPickupPossible(CartModel cart);

    /**
     * Retrieves the first possible pickup date for the given cart.
     *
     * @param cart the cart
     * @return the first possible pickup date
     */
    Optional<Date> getFirstPossiblePickupDate(CartModel cart);

    /**
     * Find first possible pickup date for given cart and pickup date response.
     *
     * @param cart     the cart
     * @param pickupDateResponse the pickup date response; must not be {@code null}
     * @return the first possible pickup date
     */
    Optional<Date> getFirstPossiblePickupDate(CartModel cart, PickupDateInfoResponseData pickupDateResponse);

    /**
     * Find the last possible pickup date for products
     *
     * @param pickupDateResponse the pickup date response
     * @return the last possible date if found
     */
    Optional<Date> getLastPossiblePickupDate(PickupDateInfoResponseData pickupDateResponse);

    /**
     * Determines whether TEC collect is possible for the given cart.
     *
     * @param cart the cart
     * @return true if TEC collect is possible
     */
    boolean isTecCollectPossible(CartModel cart);

    /**
     * Whether Tec collect is possible for given cart and pickup date pickupDateResponse.
     *
     * @param cart               the cart
     * @param pickupDateResponse the pickup date response
     * @return {@code true} if Tec collect is possible
     */
    boolean isTecCollectPossible(CartModel cart, PickupDateInfoResponseData pickupDateResponse);
}
