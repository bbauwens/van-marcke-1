package com.vanmarcke.services.basestore.impl;

import com.vanmarcke.core.model.IBMConfigurationModel;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.impl.DefaultBaseStoreService;

/**
 * Implements methods related to {@link BaseStoreModel} instances.
 *
 * @author Tom van den Berg
 * @since 16-10-2020
 */
public class VMKBaseStoreServiceImpl extends DefaultBaseStoreService implements VMKBaseStoreService {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isIBMBaseStore(BaseStoreModel baseStore) {
        return baseStore.getIbmConfiguration() != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSAPBaseStore(BaseStoreModel baseStore) {
        return baseStore.getSAPConfiguration() != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getBaseUrl() {
        IBMConfigurationModel configuration = getCurrentBaseStore().getIbmConfiguration();
        if (configuration == null || configuration.getBaseURL() == null) {
            throw new IllegalArgumentException("IBM CPI base url not found");
        }
        return configuration.getBaseURL();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUsername() {
        IBMConfigurationModel configuration = getCurrentBaseStore().getIbmConfiguration();
        if (configuration == null || configuration.getUsername() == null) {
            throw new IllegalArgumentException("IBM CPI username not found");
        }
        return configuration.getUsername();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPassword() {
        IBMConfigurationModel configuration = getCurrentBaseStore().getIbmConfiguration();
        if (configuration == null || configuration.getPassword() == null) {
            throw new IllegalArgumentException("IBM CPI password not found");
        }
        return configuration.getPassword();
    }

}
