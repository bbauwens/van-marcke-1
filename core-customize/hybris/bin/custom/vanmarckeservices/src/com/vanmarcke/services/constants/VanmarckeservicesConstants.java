package com.vanmarcke.services.constants;

import de.hybris.platform.basecommerce.enums.WeekDay;

@SuppressWarnings({"deprecation", "PMD", "squid:CallToDeprecatedMethod"})
public class VanmarckeservicesConstants extends GeneratedVanmarckeservicesConstants {

    public static final String EXTENSIONNAME = "vanmarckeservices";

    public interface ERPSystems {
        String IBM = "IBM";
        String S4HANA = "S4HANA";
    }

    public static final WeekDay[] WEEK_DAYS = new WeekDay[]{
            WeekDay.SUNDAY,
            WeekDay.MONDAY,
            WeekDay.TUESDAY,
            WeekDay.WEDNESDAY,
            WeekDay.THURSDAY,
            WeekDay.FRIDAY,
            WeekDay.SATURDAY
    };

    public interface IbmStatusCodes {
        String STATUS_SUCCESS = "OK";
        String STATUS_UNKNOWN = "UNKNOWN";
    }

    private VanmarckeservicesConstants() {
        //empty
    }

    public static final String ETIM_CLASSIFICATION = "EtimClassification";
    public static final String GS1_CLASSIFICATION = "GS1Classification";

    public interface Export {

        String EXPORT_NON_ARCHIVED_FILE_NAME = "exportNonArchivedProducts";
        String EXPORT_DATE_TIME_SUFFIX_FORMAT = "yyyyMMdd";
        String EXPORT_EXTENSION = ".csv";
        String EXPORT_HYPHEN = "-";
        int CURRENCY_DIGITS = 2;

        interface Excel {
            int NAME_COLUMN_NUMBER = 2;
        }
    }

    public interface Languages {

        String ISO_CODE_NL = "nl";
    }

    public interface OData {
        String ITEMTYPE_QUALIFIER = "itemtype";
        String ITEM_COMPOSED_TYPE_QUALIFIER = "itemComposedType";
    }

    // Session attributes for saved baskets / order / cart export to pdf and csv
    public static final String IS_EXPORT_NET_PRICE_ATTRIBUTE = "isNetPriceEnabled";
    public static final String IS_EXPORT_SESSION_ATTRIBUTE = "isExportSession";

    public static final String PAGE_TITLE_SITE_NAME = "Van Marcke Blue";
    public static final int OPEN_DAY_SEARCH_LIMIT = 365;
    public static final String DATE_PATTERN = "yyyy-MM-dd";
}
