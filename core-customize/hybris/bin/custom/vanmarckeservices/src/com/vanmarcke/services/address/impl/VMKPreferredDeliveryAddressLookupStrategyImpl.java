package com.vanmarcke.services.address.impl;

import com.google.common.base.Preconditions;
import com.vanmarcke.services.address.VMKPreferredDeliveryAddressLookupStrategy;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.strategies.DeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * Implementation for BluePreferredDeliveryAddressLookupStrategy
 */
public class VMKPreferredDeliveryAddressLookupStrategyImpl implements VMKPreferredDeliveryAddressLookupStrategy {

    private DeliveryAddressesLookupStrategy deliveryAddressesLookupStrategy;

    /**
     * {@inheritDoc}
     */
    @Override
    public AddressModel getPreferredDeliveryAddressForOrder(final AbstractOrderModel abstractOrder) {

        Preconditions.checkNotNull(abstractOrder.getUnit(), "A B2BUnit must always be set before we can continue with the Checkout!");
        final B2BUnitModel b2BUnit = abstractOrder.getUnit();

        final AddressModel deliveryAddress = b2BUnit.getShippingAddress();
        final AddressModel defaultBillingAddress = b2BUnit.getBillingAddress();

        if (deliveryAddress != null && (deliveryAddress.getCountry().equals(defaultBillingAddress.getCountry()))) {
            return deliveryAddress;
        }

        final List<AddressModel> possibleDeliveryAddresses = this.deliveryAddressesLookupStrategy.getDeliveryAddressesForOrder(abstractOrder, true);
        if (CollectionUtils.isNotEmpty(possibleDeliveryAddresses)) {
            return possibleDeliveryAddresses.get(0);
        }

        return null;
    }

    @Required
    public void setDeliveryAddressesLookupStrategy(final DeliveryAddressesLookupStrategy deliveryAddressesLookupStrategy) {
        this.deliveryAddressesLookupStrategy = deliveryAddressesLookupStrategy;
    }
}
