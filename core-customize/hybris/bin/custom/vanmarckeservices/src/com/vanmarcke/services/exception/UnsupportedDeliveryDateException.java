package com.vanmarcke.services.exception;

/**
 * The {@link UnsupportedDeliveryDateException} class.
 *
 * @author Christiaan Janssen
 * @since 13-09-2021
 */
public class UnsupportedDeliveryDateException extends Exception {

    /**
     * Creates a new instance of the {@link UnsupportedDeliveryDateException} class.
     *
     * @param message the exception message
     */
    public UnsupportedDeliveryDateException(final String message) {
        super(message);
    }
}
