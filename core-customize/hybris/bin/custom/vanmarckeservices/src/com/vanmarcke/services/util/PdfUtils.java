package com.vanmarcke.services.util;

import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 * Class that contains helper method regarding PDF File operations.
 */
public class PdfUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfUtils.class);

    /**
     * Merge all PDF files into one big PDF. If an error occurs the code will return null.
     *
     * @param files List of {@link InputStream}
     * @return {@link InputStream}
     */
    public InputStream mergePdfFiles(List<InputStream> files) {
        PDFMergerUtility utility = new PDFMergerUtility();

        files.forEach(utility::addSource);
        File tempFile = createTempFile();

        try (OutputStream ous = new FileOutputStream(tempFile)) {
            utility.setDestinationStream(ous);
            utility.mergeDocuments(MemoryUsageSetting.setupTempFileOnly());
            return new FileInputStream(tempFile);
        } catch (IOException e) {
            LOGGER.error("Could not merge PDF files!", e);
        }

        return null;
    }

    private File createTempFile() {
        File tempFile = null;
        try {
            tempFile = File.createTempFile("tmp-ds", ".pdf");
        } catch (IOException e) {
            LOGGER.error("Could not create temporary file!", e);
        }
        return tempFile;
    }
}
