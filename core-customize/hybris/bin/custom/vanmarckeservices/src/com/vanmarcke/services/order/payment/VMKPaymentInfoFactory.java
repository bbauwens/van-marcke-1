package com.vanmarcke.services.order.payment;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;

public interface VMKPaymentInfoFactory {

    /**
     * Creates a new instance of {@link PaymentInfoModel} based on the {@link CheckoutPaymentType} of the specified order
     *
     * @param order the order
     * @return {@link InvoicePaymentInfoModel} in case of <code>ACCOUNT</code> otherwise {@link PaymentInfoModel}
     */
    PaymentInfoModel createPaymentInfoForOrder(AbstractOrderModel order);

}