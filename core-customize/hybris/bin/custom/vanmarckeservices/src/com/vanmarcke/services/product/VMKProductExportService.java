package com.vanmarcke.services.product;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;

/**
 * Service providing product export functionality.
 */
@FunctionalInterface
public interface VMKProductExportService {

    /**
     * Generates an XML file for all {@link ProductModel} for the given {@link CatalogVersionModel}.
     *
     * @param catalogVersion The catalog version
     * @param path           the path of the file
     * @return the XML file
     */
    File generateProductFeed(CatalogVersionModel catalogVersion, String path) throws IOException, JAXBException, XMLStreamException;
}