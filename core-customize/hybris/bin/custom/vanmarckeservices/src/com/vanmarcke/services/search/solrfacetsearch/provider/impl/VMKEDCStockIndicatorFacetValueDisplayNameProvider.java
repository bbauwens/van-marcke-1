package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.elision.hybris.l10n.services.LocalizedMessageService;
import com.vanmarcke.core.enums.EDCStockStatus;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang3.StringUtils;

/**
 * Provider to show correct enum translations for Solr results
 *
 * @author Niels Raemaekers
 * @since 02-07-2021
 */
public class VMKEDCStockIndicatorFacetValueDisplayNameProvider extends AbstractFacetValueDisplayNameProvider {

    private static final String EDC_STOCK_VALUE_IN_STOCK = "facet.edc.stock.value.in.stock";
    private static final String EDC_STOCK_VALUE_NO_STOCK = "facet.edc.stock.value.no.stock";
    private static final String EDC_STOCK_VALUE_NOS = "facet.edc.stock.value.nos.product";

    private final LocalizedMessageService localizedMessageService;

    /**
     * Constructor for {@link VMKEDCStockIndicatorFacetValueDisplayNameProvider}
     *
     * @param localizedMessageService {@link LocalizedMessageService}
     */
    public VMKEDCStockIndicatorFacetValueDisplayNameProvider(LocalizedMessageService localizedMessageService) {
        this.localizedMessageService = localizedMessageService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName(SearchQuery searchQuery, IndexedProperty indexedProperty, String originalValue) {
        String translationKey = StringUtils.EMPTY;

        if (originalValue.equals(EDCStockStatus.NOSPRODUCT.getCode())) {
            translationKey = EDC_STOCK_VALUE_NOS;
        }
        if (originalValue.equals(EDCStockStatus.STOCK.getCode())) {
            translationKey = EDC_STOCK_VALUE_IN_STOCK;
        }
        if (originalValue.equals(EDCStockStatus.NOSTOCK.getCode())) {
            translationKey = EDC_STOCK_VALUE_NO_STOCK;
        }

        return localizedMessageService.getLocalizedMessageForKey(translationKey).getMessage();
    }
}
