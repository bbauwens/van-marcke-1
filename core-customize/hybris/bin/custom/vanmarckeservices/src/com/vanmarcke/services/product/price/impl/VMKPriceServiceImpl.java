package com.vanmarcke.services.product.price.impl;

import com.vanmarcke.cpi.data.price.ProductPriceResponseData;
import com.vanmarcke.services.product.price.VMKPriceLookupService;
import com.vanmarcke.services.product.price.VMKPriceService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.product.data.PriceCalculationData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindTaxValuesStrategy;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * The {@code DefaultBluePriceService} class implements the business logic to retrieve pricing information.
 *
 * @author Joris Cryns, Niels Raemaekers, Taki Korovessis, Christiaan Janssen, Tom van den Berg
 * @since 27-06-2019
 */
public class VMKPriceServiceImpl implements VMKPriceService {

    private final CommercePriceService commercePriceService;
    private final VMKPriceLookupService priceLookupService;

    /**
     * Creates a new instance of {@link VMKPriceServiceImpl}
     *
     * @param priceLookupService   the price lookup service
     * @param commercePriceService the default commerce price service
     */
    public VMKPriceServiceImpl(CommercePriceService commercePriceService, VMKPriceLookupService priceLookupService) {
        this.commercePriceService = commercePriceService;
        this.priceLookupService = priceLookupService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PriceCalculationData getPrice(VariantProductModel variant, String b2bUnitUid) {
        return getPriceForVariant(variant, priceLookupService.getPricing(variant.getBaseProduct(), b2bUnitUid));
    }

    /**
     * Returns the price for the given {@code variant}.
     *
     * @param variant the product variant
     * @param pricing the list of prices
     * @return the price
     */
    private PriceCalculationData getPriceForVariant(VariantProductModel variant, List<ProductPriceResponseData> pricing) {
        return pricing
                .stream()
                .filter(entry -> variant.getCode().equals(entry.getProductCode()))
                .findFirst()
                .map(productPriceResponseData -> {
                    PriceCalculationData priceData = new PriceCalculationData();
                    priceData.setPriceValue(productPriceResponseData.getPrice());
                    priceData.setVat(productPriceResponseData.getVat());
                    return priceData;
                })
                .orElseGet(() -> getFallbackPrice(variant));
    }

    /**
     * Returns the fallback price for the given {@code variant}.
     *
     * @param variant the product variant
     * @return the fallback price
     */
    private PriceCalculationData getFallbackPrice(VariantProductModel variant) {
        PriceInformation priceInformation = commercePriceService.getWebPriceForProduct(variant);
        if (priceInformation != null) {
            BigDecimal priceValue = BigDecimal.valueOf(priceInformation.getPriceValue().getValue());
            PriceCalculationData priceData = new PriceCalculationData();
            priceData.setPriceValue(priceValue);
            return priceData;
        }
        return null;
    }
}