package com.vanmarcke.services.strategies;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

/**
 * A strategy for updating the image inheritance between base {@link ProductModel} and {@link VariantProductModel}.
 */
@FunctionalInterface
public interface VMKProductImageInheritanceUpdateStrategy {

    /**
     * Updates the image inheritance for the given {@link VariantProductModel}.
     *
     * @param variantProduct the variant product
     */
    void updateProductImageInheritanceForProduct(VariantProductModel variantProduct);

}