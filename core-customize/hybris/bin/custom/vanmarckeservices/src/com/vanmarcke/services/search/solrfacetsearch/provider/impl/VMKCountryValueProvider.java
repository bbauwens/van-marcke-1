package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.core.model.VanmarckeCountryChannelModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Provider to add the Country where the product is sellable to Solr
 */
public class VMKCountryValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider {

    private FieldNameProvider fieldNameProvider;

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<FieldValue> getFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, Object model) {
        if (!(model instanceof VanMarckeVariantProductModel)) {
            return Collections.emptyList();
        } else {
            Collection<FieldValue> result = new ArrayList<>();

            Set<VanmarckeCountryChannelModel> countryChannelModels = ((VanMarckeVariantProductModel) model).getVanmarckeCountryChannel();
            if (CollectionUtils.isNotEmpty(countryChannelModels)) {
                final Collection<String> channels = getEnabledCountries(countryChannelModels);

                result.addAll(createFieldValue(channels, indexedProperty));
            }
            return result;

        }
    }

    /**
     * Retrieve the enabled countries
     *
     * @param countryChannels the country channels
     * @return the enabled countries
     */
    protected Set<String> getEnabledCountries(Set<VanmarckeCountryChannelModel> countryChannels) {
        return countryChannels.stream()
                .map(channel -> channel.getCountry().getIsocode())
                .collect(Collectors.toSet());
    }

    /**
     * Create field values.
     *
     * @param channels        the channels
     * @param indexedProperty the indexed property
     * @return the list of field values
     */
    protected List<FieldValue> createFieldValue(final Collection<String> channels, final IndexedProperty indexedProperty) {
        final List<FieldValue> fieldValues = new ArrayList<>();

        final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
        for (final String fieldName : fieldNames) {
            for (final String channel : channels) {
                fieldValues.add(new FieldValue(fieldName, channel));
            }
        }

        return fieldValues;
    }

    public FieldNameProvider getFieldNameProvider() {
        return this.fieldNameProvider;
    }

    @Required
    public void setFieldNameProvider(FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }
}