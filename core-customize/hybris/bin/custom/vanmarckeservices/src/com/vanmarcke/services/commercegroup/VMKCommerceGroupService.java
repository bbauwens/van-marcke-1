package com.vanmarcke.services.commercegroup;

import com.vanmarcke.core.model.CommerceGroupModel;

/**
 * Service providing Commerce Group functionality.
 */
@FunctionalInterface
public interface VMKCommerceGroupService {

    /**
     * Returns current commerce group from the session for current user.
     *
     * @return the current commerce group
     */
    CommerceGroupModel getCurrentCommerceGroup();

}