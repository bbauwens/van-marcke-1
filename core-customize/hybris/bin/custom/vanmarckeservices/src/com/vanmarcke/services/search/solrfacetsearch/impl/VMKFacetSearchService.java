package com.vanmarcke.services.search.solrfacetsearch.impl;

import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.provider.IndexedTypeFieldsValuesProvider;
import de.hybris.platform.solrfacetsearch.search.FacetField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultFacetSearchService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Custom implementation for {@link DefaultFacetSearchService} needed to filter variants on PDP similar
 * to the way variants are filtered on PLP, i.e. allowing MultiSelectOr facet search.
 *
 * @author Cristi Stoica
 * @since 15-07-2021
 */
public class VMKFacetSearchService extends DefaultFacetSearchService {

    private final BaseStoreService baseStoreService;

    public VMKFacetSearchService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void populateFacetFields(final FacetSearchConfig facetSearchConfig,
                                       final IndexedType indexedType,
                                       final SearchQueryTemplate searchQueryTemplate,
                                       final SearchQuery searchQuery) {
        if (BooleanUtils.isTrue(searchQueryTemplate.getOverrideSearchQueryProperties())) {
            super.populateFacetFields(facetSearchConfig, indexedType, searchQueryTemplate, searchQuery);
        } else {
            populateFacetFields(facetSearchConfig, indexedType, searchQuery);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void populateFreeTextQuery(FacetSearchConfig facetSearchConfig,
                                         IndexedType indexedType,
                                         SearchQueryTemplate searchQueryTemplate,
                                         SearchQuery searchQuery,
                                         String userQuery) {
        if (BooleanUtils.isTrue(searchQueryTemplate.getOverrideSearchQueryProperties())) {
            super.populateFreeTextQuery(facetSearchConfig, indexedType, searchQueryTemplate, searchQuery, userQuery);
        } else {
            populateFreeTextQuery(facetSearchConfig, indexedType, searchQuery, userQuery);
        }
    }

    /**
     * Overridden to filter facets by classification catalogs assigned to current base store.
     */
    protected void populateFacetFields(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType, final SearchQuery searchQuery) {
        final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
        if (currentBaseStore != null && MapUtils.isNotEmpty(indexedType.getFacetsByBaseStore()) && indexedType.getFacetsByBaseStore().containsKey(currentBaseStore.getUid())) {
            final List<FacetField> facetFields = indexedType.getFacetsByBaseStore().get(currentBaseStore.getUid()).stream()
                    .map(indexedProperty -> {
                        final FacetField facetField = new FacetField(indexedProperty.getName(), indexedProperty.getFacetType());
                        facetField.setPriority(indexedProperty.getPriority());
                        facetField.setDisplayNameProvider(indexedProperty.getFacetDisplayNameProvider());
                        facetField.setSortProvider(indexedProperty.getFacetSortProvider());
                        facetField.setTopValuesProvider(indexedProperty.getTopValuesProvider());
                        return facetField;
                    })
                    .collect(Collectors.toList());
            searchQuery.getFacets().addAll(facetFields);

            final IndexedTypeFieldsValuesProvider fieldsValuesProvider = getFieldsValuesProvider(indexedType);
            if (fieldsValuesProvider != null) {
                fieldsValuesProvider.getFacets().forEach(searchQuery::addFacet);
            }
        } else {
            super.populateFacetFields(facetSearchConfig, indexedType, searchQuery);
        }
    }
}
