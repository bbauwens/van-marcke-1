package com.vanmarcke.services.order.hook.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.order.hook.CommerceCartCalculationMethodHook;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;

public abstract class AbstractVMKCartCalculationMethodHook implements CommerceCartCalculationMethodHook {

    private final ModelService modelService;

    public AbstractVMKCartCalculationMethodHook(final ModelService modelService) {
        this.modelService = modelService;
    }

    protected boolean isB2BCart(final CartModel cart) {
        return cart.getUser() instanceof B2BCustomerModel;
    }

    protected ModelService getModelService() {
        return modelService;
    }
}
