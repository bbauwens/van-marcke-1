package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.core.helper.CheckoutDateHelper;
import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingEntryInfoResponseData;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.delivery.VMKFirstDateAvailabilityService;
import com.vanmarcke.services.delivery.VMKShippingInfoService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BinaryOperator;

/**
 * Provides methods for retrieving shipping information.
 *
 * @author Tom van den Berg
 * @since 30-12-2020
 */
public class VMKShippingInfoServiceImpl implements VMKShippingInfoService {

    private final VMKDeliveryInfoService deliveryInfoService;
    private final CheckoutDateHelper checkoutDateHelper;
    private final VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService;

    /**
     * Provides an instance of the VMKShippingInfoServiceImpl.
     *
     * @param deliveryInfoService             the delivery info service
     * @param checkoutDateHelper              the checkout date helper
     * @param vmkFirstDateAvailabilityService the first availability day service
     */
    public VMKShippingInfoServiceImpl(VMKDeliveryInfoService deliveryInfoService, CheckoutDateHelper checkoutDateHelper, VMKFirstDateAvailabilityService vmkFirstDateAvailabilityService) {
        this.deliveryInfoService = deliveryInfoService;
        this.checkoutDateHelper = checkoutDateHelper;
        this.vmkFirstDateAvailabilityService = vmkFirstDateAvailabilityService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isShippingPossible(CartModel cart) {
        return getFirstPossibleShippingDate(cart).isPresent();
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public Optional<Date> getFirstPossibleShippingDate(final CartModel cart) {
        return deliveryInfoService.getShippingDateInformation(cart)
                .map(response -> getFirstPossibleShippingDate(cart, response));
    }

    @Override
    public Date getFirstPossibleShippingDate(final CartModel cart, final ShippingDateInfoResponseData shippingDateInfoResponse) {
        ServicesUtil.validateParameterNotNullStandardMessage("shippingDateInfoResponse", shippingDateInfoResponse);

        return getEarliestShippingDate(shippingDateInfoResponse)
                .map(vmkFirstDateAvailabilityService::getFirstValidNoClosingDate)
                .filter(checkoutDateHelper::isValidDate)
                .orElse(null);

    }

    @Override
    public Date getLastPossibleShippingDate(final ShippingDateInfoResponseData response) {
        return CollectionUtils.emptyIfNull(response.getResults()).stream()
                .map(ShippingEntryInfoResponseData::getDeliveryDate)
                .filter(Objects::nonNull)
                .max(Date::compareTo)
                .orElse(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getFirstPossibleShippingDateForCompleteCart(final CartModel cart) {
        return deliveryInfoService.getShippingDateInformation(cart)
                .filter(this::allProductsHaveDeliveryDate)
                .flatMap(this::getLatestShippingDate)
                .map(vmkFirstDateAvailabilityService::getFirstValidNoClosingDate)
                .filter(checkoutDateHelper::isValidDate)
                .orElse(null);
    }

    /**
     * Find earliest shipping date from dates in shipping response.
     *
     * @param response the shipping date response
     * @return earliest shipping date if found
     */
    private Optional<Date> getEarliestShippingDate(final ShippingDateInfoResponseData response) {
        return findShippingDateByReduction(response, BinaryOperator.minBy(Date::compareTo));
    }

    /**
     * Find latest shipping date from dates in shipping response.
     *
     * @param response the shipping date response
     * @return latest shipping date if found
     */
    private Optional<Date> getLatestShippingDate(final ShippingDateInfoResponseData response) {
        return findShippingDateByReduction(response, BinaryOperator.maxBy(Date::compareTo));
    }

    /**
     * Find shipping date from dates in shipping response by reducing with given accumulator.
     *
     * @param response    the shipping date response
     * @param accumulator the accumulator
     * @return date
     */
    private Optional<Date> findShippingDateByReduction(final ShippingDateInfoResponseData response, final BinaryOperator<Date> accumulator) {
        return CollectionUtils.emptyIfNull(response.getResults()).stream()
                .map(ShippingEntryInfoResponseData::getDeliveryDate)
                .filter(Objects::nonNull)
                .reduce(accumulator);
    }

    private boolean allProductsHaveDeliveryDate(final ShippingDateInfoResponseData shippingDateResponse) {
        if (CollectionUtils.isNotEmpty(shippingDateResponse.getResults())) {
            return shippingDateResponse.getResults().stream()
                    .map(ShippingEntryInfoResponseData::getDeliveryDate)
                    .noneMatch(Objects::isNull);
        }
        return false;
    }
}
