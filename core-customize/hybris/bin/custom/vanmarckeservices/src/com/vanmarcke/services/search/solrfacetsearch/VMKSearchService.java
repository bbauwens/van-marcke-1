package com.vanmarcke.services.search.solrfacetsearch;

import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;

/**
 * Custom interface to search a SolrIndex
 */
public interface VMKSearchService {

    /**
     * Gets the {@link SearchResult} for the given {@link SearchQuery}
     *
     * @param searchQuery {@link SearchQuery}
     * @return {@link SearchResult}
     * @throws FacetSearchException
     */
    SearchResult search(SearchQuery searchQuery) throws FacetSearchException;

    /**
     * Creates a {@link SearchQuery} for {@link SolrFacetSearchConfigModel}
     *
     * @param facetSearchConfig {@link FacetSearchConfig}
     * @param indexedType       {@link IndexedType}
     * @return {@link SearchQuery}
     */
    SearchQuery createSearchQuery(FacetSearchConfig facetSearchConfig, IndexedType indexedType);

    /**
     * Creates a {@link SearchQuery} for a search term and {@link SolrFacetSearchConfigModel}
     *
     * @param term              Term to search for.
     * @param facetSearchConfig {@link FacetSearchConfig}
     * @param indexedType       {@link IndexedType}
     * @return {@link SearchQuery}
     */
    SearchQuery createTextSearchQuery(String term, FacetSearchConfig facetSearchConfig, IndexedType indexedType);
}
