package com.vanmarcke.services.b2b.impl;

import com.vanmarcke.services.b2b.VMKSSOUserService;
import com.vanmarcke.services.storelocator.pos.VMKPointOfServiceService;
import com.vanmarcke.services.storesession.VMKStoreSessionService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.StoreFinderService;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Implementation for {@link VMKSSOUserService}
 *
 * @author Niels Raemaekers
 * @since 23-06-2021
 */
public class VMKSSOUserServiceImpl implements VMKSSOUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKSSOUserServiceImpl.class);

    private final StoreFinderService<PointOfServiceDistanceData, StoreFinderSearchPageData<PointOfServiceDistanceData>> storeFinderService;
    private final VMKStoreSessionService storeSessionService;
    private final BaseStoreService baseStoreService;
    private final ModelService modelService;
    private final VMKPointOfServiceService pointOfServiceService;

    /**
     * Constructor for {@link VMKSSOUserServiceImpl}
     *
     * @param storeFinderService    the {@link StoreFinderService}
     * @param storeSessionService   the {@link VMKStoreSessionService}
     * @param baseStoreService      the {@link BaseStoreService}
     * @param modelService          the {@link ModelService}
     * @param pointOfServiceService the {@link PointOfServiceService}
     */
    public VMKSSOUserServiceImpl(StoreFinderService<PointOfServiceDistanceData, StoreFinderSearchPageData<PointOfServiceDistanceData>> storeFinderService, VMKStoreSessionService storeSessionService, BaseStoreService baseStoreService, ModelService modelService, VMKPointOfServiceService pointOfServiceService) {
        this.storeFinderService = storeFinderService;
        this.storeSessionService = storeSessionService;
        this.baseStoreService = baseStoreService;
        this.modelService = modelService;
        this.pointOfServiceService = pointOfServiceService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateUserSessionStoreIfNotPresent(UserModel customer) {
        if (customer instanceof B2BCustomerModel) {

            B2BCustomerModel b2bCustomer = (B2BCustomerModel) customer;
            if (b2bCustomer.getSessionStore() == null) {
                PointOfServiceModel pos = getNearestStore(b2bCustomer);
                b2bCustomer.setSessionStore(pos);
                modelService.save(b2bCustomer);
                if (pos != null) {
                    storeSessionService.setCurrentStore(pos.getName());
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceModel getNearestStore(B2BCustomerModel user) {
        AddressModel address = user.getDefaultB2BUnit().getBillingAddress();
        BaseStoreModel store = getBaseStore(address);
        return getNearestStore(user, store);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PointOfServiceModel getNearestStore(B2BCustomerModel user, BaseStoreModel baseStore) {
        AddressModel address = user.getDefaultB2BUnit().getBillingAddress();
        BaseStoreModel userLocatedStore = getBaseStore(address);
        PointOfServiceModel nearestStore;
        if (baseStore.equals(userLocatedStore)) {
            nearestStore = findNearestStoreForBaseStore(baseStore, address.getTown());
        } else {
            PointOfServiceModel pos = user.getSessionStore();
            GeoPoint geo = pointOfServiceService.getGeoPoint(pos);
            nearestStore = findNearestStoreForBaseStore(baseStore, geo);
        }

        if (nearestStore == null) {
            nearestStore = getFallbackStore(baseStore, address);
        }

        return nearestStore;
    }

    /**
     * Find the nearest store for the passed baseStore and address
     *
     * @param baseStore   the base store
     * @param queryObject the query object
     * @return the nearest store
     */
    protected PointOfServiceModel findNearestStoreForBaseStore(BaseStoreModel baseStore, Object queryObject) {
        PointOfServiceModel pos = null;

        PageableData pageableData = new PaginationData();
        pageableData.setCurrentPage(0);
        pageableData.setPageSize(5);

        try {
            StoreFinderSearchPageData<PointOfServiceDistanceData> searchPageData;
            if (queryObject instanceof GeoPoint) {
                GeoPoint query = (GeoPoint) queryObject;
                searchPageData = storeFinderService.positionSearch(baseStore, query, pageableData);
            } else {
                searchPageData = storeFinderService.locationSearch(baseStore, queryObject.toString(), pageableData);
            }

            if (CollectionUtils.isNotEmpty(searchPageData.getResults())) {
                PointOfServiceDistanceData posData = searchPageData.getResults().get(0);
                if (posData != null) {
                    pos = posData.getPointOfService();
                }
            }
        } catch (Exception e) {
            LOGGER.error("Unable to get the nearest point of service", e);
        }

        return pos;
    }

    /**
     * Get first store as fallback from the baseStore
     *
     * @param baseStore the base store
     * @param address the address
     * @return the first store in the list of the stores of the address country and base store
     */
    protected PointOfServiceModel getFallbackStore(BaseStoreModel baseStore, AddressModel address) {
        List<PointOfServiceModel> allStores = storeFinderService.getAllPosForCountry(address.getCountry().getIsocode(), baseStore);
        if (!CollectionUtils.isEmpty(allStores)) {
            return allStores.get(0);
        }

        return null;
    }

    /**
     * Returns the base store for the given {@code address}.
     *
     * @param address the address
     * @return the base store
     */
    private BaseStoreModel getBaseStore(AddressModel address) {
        if (address != null && address.getCountry() != null) {
            return baseStoreService.getBaseStoreForUid("blue-" + address.getCountry().getIsocode().toLowerCase());
        }
        return null;
    }
}
