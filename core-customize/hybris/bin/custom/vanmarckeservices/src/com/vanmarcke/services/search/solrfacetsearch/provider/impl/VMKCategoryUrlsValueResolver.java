package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

/**
 * Value resolver for category urls.
 *
 * @author Joris Cryns
 * @since 15-7-2019
 */
public class VMKCategoryUrlsValueResolver extends AbstractValueResolver<CategoryModel, Object, Object> {

    private UrlResolver<CategoryModel> urlResolver;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument document,
                                  IndexerBatchContext context,
                                  IndexedProperty indexedProperty,
                                  CategoryModel categoryModel,
                                  ValueResolverContext<Object, Object> valueResolverContext) throws FieldValueProviderException {

        String url = urlResolver.resolve(categoryModel);
        if (StringUtils.isNotBlank(url)) {
            document.addField(indexedProperty, url, valueResolverContext.getFieldQualifier());
        }
    }

    @Required
    public void setUrlResolver(UrlResolver<CategoryModel> urlResolver) {
        this.urlResolver = urlResolver;
    }
}
