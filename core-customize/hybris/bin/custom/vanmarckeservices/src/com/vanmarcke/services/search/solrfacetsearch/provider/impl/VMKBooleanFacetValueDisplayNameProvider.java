package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.elision.hybris.l10n.services.LocalizedMessageService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang3.BooleanUtils;

/**
 * Provider to show correct boolean translations for Solr results
 */
public class VMKBooleanFacetValueDisplayNameProvider extends AbstractFacetValueDisplayNameProvider {

    private static final String KEY_TRUE = "facet.boolean.true";
    private static final String KEY_FALSE = "facet.boolean.false";
    private final LocalizedMessageService localizedMessageService;

    /**
     * Constructor for VMKBooleanFacetValueDisplayNameProvider
     *
     * @param localizedMessageService {@link LocalizedMessageService}
     */
    public VMKBooleanFacetValueDisplayNameProvider(LocalizedMessageService localizedMessageService) {
        this.localizedMessageService = localizedMessageService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName(SearchQuery searchQuery, IndexedProperty indexedProperty, String originalValue) {
        return BooleanUtils.toBoolean(originalValue) ? localizedMessageService.getCachedLocalizedMessageForKey(KEY_TRUE).getMessage() : localizedMessageService.getCachedLocalizedMessageForKey(KEY_FALSE).getMessage();
    }
}