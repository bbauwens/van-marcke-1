package com.vanmarcke.services.order.hook.impl;

import com.vanmarcke.services.address.VMKPreferredPaymentAddressLookupStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;

/**
 * A hook that adds the payment address to the cart if they are not present
 */
public class VMKPreferredPaymentAddressCartCalculationMethodHookImpl extends AbstractVMKCartCalculationMethodHook {

    private final VMKPreferredPaymentAddressLookupStrategy bluePreferredPaymentAddressLookupStrategy;

    public VMKPreferredPaymentAddressCartCalculationMethodHookImpl(final ModelService modelService, final VMKPreferredPaymentAddressLookupStrategy bluePreferredPaymentAddressLookupStrategy) {
        super(modelService);
        this.bluePreferredPaymentAddressLookupStrategy = bluePreferredPaymentAddressLookupStrategy;
    }

    @Override
    public void afterCalculate(final CommerceCartParameter parameter) {
        // No need to do anything after calculating
    }

    @Override
    public void beforeCalculate(final CommerceCartParameter parameter) {
        final CartModel cart = parameter.getCart();
        if (isB2BCart(cart) && cart.getPaymentAddress() == null) {
            final AddressModel paymentAddress = this.bluePreferredPaymentAddressLookupStrategy.getPreferredPaymentAddressForOrder(cart);
            if (paymentAddress != null) {
                cart.setPaymentAddress(paymentAddress);
                getModelService().save(cart);
            }
        }
    }
}