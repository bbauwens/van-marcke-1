package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import com.vanmarcke.services.search.solrfacetsearch.provider.VMKModelUrlResolver;
import com.vanmarcke.services.util.URIUtils;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Resolver for product URLs
 */
public class VMKProductModelUrlResolver extends DefaultProductModelUrlResolver implements VMKModelUrlResolver<ProductModel> {

    @Override
    protected String resolveInternal(final ProductModel source) {
        return URIUtils.normalize(super.resolveInternal(source));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<CategoryModel> getCategoryPath(final CategoryModel category) {
        final List<CategoryModel> categoryPath = super.getCategoryPath(category);
        return categoryPath.stream()
                .filter(e -> isNotBlank(e.getName()))
                .collect(Collectors.toList());
    }

    /**
     * Overloaded method that resolve the URL depending on a given locale
     *
     * @param source product
     * @param locale locale
     * @return URL that contains localized categories hierarchy and product name
     * e.g /parent-category-name/child-category-name/product-name/p/product-code
     */
    @Override
    public String resolveInternal(final ProductModel source, Locale locale) {
        final ProductModel baseProduct = getProductAndCategoryHelper().getBaseProduct(source);

        final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

        String url = getPattern();

        if (currentBaseSite != null && url.contains("{baseSite-uid}")) {
            url = url.replace("{baseSite-uid}", urlEncode(currentBaseSite.getUid()));
        }
        if (url.contains("{category-path}")) {
            url = url.replace("{category-path}", buildPathString(getCategoryPath(baseProduct), locale));
        }

        if (url.contains("{product-name}")) {
            url = url.replace("{product-name}", urlSafe(baseProduct.getName(locale)));
        }
        if (url.contains("{product-code}")) {
            url = url.replace("{product-code}", urlEncode(source.getCode()));
        }

        return url;
    }

    /**
     * Overloaded method that creates localized path
     *
     * @param path   categories hierarchy
     * @param locale locale
     * @return localized categories hierarchy
     */
    private String buildPathString(final List<CategoryModel> path, Locale locale) {
        if (path == null || path.isEmpty()) {
            return "c"; // Default category part of path when missing category
        }

        final StringBuilder result = new StringBuilder();

        for (int i = 0; i < path.size(); i++) {
            if (i != 0) {
                result.append('/');
            }
            result.append(urlSafe(path.get(i).getName(locale)));
        }

        return result.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canResolve(ItemModel item) {
        return item instanceof ProductModel;
    }
}