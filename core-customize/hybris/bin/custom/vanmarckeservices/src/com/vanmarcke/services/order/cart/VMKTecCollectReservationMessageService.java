package com.vanmarcke.services.order.cart;

import de.hybris.platform.core.model.order.CartModel;

import java.util.Date;

/**
 * Defines methods which generate a Tec collect reservation message to be shown on the storefront in certain cases.
 *
 * @author Tom van den Berg
 * @since 30-12-2020
 */
public interface VMKTecCollectReservationMessageService {

    /**
     * This message informs the customer in the specific case where TEC collect is possible
     * but the difference with the pickup date is larger than 1 day
     *
     * <p>
     * More specific :
     * <p>
     * Het mandje is beschikbaar in TEC collect (dus AS400 geeft vandaag als antwoord):
     * <p>
     * T93 call (pickupdatum) geeft datum vandaag + 1 terug : geen melding
     * <p>
     * T93 call geeft datum vandaag + 2 of verder terug : wel een melding
     * <p>
     * Het mandje is niet beschikbaar in TEC collect : geen melding
     * <p>
     * e.g.: Als u uw bestelling op de huidige dag plaatst zal deze gedurende
     * 2 dagen in uw geselecteerde Technics af te halen zijn."
     *
     * @param cart               the cart
     * @param pickupDate         the pickup date
     * @param tecCollectPossible whether tec collect is possible
     * @return the info message
     */
    String generateTecCollectReservationMessage(CartModel cart, Date pickupDate, boolean tecCollectPossible);
}
