package com.vanmarcke.services.address;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;

/**
 * Interface for Payment Addresses Lookup Strategy
 */
@FunctionalInterface
public interface VMKPaymentAddressesLookupStrategy {

    /**
     * Get all the possible payment addresses for the current order
     *
     * @param abstractOrder the current order
     * @return
     */
    List<AddressModel> getPaymentAddressesForOrder(AbstractOrderModel abstractOrder);
}
