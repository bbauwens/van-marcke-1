package com.vanmarcke.services.search.solrfacetsearch.comparators.impl;

import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.solrfacetsearch.config.impl.DefaultFacetSortProvider;
import de.hybris.platform.solrfacetsearch.search.FacetValue;

/**
 * Facet values comparator for {@code boolean} type facet.
 * <p>
 * To be used as comparator reference in bean instance of {@link DefaultFacetSortProvider}.
 * Specify {@link DefaultFacetSortProvider#setDescending(boolean)} property to control sort order.
 *
 * @author Przemyslaw Mirowski
 * @since 25-10-2021
 */
public class VMKBooleanFacetComparator extends AbstractComparator<FacetValue> {
    @Override
    protected int compareInstances(final FacetValue facetValue1, final FacetValue facetValue2) {
        return Boolean.compare(Boolean.parseBoolean(facetValue1.getName()), Boolean.parseBoolean(facetValue2.getName()));
    }
}
