package com.vanmarcke.services.constants;

/**
 * This class lists the VIT Calculated Status codes.
 * <p>
 * These codes returned from the VIT reflect the product status.
 *
 * @since 3-3-2020
 * @author Tom van den Berg
 */
public final class VITCalculatedStatusConstants {

    public static final String SELLING_OFF = "A";
    public static final String NOT_AVAILABLE = "0";

    /**
     * Hides the default implicit constructor.
     */
    private VITCalculatedStatusConstants() {}
}
