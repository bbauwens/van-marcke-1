package com.vanmarcke.services.impersonation;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.impersonation.impl.DefaultImpersonationService;
import de.hybris.platform.europe1.enums.UserPriceGroup;

import static de.hybris.platform.europe1.constants.Europe1Constants.PARAMS.UPG;

public class VMKImpersonationServiceImpl extends DefaultImpersonationService implements ImpersonationService {

    @Override
    protected void configureSession(final ImpersonationContext context) {
        super.configureSession(context);

        final UserPriceGroup userPriceGroup = determineSessionPriceList(context);
        if (userPriceGroup != null) {
            getSessionService().setAttribute(UPG, userPriceGroup);
        }
    }

    protected UserPriceGroup determineSessionPriceList(final ImpersonationContext context) {
        if (context.getSite() instanceof CMSSiteModel) {
            final CMSSiteModel site = (CMSSiteModel) context.getSite();
            return site.getCommerceGroup() != null ? site.getCommerceGroup().getUserPriceGroup() : null;
        }
        return null;
    }
}