package com.vanmarcke.services.delivery;

import java.util.Date;

/**
 * This interface defines a method to calculate first possible delivery date equal or past a given first possible date
 *
 * @author Cristi Stoica
 * @since 29-09-2021
 */
public interface VMKFirstDateAvailabilityService {

    /**
     * Calculate and return the first possible delivery date equal or past a given first possible date
     *
     * @param firstPossibleShippingDate the attempted first possible date
     * @return the first possible date equal or past the attempted date
     */
    Date getFirstValidNoClosingDate(Date firstPossibleShippingDate);
}
