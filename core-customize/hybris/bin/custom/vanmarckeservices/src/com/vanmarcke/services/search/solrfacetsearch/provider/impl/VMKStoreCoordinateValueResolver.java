package com.vanmarcke.services.search.solrfacetsearch.provider.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.springframework.beans.factory.annotation.Required;

/**
 * Custom resolver to always store a value, even when lat- and longitude are empty
 */
public class VMKStoreCoordinateValueResolver extends AbstractValueResolver<PointOfServiceModel, Object, Object> {

    private ModelService modelService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addFieldValues(InputDocument document, IndexerBatchContext batchContext,
                                  IndexedProperty indexedProperty, PointOfServiceModel model,
                                  AbstractValueResolver.ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException {
        String name = indexedProperty.getName();

        Double value = modelService.getAttributeValue(model, name);

        super.filterAndAddFieldValues(document, batchContext, indexedProperty, value == null ? 0D : value, resolverContext.getFieldQualifier());
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}
