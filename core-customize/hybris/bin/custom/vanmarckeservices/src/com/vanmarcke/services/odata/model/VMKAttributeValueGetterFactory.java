package com.vanmarcke.services.odata.model;

import de.hybris.platform.integrationservices.model.AttributeValueGetter;
import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.integrationservices.model.impl.ClassificationTypeAttributeDescriptor;
import de.hybris.platform.integrationservices.model.impl.DefaultAttributeValueGetterFactory;

public class VMKAttributeValueGetterFactory extends DefaultAttributeValueGetterFactory {

    @Override
    public AttributeValueGetter create(final TypeAttributeDescriptor attributeDescriptor) {
        if (!(attributeDescriptor instanceof ClassificationTypeAttributeDescriptor) && attributeDescriptor != null) {
            return new VMKAttributeValueGetter(super.create(attributeDescriptor), attributeDescriptor, getModelService());
        } else {
            return super.create(attributeDescriptor);
        }
    }
}
