package com.vanmarcke.services.forms.delivery;

/**
 * Form for delivery method
 *
 * @author Niels Raemaekers
 * @since 09-08-2019
 */
public class DeliveryMethodForm {

    private static final String SHIPPING = "DELIVERY";
    private static final String PICK_UP = "PICKUP";

    private String deliveryMethod;
    private String name;
    private String address;
    private String postalCode;
    private String town;
    private String telephoneNumber;
    private String tecUid;
    private String yardReference;
    private String purchaseOrderNumber;
    private String deliveryComment;
    private String deliveryOption;
    private String selectedShippingDate;
    private String selectedPickupDate;
    private Boolean openOnSaturday;
    private Boolean requestSeparateInvoice;
    private String productEntriesDates;

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(final String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(final String town) {
        this.town = town;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(final String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTecUid() {
        return tecUid;
    }

    public void setTecUid(final String tecUid) {
        this.tecUid = tecUid;
    }

    public String getYardReference() {
        return yardReference;
    }

    public void setYardReference(String yardReference) {
        this.yardReference = yardReference;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getDeliveryComment() {
        return deliveryComment;
    }

    public void setDeliveryComment(final String deliveryComment) {
        this.deliveryComment = deliveryComment;
    }

    public String getDeliveryOption() {
        return deliveryOption;
    }

    public void setDeliveryOption(final String deliveryOption) {
        this.deliveryOption = deliveryOption;
    }

    public String getSelectedShippingDate() {
        return selectedShippingDate;
    }

    public void setSelectedShippingDate(String selectedShippingDate) {
        this.selectedShippingDate = selectedShippingDate;
    }

    public String getSelectedPickupDate() {
        return selectedPickupDate;
    }

    public void setSelectedPickupDate(String selectedPickupDate) {
        this.selectedPickupDate = selectedPickupDate;
    }

    public Boolean getOpenOnSaturday() {
        return openOnSaturday;
    }

    public void setOpenOnSaturday(Boolean openOnSaturday) {
        this.openOnSaturday = openOnSaturday;
    }

    public Boolean getRequestSeparateInvoice() {
        return requestSeparateInvoice;
    }

    public void setRequestSeparateInvoice(Boolean requestSeparateInvoice) {
        this.requestSeparateInvoice = requestSeparateInvoice;
    }

    public String getProductEntriesDates() {
        return productEntriesDates;
    }

    public void setProductEntriesDates(String productEntriesDates) {
        this.productEntriesDates = productEntriesDates;
    }

    /**
     * Determine the actual delivery date based on the chosen delivery method.
     *
     * @return the actual delivery date
     */
    public String getActualDate() {
        String result = null;

        if (SHIPPING.equals(deliveryMethod)) {
            result = selectedShippingDate;
        }
        if (PICK_UP.equals(deliveryMethod)) {
            result = selectedPickupDate;
        }
        return result;
    }
}
