package com.vanmarcke.services.order.cart.impl;

import de.hybris.platform.commerceservices.strategies.impl.CartValidationWithoutCartAlteringStrategy;
import de.hybris.platform.core.model.order.CartEntryModel;

/**
 * Custom implementation of {@link CartValidationWithoutCartAlteringStrategy}.
 * <p>
 * Stock level check will be always successful, as it is already performed when setting requested delivery data.
 *
 * @author Przemyslaw Mirowski
 * @since 27-08-2021
 */
public class VMKCartValidationWithoutCartAlteringStrategyImpl extends CartValidationWithoutCartAlteringStrategy {
    @Override
    protected Long getStockLevel(CartEntryModel cartEntryModel) {
        return Long.MAX_VALUE;
    }
}
