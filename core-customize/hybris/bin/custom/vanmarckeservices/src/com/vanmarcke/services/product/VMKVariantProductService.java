package com.vanmarcke.services.product;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

/**
 * This interface defines the custom business logic for the {@link VariantProductModel} instance
 *
 * @author Tom van den Berg
 * @since 11-3-2020
 */
public interface VMKVariantProductService {

    /**
     * Checks whether the given {@code VariantProduct} is purchasable.
     *
     * @param product the variant product model
     * @return boolean true if the variant product is purchasable
     */
    boolean isPurchasable(VariantProductModel product);

    /**
     * Checks whether the given {@code VariantProduct} is purchasable given the stock settings.
     * <p>
     * If the given variant is a No Stock product or a XDock product, it should not be purchasable.
     *
     * @param product the variant product model
     * @return boolean true if the variant product is purchasable given the stock settings
     */
    boolean isPotentiallyInStock(VariantProductModel product);


    /**
     * Checks whether the given {@code VariantProduct} is selling off given the stock settings.
     *
     * @param product the variant product model
     * @return boolean true if the variant product is selling off given the stock settings
     */
    boolean isSellingOffAlmostNOS(VariantProductModel product);

    /**
     * Checks whether the given {@code VariantProduct} is NO stock product and not purchasable.
     *
     * @param product the variant product model
     * @return boolean true if the variant product is NO stock product and not purchasable
     */
    boolean isNOSProduct(VariantProductModel product);

    /**
     * Checks whether the given {@code VariantProduct} is a discontinued product
     *
     * @param product the variant product model
     * @return boolean true if the variant product is on limited stock
     */
    boolean isDiscontinuedProduct(VariantProductModel product);

    /**
     * Checks whether the given {@code VariantProduct} is on limited stock and 0 EDC stock therefore not purchasable
     *
     * @param product the variant product model
     * @return boolean true if the variant product is on limited stock therefore NOT purchasable
     */
    boolean isDiscontinuedOutOfStockProduct(VariantProductModel product);


        /**
         * Check whether the given {@code VariantProduct} is not yet in stock
         *
         * @param product the variant product model
         * @return boolean true if the variant product is soon in stock, therefore not currently purchasable
         */
    boolean isSoonInStockProduct(VariantProductModel product);

    /**
     * Get the EDC stoc
     * @param product The product
     * @return the EDC stock
     */
    long getAvailableEDCStock(ProductModel product);
}