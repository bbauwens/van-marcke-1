package com.vanmarcke.services.event;

import com.vanmarcke.core.model.CustomerInteractionModel;
import com.vanmarcke.core.model.CustomerInteractionProcessModel;
import com.vanmarcke.core.model.OrderCustomerInteractionModel;
import com.vanmarcke.services.event.events.SubmitCustomerInteractionEvent;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.springframework.beans.factory.annotation.Required;

import static com.google.common.base.Preconditions.checkState;

/**
 * Listener for the SubmitCustomerInteractionEvent
 */
public class SubmitCustomerInteractionEventListener extends AbstractAcceleratorSiteEventListener<SubmitCustomerInteractionEvent> {

    private ModelService modelService;
    private BusinessProcessService businessProcessService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onSiteEvent(final SubmitCustomerInteractionEvent event) {
        final CustomerInteractionModel customerInteraction = event.getCustomerInteraction();
        checkState(customerInteraction instanceof OrderCustomerInteractionModel, "event.customerInteraction must be of type OrderCustomerInteractionModel");

        final OrderCustomerInteractionModel orderCustomerInteraction = (OrderCustomerInteractionModel) customerInteraction;
        final OrderModel order = orderCustomerInteraction.getOrder();
        ServicesUtil.validateParameterNotNullStandardMessage("event.customerInteraction.order", order);
        final CustomerInteractionProcessModel customerInteractionProcess = createOrderCustomerInteractionProcess(order);
        customerInteractionProcess.setCustomerInteraction(customerInteraction);
        getModelService().save(customerInteractionProcess);
        getBusinessProcessService().startProcess(customerInteractionProcess);
    }

    /**
     * Create the Customer Interaction Process Model
     *
     * @return the created CustomerInteractionProcessModel
     */
    protected CustomerInteractionProcessModel createOrderCustomerInteractionProcess(final OrderModel order) {
        return getBusinessProcessService().createProcess("blue-customer-interaction-process - " + order.getCode() + "-" + System.currentTimeMillis(), "blue-customer-interaction-process");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SiteChannel getSiteChannelForEvent(final SubmitCustomerInteractionEvent event) {
        final CustomerInteractionModel customerInteraction = event.getCustomerInteraction();

        checkState(customerInteraction instanceof OrderCustomerInteractionModel, "event.customerInteraction must be of type OrderCustomerInteractionModel");

        final OrderCustomerInteractionModel orderCustomerInteraction = (OrderCustomerInteractionModel) customerInteraction;

        final OrderModel order = orderCustomerInteraction.getOrder();
        ServicesUtil.validateParameterNotNullStandardMessage("event.customerInteraction.order", order);

        final BaseSiteModel site = orderCustomerInteraction.getSite();
        ServicesUtil.validateParameterNotNullStandardMessage("event.customerInteraction.site", site);

        return site.getChannel();
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    @Required
    public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }
}