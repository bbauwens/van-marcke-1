package com.vanmarcke.services.action;

import be.elision.mandrillextension.service.MandrillService;
import com.vanmarcke.email.converters.impl.MandrillTransportInformationConverter;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.commons.lang3.LocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;

import static de.hybris.platform.core.enums.OrderStatus.TRANSPORT_INFORMATION_NOT_SENT;
import static de.hybris.platform.core.enums.OrderStatus.TRANSPORT_INFORMATION_SENT;
import static org.apache.commons.lang.StringUtils.defaultIfEmpty;
import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.apache.commons.lang3.BooleanUtils.isFalse;

/**
 * Action which will trigger the event for sending a transport information email
 *
 * @author Tom van den Berg
 * @since 15-9-2020
 */
public class VMKSendTransportInformationEmailAction extends AbstractSimpleDecisionAction<OrderProcessModel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMKSendTransportInformationEmailAction.class);

    private MandrillService mandrillService;
    private MandrillTransportInformationConverter mandrillTransportInformationConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public Transition executeAction(OrderProcessModel process) {
        OrderModel order = process.getOrder();

        if (isFalse(order.getDeliveryMode() instanceof ZoneDeliveryModeModel) || isEmpty(order.getDeliveryComment())) {
            return Transition.OK; //if pickup, continue process
        }

        try {
            Locale locale = getLocale(order);
            String templateName = order.getStore().getTransportInformationEmailTemplate(locale);

            mandrillService.send(templateName, order, mandrillTransportInformationConverter, getOrderManagerEmail(order));

            setOrderStatus(order, TRANSPORT_INFORMATION_SENT);
            return Transition.OK;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        setOrderStatus(order, TRANSPORT_INFORMATION_NOT_SENT);
        return Transition.NOK;
    }

    /**
     * Returns the locale for the given {@code order}.
     *
     * @param order the order
     * @return the locale
     */
    private Locale getLocale(OrderModel order) {
        String localeString = "en";
        LanguageModel language = order.getLanguage();
        if (language != null) {
            localeString = language.getIsocode().substring(0, 2);
        }
        return LocaleUtils.toLocale(localeString);
    }

    /**
     * Retrieves the order manager email.
     *
     * @param order the order
     * @return the order manager email.
     */
    private String getOrderManagerEmail(OrderModel order) {
        String orderManagerEmail = null;
        if (order.getContactPerson() != null && order.getStore() != null) {
            orderManagerEmail = defaultIfEmpty(order.getContactPerson().getEmail(), order.getStore().getOrderManagerEmail());
        }
        return orderManagerEmail;
    }

    @Required
    public void setMandrillService(MandrillService mandrillService) {
        this.mandrillService = mandrillService;
    }

    @Required
    public void setMandrillTransportInformationConverter(MandrillTransportInformationConverter mandrillTransportInformationConverter) {
        this.mandrillTransportInformationConverter = mandrillTransportInformationConverter;
    }
}