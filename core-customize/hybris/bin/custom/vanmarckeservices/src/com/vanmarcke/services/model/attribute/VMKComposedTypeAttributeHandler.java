package com.vanmarcke.services.model.attribute;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import de.hybris.platform.servicelayer.type.TypeService;

/**
 * Handler for the dynamic attribute "composedType" on {@link ItemModel}
 *
 * @author Przemyslaw Mirowski
 * @since 6-10-2021
 */
public class VMKComposedTypeAttributeHandler extends AbstractDynamicAttributeHandler<ComposedTypeModel, ItemModel> {

    private final TypeService typeService;

    /**
     * Creates a new instance of {@link VMKComposedTypeAttributeHandler}
     *
     * @param typeService the type service
     */
    public VMKComposedTypeAttributeHandler(TypeService typeService) {
        this.typeService = typeService;
    }

    @Override
    public ComposedTypeModel get(final ItemModel item) {
        return typeService.getComposedTypeForCode(item.getItemtype());
    }
}
