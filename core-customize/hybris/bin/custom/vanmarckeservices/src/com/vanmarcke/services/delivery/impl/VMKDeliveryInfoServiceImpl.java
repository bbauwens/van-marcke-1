package com.vanmarcke.services.delivery.impl;

import com.vanmarcke.cpi.data.order.PickupDateInfoResponseData;
import com.vanmarcke.cpi.data.order.PickupDateRequestData;
import com.vanmarcke.cpi.data.order.PickupDateResponseData;
import com.vanmarcke.cpi.data.order.ShippingDateInfoResponseData;
import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateResponseData;
import com.vanmarcke.cpi.services.ESBPickupDateService;
import com.vanmarcke.cpi.services.ESBShippingDateService;
import com.vanmarcke.services.delivery.VMKDeliveryInfoService;
import com.vanmarcke.services.request.VMKYRequestKey;
import com.vanmarcke.services.request.VMKYRequestService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Optional;


/**
 * Provides methods for retrieving shipping and pickup information.
 *
 * @author Tom van den Berg
 * @since 14-01-2021
 */
public class VMKDeliveryInfoServiceImpl implements VMKDeliveryInfoService {
    private final ESBPickupDateService esbPickupDateService;
    private final ESBShippingDateService esbShippingDateService;
    private final VMKYRequestService yRequestService;
    private final Converter<AbstractOrderModel, PickupDateRequestData> pickupDateRequestConverter;
    private final Converter<AbstractOrderModel, ShippingDateRequestData> shippingDateRequestConverter;

    /**
     * Provides an instance of the VMKDeliveryInfoServiceImpl.
     *
     * @param esbPickupDateService         the ESB pickup date service
     * @param esbShippingDateService       the esb shipping date service
     * @param yRequestService              the yRequest service
     * @param pickupDateRequestConverter   the pickup date request converter
     * @param shippingDateRequestConverter the shipping date request converter
     */
    public VMKDeliveryInfoServiceImpl(ESBPickupDateService esbPickupDateService,
                                      ESBShippingDateService esbShippingDateService,
                                      VMKYRequestService yRequestService, Converter<AbstractOrderModel, PickupDateRequestData> pickupDateRequestConverter,
                                      Converter<AbstractOrderModel, ShippingDateRequestData> shippingDateRequestConverter) {
        this.esbPickupDateService = esbPickupDateService;
        this.esbShippingDateService = esbShippingDateService;
        this.yRequestService = yRequestService;
        this.pickupDateRequestConverter = pickupDateRequestConverter;
        this.shippingDateRequestConverter = shippingDateRequestConverter;
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public Optional<PickupDateInfoResponseData> getPickupDateInformation(final CartModel cart) {
        return yRequestService.computeIfAbsent(VMKYRequestKey.pickupDateInformationKey(cart), key -> {
            final PickupDateRequestData request = pickupDateRequestConverter.convert(cart);
            return Optional.ofNullable(esbPickupDateService.getPickupDateInformation(request))
                    .map(PickupDateResponseData::getData);
        });
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public Optional<ShippingDateInfoResponseData> getShippingDateInformation(final CartModel cart) {
        return yRequestService.computeIfAbsent(VMKYRequestKey.shippingDateInformationKey(cart), key -> {
            final ShippingDateRequestData request = shippingDateRequestConverter.convert(cart);

            return Optional.ofNullable(esbShippingDateService.getShippingDateInformation(request))
                    .map(ShippingDateResponseData::getData);
        });
    }
}
