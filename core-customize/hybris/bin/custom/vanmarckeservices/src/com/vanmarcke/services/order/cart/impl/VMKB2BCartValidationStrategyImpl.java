package com.vanmarcke.services.order.cart.impl;

import com.vanmarcke.services.factories.VMKCartValidationStrategyFactory;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bacceleratorservices.strategies.impl.DefaultB2BCartValidationStrategy;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Custom implementation for DefaultB2BCartValidationStrategy
 */
public class VMKB2BCartValidationStrategyImpl extends DefaultB2BCartValidationStrategy implements VMKCartValidationStrategyFactory {

    @Override
    protected Long getStockLevel(final CartEntryModel cartEntryModel) {
        return Long.MAX_VALUE;
    }

    @Override
    protected void validateDelivery(final CartModel cartModel) {

        if (cartModel.getDeliveryAddress() != null) {
            if (!isB2BCart(cartModel) || !cartModel.getUnit().equals(cartModel.getDeliveryAddress().getOwner())) {
                cartModel.setDeliveryAddress(null);
                getModelService().save(cartModel);
            }
        }
    }

    /**
     * Checks if the Cart is meant for B2B or not
     *
     * @param cart
     * @return
     */
    protected boolean isB2BCart(final CartModel cart) {
        return cart.getUser() instanceof B2BCustomerModel;
    }
}