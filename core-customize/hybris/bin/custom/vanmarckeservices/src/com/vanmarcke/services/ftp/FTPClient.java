package com.vanmarcke.services.ftp;

import java.io.IOException;
import java.io.InputStream;

import static org.apache.commons.net.ftp.FTPReply.isPositiveCompletion;

/**
 * Client for sending data to FTP
 */
public class FTPClient {

    private static final String PATH_SEPARATOR = "/";

    private static final int DEFAULT_PORT = 21;

    private final String host;
    private final int port;
    private final String user;
    private final String password;
    private final String remotePath;

    private org.apache.commons.net.ftp.FTPClient ftp;

    /**
     * Constructor to configure FTP client
     *
     * @param host       the host
     * @param port       the port
     * @param user       the user
     * @param password   the password
     * @param remotePath the remote path
     */
    public FTPClient(String host, int port, String user, String password, String remotePath) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.remotePath = remotePath;
    }

    /**
     * Constructor to configure FTP client
     *
     * @param host       the host
     * @param user       the user
     * @param password   the password
     * @param remotePath the remote path
     */
    public FTPClient(String host, String user, String password, String remotePath) {
        this(host, DEFAULT_PORT, user, password, remotePath);
    }

    /**
     * Sends a file on the server using the given name and taking input from the given InputStream.
     *
     * @param inputStream the inputStream
     * @param fileName    the file name
     */
    public void send(InputStream inputStream, String fileName) throws IOException {
        try {
            connect();
            ftp.storeFile(remotePath + PATH_SEPARATOR + fileName, inputStream);
        } finally {
            disconnect();
        }
    }

    /**
     * Establishes a connection with the FTP server.
     *
     * @throws IOException if something went wrong
     */
    protected void connect() throws IOException {
        ftp = getFTPClient();

        ftp.connect(host, port);

        int reply = ftp.getReplyCode();
        if (!isPositiveCompletion(reply)) {
            throw new IOException(String.format("Exception in connecting to FTP Server '%s:%d'", host, port));
        }

        ftp.login(user, password);

        ftp.enterLocalPassiveMode();
    }

    protected org.apache.commons.net.ftp.FTPClient getFTPClient() {
        return new org.apache.commons.net.ftp.FTPClient();
    }

    /**
     * Closes the connection to the FTP server.
     *
     * @throws IOException if something went wrong
     */
    protected void disconnect() throws IOException {
        if (ftp != null) {
            ftp.disconnect();
        }
    }

}