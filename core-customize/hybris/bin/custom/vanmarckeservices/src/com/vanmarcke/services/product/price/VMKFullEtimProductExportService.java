package com.vanmarcke.services.product.price;

import com.vanmarcke.facades.data.ProductExportFeedResult;
import de.hybris.platform.catalog.model.CatalogVersionModel;

public interface VMKFullEtimProductExportService {

    /**
     *
     * This method gets the products having modifiedTime > fromDate,
     *             fetches all the classification data for these products
     *             and export classification data in CSV format.
     *
     * @param catalogVersion the catalogVersion
     * @return Instance of ProductExportFeedResult holding the exported data.
     */
    ProductExportFeedResult createFullEtimProductExportLines (final CatalogVersionModel catalogVersion);
}
