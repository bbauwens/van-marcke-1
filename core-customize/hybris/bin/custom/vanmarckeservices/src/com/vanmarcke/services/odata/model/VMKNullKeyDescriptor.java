package com.vanmarcke.services.odata.model;

import de.hybris.platform.integrationservices.integrationkey.KeyValue;
import de.hybris.platform.integrationservices.model.KeyAttribute;
import de.hybris.platform.integrationservices.model.KeyDescriptor;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class VMKNullKeyDescriptor implements KeyDescriptor {
    private static final KeyValue EMPTY_KEY_VALUE = new KeyValue();
    private static final List<KeyAttribute> KEY_ATTRIBUTES = Collections.emptyList();

    @Override
    public KeyValue calculateKey(final Map<String, Object> item) {
        return EMPTY_KEY_VALUE;
    }

    @Override
    public boolean isKeyAttribute(final String attr) {
        return false;
    }

    @Override
    public List<KeyAttribute> getKeyAttributes() {
        return KEY_ATTRIBUTES;
    }
}
