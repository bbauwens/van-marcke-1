package com.vanmarcke.services.order.cart;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartFactory;

/**
 * Customer B2B interface for {@link CartFactory}
 */
public interface VMKB2BCartFactory extends CartFactory {

    /**
     * Checks if current {@link CartModel} is from {@link de.hybris.platform.b2b.model.B2BCustomerModel}
     *
     * @param cart {@link CartModel} to check.
     * @return Returns true if {@link CartModel} belongs to a {@link de.hybris.platform.b2b.model.B2BCustomerModel}, otherwise returns false.
     */
    boolean isB2BCart(final CartModel cart);
}
