package com.vanmarcke.services.cache.ehcache;

import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.tenant.TenantService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;

/**
 * This class is copied from the webservicescommonsserver.jar.
 * It exposes an EhCache {@link net.sf.ehcache.CacheManager} instance.
 *
 * @author Tom van den Berg
 * @since 10-06-2020
 */
public class VMKTenantAwareEhCacheManagerFactoryBean extends EhCacheManagerFactoryBean {

    public static final String CACHE_SUFFIX_PROPERTY = "webservicescommons.cacheSuffix";
    private static final String CACHE_PREFIX = "vanMarckeCommonCache_";

    private TenantService tenantService;

    public VMKTenantAwareEhCacheManagerFactoryBean(final TenantService tenantService) {
        this.tenantService = tenantService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() {
        String cacheName = CACHE_PREFIX + tenantService.getCurrentTenantId();
        this.setCacheManagerName(cacheName);
        superAfterPropertiesSet();
    }

    protected void superAfterPropertiesSet() {
        super.afterPropertiesSet();
    }

}