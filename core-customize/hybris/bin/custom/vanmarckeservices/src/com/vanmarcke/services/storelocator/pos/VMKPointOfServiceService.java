package com.vanmarcke.services.storelocator.pos;

import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;

import java.util.List;
import java.util.Map;

/**
 * Custom interface for PointOfService Service
 */
public interface VMKPointOfServiceService extends PointOfServiceService {

    /**
     * Sets the given pointOfService as current.
     *
     * @param pointOfService the pointOfService to load and set as the current pointOfService in the session
     */
    void setCurrentPointOfService(PointOfServiceModel pointOfService);

    /**
     * Returns current pointOfService from the session for current user.
     *
     * @return the current pointOfService
     */
    PointOfServiceModel getCurrentPointOfService();

    /**
     * Returns point of service from cart if present, otherwise current session point of service.
     *
     * @param cart the cart
     * @return selected point of service
     */
    PointOfServiceModel getCurrentPointOfService(CartModel cart);

    /**
     * Find the Point of Service for a given POS id
     *
     * @param storeID the given ID
     * @return the results of the search or {null} if none found
     */
    @Override
    PointOfServiceModel getPointOfServiceForName(String storeID);

    /**
     * find all Point of Services for a given list of IDs
     *
     * @param storeIDs the given list of IDs
     * @return the results of the search or {null} if none found
     */
    List<PointOfServiceModel> getPointOfServicesForStoreIDs(List<String> storeIDs);

    /**
     * Determines whether the given TEC store is open on saturday.
     *
     * @param pointOfService the point of service
     * @return true if open
     */
    boolean isPointOfServiceOpenOnSaturday(PointOfServiceModel pointOfService);

    /**
     * Retrieve the alternative TEC stores for the given cart which are currently open.
     *
     * @param cart the cart
     * @return the TEC stores which are currently open
     */
    List<PointOfServiceModel> getAlternativeTECStoresWhichAreCurrentlyOpenForCart(CartModel cart);

    /**
     * Retrieve the alternative TEC stores for the given cart which are currently open.
     *
     * @param productQuantitiesMap the map with products and their quantities
     * @return the TEC stores which are currently open
     */
    List<PointOfServiceModel> getAlternativeTECStoresWhichAreCurrentlyOpen(Map<String, Long> productQuantitiesMap);

    /**
     * Provides a {@link GeoPoint} for the given point of service.
     *
     * @param source the point of service
     * @return the geopoint
     */
    GeoPoint getGeoPoint(PointOfServiceModel source);

}
