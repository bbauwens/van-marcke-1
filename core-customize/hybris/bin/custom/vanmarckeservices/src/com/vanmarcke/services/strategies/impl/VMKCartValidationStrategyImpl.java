package com.vanmarcke.services.strategies.impl;

import com.vanmarcke.services.factories.VMKCartValidationStrategyFactory;
import de.hybris.platform.commerceservices.strategies.impl.DefaultCartValidationStrategy;

/**
 * Custom implementation for the {@link DefaultCartValidationStrategy} so this can be used in the Factory pattern.
 */
public class VMKCartValidationStrategyImpl extends DefaultCartValidationStrategy implements VMKCartValidationStrategyFactory {
}