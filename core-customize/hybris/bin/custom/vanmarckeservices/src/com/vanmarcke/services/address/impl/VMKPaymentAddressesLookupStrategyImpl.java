package com.vanmarcke.services.address.impl;

import com.google.common.base.Preconditions;
import com.vanmarcke.services.address.VMKPaymentAddressesLookupStrategy;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation for BluePaymentAddressesLookupStrategy
 */
public class VMKPaymentAddressesLookupStrategyImpl implements VMKPaymentAddressesLookupStrategy {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AddressModel> getPaymentAddressesForOrder(final AbstractOrderModel abstractOrder) {

        Preconditions.checkNotNull(abstractOrder.getUnit(), "A B2BUnit must always be set before we can continue with the Checkout!");
        final B2BUnitModel b2BUnit = abstractOrder.getUnit();

        return b2BUnit.getAddresses().stream()
                .filter(AddressModel::getBillingAddress)
                .filter(AddressModel::getVisibleInAddressBook)
                .collect(Collectors.toList());
    }
}
