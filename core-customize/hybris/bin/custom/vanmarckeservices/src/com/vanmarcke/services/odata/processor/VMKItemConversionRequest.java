package com.vanmarcke.services.odata.processor;

import de.hybris.platform.odata2services.odata.persistence.ConversionOptions;
import de.hybris.platform.odata2services.odata.persistence.ItemConversionRequest;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.atteo.evo.inflector.English;

public class VMKItemConversionRequest extends ItemConversionRequest {
    private int conversionLevel;

    public static VMKItemConversionRequestBuilder requestBuilder() {
        return new VMKItemConversionRequestBuilder(new VMKItemConversionRequest());
    }

    @Override
    public int getConversionLevel() {
        return conversionLevel;
    }

    public ItemConversionRequest propertyConversionRequest(final String propertyType, final String propertyName, final Object value) throws EdmException {
        return new VMKItemConversionRequestBuilder(new VMKItemConversionRequest())
                .from(this)
                .withOptions(getOptions().navigate(propertyName))
                .withEntitySet(getEntitySet().getEntityContainer().getEntitySet(English.plural(propertyType)))
                .withValue(value)
                .withConversionLevel(getConversionLevel() + 1)
                .build();
    }

    public static class VMKItemConversionRequestBuilder extends AbstractRequestBuilder<VMKItemConversionRequestBuilder, VMKItemConversionRequest> {
        protected VMKItemConversionRequestBuilder(VMKItemConversionRequest request) {
            super(request);
        }

        public VMKItemConversionRequestBuilder from(final ItemConversionRequest request) {
            return withAcceptLocale(request.getAcceptLocale())
                    .withEntitySet(request.getEntitySet())
                    .withIntegrationObject(request.getIntegrationObjectCode())
                    .withValue(request.getValue())
                    .withOptions(request.getOptions())
                    .withConversionLevel(request.getConversionLevel());
        }

        public VMKItemConversionRequestBuilder withValue(final Object value) {
            request().setValue(value);
            return myself();
        }

        public VMKItemConversionRequestBuilder withOptions(final ConversionOptions.ConversionOptionsBuilder optionsBuilder) {
            if (optionsBuilder != null) {
                return withOptions(optionsBuilder.build());
            }
            return myself();
        }

        public VMKItemConversionRequestBuilder withOptions(final ConversionOptions options) {
            request().setOptions(options);
            return myself();
        }

        public VMKItemConversionRequestBuilder withConversionLevel(final int level) {
            request().conversionLevel = level;
            return myself();
        }
    }
}
