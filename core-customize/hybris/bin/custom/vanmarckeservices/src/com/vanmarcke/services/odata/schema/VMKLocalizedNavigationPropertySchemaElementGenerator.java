package com.vanmarcke.services.odata.schema;

import de.hybris.platform.integrationservices.model.TypeAttributeDescriptor;
import de.hybris.platform.odata2services.odata.NestedAbstractItemTypeCannotBeCreatedException;
import de.hybris.platform.odata2services.odata.UniqueCollectionNotAllowedException;
import de.hybris.platform.odata2services.odata.schema.SchemaElementGenerator;
import de.hybris.platform.odata2services.odata.schema.association.AssociationGenerator;
import de.hybris.platform.odata2services.odata.schema.association.AssociationGeneratorRegistry;
import de.hybris.platform.odata2services.odata.schema.utils.SchemaUtils;
import org.apache.olingo.odata2.api.edm.FullQualifiedName;
import org.apache.olingo.odata2.api.edm.provider.AnnotationAttribute;
import org.apache.olingo.odata2.api.edm.provider.Association;
import org.apache.olingo.odata2.api.edm.provider.NavigationProperty;

import java.util.List;
import java.util.Optional;

public class VMKLocalizedNavigationPropertySchemaElementGenerator implements SchemaElementGenerator<Optional<NavigationProperty>, TypeAttributeDescriptor> { // NavigationPropertySchemaElementGenerator
    private final AssociationGeneratorRegistry associationGeneratorRegistry;
    private final SchemaElementGenerator<List<AnnotationAttribute>, TypeAttributeDescriptor> propertyAnnotationListGenerator;

    public VMKLocalizedNavigationPropertySchemaElementGenerator(AssociationGeneratorRegistry associationGeneratorRegistry, SchemaElementGenerator<List<AnnotationAttribute>, TypeAttributeDescriptor> propertyAnnotationListGenerator) {
        this.associationGeneratorRegistry = associationGeneratorRegistry;
        this.propertyAnnotationListGenerator = propertyAnnotationListGenerator;
    }

    @Override
    public Optional<NavigationProperty> generate(final TypeAttributeDescriptor typeAttributeDescriptor) {
        final Optional<AssociationGenerator> associationGeneratorOptional = associationGeneratorRegistry.getAssociationGenerator(typeAttributeDescriptor);
        return associationGeneratorOptional
                .map(associationGenerator -> generateInternal(associationGenerator, typeAttributeDescriptor));
    }

    private NavigationProperty generateInternal(final AssociationGenerator associationGenerator, final TypeAttributeDescriptor attribute) {
        return attribute != null ? createNavigationProperty(associationGenerator, attribute) : null;
    }

    private NavigationProperty createNavigationProperty(final AssociationGenerator associationGenerator, final TypeAttributeDescriptor attribute) {
        final Association association = associationGenerator.generate(attribute);
        validateNavigationProperty(attribute);
        return new NavigationProperty()
                .setName(attribute.getAttributeName())
                .setRelationship(new FullQualifiedName(SchemaUtils.NAMESPACE, association.getName()))
                .setFromRole(association.getEnd1().getRole())
                .setToRole(association.getEnd2().getRole())
                .setAnnotationAttributes(propertyAnnotationListGenerator.generate(attribute));
    }

    private void validateNavigationProperty(final TypeAttributeDescriptor attributeDescriptor) {
        if (attributeDescriptor.isAutoCreate() && attributeDescriptor.getAttributeType().isAbstract()) {
            throw new NestedAbstractItemTypeCannotBeCreatedException(attributeDescriptor);
        }
        if (attributeDescriptor.isKeyAttribute() && isCollection(attributeDescriptor)) {
            throw new UniqueCollectionNotAllowedException(attributeDescriptor);
        }
    }

    private boolean isCollection(final TypeAttributeDescriptor descriptor) {
        return descriptor.isCollection() || descriptor.isMap();
    }
}
