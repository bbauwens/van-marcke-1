package com.vanmarcke.saferpay.storefront.controllers.integration;

import com.vanmarcke.saferpay.facades.PaymentNotificationFacade;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Resource;

/**
 * Controller to handle the Payment Notifications
 */
@Controller("saferpayPaymentNotificationController")
@RequestMapping(value = "/checkout/payment/saferpay/notify")
public class PaymentNotificationCallbackController extends AbstractCheckoutController {

    @Resource(name = "saferpayPaymentNotificationFacade")
    private PaymentNotificationFacade paymentNotificationFacade;

    @RequestMapping(value = "/{token}", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseStatus(code = HttpStatus.OK)
    public void getNotification(@PathVariable String token) {
        paymentNotificationFacade.createNotificationForCartToken(token);
    }
}