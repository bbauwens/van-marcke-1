package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.PaymentInfoRequestData;
import com.vanmarcke.saferpay.model.SaferpayPaymentInfoModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import org.apache.commons.lang3.StringUtils;

/**
 * The {@link VMKPaymentInformationRequestPopulator} class is used to populate instances of the
 * {@link PaymentInfoRequestData} class with the information of the instances of the
 * {@link PaymentTransactionEntryModel} class.
 *
 * @author Christiaan Janssen
 * @since 05-02-2021
 */
public class VMKPaymentInformationRequestPopulator implements Populator<PaymentTransactionEntryModel, PaymentInfoRequestData> {

    private final EnumerationService enumerationService;

    /**
     * Creates a new instance of the {@link VMKPaymentInformationRequestPopulator} class.
     *
     * @param enumerationService the enumeration service
     */
    public VMKPaymentInformationRequestPopulator(EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(PaymentTransactionEntryModel source, PaymentInfoRequestData target) {
        target.setStatus(source.getType() == null ? null : enumerationService.getEnumerationName(source.getType()));

        PaymentInfoModel paymentInfo = source.getPaymentTransaction().getInfo();
        if (paymentInfo instanceof SaferpayPaymentInfoModel) {
            target.setMethod(((SaferpayPaymentInfoModel) paymentInfo).getType());
            target.setReference(source.getRequestId());
        } else {
            target.setStatus("CAPTURED");
            target.setMethod("INVOICE");
            target.setReference(StringUtils.EMPTY);
        }
    }
}
