package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.ptv.PTVRequestData;
import com.vanmarcke.cpi.data.ptv.PTVResponseData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command which tries to find an address in the PTV-XServer.
 */
@CommandConfig(serviceName = "addressValidationService", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/findAddress")
public class PostFindAddressCommand extends AbstractVMKCPICommand<PTVResponseData> {

    /**
     * Creates a new instance of {@link PostFindAddressCommand} with the specified request.
     *
     * @param request the request
     */
    public PostFindAddressCommand(final PTVRequestData request) {
        setPayLoad(request);
    }
}