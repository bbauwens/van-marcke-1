package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.order.AlternativeTECRequestData;
import com.vanmarcke.cpi.data.order.AlternativeTECResponseData;

/**
 * The {@code ESBAlternativeTECService} interface defines the methods to retrieve alternative TEC stores from the ESB.
 *
 * @author Niels Raemaekers, ChristiaanPl Janssen
 * @since 11-12-2019
 */
public interface ESBAlternativeTECService {

    /**
     * Retrieves the alternative TEC store information from the ESB.
     *
     * @param request the request object
     * @return alternative TEC store information
     */
    AlternativeTECResponseData getAlternativeTECInformation(AlternativeTECRequestData request);
}
