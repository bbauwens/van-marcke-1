package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.order.CreditCheckResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;

/**
 * The {@code ESBCreditCheckService} interface defines the methods to perform a credit check at the ESB.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 23-07-2019
 */
public interface ESBCreditCheckService {

    /**
     * Performs a credit check and returns the response.
     *
     * @param request the request object
     * @return the response object
     */
    CreditCheckResponseData getCreditCheckInformation(OrderRequestData request);
}
