package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetCreditCheckInformationCommand;
import com.vanmarcke.cpi.data.order.CreditCheckResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.services.ESBCreditCheckService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

/**
 * The {@code ESBCreditCheckServiceImpl} class implements the business logic to perform a credit check at the ESB.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 23-07-2019
 */
public class ESBCreditCheckServiceImpl implements ESBCreditCheckService {

    private static final Logger LOG = Logger.getLogger(ESBCreditCheckServiceImpl.class);

    private final CommandExecutor commandExecutor;

    /**
     * Creates a new instance of the {@link ESBCreditCheckServiceImpl} class.
     *
     * @param commandExecutor the command executor
     */
    public ESBCreditCheckServiceImpl(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CreditCheckResponseData getCreditCheckInformation(OrderRequestData request) {
        CreditCheckResponseData payload = null;
        try {
            Response<CreditCheckResponseData> response = commandExecutor.executeCommand(new GetCreditCheckInformationCommand(request), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
            } else {
                LOG.error(MessageFormat.format("Something went wrong while performing a credit check. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while performing a credit check: {0}.", e.getMessage()), e);
        }
        return payload;
    }
}