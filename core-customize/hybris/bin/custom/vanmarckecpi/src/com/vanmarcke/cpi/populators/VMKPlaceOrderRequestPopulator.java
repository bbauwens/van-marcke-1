package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.AddressRequestData;
import com.vanmarcke.cpi.data.order.OrderEntryRequestData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PaymentInfoRequestData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * The {@link VMKPlaceOrderRequestPopulator} class contains is used to populate instances of the
 * {@link OrderRequestData} class with the information of instances of the {@link AbstractOrderModel} class.
 * `
 *
 * @param <SOURCE> the order model
 * @param <TARGET> the outbound order
 * @author Taki Korovessis, Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 06-01-2020
 */
public class VMKPlaceOrderRequestPopulator<SOURCE extends AbstractOrderModel, TARGET extends OrderRequestData> extends AbstractOrderRequestPopulator<SOURCE, TARGET> {

    /**
     * Constructor for {@link VMKPlaceOrderRequestPopulator}
     *
     * @param addressRequestConverter            the address request converter
     * @param orderEntryRequestConverter         the order entry request converter
     * @param paymentInformationRequestConverter the payment information request converter
     */
    public VMKPlaceOrderRequestPopulator(final Converter<AddressModel, AddressRequestData> addressRequestConverter,
                                         final Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter,
                                         final Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter) {
        super(addressRequestConverter, orderEntryRequestConverter, paymentInformationRequestConverter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final SOURCE source, final TARGET target) {
        addCommon(source, target);
        addPurchaseOrderNumber(source, target);
        addDeliveryInformation(source, target);
        addPaymentInformation(source, target);
        addEntries(source, target);
        addTotals(source, target);
        target.setIsPartial(false);
    }
}