package com.vanmarcke.cpi.services;

import com.vanmarcke.cpi.data.product.CrossSellingRequestData;
import com.vanmarcke.cpi.data.product.CrossSellingResponseData;

/**
 * The {@code ESBCrossSellingService} interface defines the methods to retrieve cross-selling information from the ESB.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 21-11-2019
 */
public interface ESBCrossSellingService {

    /**
     * Retrieves the cross-selling information from the ESB.
     *
     * @param request the request object
     * @return the cross selling information
     */
    CrossSellingResponseData getCrossSellingInformation(CrossSellingRequestData request);
}
