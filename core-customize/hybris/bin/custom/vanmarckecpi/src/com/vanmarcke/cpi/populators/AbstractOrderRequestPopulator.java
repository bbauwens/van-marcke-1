package com.vanmarcke.cpi.populators;

import com.google.common.collect.Iterables;
import com.vanmarcke.cpi.data.order.AddressRequestData;
import com.vanmarcke.cpi.data.order.OrderEntryRequestData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PaymentInfoRequestData;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.vanmarcke.cpi.utils.NumberUtils.toScaledBigDecimal;
import static java.math.BigDecimal.ZERO;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.apache.commons.lang3.math.NumberUtils.DOUBLE_ZERO;
import static org.apache.commons.lang3.time.DateUtils.isSameDay;
import static org.joda.time.DateTime.now;

/**
 * The {@link AbstractOrderRequestPopulator} class is used as a starting point to populate instances of the
 * {@link OrderRequestData} class with information of the {@link AbstractOrderModel} instances.
 *
 * @param <SOURCE> the SAP Commerce order
 * @param <TARGET> the outbound order
 * @author Taki Korovessis, Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 20/12/2019
 */
public abstract class AbstractOrderRequestPopulator<SOURCE extends AbstractOrderModel, TARGET extends OrderRequestData> implements Populator<SOURCE, TARGET> {

    private final Converter<AddressModel, AddressRequestData> addressRequestConverter;
    private final Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter;
    private final Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter;

    /**
     * Constructor for {@link AbstractOrderRequestPopulator}
     *
     * @param addressRequestConverter            the address request converter
     * @param orderEntryRequestConverter         the order entry request converter
     * @param paymentInformationRequestConverter the payment information request converter
     */
    public AbstractOrderRequestPopulator(Converter<AddressModel, AddressRequestData> addressRequestConverter,
                                         Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter,
                                         Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter) {
        this.addressRequestConverter = addressRequestConverter;
        this.orderEntryRequestConverter = orderEntryRequestConverter;
        this.paymentInformationRequestConverter = paymentInformationRequestConverter;
    }

    /**
     * Adds the common order information.
     *
     * @param source the SAP Commerce order
     * @param target the outbound order
     */
    protected void addCommon(SOURCE source, TARGET target) {
        target.setOrderID(source.getCode());
        target.setCreationDate(source.getCreationtime());
        target.setCurrency(source.getCurrency() == null ? null : source.getCurrency().getIsocode());
        target.setLanguage(source.getLocale());
        target.setCustomerID(source.getUnit() == null ? null : source.getUnit().getUid());

        if (source.getUser() instanceof B2BCustomerModel) {
            B2BCustomerModel user = (B2BCustomerModel) source.getUser();
            target.setUserID(user.getUid());
            target.setCardID(getVirtualCardNumber(user.getCustomerID()));
        }

        // Added this so the call doesn't fail
        target.setPoNumber(StringUtils.EMPTY);
    }

    /**
     * Returns the virtual card number for the given {@code customerID}.
     *
     * @param customerID the customer ID
     * @return the virtual card number
     */
    protected String getVirtualCardNumber(String customerID) {
        if (StringUtils.isEmpty(customerID)) {
            return null;
        }
        int cartID = Integer.parseInt(StringUtils.removeStart(customerID, "P"));
        cartID += 500000;

        return String.valueOf(cartID);
    }

    /**
     * Adds the order entries to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addEntries(SOURCE source, TARGET target) {
        target.setEntries(orderEntryRequestConverter.convertAll(source.getEntries()));
    }

    /**
     * Adds total prices to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addTotals(SOURCE source, TARGET target) {
        target.setSubTotal(toScaledBigDecimal(source.getSubtotal()));
        target.setDeliveryCost(toScaledBigDecimal(source.getDeliveryCost()));
        target.setTotalTax(toScaledBigDecimal(source.getTotalTax()));
        target.setTotalPrice(calculateTotal(source));
    }

    /**
     * Adds delivery information to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addDeliveryInformation(SOURCE source, TARGET target) {
        target.setStoreID(StringUtils.EMPTY);
        target.setStoreCollect(false);
        target.setDeliveryReference(source.getYardReference() == null ? StringUtils.EMPTY : StringUtils.normalizeSpace(source.getYardReference()));
        target.setDeliveryComment(source.getDeliveryComment() == null ? StringUtils.EMPTY : StringUtils.normalizeSpace(source.getDeliveryComment()));

        if (source.getDeliveryMode() instanceof ZoneDeliveryModeModel && source.getDeliveryAddress() != null) {
            target.setDeliveryAddress(addressRequestConverter.convert(source.getDeliveryAddress()));
        } else if (source.getDeliveryMode() instanceof PickUpDeliveryModeModel) {
            target.setStoreID(source.getDeliveryPointOfService() == null ? StringUtils.EMPTY : source.getDeliveryPointOfService().getName());
            target.setDeliveryAddress(addEmptyAddress());
            target.setStoreCollect(isSameDay(now().toDate(), source.getDeliveryDate()));
        }

        target.setIsPartial(areRequestedDeliveryDatesTheSame(source));
    }

    /**
     * Check to see if the requested delivery dates on the entries are all the same date
     *
     * @param source the source
     * @return the decision
     */
    protected boolean areRequestedDeliveryDatesTheSame(SOURCE source) {
        List<Date> requestedDates = source.getEntries()
                .stream()
                .map(AbstractOrderEntryModel::getDeliveryDate)
                .distinct()
                .collect(Collectors.toList());
        return requestedDates.size() <= 1;
    }

    /**
     * Adds purchase order number to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addPurchaseOrderNumber(SOURCE source, TARGET target) {
        target.setPurchaseOrderNumber(source.getPurchaseOrderNumber() == null ? StringUtils.EMPTY : source.getPurchaseOrderNumber());
    }

    /**
     * Adds payment information to the order request.
     *
     * @param source the order model
     * @param target the order request data
     */
    protected void addPaymentInformation(SOURCE source, TARGET target) {
        PaymentTransactionEntryModel transactionEntry = getCurrentPaymentTransactionEntry(source);
        if (transactionEntry != null) {
            target.setPaymentInfo(paymentInformationRequestConverter.convert(transactionEntry));
        }
    }

    /**
     * Returns the last (thus current) payment transaction entry or the given order.
     * <p>
     * Returns {@code null} if there's no {@link PaymentTransactionEntryModel} found.
     *
     * @param order the order
     * @return the {@link PaymentTransactionEntryModel} instance
     */
    private PaymentTransactionEntryModel getCurrentPaymentTransactionEntry(SOURCE order) {
        PaymentTransactionEntryModel paymentTransactionEntry = null;
        if (CollectionUtils.isNotEmpty(order.getPaymentTransactions())) {
            PaymentTransactionModel paymentTransaction = Iterables.getLast(order.getPaymentTransactions());
            if (CollectionUtils.isNotEmpty(paymentTransaction.getEntries())) {
                paymentTransactionEntry = Iterables.getLast(paymentTransaction.getEntries());
            }
        }
        return paymentTransactionEntry;
    }

    /**
     * Calculates the total price for the given {@link AbstractOrderModel}.
     *
     * @param source the abstract order
     * @return the total price as BigDecimal
     */
    protected BigDecimal calculateTotal(AbstractOrderModel source) {
        if (source.getTotalPrice() == null) {
            return toScaledBigDecimal(DOUBLE_ZERO);
        }

        BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice());

        // Add the taxes to the total price if the cart is net; if the total was null taxes should be null as well
        if (isTrue(source.getNet()) && totalPrice.compareTo(ZERO) != 0 && source.getTotalTax() != null) {
            totalPrice = totalPrice.add(BigDecimal.valueOf(source.getTotalTax()));
        }

        return toScaledBigDecimal(totalPrice);
    }

    /**
     * Creates a {@link AddressRequestData} instance with empty fields.
     *
     * @return the address request
     */
    private AddressRequestData addEmptyAddress() {
        AddressRequestData addressRequest = new AddressRequestData();
        addressRequest.setApartment(StringUtils.EMPTY);
        addressRequest.setCity(StringUtils.EMPTY);
        addressRequest.setCountry(StringUtils.EMPTY);
        addressRequest.setEmail(StringUtils.EMPTY);
        addressRequest.setFax(StringUtils.EMPTY);
        addressRequest.setMobile(StringUtils.EMPTY);
        addressRequest.setPhone(StringUtils.EMPTY);
        addressRequest.setName(StringUtils.EMPTY);
        addressRequest.setPostalCode(StringUtils.EMPTY);
        addressRequest.setStreetName(StringUtils.EMPTY);
        addressRequest.setStreetNumber(StringUtils.EMPTY);
        return addressRequest;
    }
}
