package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateResponseData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

@CommandConfig(serviceName = "shippingDateInformationService", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/deliverydate")
public class GetShippingDateInformationCommand extends AbstractVMKCPICommand<ShippingDateResponseData> {

    /**
     * Creates a new instance of {@link GetShippingDateInformationCommand} with the specified request.
     *
     * @param request the request
     */
    public GetShippingDateInformationCommand(final ShippingDateRequestData request) {
        setPayLoad(request);
    }
}
