package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.CreditCheckResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command which gets the CreditCheckInformation from the backend (via the ESB)
 */
@CommandConfig(serviceName = "creditCheckInformationService", authenticationType = AuthenticationType.BASIC, httpMethod = HttpMethod.POST, url = "/creditcheck")
public class GetCreditCheckInformationCommand extends AbstractVMKCPICommand<CreditCheckResponseData> {

    /**
     * Constructor which sets the credit check request object to the payload.
     */
    public GetCreditCheckInformationCommand(final OrderRequestData request) {
        setPayLoad(request);
    }
}
