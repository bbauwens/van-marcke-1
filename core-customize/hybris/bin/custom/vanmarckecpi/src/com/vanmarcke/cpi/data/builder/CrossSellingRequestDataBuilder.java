package com.vanmarcke.cpi.data.builder;

import com.vanmarcke.cpi.data.product.CrossSellingRequestData;

/**
 * The {@code CrossSellingRequestDataBuilder} class is an implementation of the builder pattern. It makes the creation
 * of {@link CrossSellingRequestData} instances easier and cleaner.
 *
 * @author Christiaan Janssen
 * @since 29-04-2020
 */
public final class CrossSellingRequestDataBuilder {

    private static final String WINDOW_NR = "1";
    private static final String SIZE = "5";

    private final String productId;
    private final String windowNr;
    private final String size;

    private String country;

    /**
     * Creates a new instance of the {@link CrossSellingRequestDataBuilder} class.
     *
     * @param productId the product ID
     * @param windowNr  the window number
     * @param size      the size
     */
    private CrossSellingRequestDataBuilder(String productId, String windowNr, String size) {
        this.productId = productId;
        this.windowNr = windowNr;
        this.size = size;
    }

    /**
     * The entry point to the {@link CrossSellingRequestDataBuilder} class.
     * <p>
     * Start building a {@link CrossSellingRequestData} instance with the given {@code productCode}.
     *
     * @param productCode the product code
     * @return the {@link CrossSellingRequestDataBuilder} instance for method chaining
     */
    public static CrossSellingRequestDataBuilder aCrossSellingRequest(String productCode) {
        return new CrossSellingRequestDataBuilder(productCode, WINDOW_NR, SIZE);
    }

    /**
     * Sets the given {@code countryISO} to the current {@link CrossSellingRequestDataBuilder} instance
     *
     * @param countryISO the ISO code of the country
     * @return the {@link CrossSellingRequestDataBuilder} instance for method chaining
     */
    public CrossSellingRequestDataBuilder withCountryISO(String countryISO) {
        this.country = countryISO;
        return this;
    }

    /**
     * Creates and returns a new {@link CrossSellingRequestData} instance.
     *
     * @return the {@link CrossSellingRequestData} instance
     */
    public CrossSellingRequestData build() {
        CrossSellingRequestData crossSellingRequest = new CrossSellingRequestData();
        crossSellingRequest.setProductId(productId);
        crossSellingRequest.setWindowNr(windowNr);
        crossSellingRequest.setSize(size);
        crossSellingRequest.setCountry(country);
        return crossSellingRequest;
    }
}
