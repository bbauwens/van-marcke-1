package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetPlaceOrderInformationCommand;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PlaceOrderResponseData;
import com.vanmarcke.cpi.services.ESBPlaceOrderService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

/**
 * The {@code ESBPlaceOrderServiceImpl} class implements the business logic to place an order at the ESB.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 20-02-2020
 */
public class ESBPlaceOrderServiceImpl implements ESBPlaceOrderService {

    private static final Logger LOG = Logger.getLogger(ESBPlaceOrderServiceImpl.class);

    private CommandExecutor commandExecutor;

    /**
     * Creates a new instance of the {@link ESBPlaceOrderServiceImpl} class.
     *
     * @param commandExecutor the command executor
     */
    public ESBPlaceOrderServiceImpl(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PlaceOrderResponseData getPlaceOrderInformation(OrderRequestData request) {
        PlaceOrderResponseData payload = null;
        try {
            Response<PlaceOrderResponseData> response = commandExecutor.executeCommand(new GetPlaceOrderInformationCommand(request), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
            } else {
                LOG.error(MessageFormat.format("Something went wrong while placing an order. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while placing an order: {0}.", e.getMessage()), e);
        }
        return payload;
    }
}
