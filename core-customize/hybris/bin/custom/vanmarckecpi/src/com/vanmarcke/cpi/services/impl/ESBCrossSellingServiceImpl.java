package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetCrossSellingInformationCommand;
import com.vanmarcke.cpi.data.product.CrossSellingRequestData;
import com.vanmarcke.cpi.data.product.CrossSellingResponseData;
import com.vanmarcke.cpi.services.ESBCrossSellingService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

/**
 * The {@code ESBCrossSellingServiceImpl} class implements the business logic to retrieve cross-selling information
 * from the ESB.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 21-11-2019
 */
public class ESBCrossSellingServiceImpl implements ESBCrossSellingService {

    private static final Logger LOG = Logger.getLogger(ESBCrossSellingServiceImpl.class);

    private final CommandExecutor commandExecutor;

    /**
     * Creates a new instance of the {@link ESBCrossSellingServiceImpl} class.
     *
     * @param commandExecutor the command executor
     */
    public ESBCrossSellingServiceImpl(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CrossSellingResponseData getCrossSellingInformation(CrossSellingRequestData request) {
        CrossSellingResponseData payload = null;
        try {
            Response<CrossSellingResponseData> response = this.commandExecutor.executeCommand(new GetCrossSellingInformationCommand(request), null);
            if (HttpStatus.OK.equals(response.getHttpStatus()) && response.getPayLoad() != null) {
                payload = response.getPayLoad();
            } else {
                LOG.error(MessageFormat.format("Something went wrong while retrieving cross-selling information. Response Status: {0}.", response.getHttpStatus()));
            }
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Something went wrong while retrieving cross-selling information: {0}.", e.getMessage()), e);
        }
        return payload;
    }
}
