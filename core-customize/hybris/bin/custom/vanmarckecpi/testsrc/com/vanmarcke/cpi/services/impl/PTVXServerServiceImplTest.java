package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.PostFindAddressCommand;
import com.vanmarcke.cpi.data.ptv.PTVAddressData;
import com.vanmarcke.cpi.data.ptv.PTVRequestData;
import com.vanmarcke.cpi.data.ptv.PTVResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * The {@code PTVXServerServiceImplTest} class contains the unit tests for the {@link PTVXServerServiceImpl} class.
 *
 * @author David Vangeneugden, Christiaan Janssen
 * @since 06-06-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PTVXServerServiceImplTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private PTVXServerServiceImpl ptvxServerService;

    @Captor
    private ArgumentCaptor<PostFindAddressCommand> captor;

    @Test
    public void testFindAddressWithInvalidResponseCode() {
        PTVAddressData address = new PTVAddressData();

        PTVResponseData expectedPayload = new PTVResponseData();

        Response<PTVResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(PostFindAddressCommand.class), eq(null))).thenReturn(response);

        PTVResponseData actualPayload = ptvxServerService.findAddress(address);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        PostFindAddressCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        PTVRequestData request = (PTVRequestData) command.getPayLoad();

        Assertions
                .assertThat(request)
                .isNotNull();

        Assertions
                .assertThat(request.getAddr())
                .isEqualTo(address);

        Assertions
                .assertThat(request.getOptions())
                .hasSize(2);

        Assertions
                .assertThat(request.getOptions().get(0).getType())
                .isEqualTo("SearchOption");

        Assertions
                .assertThat(request.getOptions().get(0).getParam())
                .isEqualTo("RESULT_LANGUAGE");

        Assertions
                .assertThat(request.getOptions().get(0).getValue())
                .isEqualTo("DUT");

        Assertions
                .assertThat(request.getOptions().get(1).getType())
                .isEqualTo("SearchOption");

        Assertions
                .assertThat(request.getOptions().get(1).getParam())
                .isEqualTo("COUNTRY_CODETYPE");

        Assertions
                .assertThat(request.getOptions().get(1).getValue()).isEqualTo("1");
    }

    @Test
    public void testFindAddressWithoutPayload() {
        PTVAddressData address = new PTVAddressData();

        Response<PTVResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(PostFindAddressCommand.class), eq(null))).thenReturn(response);

        PTVResponseData actualPayload = ptvxServerService.findAddress(address);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        PostFindAddressCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        PTVRequestData request = (PTVRequestData) command.getPayLoad();

        Assertions
                .assertThat(request)
                .isNotNull();

        Assertions
                .assertThat(request.getAddr())
                .isEqualTo(address);

        Assertions
                .assertThat(request.getOptions())
                .hasSize(2);

        Assertions
                .assertThat(request.getOptions().get(0).getType())
                .isEqualTo("SearchOption");

        Assertions
                .assertThat(request.getOptions().get(0).getParam())
                .isEqualTo("RESULT_LANGUAGE");

        Assertions
                .assertThat(request.getOptions().get(0).getValue())
                .isEqualTo("DUT");

        Assertions
                .assertThat(request.getOptions().get(1).getType())
                .isEqualTo("SearchOption");

        Assertions
                .assertThat(request.getOptions().get(1).getParam())
                .isEqualTo("COUNTRY_CODETYPE");

        Assertions
                .assertThat(request.getOptions().get(1).getValue()).isEqualTo("1");
    }

    @Test
    public void testFindAddressWithExceptionDuringRemoteCall() {
        PTVAddressData address = new PTVAddressData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(PostFindAddressCommand.class), eq(null))).thenThrow(exception);

        PTVResponseData actualPayload = ptvxServerService.findAddress(address);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        PostFindAddressCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        PTVRequestData request = (PTVRequestData) command.getPayLoad();

        Assertions
                .assertThat(request)
                .isNotNull();

        Assertions
                .assertThat(request.getAddr())
                .isEqualTo(address);

        Assertions
                .assertThat(request.getOptions())
                .hasSize(2);

        Assertions
                .assertThat(request.getOptions().get(0).getType())
                .isEqualTo("SearchOption");

        Assertions
                .assertThat(request.getOptions().get(0).getParam())
                .isEqualTo("RESULT_LANGUAGE");

        Assertions
                .assertThat(request.getOptions().get(0).getValue())
                .isEqualTo("DUT");

        Assertions
                .assertThat(request.getOptions().get(1).getType())
                .isEqualTo("SearchOption");

        Assertions
                .assertThat(request.getOptions().get(1).getParam())
                .isEqualTo("COUNTRY_CODETYPE");

        Assertions
                .assertThat(request.getOptions().get(1).getValue()).isEqualTo("1");
    }

    @Test
    public void testFindAddress() {
        PTVAddressData address = new PTVAddressData();

        PTVResponseData expectedPayload = new PTVResponseData();

        Response<PTVResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(PostFindAddressCommand.class), eq(null))).thenReturn(response);

        PTVResponseData actualPayload = ptvxServerService.findAddress(address);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        PostFindAddressCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        PTVRequestData request = (PTVRequestData) command.getPayLoad();

        Assertions
                .assertThat(request)
                .isNotNull();

        Assertions
                .assertThat(request.getAddr())
                .isEqualTo(address);

        Assertions
                .assertThat(request.getOptions())
                .hasSize(2);

        Assertions
                .assertThat(request.getOptions().get(0).getType())
                .isEqualTo("SearchOption");

        Assertions
                .assertThat(request.getOptions().get(0).getParam())
                .isEqualTo("RESULT_LANGUAGE");

        Assertions
                .assertThat(request.getOptions().get(0).getValue())
                .isEqualTo("DUT");

        Assertions
                .assertThat(request.getOptions().get(1).getType())
                .isEqualTo("SearchOption");

        Assertions
                .assertThat(request.getOptions().get(1).getParam())
                .isEqualTo("COUNTRY_CODETYPE");

        Assertions
                .assertThat(request.getOptions().get(1).getValue()).isEqualTo("1");
    }
}