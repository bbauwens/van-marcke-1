package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetCartValidationInformationCommand;
import com.vanmarcke.cpi.data.order.CartValidationResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * The {@code ESBCartValidationServiceImplTest} class contains the unit tests for the
 * {@link ESBCartValidationServiceImpl} class.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 03-02-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ESBCartValidationServiceImplTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private ESBCartValidationServiceImpl esbCartValidationService;

    @Captor
    private ArgumentCaptor<GetCartValidationInformationCommand> captor;

    @Test
    public void testGetCartValidationInformationWithInvalidResponseCode() {
        OrderRequestData request = new OrderRequestData();

        CartValidationResponseData expectedPayload = new CartValidationResponseData();

        Response<CartValidationResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetCartValidationInformationCommand.class), eq(null))).thenReturn(response);

        CartValidationResponseData actualPayload = esbCartValidationService.getCartValidationInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCartValidationInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCartValidationInformationWithoutPayload() {
        OrderRequestData request = new OrderRequestData();

        Response<CartValidationResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(GetCartValidationInformationCommand.class), eq(null))).thenReturn(response);

        CartValidationResponseData actualPayload = esbCartValidationService.getCartValidationInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCartValidationInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCartValidationInformationWithExceptionDuringRemoteCall() {
        OrderRequestData request = new OrderRequestData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(GetCartValidationInformationCommand.class), eq(null))).thenThrow(exception);

        CartValidationResponseData actualPayload = esbCartValidationService.getCartValidationInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetCartValidationInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetCartValidationInformation() {
        OrderRequestData request = new OrderRequestData();

        CartValidationResponseData expectedPayload = new CartValidationResponseData();

        Response<CartValidationResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetCartValidationInformationCommand.class), eq(null))).thenReturn(response);

        CartValidationResponseData actualPayload = esbCartValidationService.getCartValidationInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        GetCartValidationInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }
}