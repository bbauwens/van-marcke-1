package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.OrderEntryRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKOrderEntryRequestPopulatorTest} class contains the unit tests for the
 * {@link VMKOrderEntryRequestPopulator} class.
 *
 * @author Taki Korovessis, Tom van den Berg, Christiaan Janssen
 * @since 11-12-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKOrderEntryRequestPopulatorTest {

    private static final String PRODUCT_CODE = RandomStringUtils.random(10);
    private static final Long QUANTITY = RandomUtils.nextLong();
    private static final Double BASE_PRICE = RandomUtils.nextDouble();
    private static final Double TOTAL_PRICE = RandomUtils.nextDouble();

    @InjectMocks
    private VMKOrderEntryRequestPopulator<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestPopulator;

    @Test
    public void testPopulate_withoutValues() {
        AbstractOrderEntryModel orderEntryModel = mock(AbstractOrderEntryModel.class);

        OrderEntryRequestData orderEntryRequestData = new OrderEntryRequestData();

        orderEntryRequestPopulator.populate(orderEntryModel, orderEntryRequestData);

        assertThat(orderEntryRequestData.getProductID()).isNull();
        assertThat(orderEntryRequestData.getQuantity()).isEqualTo(0L);
        assertThat(orderEntryRequestData.getBasePrice()).isEqualTo(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderEntryRequestData.getTotalPrice()).isEqualTo(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderEntryRequestData.getDeliveryDate()).isNull();
    }

    @Test
    public void testPopulate_withValues() throws Exception {
        ProductModel productModel = mock(ProductModel.class);
        when(productModel.getCode()).thenReturn(PRODUCT_CODE);

        AbstractOrderEntryModel orderEntryModel = mock(AbstractOrderEntryModel.class);
        Date entryDeliveryPickupDate = new SimpleDateFormat("yyyy-MM-dd").parse("2021-04-16");
        when(orderEntryModel.getQuantity()).thenReturn(QUANTITY);
        when(orderEntryModel.getProduct()).thenReturn(productModel);
        when(orderEntryModel.getBasePrice()).thenReturn(BASE_PRICE);
        when(orderEntryModel.getTotalPrice()).thenReturn(TOTAL_PRICE);
        when(orderEntryModel.getDeliveryDate()).thenReturn(entryDeliveryPickupDate);

        OrderEntryRequestData orderEntryRequestData = new OrderEntryRequestData();

        orderEntryRequestPopulator.populate(orderEntryModel, orderEntryRequestData);

        assertThat(orderEntryRequestData.getProductID()).isEqualTo(PRODUCT_CODE);
        assertThat(orderEntryRequestData.getQuantity()).isEqualTo(QUANTITY);
        assertThat(orderEntryRequestData.getBasePrice()).isEqualTo(BigDecimal.valueOf(BASE_PRICE).setScale(2, RoundingMode.HALF_UP));
        assertThat(orderEntryRequestData.getTotalPrice()).isEqualTo(BigDecimal.valueOf(TOTAL_PRICE).setScale(2, RoundingMode.HALF_UP));
        assertThat(orderEntryRequestData.getDeliveryDate()).isEqualTo(entryDeliveryPickupDate);
    }
}
