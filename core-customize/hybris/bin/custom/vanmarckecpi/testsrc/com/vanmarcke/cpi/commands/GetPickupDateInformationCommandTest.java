package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.PickupDateRequestData;
import com.vanmarcke.cpi.data.order.PickupDateResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GetPickupDateInformationCommandTest {

    private GetPickupDateInformationCommand command;

    @Test
    public void testConstructor() {
        PickupDateRequestData requestData = mock(PickupDateRequestData.class);

        command = new GetPickupDateInformationCommand(requestData);

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(command.getServiceName()).isEqualTo("pickupDateInformationService");
        assertThat(command.getUrl()).isEqualTo("/pickupdate1");
        assertThat(command.getPayLoad()).isEqualTo(requestData);
        assertThat(command.getResponseType()).isEqualTo(PickupDateResponseData.class);
    }

}
