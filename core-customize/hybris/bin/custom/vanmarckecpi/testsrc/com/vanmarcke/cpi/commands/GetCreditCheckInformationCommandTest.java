package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.order.CreditCheckResponseData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GetCreditCheckInformationCommandTest {

    private GetCreditCheckInformationCommand command;

    @Test
    public void testConstructor() {
        OrderRequestData requestData = mock(OrderRequestData.class);

        command = new GetCreditCheckInformationCommand(requestData);

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(command.getServiceName()).isEqualTo("creditCheckInformationService");
        assertThat(command.getUrl()).isEqualTo("/creditcheck");
        assertThat(command.getPayLoad()).isEqualTo(requestData);
        assertThat(command.getResponseType()).isEqualTo(CreditCheckResponseData.class);
    }

}