package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.AddressRequestData;
import com.vanmarcke.cpi.data.order.DeliveryEntryRequestData;
import com.vanmarcke.cpi.data.order.ShippingDateRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKShippingDateRequestPopulatorTest {

    @Mock
    private Converter<AddressModel, AddressRequestData> addressRequestConverter;
    @Mock
    private Converter<AbstractOrderEntryModel, DeliveryEntryRequestData> entryRequestDataConverter;

    private VMKShippingDateRequestPopulator populator;

    @Before
    public void setup() {
        populator = new VMKShippingDateRequestPopulator(addressRequestConverter, entryRequestDataConverter);
    }

    @Test
    public void testPopulate() {
        AddressModel addressModel = mock(AddressModel.class);
        B2BUnitModel unitModel = mock(B2BUnitModel.class);
        AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
        AbstractOrderModel source = mock(AbstractOrderModel.class);

        when(unitModel.getUid()).thenReturn("unit-uid");

        when(source.getCode()).thenReturn("code");
        when(source.getUnit()).thenReturn(unitModel);

        when(source.getDeliveryAddress()).thenReturn(addressModel);

        when(source.getEntries()).thenReturn(singletonList(abstractOrderEntryModel));

        DeliveryEntryRequestData entryRequest = mock(DeliveryEntryRequestData.class);
        when(entryRequestDataConverter.convertAll(singletonList(abstractOrderEntryModel))).thenReturn(singletonList(entryRequest));

        AddressRequestData addressRequest = mock(AddressRequestData.class);
        when(addressRequestConverter.convert(addressModel)).thenReturn(addressRequest);

        ShippingDateRequestData result = new ShippingDateRequestData();
        populator.populate(source, result);

        assertThat(result.getOrderID()).isEqualTo("code");
        assertThat(result.getCustomerID()).isEqualTo("unit-uid");
        assertThat(result.getShippingAddress()).isEqualTo(addressRequest);
        assertThat(result.getEntries()).containsExactly(entryRequest);
    }

}