package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.AddressRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The {@link VMKAddressRequestPopulatorTest} class contains the unit tests for the {@link VMKAddressRequestPopulator}
 * class.
 *
 * @author Taki Korovessis, Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 11/12/2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKAddressRequestPopulatorTest {

    private static final String FIRST_NAME = RandomStringUtils.random(10);
    private static final String LAST_NAME = RandomStringUtils.random(10);
    private static final String FULL_NAME = RandomStringUtils.random(10);
    private static final String COMPANY_NAME = RandomStringUtils.random(10);
    private static final String STREET_NAME = RandomStringUtils.random(10);
    private static final String STREET_NUMBER = RandomStringUtils.random(10);
    private static final String APARTMENT = RandomStringUtils.random(10);
    private static final String POSTAL_CODE = RandomStringUtils.random(10);
    private static final String CITY = RandomStringUtils.random(10);
    private static final String COUNTRY = RandomStringUtils.random(10);
    private static final String PHONE = RandomStringUtils.random(10);
    private static final String MOBILE = RandomStringUtils.random(10);
    private static final String FAX = RandomStringUtils.random(10);
    private static final String EMAIL = RandomStringUtils.random(10);
    private static final String CUSTOMER_UID = RandomStringUtils.random(10);

    @Mock
    private CustomerNameStrategy customerNameStrategy;

    @InjectMocks
    private VMKAddressRequestPopulator<AddressModel, AddressRequestData> addressRequestPopulator;

    @Test
    public void testPopulate_withoutValues() {
        AddressModel addressModel = mock(AddressModel.class);

        AddressRequestData addressRequestData = new AddressRequestData();

        addressRequestPopulator.populate(addressModel, addressRequestData);

        assertThat(addressRequestData.getName()).isNull();
        assertThat(addressRequestData.getStreetName()).isNull();
        assertThat(addressRequestData.getStreetNumber()).isNull();
        assertThat(addressRequestData.getApartment()).isEmpty();
        assertThat(addressRequestData.getPostalCode()).isNull();
        assertThat(addressRequestData.getCity()).isNull();
        assertThat(addressRequestData.getCountry()).isNull();
        assertThat(addressRequestData.getEmail()).isEmpty();
        assertThat(addressRequestData.getPhone()).isEmpty();
        assertThat(addressRequestData.getMobile()).isEmpty();
        assertThat(addressRequestData.getFax()).isEmpty();

        verifyZeroInteractions(customerNameStrategy);
    }

    @Test
    public void testPopulate_withValues() {
        CountryModel countryModel = mock(CountryModel.class);
        when(countryModel.getIsocode()).thenReturn(COUNTRY);

        AddressModel addressModel = mock(AddressModel.class);
        when(addressModel.getCompany()).thenReturn(COMPANY_NAME);
        when(addressModel.getStreetname()).thenReturn(STREET_NAME);
        when(addressModel.getStreetnumber()).thenReturn(STREET_NUMBER);
        when(addressModel.getAppartment()).thenReturn(APARTMENT);
        when(addressModel.getPostalcode()).thenReturn(POSTAL_CODE);
        when(addressModel.getTown()).thenReturn(CITY);
        when(addressModel.getCountry()).thenReturn(countryModel);
        when(addressModel.getPhone1()).thenReturn(PHONE);
        when(addressModel.getPhone2()).thenReturn(MOBILE);
        when(addressModel.getFax()).thenReturn(FAX);
        when(addressModel.getEmail()).thenReturn(EMAIL);

        AddressRequestData addressRequestData = new AddressRequestData();

        addressRequestPopulator.populate(addressModel, addressRequestData);

        assertThat(addressRequestData.getName()).isEqualTo(COMPANY_NAME);
        assertThat(addressRequestData.getStreetName()).isEqualTo(STREET_NAME);
        assertThat(addressRequestData.getStreetNumber()).isEqualTo(STREET_NUMBER);
        assertThat(addressRequestData.getApartment()).isEqualTo(APARTMENT);
        assertThat(addressRequestData.getPostalCode()).isEqualTo(POSTAL_CODE);
        assertThat(addressRequestData.getCity()).isEqualTo(CITY);
        assertThat(addressRequestData.getCountry()).isEqualTo(COUNTRY);
        assertThat(addressRequestData.getEmail()).isEqualTo(EMAIL);
        assertThat(addressRequestData.getPhone()).isEqualTo(PHONE);
        assertThat(addressRequestData.getMobile()).isEqualTo(MOBILE);
        assertThat(addressRequestData.getFax()).isEqualTo(FAX);

        verifyZeroInteractions(customerNameStrategy);
    }

    @Test
    public void testGetName_withoutValues() {
        AddressModel addressModel = mock(AddressModel.class);

        String actualName = addressRequestPopulator.getName(addressModel);

        assertThat(actualName).isNull();

        verifyZeroInteractions(customerNameStrategy);
    }

    @Test
    public void testGetName_withCompanyName() {
        AddressModel addressModel = mock(AddressModel.class);
        when(addressModel.getCompany()).thenReturn(COMPANY_NAME);

        String actualName = addressRequestPopulator.getName(addressModel);

        assertThat(actualName).isEqualTo(COMPANY_NAME);

        verifyZeroInteractions(customerNameStrategy);
    }

    @Test
    public void testGetName_withFirstAndLastName() {
        AddressModel addressModel = mock(AddressModel.class);
        when(addressModel.getCompany()).thenReturn(COMPANY_NAME);
        when(addressModel.getFirstname()).thenReturn(FIRST_NAME);
        when(addressModel.getLastname()).thenReturn(LAST_NAME);

        when(customerNameStrategy.getName(FIRST_NAME, LAST_NAME)).thenReturn(FULL_NAME);

        String actualName = addressRequestPopulator.getName(addressModel);

        assertThat(actualName).isEqualTo(FULL_NAME);

        verify(customerNameStrategy).getName(FIRST_NAME, LAST_NAME);
    }

    @Test
    public void testGetEmailAddress_withoutValues() {
        AddressModel addressModel = mock(AddressModel.class);

        String actualEmailAddress = addressRequestPopulator.getEmailAddress(addressModel);

        assertThat(actualEmailAddress).isNull();
    }

    @Test
    public void testGetEmailAddress_withoutOrder() {
        AddressModel addressModel = mock(AddressModel.class);
        when(addressModel.getEmail()).thenReturn(EMAIL);

        String actualEmailAddress = addressRequestPopulator.getEmailAddress(addressModel);

        assertThat(actualEmailAddress).isEqualTo(EMAIL);
    }

    @Test
    public void testGetEmailAddress_withOrder_withoutUser() {
        OrderModel order = mock(OrderModel.class);

        AddressModel addressModel = mock(AddressModel.class);
        when(addressModel.getEmail()).thenReturn(EMAIL);
        when(addressModel.getOwner()).thenReturn(order);

        String actualEmailAddress = addressRequestPopulator.getEmailAddress(addressModel);

        assertThat(actualEmailAddress).isEqualTo(EMAIL);
    }

    @Test
    public void testGetEmailAddress_withOrder_withUser() {
        B2BCustomerModel customer = mock(B2BCustomerModel.class);
        when(customer.getUid()).thenReturn(CUSTOMER_UID);

        OrderModel order = mock(OrderModel.class);
        when(order.getUser()).thenReturn(customer);

        AddressModel addressModel = mock(AddressModel.class);
        when(addressModel.getEmail()).thenReturn(EMAIL);
        when(addressModel.getOwner()).thenReturn(order);

        String actualEmailAddress = addressRequestPopulator.getEmailAddress(addressModel);

        assertThat(actualEmailAddress).isEqualTo(CUSTOMER_UID);
    }

    @Test
    public void testGetOrder_withoutValue() {
        AddressModel addressModel = mock(AddressModel.class);

        AbstractOrderModel actualOrder = addressRequestPopulator.getOrder(addressModel);

        assertThat(actualOrder).isNull();
    }

    @Test
    public void testGetOrder_withOrder() {
        OrderModel orderModel = mock(OrderModel.class);

        AddressModel addressModel = mock(AddressModel.class);
        when(addressModel.getOwner()).thenReturn(orderModel);

        AbstractOrderModel actualOrder = addressRequestPopulator.getOrder(addressModel);

        assertThat(actualOrder).isEqualTo(orderModel);
    }

    @Test
    public void testGetOrder_withCart() {
        CartModel cartModel = mock(CartModel.class);

        AddressModel addressModel = mock(AddressModel.class);
        when(addressModel.getOwner()).thenReturn(cartModel);

        AbstractOrderModel actualOrder = addressRequestPopulator.getOrder(addressModel);

        assertThat(actualOrder).isEqualTo(cartModel);
    }
}