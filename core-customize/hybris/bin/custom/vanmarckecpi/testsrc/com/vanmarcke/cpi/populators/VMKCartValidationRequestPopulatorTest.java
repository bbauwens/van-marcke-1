package com.vanmarcke.cpi.populators;

import com.vanmarcke.cpi.data.order.AddressRequestData;
import com.vanmarcke.cpi.data.order.OrderEntryRequestData;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PaymentInfoRequestData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKCartValidationRequestPopulatorTest} class contains the unit tests for the
 * {@link VMKCartValidationRequestPopulator} class.
 *
 * @author Taki Korovessis, Niels Raemaekers, Tom van den Berg, Christiaan Janssen
 * @since 20-12-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKCartValidationRequestPopulatorTest {

    private static final String B2B_CUSTOMER_UID = RandomStringUtils.random(10);
    private static final Long CUSTOMER_ID = RandomUtils.nextLong(0, 99999);

    private static final String B2B_UNIT_UID = RandomStringUtils.random(10);

    private static final String CURRENCY_ISO = RandomStringUtils.random(10);
    private static final String LANGUAGE_ISO = RandomStringUtils.random(10);

    private static final String ORDER_CODE = RandomStringUtils.random(10);
    private static final String ORDER_PO = RandomStringUtils.random(10);
    private static final String ORDER_DELIVERY_REFERENCE = RandomStringUtils.random(10);
    private static final String ORDER_DELIVERY_COMMENT = RandomStringUtils.random(10);
    private static final Date ORDER_DELIVERY_DATE = new Date();
    private static final Date ORDER_CREATION_DATE = new Date();
    private static final Double ORDER_SUBTOTAL = RandomUtils.nextDouble();
    private static final Double ORDER_DELIVERY_COST = RandomUtils.nextDouble();
    private static final Double ORDER_TOTAL_TAX = RandomUtils.nextDouble();
    private static final Double ORDER_TOTAL_PRICE = RandomUtils.nextDouble();

    @Mock
    private Converter<AddressModel, AddressRequestData> addressRequestConverter;

    @Mock
    private Converter<AbstractOrderEntryModel, OrderEntryRequestData> orderEntryRequestConverter;

    @Mock
    private Converter<PaymentTransactionEntryModel, PaymentInfoRequestData> paymentInformationRequestConverter;

    private VMKCartValidationRequestPopulator<AbstractOrderModel, OrderRequestData> cartValidationRequestPopulator;

    @Before
    public void setUp() {
        cartValidationRequestPopulator = new VMKCartValidationRequestPopulator<>(addressRequestConverter, orderEntryRequestConverter, paymentInformationRequestConverter);
    }

    @Test
    public void testPopulate_withoutValues() {
        OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getEntries()).thenReturn(Collections.emptyList());

        when(orderEntryRequestConverter.convertAll(Collections.emptyList())).thenReturn(Collections.emptyList());

        OrderRequestData orderRequestData = new OrderRequestData();

        cartValidationRequestPopulator.populate(orderModel, orderRequestData);

        assertThat(orderRequestData.getOrderID()).isNull();
        assertThat(orderRequestData.getCreationDate()).isNull();
        assertThat(orderRequestData.getCurrency()).isNull();
        assertThat(orderRequestData.getLanguage()).isNull();
        assertThat(orderRequestData.getCustomerID()).isNull();
        assertThat(orderRequestData.getUserID()).isNull();
        assertThat(orderRequestData.getCardID()).isNull();
        assertThat(orderRequestData.getEntries()).isEmpty();
        assertThat(orderRequestData.getSubTotal()).isEqualTo(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderRequestData.getDeliveryCost()).isEqualTo(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderRequestData.getTotalTax()).isEqualTo(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderRequestData.getTotalPrice()).isEqualTo(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderRequestData.getStoreID()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.isStoreCollect()).isFalse();
        assertThat(orderRequestData.getDeliveryComment()).isEmpty();
        assertThat(orderRequestData.getDeliveryReference()).isEmpty();
        assertThat(orderRequestData.getDeliveryAddress()).isNull();
        assertThat(orderRequestData.getPurchaseOrderNumber()).isEmpty();
        assertThat(orderRequestData.getPaymentInfo()).isNotNull();
        assertThat(orderRequestData.getPaymentInfo().getMethod()).isEmpty();
        assertThat(orderRequestData.getPaymentInfo().getReference()).isEmpty();
        assertThat(orderRequestData.getPaymentInfo().getStatus()).isEmpty();

        verify(orderEntryRequestConverter).convertAll(Collections.emptyList());
        verifyZeroInteractions(addressRequestConverter);
        verifyZeroInteractions(paymentInformationRequestConverter);
    }

    @Test
    public void testPopulate_withValues() {
        B2BCustomerModel b2bCustomerModel = mock(B2BCustomerModel.class);
        when(b2bCustomerModel.getUid()).thenReturn(B2B_CUSTOMER_UID);
        when(b2bCustomerModel.getCustomerID()).thenReturn("P" + CUSTOMER_ID);

        B2BUnitModel b2bUnitModel = mock(B2BUnitModel.class);
        when(b2bUnitModel.getUid()).thenReturn(B2B_UNIT_UID);

        CurrencyModel currencyModel = mock(CurrencyModel.class);
        when(currencyModel.getIsocode()).thenReturn(CURRENCY_ISO);

        AbstractOrderEntryModel orderEntryModel = mock(AbstractOrderEntryModel.class);

        OrderModel orderModel = mock(OrderModel.class);
        when(orderModel.getCode()).thenReturn(ORDER_CODE);
        when(orderModel.getCreationtime()).thenReturn(ORDER_CREATION_DATE);
        when(orderModel.getCurrency()).thenReturn(currencyModel);
        when(orderModel.getLocale()).thenReturn(LANGUAGE_ISO);
        when(orderModel.getUnit()).thenReturn(b2bUnitModel);
        when(orderModel.getUser()).thenReturn(b2bCustomerModel);
        when(orderModel.getEntries()).thenReturn(Collections.singletonList(orderEntryModel));
        when(orderModel.getSubtotal()).thenReturn(ORDER_SUBTOTAL);
        when(orderModel.getDeliveryCost()).thenReturn(ORDER_DELIVERY_COST);
        when(orderModel.getTotalTax()).thenReturn(ORDER_TOTAL_TAX);
        when(orderModel.getTotalPrice()).thenReturn(ORDER_TOTAL_PRICE);
        when(orderModel.getDeliveryDate()).thenReturn(ORDER_DELIVERY_DATE);
        when(orderModel.getDeliveryComment()).thenReturn(ORDER_DELIVERY_COMMENT);
        when(orderModel.getYardReference()).thenReturn(ORDER_DELIVERY_REFERENCE);
        when(orderModel.getPurchaseOrderNumber()).thenReturn(ORDER_PO);

        OrderEntryRequestData orderEntryRequestData = new OrderEntryRequestData();

        when(orderEntryRequestConverter.convertAll(Collections.singletonList(orderEntryModel))).thenReturn(Collections.singletonList(orderEntryRequestData));

        OrderRequestData orderRequestData = new OrderRequestData();

        cartValidationRequestPopulator.populate(orderModel, orderRequestData);

        assertThat(orderRequestData.getOrderID()).isEqualTo(ORDER_CODE);
        assertThat(orderRequestData.getCreationDate()).isEqualTo(ORDER_CREATION_DATE);
        assertThat(orderRequestData.getCurrency()).isEqualTo(CURRENCY_ISO);
        assertThat(orderRequestData.getLanguage()).isEqualTo(LANGUAGE_ISO);
        assertThat(orderRequestData.getCustomerID()).isEqualTo(B2B_UNIT_UID);
        assertThat(orderRequestData.getUserID()).isEqualTo(B2B_CUSTOMER_UID);
        assertThat(orderRequestData.getCardID()).isEqualTo(String.valueOf((CUSTOMER_ID + 500000)));
        assertThat(orderRequestData.getEntries()).containsExactly(orderEntryRequestData);
        assertThat(orderRequestData.getSubTotal()).isEqualTo(BigDecimal.valueOf(ORDER_SUBTOTAL).setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderRequestData.getDeliveryCost()).isEqualTo(BigDecimal.valueOf(ORDER_DELIVERY_COST).setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderRequestData.getTotalTax()).isEqualTo(BigDecimal.valueOf(ORDER_TOTAL_TAX).setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderRequestData.getTotalPrice()).isEqualTo(BigDecimal.valueOf(ORDER_TOTAL_PRICE).setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(orderRequestData.getStoreID()).isEqualTo(StringUtils.EMPTY);
        assertThat(orderRequestData.isStoreCollect()).isFalse();
        assertThat(orderRequestData.getDeliveryReference()).isEqualTo(ORDER_DELIVERY_REFERENCE);
        assertThat(orderRequestData.getDeliveryComment()).isEqualTo(ORDER_DELIVERY_COMMENT);
        assertThat(orderRequestData.getDeliveryAddress()).isNull();
        assertThat(orderRequestData.getPurchaseOrderNumber()).isEqualTo(ORDER_PO);
        assertThat(orderRequestData.getPaymentInfo()).isNotNull();
        assertThat(orderRequestData.getPaymentInfo().getMethod()).isEmpty();
        assertThat(orderRequestData.getPaymentInfo().getReference()).isEmpty();
        assertThat(orderRequestData.getPaymentInfo().getStatus()).isEmpty();

        verify(orderEntryRequestConverter).convertAll(Collections.singletonList(orderEntryModel));
        verifyZeroInteractions(addressRequestConverter);
        verifyZeroInteractions(paymentInformationRequestConverter);
    }
}
