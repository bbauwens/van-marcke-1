package com.vanmarcke.cpi.commands;

import com.vanmarcke.cpi.data.price.PriceRequestData;
import com.vanmarcke.cpi.data.price.PriceResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GetPriceInformationCommandTest {

    private GetPriceInformationCommand command;

    @Test
    public void testConstructor() {
        PriceRequestData priceRequestData = mock(PriceRequestData.class);

        command = new GetPriceInformationCommand(priceRequestData);

        assertThat(command.getHttpMethod()).isEqualTo(HttpMethod.POST);
        assertThat(command.getServiceName()).isEqualTo("priceInformationService");
        assertThat(command.getUrl()).isEqualTo("/pricing");
        assertThat(command.getPayLoad()).isEqualTo(priceRequestData);
        assertThat(command.getResponseType()).isEqualTo(PriceResponseData.class);
    }

}