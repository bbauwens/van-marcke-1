package com.vanmarcke.cpi.services.impl;

import com.vanmarcke.cpi.commands.GetPlaceOrderInformationCommand;
import com.vanmarcke.cpi.data.order.OrderRequestData;
import com.vanmarcke.cpi.data.order.PlaceOrderResponseData;
import de.hybris.bootstrap.annotations.UnitTest;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.impl.ResponseImpl;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * The {@code ESBPlaceOrderServiceImplTest} class contains the unit test for the {@link ESBPlaceOrderServiceImpl} class.
 *
 * @author Niels Raemaekers, Christiaan Janssen
 * @since 20-02-2020
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ESBPlaceOrderServiceImplTest {

    @Mock
    private CommandExecutor executor;

    @InjectMocks
    private ESBPlaceOrderServiceImpl esbPlaceOrderService;

    @Captor
    private ArgumentCaptor<GetPlaceOrderInformationCommand> captor;

    @Test
    public void testGetPlaceOrderInformationWithInvalidResponseCode() {
        OrderRequestData request = new OrderRequestData();

        PlaceOrderResponseData expectedPayload = new PlaceOrderResponseData();

        Response<PlaceOrderResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.FORBIDDEN);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetPlaceOrderInformationCommand.class), eq(null))).thenReturn(response);

        PlaceOrderResponseData actualPayload = esbPlaceOrderService.getPlaceOrderInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetPlaceOrderInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetPlaceOrderInformationWithoutPayload() {
        OrderRequestData request = new OrderRequestData();

        Response<PlaceOrderResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);

        when(executor.executeCommand(any(GetPlaceOrderInformationCommand.class), eq(null))).thenReturn(response);

        PlaceOrderResponseData actualPayload = esbPlaceOrderService.getPlaceOrderInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetPlaceOrderInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetPlaceOrderInformationWithExceptionDuringRemoteCall() {
        OrderRequestData request = new OrderRequestData();

        RuntimeException exception = mock(RuntimeException.class);

        when(executor.executeCommand(any(GetPlaceOrderInformationCommand.class), eq(null))).thenThrow(exception);

        PlaceOrderResponseData actualPayload = esbPlaceOrderService.getPlaceOrderInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isNull();

        GetPlaceOrderInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }

    @Test
    public void testGetPlaceOrderInformation() {
        OrderRequestData request = new OrderRequestData();

        PlaceOrderResponseData expectedPayload = new PlaceOrderResponseData();

        Response<PlaceOrderResponseData> response = new ResponseImpl<>();
        response.setHttpStatus(HttpStatus.OK);
        response.setPayLoad(expectedPayload);

        when(executor.executeCommand(any(GetPlaceOrderInformationCommand.class), eq(null))).thenReturn(response);

        PlaceOrderResponseData actualPayload = esbPlaceOrderService.getPlaceOrderInformation(request);

        verify(executor).executeCommand(captor.capture(), eq(null));

        Assertions
                .assertThat(actualPayload)
                .isEqualTo(expectedPayload);

        GetPlaceOrderInformationCommand command = captor.getValue();

        Assertions
                .assertThat(command)
                .isNotNull();

        Assertions
                .assertThat(command.getPayLoad())
                .isEqualTo(request);
    }
}