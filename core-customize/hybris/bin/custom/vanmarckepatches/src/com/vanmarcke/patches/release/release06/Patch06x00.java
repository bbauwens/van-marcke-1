package com.vanmarcke.patches.release.release06;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R6;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * Defines the impex files of patch 06.
 *
 * @author Raemaekers Niels
 * @since 10-06-2021
 */
public class Patch06x00 extends AbstractVMKPatch {

    /**
     * Provides an implementation of Patch06x00.
     */
    public Patch06x00() {
        super("06_00", "Release 6 - Phase 1", R6, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r06_00_574_metaDataForHomepage.impex", languages, updateLanguagesOnly);
        importGlobalData("r06_00_562_translationForFacetWithUnit.impex", languages, updateLanguagesOnly);
    }
}
