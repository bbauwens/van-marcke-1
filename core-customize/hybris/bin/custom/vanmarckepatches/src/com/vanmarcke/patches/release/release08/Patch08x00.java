package com.vanmarcke.patches.release.release08;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R8;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * Defines the impex files of patch 08.
 *
 * @author Cristi Stoica
 * @since 02-07-2021
 */
public class Patch08x00 extends AbstractVMKPatch {

    /**
     * Provides an implementation of Patch08x00.
     */
    public Patch08x00() {
        super("08_00", "Release 8 - Phase 1", R8, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r08_00_569_productAvailabilitySearchRestrictions.impex", languages, updateLanguagesOnly);
        importGlobalData("r08_00_00_blue_product_solr_search_template.impex", languages, updateLanguagesOnly);
    }
}
