package com.vanmarcke.patches.release.release01;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R1;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

public class Patch01x00 extends AbstractVMKPatch {

    public Patch01x00() {
        super("01_00", "Release 1 - Phase 1", R1, V1);
    }

    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r01_00_001_content_catalog.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_002_promotions.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_003_stores_openinghours.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_004_stores_core.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_005_user_groups_asm.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_010_blue_product_solr.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_011_stores_solr.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_012_categories_solr.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_020_sites.impex", languages, updateLanguagesOnly);

        importGlobalData("r01_00_030_cms_core.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_031_cms_sample.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_032_cms_header.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_033_cms_categorypage.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_035_cms_categorylistercomponent.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_036_cms_asm.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_037_cms_checkout.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_038_cms_pdp.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_039_cms_plp.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_040_cms_homepage.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_041_cms_notfound.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_042_cms_search.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_043_constraints.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_044_saml_mapping.impex", languages, updateLanguagesOnly);
        importGlobalData("r01_00_044_cms_site_navigation.impex", languages, updateLanguagesOnly);
    }
}