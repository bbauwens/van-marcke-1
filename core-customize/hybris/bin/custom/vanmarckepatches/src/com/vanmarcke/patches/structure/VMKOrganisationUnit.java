package com.vanmarcke.patches.structure;

import de.hybris.platform.patches.organisation.ImportLanguage;
import de.hybris.platform.patches.organisation.ImportOrganisationUnit;
import de.hybris.platform.patches.organisation.StructureState;

import java.util.Collection;
import java.util.List;

import static java.util.Arrays.asList;

public enum VMKOrganisationUnit implements ImportOrganisationUnit {

    VMK("VMK", "Van Marcke", asList(VMKLanguage.values()));

    private final String code;
    private final String name;
    private final Collection<ImportLanguage> languages;

    VMKOrganisationUnit(final String code, final String name, final List<ImportLanguage> languages) {
        this.code = code;
        this.name = name;
        this.languages = languages;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getFolderName() {
        return null;
    }

    @Override
    public String getCommonFolderName() {
        return null;
    }

    @Override
    public Collection getChildren() {
        return null;
    }

    @Override
    public Collection<ImportLanguage> getLanguages() {
        return languages;
    }

    @Override
    public ImportOrganisationUnit getParent() {
        return null;
    }

    @Override
    public void setParent(ImportOrganisationUnit importOrganisationUnit) {
    }

    @Override
    public StructureState getStructureState() {
        return VMKStructureState.V1;
    }
}
