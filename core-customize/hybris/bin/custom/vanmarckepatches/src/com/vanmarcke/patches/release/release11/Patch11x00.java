package com.vanmarcke.patches.release.release11;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R11;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * The {@link Patch11x00} class contains lists the patches for release 10.
 *
 * @author Cristi Stoica
 * @since 06-10-2021
 */
public class Patch11x00 extends AbstractVMKPatch {
    /**
     * Provides an implementation of Patch11x00.
     */
    public Patch11x00() {
        super("11_00", "Release 11 - Phase 1", R11, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r11_00_stockStatusSolrIndexedProperty.impex", languages, updateLanguagesOnly);
        importGlobalData("r11_00_localizedMessages.impex", languages, updateLanguagesOnly);
    }
}
