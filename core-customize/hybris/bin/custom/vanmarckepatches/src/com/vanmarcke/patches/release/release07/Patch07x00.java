package com.vanmarcke.patches.release.release07;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R7;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * Defines the impex files of patch 07.
 *
 * @author Niels Raemaekers
 * @since 24-06-2021
 */
public class Patch07x00 extends AbstractVMKPatch {

    /**
     * Provides an implementation of Patch07x00.
     */
    public Patch07x00() {
        super("07_00", "Release 7 - Phase 1", R7, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r07_00_470_translation_alternativeTec_SKUA.impex", languages, updateLanguagesOnly);
    }
}
