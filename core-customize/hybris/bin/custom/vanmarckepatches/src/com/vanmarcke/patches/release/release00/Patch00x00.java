package com.vanmarcke.patches.release.release00;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;
import de.hybris.platform.patches.organisation.StructureState;
import de.hybris.platform.validation.services.ValidationService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R0;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

public class Patch00x00 extends AbstractVMKPatch {

    private ValidationService validationService;

    public Patch00x00() {
        super("00_00", "Initial Data", R0, V1);
    }

    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r00_00_000_i18n.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_001_product_catalog.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_002_content_catalog.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_003_user_groups.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_004_user_rights.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_005_multimedia.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_006_pricing.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_007_cronjobs.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_008_solr_settings.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_009_backoffice_solr.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_010_backoffice_dashboard.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_011_employees.impex", languages, updateLanguagesOnly);
        importGlobalData("r00_00_012_products.impex", languages, updateLanguagesOnly);
    }

    @Override
    protected void afterCreateGlobalData(final StructureState structureState, final boolean update) {
        // reload validation engine after inserting new constraints
        validationService.reloadValidationEngine();
    }

    @Required
    public void setValidationService(final ValidationService validationService) {
        this.validationService = validationService;
    }
}