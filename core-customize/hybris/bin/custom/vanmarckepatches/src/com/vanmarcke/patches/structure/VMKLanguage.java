package com.vanmarcke.patches.structure;

import de.hybris.platform.patches.organisation.ImportLanguage;

/**
 * Locales enumeration used in import process.
 * This should contain all different locales which are used on the site.
 */
public enum VMKLanguage implements ImportLanguage {

    DE("de"),
    EN("en"),
    ES("es"),
    FR("fr"),
    IT("it"),
    NL("nl"),
    FR_BE("fr_BE"),
    FR_FR("fr_FR"),
    NL_BE("nl_BE");

    private String code;

    VMKLanguage(final String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return this.code;
    }

}
