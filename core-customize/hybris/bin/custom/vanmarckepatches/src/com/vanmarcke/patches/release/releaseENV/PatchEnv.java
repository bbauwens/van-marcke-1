package com.vanmarcke.patches.release.releaseENV;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.Rerunnable;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.ENV;
import static com.vanmarcke.patches.structure.VMKStructureState.LAST;

public class PatchEnv extends AbstractVMKPatch implements Rerunnable {

    public PatchEnv() {
        super("env", "Environment Data", ENV, LAST);
    }

    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("renv_000_solr_settings.impex", languages, updateLanguagesOnly);
        importGlobalData("renv_001_backoffice_solr.impex", languages, updateLanguagesOnly);
    }
}