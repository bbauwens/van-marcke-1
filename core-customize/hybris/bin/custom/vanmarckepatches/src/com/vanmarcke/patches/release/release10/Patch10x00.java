package com.vanmarcke.patches.release.release10;

import com.vanmarcke.patches.release.AbstractVMKPatch;
import de.hybris.platform.patches.organisation.ImportLanguage;

import java.util.Set;

import static com.vanmarcke.patches.structure.VMKRelease.R10;
import static com.vanmarcke.patches.structure.VMKStructureState.V1;

/**
 * The {@link Patch10x00} class contains lists the patches for release 10.
 *
 * @author Cristi Stoica
 * @since 28-09-2021
 */
public class Patch10x00 extends AbstractVMKPatch {
    /**
     * Provides an implementation of Patch10x00.
     */
    public Patch10x00() {
        super("10_00", "Release 10 - Phase 1", R10, V1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createGlobalData(final Set<ImportLanguage> languages, final boolean updateLanguagesOnly) {
        importGlobalData("r10_00_747_fallback_customers.impex", languages, updateLanguagesOnly);
        importGlobalData("r10_00_746_localizedMessage.impex", languages, updateLanguagesOnly);
    }
}
