package com.vanmarcke.patches.structure;

import de.hybris.platform.patches.organisation.StructureState;

/**
 * Enumeration that defines structure state; it may be used in different objects to indicate for which milestone given
 * object was introduced.
 */
public enum VMKStructureState implements StructureState {
    V1,
    LAST;

    @Override
    public boolean isAfter(final StructureState structureState) {
        if (this == structureState) {
            return false;
        }
        for (final StructureState iterateValue : values()) {
            if (structureState.equals(iterateValue)) {
                return true;
            }
            if (this.equals(iterateValue)) {
                return false;
            }
        }
        return false;
    }
}