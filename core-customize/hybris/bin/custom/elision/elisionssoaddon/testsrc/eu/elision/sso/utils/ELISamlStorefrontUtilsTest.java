package eu.elision.sso.utils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.Config;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static eu.elision.sso.utils.ELISamlStorefrontUtils.LANGUAGE_COOKIE_MAX_AGE;
import static eu.elision.sso.utils.ELISamlStorefrontUtils.LANGUAGE_COOKIE_NAME;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ELISamlStorefrontUtilsTest {

    @Mock
    private ConfigurationService configurationService;
    @InjectMocks
    private ELISamlStorefrontUtils eliSamlStorefrontUtils;

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private Configuration configuration;

    @Before
    public void setup() {
        when(configurationService.getConfiguration()).thenReturn(configuration);
    }

    @Test
    public void testCreateOrUpdateLanguageCookie() throws Exception {
        String languageCode = "nl_BE";
        when(configuration.getString(LANGUAGE_COOKIE_NAME)).thenReturn("language");
        when(configuration.getString(LANGUAGE_COOKIE_MAX_AGE)).thenReturn("60");
        //whenNew(Cookie.class).withArguments("language", languageCode).thenReturn(cookie);
        eliSamlStorefrontUtils.createOrUpdateLanguageCookie(response, languageCode);
        verify(response).addCookie(any(Cookie.class));
    }

    @Test
    public void testCreateOrUpdateLanguageCookieNoCookieName() {
        String languageCode = "nl_BE";
        when(configuration.getString(LANGUAGE_COOKIE_MAX_AGE)).thenReturn("60");
        eliSamlStorefrontUtils.createOrUpdateLanguageCookie(response, languageCode);
        verifyZeroInteractions(response);
    }

    @Test(expected = NumberFormatException.class)
    public void testCreateOrUpdateLanguageCookieNoMaxAge() {
        String languageCode = "nl_BE";

        eliSamlStorefrontUtils.createOrUpdateLanguageCookie(response, languageCode);
    }

    @Test
    public void testEraseSamlCookie() throws Exception {
        when(configuration.getString(anyString())).thenReturn("LoginToken");
        eliSamlStorefrontUtils.eraseSamlCookie(response);
        verify(response).addCookie(any(Cookie.class));
    }

    @Test
    public void testEraseSamlCookieWhenNoName() {
        eliSamlStorefrontUtils.eraseSamlCookie(response);
        verifyZeroInteractions(response);
    }


    @Test
    public void testGetSamlCookie() {
        Cookie cookie = mock(Cookie.class);
        when(configuration.getString(anyString())).thenReturn("LoginToken");
        Cookie result = eliSamlStorefrontUtils.getSamlCookie(request);
    }

    @Test
    public void testGetSamlCookieNoCookieName() {
        Cookie result = eliSamlStorefrontUtils.getSamlCookie(request);
        assertThat(result).isNull();
    }
}