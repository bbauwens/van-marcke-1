package eu.elision.sso.utils;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.Config;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Utils class to update, create or delete cookies concerning the the SAML SSO
 */
public class ELISamlStorefrontUtils {
    public static final String SSO_COOKIE_NAME = "sso.cookie.name";
    public static final String LANGUAGE_COOKIE_NAME = "sso.language.cookie";
    public static final String LANGUAGE_COOKIE_MAX_AGE = "sso.language.cookie.max.age";

    private final ConfigurationService configurationService;

    public ELISamlStorefrontUtils(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * Static method to create/update the language {@link Cookie}
     *
     * @param response     {@link HttpServletResponse}
     * @param languageCode String containing the languageCode to set as value on the {@link Cookie}
     */
    public void createOrUpdateLanguageCookie(HttpServletResponse response, String languageCode) {
        final String cookieName = configurationService.getConfiguration().getString(LANGUAGE_COOKIE_NAME);
        int maxAge = Integer.parseInt(configurationService.getConfiguration().getString(LANGUAGE_COOKIE_MAX_AGE));
        if (StringUtils.isNotBlank(cookieName)) {
            Cookie cookie = new Cookie(cookieName, languageCode);
            cookie.setMaxAge(maxAge);
            cookie.setPath("/");
            cookie.setHttpOnly(true);
            cookie.setSecure(true);
            response.addCookie(cookie);
        }
    }

    /**
     * Static method the remove the cookie from the browser
     *
     * @param response {@link HttpServletResponse}
     */
    public void eraseSamlCookie(final HttpServletResponse response) {
        final String cookieName = configurationService.getConfiguration().getString(SSO_COOKIE_NAME);
        if (cookieName != null) {
            final Cookie cookie = new Cookie(cookieName, "");
            cookie.setMaxAge(0);
            cookie.setPath("/");
            cookie.setHttpOnly(true);
            cookie.setSecure(true);
            response.addCookie(cookie);
        }
    }

    /**
     * Static method to get the Saml Cookie from the request.
     *
     * @param request {@link HttpServletRequest}
     * @return {@link Cookie} if present, else return <code>null</code>
     */
    public Cookie getSamlCookie(final HttpServletRequest request) {
        final String cookieName = configurationService.getConfiguration().getString(SSO_COOKIE_NAME);
        return cookieName != null ? WebUtils.getCookie(request, cookieName) : null;
    }
}
