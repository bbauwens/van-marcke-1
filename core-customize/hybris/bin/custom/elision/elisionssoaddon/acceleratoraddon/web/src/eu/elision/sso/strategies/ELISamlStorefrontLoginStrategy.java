package eu.elision.sso.strategies;

import de.hybris.platform.servicelayer.security.spring.HybrisSessionFixationProtectionStrategy;
import eu.elision.sso.security.ELISamlAuthenticationSuccessHandler;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Custom Login Strategy for the Storefront
 */
public class ELISamlStorefrontLoginStrategy {

    private UserDetailsService userDetailsService;
    private HybrisSessionFixationProtectionStrategy sessionFixationStrategy;
    private ELISamlAuthenticationSuccessHandler eliSamlAuthenticationSuccessHandler;

    public ELISamlStorefrontLoginStrategy(UserDetailsService userDetailsService, HybrisSessionFixationProtectionStrategy sessionFixationStrategy,
                                          ELISamlAuthenticationSuccessHandler eliSamlAuthenticationSuccessHandler) {
        this.userDetailsService = userDetailsService;
        this.sessionFixationStrategy = sessionFixationStrategy;
        this.eliSamlAuthenticationSuccessHandler = eliSamlAuthenticationSuccessHandler;
    }

    /**
     * Login the {@link de.hybris.platform.core.model.user.UserModel} to the Storefront
     *
     * @param username uid of the {@link de.hybris.platform.core.model.user.UserModel}
     * @param request  {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     */
    public void login(final String username, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, "", userDetails.getAuthorities());
        token.setDetails(new WebAuthenticationDetails(request));
        SecurityContextHolder.getContext().setAuthentication(token);
        sessionFixationStrategy.onAuthentication(token, request, response);
        eliSamlAuthenticationSuccessHandler.onAuthenticationSuccess(request, response, token);
    }
}
