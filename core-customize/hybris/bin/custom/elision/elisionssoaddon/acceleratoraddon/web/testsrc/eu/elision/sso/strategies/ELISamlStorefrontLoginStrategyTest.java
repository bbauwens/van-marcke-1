package eu.elision.sso.strategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.security.spring.HybrisSessionFixationProtectionStrategy;
import eu.elision.sso.security.ELISamlAuthenticationSuccessHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ELISamlStorefrontLoginStrategyTest {

    @Mock
    private UserDetailsService userDetailsService;
    @Mock
    private HybrisSessionFixationProtectionStrategy sessionFixationStrategy;
    @Mock
    private ELISamlAuthenticationSuccessHandler eliSamlAuthenticationSuccessHandler;

    @InjectMocks
    private ELISamlStorefrontLoginStrategy eliSamlStorefrontLoginStrategy;

    @Test
    public void testLogin() throws IOException, ServletException {
        String userName = "test@test.be";
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
        UserDetails userDetails = mock(UserDetails.class);

        when(userDetailsService.loadUserByUsername(userName)).thenReturn(userDetails);

        eliSamlStorefrontLoginStrategy.login(userName, httpServletRequest, httpServletResponse);

        verify(sessionFixationStrategy).onAuthentication(any(UsernamePasswordAuthenticationToken.class), eq(httpServletRequest), eq(httpServletResponse));
        verify(eliSamlAuthenticationSuccessHandler).onAuthenticationSuccess(eq(httpServletRequest), eq(httpServletResponse), any(UsernamePasswordAuthenticationToken.class));
    }
}