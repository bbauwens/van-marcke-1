package eu.elision.sso.filters;

import de.hybris.platform.assistedservicestorefront.filter.AssistedServiceFilter;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.jalo.user.CookieBasedLoginToken;
import de.hybris.platform.jalo.user.LoginToken;
import de.hybris.platform.servicelayer.user.UserService;
import eu.elision.sso.utils.ELISamlStorefrontUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Custom {@link AssistedServiceFilter} in order to override the {@link AssistedServiceFilter#shouldNotFilter(HttpServletRequest)}
 */
public class ELIAssistedServiceFilter extends AssistedServiceFilter {

    private UserService userService;
    private ELISamlStorefrontUtils eliSamlStorefrontUtils;

    public ELIAssistedServiceFilter(UserService userService, ELISamlStorefrontUtils eliSamlStorefrontUtils) {
        this.userService = userService;
        this.eliSamlStorefrontUtils = eliSamlStorefrontUtils;
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        Cookie cookie = eliSamlStorefrontUtils.getSamlCookie(request);
        if (cookie != null) {
            LoginToken token = new CookieBasedLoginToken(cookie);
            return !(userService.getUserForUID(token.getUser().getUid()) instanceof EmployeeModel);
        }
        return true;
    }
}
