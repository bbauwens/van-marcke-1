# Integration Extension

The goal of this extension is to make integrations easier. This should hide all boilerplate code, so you can just focus on the important stuff.
REST Calls are wrapped in a HystrixCommand, this makes it possible to monitor the calls and setup dashboards if needed/wanted.

## Usage

Following code snippets are just examples on how to use this extension.
The idea is that this code ends up in you project specific extensions, so this extension stays clean to re-use in other projects as well.

### Spring configuration (In your own extensions)

```xml
    <alias name="myCommandExecutorFactory" alias="commandExecutorFactory"/>
    <bean name="myCommandExecutorFactory" class="com.my.integrations.MyCommandExecutorFactory"/>

    <!-- Commands -->
    <bean id="stockCommandExecutor" factory-method="getInstance" factory-bean="commandExecutorFactory">
        <constructor-arg name="serviceName" value="stockInformationService"/>
    </bean>

    <!-- Services -->
    <bean id="esbStockService" class="com.my.integrations.esb.services.impl.ESBStockServiceImpl">
        <property name="commandExecutorFactory" ref="commandExecutorFactory"/>
    </bean>
```

### Property files 
```properties
# Base URL for the service (domain name)
service.stockInformationService.baseurl=https://dummy.be
# Do you want to use the 'mock' implementation for this service (is useful when the backend is not yet ready)
service.stockInformationService.mockEnabled=true
```

### Java Command Class

Implement the Command class, this will configure the 'call' to the external service.

```java
package com.my.integrations.esb.commands;

import com.my.integrations.esb.data.stock.StockRequestData;
import com.my.integrations.esb.data.stock.StockResponseData;
import eu.elision.integration.command.AbstractRESTCommand;
import eu.elision.integration.command.configuration.AuthenticationType;
import eu.elision.integration.command.configuration.annotation.CommandConfig;
import org.springframework.http.HttpMethod;

/**
 * Command which gets the StockInformation from the backend (via the ESB)
 */
@CommandConfig(serviceName = "stockInformationService", authenticationType = AuthenticationType.NONE, httpMethod = HttpMethod.POST, url = "/stock")
public class GetStockInformationCommand extends AbstractRESTCommand<StockResponseData> {

    /**
     * Constructor which sets the stock request object to the payload.
     */
    public GetStockInformationCommand(StockRequestData stockRequest) {
        setPayLoad(stockRequest);
    }
}
```

### Implementing your own version of the CommandExecutorFactory (required!)

This is where you can define which commandExecutors should be returned. In this example, you can see that we have a mock implementation for the StockCommand.
This mock implementation will only be returned if mocking for that service is enabled.

```java
package com.my.integrations;

import com.my.integrations.esb.executors.MockStockCommandExecutor;
import de.hybris.platform.util.Config;
import eu.elision.integration.command.executer.impl.DefaultCommandExecutorFactory;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.executer.impl.CommandExecutorImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation, to be able to return VanMarcke specific commandExecutors
 */
public class MyCommandExecutorFactory extends DefaultCommandExecutorFactory {
    private String MOCK_ENABLED_PROPERTY = "service.%s.mockEnabled";
    private String BASE_URL_PROPERTY = "service.%s.baseurl";
    Map<String, CommandExecutor> executorMap = new HashMap<>();

    @Override
    public CommandExecutor getInstance(String serviceName) {
        boolean mockEnabled = Config.getBoolean(String.format(MOCK_ENABLED_PROPERTY, serviceName), false);
        String baseUrl = Config.getString(String.format(BASE_URL_PROPERTY, serviceName), null);
        String key = getKey(serviceName, mockEnabled);

        if(executorMap.containsKey(key)) {
            return executorMap.get(key);
        }

        CommandExecutor executor = null;
        if (mockEnabled) {
            if ("stockInformationService".equals(serviceName)) {
                executor = new MockStockCommandExecutor();
            }

        }

        if(executor == null) {
            executor = new CommandExecutorImpl(baseUrl);
        }

        executorMap.put(key, executor);
        return executor;
    }

    private String getKey(String serviceName, boolean mockEnabled) {
        return serviceName + "_" + mockEnabled;
    }
}
```

### Using the command class in your services...

```java
package com.my.integrations.esb.services.impl;

import com.my.integrations.esb.commands.GetStockInformationCommand;
import com.my.integrations.esb.services.ESBStockService;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import com.my.integrations.esb.data.stock.StockRequestData;
import com.my.integrations.esb.data.stock.StockResponseData;
import eu.elision.integration.command.executer.CommandExecutorFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * Service which handles stock related communication with the ESB.
 */
public class ESBStockServiceImpl implements ESBStockService {
    private CommandExecutorFactory commandExecutorFactory;

    @Override
    public StockResponseData getStockInformation(StockRequestData stockRequest) {
        GetStockInformationCommand command = new GetStockInformationCommand(stockRequest);
        Response<StockResponseData> result = commandExecutorFactory.getInstance(command.getServiceName()).executeCommand(command, null);
        return result.getPayLoad();
    }

    @Required
    public void setCommandExecutorFactory(CommandExecutorFactory commandExecutorFactory) {
        this.commandExecutorFactory = commandExecutorFactory;
    }
}

```
