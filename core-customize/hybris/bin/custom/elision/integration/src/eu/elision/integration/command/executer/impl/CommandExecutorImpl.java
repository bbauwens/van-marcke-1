package eu.elision.integration.command.executer.impl;

import eu.elision.integration.command.Command;
import eu.elision.integration.command.Response;
import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.executer.CommandExecutorCallback;

import java.util.Map;

import static org.apache.commons.collections4.MapUtils.getString;
import static org.springframework.util.Assert.hasLength;

public class CommandExecutorImpl implements CommandExecutor {

    private final Map<String, Object> properties;

    public CommandExecutorImpl(final Map<String, Object> properties) {
        hasLength(getString(properties, BASE_URL), "base url cannot be null");

        this.properties = properties;
    }

    @Override
    public <T> Response<T> executeCommand(final Command<T> command, final CommandExecutorCallback<T> commandExecutorCallback) {
        Response<T> response = command.execute(properties);

        if (commandExecutorCallback != null) {
            commandExecutorCallback.execute(response);
        }

        return response;
    }
}