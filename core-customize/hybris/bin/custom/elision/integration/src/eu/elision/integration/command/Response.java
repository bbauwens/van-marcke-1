package eu.elision.integration.command;

import org.springframework.http.HttpStatus;

/**
 * Response that is returned after executing a Command
 *
 * @param <T> type of the response
 */
public interface Response<T> {

    /**
     * Getter that returns the payload of type T
     *
     * @return T the payload to return
     */
    T getPayLoad();

    /**
     * Settter to set the payload of type T
     *
     * @param t the payload to set
     */
    void setPayLoad(T t);

    /**
     * Getter that returns the Http status
     *
     * @return HttpStatus the HttpStatus to return
     */
    HttpStatus getHttpStatus();

    /**
     * Setter to set the Http status
     *
     * @param httpStatus the HttpStatus to set
     */
    void setHttpStatus(HttpStatus httpStatus);

    /**
     * Getter that returns the error message
     *
     * @return String the errorMessage to return
     */
    String getErrorMessage();

    /**
     * Setter to set the error message
     *
     * @param errorMessage the error message
     */
    void setErrorMessage(String errorMessage);

}