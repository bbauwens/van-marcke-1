package eu.elision.integration.setup;

import de.hybris.platform.core.initialization.SystemSetup;
import eu.elision.integration.constants.IntegrationConstants;

@SystemSetup(extension = IntegrationConstants.EXTENSIONNAME)
public class IntegrationSystemSetup {

}