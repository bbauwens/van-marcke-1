package eu.elision.integration.command.executer.impl;


import eu.elision.integration.command.executer.CommandExecutor;
import eu.elision.integration.command.executer.CommandExecutorFactory;
import org.apache.commons.lang3.NotImplementedException;

/**
 * Factory to return the correct instance of the executor.
 * This makes it possible to return mock objects if needed.
 *
 * This is made abstract, because you will have to forsee the implementation yourself (we don't know which commands to return :) )
 *
 * @author vangeda
 */
public class DefaultCommandExecutorFactory implements CommandExecutorFactory {

    /**
     * Get the instance of the correct CommandExecutor
     *
     * @param serviceName name of the Service
     * @return command executor instance
     */
    public CommandExecutor getInstance(String serviceName) {
        throw new NotImplementedException("CommandExecutorFactory method not implemented!");
    }
}
