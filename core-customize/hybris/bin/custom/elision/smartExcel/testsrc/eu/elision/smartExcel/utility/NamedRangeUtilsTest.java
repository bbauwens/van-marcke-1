package eu.elision.smartExcel.utility;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Elision
 */
@RunWith(MockitoJUnitRunner.class)
public class NamedRangeUtilsTest {

    @Test
    public void testGetValidNamedRangeName() {
        assertThat(NamedRangeUtils.getValidNamedRangeName("abcXYZ123._)àç!è§(é^^$")).isEqualTo("abcXYZ123.____________");
        assertThat(NamedRangeUtils.getValidNamedRangeName("TestName")).isEqualTo("TestName");
        assertThat(NamedRangeUtils.getValidNamedRangeName("Test Name")).isEqualTo("Test_Name");
        assertThat(NamedRangeUtils.getValidNamedRangeName("")).isEqualTo("");
        assertThat(NamedRangeUtils.getValidNamedRangeName(null)).isEqualTo("");
    }
}
