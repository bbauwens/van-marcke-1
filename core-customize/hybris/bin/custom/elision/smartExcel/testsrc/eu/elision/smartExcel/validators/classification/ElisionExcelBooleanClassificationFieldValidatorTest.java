package eu.elision.smartExcel.validators.classification;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.validators.data.ExcelValidationResult;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import java.util.HashMap;
import java.util.Map;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * @author Elision
 */
@RunWith(MockitoJUnitRunner.class)
public class ElisionExcelBooleanClassificationFieldValidatorTest {
    private static final HashMap<String, Object> ANY_CONTEXT = new HashMap();
    private final ElisionExcelBooleanClassificationFieldValidator validator = new ElisionExcelBooleanClassificationFieldValidator();

    public ElisionExcelBooleanClassificationFieldValidatorTest() {
    }

    @Test
    public void shouldValidatorBeInvokedWhenInputIsOfTypeBoolean() {
        ImportParameters importParameters = new ImportParameters(null, null, "some value", null, Lists.newArrayList(new Map[]{new HashMap()}));
        boolean output = this.validator.canHandle(this.mockExcelAttribute(), importParameters);
        Assert.assertTrue(output);
    }

    @Test
    public void shouldValidatorNotBeHandledWhenInputIsRangeOfBooleans() {
        ImportParameters importParameters = new ImportParameters(null, null, "RANGE[TRUE;FALSE]", null, Lists.newArrayList(new Map[]{new HashMap()}));
        boolean output = this.validator.canHandle(this.mockExcelAttribute(), importParameters);
        Assert.assertFalse(output);
    }

    @Test
    public void shouldValidatorNotBeHandledWhenInputIsMultivaluedBoolean() {
        ImportParameters importParameters = new ImportParameters(null, null, "TRUE,FALSE", null, Lists.newArrayList(new Map[]{new HashMap()}));
        boolean output = this.validator.canHandle(this.mockExcelAttribute(), importParameters);
        Assert.assertFalse(output);
    }

    @Test
    public void shouldValidationSucceedWhenInputIsBoolean() {
        ImportParameters importParameters = new ImportParameters(null, null, "TRUE", null, Lists.newArrayList(new Map[]{new HashMap()}));
        ExcelValidationResult result = this.validator.validate(this.mockExcelAttribute(), importParameters, ANY_CONTEXT);
        Assert.assertFalse(result.hasErrors());
    }

    @Test
    public void shouldValidationSucceedWhenInputIsNA() {
        ImportParameters importParameters = new ImportParameters(null, null, "N/A", null, Lists.newArrayList(new Map[]{new HashMap()}));
        ExcelValidationResult result = this.validator.validate(this.mockExcelAttribute(), importParameters, ANY_CONTEXT);
        Assert.assertFalse(result.hasErrors());
    }

    @Test
    public void shouldValidationFailWhenInputIsNotBoolean() {
        ImportParameters importParameters = new ImportParameters(null, null, "blabla", null, Lists.newArrayList(new Map[]{new HashMap()}));
        ExcelValidationResult result = this.validator.validate(this.mockExcelAttribute(), importParameters, ANY_CONTEXT);
        Assert.assertTrue(result.hasErrors());
    }

    private ExcelClassificationAttribute mockExcelAttribute() {
        ExcelClassificationAttribute attribute = Mockito.mock(ExcelClassificationAttribute.class);
        ClassAttributeAssignmentModel assignmentModel = Mockito.mock(ClassAttributeAssignmentModel.class);
        BDDMockito.given(assignmentModel.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.BOOLEAN);
        BDDMockito.given(attribute.getAttributeAssignment()).willReturn(assignmentModel);
        return attribute;
    }
}