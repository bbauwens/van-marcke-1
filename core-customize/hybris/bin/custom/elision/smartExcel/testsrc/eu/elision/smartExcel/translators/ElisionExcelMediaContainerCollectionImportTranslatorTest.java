package eu.elision.smartExcel.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.CollectionTypeModel;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.servicelayer.type.TypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ElisionExcelMediaContainerCollectionImportTranslatorTest {

    @Mock
    ElisionExcelMediaContainerImportTranslator elisionExcelMediaContainerImportTranslator;

    @Mock
    TypeService typeService;

    @Spy
    @InjectMocks
    ElisionExcelMediaContainerCollectionImportTranslator elisionExcelMediaContainerCollectionImportTranslator;

    @Test
    public void testCanHandle() {
        // given
        AttributeDescriptorModel attributeDescriptorModel = mock(AttributeDescriptorModel.class);
        CollectionTypeModel collectionTypeModel = mock(CollectionTypeModel.class);
        TypeModel typeModel  = mock(TypeModel.class);
        when(attributeDescriptorModel.getAttributeType()).thenReturn(collectionTypeModel);
        when(collectionTypeModel.getElementType()).thenReturn(typeModel);
        when(typeModel.getCode()).thenReturn("typeCode");
        doReturn(typeService).when(elisionExcelMediaContainerCollectionImportTranslator).getTypeService();

        //when
        elisionExcelMediaContainerCollectionImportTranslator.canHandle(attributeDescriptorModel);

        // then
        verify(typeService).isAssignableFrom("typeCode", "MediaContainer" );
    }

    @Test
    public void testReferenceFormat() {
        AttributeDescriptorModel attributeDescriptorModel = mock(AttributeDescriptorModel.class);
        elisionExcelMediaContainerImportTranslator.referenceFormat(attributeDescriptorModel);
        verify(elisionExcelMediaContainerImportTranslator).referenceFormat(attributeDescriptorModel);
    }

    @Test
    public void testExportData() {
        // given
        String exportedMedia1 = "http://media1.jpg";
        String exportedMedia2 = "http://media2.jpg";

        MediaContainerModel mediaContainerModel1 = mock(MediaContainerModel.class);
        MediaContainerModel mediaContainerModel2 = mock(MediaContainerModel.class);
        when(elisionExcelMediaContainerImportTranslator.exportMedia(mediaContainerModel1)).thenReturn(Optional.of(exportedMedia1));
        when(elisionExcelMediaContainerImportTranslator.exportMedia(mediaContainerModel2)).thenReturn(Optional.of(exportedMedia2));

        // when
        Optional<Object> result = elisionExcelMediaContainerCollectionImportTranslator.exportData(List.of(mediaContainerModel1, mediaContainerModel2));

        // then
        assertThat(result.isPresent());
        assertThat(result.get()).isEqualTo(String.join(",", exportedMedia1, exportedMedia2));
    }
}