package eu.elision.smartExcel.translators.classification;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.translators.classification.ExcelClassificationEnumTypeTranslator;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Custom implementation of the ExcelClassificationEnumTypeTranslator, to be able to override some functionality
 *
 * @author Elision
 */
public class ElisionExcelClassificationEnumTypeTranslator extends ExcelClassificationEnumTypeTranslator implements ElisionExcelAttributeTranslator<ExcelClassificationAttribute> {

    private static Optional<String> extractClassificationAttributeValueCode(Object value) {
        return Optional.ofNullable(value).filter(ClassificationAttributeValueModel.class::isInstance).map(ClassificationAttributeValueModel.class::cast).map(ClassificationAttributeValueModel::getName);
    }

    private static Optional<String> extractHybrisEnumValueName(Object value) {
        return Optional.ofNullable(value).filter(HybrisEnumValue.class::isInstance).map(HybrisEnumValue.class::cast).map(HybrisEnumValue::getCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canHandleNamedRanges() {
        return true;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Adapted this code to catch exceptions and to handle 'null' features
     */
    public Optional<String> exportData(@Nonnull ExcelClassificationAttribute excelAttribute, @Nonnull Object objectToExport) {
        if (objectToExport instanceof ProductModel) {
            Feature feature = this.getClassificationService().getFeature((ProductModel) objectToExport, excelAttribute.getAttributeAssignment());
            if (feature != null) {
                return Optional.ofNullable((String) this.getStreamOfValuesToJoin(excelAttribute, feature).collect(Collectors.joining(",")));
            }
        }
        return Optional.empty();
    }

    /**
     * {@inheritDoc}
     */
    public Optional<String> exportSingle(@Nonnull ExcelClassificationAttribute excelAttribute, @Nonnull FeatureValue featureToExport) {
        Object featureValue = featureToExport.getValue();
        return Stream.of(extractHybrisEnumValueName(featureValue), this.extractClassificationAttributeValueCode(featureValue)).filter(Optional::isPresent).map(Optional::get).findFirst();
    }

}
