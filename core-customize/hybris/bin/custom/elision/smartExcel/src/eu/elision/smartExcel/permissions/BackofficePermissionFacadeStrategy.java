package eu.elision.smartExcel.permissions;

/**
 * This is a copy of all code we need from the DefaultPlatformPermissionFacadeStrategy. We needed to copy this because the original sources are only available in the webcontext.
 *
 * @author Elision
 */
public interface BackofficePermissionFacadeStrategy {

    /**
     * Check if property can be changed
     *
     * @param typeCode the typeCode
     * @param property the property which we want to check
     * @return boolean indicating whether property can be changed
     */
    boolean canChangeProperty(String typeCode, String property);

}
