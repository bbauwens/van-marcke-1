package eu.elision.smartExcel.extractor;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.template.populator.extractor.ClassificationFullNameExtractor;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import org.apache.commons.lang.text.StrSubstitutor;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Custom Extractor in order to overwrite {@link ClassificationFullNameExtractor#extract(ExcelClassificationAttribute)}
 */
public class ElisionClassificationFullNameExtractor extends ClassificationFullNameExtractor {

    private static String getClassificationAttributeCode(ExcelClassificationAttribute excelAttribute) {
        return Optional.of(excelAttribute)
                .map(ExcelClassificationAttribute::getAttributeAssignment)
                .map(ClassAttributeAssignmentModel::getClassificationAttribute)
                .map(ClassificationAttributeModel::getCode)
                .orElse("");
    }

    private static String getClassificationAttributeName(ExcelClassificationAttribute excelAttribute) {
        return Optional.of(excelAttribute)
                .map(ExcelClassificationAttribute::getAttributeAssignment)
                .map(ClassAttributeAssignmentModel::getClassificationAttribute)
                .map(ClassificationAttributeModel::getName)
                .orElse("");
    }

    @Override
    public String extract(ExcelClassificationAttribute excelAttribute) {
        Map<String, String> params = new HashMap<>();
        params.put("attribute", getClassificationAttributeCode(excelAttribute));
        params.put("name", getClassificationAttributeName(excelAttribute));

        return (new StrSubstitutor(params)).replace("${attribute}.${name}");
    }
}
