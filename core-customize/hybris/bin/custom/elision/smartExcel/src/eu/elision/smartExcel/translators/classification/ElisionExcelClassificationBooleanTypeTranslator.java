package eu.elision.smartExcel.translators.classification;

import com.hybris.backoffice.excel.data.ExcelClassificationAttribute;
import com.hybris.backoffice.excel.data.ImpexHeaderValue;
import com.hybris.backoffice.excel.data.ImpexValue;
import com.hybris.backoffice.excel.data.ImportParameters;
import com.hybris.backoffice.excel.importing.ExcelImportContext;
import com.hybris.backoffice.excel.translators.classification.ExcelClassificationJavaTypeTranslator;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.elision.smartExcel.constants.SmartExcelConstants.Types.BOOLEAN_NOT_AVAILABLE;

/**
 * Custom Translator to be able to render 'dropdowns' for booleans.
 *
 * @author Elision
 */
public class ElisionExcelClassificationBooleanTypeTranslator extends ExcelClassificationJavaTypeTranslator implements ElisionExcelAttributeTranslator<ExcelClassificationAttribute> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> exportData(@Nonnull ExcelClassificationAttribute excelAttribute, @Nonnull Object objectToExport) {
        if (objectToExport instanceof ProductModel) {
            Feature feature = this.getClassificationService().getFeature((ProductModel) objectToExport, excelAttribute.getAttributeAssignment());
            if (feature != null) {
                return Optional.ofNullable(this.getStreamOfValuesToJoin(excelAttribute, feature).collect(Collectors.joining(",")));
            } else {
                return Optional.ofNullable(BOOLEAN_NOT_AVAILABLE);
            }
        }
        return Optional.empty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ImpexValue importSimple(ExcelClassificationAttribute excelAttribute, ImportParameters importParameters, ExcelImportContext excelImportContext) {
        Serializable value = importParameters.getCellValue().equals(BOOLEAN_NOT_AVAILABLE) ? "" : importParameters.getCellValue();
        String headerValueName = this.getClassificationAttributeHeaderValueCreator().create(excelAttribute, excelImportContext);
        return new ImpexValue(value, (new ImpexHeaderValue.Builder(headerValueName)).withLang(importParameters.getIsoCode()).withQualifier(excelAttribute.getQualifier()).build());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canHandleAttribute(@Nonnull ExcelClassificationAttribute excelClassificationAttribute) {
        return excelClassificationAttribute.getAttributeAssignment().getAttributeType() == ClassificationAttributeTypeEnum.BOOLEAN;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canHandleNamedRanges() {
        return true;
    }
}
