package eu.elision.smartExcel.permissions.impl;

import com.google.common.collect.Maps;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.security.permissions.PermissionCRUDService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import eu.elision.smartExcel.permissions.BackofficePermissionFacadeStrategy;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

/**
 * This is a copy of all code we need from the DefaultPlatformPermissionFacadeStrategy. We needed to copy this because the original sources are only available in the webcontext.
 *
 * @author Elision
 */
public class ExcelBackofficePermissionFacadeStrategy implements BackofficePermissionFacadeStrategy {
    private PermissionCRUDService permissionCRUDService;
    private final Map<String, Boolean> permissionAwareTypeCache = Maps.newHashMap();
    private TypeService typeService;
    private UserService userService;

    /**
     * {@inheritDoc}
     */
    public boolean canChangeProperty(String typeCode, String property) {
        String cacheKey = userService.getCurrentUser().getUid() + typeCode + property;
        Boolean result = permissionAwareTypeCache.get(cacheKey);
        if (result == null) {
            if (!this.isPermissionAwareType(typeCode)) {
                result = true;
            } else {
                try {
                    //permissions are inherited, so we can just check attribute level...
                    result = permissionCRUDService.canChangeAttribute(typeCode, property);
                } catch (UnknownIdentifierException var5) {
                    //attribute not found, return false... (cell will be locked)
                    result = false;
                }
            }
            permissionAwareTypeCache.put(cacheKey, result);
        }
        return result;
    }

    private boolean isPermissionAwareType(String typeCode) {
        Boolean result = permissionAwareTypeCache.get(userService.getCurrentUser().getUid() + typeCode);
        if (result == null) {
            try {
                result = typeService.getTypeForCode(typeCode) instanceof ComposedTypeModel;
            } catch (UnknownIdentifierException var4) {
                result = Boolean.FALSE;
            }

            permissionAwareTypeCache.put(userService.getCurrentUser().getUid() + typeCode, result);
        }

        return result;
    }

    @Required
    public void setPermissionCRUDService(PermissionCRUDService permissionCRUDService) {
        this.permissionCRUDService = permissionCRUDService;
    }

    @Required
    public void setTypeService(TypeService typeService) {
        this.typeService = typeService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
