package eu.elision.smartExcel.utility;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Utility class to handle named ranges in the excel sheet
 *
 * @author Elision
 */
public class NamedRangeUtils {
    private static final Logger LOG = LoggerFactory.getLogger(NamedRangeUtils.class);

    /**
     * Creates a named range for the given classification attribute
     *
     * @param workbook            the workbook
     * @param valuesSheet         the sheet on which we place the references
     * @param attributesAndValues the map with values filtered by attribute
     * @param rowIndex            the current row index
     * @param cellIndex           the current cell index
     * @param columnIndex         the current column index
     */
    public static void createNamedRange(final Workbook workbook, final XSSFSheet valuesSheet, final Map<String, Set<String>> attributesAndValues, final AtomicInteger rowIndex, final AtomicInteger cellIndex, final AtomicInteger columnIndex) {
        for (final Map.Entry<String, Set<String>> entry : attributesAndValues.entrySet()) {
            final String key = entry.getKey();
            final Set<String> values = entry.getValue();

            if (CollectionUtils.isNotEmpty(values)) {
                {
                    values.forEach(value -> {
                        final XSSFRow currentRow = valuesSheet.getRow(rowIndex.get());
                        if (null == currentRow) {
                            final XSSFCell cell = valuesSheet.createRow(rowIndex.get()).createCell(cellIndex.get());
                            cell.setCellValue(value);
                        } else {
                            final XSSFCell cell = currentRow.createCell(cellIndex.get());
                            cell.setCellValue(value);
                        }
                        rowIndex.incrementAndGet();
                    });
                }

                //CREATE NAMED RANGES
                final Name namedCell = workbook.createName();
                namedCell.setNameName(getValidNamedRangeName(key));
                final String column = CellReference.convertNumToColString(columnIndex.get());
                final String reference = new StringBuilder().append(valuesSheet.getSheetName()).append("!$").append(column).append("$1:$").append(column).append("$").append(rowIndex.get()).toString(); // area reference
                namedCell.setRefersToFormula(reference);

                cellIndex.incrementAndGet();
                columnIndex.incrementAndGet();
                rowIndex.set(0);
                LOG.debug("Created named range with name {}", namedCell.getNameName());
            }
        }
    }

    /**
     * Replace characters which are not allowed by Excel for Named Ranges.
     *
     * @param originalName the original name to be used for the named range
     * @return the name as it will be used for the named range
     */
    public static String getValidNamedRangeName(final String originalName) {
        if (originalName != null) {
            final String result = originalName.replaceAll("[^a-zA-Z0-9\\.\\_]", "_");
            return Character.isDigit(result.charAt(0)) ? String.format("_%s", result) : result;
        }
        return "";
    }

    /**
     * Adds a dropdown list to the enum cell. The values of the dropdown list contain all the possible enum values for a
     * single attribute.
     *
     * @param sheet          the sheet
     * @param namedRangeName the code of the attribute
     * @param targetCell     the cell that needs a dropdown list
     */
    public static void setCellDropDownList(final Sheet sheet, final String namedRangeName, final Cell targetCell) {
        final XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
        final String classificationAttributeCode = NamedRangeUtils.getValidNamedRangeName(namedRangeName);
        final XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createFormulaListConstraint(classificationAttributeCode);
        final CellRangeAddressList addressList = new CellRangeAddressList(targetCell.getRowIndex(), targetCell.getRowIndex(), targetCell.getColumnIndex(), targetCell.getColumnIndex());
        final XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setShowErrorBox(true);
        sheet.addValidationData(validation);
    }

}
