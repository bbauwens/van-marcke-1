package eu.elision.smartExcel.template.populator.typesheet;

import com.hybris.backoffice.excel.data.ExcelAttributeDescriptorAttribute;
import com.hybris.backoffice.excel.template.AttributeNameFormatter;
import com.hybris.backoffice.excel.template.CollectionFormatter;
import com.hybris.backoffice.excel.template.filter.ExcelFilter;
import com.hybris.backoffice.excel.template.populator.DefaultExcelAttributeContext;
import com.hybris.backoffice.excel.translators.ExcelTranslatorRegistry;
import de.hybris.platform.core.model.c2l.C2LItemModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import eu.elision.smartExcel.permissions.BackofficePermissionFacadeStrategy;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Custom implementation of the TypeSystemRowFactory to be able to skip some fields in the validation.
 * Cells which are locked are not writeable for the user...
 *
 * @author Elision
 */
public class ElisionTypeSystemRowFactory {
    private CommonI18NService commonI18NService;
    private ExcelFilter<AttributeDescriptorModel> excelUniqueFilter;
    private ExcelTranslatorRegistry excelTranslatorRegistry;
    private CollectionFormatter collectionFormatter;
    private AttributeNameFormatter<ExcelAttributeDescriptorAttribute> attributeNameFormatter;
    private BackofficePermissionFacadeStrategy permissionFacadeStrategy;

    public ElisionTypeSystemRowFactory() {
    }

    public ElisionTypeSystemRow create(@Nonnull AttributeDescriptorModel attributeDescriptor) {
        String attributeDisplayName = this.getAttributeDisplayName(attributeDescriptor);
        ElisionTypeSystemRow created = new ElisionTypeSystemRow();
        created.setTypeCode(this.collectionFormatter.formatToString(attributeDescriptor.getEnclosingType().getCode()));
        created.setTypeName(attributeDescriptor.getEnclosingType().getName());
        created.setAttrQualifier(attributeDescriptor.getQualifier());
        created.setAttrName(attributeDescriptor.getName());
        created.setAttrOptional(attributeDescriptor.getOptional());
        created.setAttrTypeCode(attributeDescriptor.getAttributeType().getCode());
        created.setAttrTypeItemType(attributeDescriptor.getDeclaringEnclosingType().getCode());
        created.setAttrLocalized(attributeDescriptor.getLocalized());
        created.setAttrLocLang(this.getAttrLocLang(attributeDescriptor.getLocalized()));
        created.setAttrDisplayName(attributeDisplayName);
        created.setAttrUnique(this.excelUniqueFilter.test(attributeDescriptor));
        created.setAttrReferenceFormat(this.getReferenceFormat(attributeDescriptor));
        created.setLocked(!permissionFacadeStrategy.canChangeProperty(attributeDescriptor.getEnclosingType().getCode(), attributeDescriptor.getQualifier()));
        return created;
    }

    private String getAttributeDisplayName(AttributeDescriptorModel attributeDescriptor) {
        Collection<String> attributeNames =
                this.commonI18NService.getAllLanguages().stream()
                        .filter(C2LItemModel::getActive)
                        .map(C2LItemModel::getIsocode)
                        .map((lang) -> this.attributeNameFormatter.format(DefaultExcelAttributeContext.ofExcelAttribute(new ExcelAttributeDescriptorAttribute(attributeDescriptor
                                , lang))))
                        .collect(Collectors.toList());
        return this.collectionFormatter.formatToString(attributeNames);
    }

    private String getAttrLocLang(boolean localized) {
        if (!localized) {
            return "";
        } else {
            List<String> isoCodes = this.commonI18NService.getAllLanguages().stream().filter(C2LItemModel::getActive).map(C2LItemModel::getIsocode).collect(Collectors.toList());
            return this.collectionFormatter.formatToString(isoCodes);
        }
    }

    private String getReferenceFormat(AttributeDescriptorModel attributeDescriptor) {
        return this.excelTranslatorRegistry.getTranslator(attributeDescriptor)
                .map((excelValueTranslator) -> excelValueTranslator.referenceFormat(attributeDescriptor)).orElse("");
    }

    public ElisionTypeSystemRow merge(@Nonnull ElisionTypeSystemRow first, @Nonnull ElisionTypeSystemRow second) {
        ElisionTypeSystemRow merged = new ElisionTypeSystemRow();
        BeanUtils.copyProperties(first, merged);
        if (!first.getTypeCode().contains(second.getTypeCode())) {
            merged.setTypeCode(String.join(",", first.getTypeCode(), second.getTypeCode()));
        }

        return merged;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    @Required
    public void setExcelUniqueFilter(ExcelFilter<AttributeDescriptorModel> excelUniqueFilter) {
        this.excelUniqueFilter = excelUniqueFilter;
    }

    @Required
    public void setExcelTranslatorRegistry(ExcelTranslatorRegistry excelTranslatorRegistry) {
        this.excelTranslatorRegistry = excelTranslatorRegistry;
    }

    @Required
    public void setCollectionFormatter(CollectionFormatter collectionFormatter) {
        this.collectionFormatter = collectionFormatter;
    }

    @Required
    public void setAttributeNameFormatter(AttributeNameFormatter<ExcelAttributeDescriptorAttribute> attributeNameFormatter) {
        this.attributeNameFormatter = attributeNameFormatter;
    }

    @Required
    public void setPermissionFacadeStrategy(BackofficePermissionFacadeStrategy platformPermissionFacadeStrategy) {
        this.permissionFacadeStrategy = platformPermissionFacadeStrategy;
    }
}
