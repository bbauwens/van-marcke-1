package eu.elision.smartExcel.constants;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class SmartExcelConstants extends GeneratedSmartExcelConstants {
    public static final String EXTENSIONNAME = "smartExcel";

    private SmartExcelConstants() {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
    public static enum UtilitySheet {
        CLASSIFICATION_TYPE_SYSTEM("ClassificationTypeSystem"),
        HEADER_PROMPT("HeaderPrompt"),
        PK("_PK"),
        TYPE_TEMPLATE("TypeTemplate"),
        TYPE_SYSTEM("TypeSystem"),
        VALUELISTS_TYPESYSTEM_SHEET("ValueLists_TypeSystem"),
        VALUELISTS_CLASSIFICATION_SHEET("ValueLists_Classification");

        private final String sheetName;

        private UtilitySheet(String sheetName) {
            this.sheetName = sheetName;
        }

        public static boolean isUtilitySheet(Collection<SmartExcelConstants.UtilitySheet> utilitySheets, String sheetName) {
            return utilitySheets.stream().map(SmartExcelConstants.UtilitySheet::getSheetName).anyMatch((utilitySheetName) -> {
                return StringUtils.equals(utilitySheetName, sheetName);
            });
        }

        public String getSheetName() {
            return this.sheetName;
        }
    }

    public static class NamedRanges {
        public static final String CLASSIFICATION_OPTION_NOT_APPLICABLE = "N/A";
    }

    public static class Types {
        public static final String BOOLEAN_TYPE = "java.lang.Boolean";
        public static final String BOOLEAN_NOT_AVAILABLE = "N/A";
        public static final String[] BOOLEAN_TYPE_VALUES = {BOOLEAN_NOT_AVAILABLE, "true", "false"};
        public static final String BOOLEAN_PRIMITIVE_TYPE = "boolean";
        public static final String[] BOOLEAN_PRIMITIVE_TYPE_VALUES = {"true", "false"};

    }

    public static enum TypeSystem {
        TYPE_CODE(0),
        TYPE_NAME(1),
        ATTR_QUALIFIER(2),
        ATTR_NAME(3),
        ATTR_OPTIONAL(4),
        ATTR_TYPE_CODE(5),
        ATTR_TYPE_ITEMTYPE(6),
        ATTR_LOCALIZED(7),
        ATTR_LOC_LANG(8),
        ATTR_DISPLAYED_NAME(9),
        ATTR_UNIQUE(10),
        REFERENCE_FORMAT(11),
        ATTR_LOCKED(12);

        private final int index;

        private TypeSystem(int index) {
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }
    }

}
