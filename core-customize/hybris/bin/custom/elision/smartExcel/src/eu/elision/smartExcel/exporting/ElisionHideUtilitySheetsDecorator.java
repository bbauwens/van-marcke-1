package eu.elision.smartExcel.exporting;

import com.hybris.backoffice.excel.data.ExcelExportResult;
import com.hybris.backoffice.excel.exporting.ExcelExportWorkbookDecorator;
import com.hybris.backoffice.excel.exporting.HideUtilitySheetsDecorator;
import de.hybris.platform.util.Config;
import eu.elision.smartExcel.constants.SmartExcelConstants.UtilitySheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;

/**
 * Custom version of the HideUtilitySheetsDecorator, this implementation makes it possible to add custom sheets to the list of sheets to hide.
 *
 * @author Elision
 */
public class ElisionHideUtilitySheetsDecorator extends HideUtilitySheetsDecorator implements ExcelExportWorkbookDecorator {

    private Collection<UtilitySheet> customUtilitySheets;

    /**
     * {@inheritDoc}
     */
    @Override
    public void decorate(ExcelExportResult excelExportResult) {
        if (Config.getBoolean("excel.sheet.hideSheets.enabled", true)) {
            this.customUtilitySheets.forEach((customUtilitySheet) ->
                    this.hideUtilitySheet(excelExportResult.getWorkbook(), customUtilitySheet.getSheetName())
            );
        }
    }

    /**
     * Activate the first non utility sheet. This loops over the list of utilitySheets to determine which sheet should be activated
     *
     * @param workbook the workbook
     */
    @Override
    protected void activateFirstNonUtilitySheet(Workbook workbook) {
        if (UtilitySheet.isUtilitySheet(customUtilitySheets, workbook.getSheetName(workbook.getActiveSheetIndex()))) {
            for (int i = 0; i < workbook.getNumberOfSheets(); ++i) {
                if (!UtilitySheet.isUtilitySheet(customUtilitySheets, workbook.getSheetName(i))) {
                    workbook.setActiveSheet(i);
                }
            }
        }
    }

    /**
     * set the utility sheets
     *
     * @param utilitySheets utility sheet collection
     */
    @Required
    public void setCustomUtilitySheets(Collection<UtilitySheet> utilitySheets) {
        this.customUtilitySheets = utilitySheets;
    }
}
