package eu.elision.smartExcel.template.mapper;

import com.hybris.backoffice.excel.data.ExcelExportResult;
import com.hybris.backoffice.excel.template.filter.ExcelFilter;
import com.hybris.backoffice.excel.template.mapper.ExcelMapper;
import com.hybris.backoffice.excel.template.mapper.ToAttributeDescriptorsMapper;
import com.hybris.backoffice.excel.template.sheet.ExcelSheetService;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import eu.elision.smartExcel.constants.SmartExcelConstants.UtilitySheet;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Custom version of the FromExcelResultToAttributeDescriptorMapper, to be able to use our own ExcludedSheets enum
 *
 * @author Elision
 */
public class FromExcelResultToAttributeDescriptorsMapper implements ToAttributeDescriptorsMapper<ExcelExportResult> {
    private ExcelMapper<String, AttributeDescriptorModel> mapper;
    private ExcelSheetService excelSheetService;
    private Collection<ExcelFilter<AttributeDescriptorModel>> filters;

    public FromExcelResultToAttributeDescriptorsMapper() {
        //empty constructor
    }

    /**
     * {@inheritDoc}
     */
    public Collection<AttributeDescriptorModel> apply(ExcelExportResult excelExportResult) {
        Predicate<String> isNotUtilitySheet = (sheetName) -> {
            return Stream.of(UtilitySheet.values()).map(UtilitySheet::getSheetName).noneMatch((utilitySheetName) -> {
                return StringUtils.equals(utilitySheetName, sheetName);
            });
        };
        return (Collection) IntStream.range(0, excelExportResult.getWorkbook().getNumberOfSheets()).mapToObj(excelExportResult.getWorkbook()::getSheetName).filter(isNotUtilitySheet).map((sheetName) -> {
            return this.excelSheetService.findTypeCodeForSheetName(excelExportResult.getWorkbook(), sheetName);
        }).map(this.mapper).flatMap(Collection::stream).distinct().filter((attribute) -> {
            return this.filter(attribute, this.filters);
        }).collect(Collectors.toList());
    }

    @Required
    public void setExcelSheetService(ExcelSheetService excelSheetService) {
        this.excelSheetService = excelSheetService;
    }

    @Required
    public void setMapper(ExcelMapper<String, AttributeDescriptorModel> mapper) {
        this.mapper = mapper;
    }

    public void setFilters(Collection<ExcelFilter<AttributeDescriptorModel>> filters) {
        this.filters = filters;
    }
}