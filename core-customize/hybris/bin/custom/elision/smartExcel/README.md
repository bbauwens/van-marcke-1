# Smart Excel Extension

## How excel cell locking works

To be able to lock cells in excel, you need to protect the sheet with a password.<br/>
During the export, we will check permissions for the logged in user to decide whether a cell needs to be locked.

The configuration of the permissions should be done on usergroup/user by configuring it on the itemtype. Configuration in the backoffice-config.xml will be ignored.
If you don't know how to do it, check the hybris help pages.

- [Type System - Type Permissions](https://help.hybris.com/1811/hcd/a971dfc0a3c04e668338b35e9f35aefc.html)
- [Type System - Attribute Permissions](https://help.hybris.com/1811/hcd/e5717d2f51e740989308f6d7e4ef35cc.html)

The locked cells will get a color (grey for locked, some kind of blue if not locked). These colors are not visible for the users, only if you unlock the sheet and remove all conditional formatting, you will see the colors.
However, these colors are not removed to be able to 'debug' the sheet in case things go wrong with the locking.

## Configuration

You can configure the way the excel should be locked after exporting. <br/>
Also the locking password can be configured.

### Available properties

backoffice.excel.import.max.rows=4000
backoffice.excel.export.max.rows=4000
backoffice.excel.export.max.attributes=2000

##### lock settings, if properties are not provided, they will be defaulted to false (no protection)

backoffice.excel.utility.sheets.hidden=true
excel.lock.sheet.enabled=true
excel.lock.sheet.autoFilter=false
excel.lock.sheet.deleteColumns=true
excel.lock.sheet.deleteRows=true
excel.lock.sheet.formatCells=true
excel.lock.sheet.formatColumns=true
excel.lock.sheet.formatRows=true
excel.lock.sheet.insertColumns=true
excel.lock.sheet.insertHyperlinks=true
excel.lock.sheet.insertRows=true
excel.lock.sheet.pivotTables=true
excel.lock.sheet.sort=false
excel.lock.sheet.objects=false
excel.lock.sheet.scenarios=true
excel.lock.sheet.selectLockedCells=true
excel.lock.sheet.selectUnlockedCells=false
excel.lock.workbook.structure=true

##### generated sheet will be password protected
excel.sheet.password=xxx
