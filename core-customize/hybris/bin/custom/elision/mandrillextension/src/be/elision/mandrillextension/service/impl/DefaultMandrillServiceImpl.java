package be.elision.mandrillextension.service.impl;

import be.elision.mandrillextension.converter.MandrillConverter;
import be.elision.mandrillextension.helper.service.MandrillHelperService;
import be.elision.mandrillextension.service.MandrillService;
import com.google.common.base.Preconditions;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.util.*;

import static java.util.stream.Collectors.toList;

/**
 * Default implementation of {@link MandrillService}.
 *
 * @author marteto
 */
public class DefaultMandrillServiceImpl implements MandrillService {

    private static final String HANDLEBARS = "handlebars";

    private MandrillApi mandrillApi;
    private MandrillApi mandrillTestApi;
    private MandrillHelperService mandrillHelperService;
    private ConfigurationService configurationService;

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> MandrillMessageStatus[] send(final String templateName, final T object, final MandrillConverter<T> converter, final String... email) throws IOException, MandrillApiError {
        return send(templateName, object, converter, email, null, null);
    }

    @Override
    public <T> MandrillMessageStatus[] send(final String templateName, final T object, final MandrillConverter<T> converter, final String[] email, final String[] cc) throws IOException, MandrillApiError {
        return send(templateName, object, converter, email, cc, null);
    }

    @Override
    public <T> MandrillMessageStatus[] send(final String templateName, final T object, final MandrillConverter<T> converter, final String[] email, final String[] cc, final String[] bcc) throws IOException, MandrillApiError {
        Preconditions.checkArgument(StringUtils.isNotBlank(templateName), "Template name can not be blank");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(email) && !containsNullValues(email), "email can not be empty");
        Preconditions.checkArgument(converter != null, "Converter can not be null");

        final MandrillMessage message = createMessage(email);
        setMergeVars(converter, object, message);
        addDefaultCc(message, templateName);
        addDefaultBcc(message, templateName);

        if (ArrayUtils.isNotEmpty(cc) && !containsNullValues(cc)) {
            addCc(message, cc);
        }

        if (ArrayUtils.isNotEmpty(bcc) && !containsNullValues(bcc)) {
            addBcc(message, bcc);
        }

        validateRecipients(message);

        if (isTestModeActive()) {
            return mandrillTestApi.messages().sendTemplate(templateName, new HashMap<>(1), message, false);
        }

        return mandrillApi.messages().sendTemplate(templateName, new HashMap<>(1), message, false);
    }

    private void validateRecipients(final MandrillMessage message) {
        final List<String> recipients = getSortedRecipients(message);
        if (message.getTo().size() == message.getMergeVars().size()) {
            final List<String> mergeVarRecipients = getSortedMergeVarRecipients(message);
            if (!mergeVarRecipients.equals(recipients)) {
                if (message.getTo().size() == 1) {
                    message.getMergeVars().get(0).setRcpt(recipients.get(0));
                } else {
                    throw new RuntimeException("Could not send emails for " + recipients);
                }
            }

        } else {
            final List<MergeVarBucket> newBuckets = new ArrayList<>();
            for (final String email : recipients) {
                final MergeVarBucket bucket = BeanUtils.instantiateClass(MergeVarBucket.class);
                BeanUtils.copyProperties(message.getMergeVars().get(0), bucket);
                bucket.setRcpt(email);
                newBuckets.add(bucket);
            }
            message.setMergeVars(newBuckets);
        }

    }

    private void addCc(final MandrillMessage message, final String[] cc) {
        final List<MandrillMessage.Recipient> recipients = message.getTo();
        recipients.addAll(mandrillHelperService.getCCs(cc));

        message.setTo(recipients);
    }

    private void addDefaultCc(final MandrillMessage message, final String templateName) {
        final String ccEmail = configurationService.getConfiguration().getString(String.format("mandrill.%s.cc", templateName));

        if (StringUtils.isNotBlank(ccEmail)) {
            final List<MandrillMessage.Recipient> recipients = message.getTo();
            recipients.addAll(mandrillHelperService.getCCs(ccEmail));

            message.setTo(recipients);
        } else {
            addDefaultCc(message);
        }
    }

    private void addDefaultCc(final MandrillMessage message) {
        final String ccEmail = configurationService.getConfiguration().getString("mandrill.otheremails.cc");

        if (StringUtils.isNotBlank(ccEmail)) {
            final List<MandrillMessage.Recipient> recipients = message.getTo();
            recipients.addAll(mandrillHelperService.getCCs(ccEmail));

            message.setTo(recipients);
        }
    }

    private void addBcc(final MandrillMessage message, final String[] bcc) {
        final List<MandrillMessage.Recipient> recipients = message.getTo();
        recipients.addAll(mandrillHelperService.getBCCs(bcc));

        message.setTo(recipients);
    }

    private void addDefaultBcc(final MandrillMessage message, final String templateName) {
        final String bccEmail = configurationService.getConfiguration().getString(String.format("mandrill.%s.bcc", templateName));

        if (StringUtils.isNotBlank(bccEmail)) {
            final List<MandrillMessage.Recipient> recipients = message.getTo();
            recipients.addAll(mandrillHelperService.getBCCs(bccEmail));

            message.setTo(recipients);
        } else {
            addDefaultBcc(message);
        }
    }

    private void addDefaultBcc(final MandrillMessage message) {
        final String bccEmail = configurationService.getConfiguration().getString("mandrill.otheremails.bcc");

        if (StringUtils.isNotBlank(bccEmail)) {
            final List<MandrillMessage.Recipient> recipients = message.getTo();
            recipients.addAll(mandrillHelperService.getBCCs(bccEmail));

            message.setTo(recipients);
        }
    }

    private List<String> getSortedRecipients(final MandrillMessage message) {
        return message.getTo().stream()
                .map(MandrillMessage.Recipient::getEmail)
                .sorted()
                .collect(toList());
    }

    private List<String> getSortedMergeVarRecipients(final MandrillMessage message) {
        return message.getMergeVars().stream()
                .map(MergeVarBucket::getRcpt)
                .sorted()
                .collect(toList());
    }

    private boolean isTestModeActive() {
        return configurationService.getConfiguration().getBoolean("mandrill.testmode");
    }

    private <T> void setMergeVars(final MandrillConverter<T> converter, final T object, final MandrillMessage message) {
        final List<MergeVarBucket> mergeVarBucket = converter.convert(object);
        message.setMergeVars(mergeVarBucket);
    }

    private MandrillMessage createMessage(final String... email) {
        final MandrillMessage message = new MandrillMessage();
        message.setMergeLanguage(HANDLEBARS);
        message.setTo(mandrillHelperService.getRecipients(email));
        return message;
    }

    private boolean containsNullValues(final String... emails) {
        return !Arrays.stream(emails).allMatch(Objects::nonNull);
    }

    /**
     * Setter method for the {@link MandrillApi}.
     *
     * @param mandrillApi The {@link MandrillApi} to be set.
     */
    @Required
    public void setMandrillApi(final MandrillApi mandrillApi) {
        this.mandrillApi = mandrillApi;
    }

    /**
     * Setter method for {@link MandrillApi}.
     *
     * @param mandrillTestApi The {@link MandrillApi} to be set.
     */
    @Required
    public void setMandrillTestApi(final MandrillApi mandrillTestApi) {
        this.mandrillTestApi = mandrillTestApi;
    }

    /**
     * Setter method for {@link MandrillHelperService}.
     *
     * @param mandrillHelperService The {@link MandrillHelperService} to be set.
     */
    @Required
    public void setMandrillHelperService(final MandrillHelperService mandrillHelperService) {
        this.mandrillHelperService = mandrillHelperService;
    }

    /**
     * Setter method for {@link ConfigurationService}.
     *
     * @param configurationService The {@link ConfigurationService} to be set.
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
