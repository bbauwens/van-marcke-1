package be.elision.mandrillextension.service;

import be.elision.mandrillextension.converter.MandrillConverter;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;

import java.io.IOException;

/**
 * Interface for {@link MandrillService}.
 *
 * @author marteto
 */
public interface MandrillService {

    /**
     * Sends an email.
     *
     * @param templateName The name of the template to be used.
     * @param object       The object that contains the variables.
     * @param converter    The converter that should be used in conjunction with the object.
     * @param email        The email of the recipients.
     * @return {@link MandrillMessageStatus[]}
     * @throws IOException
     * @throws MandrillApiError
     */
    <T> MandrillMessageStatus[] send(final String templateName, final T object, final MandrillConverter<T> converter, final String... email) throws IOException, MandrillApiError;

    /**
     * Sends an email.
     *
     * @param templateName The name of the template to be used.
     * @param object       The object that contains the variables.
     * @param converter    The converter that should be used in conjunction with the object.
     * @param email        The email of the recipients.
     * @param cc           The email of the other recipients.
     * @return {@link MandrillMessageStatus[]}
     * @throws IOException
     * @throws MandrillApiError
     */
    <T> MandrillMessageStatus[] send(final String templateName, final T object, final MandrillConverter<T> converter, final String[] email, final String[] cc) throws IOException, MandrillApiError;

    /**
     * Sends an email.
     *
     * @param templateName The name of the template to be used.
     * @param object       The object that contains the variables.
     * @param converter    The converter that should be used in conjunction with the object.
     * @param email        The email of the recipients.
     * @param cc           The email of the other recipients.
     * @param bcc          The email of the other recipients.
     * @return {@link MandrillMessageStatus[]}
     * @throws IOException
     * @throws MandrillApiError
     */
    <T> MandrillMessageStatus[] send(final String templateName, final T object, MandrillConverter<T> converter, final String[] email, final String[] cc, final String[] bcc) throws IOException, MandrillApiError;
}
