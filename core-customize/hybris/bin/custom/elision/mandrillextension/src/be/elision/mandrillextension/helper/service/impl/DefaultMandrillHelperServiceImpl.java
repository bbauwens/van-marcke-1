package be.elision.mandrillextension.helper.service.impl;

import be.elision.mandrillextension.helper.service.MandrillHelperService;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;

/**
 * Default implementation for {@link MandrillHelperService}.
 *
 * @author marteto
 */
public class DefaultMandrillHelperServiceImpl implements MandrillHelperService {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Recipient> getRecipients(final String... emails) {
        List<Recipient> recipientList = Collections.emptyList();

        if (ArrayUtils.isNotEmpty(emails)) {
            recipientList = Arrays.stream(emails)
                    .filter(Objects::nonNull)
                    .filter(StringUtils::isNotBlank)
                    .map(this::convertEmailToRecipient)
                    .collect(Collectors.toList());
        }
        return recipientList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Recipient> getCCs(final String... emails) {
        List<Recipient> recipientList = Collections.emptyList();

        if (ArrayUtils.isNotEmpty(emails)) {
            recipientList = Arrays.stream(emails)
                    .filter(Objects::nonNull)
                    .filter(StringUtils::isNotBlank)
                    .map(this::convertEmailToCCRecipient)
                    .collect(Collectors.toList());
        }
        return recipientList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Recipient> getBCCs(final String... emails) {
        List<Recipient> recipientList = Collections.emptyList();

        if (ArrayUtils.isNotEmpty(emails)) {
            recipientList = Arrays.stream(emails)
                    .filter(Objects::nonNull)
                    .filter(StringUtils::isNotBlank)
                    .map(this::convertEmailToBCCRecipient)
                    .collect(Collectors.toList());
        }
        return recipientList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Recipient convertEmailToRecipient(final String email) {
        Recipient recipient = new Recipient();
        recipient.setEmail(email);
        return recipient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Recipient convertEmailToCCRecipient(final String email) {
        Recipient recipient = convertEmailToRecipient(email);
        recipient.setType(Recipient.Type.CC);
        return recipient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Recipient convertEmailToBCCRecipient(final String email) {
        Recipient recipient = convertEmailToRecipient(email);
        recipient.setType(Recipient.Type.BCC);
        return recipient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MergeVar extractSingleMergeVar(final List<MergeVarBucket> mergeVarBuckets) {
        MergeVar firstMergeVar = null;

        if (CollectionUtils.isNotEmpty(mergeVarBuckets)) {
            MergeVar[] firstOrderBucketVars = mergeVarBuckets.get(0).getVars();
            if (ArrayUtils.isNotEmpty(firstOrderBucketVars)) {
                firstMergeVar = firstOrderBucketVars[0];
            }
        }
        return firstMergeVar;
    }
}
