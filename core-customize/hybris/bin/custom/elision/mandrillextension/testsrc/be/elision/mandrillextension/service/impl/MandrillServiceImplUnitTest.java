package be.elision.mandrillextension.service.impl;

import be.elision.mandrillextension.converter.MandrillConverter;
import be.elision.mandrillextension.helper.service.impl.DefaultMandrillHelperServiceImpl;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.controller.MandrillMessagesApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.*;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author marteto
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MandrillServiceImplUnitTest {

    private DefaultMandrillServiceImpl mandrillService;

    @Mock
    private MandrillConverter<OrderModel> mandrillConverter;
    @Mock
    private MandrillApi mandrillApi;
    @Mock
    private MandrillApi mandrillTestApi;
    @Mock
    private DefaultMandrillHelperServiceImpl mandrillHelperService;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        given(configurationService.getConfiguration()).willReturn(configuration);
        given(configuration.getString("mandrill.otheremails.bcc")).willReturn("bccemail");

        mandrillService = new DefaultMandrillServiceImpl();
        mandrillService.setMandrillApi(mandrillApi);
        mandrillService.setMandrillHelperService(mandrillHelperService);
        mandrillService.setMandrillTestApi(mandrillTestApi);
        mandrillService.setConfigurationService(configurationService);
    }

    @Test
    public void testSend_Success() throws IOException, MandrillApiError {
        String templateName = "Template";
        String email = "email";
        OrderModel order = mock(OrderModel.class);
        MandrillMessage.MergeVarBucket mergeVarBucket = new MandrillMessage.MergeVarBucket();
        mergeVarBucket.setRcpt("email");
        MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
        recipient.setEmail("email");

        given(configuration.getBoolean("mandrill.testmode")).willReturn(false);

        MandrillMessagesApi mandrillMessagesApi = mock(MandrillMessagesApi.class);
        when(mandrillApi.messages()).thenReturn(mandrillMessagesApi);

        when(mandrillConverter.convert(order)).thenReturn(Collections.singletonList(mergeVarBucket));
        List<MandrillMessage.Recipient> recipients = new ArrayList<>();
        recipients.add(recipient);
        when(mandrillHelperService.getRecipients(email)).thenReturn(recipients);

        MandrillMessage.Recipient recipientBBC = new MandrillMessage.Recipient();
        recipientBBC.setEmail("bccemail");
        recipientBBC.setType(MandrillMessage.Recipient.Type.BCC);
        when(mandrillHelperService.getBCCs("bccemail")).thenReturn(Arrays.asList(recipientBBC));

        mandrillService.send(templateName, order, mandrillConverter, email);

        ArgumentCaptor<MandrillMessage> captor = ArgumentCaptor.forClass(MandrillMessage.class);
        verify(mandrillMessagesApi).sendTemplate(eq("Template"), any(HashMap.class), captor.capture(), eq(false));
        assertThat(captor.getValue().getMergeLanguage()).isEqualTo("handlebars");
        assertThat(captor.getValue().getTo().get(0)).isEqualTo(recipient);
        assertThat(captor.getValue().getTo().size()).isEqualTo(2);
        assertThat(captor.getValue().getTo().get(1).getEmail()).isEqualTo("bccemail");
        assertThat(captor.getValue().getTo().get(1).getType()).isEqualTo(MandrillMessage.Recipient.Type.BCC);
    }

    @Test
    public void testSend_OverrideBucketRcptSuccess() throws IOException, MandrillApiError {
        String templateName = "Template";
        String email = "email";
        OrderModel order = mock(OrderModel.class);
        MandrillMessage.MergeVarBucket mergeVarBucket = new MandrillMessage.MergeVarBucket();
        mergeVarBucket.setRcpt("foo");
        MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
        recipient.setEmail("bar");

        given(configuration.getBoolean("mandrill.testmode")).willReturn(false);

        MandrillMessagesApi mandrillMessagesApi = mock(MandrillMessagesApi.class);
        when(mandrillApi.messages()).thenReturn(mandrillMessagesApi);

        when(mandrillConverter.convert(order)).thenReturn(Collections.singletonList(mergeVarBucket));
        when(mandrillHelperService.getRecipients(email)).thenReturn(Collections.singletonList(recipient));

        ArgumentCaptor<MandrillMessage> captor = ArgumentCaptor.forClass(MandrillMessage.class);

        mandrillService.send(templateName, order, mandrillConverter, email);

        verify(mandrillMessagesApi).sendTemplate(eq("Template"), any(HashMap.class), captor.capture(), eq(false));
        assertThat(captor.getValue().getMergeLanguage()).isEqualTo("handlebars");
        assertThat(captor.getValue().getMergeVars().get(0).getRcpt()).isEqualTo("bar");
        assertThat(captor.getValue().getTo().get(0)).isEqualTo(recipient);
    }

    @Test
    public void testSend_AdminSuccess() throws IOException, MandrillApiError {
        String templateName = "Template";
        String email = "foo@bar.com";
        OrderModel order = mock(OrderModel.class);
        MandrillMessage.MergeVarBucket mergeVarBucket = new MandrillMessage.MergeVarBucket();
        mergeVarBucket.setRcpt("email");
        MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
        recipient.setEmail("email");

        given(configuration.getBoolean("mandrill.testmode")).willReturn(true);

        MandrillMessagesApi mandrillMessagesApi = mock(MandrillMessagesApi.class);
        when(mandrillTestApi.messages()).thenReturn(mandrillMessagesApi);

        when(mandrillConverter.convert(order)).thenReturn(Collections.singletonList(mergeVarBucket));
        when(mandrillHelperService.getRecipients(email)).thenReturn(Collections.singletonList(recipient));

        ArgumentCaptor<MandrillMessage> captor = ArgumentCaptor.forClass(MandrillMessage.class);

        mandrillService.send(templateName, order, mandrillConverter, email);

        verify(mandrillMessagesApi).sendTemplate(eq("Template"), any(HashMap.class), captor.capture(), eq(false));
        assertThat(captor.getValue().getMergeLanguage()).isEqualTo("handlebars");
        assertThat(captor.getValue().getTo().get(0)).isEqualTo(recipient);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSend_EmailIsNullFail() throws IOException, MandrillApiError {
        OrderModel order = mock(OrderModel.class);

        String email = null;
        mandrillService.send("templatename", order, mandrillConverter, email);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSend_ConverterIsNullFail() throws IOException, MandrillApiError {
        OrderModel order = mock(OrderModel.class);
        String email = "email";
        mandrillService.send("templatename", order, null, email);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSend_EmptyTemplateNameFail() throws IOException, MandrillApiError {
        OrderModel order = mock(OrderModel.class);
        String email = "email";
        mandrillService.send("", order, mandrillConverter, email);
    }

}
