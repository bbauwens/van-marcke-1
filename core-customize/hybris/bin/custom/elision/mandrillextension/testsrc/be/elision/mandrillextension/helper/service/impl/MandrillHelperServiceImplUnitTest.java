package be.elision.mandrillextension.helper.service.impl;

import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import static org.fest.assertions.Assertions.assertThat;

/**
 * @author marteto
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MandrillHelperServiceImplUnitTest {

    @InjectMocks
    private DefaultMandrillHelperServiceImpl mandrillHelperService;

    @Test
    public void testGetRecipients_Success() {
        final String email = "foo@bar.com";

        List<Recipient> actual = mandrillHelperService.getRecipients(email);

        assertThat(CollectionUtils.isNotEmpty(actual)).isTrue();
        assertThat(actual.get(0).getEmail()).isEqualTo(email);
    }

    @Test
    public void testGetRecipients_NullSuccess() {
        final String email = null;

        List<Recipient> actual = mandrillHelperService.getRecipients(email);

        assertThat(CollectionUtils.isEmpty(actual)).isTrue();
    }

    @Test
    public void testGetRecipients_MultipleSuccess() {
        final String email = "foo@bar.com";
        final String secondEmail = "bar@foo.com";

        List<Recipient> actual = mandrillHelperService.getRecipients(email, secondEmail);

        assertThat(CollectionUtils.isNotEmpty(actual)).isTrue();
        assertThat(actual.get(0).getEmail()).isEqualTo(email);
        assertThat(actual.get(1).getEmail()).isEqualTo(secondEmail);
    }

    @Test
    public void testExtractSingleMergeVar_Success() {
        MandrillMessage.MergeVarBucket mergeVarBucket = new MandrillMessage.MergeVarBucket();
        MandrillMessage.MergeVar expected = new MandrillMessage.MergeVar();
        mergeVarBucket.setVars(new MandrillMessage.MergeVar[]{expected});
        List<MandrillMessage.MergeVarBucket> mergeVarBuckets = Collections.singletonList(mergeVarBucket);

        MandrillMessage.MergeVar actual = mandrillHelperService.extractSingleMergeVar(mergeVarBuckets);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void testExtractSingleMergeVar_EmptyListSuccess() {
        List<MandrillMessage.MergeVarBucket> mergeVarBuckets = Collections.emptyList();

        MandrillMessage.MergeVar actual = mandrillHelperService.extractSingleMergeVar(mergeVarBuckets);

        assertThat(actual).isNull();
    }

    @Test
    public void testGetBBCRecipients_Success() {
        final String email = "foo@bar.com";

        List<Recipient> actual = mandrillHelperService.getBCCs(email);

        assertThat(CollectionUtils.isNotEmpty(actual)).isTrue();
        assertThat(actual.get(0).getEmail()).isEqualTo(email);
        assertThat(actual.get(0).getType()).isEqualTo(Recipient.Type.BCC);
    }

}
