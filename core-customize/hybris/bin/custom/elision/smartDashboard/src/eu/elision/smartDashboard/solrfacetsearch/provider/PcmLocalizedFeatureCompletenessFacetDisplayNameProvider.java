package eu.elision.smartDashboard.solrfacetsearch.provider;


import de.hybris.platform.platformbackoffice.services.ClassificationAttributeAssignmentService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;

import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.SEPARATOR;
import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.UNLOCALIZED_STRING;

/**
 * Facet Search Value provider for the Display Name of the Localized Feature attributes
 *
 * @author vangeda
 */
public class PcmLocalizedFeatureCompletenessFacetDisplayNameProvider implements FacetValueDisplayNameProvider {

    private I18NService i18nService;
    private ClassificationAttributeAssignmentService classificationAttributeAssignmentService;

    private static final Logger LOG = LoggerFactory.getLogger(PcmLocalizedFeatureCompletenessFacetDisplayNameProvider.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName(SearchQuery searchQuery, IndexedProperty indexedProperty, String name) {
        String localizedName = null;
        if (name != null) {
            Locale queryLocale = LocaleUtils.toLocale(searchQuery.getLanguage());
            String[] splitValue = name.split(SEPARATOR);
            i18nService.setLocalizationFallbackEnabled(true);
            try {
                localizedName = classificationAttributeAssignmentService.findClassAttributeAssignment(splitValue[3]).getClassificationAttribute().getName(queryLocale);
                if (StringUtils.isBlank(localizedName)) {
                    localizedName = splitValue[3];
                }
                if (!UNLOCALIZED_STRING.equalsIgnoreCase(splitValue[1])) {
                    localizedName = localizedName + " [" + splitValue[1] + "]";
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                localizedName = name;
            }
        }
        return localizedName;
    }

    @Required
    public void setI18nService(I18NService i18nService) {
        this.i18nService = i18nService;
    }

    @Required
    public void setClassificationAttributeAssignmentService(ClassificationAttributeAssignmentService classificationAttributeAssignmentService) {
        this.classificationAttributeAssignmentService = classificationAttributeAssignmentService;
    }
}
