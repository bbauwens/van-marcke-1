package eu.elision.smartDashboard.constants;

import java.util.Locale;

/**
 * Global class for all SmartDashboard constants. You can add global constants for your extension into this class.
 */
public final class SmartDashboardConstants extends GeneratedSmartDashboardConstants {
    public static final String EXTENSIONNAME = "smartDashboard";

    private SmartDashboardConstants() {
        //empty to avoid instantiating this constant class
    }

    public static class ConfigurationProperties {
        public static final String INDEX_ONLY_MANDATORY_CLASS_ATTRIBUTES = "pcm.dashboard.index.only.classification.mandatory";
    }

    public static class Facets {
        public static final String SEPARATOR = "#";
        public static final String COMPLETENESS_PREFIX = "completeness";
        public static final String LOCALE_COMPLETENESS_SUFFIX = "locale";
        public static final String CLASSIFICATION_SYSTEM_COMPLETENESS_SUFFIX = "classification";
        public static final String CLASSIFICATION_CATEGORY_COMPLETENESS_SUFFIX = "classification_category";
        public static final String LOCALE_CLASSIFICATION_SYSTEM_COMPLETENESS_SUFFIX = "locale_classification";
        public static final String ATTRIBUTE_COMPLETENESS_SUFFIX = "attribute";
        public static final String FEATURE_COMPLETENESS_SUFFIX = "feature";
        public static final String PRODUCT_COMPLETENESS_FACET_SOLR_NAMESPACE = "completeness_product_double";
        public static final String UNLOCALIZED_STRING = "unlocalized";
        public static final Locale UNLOCALIZED_LOCALE = new Locale(UNLOCALIZED_STRING);
    }
}
