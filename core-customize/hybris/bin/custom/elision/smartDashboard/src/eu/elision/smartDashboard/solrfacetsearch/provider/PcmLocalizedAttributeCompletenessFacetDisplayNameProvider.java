package eu.elision.smartDashboard.solrfacetsearch.provider;


import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;

import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.SEPARATOR;
import static eu.elision.smartDashboard.constants.SmartDashboardConstants.Facets.UNLOCALIZED_STRING;

/**
 * Facet Search Value provider for the localized attribute names
 *
 * @author vangeda
 */
public class PcmLocalizedAttributeCompletenessFacetDisplayNameProvider implements FacetValueDisplayNameProvider {

    private TypeService typeService;
    private I18NService i18nService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName(SearchQuery searchQuery, IndexedProperty indexedProperty, String name) {
        if (name != null) {
            Locale queryLocale = LocaleUtils.toLocale(searchQuery.getLanguage());
            String[] splitValue = name.split(SEPARATOR);
            i18nService.setLocalizationFallbackEnabled(true);
            String localizedName = null;
            try {
                localizedName = typeService.getAttributeDescriptor(searchQuery.getIndexedType().getComposedType(), splitValue[2]).getName(queryLocale);
            } catch (Exception e) {
                //do nothing, this attribute is just not available on Product...
            }
            if (StringUtils.isBlank(localizedName)) {
                localizedName = splitValue[2];
            }
            if (!UNLOCALIZED_STRING.equalsIgnoreCase(splitValue[0])) {
                localizedName = localizedName + " [" + splitValue[0] + "]";
            }
            return localizedName;
        }
        return null;
    }

    @Required
    public void setI18nService(I18NService i18nService) {
        this.i18nService = i18nService;
    }

    @Required
    public void setTypeService(TypeService typeService) {
        this.typeService = typeService;
    }
}
