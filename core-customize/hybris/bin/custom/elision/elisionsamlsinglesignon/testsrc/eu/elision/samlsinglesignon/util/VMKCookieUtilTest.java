package eu.elision.samlsinglesignon.util;

import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The {@link VMKCookieUtilTest} class contains the unit tests for the {@link VMKCookieUtil} class.
 *
 * @author Christiaan Janssen
 * @since 14-07-2020
 */
@UnitTest
public class VMKCookieUtilTest {

    private static final String COOKIE_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String COOKIE_VALUE = RandomStringUtils.randomAlphabetic(10);

    @Test
    public void testGetCookieWithoutCookies() {
        HttpServletRequest request = mock(HttpServletRequest.class);

        Cookie actualCookie = VMKCookieUtil.getCookie(request, COOKIE_NAME);

        Assertions
                .assertThat(actualCookie)
                .isNull();
    }

    @Test
    public void testGetCookieWithoutMatchingCookie() {
        Cookie cookie = mock(Cookie.class);
        when(cookie.getName()).thenReturn(RandomStringUtils.random(10));

        Cookie anotherCookie = mock(Cookie.class);
        when(anotherCookie.getName()).thenReturn(RandomStringUtils.random(10));

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie, anotherCookie});

        Cookie actualCookie = VMKCookieUtil.getCookie(request, COOKIE_NAME);

        Assertions
                .assertThat(actualCookie)
                .isNull();
    }

    @Test
    public void testGetCookieWithMatchingCookie() {
        Cookie cookie = mock(Cookie.class);
        when(cookie.getName()).thenReturn(RandomStringUtils.random(10));

        Cookie expectedCookie = mock(Cookie.class);
        when(expectedCookie.getName()).thenReturn(COOKIE_NAME);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie, expectedCookie});

        Cookie actualCookie = VMKCookieUtil.getCookie(request, COOKIE_NAME);

        Assertions
                .assertThat(actualCookie)
                .isEqualTo(expectedCookie);
    }

    @Test
    public void testExtractValueWithoutCookie() {
        String actualValue = VMKCookieUtil.extractValue(null);

        Assertions
                .assertThat(actualValue)
                .isNull();
    }

    @Test
    public void testExtractValueWithCookie() {
        Cookie cookie = mock(Cookie.class);
        when(cookie.getValue()).thenReturn(COOKIE_VALUE);

        String actualValue = VMKCookieUtil.extractValue(cookie);

        Assertions
                .assertThat(actualValue)
                .isEqualTo(COOKIE_VALUE);
    }
}