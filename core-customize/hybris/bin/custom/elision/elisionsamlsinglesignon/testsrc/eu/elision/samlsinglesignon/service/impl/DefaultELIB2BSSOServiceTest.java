package eu.elision.samlsinglesignon.service.impl;

import com.vanmarcke.core.enums.ConsentType;
import com.vanmarcke.core.enums.OptType;
import com.vanmarcke.core.model.VMKConsentModel;
import com.vanmarcke.services.config.VMKConfigurationService;
import com.vanmarcke.services.consent.VMKConsentService;
import com.vanmarcke.services.model.builder.AddressModelMockBuilder;
import com.vanmarcke.services.model.builder.B2BUnitModelMockBuilder;
import com.vanmarcke.services.model.builder.CountryModelMockBuilder;
import com.vanmarcke.services.model.builder.PointOfServiceModelMockBuilder;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.samlsinglesignon.DefaultSSOService;
import de.hybris.platform.samlsinglesignon.SAMLService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import eu.elision.samlsinglesignon.dao.SSOUserDao;
import eu.elision.samlsinglesignon.service.SSOB2BUnitService;
import eu.elision.samlsinglesignon.strategy.ELISamlUserGroupLookupStrategy;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.opensaml.saml2.core.NameID;
import org.springframework.security.saml.SAMLCredential;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * The {@link DefaultELIB2BSSOServiceTest} class contains the unit tests for the {@link DefaultELIB2BSSOService} class.
 *
 * @author Christiaan Janssen
 * @since 13-11-2019
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultELIB2BSSOServiceTest {

    private static final String UID = RandomStringUtils.random(10);
    private static final String NAME = RandomStringUtils.random(10);
    private static final String CUSTOMER_ID = RandomStringUtils.random(10);
    private static final String COMPANY_ID = RandomStringUtils.random(10);
    private static final String ROLE = RandomStringUtils.random(10);
    private static final String ISO = RandomStringUtils.random(10);
    private static final String TOWN = RandomStringUtils.random(10);

    @Mock
    private SSOB2BUnitService b2BUnitService;

    @Mock
    private ModelService modelService;

    @Mock
    private SAMLService samlService;

    @Mock
    private UserService userService;

    @Mock
    private SSOUserDao ssoUserDao;

    @Mock
    private ELISamlUserGroupLookupStrategy samlUserGroupLookupStrategy;

    @Mock
    private VMKConfigurationService configurationService;

    @Mock
    private VMKConsentService consentService;

    @Spy
    @InjectMocks
    private DefaultELIB2BSSOService ssoService;

    @Captor
    private ArgumentCaptor<List<VMKConsentModel>> captor;

    @Test
    public void testGetOrCreateSSOUserWithNewUser() {
        NameID nameID = mock(NameID.class);

        SAMLCredential credential = mock(SAMLCredential.class);
        when(credential.getNameID()).thenReturn(nameID);

        when(configurationService.getString("sso.uid.attribute.key", "uid")).thenReturn(CUSTOMER_ID);
        when(samlService.getCustomAttribute(credential, CUSTOMER_ID)).thenReturn(CUSTOMER_ID);

        when(configurationService.getString("sso.usergroup.attribute.key", "usergroup")).thenReturn(ROLE);
        when(samlService.getCustomAttributes(credential, ROLE)).thenReturn(Collections.singletonList(ROLE));

        DefaultSSOService.SSOUserMapping mapping = mock(DefaultSSOService.SSOUserMapping.class);

        doReturn(mapping).when(ssoService).findMapping(Collections.singletonList(ROLE));

        UnknownIdentifierException exception = mock(UnknownIdentifierException.class);

        when(ssoUserDao.findUserByCustomerID(CUSTOMER_ID)).thenThrow(exception);

        B2BCustomerModel customer = new B2BCustomerModel();

        doReturn(customer).when(ssoService).createNewUser(UID.toLowerCase(), NAME, mapping);
        doNothing().when(ssoService).adjustUserAttributes(customer, mapping);

        when(configurationService.getString("sso.company.attribute.key", "company")).thenReturn(COMPANY_ID);
        when(samlService.getCustomAttribute(credential, COMPANY_ID)).thenReturn(COMPANY_ID);

        CountryModel country = CountryModelMockBuilder
                .aCountry()
                .withISOCode(ISO)
                .build();

        AddressModel address = AddressModelMockBuilder
                .anAddress()
                .withTown(TOWN)
                .withCountry(country)
                .build();

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(RandomStringUtils.random(10))
                .withShippingAddress(address)
                .build();

        when(b2BUnitService.getOrCreateSSOB2BUnit(COMPANY_ID)).thenReturn(b2bUnit);

        UserModel actualUser = ssoService.getOrCreateSSOUser(UID, NAME, credential);

        Assertions
                .assertThat(actualUser)
                .isEqualTo(customer);

        Assertions
                .assertThat(customer.getUid())
                .isEqualTo(UID.toLowerCase());

        Assertions
                .assertThat(customer.getName())
                .isEqualTo(NAME);

        Assertions
                .assertThat(customer.getCustomerID())
                .isEqualTo(CUSTOMER_ID);

        Assertions
                .assertThat(customer.getDefaultB2BUnit())
                .isEqualTo(b2bUnit);

        Assertions
                .assertThat(customer.getEmail())
                .isEqualTo(UID.toLowerCase());

        verify(configurationService).getString("sso.uid.attribute.key", "uid");
        verify(samlService).getCustomAttribute(credential, CUSTOMER_ID);
        verify(configurationService).getString("sso.usergroup.attribute.key", "usergroup");
        verify(samlService).getCustomAttributes(credential, ROLE);
        verify(ssoService).findMapping(Collections.singletonList(ROLE));
        verify(ssoUserDao).findUserByCustomerID(CUSTOMER_ID);
        verify(ssoService).createNewUser(UID.toLowerCase(), NAME, mapping);
        verify(ssoService).adjustUserAttributes(customer, mapping);
        verify(configurationService).getString("sso.company.attribute.key", "company");
        verify(samlService).getCustomAttribute(credential, COMPANY_ID);
        verify(b2BUnitService).getOrCreateSSOB2BUnit(COMPANY_ID);
        verify(modelService).save(customer);
    }

    @Test
    public void testGetOrCreateSSOUserWithExistingUser() {
        NameID nameID = mock(NameID.class);

        SAMLCredential credential = mock(SAMLCredential.class);
        when(credential.getNameID()).thenReturn(nameID);

        when(configurationService.getString("sso.uid.attribute.key", "uid")).thenReturn(CUSTOMER_ID);
        when(samlService.getCustomAttribute(credential, CUSTOMER_ID)).thenReturn(CUSTOMER_ID);

        when(configurationService.getString("sso.usergroup.attribute.key", "usergroup")).thenReturn(ROLE);
        when(samlService.getCustomAttributes(credential, ROLE)).thenReturn(Collections.singletonList(ROLE));

        DefaultSSOService.SSOUserMapping mapping = mock(DefaultSSOService.SSOUserMapping.class);

        doReturn(mapping).when(ssoService).findMapping(Collections.singletonList(ROLE));

        PointOfServiceModel pos = PointOfServiceModelMockBuilder
                .aPointOfService()
                .build();

        B2BCustomerModel customer = new B2BCustomerModel();
        customer.setSessionStore(pos);

        when(ssoUserDao.findUserByCustomerID(CUSTOMER_ID)).thenReturn(customer);

        doNothing().when(ssoService).adjustUserAttributes(customer, mapping);

        when(configurationService.getString("sso.company.attribute.key", "company")).thenReturn(COMPANY_ID);
        when(samlService.getCustomAttribute(credential, COMPANY_ID)).thenReturn(COMPANY_ID);

        B2BUnitModel b2bUnit = B2BUnitModelMockBuilder
                .aB2BUnit(RandomStringUtils.random(10))
                .build();

        when(b2BUnitService.getOrCreateSSOB2BUnit(COMPANY_ID)).thenReturn(b2bUnit);

        UserModel actualUser = ssoService.getOrCreateSSOUser(UID, NAME, credential);

        Assertions
                .assertThat(actualUser)
                .isEqualTo(customer);

        Assertions
                .assertThat(customer.getUid())
                .isEqualTo(UID.toLowerCase());

        Assertions
                .assertThat(customer.getName())
                .isEqualTo(NAME);

        Assertions
                .assertThat(customer.getCustomerID())
                .isEqualTo(CUSTOMER_ID);

        Assertions
                .assertThat(customer.getDefaultB2BUnit())
                .isEqualTo(b2bUnit);

        Assertions
                .assertThat(customer.getEmail())
                .isEqualTo(UID.toLowerCase());

        Assertions
                .assertThat(customer.getSessionStore())
                .isEqualTo(pos);

        verify(configurationService).getString("sso.uid.attribute.key", "uid");
        verify(samlService).getCustomAttribute(credential, CUSTOMER_ID);
        verify(configurationService).getString("sso.usergroup.attribute.key", "usergroup");
        verify(samlService).getCustomAttributes(credential, ROLE);
        verify(ssoService).findMapping(Collections.singletonList(ROLE));
        verify(ssoUserDao).findUserByCustomerID(CUSTOMER_ID);
        verify(ssoService, never()).createNewUser(UID.toLowerCase(), NAME, mapping);
        verify(ssoService).adjustUserAttributes(customer, mapping);
        verify(configurationService).getString("sso.company.attribute.key", "company");
        verify(samlService).getCustomAttribute(credential, COMPANY_ID);
        verify(b2BUnitService).getOrCreateSSOB2BUnit(COMPANY_ID);
        verify(modelService).save(customer);
    }

    @Test
    public void testGetOrCreateSSOUserWithConsents() {
        Cookie cookie = mock(Cookie.class);
        when(cookie.getName()).thenReturn("CONSENTS");
        when(cookie.getValue()).thenReturn("%5B%7B%22consentType%22%3A%22ANALYTICAL_COOKIES%22%2C%22optType%22%3A%22OPT_OUT%22%7D%2C%7B%22consentType%22%3A%22FUNCTIONAL_COOKIES%22%2C%22optType%22%3A%22OPT_IN%22%7D%5D");

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{cookie});

        SAMLCredential credential = mock(SAMLCredential.class);

        UserModel user = mock(UserModel.class);

        doReturn(user).when(ssoService).getOrCreateSSOUser(UID, NAME, credential);

        UserModel actualUser = ssoService.getOrCreateSSOUser(UID, NAME, credential, request);

        Assertions
                .assertThat(actualUser)
                .isEqualTo(user);

        verify(consentService).processConsents(captor.capture(), eq(request));

        List<VMKConsentModel> consents = captor.getValue();

        Assertions
                .assertThat(consents)
                .hasSize(2);

        Assertions
                .assertThat(consents.get(0).getConsentType())
                .isEqualTo(ConsentType.ANALYTICAL_COOKIES);

        Assertions
                .assertThat(consents.get(0).getOptType())
                .isEqualTo(OptType.OPT_OUT);

        Assertions
                .assertThat(consents.get(1).getConsentType())
                .isEqualTo(ConsentType.FUNCTIONAL_COOKIES);

        Assertions
                .assertThat(consents.get(1).getOptType())
                .isEqualTo(OptType.OPT_IN);
    }
}
