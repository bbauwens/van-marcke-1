package eu.elision.samlsinglesignon.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.samlsinglesignon.DefaultSSOService;
import de.hybris.platform.samlsinglesignon.SAMLService;
import de.hybris.platform.samlsinglesignon.model.SamlUserGroupModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import eu.elision.samlsinglesignon.strategy.ELISamlUserGroupLookupStrategy;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.saml.SAMLCredential;

import java.util.Collections;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultELIASMSSOServiceTest {

    @Mock
    private ModelService modelService;
    @Mock
    private SAMLService samlService;
    @Mock
    private UserService userService;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private ELISamlUserGroupLookupStrategy samlUserGroupLookupStrategy;
    @InjectMocks
    private DefaultELIASMSSOService defaultELIASMSSOService;

    @Mock
    private SAMLCredential samlCredential;
    @Mock
    private EmployeeModel userModel;
    @Mock
    private UserGroupModel userGroupModel;
    @Mock
    private TypeModel typeModel;
    @Mock
    private SamlUserGroupModel samlUserGroupModel;
    @Mock
    private Configuration configuration;

    private DefaultSSOService.SSOUserMapping ssoUserMapping;

    @Before
    public void setup() {
        ssoUserMapping = new DefaultSSOService.SSOUserMapping();

        defaultELIASMSSOService.setModelService(modelService);
        defaultELIASMSSOService.setUserService(userService);
        defaultELIASMSSOService = spy(defaultELIASMSSOService);

        when(configurationService.getConfiguration()).thenReturn(configuration);
        when(configuration.getBoolean("sso.database.usergroup.mapping", false)).thenReturn(true);
        when(configuration.getString("sso.usergroup.attribute.key", "usergroup")).thenReturn("usergroup");
        when(configuration.getString("sso.company.attribute.key", "company")).thenReturn("company");
        when(configuration.getString("sso.uid.attribute.key", "uid")).thenReturn("uid");

        when(samlService.getCustomAttributes(samlCredential, "usergroup")).thenReturn(singletonList("saml-role-asm"));

        when(userGroupModel.getUid()).thenReturn("asm-group");

        when(userService.getUserGroupForUID("asm-group")).thenReturn(userGroupModel);

        when(typeModel.getCode()).thenReturn("Employee");

        when(samlUserGroupModel.getUserType()).thenReturn(typeModel);
        when(samlUserGroupModel.getUserGroups()).thenReturn(singleton(userGroupModel));

        when(samlUserGroupLookupStrategy.getSamlUserGroupsForUserType(singletonList("saml-role-asm"), "Employee")).thenReturn(singletonList(samlUserGroupModel));
    }

    @Test
    public void testGetOrCreateSSOUser_withNewUser() {
        when(userService.getUserForUID("asm$id")).thenThrow(UnknownIdentifierException.class);

        when(modelService.create("Employee")).thenReturn(userModel);

        ssoUserMapping.setGroups(Collections.singleton("asm-group"));
        ssoUserMapping.setType(EmployeeModel._TYPECODE);
        doReturn(ssoUserMapping).when(defaultELIASMSSOService).findMapping(any());
        doReturn(userModel).when(defaultELIASMSSOService).createNewUser("asm$id", "name", ssoUserMapping);

        UserModel result = defaultELIASMSSOService.getOrCreateSSOUser("ID", "name", samlCredential);

        assertThat(result).isEqualTo(userModel);

        verify(modelService).save(userModel);
    }

    @Test
    public void testGetOrCreateSSOUser_withExistingUser() {
        when(userService.getUserForUID("asm$id")).thenReturn(userModel);

        ssoUserMapping.setGroups(Collections.singleton("asm-group"));
        doReturn(ssoUserMapping).when(defaultELIASMSSOService).findMapping(any());

        UserModel result = defaultELIASMSSOService.getOrCreateSSOUser("ID", "name", samlCredential);

        assertThat(result).isEqualTo(userModel);

        verify(userModel).setGroups(Sets.newSet(userGroupModel));

        verify(modelService).save(userModel);
    }

}