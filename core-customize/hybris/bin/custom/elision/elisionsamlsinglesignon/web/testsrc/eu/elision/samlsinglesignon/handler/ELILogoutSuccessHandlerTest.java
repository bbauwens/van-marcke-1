package eu.elision.samlsinglesignon.handler;

import com.vanmarcke.services.config.VMKConfigurationService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.RedirectStrategy;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ELILogoutSuccessHandlerTest {

    private static final String LANGUAGE_COOKIE_NAME = "sso.language.cookie";
    private static final String STORE_COOKIE_NAME = "sso.store.cookie";

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private Authentication authentication;
    @Mock
    private RedirectStrategy redirectStrategy;
    @Mock
    VMKConfigurationService configurationService;
    @Mock
    BaseSiteService baseSiteService;

    @Spy
    @InjectMocks
    private ELILogoutSuccessHandler eliLogoutSuccessHandler;

    @Before
    public void setUp() {
        eliLogoutSuccessHandler.setDefaultTargetUrl("/logout");
        eliLogoutSuccessHandler.setAlwaysUseDefaultTargetUrl(true);
        eliLogoutSuccessHandler.setRedirectStrategy(redirectStrategy);
    }

    @Test
    public void testOnLogoutSuccessAndSiteIsDIY() throws IOException, ServletException {
        HttpSession httpSession = mock(HttpSession.class);
        Cookie languageCookie = mock(Cookie.class);
        Cookie storeCookie = mock(Cookie.class);
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(configurationService.getString(LANGUAGE_COOKIE_NAME, "")).thenReturn("language");
        when(configurationService.getString(STORE_COOKIE_NAME, "")).thenReturn("store");
        when(request.getSession()).thenReturn(httpSession);

        doReturn(languageCookie).when(eliLogoutSuccessHandler).getCookie(request, "language");
        doReturn(storeCookie).when(eliLogoutSuccessHandler).getCookie(request, "store");

        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.DIY);
        when(baseSiteService.getBaseSiteForUID(anyString())).thenReturn(baseSiteModel);

        when(languageCookie.getValue()).thenReturn("nl_BE");
        when(storeCookie.getValue()).thenReturn("diy");
        eliLogoutSuccessHandler.onLogoutSuccess(request, response, authentication);
        verify(redirectStrategy).sendRedirect(request, response, "/diy/nl_BE/logout");
    }

    @Test
    public void testOnLogoutSuccessAndSiteIsNotDIY() throws IOException, ServletException {
        HttpSession httpSession = mock(HttpSession.class);
        Cookie languageCookie = mock(Cookie.class);
        Cookie storeCookie = mock(Cookie.class);
        BaseSiteModel baseSiteModel = mock(BaseSiteModel.class);

        when(configurationService.getString(LANGUAGE_COOKIE_NAME, "")).thenReturn("language");
        when(configurationService.getString(STORE_COOKIE_NAME, "")).thenReturn("store");
        when(request.getSession()).thenReturn(httpSession);

        doReturn(languageCookie).when(eliLogoutSuccessHandler).getCookie(request, "language");
        doReturn(storeCookie).when(eliLogoutSuccessHandler).getCookie(request, "store");

        when(baseSiteModel.getChannel()).thenReturn(SiteChannel.B2B);
        when(baseSiteService.getBaseSiteForUID(anyString())).thenReturn(baseSiteModel);

        when(languageCookie.getValue()).thenReturn("nl_BE");
        when(storeCookie.getValue()).thenReturn("store-other-than-diy");
        eliLogoutSuccessHandler.onLogoutSuccess(request, response, authentication);
        verify(redirectStrategy).sendRedirect(request, response, "/nl_BE/logout");
    }

    @Test
    public void testOnLogoutSuccessWhenNoCookie() throws IOException, ServletException {
        HttpSession httpSession = mock(HttpSession.class);

        when(configurationService.getString(LANGUAGE_COOKIE_NAME, "")).thenReturn("language");
        when(configurationService.getString(STORE_COOKIE_NAME, "")).thenReturn("store");
        when(request.getSession()).thenReturn(httpSession);

        eliLogoutSuccessHandler.onLogoutSuccess(request, response, authentication);
        verify(redirectStrategy).sendRedirect(request, response, "/logout");
    }
}