package eu.elision.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import eu.elision.samlsinglesignon.dao.SSOCmsSiteDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultELIReferenceURLStrategyTest {

    @Mock
    private SSOCmsSiteDao ssoCmsSiteDao;
    @InjectMocks
    private DefaultELIReferenceURLStrategy defaultELIReferenceURLStrategy;

    @Test
    public void testCreateReferenceURL_requestISO_nonB2BUser() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        UserModel user = mock(UserModel.class);

        when(request.getServletPath()).thenReturn("/saml/nl_BE");
        String referenceURL = defaultELIReferenceURLStrategy.createReferenceURL(request, user);

        assertThat(referenceURL).isEqualTo("nl_BE");
        verifyZeroInteractions(ssoCmsSiteDao);
    }

    @Test
    public void testCreateReferenceURL_requestISO_nonB2BUser_withQuery() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        UserModel user = mock(UserModel.class);

        when(request.getServletPath()).thenReturn("/saml/nl_BE");
        when(request.getQueryString()).thenReturn("someQueryString");
        String referenceURL = defaultELIReferenceURLStrategy.createReferenceURL(request, user);

        assertThat(referenceURL).isEqualTo("nl_BE?someQueryString");
        verifyZeroInteractions(ssoCmsSiteDao);
    }

    @Test
    public void testCreateReferenceURL_requestISO_isB2BUser() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        UserModel user = mock(UserModel.class);

        when(request.getServletPath()).thenReturn("/saml/nl_BE");
        String referenceURL = defaultELIReferenceURLStrategy.createReferenceURL(request, user);

        assertThat(referenceURL).isEqualTo("nl_BE");
        verifyZeroInteractions(ssoCmsSiteDao);
    }

    @Test
    public void testCreateReferenceURL_isB2BUser_isoFromSession() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        B2BCustomerModel user = mock(B2BCustomerModel.class);
        LanguageModel language = mock(LanguageModel.class);

        when(request.getServletPath()).thenReturn("/saml");
        when(user.getSessionLanguage()).thenReturn(language);
        when(language.getIsocode()).thenReturn("nl_BE");
        String referenceURL = defaultELIReferenceURLStrategy.createReferenceURL(request, user);

        assertThat(referenceURL).isEqualTo("nl_BE");
        verifyZeroInteractions(ssoCmsSiteDao);
    }

    @Test
    public void testCreateReferenceURL_isB2BUser_isoFromB2BUnit() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        B2BCustomerModel user = mock(B2BCustomerModel.class);
        B2BUnitModel company = mock(B2BUnitModel.class);
        AddressModel billingAddress = mock(AddressModel.class);
        CountryModel country = mock(CountryModel.class);
        CMSSiteModel cmsSite = mock(CMSSiteModel.class);
        LanguageModel language = mock(LanguageModel.class);

        when(request.getServletPath()).thenReturn("/saml");
        when(user.getSessionLanguage()).thenReturn(null);
        when(user.getDefaultB2BUnit()).thenReturn(company);
        when(company.getBillingAddress()).thenReturn(billingAddress);
        when(billingAddress.getCountry()).thenReturn(country);
        when(ssoCmsSiteDao.findCMSSiteForUserBasedOnCountry(country)).thenReturn(cmsSite);
        when(cmsSite.getDefaultLanguage()).thenReturn(language);
        when(language.getIsocode()).thenReturn("nl_BE");
        String referenceURL = defaultELIReferenceURLStrategy.createReferenceURL(request, user);

        assertThat(referenceURL).isEqualTo("nl_BE");
        verify(ssoCmsSiteDao).findCMSSiteForUserBasedOnCountry(country);
    }
}