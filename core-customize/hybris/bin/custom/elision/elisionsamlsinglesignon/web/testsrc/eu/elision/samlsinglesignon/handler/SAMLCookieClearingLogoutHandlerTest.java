package eu.elision.samlsinglesignon.handler;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;

import javax.servlet.http.Cookie;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SAMLCookieClearingLogoutHandlerTest {

    @Test
    public void testLogout() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        MockHttpServletRequest request = new MockHttpServletRequest();

        SAMLCookieClearingLogoutHandler handler = new SAMLCookieClearingLogoutHandler("my_cookie");
        handler.logout(request, response, mock(Authentication.class));

        assertThat(response.getCookies()).hasSize(1);

        for (Cookie c : response.getCookies()) {
            assertThat(c.getPath()).isEqualTo("/");
            assertThat(c.getMaxAge()).isZero();
        }
    }

}