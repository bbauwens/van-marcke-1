package eu.elision.samlsinglesignon.handler;

import com.vanmarcke.services.config.VMKConfigurationService;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Custom Logout Success Handler
 */
public class ELILogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

    private static final String LANGUAGE_COOKIE_NAME = "sso.language.cookie";
    private static final String STORE_COOKIE_NAME = "sso.store.cookie";
    private static final String DIY_PATH = "diy/";

    private VMKConfigurationService configurationService;
    private BaseSiteService baseSiteService;
    /**
     * {@inheritDoc}
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(null);
        request.getSession().invalidate();

        super.onLogoutSuccess(request, response, authentication);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {
        String targetUrl = super.determineTargetUrl(request, response);
        final String languageCookieName = configurationService.getString(LANGUAGE_COOKIE_NAME, "");
        final String storeCookieName = configurationService.getString(STORE_COOKIE_NAME, "");

        Cookie languageCookie = getCookie(request, languageCookieName);
        Cookie storeCookie = getCookie(request, storeCookieName);

        boolean isDiy = storeCookie != null &&
                StringUtils.isNotBlank(storeCookie.getValue()) &&
                baseSiteService.getBaseSiteForUID(storeCookie.getValue()).getChannel().equals(SiteChannel.DIY);

        if (languageCookie != null) {
            String pathFromCookies = isDiy ? DIY_PATH + languageCookie.getValue() : languageCookie.getValue();
            targetUrl = "/" + pathFromCookies + targetUrl;
        }

        return targetUrl;
    }

    protected Cookie getCookie(HttpServletRequest request, String name) {
        return WebUtils.getCookie(request, name);
    }

    public void setConfigurationService(VMKConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}
