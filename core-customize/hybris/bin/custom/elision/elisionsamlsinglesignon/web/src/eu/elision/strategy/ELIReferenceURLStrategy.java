package eu.elision.strategy;

import de.hybris.platform.core.model.user.UserModel;

import javax.servlet.http.HttpServletRequest;

/**
 * Strategy for the Reference URL
 *
 * @author Niels Raemaekers
 * @since 17/06/2020
 */
public interface ELIReferenceURLStrategy {

    /**
     * Create the correct reference url for the given user and request
     *
     * @param request the given request
     * @param user    the given user
     * @return the reference URL
     */
    String createReferenceURL(HttpServletRequest request, UserModel user);
}
