package eu.elision.samlsinglesignon.controller;

import com.vanmarcke.services.b2b.VMKSSOUserService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.persistence.security.EJBPasswordEncoderNotFoundException;
import de.hybris.platform.samlsinglesignon.SAMLService;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.Config;
import eu.elision.samlsinglesignon.enums.SSOContext;
import eu.elision.samlsinglesignon.factory.ELISSOUserServiceFactory;
import eu.elision.samlsinglesignon.util.VMKCookieUtil;
import eu.elision.strategy.ELIReferenceURLStrategy;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

@Controller
@RequestMapping("/saml")
public class RedirectionController {

    private static final Logger LOG = LoggerFactory.getLogger(RedirectionController.class);

    private static final String SSO_DEFAULT_COOKIE_DOMAIN = null;
    private static final String LANGUAGE_COOKIE = "language";
    private static final String DIY = "diy/";

    private final ELISSOUserServiceFactory ssoUserServiceFactory;
    private final CommonI18NService commonI18NService;
    private final CMSSiteService cmsSiteService;
    private final SAMLService samlService;
    private final ELIReferenceURLStrategy eliReferenceURLStrategy;
    private final VMKSSOUserService vmkssoUserService;

    /**
     * Creates a new instance of the {@link RedirectionController} class.
     *
     * @param ssoUserServiceFactory   the user service factory
     * @param commonI18NService       the I18N service
     * @param cmsSiteService          the CMS site service
     * @param samlService             the SAML service
     * @param eliReferenceURLStrategy the reference URL strategy
     * @param vmkssoUserService       the VMK SSO User Service
     */
    public RedirectionController(ELISSOUserServiceFactory ssoUserServiceFactory,
                                 CommonI18NService commonI18NService,
                                 CMSSiteService cmsSiteService,
                                 SAMLService samlService,
                                 ELIReferenceURLStrategy eliReferenceURLStrategy,
                                 VMKSSOUserService vmkssoUserService) {
        this.ssoUserServiceFactory = ssoUserServiceFactory;
        this.commonI18NService = commonI18NService;
        this.cmsSiteService = cmsSiteService;
        this.samlService = samlService;
        this.eliReferenceURLStrategy = eliReferenceURLStrategy;
        this.vmkssoUserService = vmkssoUserService;
    }

    /**
     * Retrieves the required redirect URL based on the SAML credential.
     *
     * @param response the http response
     * @param request  the http request
     * @return the redirect url
     */
    @GetMapping(value = "/**")
    public String redirect(HttpServletResponse response, HttpServletRequest request) {
        SAMLCredential credential = (SAMLCredential) SecurityContextHolder.getContext().getAuthentication().getCredentials();

        String redirectURL = Config.getString("sso.redirect.url", "https://localhost:9002/");

        final SSOContext ssoContext = request.getParameter("asm") != null ? SSOContext.ASM : SSOContext.B2B;

        try {
            UserModel user = ssoUserServiceFactory.getOrCreateSSOUser(ssoContext, credential, request);
            vmkssoUserService.updateUserSessionStoreIfNotPresent(user);
            
            String language = getLanguage(credential, request);
            String country = getCountry(user);

            String referenceURL = eliReferenceURLStrategy.createReferenceURL(request, user);
            storeTokenFromSaml(response, user, language);

            return "redirect:" + getLocalizedURL(redirectURL, referenceURL, language, country);
        } catch (Exception e) {
            LOG.error(MessageFormat.format("An exception occurred when trying to redirect: {0}.", e.getMessage()), e);
            return "redirect:" + redirectURL + "elisionsamlsinglesignon/error";
        }
    }

    /**
     * Retrieve the localized URL for the given language and country.
     *
     * @param baseURL         the base URL
     * @param oldReferenceURL the old reference URL
     * @param language        the language
     * @param country         the country
     * @return the localized URL
     */
    protected String getLocalizedURL(String baseURL, String oldReferenceURL, String language, String country) {
        String prefix = MessageFormat.format("{0}_{1}/", language, country);
        String url = getURL(oldReferenceURL, baseURL, prefix);
        return validate(url, country, baseURL, oldReferenceURL);
    }

    /**
     * Retrieve the correct URL for the given parameters.
     *
     * @param referenceURL the reference URL
     * @param baseURL      the base URL
     * @param prefix       the prefix
     * @return the URL
     */
    private String getURL(String referenceURL, String baseURL, String prefix) {

        if (referenceURL.contains(DIY)) {
            return baseURL + referenceURL;
        }

        String url = baseURL;

        Pattern pattern = Pattern.compile("^[a-z]{2}_[A-Z]{2}[/]?");
        Matcher matcher = pattern.matcher(referenceURL);

        if (matcher.find()) {
            url += matcher.replaceFirst(prefix);
        } else {
            url += prefix + referenceURL;
        }
        return url;
    }

    /**
     * Validate if the given parameters can be linked to an existing CMS site.
     *
     * @param currentURL      the current URL
     * @param country         the country
     * @param baseURL         the base URL
     * @param oldReferenceURL the old reference URL
     * @return the CMS site URL
     */
    private String validate(String currentURL, String country, String baseURL, String oldReferenceURL) {
        String url = baseURL;
        try {
            cmsSiteService.getSiteForURL(new URL(currentURL));
            url = currentURL;
        } catch (CMSItemNotFoundException | MalformedURLException e) {
                for (CMSSiteModel site : cmsSiteService.getSites()) {
                    if (("blue-" + country.toLowerCase()).equals(site.getUid())) {
                        String prefix = site.getDefaultLanguage().getIsocode() + "/";
                        url = getURL(oldReferenceURL, baseURL, prefix);
                        break;
                    }

            }
        }
        return url;
    }

    /**
     * Retrieve the language for the current session from the cookie or from the SAML credential as a fallback.
     *
     * @param credential the SAML credential
     * @param request    the HTTP request
     * @return the language
     */
    protected String getLanguage(SAMLCredential credential, HttpServletRequest request) {
        String language = getLanguageFromCookie(request);
        if (StringUtils.isEmpty(language)) {
            language = samlService.getLanguage(credential, request, commonI18NService);
        }
        return language;
    }

    /**
     * Retrieves the language from the language cookie.
     * <p>
     * Returns {@code null} if the language cookie does not exist.
     *
     * @param request the HTTP request
     * @return the language
     */
    private String getLanguageFromCookie(HttpServletRequest request) {
        String language = null;
        Cookie languageCookie = VMKCookieUtil.getCookie(request, LANGUAGE_COOKIE);
        if (languageCookie != null && isNotEmpty(languageCookie.getValue())) {
            language = languageCookie.getValue().substring(0, 2);
        }
        return language;
    }

    /**
     * Retrieve the country iso code for the given user.
     *
     * @param user the user
     * @return the country iso code
     */
    private String getCountry(UserModel user) {
        String country = "";
        if (user instanceof B2BCustomerModel) {
            B2BUnitModel b2bUnit = ((B2BCustomerModel) user).getDefaultB2BUnit();
            if (b2bUnit.getShippingAddress() != null) {
                return b2bUnit.getShippingAddress().getCountry().getIsocode();
            }
        }
        return country;
    }

    /**
     * Store the SAML token on the cookie.
     *
     * @param response        the HTTP response
     * @param user            the user
     * @param languageIsoCode the language iso code
     */
    private void storeTokenFromSaml(HttpServletResponse response, UserModel user, String languageIsoCode) {
        try {
            String cookiePath = Config.getString("sso.cookie.path", "/");
            String cookieMaxAgeStr = Config.getString("sso.cookie.max.age", String.valueOf(60));
            int cookieMaxAge;
            if (!NumberUtils.isCreatable(cookieMaxAgeStr)) {
                cookieMaxAge = 60;
            } else {
                cookieMaxAge = Integer.parseInt(cookieMaxAgeStr);
            }

            UserManager.getInstance().storeLoginTokenCookie(Config.getString("sso.cookie.name", "samlPassThroughToken"), user.getUid(), languageIsoCode, null, cookiePath, Config.getString("sso.cookie.domain", SSO_DEFAULT_COOKIE_DOMAIN), true, cookieMaxAge, response);
        } catch (EJBPasswordEncoderNotFoundException var7) {
            throw new SystemException(var7);
        }
    }
}
