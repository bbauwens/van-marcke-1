package eu.elision.samlsinglesignon.service.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanmarcke.core.model.VMKConsentModel;
import com.vanmarcke.facades.data.VMKConsentData;
import com.vanmarcke.services.config.VMKConfigurationService;
import com.vanmarcke.services.consent.VMKConsentService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.samlsinglesignon.DefaultSSOService;
import de.hybris.platform.samlsinglesignon.SAMLService;
import de.hybris.platform.samlsinglesignon.model.SamlUserGroupModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import eu.elision.samlsinglesignon.dao.SSOUserDao;
import eu.elision.samlsinglesignon.service.ELISSOUserService;
import eu.elision.samlsinglesignon.service.SSOB2BUnitService;
import eu.elision.samlsinglesignon.strategy.ELISamlUserGroupLookupStrategy;
import eu.elision.samlsinglesignon.util.VMKCookieUtil;
import org.apache.commons.lang3.StringUtils;
import org.opensaml.saml2.core.NameID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.saml.SAMLCredential;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static java.lang.String.format;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.lowerCase;

/**
 * Extends the default implementation {@link DefaultSSOService} to handle B2B Customers.
 */
public class DefaultELIB2BSSOService extends DefaultSSOService implements ELISSOUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultELIB2BSSOService.class);

    private static final String CONSENT_COOKIE = "CONSENTS";

    private final SSOB2BUnitService b2BUnitService;
    private final ModelService modelService;
    private final SAMLService samlService;
    private final SSOUserDao ssoUserDao;
    private final VMKConfigurationService configurationService;
    private final ELISamlUserGroupLookupStrategy samlUserGroupLookupStrategy;
    private final VMKConsentService consentService;

    /**
     * Creates an instance of the {@link DefaultELIB2BSSOService} class.
     *
     * @param b2BUnitService              the B2B unit service
     * @param modelService                the model service
     * @param samlService                 the SAML service
     * @param ssoUserDao                  the user DAO
     * @param configurationService        the configuration service
     * @param samlUserGroupLookupStrategy the user group lookup strategy
     * @param consentService              the consent service
     */
    public DefaultELIB2BSSOService(SSOB2BUnitService b2BUnitService,
                                   ModelService modelService,
                                   SAMLService samlService,
                                   SSOUserDao ssoUserDao,
                                   VMKConfigurationService configurationService,
                                   ELISamlUserGroupLookupStrategy samlUserGroupLookupStrategy,
                                   VMKConsentService consentService) {
        this.b2BUnitService = b2BUnitService;
        this.modelService = modelService;
        this.samlService = samlService;
        this.ssoUserDao = ssoUserDao;
        this.configurationService = configurationService;
        this.samlUserGroupLookupStrategy = samlUserGroupLookupStrategy;
        this.consentService = consentService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserModel getOrCreateSSOUser(String id, String name, SAMLCredential samlCredential) {
        NameID nameID = getNameID(id, samlCredential);
        String customerID = getCustomerID(id, samlCredential, nameID);

        SSOUserMapping userMapping = getUserMapping(id, samlCredential);

        String uid = lowerCase(id);
        UserModel user = lookupExisting(customerID, userMapping);
        if (user == null) {
            user = createNewUser(uid, name, userMapping);
        }

        checkState(user instanceof B2BCustomerModel, "user must be of type B2BCustomer");
        B2BCustomerModel b2bCustomer = (B2BCustomerModel) user;

        adjustUserAttributes(b2bCustomer, userMapping);

        B2BUnitModel b2bUnit = b2BUnitService.getOrCreateSSOB2BUnit(getCompanyID(id, samlCredential));

        b2bCustomer.setUid(uid);
        b2bCustomer.setName(name);
        b2bCustomer.setCustomerID(customerID);
        b2bCustomer.setDefaultB2BUnit(b2bUnit);
        b2bCustomer.setEmail(uid);
        b2bCustomer.setSessionStore(b2bUnit.getFavoritePointOfService());

        modelService.save(b2bCustomer);

        return b2bCustomer;
    }

    /**
     * Returns the name ID.
     *
     * @param id             the user ID
     * @param samlCredential the SAML credential
     * @return the name ID
     */
    private NameID getNameID(String id, SAMLCredential samlCredential) {
        checkArgument(isNotEmpty(id), "User id cannot be empty");
        NameID nameID = samlCredential.getNameID();
        checkState(nameID != null, format("Name ID is required - cannot accept user '%s'", id));
        return nameID;
    }

    /**
     * Returns the roles.
     *
     * @param id             the user ID
     * @param samlCredential the SAML credential
     * @return the roles
     */
    private Collection<String> getRoles(String id, SAMLCredential samlCredential) {
        String property = configurationService.getString("sso.usergroup.attribute.key", "usergroup");
        Collection<String> roles = samlService.getCustomAttributes(samlCredential, property);
        checkState(isNotEmpty(roles), format("Roles attribute is required - cannot accept user '%s'", id));
        return roles;
    }

    /**
     * Returns the company ID.
     *
     * @param id             the user ID
     * @param samlCredential the SAML credential
     * @return the company ID
     */
    private String getCompanyID(String id, SAMLCredential samlCredential) {
        String property = configurationService.getString("sso.company.attribute.key", "company");
        String companyID = samlService.getCustomAttribute(samlCredential, property);
        checkState(isNotBlank(companyID), format("Company attribute is required - cannot accept user '%s'", id));
        return companyID;
    }

    /**
     * Returns the customer ID.
     *
     * @param id             the user ID
     * @param samlCredential the SAML credential
     * @return the customer ID
     */
    private String getCustomerID(String id, SAMLCredential samlCredential, NameID nameID) {
        String property = configurationService.getString("sso.uid.attribute.key", "uid");
        String customerID = defaultIfBlank(nameID.getValue(), samlService.getCustomAttribute(samlCredential, property));
        checkState(isNotBlank(customerID), format("UID attribute is required - cannot accept user '%s'", id));
        return customerID;
    }

    /**
     * Returns the user mapping.
     *
     * @param id             the user ID
     * @param samlCredential the SAML credential
     * @return the user mapping
     */
    private SSOUserMapping getUserMapping(String id, SAMLCredential samlCredential) {
        SSOUserMapping userMapping = findMapping(getRoles(id, samlCredential));
        checkState(userMapping != null, "No SSO user mapping available - cannot accept user " + id);
        return userMapping;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserModel getOrCreateSSOUser(String id, String name, SAMLCredential samlCredential, HttpServletRequest request) {
        UserModel b2bCustomer = getOrCreateSSOUser(id, name, samlCredential);
        consentService.processConsents(extractConsents(request), request);
        return b2bCustomer;
    }

    /**
     * Extracts the consents from the given {@code request}.
     *
     * @param request the HTTP request
     * @return the consents
     */
    private List<VMKConsentModel> extractConsents(HttpServletRequest request) {
        Cookie cookie = VMKCookieUtil.getCookie(request, CONSENT_COOKIE);

        String value = null;

        if (cookie == null) {
            return Collections.emptyList();
        }

        try {
            value = URLDecoder.decode(Objects.requireNonNull(VMKCookieUtil.extractValue(cookie)), StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            // We cannot use the StandardCharsets directly because ant is still running on Java 1.8.
        }

        if (StringUtils.isEmpty(value)) {
            return Collections.emptyList();
        }

        return convert(extract(value));
    }

    /**
     * Extracts the consent data from the given {@code value}.
     *
     * @param value the string value
     * @return the consent data
     */
    private List<VMKConsentData> extract(String value) {
        return new Gson().fromJson(value, new TypeToken<List<VMKConsentData>>() {
        }.getType());
    }

    /**
     * Converts the given {@code consentDTOs} to instances of the {@link VMKConsentModel} class.
     *
     * @param consentDTOs the consent DTOs
     * @return the {@link VMKConsentModel} instances
     */
    private List<VMKConsentModel> convert(List<VMKConsentData> consentDTOs) {
        return consentDTOs
                .stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    /**
     * Converts the given {@code consentDTO} to an instance of the {@link VMKConsentModel} class.
     *
     * @param consentDTO the consent DTO
     * @return the {@link VMKConsentModel} instance
     */
    private VMKConsentModel convert(VMKConsentData consentDTO) {
        VMKConsentModel consentModel = new VMKConsentModel();
        consentModel.setConsentType(consentDTO.getConsentType());
        consentModel.setOptType(consentDTO.getOptType());
        return consentModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DefaultSSOService.SSOUserMapping findMappingInDatabase(Collection<String> roles) {
        List<SamlUserGroupModel> userGroupModels = samlUserGroupLookupStrategy.getSamlUserGroupsForUserType(roles, B2BCustomerModel._TYPECODE);
        validateMappings(roles, userGroupModels);
        return performMapping(userGroupModels);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected UserModel lookupExisting(String customerID, SSOUserMapping mapping) {
        try {
            return ssoUserDao.findUserByCustomerID(customerID);
        } catch (UnknownIdentifierException ex) {
            LOGGER.debug(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SSOUserMapping findMapping(Collection<String> roles) {
        // This method is overridden for testing purposes
        return super.findMapping(roles);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected UserModel createNewUser(String id, String name, SSOUserMapping userMapping) {
        // This method is overridden for testing purposes
        return super.createNewUser(id, name, userMapping);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void adjustUserAttributes(UserModel user, SSOUserMapping mapping) {
        // This method is overridden for testing purposes
        super.adjustUserAttributes(user, mapping);
    }
}
