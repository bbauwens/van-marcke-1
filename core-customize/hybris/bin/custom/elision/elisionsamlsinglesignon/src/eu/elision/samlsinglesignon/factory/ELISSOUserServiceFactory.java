package eu.elision.samlsinglesignon.factory;

import de.hybris.platform.core.model.user.UserModel;
import eu.elision.samlsinglesignon.enums.SSOContext;
import org.springframework.security.saml.SAMLCredential;

import javax.servlet.http.HttpServletRequest;

/**
 * A {@link SSOContext} based factory to delegate to the correct SSO service for getting/creating user.
 */
public interface ELISSOUserServiceFactory {

    /**
     * Getting/creating user for the given SSO Context and Saml credential.
     *
     * @param ssoContext     the ssoContext
     * @param samlCredential the samlCredential
     * @param request        the http request
     * @return {@link UserModel} for existing user or for the newly created
     */
    UserModel getOrCreateSSOUser(SSOContext ssoContext, SAMLCredential samlCredential, HttpServletRequest request);
}