package eu.elision.samlsinglesignon.strategy.impl;

import de.hybris.platform.samlsinglesignon.SamlUserGroupDAO;
import de.hybris.platform.samlsinglesignon.model.SamlUserGroupModel;
import eu.elision.samlsinglesignon.strategy.ELISamlUserGroupLookupStrategy;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Default implementation of {@link ELISamlUserGroupLookupStrategy}
 */
public class DefaultELISamlUserGroupLookupStrategy implements ELISamlUserGroupLookupStrategy {

    private final SamlUserGroupDAO samlUserGroupDAO;

    /**
     * Creates a new instance of {@link DefaultELISamlUserGroupLookupStrategy}
     *
     * @param samlUserGroupDAO the samlUserGroupDAO
     */
    public DefaultELISamlUserGroupLookupStrategy(final SamlUserGroupDAO samlUserGroupDAO) {
        this.samlUserGroupDAO = samlUserGroupDAO;
    }

    @Override
    public List<SamlUserGroupModel> getSamlUserGroupsForUserType(final Collection<String> roles, final String userType) {
        checkArgument(isNotEmpty(roles), "Roles must not be empty or null");
        checkArgument(isNotEmpty(userType), "user type must not be empty or null");

        return roles.stream()
                .map(samlUserGroupDAO::findSamlUserGroup)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(e -> userType.equals(e.getUserType().getCode()))
                .collect(Collectors.toList());
    }
}