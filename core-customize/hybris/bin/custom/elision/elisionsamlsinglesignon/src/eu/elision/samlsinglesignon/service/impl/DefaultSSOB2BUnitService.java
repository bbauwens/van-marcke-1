package eu.elision.samlsinglesignon.service.impl;

import com.vanmarcke.cpi.builders.CustomerRequestDataBuilder;
import com.vanmarcke.cpi.data.customer.CustomerInfoResponseData;
import com.vanmarcke.cpi.data.customer.CustomerRequestData;
import com.vanmarcke.cpi.data.customer.CustomerResponseData;
import com.vanmarcke.cpi.services.CustomerService;
import de.hybris.platform.b2b.company.B2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.user.UserService;
import eu.elision.samlsinglesignon.service.SSOB2BUnitService;
import org.springframework.beans.factory.annotation.Required;

import static com.google.common.base.Preconditions.checkState;
import static java.lang.String.format;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

/**
 * Default SSO service for getting/creating user
 */
public class DefaultSSOB2BUnitService extends AbstractBusinessService implements SSOB2BUnitService {

    private transient B2BCommerceUnitService b2BCommerceUnitService;
    private transient UserService userService;
    private transient CustomerService cpiCustomerService;

    private transient Converter<CustomerInfoResponseData, AddressModel> addressModelConverter;
    private transient Converter<CustomerInfoResponseData, B2BUnitModel> b2BUnitModelConverter;

    @Override
    public B2BUnitModel getOrCreateSSOB2BUnit(final String uid) {
        B2BUnitModel unitModel = b2BCommerceUnitService.getUnitForUid(uid);
        if (unitModel == null) {
            final CustomerRequestData request = CustomerRequestDataBuilder.customerRequest()
                    .withCustomerID(uid)
                    .build();

            final CustomerResponseData response = cpiCustomerService.getCustomer(request);

            checkState(response != null && isNotEmpty(response.getData()), format("Cannot retrieve customer '%s' from ERP", uid));

            final CustomerInfoResponseData customerInfo = response.getData().get(0);

            // A B2B Unit without a parent is only allowed to be created by a user belonging to the group 'admingroup'.
            unitModel = getSessionService().executeInLocalView(new SessionExecutionBody() {
                @Override
                public Object execute() {
                    final B2BUnitModel newUnit = getModelService().create(B2BUnitModel.class);
                    b2BUnitModelConverter.convert(customerInfo, newUnit);
                    getModelService().save(newUnit);

                    final AddressModel newAddress = getModelService().create(AddressModel.class);
                    newAddress.setBillingAddress(true);
                    newAddress.setShippingAddress(true);
                    newAddress.setVisibleInAddressBook(true);
                    addressModelConverter.convert(customerInfo, newAddress);
                    saveAddressEntry(newUnit, newAddress);

                    return newUnit;
                }
            }, userService.getAdminUser());
        }
        return unitModel;
    }

    protected void saveAddressEntry(final B2BUnitModel unit, final AddressModel address) {
        unit.setBillingAddress(address);
        unit.setShippingAddress(address);
        b2BCommerceUnitService.saveAddressEntry(unit, address);
    }

    @Required
    public void setB2BCommerceUnitService(final B2BCommerceUnitService b2BCommerceUnitService) {
        this.b2BCommerceUnitService = b2BCommerceUnitService;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    @Required
    public void setCpiCustomerService(final CustomerService cpiCustomerService) {
        this.cpiCustomerService = cpiCustomerService;
    }

    @Required
    public void setAddressModelConverter(final Converter<CustomerInfoResponseData, AddressModel> addressModelConverter) {
        this.addressModelConverter = addressModelConverter;
    }

    @Required
    public void setB2BUnitModelConverter(final Converter<CustomerInfoResponseData, B2BUnitModel> b2BUnitModelConverter) {
        this.b2BUnitModelConverter = b2BUnitModelConverter;
    }
}