package com.elision.hybris.l10n.facades;

import java.util.Locale;

public interface LocalizedMessageFacade {

    String getMessageForCodeAndLocale(String code, Locale locale);

    String getMessageForCodeAndCurrentLocale(String code);

    String getFormattedMessageForCodeAndCurrentLocale(String code, Object[] arguments);

    void createOrUpdateLocalizedMessage(String code, Locale locale, String message);
}
