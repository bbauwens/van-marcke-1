package com.elision.hybris.l10n.facades.impl;

import com.elision.hybris.l10n.facades.LocalizedMessageFacade;
import com.elision.hybris.l10n.model.LocalizedMessageModel;
import com.elision.hybris.l10n.services.LocalizedMessageService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Locale;
import java.util.Optional;

public class DefaultLocalizedMessageFacade implements LocalizedMessageFacade {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultLocalizedMessageFacade.class);

    private final I18NService i18NService;
    private final LocalizedMessageService localizedMessageService;
    private final ModelService modelService;

    @Autowired
    public DefaultLocalizedMessageFacade(I18NService i18NService,
                                         LocalizedMessageService localizedMessageService,
                                         ModelService modelService) {
        this.i18NService = i18NService;
        this.localizedMessageService = localizedMessageService;
        this.modelService = modelService;
    }

    @Override
    public String getMessageForCodeAndLocale(final String code, final Locale locale) {
        if (StringUtils.isEmpty(code) || locale == null) {
            return StringUtils.EMPTY;
        }

        return Optional.ofNullable(this.localizedMessageService.getCachedLocalizedMessageForKey(code))
                .map(message -> message.getMessage(locale))
                .orElse(null);
    }

    @Override
    public String getMessageForCodeAndCurrentLocale(String code) {
        return getMessageForCodeAndLocale(code, i18NService.getCurrentLocale());
    }

    @Override
    public String getFormattedMessageForCodeAndCurrentLocale(final String code, final Object[] arguments) {
        final Locale currentLocale = i18NService.getCurrentLocale();
        final String message = getMessageForCodeAndLocale(code, currentLocale);

        return localizedMessageService.formatLocalizedMessage(message, arguments);
    }

    @Override
    public void createOrUpdateLocalizedMessage(final String code, final Locale locale, final String message) {
        if (StringUtils.isEmpty(code) || locale == null) {
            return;
        }

        try {
            final LocalizedMessageModel messageModel = Optional.ofNullable(this.localizedMessageService.getLocalizedMessageForKey(code))
                    .orElseGet(() -> this.modelService.create(LocalizedMessageModel.class));

            messageModel.setKey(code);
            messageModel.setMessage(message, locale);
            this.modelService.save(messageModel);

            this.localizedMessageService.evictLocalizedMessageFromCache(code);
        } catch (final Exception ex) {
            LOGGER.error("An exception occurred while saving a localized message (key: {}): {}", code, ex.getMessage());
        }
    }
}
