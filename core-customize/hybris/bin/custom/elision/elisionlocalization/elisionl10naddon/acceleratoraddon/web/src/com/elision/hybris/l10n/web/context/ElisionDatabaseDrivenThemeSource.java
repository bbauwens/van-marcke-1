package com.elision.hybris.l10n.web.context;


import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.ui.context.Theme;
import org.springframework.ui.context.ThemeSource;
import org.springframework.ui.context.support.SimpleTheme;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ElisionDatabaseDrivenThemeSource implements ThemeSource {

    private static final Map<String, Theme> themeCache = new ConcurrentHashMap<>();

    private MessageSource databaseDrivenMessageSource;

    @Override
    public Theme getTheme(final String themeName) {
        if (themeName == null) {
            return null;
        }

        final Theme theme = themeCache.get(themeName);
        return (theme != null) ? theme : themeCache.computeIfAbsent(themeName, v -> computeThemeForGivenKey(themeName));
    }

    private Theme computeThemeForGivenKey(final String themeName) {
        return new SimpleTheme(themeName, this.databaseDrivenMessageSource);
    }

    @Required
    public void setDatabaseDrivenMessageSource(final MessageSource databaseDrivenMessageSource) {
        this.databaseDrivenMessageSource = databaseDrivenMessageSource;
    }
}
