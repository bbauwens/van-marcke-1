package com.elision.hybris.l10n.dao.impl;

import com.elision.hybris.l10n.dao.LocalizedMessageDao;
import com.elision.hybris.l10n.model.LocalizedMessageModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import org.springframework.cache.ehcache.EhCacheCacheManager;

public class DefaultLocalizedMessageDao extends AbstractItemDao implements LocalizedMessageDao {

    private static final String LOCALIZED_MESSAGE_FOR_KEY_QUERY = "SELECT {" + LocalizedMessageModel.PK + "} " +
            "FROM {" + LocalizedMessageModel._TYPECODE + "} " +
            "WHERE {" + LocalizedMessageModel.KEY + "} = ?" + LocalizedMessageModel.KEY;

    @Override
    public LocalizedMessageModel getLocalizedMessageForKey(final String key) throws ModelNotFoundException, AmbiguousIdentifierException {
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(LOCALIZED_MESSAGE_FOR_KEY_QUERY);
        searchQuery.addQueryParameter(LocalizedMessageModel.KEY, key);
        return searchUnique(searchQuery);
    }
}
