package com.elision.hybris.l10n.services.impl;


import com.elision.hybris.l10n.dao.LocalizedMessageDao;
import com.elision.hybris.l10n.model.LocalizedMessageModel;
import com.elision.hybris.l10n.services.LocalizedMessageService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultLocalizedMessageServiceTest {

    private LocalizedMessageService localizedMessageService;

    @Mock
    private LocalizedMessageDao localizedMessageDao;
    @Mock
    private I18NService i18NService;

    @Before
    public void setUp() {
        localizedMessageService = new DefaultLocalizedMessageService(localizedMessageDao, i18NService);
    }

    @Test
    public void getLocalizedMessageForKey_should_return_null_for_empty_key() {
        assertThat(localizedMessageService.getLocalizedMessageForKey(StringUtils.EMPTY)).isNull();
    }

    @Test
    public void getLocalizedMessageForKey_should_return_null_for_null_key() {
        assertThat(localizedMessageService.getLocalizedMessageForKey(null)).isNull();
    }

    @Test
    public void getLocalizedMessageForKey_should_return_null_if_localized_message_does_not_exist() {
        when(localizedMessageDao.getLocalizedMessageForKey(anyString())).thenThrow(ModelNotFoundException.class);

        final LocalizedMessageModel localizedMessage = localizedMessageService.getLocalizedMessageForKey(RandomStringUtils.random(5));

        verify(localizedMessageDao, times(1)).getLocalizedMessageForKey(anyString());

        assertThat(localizedMessage).isNull();
    }

    @Test
    public void getLocalizedMessageForKey_should_return_existing_localized_message() {
        final LocalizedMessageModel existingLocalizedMessage = new LocalizedMessageModel();
        when(localizedMessageDao.getLocalizedMessageForKey(anyString())).thenReturn(existingLocalizedMessage);

        final LocalizedMessageModel localizedMessage = localizedMessageService.getLocalizedMessageForKey(RandomStringUtils.random(5));

        verify(localizedMessageDao, times(1)).getLocalizedMessageForKey(anyString());

        assertThat(localizedMessage).isEqualTo(existingLocalizedMessage);
    }
}