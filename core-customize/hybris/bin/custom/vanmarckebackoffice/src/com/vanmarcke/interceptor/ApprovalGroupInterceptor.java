package com.vanmarcke.interceptor;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.servicelayer.user.UserService;

import static com.vanmarcke.constants.VanmarckebackofficeConstants.APPROVAL_GROUP;

/**
 * The {@link ApprovalGroupInterceptor} class us used to check what approval status is being saved by the product
 * maganers.
 *
 * @author Jeroen Vandevelde
 * @since 28-60-2020
 */
public class ApprovalGroupInterceptor implements ValidateInterceptor {

    private UserService userService;

    public ApprovalGroupInterceptor(UserService userService) {
        this.userService = userService;
    }

    /**
     * Overrides onValidate. Checks before saving the product if current user is member of the ApprovalGroup and what
     * the product state has been set to, and handles accordingly.
     */
    @Override
    public void onValidate(Object model, InterceptorContext interceptorContext) throws InterceptorException {
        if (model instanceof ProductModel) {
            UserGroupModel approvalGroup = userService.getUserGroupForUID(APPROVAL_GROUP);
            UserModel currentUser = userService.getCurrentUser();
            ProductModel product = (ProductModel) model;
            if (approvalGroup != null && currentUser != null && userService.isMemberOfGroup(currentUser, approvalGroup) && !isStatusCheckorUnapproved(product)) {
                throw new InterceptorException("You do not have the correct rights to put the approval state on anything else than 'check' or 'unapproved'.");
            }
        }
    }

    /**
     * Checks if the status is check or unapproved
     *
     * @param product the productModel
     * @return the boolean value if the status is check or unapproved
     */
    private boolean isStatusCheckorUnapproved(ProductModel product) {
        return ArticleApprovalStatus.CHECK.equals(product.getApprovalStatus()) || ArticleApprovalStatus.UNAPPROVED.equals(product.getApprovalStatus());
    }
}
