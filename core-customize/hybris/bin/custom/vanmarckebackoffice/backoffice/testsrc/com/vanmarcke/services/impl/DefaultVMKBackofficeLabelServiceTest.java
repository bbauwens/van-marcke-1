package com.vanmarcke.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVMKBackofficeLabelServiceTest {

    @InjectMocks
    private DefaultVMKBackofficeLabelService defaultVMKBackofficeLabelService;

    @Test
    public void testGetDateLabel_withoutDate() {
        String result = defaultVMKBackofficeLabelService.getDateLabel("noDate", null, null);
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetDateLabel_withDate() {
        Date date = Date.from(LocalDateTime.of(2019, 8, 4, 20, 45, 16).toInstant(ZoneOffset.of("+02:00")));

        String result = defaultVMKBackofficeLabelService.getDateLabel(date, "HH:mm", null);
        assertThat(result).isEqualTo("18:45");
    }

    @Test
    public void testGetDateLabel_withDate_withoutDatePattern() {
        Date date = Date.from(LocalDateTime.of(2019, 8, 4, 20, 45, 16).toInstant(ZoneOffset.of("+02:00")));

        String result = defaultVMKBackofficeLabelService.getDateLabel(date, null, null);
        assertThat(result).isEqualTo("2019-08-04");
    }

    @Test
    public void testGetDateLabel_withDate_withPattern() {
        Date date = Date.from(LocalDateTime.of(2019, 8, 4, 20, 45, 16).toInstant(ZoneOffset.of("+02:00")));

        String result = defaultVMKBackofficeLabelService.getDateLabel(date, "HH:mm", "DST");
        assertThat(result).isEqualTo("18:45");
    }

}