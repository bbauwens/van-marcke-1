package com.vanmarcke.widgets.refineby;

import com.hybris.backoffice.widgets.refineby.renderer.StandardFacetRenderer;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.search.data.facet.FacetData;

public class VanmarckeFacetRenderer extends StandardFacetRenderer {

    /**
     * Excludes facets with empty values (Overwrites the default behaviour that also excludes the facets with only one value)
     */
    @Override
    protected boolean shouldFacetBeRendered(FacetData facet, WidgetInstanceManager wim) {
        return !facet.getFacetValues().isEmpty();
    }
}
