package com.vanmarcke.widgets.editors;

import com.hybris.cockpitng.editors.EditorContext;
import com.hybris.cockpitng.editors.EditorListener;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.platformbackoffice.classification.ClassificationInfo;
import de.hybris.platform.platformbackoffice.classification.editor.FeatureValueEditor;
import org.zkoss.zk.ui.Component;

/**
 * Custom implementation of the {@link FeatureValueEditor} editor.
 * Renders only the unit selector widget if an unit is defined for the {@link FeatureValue}.
 */
public class VMKFeatureValueEditor extends FeatureValueEditor {

    public VMKFeatureValueEditor() {
    }

    @Override
    public void render(final Component parent, final EditorContext<FeatureValue> context, final EditorListener<FeatureValue> listener) {
        final ClassificationInfo info = (ClassificationInfo) context.getParameter("classificationInfo");
        context.setParameter("classificationInfo", new VMKClassificationInfo(info));
        super.render(parent, context, listener);
    }
}