package com.vanmarcke.widgets.editors.utils;

import com.hybris.cockpitng.util.UITools;
import com.vanmarcke.dto.VMKListItemAttributeDTO;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.integrationbackoffice.dto.AbstractListItemDTO;
import de.hybris.platform.integrationbackoffice.dto.ListItemAttributeDTO;
import de.hybris.platform.integrationbackoffice.dto.ListItemClassificationAttributeDTO;
import de.hybris.platform.integrationbackoffice.dto.ListItemStructureType;
import de.hybris.platform.integrationbackoffice.services.ReadService;
import de.hybris.platform.integrationbackoffice.utility.QualifierNameUtils;
import de.hybris.platform.integrationbackoffice.widgets.editor.data.ListItemAttributeStatus;
import de.hybris.platform.integrationbackoffice.widgets.editor.data.TreeNodeData;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemAttributeModel;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemModel;
import de.hybris.platform.integrationservices.model.IntegrationObjectModel;
import org.apache.commons.lang.BooleanUtils;
import org.zkoss.zhtml.Button;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VMKEditorUtils {
    private static final String CSS_LISTITEM_DISABLED = "z-listitem-disabled";
    private static final String CSS_LISTITEM_NOT_SUPPORTED = "z-listitem-not-supported";
    private static final String CSS_LISTITEM_NOT_SUPPORTED_CELL = "z-listitem-not-supported-cell";
    private static final String CSS_LISTCELL_UNIQUE = "yw-integrationbackoffice-editor-listbox-checkbox-unique";
    private static final String CSS_LISTCELL_AUTOCREATE = "yw-integrationbackoffice-editor-listbox-checkbox-autocreate";
    private static final String CSS_BUTTON_DROPDOWN = "ye-actiondots-integrationbackoffice-btn";
    private static final String CSS_MENUPOPUP = "ye-inline-editor-row-popup";

    private VMKEditorUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static Comboitem createComboItem(String label, Object value) {
        Comboitem item = new Comboitem(label);
        item.setValue(value);
        return item;
    }

    public static Treeitem createTreeItem(TreeNodeData value, boolean expanded) {
        String label = value.getQualifier() == null ? value.getComposedTypeModel().getCode() : value.getAlias() + " [" + value.getComposedTypeModel().getCode() + "]";
        Treeitem treeitem = new Treeitem();
        Treerow treerow = new Treerow();
        Treecell treecell = new Treecell(label);
        treerow.appendChild(treecell);
        treeitem.appendChild(treerow);
        treeitem.setValue(value);
        treeitem.setOpen(expanded);
        return treeitem;
    }

    public static Listitem createListItem(AbstractListItemDTO dto, boolean isComplex, boolean hasSubtypes, List<String> labels, boolean inEditMode, ReadService readService) {
        Listitem listitem = new Listitem();
        Listcell labelListcell = new Listcell(dto.getAlias());
        Listcell descriptionListcell = new Listcell(dto.getDescription());
        Listcell attributeStatusListcell = new Listcell();
        Span attributeStatusSpan = new Span();
        Listcell uniqueListcell = new Listcell();
        Checkbox uniqueCheckbox = new Checkbox();
        Listcell autocreateListcell = new Listcell();
        Checkbox autocreateCheckbox = new Checkbox();
        Listcell dropdownListcell = new Listcell();
        Button dropdownButton = new Button();
        Menupopup menuPopup = createMenuPopup(labels);
        uniqueListcell.setSclass("yw-integrationbackoffice-editor-listbox-checkbox-unique");
        autocreateListcell.setSclass("yw-integrationbackoffice-editor-listbox-checkbox-autocreate");
        dropdownButton.setSclass("ye-actiondots-integrationbackoffice-btn");
        menuPopup.setSclass("ye-inline-editor-row-popup");
        attributeStatusListcell.appendChild(attributeStatusSpan);
        uniqueListcell.appendChild(uniqueCheckbox);
        autocreateListcell.appendChild(autocreateCheckbox);
        dropdownButton.appendChild(menuPopup);
        dropdownListcell.appendChild(dropdownButton);
        listitem.appendChild(labelListcell);
        listitem.appendChild(descriptionListcell);
        listitem.appendChild(attributeStatusListcell);
        listitem.appendChild(uniqueListcell);
        listitem.appendChild(autocreateListcell);
        listitem.appendChild(dropdownListcell);
        if (dto instanceof VMKListItemAttributeDTO) {
            VMKListItemAttributeDTO attributeDTO = (VMKListItemAttributeDTO)dto;
            ListItemAttributeStatus status = determineAttributeStatus(attributeDTO, readService);
            assignStatusBadgeValues(attributeStatusSpan, status);
            assignUniqueCheckboxRules(attributeDTO, uniqueCheckbox);
            assignAutocreateCheckboxRules(attributeDTO, isComplex, autocreateCheckbox);
            assignListitemRules(attributeDTO, listitem);
        } else {
            assignClassificationRules((ListItemClassificationAttributeDTO)dto, listitem, uniqueCheckbox, autocreateCheckbox);
        }

        assignMenuitemRules(isComplex, hasSubtypes, menuPopup);
        if (!inEditMode) {
            assignEditModeRules(listitem, uniqueCheckbox, autocreateCheckbox, menuPopup);
        }

        listitem.setValue(dto);
        return listitem;
    }

    public static Menupopup createMenuPopup(List<String> labels) {
        Menupopup popup = new Menupopup();
        labels.forEach((label) -> {
            Menuitem item = new Menuitem(label);
            popup.appendChild(item);
        });
        return popup;
    }

    public static Treeitem renameTreeitem(Treeitem treeitem, AbstractListItemDTO dto) {
        if (treeitem != null) {
            TreeNodeData treeNodeData = (TreeNodeData)treeitem.getValue();
            String alias = dto.getAlias();
            treeNodeData.setAlias(alias);
            treeitem.setLabel(alias + " [" + treeNodeData.getComposedTypeModel().getCode() + "]");
        }

        return treeitem;
    }

    public static Treeitem findInTreechildren(String qualifier, Treechildren treechildren) {
        if (treechildren != null) {
            List<Treeitem> children = treechildren.getChildren();
            Iterator var4 = children.iterator();

            while(var4.hasNext()) {
                Treeitem treeitem = (Treeitem)var4.next();
                TreeNodeData treeNodeData = (TreeNodeData)treeitem.getValue();
                if (treeNodeData.getQualifier().equals(qualifier)) {
                    return treeitem;
                }
            }
        }

        return null;
    }

    public static List<AbstractListItemDTO> updateDTOs(List<AbstractListItemDTO> oldDTOs, List<AbstractListItemDTO> newDTOs) {
        newDTOs.forEach((newDTO) -> {
            if (newDTO instanceof VMKListItemAttributeDTO) {
                ListItemAttributeDTO newAttributeDTO = (VMKListItemAttributeDTO)newDTO;
                oldDTOs.forEach((oldDTO) -> {
                    if (oldDTO instanceof VMKListItemAttributeDTO) {
                        ListItemAttributeDTO oldAttributeDTO = (VMKListItemAttributeDTO)oldDTO;
                        if (oldAttributeDTO.getAttributeDescriptor().getQualifier().equals(newAttributeDTO.getAttributeDescriptor().getQualifier())) {
                            oldAttributeDTO.setAlias(newAttributeDTO.getAlias());
                            oldAttributeDTO.setSelected(newAttributeDTO.isSelected());
                            oldAttributeDTO.setCustomUnique(newAttributeDTO.isCustomUnique());
                            oldAttributeDTO.setAutocreate(newAttributeDTO.isAutocreate());
                            oldAttributeDTO.setType(newAttributeDTO.getType());
                        }
                    }

                });
            } else {
                oldDTOs.add(newDTO);
            }

        });
        return oldDTOs;
    }

    public static Map<ComposedTypeModel, List<AbstractListItemDTO>> convertIntegrationObjectToDTOMap(ReadService readService, IntegrationObjectModel integrationObject) {
        return (Map)integrationObject.getItems().stream().collect(Collectors.toMap(IntegrationObjectItemModel::getType, (item) -> {
            return getItemDTOS(readService, item);
        }));
    }

    public static ListItemStructureType getListItemStructureType(ReadService readService, AttributeDescriptorModel attribute) {
        ListItemStructureType structureType;
        if (readService.isCollectionType(attribute.getAttributeType().getItemtype())) {
            structureType = ListItemStructureType.COLLECTION;
        } else if (readService.isMapType(attribute.getAttributeType().getItemtype())) {
            structureType = ListItemStructureType.MAP;
        } else {
            structureType = ListItemStructureType.NONE;
        }

        return structureType;
    }

    public static Set<AttributeDescriptorModel> getStructuredAttributes(List<AbstractListItemDTO> dtoList) {
        Stream<AbstractListItemDTO> var10000 = dtoList.stream();

        var10000 = var10000.filter(VMKListItemAttributeDTO.class::isInstance);
        VMKListItemAttributeDTO.class.getClass();
        return (Set)var10000.map(VMKListItemAttributeDTO.class::cast).filter((dto) -> {
            return dto.getStructureType() == ListItemStructureType.COLLECTION || dto.getStructureType() == ListItemStructureType.MAP;
        }).map(VMKListItemAttributeDTO::getAttributeDescriptor).collect(Collectors.toSet());
    }

    public static Set<TreeNodeData> getReferenceClassificationAttributes(List<AbstractListItemDTO> dtoList) {
        Stream<AbstractListItemDTO> var10000 = dtoList.stream();
        ListItemClassificationAttributeDTO.class.getClass();
        var10000 = var10000.filter(ListItemClassificationAttributeDTO.class::isInstance);
        ListItemClassificationAttributeDTO.class.getClass();
        return (Set)var10000.map(ListItemClassificationAttributeDTO.class::cast).filter((dto) -> {
            return dto.getClassAttributeAssignmentModel().getReferenceType() != null;
        }).map((dto) -> {
            return new TreeNodeData(dto.getClassificationAttributeCode(), dto.getAlias(), dto.getClassAttributeAssignmentModel().getReferenceType());
        }).collect(Collectors.toSet());
    }

    public static Map<ComposedTypeModel, Map<String, List<AbstractListItemDTO>>> compileDuplicationMap(ComposedTypeModel currentType, List<AbstractListItemDTO> currentAttributes, Map<ComposedTypeModel, Map<String, List<AbstractListItemDTO>>> duplicationMap) {
        duplicationMap.putIfAbsent(currentType, new HashMap<>());
        duplicationMap.get(currentType).clear();
        currentAttributes.forEach((dto) -> {
            if (dto.isSelected()) {
                duplicationMap.get(currentType).putIfAbsent(dto.getAlias(), new ArrayList<>());
                ((List)((Map)duplicationMap.get(currentType)).get(dto.getAlias())).add(dto);
            }

        });
        duplicationMap.get(currentType).entrySet().removeIf((name) -> {
            return name.getValue().size() < 2;
        });
        duplicationMap.entrySet().removeIf((type) -> {
            return type.getValue().isEmpty();
        });
        return duplicationMap;
    }

    public static boolean markRowsWithDuplicateNames(Collection<Listitem> items, Map<String, List<AbstractListItemDTO>> duplicateEntries) {
        boolean hasHighlight = false;
        items.forEach((item) -> {
            UITools.removeSClass(item, "integration-object-attribute-name-conflict");
        });
        if (duplicateEntries != null) {
            Iterator var4 = duplicateEntries.values().iterator();

            while(var4.hasNext()) {
                List<AbstractListItemDTO> listItemDTOS = (List)var4.next();

                AbstractListItemDTO entry;
                for(Iterator var6 = listItemDTOS.iterator(); var6.hasNext(); hasHighlight = highlightListitem(items, hasHighlight, entry)) {
                    entry = (AbstractListItemDTO)var6.next();
                }
            }
        }

        return hasHighlight;
    }

    public static boolean isClassificationAttributePresent(ClassAttributeAssignmentModel classAttributeAssignmentModel, List<AbstractListItemDTO> dtos) {
        Iterator var3 = dtos.iterator();

        while(var3.hasNext()) {
            AbstractListItemDTO dto = (AbstractListItemDTO)var3.next();
            if (dto instanceof ListItemClassificationAttributeDTO) {
                ListItemClassificationAttributeDTO classificationAttributeDTO = (ListItemClassificationAttributeDTO)dto;
                String dtoAttributeCode = classificationAttributeDTO.getClassificationAttributeCode();
                String assignmentModelCode = QualifierNameUtils.removeNonAlphaNumericCharacters(classAttributeAssignmentModel.getClassificationAttribute().getCode());
                String dtoCategory = classificationAttributeDTO.getCategoryCode();
                String assignmentModelCategory = classAttributeAssignmentModel.getClassificationClass().getCode();
                if (dtoAttributeCode.equals(assignmentModelCode) && dtoCategory.equals(assignmentModelCategory)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static <T extends ListItemAttributeDTO> ListItemAttributeStatus determineAttributeStatus(T dto, ReadService readService) {
        if (!dto.isSupported()) {
            return ListItemAttributeStatus.NOT_SUPPORTED;
        } else if (dto.isRequired()) {
            return ListItemAttributeStatus.REQUIRED;
        } else {
            return readService.getReadOnlyAttributes().contains(dto.getAttributeDescriptor().getQualifier()) ? ListItemAttributeStatus.READ_ONLY : ListItemAttributeStatus.NONE;
        }
    }

    private static void assignEditModeRules(Listitem listitem, Checkbox uniqueCheckbox, Checkbox autocreateCheckbox, Menupopup menuPopup) {
        uniqueCheckbox.setDisabled(true);
        autocreateCheckbox.setDisabled(true);
        listitem.setSclass("z-listitem-disabled");
        ((Component)menuPopup.getChildren().get(1)).setVisible(false);
        ((Component)menuPopup.getChildren().get(2)).setVisible(false);
    }

    private static void assignUniqueCheckboxRules(ListItemAttributeDTO dto, Checkbox attrUniqueCheckbox) {
        ListItemStructureType structureType = dto.getStructureType();
        if (structureType != ListItemStructureType.COLLECTION && structureType != ListItemStructureType.MAP) {
            if (dto.getAttributeDescriptor().getUnique()) {
                attrUniqueCheckbox.setChecked(true);
                attrUniqueCheckbox.setDisabled(true);
            } else {
                attrUniqueCheckbox.setChecked(dto.isCustomUnique());
            }
        } else {
            attrUniqueCheckbox.setDisabled(true);
        }

    }

    private static void assignAutocreateCheckboxRules(ListItemAttributeDTO dto, boolean isComplex, Checkbox attrAutocreateCheckbox) {
        boolean isUnique = dto.getAttributeDescriptor().getUnique();
        boolean isAbstract = dto.getType() instanceof ComposedTypeModel ? ((ComposedTypeModel)dto.getType()).getAbstract() : false;
        if (isComplex && !isUnique && !isAbstract) {
            attrAutocreateCheckbox.setChecked(dto.isAutocreate());
        } else {
            attrAutocreateCheckbox.setDisabled(true);
        }

    }

    private static void assignListitemRules(ListItemAttributeDTO dto, Listitem child) {
        if (dto.isSupported()) {
            if (dto.isRequired()) {
                child.setSelected(true);
                child.setSclass("z-listitem-disabled");
            } else if (dto.isCustomUnique()) {
                child.setSelected(true);
            } else {
                child.setSelected(dto.isSelected());
            }
        } else {
            child.setSclass("z-listitem-not-supported");
            ((Listcell)child.getChildren().get(0)).setSclass("z-listitem-not-supported-cell");
            ((Listcell)child.getChildren().get(1)).setSclass("z-listitem-not-supported-cell");
        }

    }

    private static void assignClassificationRules(ListItemClassificationAttributeDTO dto, Listitem listitem, Checkbox uniqueCheckbox, Checkbox autocreateCheckbox) {
        listitem.setSelected(dto.isSelected());
        uniqueCheckbox.setDisabled(true);
        autocreateCheckbox.setChecked(dto.isAutocreate());
        autocreateCheckbox.setDisabled(dto.getClassAttributeAssignmentModel().getReferenceType() == null);
    }

    private static void assignMenuitemRules(boolean isComplex, boolean hasSubtypes, Menupopup detailsMenu) {
        if (!isComplex || !hasSubtypes) {
            detailsMenu.getLastChild().setVisible(false);
        }

    }

    private static List<AbstractListItemDTO> getItemDTOS(ReadService readService, IntegrationObjectItemModel item) {
        List<ListItemAttributeDTO> attributes = (List)item.getAttributes().stream().map((attr) -> {
            return toListItemAttributeDTO(readService, attr);
        }).collect(Collectors.toList());
        List<ListItemClassificationAttributeDTO> classificationAttributes = (List)item.getClassificationAttributes().stream().map((attr) -> {
            return new ListItemClassificationAttributeDTO(true, false, BooleanUtils.isTrue(attr.getAutoCreate()), attr.getClassAttributeAssignment(), attr.getAttributeName());
        }).collect(Collectors.toList());
        List<AbstractListItemDTO> combinedAttributes = new ArrayList();
        combinedAttributes.addAll(attributes);
        combinedAttributes.addAll(classificationAttributes);
        return combinedAttributes;
    }

    private static ListItemAttributeDTO toListItemAttributeDTO(ReadService readService, IntegrationObjectItemAttributeModel attribute) {
        AttributeDescriptorModel attributeDescriptor = attribute.getAttributeDescriptor();
        boolean customUnique = BooleanUtils.isTrue(attribute.getUnique()) && BooleanUtils.isNotTrue(attributeDescriptor.getUnique());
        boolean autocreate = BooleanUtils.isTrue(attribute.getAutoCreate());
        ListItemStructureType structureType = getListItemStructureType(readService, attributeDescriptor);
        String alias = attribute.getAttributeName();
        TypeModel type = attribute.getReturnIntegrationObjectItem() != null ? attribute.getReturnIntegrationObjectItem().getType() : attributeDescriptor.getAttributeType();
        return new VMKListItemAttributeDTO(true, customUnique, autocreate, attributeDescriptor, structureType, alias, (TypeModel)type);
    }

    private static boolean highlightListitem(Collection<Listitem> items, boolean hasHighlight, AbstractListItemDTO entry) {
        Iterator var4 = items.iterator();

        while(var4.hasNext()) {
            Listitem item = (Listitem)var4.next();
            if (item.getValue().equals(entry)) {
                UITools.addSClass(item, "integration-object-attribute-name-conflict");
                hasHighlight = true;
            }
        }

        return hasHighlight;
    }

    private static void assignStatusBadgeValues(Span statusBadge, ListItemAttributeStatus status) {
        Label statusLabel = new Label(status.getLabel());
        statusBadge.setSclass(status.getStyling());
        statusBadge.appendChild(statusLabel);
    }

}
