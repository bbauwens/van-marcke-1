package com.vanmarcke.widgets.editors.controllers;

import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.annotations.GlobalCockpitEvent;
import com.hybris.cockpitng.annotations.SocketEvent;
import com.hybris.cockpitng.annotations.ViewEvent;
import com.hybris.cockpitng.core.events.CockpitEvent;
import com.hybris.cockpitng.core.events.CockpitEventQueue;
import com.hybris.cockpitng.core.events.impl.DefaultCockpitEvent;
import com.hybris.cockpitng.util.DefaultWidgetController;
import com.hybris.cockpitng.util.notifications.NotificationService;
import com.vanmarcke.dto.VMKListItemAttributeDTO;
import com.vanmarcke.widgets.editors.utils.VMKEditorUtils;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.integrationbackoffice.dto.AbstractListItemDTO;
import de.hybris.platform.integrationbackoffice.dto.ListItemAttributeDTO;
import de.hybris.platform.integrationbackoffice.dto.ListItemClassificationAttributeDTO;
import de.hybris.platform.integrationbackoffice.dto.ListItemStructureType;
import de.hybris.platform.integrationbackoffice.services.ReadService;
import de.hybris.platform.integrationbackoffice.services.WriteService;
import de.hybris.platform.integrationbackoffice.utility.ItemTypeMatchSelector;
import de.hybris.platform.integrationbackoffice.widgets.editor.data.IntegrationFilterState;
import de.hybris.platform.integrationbackoffice.widgets.editor.data.SubtypeData;
import de.hybris.platform.integrationbackoffice.widgets.editor.data.TreeNodeData;
import de.hybris.platform.integrationbackoffice.widgets.editor.utility.EditorAccessRights;
import de.hybris.platform.integrationbackoffice.widgets.editor.utility.EditorAttributesFilteringService;
import de.hybris.platform.integrationbackoffice.widgets.editor.utility.EditorBlacklists;
import de.hybris.platform.integrationbackoffice.widgets.editor.utility.EditorTrimmer;

import de.hybris.platform.integrationbackoffice.widgets.editor.utility.EditorValidator;
import de.hybris.platform.integrationbackoffice.widgets.editor.utility.IntegrationObjectRootUtils;
import de.hybris.platform.integrationbackoffice.widgets.modals.builders.AuditReportBuilder;
import de.hybris.platform.integrationbackoffice.widgets.modals.data.CreateIntegrationObjectModalData;
import de.hybris.platform.integrationbackoffice.widgets.modals.data.RenameAttributeModalData;
import de.hybris.platform.integrationbackoffice.widgets.modals.data.SelectedClassificationAttributesData;
import de.hybris.platform.integrationbackoffice.widgets.providers.ClassificationAttributeQualifierProvider;
import de.hybris.platform.integrationservices.enums.ItemTypeMatchEnum;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemModel;
import de.hybris.platform.integrationservices.model.IntegrationObjectModel;
import de.hybris.platform.integrationservices.util.Log;
import org.assertj.core.util.Arrays;
import org.slf4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VMKIntegrationObjectEditorController extends DefaultWidgetController {
    private static final Logger LOG = Log.getLogger(VMKIntegrationObjectEditorController.class);
    private static final int MAX_DEPTH = 5;
    private static final String ASCENDING = "ascending";
    private static final String DESCENDING = "descending";
    @WireVariable
    private transient WriteService writeService;
    @WireVariable
    private transient ReadService readService;
    @WireVariable
    private transient EditorAttributesFilteringService editorAttrFilterService;
    @WireVariable
    private transient CockpitEventQueue cockpitEventQueue;
    @WireVariable
    private transient NotificationService notificationService;
    @WireVariable
    private transient EditorAccessRights editorAccessRights;
    @WireVariable
    private transient AuditReportBuilder auditReportBuilder;
    @WireVariable
    private transient ClassificationAttributeQualifierProvider classificationAttributeQualifierProvider;
    @Resource
    private transient ItemTypeMatchSelector itemTypeMatchSelector;
    private Tree composedTypeTree;
    private Listbox attributesListBox;
    private Listheader attributeNameListheader;
    private Listheader descriptionListheader;
    private List<String> attributeMenuPopupLabels;
    private Deque<ComposedTypeModel> ancestors;
    private boolean editModeFlag;
    private transient IntegrationFilterState filterState;
    private transient Map<ComposedTypeModel, List<AbstractListItemDTO>> currentAttributesMap;
    private transient Map<ComposedTypeModel, ItemTypeMatchEnum> itemTypeMatchMap;
    private transient Set<SubtypeData> subtypeDataSet;
    private transient boolean isModified;
    private IntegrationObjectModel selectedIntegrationObject;
    private ComposedTypeModel selectedComposedType;
    private transient Map<ComposedTypeModel, Map<String, List<AbstractListItemDTO>>> attributeDuplicationMap;

    public VMKIntegrationObjectEditorController() {
        this.filterState = IntegrationFilterState.SHOW_ALL;
        this.itemTypeMatchMap = new HashMap();
    }

    public void initialize(Component component) {
        super.initialize(component);
        this.setEditModeFlag(this.editorAccessRights.isUserAdmin());
        this.setModified(false);
        this.attributeMenuPopupLabels = new ArrayList();
        this.attributeMenuPopupLabels.add(this.getLabel("integrationbackoffice.editMode.menuItem.viewDetails"));
        this.attributeMenuPopupLabels.add(this.getLabel("integrationbackoffice.editMode.menuItem.renameAttribute"));
        this.attributeMenuPopupLabels.add(this.getLabel("integrationbackoffice.editMode.menuItem.retypeAttribute"));
    }

    public void setReadService(ReadService readService) {
        this.readService = readService;
    }

    public Tree getComposedTypeTree() {
        return this.composedTypeTree;
    }

    public void setComposedTypeTree(Tree composedTypeTree) {
        this.composedTypeTree = composedTypeTree;
    }

    public Listbox getAttributesListBox() {
        return this.attributesListBox;
    }

    public void setAttributesListBox(Listbox attributesListBox) {
        this.attributesListBox = attributesListBox;
    }

    public Deque<ComposedTypeModel> getAncestors() {
        return this.ancestors;
    }

    public void setAncestors(Deque<ComposedTypeModel> ancestors) {
        this.ancestors = ancestors;
    }

    public boolean getEditModeFlag() {
        return this.editModeFlag;
    }

    public void setEditModeFlag(boolean editModeFlag) {
        this.editModeFlag = editModeFlag;
    }

    public IntegrationFilterState getFilterState() {
        return this.filterState;
    }

    public void setFilterState(IntegrationFilterState state) {
        this.filterState = state;
    }

    public Map<ComposedTypeModel, List<AbstractListItemDTO>> getCurrentAttributesMap() {
        return this.currentAttributesMap;
    }

    public void setCurrentAttributesMap(Map<ComposedTypeModel, List<AbstractListItemDTO>> currentAttributesMap) {
        this.currentAttributesMap = currentAttributesMap;
    }

    public Map<ComposedTypeModel, ItemTypeMatchEnum> getItemTypeMatchMap() {
        return this.itemTypeMatchMap;
    }

    public void setItemTypeMatchMap(Map<ComposedTypeModel, ItemTypeMatchEnum> itemTypeMatchMap) {
        this.itemTypeMatchMap = itemTypeMatchMap;
    }

    public Set<SubtypeData> getSubtypeDataSet() {
        return this.subtypeDataSet;
    }

    public void setSubtypeDataSet(Set<SubtypeData> subtypeDataSet) {
        this.subtypeDataSet = subtypeDataSet;
    }

    public boolean isModified() {
        return this.isModified;
    }

    public void setModified(boolean modified) {
        this.isModified = modified;
    }

    public IntegrationObjectModel getSelectedIntegrationObject() {
        return this.selectedIntegrationObject;
    }

    public void setSelectedIntegrationObject(IntegrationObjectModel integrationObject) {
        this.selectedIntegrationObject = integrationObject;
    }

    public ComposedTypeModel getSelectedComposedType() {
        return this.selectedComposedType;
    }

    protected NotificationService getNotificationService() {
        return this.notificationService;
    }

    public Map<ComposedTypeModel, Map<String, List<AbstractListItemDTO>>> getAttributeDuplicationMap() {
        return this.attributeDuplicationMap;
    }

    public AuditReportBuilder getDefaultAuditReportBuilder() {
        return this.auditReportBuilder;
    }

    public void setDefaultAuditReportBuilder(AuditReportBuilder auditReportBuilder) {
        this.auditReportBuilder = auditReportBuilder;
    }

    @GlobalCockpitEvent(
            eventName = "objectsDeleted",
            scope = "session"
    )
    public void handleIntegrationObjectDeletedEvent(CockpitEvent event) {
        Stream var10000 = event.getDataAsCollection().stream();
        IntegrationObjectModel.class.getClass();
        if (var10000.anyMatch(IntegrationObjectModel.class::isInstance)) {
            this.clearSelectedIntegrationObject();
        }

    }

    @ViewEvent(
            componentID = "composedTypeTree",
            eventName = "onSelect"
    )
    public void composedTypeTreeOnSelect() {
        TreeNodeData tnd = this.getComposedTypeTree().getSelectedItem().getValue();
        this.selectedComposedType = tnd.getComposedTypeModel();
        this.sendOutput("sendClickedItem", this.selectedComposedType.getCode());
        this.populateListBox(this.selectedComposedType);
    }

    @SocketEvent(
            socketId = "receiveIntegrationObjectComboBox"
    )
    public void loadIntegrationObject(IntegrationObjectModel integrationObject) {
        this.setSelectedIntegrationObject(IntegrationObjectRootUtils.resolveIntegrationObjectRoot(integrationObject));
        this.attributeDuplicationMap = new HashMap();
        IntegrationObjectItemModel root = this.selectedIntegrationObject.getRootItem();
        if (root != null) {
            Map<ComposedTypeModel, List<AbstractListItemDTO>> convertedMap = VMKEditorUtils.convertIntegrationObjectToDTOMap(readService, this.selectedIntegrationObject);
            this.compileSubtypeDataSet(convertedMap);
            this.createTree(root.getType(), convertedMap);
            Map<ComposedTypeModel, List<AbstractListItemDTO>> trimmedMap = EditorTrimmer.trimMap(this.readService, this.getCurrentAttributesMap(), this.getComposedTypeTree(), false);
            if (!"".equals(EditorValidator.validateHasKey(trimmedMap))) {
                this.showObjectLoadedFurtherConfigurationMessage();
            }
        } else {
            this.clearTree();
            this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.invalidObjectLoaded"));
        }

        this.setModified(false);
    }

    @SocketEvent(
            socketId = "createIntegrationObjectEvent"
    )
    public void createNewIntegrationObject(CreateIntegrationObjectModalData data) {
        this.setModified(false);
        this.attributeDuplicationMap = new HashMap();
        this.setFilterState(IntegrationFilterState.SHOW_ALL);
        this.setSubtypeDataSet(new HashSet());
        this.sendOutput("filterStateChangeOutput", IntegrationFilterState.SHOW_ALL);
        this.createTree(data.getComposedTypeModel(), Collections.emptyMap());
        Map<ComposedTypeModel, List<AbstractListItemDTO>> trimmedMap = this.createIntegrationObject(data);
        if (!"".equals(EditorValidator.validateHasKey(trimmedMap))) {
            this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.serviceCreatedNeedsFurtherConfig"));
        } else {
            this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, this.getLabel("integrationbackoffice.editMode.info.msg.serviceCreated"));
        }

    }

    @SocketEvent(
            socketId = "saveEvent"
    )
    public void updateIntegrationObject(String message) {
        Map<ComposedTypeModel, List<AbstractListItemDTO>> trimmedAttributesMap = EditorTrimmer.trimMap(this.readService, this.getCurrentAttributesMap(), this.getComposedTypeTree(), false);
        if (this.validation(trimmedAttributesMap)) {
            this.setItemTypeMatchMap(this.getItemTypeMatchForIntegrationObjectItem(this.selectedIntegrationObject));
            IntegrationObjectModel ioModel = this.setItemTypeMatchForIntegrationObject(this.writeService.createDefinitions(this.selectedIntegrationObject, trimmedAttributesMap, this.selectedIntegrationObject.getRootItem().getCode()));
            this.getItemTypeMatchMap().clear();
            if (this.selectedIntegrationObject.getRootItem() != null) {
                this.persistenceSetup(ioModel);
            } else {
                Messagebox.show(this.getLabel("integrationbackoffice.editMode.warning.msg.saveConfirmation"), this.getLabel("integrationbackoffice.editMode.warning.title.saveConfirmation"), new Messagebox.Button[]{Messagebox.Button.OK, Messagebox.Button.CANCEL}, new String[]{this.getLabel("integrationbackoffice.editMode.button.saveDefinition")}, null, null, (clickEvent) -> {
                    if (clickEvent.getButton() == Messagebox.Button.OK) {
                        this.persistenceSetup(ioModel);
                        this.setModified(true);
                    }

                });
            }
        }

    }

    @SocketEvent(
            socketId = "refreshEvent"
    )
    public void refreshButtonOnClick(String message) {
        if (this.selectedIntegrationObject != null) {
            this.loadIntegrationObject(this.selectedIntegrationObject);
            this.setModified(false);
            this.attributeDuplicationMap.clear();
        }

    }

    @SocketEvent(
            socketId = "receiveDelete"
    )
    public void deleteActionOnPerform() {
        if (this.selectedIntegrationObject != null) {
            Messagebox.show(this.getLabel("integrationbackoffice.editMode.info.msg.deleteConfirmation", Arrays.array(this.selectedIntegrationObject.getCode())), this.getLabel("integrationbackoffice.editMode.info.title.deleteConfirmation"), new Messagebox.Button[]{Messagebox.Button.OK, Messagebox.Button.CANCEL}, null, null, null, (clickEvent) -> {
                if (clickEvent.getButton() == Messagebox.Button.OK) {
                    this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, this.getLabel("integrationbackoffice.editMode.info.msg.delete"));
                    this.writeService.deleteIntegrationObject(this.selectedIntegrationObject);
                    this.cockpitEventQueue.publishEvent(new DefaultCockpitEvent("objectsDeleted", this.selectedIntegrationObject, null));
                }

            });
        } else {
            this.showNoServiceSelectedMessage();
        }

    }

    @SocketEvent(
            socketId = "metadataModalEvent"
    )
    public void metaDataModelRequestHandler(String message) {
        this.sendCurrentIntegrationObject("openMetadataViewer");
    }

    private void sendCurrentIntegrationObject(String message) {
        if (this.selectedIntegrationObject != null && this.selectedIntegrationObject.getRootItem() != null) {
            Map<ComposedTypeModel, List<AbstractListItemDTO>> trimmedMap = EditorTrimmer.trimMap(this.readService, this.getCurrentAttributesMap(), this.getComposedTypeTree(), false);
            if (!"".equals(EditorValidator.validateHasKey(trimmedMap))) {
                this.showObjectLoadedFurtherConfigurationMessage();
            } else {
                this.sendOutput(message, this.selectedIntegrationObject);
            }
        } else {
            this.showNoServiceSelectedMessage();
        }

    }

    @SocketEvent(
            socketId = "openItemTypeIOIModalEvent"
    )
    public void itemTypeIOIModalRequestHandler(String message) {
        this.sendCurrentIntegrationObject("openItemTypeIOIModal");
    }

    @SocketEvent(
            socketId = "auditReportEvent"
    )
    public void downloadIntegrationObjectReport() {
        if (this.getSelectedIntegrationObject() != null && this.getSelectedIntegrationObject().getRootItem() != null) {
            Map<String, InputStream> auditReportMapRes = this.auditReportBuilder.generateAuditReport(this.getSelectedIntegrationObject());
            if (!auditReportMapRes.isEmpty()) {
                LOG.info("Audit Report has been Created Successfully!");
                this.auditReportBuilder.downloadAuditReport(auditReportMapRes);
            } else {
                LOG.info("Audit Report Creation Failed!");
            }
        } else {
            this.showNoServiceSelectedMessage();
        }

    }

    @SocketEvent(
            socketId = "receiveClone"
    )
    public void cloneActionOnPerform() {
        if (this.selectedIntegrationObject != null && this.selectedIntegrationObject.getRootItem() != null) {
            if (!this.isModified()) {
                this.sendOutput("openCloneModal", this.selectedIntegrationObject);
            } else {
                this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.saveBeforeCloning"));
            }
        } else {
            this.showNoServiceSelectedMessage();
        }

    }

    @SocketEvent(
            socketId = "saveButtonItemTypeMatch"
    )
    public void itemTypeMatchSaveHandler(Collection<IntegrationObjectItemModel> integrationObjectItemModels) {
        LOG.debug("The number of items to be saved {}", integrationObjectItemModels.size());
        this.writeService.persistIntegrationObjectItems(integrationObjectItemModels);
        this.cockpitEventQueue.publishEvent(new DefaultCockpitEvent("objectsUpdated", this.selectedIntegrationObject, null));
        this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, this.getLabel("integrationbackoffice.itemTypeMatchIOIModal.msg.save"));
    }

    @SocketEvent(
            socketId = "cloneIntegrationObjectEvent"
    )
    public void cloneIntegrationObject(CreateIntegrationObjectModalData data) {
        if (data.getComposedTypeModel() != null) {
            this.filterStateChange(IntegrationFilterState.SHOW_ALL);
            this.sendOutput("filterStateChangeOutput", IntegrationFilterState.SHOW_ALL);
            this.setModified(false);
            this.setItemTypeMatchMap(this.getItemTypeMatchForIntegrationObjectItem(this.selectedIntegrationObject));
            Map<ComposedTypeModel, List<AbstractListItemDTO>> trimmedMap = this.createIntegrationObject(data);
            this.getItemTypeMatchMap().clear();
            if (!"".equals(EditorValidator.validateHasKey(trimmedMap))) {
                this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.serviceClonedNeedsFurtherConfig"));
            } else {
                this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, this.getLabel("integrationbackoffice.editMode.info.msg.serviceCloned"));
            }
        } else {
            this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.integrationContextLost"));
        }

    }

    @SocketEvent(
            socketId = "renameAttributeEvent"
    )
    public void renameAttribute(RenameAttributeModalData renameAttributeModalData) {
        ComposedTypeModel parentComposedType = renameAttributeModalData.getParent();
        AbstractListItemDTO updatedDTO = renameAttributeModalData.getDto();
        String alias = updatedDTO.getAlias();
        AbstractListItemDTO matchedDTO;
        Stream var10000;
        if (updatedDTO instanceof VMKListItemAttributeDTO) {
            AttributeDescriptorModel attributeDescriptor = ((ListItemAttributeDTO) updatedDTO).getAttributeDescriptor();
            var10000 = ((List) this.getCurrentAttributesMap().get(parentComposedType)).stream();

            var10000 = var10000.filter(VMKListItemAttributeDTO.class::isInstance);

            Optional<ListItemAttributeDTO> optionalListItemAttributeDTO = var10000.map(ListItemAttributeDTO.class::cast).filter((listItemDTO) -> {
                return ((ListItemAttributeDTO) listItemDTO).getAttributeDescriptor().equals(attributeDescriptor);
            }).findFirst();
            matchedDTO = optionalListItemAttributeDTO.orElseThrow(() -> {
                return new NoSuchElementException("No AttributeDescriptor was found");
            });
        } else {
            String categoryCode = ((ListItemClassificationAttributeDTO) updatedDTO).getCategoryCode();
            String classificationAttributeCode = ((ListItemClassificationAttributeDTO) updatedDTO).getClassificationAttributeCode();
            var10000 = ((List) this.getCurrentAttributesMap().get(parentComposedType)).stream();
            ListItemClassificationAttributeDTO.class.getClass();
            var10000 = var10000.filter(ListItemClassificationAttributeDTO.class::isInstance);
            ListItemClassificationAttributeDTO.class.getClass();
            Optional<ListItemClassificationAttributeDTO> optionalListItemClassificationAttributeDTO = var10000.map(ListItemClassificationAttributeDTO.class::cast).filter((listItemDTO) -> {
                return ((ListItemClassificationAttributeDTO) listItemDTO).getCategoryCode().equals(categoryCode) && ((ListItemClassificationAttributeDTO) listItemDTO).getClassificationAttributeCode().equals(classificationAttributeCode);
            }).findFirst();
            matchedDTO = optionalListItemClassificationAttributeDTO.orElseThrow(() -> {
                return new NoSuchElementException(String.format("No ClassificationAttribute was found for %s", classificationAttributeCode));
            });
        }

        matchedDTO.setAlias(alias);
        matchedDTO.setSelected(true);
        if (matchedDTO instanceof VMKListItemAttributeDTO) {
            ListItemAttributeDTO listItemAttributeDTO = (VMKListItemAttributeDTO) matchedDTO;
            this.checkTreeNodeForStructuredType(listItemAttributeDTO);
            if (this.readService.isComplexType(listItemAttributeDTO.getType())) {
                this.getComposedTypeTree().getItems().forEach((treeitem) -> {
                    TreeNodeData treeNodeData = treeitem.getValue();
                    if (parentComposedType.equals(treeNodeData.getComposedTypeModel())) {
                        String qualifier = listItemAttributeDTO.getAttributeDescriptor().getQualifier();
                        Treeitem childTreeitem = VMKEditorUtils.findInTreechildren(qualifier, treeitem.getTreechildren());
                        VMKEditorUtils.renameTreeitem(childTreeitem, matchedDTO);
                    }

                });
            }
        } else {
            ListItemClassificationAttributeDTO classificationAttributeDTO = (ListItemClassificationAttributeDTO) matchedDTO;
            if (classificationAttributeDTO.getClassAttributeAssignmentModel().getReferenceType() != null) {
                this.getComposedTypeTree().getItems().forEach((treeitem) -> {
                    TreeNodeData treeNodeData = treeitem.getValue();
                    if (parentComposedType.equals(treeNodeData.getComposedTypeModel())) {
                        String qualifier = classificationAttributeDTO.getClassificationAttributeCode();
                        Treeitem childTreeitem = VMKEditorUtils.findInTreechildren(qualifier, treeitem.getTreechildren());
                        VMKEditorUtils.renameTreeitem(childTreeitem, matchedDTO);
                    }

                });
            }
        }

        this.autoSelectAttributeRelation(this.getComposedTypeTree().getSelectedItem());
        if (this.readService.isProductType(this.getSelectedComposedType().getCode())) {
            this.attributeDuplicationMap = VMKEditorUtils.compileDuplicationMap(this.getSelectedComposedType(), this.getCurrentAttributesMap().get(this.getSelectedComposedType()), this.attributeDuplicationMap);
        }

        this.populateListBox(parentComposedType);
        this.focusOnListitem(alias);
        this.enableSaveButton();
    }

    @SocketEvent(
            socketId = "retypeAttributeEvent"
    )
    public void retypeAttribute(SubtypeData subtypeData) {
        String alias = subtypeData.getAttributeAlias();
        ComposedTypeModel parentComposedType = subtypeData.getParentNodeType();
        ComposedTypeModel newComposedType = this.readService.getComposedTypeModelFromTypeModel(subtypeData.getSubtype());
        Treeitem currentTreeitem = this.getComposedTypeTree().getSelectedItem();
        Stream var10000 = ((List) this.getCurrentAttributesMap().get(parentComposedType)).stream();

        var10000 = var10000.filter(VMKListItemAttributeDTO.class::isInstance);

        Optional<ListItemAttributeDTO> optionalListItemAttributeDTO = var10000.map(ListItemAttributeDTO.class::cast).filter((listItemDTO) -> {
            return ((ListItemAttributeDTO) listItemDTO).getAlias().equals(alias);
        }).findFirst();
        ListItemAttributeDTO dto = optionalListItemAttributeDTO.orElseThrow(() -> {
            return new NoSuchElementException(String.format("No ListItemAttribute was found for attribute with alias %s", alias));
        });
        dto.setType(newComposedType);
        dto.setSelected(true);
        if (dto.isAutocreate() && newComposedType.getAbstract()) {
            dto.setAutocreate(false);
            this.getNotificationService().notifyUser("integrationbackoffice", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.abstractTypeAutocreate", Arrays.array(dto.getAlias())));
        }

        this.autoSelectAttributeRelation(currentTreeitem);
        this.populateAttributesMap(newComposedType);
        this.getSubtypeDataSet().add(subtypeData);
        String dtoAttributeDescriptorQualifier = dto.getAttributeDescriptor().getQualifier();
        if (this.readService.isComplexType(dto.getType())) {
            Treechildren nodeChildren = currentTreeitem.getTreechildren();
            if (VMKEditorUtils.findInTreechildren(dtoAttributeDescriptorQualifier, nodeChildren) == null) {
                this.createDynamicTreeNode(dto.getAttributeDescriptor().getQualifier(), dto.getAlias(), dto.getType());
            }
        }

        List<Treeitem> treeitems = new ArrayList(this.getComposedTypeTree().getItems());
        Iterator iterator = treeitems.iterator();

        while (iterator.hasNext()) {
            Treeitem treeitem = (Treeitem) iterator.next();
            TreeNodeData treeNodeData = treeitem.getValue();
            ComposedTypeModel composedTypeModel = treeNodeData.getComposedTypeModel();
            if (parentComposedType.equals(composedTypeModel)) {
                Treechildren treechildren = treeitem.getTreechildren();
                Treeitem childTreeitem = VMKEditorUtils.findInTreechildren(dtoAttributeDescriptorQualifier, treechildren);
                if (childTreeitem != null) {
                    TreeNodeData childTreeNodeData = childTreeitem.getValue();
                    childTreeNodeData.setComposedTypeModel(newComposedType);
                    Treeitem updatedTreeitem = VMKEditorUtils.createTreeItem(childTreeNodeData, childTreeitem.isOpen());
                    updatedTreeitem.appendChild(new Treechildren());
                    treechildren.removeChild(childTreeitem);
                    treechildren.appendChild(updatedTreeitem);
                    this.setAncestors(this.determineTreeitemAncestors(updatedTreeitem));
                    this.populateTree(updatedTreeitem, Collections.emptyMap());
                    iterator.remove();
                }
            }
        }

        LOG.info("Base type {} updated to subtype {} under parent of type {}", subtypeData.getBaseType().getCode(), newComposedType.getCode(), parentComposedType.getCode());
        Events.sendEvent("onSelect", this.getComposedTypeTree(), currentTreeitem);
        this.focusOnListitem(alias);
        this.enableSaveButton();
    }

    @SocketEvent(
            socketId = "filterStateChangeInput"
    )
    public void filterStateChange(IntegrationFilterState state) {
        this.setFilterState(state);
        IntegrationObjectItemModel root = this.selectedIntegrationObject.getRootItem();
        if (root != null) {
            if (this.getEditModeFlag()) {
                this.createTree(root.getType(), EditorTrimmer.trimMap(this.readService, this.getCurrentAttributesMap(), this.getComposedTypeTree(), false));
            } else {
                this.createTree(root.getType(), VMKEditorUtils.convertIntegrationObjectToDTOMap(this.readService, this.selectedIntegrationObject));
            }
        } else {
            this.clearTree();
            this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.invalidObjectLoaded"));
        }

    }

    /**
     * @deprecated
     */
    @Deprecated(
            since = "2005",
            forRemoval = true
    )
    public void addClassificationAttributes(Collection<ClassAttributeAssignmentModel> attributes) {
        this.addClassificationAttributes(new SelectedClassificationAttributesData(attributes, false));
    }

    @SocketEvent(
            socketId = "addClassificationAttributesEvent"
    )
    public void addClassificationAttributes(SelectedClassificationAttributesData selectedClassificationAttributesData) {
        if (!this.readService.isProductType(this.getSelectedComposedType().getCode())) {
            this.getNotificationService().notifyUser("integrationbackoffice", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.cannotAddClassificationAttributeToNotProductType"));
        } else {
            boolean attributeAlreadyPresent = false;
            boolean useFullClassificationQualifier = selectedClassificationAttributesData.isUseFullQualifier();
            Iterator var5 = selectedClassificationAttributesData.getAssignments().iterator();

            while (var5.hasNext()) {
                ClassAttributeAssignmentModel assignment = (ClassAttributeAssignmentModel) var5.next();
                if (!VMKEditorUtils.isClassificationAttributePresent(assignment, this.getCurrentAttributesMap().get(this.getSelectedComposedType()))) {
                    String alias = useFullClassificationQualifier ? this.classificationAttributeQualifierProvider.provide(assignment) : "";
                    ListItemClassificationAttributeDTO dto = new ListItemClassificationAttributeDTO(true, false, false, assignment, alias);
                    this.getCurrentAttributesMap().get(this.getSelectedComposedType()).add(dto);
                    if (ClassificationAttributeTypeEnum.REFERENCE.equals(assignment.getAttributeType())) {
                        this.createDynamicTreeNode(dto.getClassificationAttributeCode(), dto.getAlias(), assignment.getReferenceType());
                    }
                } else if (!assignment.getMandatory()) {
                    attributeAlreadyPresent = true;
                }
            }

            if (attributeAlreadyPresent) {
                this.getNotificationService().notifyUser("integrationbackoffice", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.attributeAlreadyPresent"));
            }

            this.attributeDuplicationMap = VMKEditorUtils.compileDuplicationMap(this.getSelectedComposedType(), this.getCurrentAttributesMap().get(this.getSelectedComposedType()), this.attributeDuplicationMap);
            this.populateListBox(this.getSelectedComposedType());
            this.sendOutput("enableSaveButton", true);
        }
    }

    private void persistenceSetup(IntegrationObjectModel ioModel) {
        this.persistIntegrationObject(ioModel);
        this.setModified(false);
        this.sendOutput("enableSaveButton", false);
    }

    private void persistIntegrationObject(IntegrationObjectModel ioModel) {
        this.writeService.persistIntegrationObject(ioModel);
        this.cockpitEventQueue.publishEvent(new DefaultCockpitEvent("objectsUpdated", this.selectedIntegrationObject, null));
        this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, this.getLabel("integrationbackoffice.editMode.info.msg.save"));
    }

    private Map<ComposedTypeModel, List<AbstractListItemDTO>> createIntegrationObject(CreateIntegrationObjectModalData data) {
        IntegrationObjectModel selectedIO = this.writeService.createIntegrationObject(data.getName(), data.getType());
        this.setSelectedIntegrationObject(selectedIO);
        Map<ComposedTypeModel, List<AbstractListItemDTO>> trimmedMap = EditorTrimmer.trimMap(this.readService, this.getCurrentAttributesMap(), this.getComposedTypeTree(), false);
        IntegrationObjectModel ioModel = this.setItemTypeMatchForIntegrationObject(this.writeService.createDefinitions(this.selectedIntegrationObject, trimmedMap, data.getComposedTypeModel().getCode()));
        this.writeService.persistIntegrationObject(ioModel);
        this.cockpitEventQueue.publishEvent(new DefaultCockpitEvent("objectCreated", this.selectedIntegrationObject, null));
        return trimmedMap;
    }

    private IntegrationObjectModel setItemTypeMatchForIntegrationObject(IntegrationObjectModel integrationObjectModel) {
        integrationObjectModel.getItems().forEach(this::setItemTypeMatchForIntegrationObjectItem);
        return integrationObjectModel;
    }

    private void setItemTypeMatchForIntegrationObjectItem(IntegrationObjectItemModel item) {
        if (item.getItemTypeMatch() == null) {
            if (this.getItemTypeMatchMap().containsKey(item.getType())) {
                item.setItemTypeMatch(this.getItemTypeMatchMap().get(item.getType()));
            } else {
                item.setItemTypeMatch(this.getDefaultItemTypeMatchEnum(item));
            }
        }

    }

    private Map<ComposedTypeModel, ItemTypeMatchEnum> getItemTypeMatchForIntegrationObjectItem(IntegrationObjectModel integrationObjectModel) {
        return integrationObjectModel.getItems().stream().filter((it) -> {
            return it.getItemTypeMatch() != null;
        }).collect(Collectors.toMap(IntegrationObjectItemModel::getType, IntegrationObjectItemModel::getItemTypeMatch));
    }

    private ItemTypeMatchEnum getDefaultItemTypeMatchEnum(IntegrationObjectItemModel integrationObjectItemModel) {
        return ItemTypeMatchEnum.valueOf(this.itemTypeMatchSelector.getToSelectItemTypeMatch(integrationObjectItemModel).name());
    }

    private void clearTree() {
        this.getComposedTypeTree().getTreechildren().getChildren().clear();
        this.getAttributesListBox().getItems().clear();
        this.setAncestors(new ArrayDeque());
    }

    private void createTree(ComposedTypeModel rootType, Map<ComposedTypeModel, List<AbstractListItemDTO>> existingDefinitions) {
        this.clearTree();
        this.getAncestors().push(rootType);
        Treechildren rootLevel = this.getComposedTypeTree().getTreechildren();
        TreeNodeData tnd = new TreeNodeData(null, null, rootType);
        Treeitem rootTreeItem = VMKEditorUtils.createTreeItem(tnd, true);
        rootLevel.appendChild(rootTreeItem);
        this.setCurrentAttributesMap(new HashMap());
        if (this.getFilterState() == IntegrationFilterState.SHOW_ALL) {
            this.populateAttributesMap(rootType);
            this.populateTree(rootTreeItem, existingDefinitions);
            this.loadExistingDefinitions(existingDefinitions);
        } else {
            this.setCurrentAttributesMap(existingDefinitions);
            this.populateTreeInOnlySelectedMode(rootTreeItem, existingDefinitions);
        }

        rootTreeItem.setSelected(true);
        Events.sendEvent("onSelect", this.getComposedTypeTree(), rootTreeItem);
    }

    private void populateTree(Treeitem parent, Map<ComposedTypeModel, List<AbstractListItemDTO>> existingDefinitions) {
        ComposedTypeModel parentType = ((TreeNodeData) parent.getValue()).getComposedTypeModel();
        List<AbstractListItemDTO> existingAttributes = existingDefinitions.get(parentType) == null ? Collections.emptyList() : existingDefinitions.get(parentType);
        Set<AttributeDescriptorModel> filteredAttributes = this.editorAttrFilterService.filterAttributesForTree(parentType);
        Set<AttributeDescriptorModel> existingCollections = VMKEditorUtils.getStructuredAttributes(existingAttributes);
        filteredAttributes.addAll(existingCollections);
        Set<TreeNodeData> treeNodeDataSet = filteredAttributes.stream().filter((attributeDescriptor) -> {
            ComposedTypeModel attributeType = this.readService.getComplexTypeForAttributeDescriptor(attributeDescriptor);
            return attributeType != null;
        }).map((attributeDescriptor) -> {
            String attributeDescriptorQualifier = attributeDescriptor.getQualifier();
            ComposedTypeModel attributeType = this.readService.getComplexTypeForAttributeDescriptor(attributeDescriptor);
            ComposedTypeModel attributeSubtype = this.findSubtypeMatch(parentType, attributeDescriptorQualifier, attributeType);
            ComposedTypeModel determinedType = attributeSubtype != null ? attributeSubtype : attributeType;
            Iterator var9 = existingAttributes.iterator();

            AbstractListItemDTO dto;
            do {
                if (!var9.hasNext()) {
                    return new TreeNodeData(attributeDescriptorQualifier, null, determinedType);
                }

                dto = (AbstractListItemDTO) var9.next();
            } while (!(dto instanceof VMKListItemAttributeDTO) || !((ListItemAttributeDTO) dto).getAttributeDescriptor().equals(attributeDescriptor));

            return new TreeNodeData(attributeDescriptorQualifier, dto.getAlias(), determinedType);
        }).collect(Collectors.toSet());
        treeNodeDataSet.addAll(VMKEditorUtils.getReferenceClassificationAttributes(existingAttributes));
        treeNodeDataSet.stream().sorted((attribute1, attribute2) -> {
            return attribute1.getAlias().compareToIgnoreCase(attribute2.getAlias());
        }).forEach((treeNodeData) -> {
            ComposedTypeModel composedType = treeNodeData.getComposedTypeModel();
            if (!this.getAncestors().contains(composedType) && !EditorBlacklists.getTypesBlackList().contains(composedType.getCode())) {
                this.getAncestors().addFirst(composedType);
                if (!this.getCurrentAttributesMap().containsKey(composedType)) {
                    this.populateAttributesMap(composedType);
                }

                Treeitem treeitem = this.appendTreeitem(parent, treeNodeData);
                if (treeitem.getLevel() <= 5) {
                    this.populateTree(treeitem, existingDefinitions);
                }

                this.getAncestors().pollFirst();
            }

        });
    }

    private void populateTreeInOnlySelectedMode(Treeitem parent, Map<ComposedTypeModel, List<AbstractListItemDTO>> existingDefinitions) {
        TreeNodeData parentTreeNodeData = parent.getValue();
        ComposedTypeModel parentType = parentTreeNodeData.getComposedTypeModel();
        List<AbstractListItemDTO> existingAttributes = existingDefinitions.get(parentType) == null ? Collections.emptyList() : existingDefinitions.get(parentType);
        Stream<AbstractListItemDTO> var10000 = existingAttributes.stream();

        var10000 = var10000.filter(VMKListItemAttributeDTO.class::isInstance);

        List<TreeNodeData> treeNodeDataSet = var10000.map(ListItemAttributeDTO.class::cast).filter((listItemDTO) -> {
            ComposedTypeModel attributeType = this.readService.getComplexTypeForAttributeDescriptor(listItemDTO.getAttributeDescriptor());
            return attributeType != null;
        }).map((listItemDTO) -> {
            AttributeDescriptorModel attributeDescriptor = listItemDTO.getAttributeDescriptor();
            String attributeDescriptorQualifier = attributeDescriptor.getQualifier();
            ComposedTypeModel attributeType = this.readService.getComplexTypeForAttributeDescriptor(attributeDescriptor);
            ComposedTypeModel attributeSubtype = this.findSubtypeMatch(parentType, attributeDescriptorQualifier, attributeType);
            ComposedTypeModel determinedType = attributeSubtype != null ? attributeSubtype : attributeType;
            return new TreeNodeData(attributeDescriptorQualifier, listItemDTO.getAlias(), determinedType);
        }).collect(Collectors.toList());
        treeNodeDataSet.addAll(VMKEditorUtils.getReferenceClassificationAttributes(existingAttributes));
        treeNodeDataSet.stream().sorted((attribute1, attribute2) -> {
            return attribute1.getAlias().compareToIgnoreCase(attribute2.getAlias());
        }).forEach((treeNodeData) -> {
            ComposedTypeModel composedType = treeNodeData.getComposedTypeModel();
            if (!this.getAncestors().contains(composedType)) {
                this.getAncestors().addFirst(composedType);
                Treeitem treeitem = this.appendTreeitem(parent, treeNodeData);
                this.populateTreeInOnlySelectedMode(treeitem, existingDefinitions);
                this.getAncestors().pollFirst();
            }

        });
    }

    public ComposedTypeModel findSubtypeMatch(ComposedTypeModel parentType, String attributeQualifier, ComposedTypeModel attributeType) {
        Optional<SubtypeData> data = this.getSubtypeDataSet().stream().filter((p) -> {
            return p.getParentNodeType().equals(parentType) && p.getBaseType().equals(attributeType) && attributeQualifier.equals(p.getAttributeQualifier());
        }).findFirst();
        ComposedTypeModel attributeSubtype;
        if (data.isPresent()) {
            attributeSubtype = this.readService.getComposedTypeModelFromTypeModel(data.get().getSubtype());
        } else {
            attributeSubtype = null;
        }

        return attributeSubtype;
    }

    private Treeitem appendTreeitem(Treeitem parent, TreeNodeData tnd) {
        Treeitem treeitem = VMKEditorUtils.createTreeItem(tnd, false);
        if (parent.getTreechildren() == null) {
            parent.appendChild(new Treechildren());
        }

        parent.getTreechildren().appendChild(treeitem);
        return treeitem;
    }

    private void createDynamicTreeNode(String qualifier, String alias, TypeModel typeModel) {
        ComposedTypeModel type = (ComposedTypeModel) typeModel;
        TreeNodeData tnd = new TreeNodeData(qualifier, alias, type);
        Treeitem parent = this.getComposedTypeTree().getSelectedItem();
        Treeitem treeItem = this.appendTreeitem(parent, tnd);
        this.populateAttributesMap(type);
        this.getAncestors().clear();
        this.populateTree(treeItem, Collections.emptyMap());
    }

    private void populateListBox(ComposedTypeModel key) {
        this.getAttributesListBox().getItems().clear();
        String attributeNamesSortingDirection = this.attributeNameListheader.getSortDirection();
        String descriptionsSortingDirection = this.descriptionListheader.getSortDirection();
        getCurrentAttributesMap().get(key).stream().sorted((dto1, dto2) -> {
            if ("ascending".equals(attributeNamesSortingDirection)) {
                return dto1.getAlias().compareToIgnoreCase(dto2.getAlias());
            } else if ("descending".equals(attributeNamesSortingDirection)) {
                return dto2.getAlias().compareToIgnoreCase(dto1.getAlias());
            } else if ("ascending".equals(descriptionsSortingDirection)) {
                return dto1.getDescription().compareToIgnoreCase(dto2.getDescription());
            } else {
                return "descending".equals(descriptionsSortingDirection) ? dto2.getDescription().compareToIgnoreCase(dto1.getDescription()) : dto1.getAlias().compareToIgnoreCase(dto2.getAlias());
            }
        }).forEach((dto) -> {
            Listitem listItem = this.setupListItem(key, dto);
            this.getAttributesListBox().appendChild(listItem);
        });
        if (VMKEditorUtils.markRowsWithDuplicateNames(this.getAttributesListBox().getItems(), this.getAttributeDuplicationMap().get(this.getSelectedComposedType()))) {
            this.getNotificationService().notifyUser("integrationbackoffice", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.nameDuplication"));
        }

    }

    private Listitem setupListItem(ComposedTypeModel key, AbstractListItemDTO abstractListItemDTO) {
        boolean isComplex = false;
        boolean hasSubtypes = false;
        if (abstractListItemDTO instanceof VMKListItemAttributeDTO) {
            ListItemAttributeDTO dto = (VMKListItemAttributeDTO) abstractListItemDTO;
            isComplex = this.readService.isComplexType(dto.getType());
            if (isComplex) {
                hasSubtypes = !this.readService.getComposedTypeModelFromTypeModel(dto.getBaseType()).getSubtypes().isEmpty();
            }
        }

        Listitem listItem = VMKEditorUtils.createListItem(abstractListItemDTO, isComplex, hasSubtypes, this.attributeMenuPopupLabels, this.getEditModeFlag(), this.readService);
        Checkbox uniqueCheckbox = (Checkbox) listItem.getChildren().get(3).getFirstChild();
        Checkbox autocreateCheckbox = (Checkbox) listItem.getChildren().get(4).getFirstChild();
        org.zkoss.zhtml.Button optionsBtn = (org.zkoss.zhtml.Button) listItem.getLastChild().getFirstChild();
        Menuitem viewDetails = (Menuitem) optionsBtn.getFirstChild().getFirstChild();
        Menuitem renameAttribute = (Menuitem) optionsBtn.getFirstChild().getFirstChild().getNextSibling();
        Menuitem retypeAttribute = (Menuitem) optionsBtn.getFirstChild().getLastChild();
        List<AbstractListItemDTO> itemAttributeDTOs = new ArrayList(this.getCurrentAttributesMap().get(key));
        itemAttributeDTOs.remove(abstractListItemDTO);
        RenameAttributeModalData renameAttributeModalData = new RenameAttributeModalData(itemAttributeDTOs, abstractListItemDTO, key);
        if (this.getEditModeFlag()) {
            if (abstractListItemDTO instanceof VMKListItemAttributeDTO) {
                this.addListItemEvents((VMKListItemAttributeDTO) abstractListItemDTO, listItem, uniqueCheckbox, autocreateCheckbox);
            } else {
                listItem.addEventListener("onClick", (event) -> {
                    if (!listItem.isSelected()) {
                        autocreateCheckbox.setChecked(false);
                    }

                    this.updateAttribute(listItem);
                });
            }

            this.addCheckboxEvents(listItem, uniqueCheckbox, autocreateCheckbox);
            this.addClassificationEvents(listItem);
        } else {
            this.addMaintainStateEvents(abstractListItemDTO, listItem);
        }

        this.addButtonEvents(listItem, optionsBtn);
        this.addMenuItemEvents(renameAttributeModalData, viewDetails, renameAttribute, retypeAttribute);
        if (!this.getEditModeFlag()) {
            uniqueCheckbox.setDisabled(true);
            autocreateCheckbox.setDisabled(true);
            renameAttribute.setVisible(false);
        }

        return listItem;
    }

    private void focusOnListitem(String alias) {
        Iterator var3 = this.getAttributesListBox().getItems().iterator();

        while (var3.hasNext()) {
            Listitem listitem = (Listitem) var3.next();
            AbstractListItemDTO dto = listitem.getValue();
            if (dto.getAlias().equals(alias)) {
                Clients.scrollIntoView(listitem);
                break;
            }
        }

    }

    private void autoSelectAttributeRelation(Treeitem currentTreeitem) {
        Treeitem parentTreeitem;
        for (Treeitem rootTreeitem = (Treeitem) this.getComposedTypeTree().getTreechildren().getFirstChild(); currentTreeitem != rootTreeitem; currentTreeitem = parentTreeitem) {
            String qualifier = ((TreeNodeData) currentTreeitem.getValue()).getQualifier();
            parentTreeitem = currentTreeitem.getParentItem();
            ComposedTypeModel parentType = ((TreeNodeData) parentTreeitem.getValue()).getComposedTypeModel();
            Stream var10000 = ((List) this.getCurrentAttributesMap().get(parentType)).stream();

            var10000 = var10000.filter(VMKListItemAttributeDTO.class::isInstance);

            var10000.map(ListItemAttributeDTO.class::cast).forEach((listItemDTO) -> {
                if (((ListItemAttributeDTO) listItemDTO).getAttributeDescriptor().getQualifier().equals(qualifier)) {
                    ((ListItemAttributeDTO) listItemDTO).setSelected(true);
                }

            });
        }

    }

    private void populateAttributesMap(ComposedTypeModel typeModel) {
        if (this.getCurrentAttributesMap().get(typeModel) == null) {
            List<AbstractListItemDTO> dtoList = new ArrayList();
            Set<AttributeDescriptorModel> filteredAttributes = this.editorAttrFilterService.filterAttributesForAttributesMap(typeModel);
            filteredAttributes.forEach((attribute) -> {
                boolean selected = attribute.getUnique() && !attribute.getOptional();
                ListItemStructureType structureType = VMKEditorUtils.getListItemStructureType(this.readService, attribute);
                dtoList.add(new VMKListItemAttributeDTO(selected, false, false, attribute, structureType, "", null));
            });
            this.getCurrentAttributesMap().put(typeModel, dtoList);
        }

    }

    private void loadExistingDefinitions(Map<ComposedTypeModel, List<AbstractListItemDTO>> existingDefinitions) {
        existingDefinitions.forEach((key, value) -> {
            this.getCurrentAttributesMap().forEach((key2, value2) -> {
                if (key2.equals(key)) {
                    this.getCurrentAttributesMap().replace(key2, VMKEditorUtils.updateDTOs(value2, value));
                }

            });
        });
    }

    public void compileSubtypeDataSet(Map<ComposedTypeModel, List<AbstractListItemDTO>> existingDefinitions) {
        this.setSubtypeDataSet(new HashSet());
        existingDefinitions.forEach((key, value) -> {
            Stream<AbstractListItemDTO> var10000 = value.stream();

            var10000 = var10000.filter(VMKListItemAttributeDTO.class::isInstance);

            var10000.map(ListItemAttributeDTO.class::cast).forEach((dto) -> {
                if (!dto.getType().equals(dto.getBaseType())) {
                    SubtypeData data = new SubtypeData(key, dto.getType(), dto.getBaseType(), dto.getAlias(), dto.getAttributeDescriptor().getQualifier());
                    this.getSubtypeDataSet().add(data);
                }

            });
        });
    }

    private void addMenuItemEvents(RenameAttributeModalData ramd, Menuitem viewDetails, Menuitem renameAttribute, Menuitem retypeAttribute) {
        renameAttribute.addEventListener("onClick", (event) -> {
            this.sendOutput("openRenameAttribute", ramd);
        });
        if (ramd.getDto() instanceof VMKListItemAttributeDTO) {
            viewDetails.addEventListener("onClick", (event) -> {
                this.sendOutput("openAttributeDetails", ramd.getDto());
            });
            ListItemAttributeDTO ramdDTO = (VMKListItemAttributeDTO) ramd.getDto();
            SubtypeData data = new SubtypeData(ramd.getParent(), ramdDTO.getType(), ramdDTO.getBaseType(), ramd.getDto().getAlias(), ramdDTO.getAttributeDescriptor().getQualifier());
            retypeAttribute.addEventListener("onClick", (event) -> {
                this.sendOutput("openRetypeAttribute", data);
            });
        } else {
            viewDetails.addEventListener("onClick", (event) -> {
                this.sendOutput("openClassificationAttributeDetails", ramd.getDto());
            });
        }

    }

    private void addCheckboxEvents(Listitem listItem, Checkbox uniqueCheckbox, Checkbox autocreateCheckbox) {
        uniqueCheckbox.addEventListener("onCheck", (event) -> {
            this.checkboxEventActions(listItem, uniqueCheckbox);
        });
        autocreateCheckbox.addEventListener("onCheck", (event) -> {
            this.checkboxEventActions(listItem, autocreateCheckbox);
        });
    }

    private void checkboxEventActions(Listitem listItem, Checkbox checkbox) {
        if (!listItem.isDisabled()) {
            if (checkbox.isChecked()) {
                listItem.setSelected(true);
                if (listItem.getValue() instanceof VMKListItemAttributeDTO) {
                    this.checkTreeNodeForStructuredType(listItem.getValue());
                }
            }

            this.updateAttribute(listItem);
        }

    }

    private void addButtonEvents(Listitem listItem, org.zkoss.zhtml.Button detailsBtn) {
        detailsBtn.addEventListener("onClick", (event) -> {
            if (!listItem.isDisabled()) {
                Menupopup menuPopup = (Menupopup) listItem.getLastChild().getFirstChild().getFirstChild();
                menuPopup.open(detailsBtn);
            }

        });
    }

    private void addListItemEvents(ListItemAttributeDTO dto, Listitem listItem, Checkbox uniqueCheckbox, Checkbox autocreateCheckbox) {
        if (dto.isRequired()) {
            listItem.addEventListener("onClick", (event) -> {
                this.maintainSelectionState(listItem, true);
            });
        } else if (!dto.isSupported()) {
            listItem.addEventListener("onClick", (event) -> {
                this.maintainSelectionState(listItem, false);
                this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.illegalMapTypeAttribute", Arrays.array(dto.getAttributeDescriptor().getQualifier())));
            });
        } else if (!listItem.isDisabled()) {
            listItem.addEventListener("onClick", (event) -> {
                if (listItem.isSelected()) {
                    this.checkTreeNodeForStructuredType(dto);
                } else {
                    if (!dto.getAttributeDescriptor().getUnique()) {
                        uniqueCheckbox.setChecked(false);
                    }

                    autocreateCheckbox.setChecked(false);
                }

                this.updateAttribute(listItem);
            });
        }

    }

    private void addMaintainStateEvents(AbstractListItemDTO dto, Listitem listItem) {
        if (dto.isSelected()) {
            listItem.addEventListener("onClick", (event) -> {
                this.maintainSelectionState(listItem, true);
            });
        } else {
            listItem.addEventListener("onClick", (event) -> {
                this.maintainSelectionState(listItem, false);
            });
        }

    }

    private void maintainSelectionState(Listitem listItem, boolean selected) {
        listItem.setSelected(selected);
    }

    private void addClassificationEvents(Listitem listItem) {
        listItem.addEventListener("onClick", (event) -> {
            if (this.readService.isProductType(this.getSelectedComposedType().getCode())) {
                this.attributeDuplicationMap = VMKEditorUtils.compileDuplicationMap(this.getSelectedComposedType(), this.getCurrentAttributesMap().get(this.getSelectedComposedType()), this.attributeDuplicationMap);
                VMKEditorUtils.markRowsWithDuplicateNames(this.getAttributesListBox().getItems(), this.getAttributeDuplicationMap().get(this.getSelectedComposedType()));
            }

        });
    }

    private Deque<ComposedTypeModel> determineTreeitemAncestors(Treeitem currentTreeitem) {
        ArrayDeque currentTreePath;
        for (currentTreePath = new ArrayDeque(); currentTreeitem.getLevel() > 0; currentTreeitem = currentTreeitem.getParentItem()) {
            currentTreePath.addFirst(((TreeNodeData) currentTreeitem.getValue()).getComposedTypeModel());
        }

        currentTreePath.addFirst(((TreeNodeData) currentTreeitem.getValue()).getComposedTypeModel());
        return currentTreePath;
    }

    private boolean validation(Map<ComposedTypeModel, List<AbstractListItemDTO>> trimmedAttributesMap) {
        String VALIDATION_MESSAGE_TITLE = this.getLabel("integrationbackoffice.editMode.error.title.validation");
        String VALIDATION_DUPLICATION_TITLE = this.getLabel("integrationbackoffice.editMode.error.title.duplicationValidation");
        String validationError = EditorValidator.validateDefinitions(trimmedAttributesMap);
        if (!"".equals(validationError)) {
            Messagebox.show(this.getLabel("integrationbackoffice.editMode.error.msg.definitionValidation", Arrays.array(validationError)), VALIDATION_MESSAGE_TITLE, 1, "z-messagebox-icon z-messagebox-error");
            return false;
        } else {
            validationError = EditorValidator.validateHasKey(trimmedAttributesMap);
            if (!"".equals(validationError)) {
                Messagebox.show(this.getLabel("integrationbackoffice.editMode.error.msg.uniqueValidation", Arrays.array(validationError)), VALIDATION_MESSAGE_TITLE, 1, "z-messagebox-icon z-messagebox-error");
                return false;
            } else {
                validationError = EditorValidator.validateHasNoDuplicateAttributeNames(this.getAttributeDuplicationMap());
                if (!"".equals(validationError)) {
                    Messagebox.show(this.getLabel("integrationbackoffice.editMode.error.msg.duplicationValidation", Arrays.array(validationError)), VALIDATION_DUPLICATION_TITLE, 1, "z-messagebox-icon z-messagebox-error");
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    public void updateAttribute(Listitem listitem) {
        AbstractListItemDTO dto = listitem.getValue();
        List<Component> components = listitem.getChildren();
        Checkbox uCheckbox = (Checkbox) components.get(3).getFirstChild();
        Checkbox aCheckbox = (Checkbox) components.get(4).getFirstChild();
        Listcell attributeLabel = (Listcell) components.get(0);
        dto.setAlias(attributeLabel.getLabel());
        dto.setSelected(listitem.isSelected());
        dto.setCustomUnique(uCheckbox.isChecked());
        dto.setAutocreate(aCheckbox.isChecked());
        if (this.getEditModeFlag()) {
            this.enableSaveButton();
        }

    }

    private void checkTreeNodeForStructuredType(ListItemAttributeDTO dto) {
        boolean isStructuredType = dto.getStructureType() == ListItemStructureType.MAP || dto.getStructureType() == ListItemStructureType.COLLECTION;
        if (isStructuredType && this.readService.isComplexType(dto.getType())) {
            Treechildren nodeChildren = this.getComposedTypeTree().getSelectedItem().getTreechildren();
            String attributeDescriptorQualifier = dto.getAttributeDescriptor().getQualifier();
            if (VMKEditorUtils.findInTreechildren(attributeDescriptorQualifier, nodeChildren) == null) {
                this.createDynamicTreeNode(dto.getAttributeDescriptor().getQualifier(), dto.getAlias(), dto.getType());
            }
        }

    }

    private void showNoServiceSelectedMessage() {
        this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.noServiceSelected"));
    }

    private void showObjectLoadedFurtherConfigurationMessage() {
        this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.WARNING, this.getLabel("integrationbackoffice.editMode.warning.msg.serviceLoadedNeedsFurtherConfig"));
    }

    private void enableSaveButton() {
        if (!this.isModified()) {
            this.setModified(true);
            this.sendOutput("enableSaveButton", true);
        }

    }

    private void clearSelectedIntegrationObject() {
        this.clearTree();
        this.setSelectedIntegrationObject(null);
        this.attributeDuplicationMap.clear();
    }

}
