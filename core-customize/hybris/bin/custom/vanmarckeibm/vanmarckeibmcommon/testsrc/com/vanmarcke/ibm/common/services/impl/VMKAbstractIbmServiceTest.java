package com.vanmarcke.ibm.common.services.impl;

import com.vanmarcke.ibm.common.resttemplate.impl.VMKRestTemplate;
import com.vanmarcke.services.basestore.VMKBaseStoreService;
import org.apache.commons.lang.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VMKAbstractIbmServiceTest {

    private static final String URL = RandomStringUtils.randomAlphabetic(10);

    private VMKBaseStoreService baseStoreService;
    private VMKRestTemplate restTemplate;
    private VMKAbstractIbmServiceImpl ibmService;

    @Before
    public void setUp() {
        baseStoreService = mock(VMKBaseStoreService.class);
        restTemplate = mock(VMKRestTemplate.class);
        ibmService = new VMKAbstractIbmServiceImpl(baseStoreService, restTemplate);
    }

    @Test
    public void testGetUrl() {
        when(baseStoreService.getBaseUrl()).thenReturn(URL);

        Assertions
                .assertThat(ibmService.getUrl())
                .isEqualTo(URL);
    }

    /**
     * Private inner class for testing an abstract class.
     */
    private static class VMKAbstractIbmServiceImpl extends VMKAbstractIbmService {

        protected VMKAbstractIbmServiceImpl(VMKBaseStoreService baseStoreService, VMKRestTemplate restTemplate) {
            super(baseStoreService, restTemplate);
        }
    }
}