package com.vanmarcke.ibm.common.exception;

/**
 * Custom generic IBM exception.
 *
 * @author Tom van den Berg
 * @since 28-10-2020
 */
public class IBMException extends Exception{

    /**
     * {@inheritDoc}
     */
    public IBMException(String message) {
        super(message);
    }
}
