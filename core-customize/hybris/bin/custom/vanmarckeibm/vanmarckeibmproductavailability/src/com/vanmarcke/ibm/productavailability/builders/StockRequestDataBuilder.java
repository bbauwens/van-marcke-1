package com.vanmarcke.ibm.productavailability.builders;

import com.vanmarcke.cpi.data.stock.StockRequestData;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

/**
 * Builder implementation for a {@link StockRequestData} instance.
 *
 * @author Tom van den Berg
 * @since 20-10-2020
 */
public class StockRequestDataBuilder {

    private final String productCode;
    private final String[] wareHouseIdentifiers;
    private final String customerID;

    /**
     * The entry point of the {@code StockRequestDataBuilder}.
     *
     * @param productCode          the product code
     * @param wareHouseIdentifiers the wareHouseIdentifiers
     * @return the {@code StockRequestDataBuilder} instance for method chaining
     */
    public static StockRequestDataBuilder aStockRequest(String productCode, String customerID, String... wareHouseIdentifiers) {
        return new StockRequestDataBuilder(productCode, customerID, wareHouseIdentifiers);
    }

    /**
     * Creates an implementation of the {@link StockRequestData}.
     *
     * @return the stock request data
     */
    public StockRequestData build() {
        StockRequestData request = new StockRequestData();
        request.setProductIDs(StringUtils.isEmpty(productCode) ? emptyList() : singletonList(productCode));
        request.setStoreIDs(ArrayUtils.isEmpty(wareHouseIdentifiers) ? emptyList() : Arrays.asList(wareHouseIdentifiers));
        request.setCustomerID(customerID);
        return request;
    }

    /**
     * Creates a new instance of the {@link StockRequestData}.
     *
     * @param productCode          the product code
     * @param customerID
     * @param wareHouseIdentifiers the wareHouseIdentifiers
     */
    private StockRequestDataBuilder(String productCode, String customerID, String... wareHouseIdentifiers) {
        this.productCode = productCode;
        this.customerID = customerID;
        this.wareHouseIdentifiers = wareHouseIdentifiers;
    }
}
