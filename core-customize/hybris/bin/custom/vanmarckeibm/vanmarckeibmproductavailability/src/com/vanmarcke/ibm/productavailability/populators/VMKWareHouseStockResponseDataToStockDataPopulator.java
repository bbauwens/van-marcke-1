package com.vanmarcke.ibm.productavailability.populators;

import com.vanmarcke.cpi.data.stock.WareHouseStockResponseData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static de.hybris.platform.basecommerce.enums.StockLevelStatus.INSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.OUTOFSTOCK;

public class VMKWareHouseStockResponseDataToStockDataPopulator implements Populator<WareHouseStockResponseData, StockData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(WareHouseStockResponseData source, StockData target) throws ConversionException {
        target.setStockLevel(getStockLevel(source));
        target.setStockLevelStatus(getStockLevel(source) > 0 ? INSTOCK : OUTOFSTOCK);
        target.setWareHouse(source.getCode());
    }

    /**
     * Get the stock level from the WareHouseStockResponseData with a fallback to 0.
     *
     * @param source the WareHouseStockResponseData
     * @return The available stock level or 0.
     */
    private long getStockLevel(WareHouseStockResponseData source) {
        return source.getAvailable() != null ? source.getAvailable() : 0;
    }
}
