package com.vanmarcke.ibm.productavailability.populators;

import com.vanmarcke.cpi.data.stock.ProductStockResponseData;
import com.vanmarcke.cpi.data.stock.WareHouseStockResponseData;
import com.vanmarcke.facades.product.data.StockListData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static de.hybris.platform.basecommerce.enums.StockLevelStatus.INSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.OUTOFSTOCK;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class VMKProductStockResponseDataToStockDataPopulatorTest {

    private Populator<ProductStockResponseData, StockListData> vmkProductStockResponseDataToStockDataPopulator;
    private Converter<WareHouseStockResponseData, StockData> vmkWareHouseStockResponseDataToStockDataConverter;

    @Before
    public void setUp() {
        vmkWareHouseStockResponseDataToStockDataConverter = mock(Converter.class);
        vmkProductStockResponseDataToStockDataPopulator = new VMKProductStockResponseDataToStockDataPopulator(vmkWareHouseStockResponseDataToStockDataConverter);
    }

    @Test
    public void populateSetsEDCStockDataWithInStockWhenAvailable() {
        ProductStockResponseData productStockResponseData = mock(ProductStockResponseData.class);
        when(productStockResponseData.getAvailable()).thenReturn(true);
        when(productStockResponseData.getAvailableAmountEDC()).thenReturn(10L);
        StockListData stockListData = new StockListData();

        vmkProductStockResponseDataToStockDataPopulator.populate(productStockResponseData, stockListData);

        StockData edcStockData = stockListData.getStock().get(0);
        assertThat(edcStockData.getWareHouse()).isEqualTo("EDC");
        assertThat(edcStockData.getStockLevelStatus()).isEqualTo(INSTOCK);
        assertThat(edcStockData.getStockLevel()).isEqualTo(10L);
    }

    @Test
    public void populateSetsEDCStockDataWithOutOfStockWhenUnavailable() {
        ProductStockResponseData productStockResponseData = mock(ProductStockResponseData.class);
        when(productStockResponseData.getAvailable()).thenReturn(false);
        when(productStockResponseData.getAvailableAmountEDC()).thenReturn(null);
        StockListData stockListData = new StockListData();

        vmkProductStockResponseDataToStockDataPopulator.populate(productStockResponseData, stockListData);

        StockData edcStockData = stockListData.getStock().get(0);
        assertThat(edcStockData.getStockLevel()).isEqualTo(0L);
        assertThat(edcStockData.getWareHouse()).isEqualTo("EDC");
        assertThat(edcStockData.getStockLevelStatus()).isEqualTo(OUTOFSTOCK);
    }

    @Test
    public void populateConvertsAndPopulatesWareHouses() {
        StockData expected = mock(StockData.class);
        List<WareHouseStockResponseData> wareHouseStockResponseDataList = mock(List.class);
        ProductStockResponseData productStockResponseData = mock(ProductStockResponseData.class);

        when(productStockResponseData.getWarehouses()).thenReturn(wareHouseStockResponseDataList);
        when(vmkWareHouseStockResponseDataToStockDataConverter.convertAll(wareHouseStockResponseDataList)).thenReturn(Collections.singletonList(expected));
        StockListData stockListData = new StockListData();

        vmkProductStockResponseDataToStockDataPopulator.populate(productStockResponseData, stockListData);

        assertThat(stockListData.getStock()).contains(expected);
    }
}