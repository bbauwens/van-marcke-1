package com.vanmarcke.ibm.productavailability.cache;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class VMKIbmStockInformationCacheKeyGeneratorTest {

    private VMKIbmStockInformationCacheKeyGenerator stockInformationCacheKeyGenerator;

    @Before
    public void setUp() {
        stockInformationCacheKeyGenerator = new VMKIbmStockInformationCacheKeyGenerator();
    }

    @Test
    public void generate() {
        ProductModel product = mock(ProductModel.class);
        String[] stores = new String[]{"CD", "AB"};
        when(product.getCode()).thenReturn("code");

        // null for the first 2 parameters since they are not relevant and Method can not be mocked.
        String key = stockInformationCacheKeyGenerator.generate(null, null, product, stores);

        assertThat(key).isEqualTo("code_[AB, CD]");
    }

    @Test
    public void generate_noStores() {
        ProductModel product = mock(ProductModel.class);
        String[] stores = new String[]{};
        when(product.getCode()).thenReturn("code");

        // null for the first 2 parameters since they are not relevant and Method can not be mocked.
        String key = stockInformationCacheKeyGenerator.generate(null, null, product, stores);

        assertThat(key).isEqualTo("code_EDC");
    }

}