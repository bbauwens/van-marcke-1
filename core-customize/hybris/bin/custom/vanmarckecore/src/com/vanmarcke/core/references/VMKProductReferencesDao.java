package com.vanmarcke.core.references;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.daos.ProductReferencesDao;

import java.util.List;

/**
 * Custom Dao for Product References
 *
 * @author Niels Raemaekers
 * @since 12-05-2020
 */
public interface VMKProductReferencesDao extends ProductReferencesDao {

    /**
     * Find all the inactive Product References
     *
     * @return all inactive product references or empty list
     */
    List<ProductReferenceModel> findAllInactiveProductReferences();
}
