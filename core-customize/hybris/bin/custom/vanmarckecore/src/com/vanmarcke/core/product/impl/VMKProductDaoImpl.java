package com.vanmarcke.core.product.impl;

import com.vanmarcke.core.model.VanMarckeVariantProductModel;
import com.vanmarcke.core.product.VMKProductDao;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Custom extension of defaut product dao
 *
 * @author Cristi Stoica
 * @since 15-06-2021
 */
public class VMKProductDaoImpl extends DefaultProductDao implements VMKProductDao {

    public VMKProductDaoImpl(String typecode) {
        super(typecode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getBaseProductCode(String code) {
        final Map params = new HashMap<>();
        final StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT {bp:").append(ProductModel.CODE).append("} ")
                .append("FROM {").append(ProductModel._TYPECODE).append(" AS bp ")
                .append("JOIN ").append(VanMarckeVariantProductModel._TYPECODE).append(" AS p ")
                .append("ON {p:").append(VanMarckeVariantProductModel.BASEPRODUCT).append("}={bp:").append(ProductModel.PK).append("} } ")
                .append("WHERE {p:").append(VanMarckeVariantProductModel.CODE).append("} = ?").append(VanMarckeVariantProductModel.CODE);

        params.put(VanMarckeVariantProductModel.CODE, code);
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(queryString.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(String.class));
        List<Object> results = getFlexibleSearchService().search(searchQuery).getResult();

        if (CollectionUtils.isEmpty(results)) {
            throw new UnknownIdentifierException("Could not find base product for product code " + code);
        }

        return (String) results.get(0);
    }
}
