package com.vanmarcke.core.cms.impl;

import com.vanmarcke.core.cms.VMKConsentParagraphComponentDAO;
import com.vanmarcke.core.model.ConsentParagraphComponentModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

/**
 * The {@link VMKConsentParagraphComponentDAOImpl} class implements the methods to retrieve instances of the {@link ConsentParagraphComponentModel} class.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
public class VMKConsentParagraphComponentDAOImpl implements VMKConsentParagraphComponentDAO {

    public static final String FIND_ALL = "SELECT {cp.pk} FROM {ConsentParagraphComponent AS cp JOIN CatalogVersion AS cv ON {cp.catalogVersion} = {cv.pk} JOIN Catalog AS c ON {cv.pk} = {c.activeCatalogVersion}} WHERE {c.id} = ?catalogId AND {cv.version} = ?catalogVersion";

    private final FlexibleSearchService flexibleSearchService;

    /**
     * Creates a new instance of the {@link VMKConsentParagraphComponentDAOImpl} class.
     *
     * @param flexibleSearchService the flexible search service
     */
    public VMKConsentParagraphComponentDAOImpl(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ConsentParagraphComponentModel> findAll(CatalogVersionModel cv) {
        FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ALL);
        query.addQueryParameter("catalogId", cv.getCatalog().getId());
        query.addQueryParameter("catalogVersion", cv.getVersion());
        return flexibleSearchService.<ConsentParagraphComponentModel>search(query).getResult();
    }
}
