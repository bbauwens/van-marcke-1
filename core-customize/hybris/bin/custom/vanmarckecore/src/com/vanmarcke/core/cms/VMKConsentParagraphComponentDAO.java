package com.vanmarcke.core.cms;

import com.vanmarcke.core.model.ConsentParagraphComponentModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.List;

/**
 * The {@link VMKConsentParagraphComponentDAO} interfaces defines the methods to retrieve instances of the
 * {@link ConsentParagraphComponentModel} class.
 *
 * @author Christiaan Janssen
 * @since 16-07-2020
 */
public interface VMKConsentParagraphComponentDAO {

    /**
     * Returns the {@link ConsentParagraphComponentModel} instances for the given {@code cv}.
     *
     * @param cv the catalog version
     * @return the ConsentParagraphComponentModel instances
     */
    List<ConsentParagraphComponentModel> findAll(CatalogVersionModel cv);
}
