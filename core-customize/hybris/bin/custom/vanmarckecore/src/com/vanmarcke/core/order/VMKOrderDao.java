package com.vanmarcke.core.order;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.daos.OrderDao;

import java.util.List;

/**
 * Custom interface for Order Dao
 *
 * @author Niels Raemaekers
 * @since 19-05-2020
 */
public interface VMKOrderDao extends OrderDao {

    /**
     * Find all the failed orders
     *
     * @return list with the failed orders
     */
    List<OrderModel> findFailedOrders();

    /**
     * Find the order for the given {@code cartId} and {@code baseSite}.
     *
     * @param guid     the cart's unique identifier
     * @param baseSite the base site
     * @return the order
     */
    OrderModel findOrderByGuidAndSite(String guid, BaseSiteModel baseSite);
}
