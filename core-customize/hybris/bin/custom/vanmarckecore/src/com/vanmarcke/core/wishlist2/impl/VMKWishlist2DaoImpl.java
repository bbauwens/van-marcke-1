package com.vanmarcke.core.wishlist2.impl;

import com.google.common.collect.ImmutableMap;
import com.vanmarcke.core.wishlist2.VMKWishlist2Dao;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.core.servicelayer.data.SortData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchParameter;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchService;
import de.hybris.platform.servicelayer.search.paginated.constants.SearchConstants;
import de.hybris.platform.servicelayer.search.paginated.util.PaginatedSearchUtils;
import de.hybris.platform.wishlist2.impl.daos.impl.DefaultWishlist2Dao;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VMKWishlist2DaoImpl extends DefaultWishlist2Dao implements VMKWishlist2Dao {

    private static final String FIND_WL_BY_USER_AND_NAME = "SELECT {pk} FROM {Wishlist2} WHERE {user} = ?user AND {name} = ?name";
    private static final String FIND_WLE_BY_PK = "SELECT {wle.pk} FROM {Wishlist2Entry AS wle JOIN Wishlist2 AS wl ON {wle.wishlist} = {wl.pk}} WHERE {wle.pk} = ?pk AND {wl.user} = ?user";

    private PaginatedFlexibleSearchService paginatedFlexibleSearchService;

    public VMKWishlist2DaoImpl(PaginatedFlexibleSearchService paginatedFlexibleSearchService) {
        this.paginatedFlexibleSearchService = paginatedFlexibleSearchService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Wishlist2Model getWishlistByUserAndName(@NonNull UserModel user, @NonNull String name) {
        Assert.notNull(user, "The user cannot be null.");
        Assert.notNull(name, "The name cannot be null.");

        FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_WL_BY_USER_AND_NAME);
        query.addQueryParameter("user", user);
        query.addQueryParameter("name", name);

        return getFlexibleSearchService().searchUnique(query);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Wishlist2Model findWishListByUserAndName(@NonNull UserModel user, @NonNull String name) {
        Assert.notNull(user, "The user cannot be null.");
        Assert.notNull(name, "The name cannot be null.");

        FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_WL_BY_USER_AND_NAME);
        query.addQueryParameter("user", user);
        query.addQueryParameter("name", name);

        final SearchResult<Wishlist2Model> result = getFlexibleSearchService().search(query);
        return CollectionUtils.isNotEmpty(result.getResult()) ? result.getResult().get(0) : null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchPageData<Wishlist2Model> getWishlistsForUser(@NonNull UserModel user, PaginationData paginationData, SortData sortData) {

        final Map<String, Object> params = ImmutableMap.<String, Object>builder().put("user", user).build();
        final FlexibleSearchQuery query = new FlexibleSearchQuery("select {w.pk} from {Wishlist2 as w} where {w.user} = ?user", params);

        return genericSearchForWishlists(query, paginationData, sortData);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Wishlist2EntryModel> findWishListEntriesByUserAndPK(@NonNull UserModel user, @NonNull String pk) {
        Assert.notNull(user, "The user cannot be null.");
        Assert.notNull(pk, "The primary key cannot be null.");

        FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_WLE_BY_PK);
        query.addQueryParameter("user", user);
        query.addQueryParameter("pk", pk);

        return getFlexibleSearchService().<Wishlist2EntryModel>search(query).getResult();
    }

    @Override
    public Integer getSavedCartsCountForSiteAndUser(UserModel user) {

        final String query = new StringBuilder()
                .append("GET {").append(Wishlist2Model._TYPECODE).append("} ")
                .append("WHERE {").append(Wishlist2Model.USER).append("} = ?user")
                .toString();

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);

        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
        fQuery.addQueryParameters(params);
        fQuery.setResultClassList(Collections.singletonList(PK.class));

        final SearchResult<Integer> searchResult = search(fQuery);

        return searchResult.getCount();
    }

    /**
     * Perform a generic, internal search for wishlist instances
     *
     * @param query The query to execute
     * @param paginationData pagination data
     * @param sortData sorting requirements
     *
     * @return Search result of wishlist instances
     */
    protected SearchPageData<Wishlist2Model> genericSearchForWishlists(final FlexibleSearchQuery query, final PaginationData paginationData, final SortData sortData) {

        final ImmutableMap.Builder<String, String> sortMapBuilder = ImmutableMap.builder();
        final ImmutableMap.Builder<String, String> sortCodeToQueryAliasBuilder = ImmutableMap.builder();
        if( sortData != null && StringUtils.isNotBlank(sortData.getCode())) {
            sortMapBuilder.put(sortData.getCode(), sortData.isAsc() ? SearchConstants.ASCENDING : SearchConstants.DESCENDING);
            sortCodeToQueryAliasBuilder.put(sortData.getCode(), "w");
        }

        final SearchPageData<Wishlist2Model> spd = PaginatedSearchUtils.createSearchPageDataWithPaginationAndSorting(
                paginationData.getPageSize(),
                paginationData.getCurrentPage(),
                paginationData.isNeedsTotal(),
                sortMapBuilder.build());

        final PaginatedFlexibleSearchParameter searchParameter = new PaginatedFlexibleSearchParameter();
        searchParameter.setSearchPageData(spd);
        searchParameter.setFlexibleSearchQuery(query);
        searchParameter.setSortCodeToQueryAlias(sortCodeToQueryAliasBuilder.build());

        return paginatedFlexibleSearchService.search(searchParameter);
    }
}