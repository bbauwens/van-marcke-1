package com.vanmarcke.core.pos.impl;

import com.vanmarcke.core.pos.VMKPointOfServiceDao;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.impl.DefaultPointOfServiceDao;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

/**
 * Implementation for VMKPointOfServiceDao
 */
public class VMKPointOfServiceDaoImpl extends DefaultPointOfServiceDao implements VMKPointOfServiceDao {

    private static final String QUERY_POS_FOR_IDS = "SELECT {pk} FROM {PointOfService} WHERE {name} IN (?posNameList)";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PointOfServiceModel> getPointOfServicesForIDs(List<String> storeIDs) {
        FlexibleSearchQuery fQuery = new FlexibleSearchQuery(QUERY_POS_FOR_IDS);
        fQuery.addQueryParameter("posNameList", storeIDs);
        SearchResult<PointOfServiceModel> result = getFlexibleSearchService().search(fQuery);
        return result.getResult();
    }
}
