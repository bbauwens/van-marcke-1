package com.vanmarcke.core.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentTransactionUtilsTest {

    @Test
    public void testGetCurrentPaymentTransaction_withEmptyList() {
        PaymentTransactionModel result = PaymentTransactionUtils.getCurrentPaymentTransaction(null);
        assertThat(result).isNull();
    }

    @Test
    public void testGetCurrentPaymentTransaction() {
        PaymentTransactionModel paymentTransactionModel1 = mock(PaymentTransactionModel.class);
        PaymentTransactionModel paymentTransactionModel2 = mock(PaymentTransactionModel.class);

        PaymentTransactionModel result = PaymentTransactionUtils.getCurrentPaymentTransaction(asList(paymentTransactionModel1, paymentTransactionModel2));
        assertThat(result).isEqualTo(paymentTransactionModel2);
    }

    @Test
    public void testGetCurrentPaymentEntryTransaction() {
        PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel1 = mock(PaymentTransactionEntryModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel2 = mock(PaymentTransactionEntryModel.class);

        when(paymentTransactionModel.getEntries()).thenReturn(asList(paymentTransactionEntryModel1, paymentTransactionEntryModel2));
        PaymentTransactionEntryModel result = PaymentTransactionUtils.getCurrentPaymentTransactionEntry(singleton(paymentTransactionModel));

        assertThat(result).isEqualTo(paymentTransactionEntryModel2);
    }
}