package com.vanmarcke.core.pos.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKPointOfServiceDaoImplTest {

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> flexibleSearchQueryCaptor;
    @Mock
    private FlexibleSearchService flexibleSearchService;

    @InjectMocks
    private VMKPointOfServiceDaoImpl defaultVMKPointOfServiceDao;

    @Before
    public void setup() {
        initMocks(this);
        defaultVMKPointOfServiceDao = new VMKPointOfServiceDaoImpl();
        defaultVMKPointOfServiceDao.setFlexibleSearchService(flexibleSearchService);
    }

    @Test
    public void findPointOfServicesForIDs() {
        List<String> storeIDs = Arrays.asList("EA");
        SearchResult searchResult = mock(SearchResult.class);
        PointOfServiceModel pointOfService = mock(PointOfServiceModel.class);

        when(this.flexibleSearchService.search(flexibleSearchQueryCaptor.capture())).thenReturn(searchResult);
        when(searchResult.getResult()).thenReturn(Collections.singletonList(pointOfService));
        List<PointOfServiceModel> result = defaultVMKPointOfServiceDao.getPointOfServicesForIDs(storeIDs);

        verify(flexibleSearchService).search(flexibleSearchQueryCaptor.capture());
        assertThat(result).isNotEmpty().contains(pointOfService);

        final FlexibleSearchQuery fQuery = this.flexibleSearchQueryCaptor.getValue();
        assertThat(fQuery.getQueryParameters()).includes(entry("posNameList", storeIDs));
        assertThat(fQuery.getQuery()).isEqualTo("SELECT {pk} FROM {PointOfService} WHERE {name} IN (?posNameList)");
    }

    @Test
    public void findPointOfServicesForIDs_emptyList() {
        List<String> storeIDs = Arrays.asList("EA");
        SearchResult searchResult = mock(SearchResult.class);

        when(this.flexibleSearchService.search(flexibleSearchQueryCaptor.capture())).thenReturn(searchResult);
        when(searchResult.getResult()).thenReturn(Collections.emptyList());
        List<PointOfServiceModel> result = defaultVMKPointOfServiceDao.getPointOfServicesForIDs(storeIDs);

        verify(flexibleSearchService).search(flexibleSearchQueryCaptor.capture());
        assertThat(result).isEmpty();

        final FlexibleSearchQuery fQuery = this.flexibleSearchQueryCaptor.getValue();
        assertThat(fQuery.getQueryParameters()).includes(entry("posNameList", storeIDs));
        assertThat(fQuery.getQuery()).isEqualTo("SELECT {pk} FROM {PointOfService} WHERE {name} IN (?posNameList)");
    }

    @Test
    public void findPointOfServicesForIDs_List() {
        List<String> storeIDs = Arrays.asList("EK", "ED", "ED", "EM", "EJ", "EP", "EE", "EN", "EA", "EA", "EL", "ET", "ET", "ER", "EF", "EV", "EV", "ES", "EI", "EX", "EY", "FA", "FI");
        SearchResult searchResult = mock(SearchResult.class);
        PointOfServiceModel pointOfService = mock(PointOfServiceModel.class);

        when(this.flexibleSearchService.search(flexibleSearchQueryCaptor.capture())).thenReturn(searchResult);
        when(searchResult.getResult()).thenReturn(Collections.singletonList(pointOfService));
        List<PointOfServiceModel> result = defaultVMKPointOfServiceDao.getPointOfServicesForIDs(storeIDs);

        verify(flexibleSearchService).search(flexibleSearchQueryCaptor.capture());
        assertThat(result).isNotEmpty().contains(pointOfService);

        final FlexibleSearchQuery fQuery = this.flexibleSearchQueryCaptor.getValue();
        assertThat(fQuery.getQueryParameters()).includes(entry("posNameList", storeIDs));
        assertThat(fQuery.getQuery()).isEqualTo("SELECT {pk} FROM {PointOfService} WHERE {name} IN (?posNameList)");
    }
}
