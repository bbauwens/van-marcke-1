package com.vanmarcke.core.references.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKProductReferencesDaoImplTest {

    @Captor
    private ArgumentCaptor<FlexibleSearchQuery> flexibleSearchQueryCaptor;
    @Mock
    private FlexibleSearchService flexibleSearchService;
    @InjectMocks
    private VMKProductReferencesDaoImpl defaultVMKProductReferencesDao;

    @Test
    public void testFindAllInactiveProductReferences() {
        ProductReferenceModel productReference = mock(ProductReferenceModel.class);
        SearchResult searchResult = mock(SearchResult.class);

        when(this.flexibleSearchService.search(this.flexibleSearchQueryCaptor.capture())).thenReturn(searchResult);
        when(searchResult.getResult()).thenReturn(Collections.singletonList(productReference));
        List<ProductReferenceModel> result = defaultVMKProductReferencesDao.findAllInactiveProductReferences();

        assertThat(result).isNotEmpty().containsExactly(productReference);
        final FlexibleSearchQuery fQuery = this.flexibleSearchQueryCaptor.getValue();
        assertThat(fQuery.getQuery()).isEqualTo("SELECT {pk} FROM {ProductReference} WHERE {active} = 0");
        verify(flexibleSearchService).search(fQuery);
    }
}