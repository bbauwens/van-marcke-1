<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url var="exportBasketUrl"
            value="/export-basket/generate" htmlEscape="false">
</spring:url>
<div class="vmb-modal fade" id="vmbExportWishListModal" tabindex="-1" role="dialog" aria-labelledby="vmbRestoreWishListModalTitle">
    <form method="get" action="${exportBasketUrl}" id="exportSavedBasketForm">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="vmbRestoreWishListModalTitle"><spring:theme code='savedcart.export.popup.title'/></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <spring:theme code="text.account.savedcart.to.activecart"/>
                    </p>
                    <p>
                        <span><spring:theme code="text.account.savedcart.cart.name"/>:</span>&nbsp;<strong>${fn:escapeXml(wishListData.name)}</strong>
                    </p>
                    <div class="restore-current-cart-form js-export-wish-list-form">
                        <div class="js-export-error-container help-block"></div>
                        <label for="exportAsPdf">
                            <input type="radio" id="exportWishListAsPdf" name="exportType" value="pdf" class="js-keep-restored-cart" checked="checked">
                            <spring:theme code="text.account.wishlist.export.aspdf"/>
                        </label>
                        <div class="form-group" id="exportWishListAsPdfBody">
                            <c:if test="${!basketType.equals('ORDER')}">
                                <label for="pdfNettoPrice">
                                    <input type="checkbox"
                                            id="exportWishListAsPdfNettoPrice"
                                            name="exportPdfNettoPrice"
                                            value="true"
                                            class="toggle-button__input"/>
                                    <spring:theme code="text.account.savedcart.export.aspdf.netprices"/>
                                </label><br>
                            </c:if>
                            <label for="pdfIncludeImage">
                                <input  type="checkbox"
                                        id="exportWishListAsPdfIncludeImage"
                                        name="exportPdfIncludeImage"
                                        value="true"
                                        class="toggle-button__input" checked/>
                                <spring:theme code="text.account.savedcart.export.aspdf.showimage" />
                            </label><br>
                            <label for="pdfIncludeTechnical">
                                <input  type="checkbox"
                                        id="exportWishListAsPdfIncludeTechnical"
                                        name="exportPdfTechnical"
                                        value="true"
                                        class="toggle-button__input"
                                        checked/>
                                <spring:theme code="text.account.savedcart.export.aspdf.technical" />
                            </label><br>
                            <p><spring:theme code="text.account.savedcart.export.aspdf.emptylines"/></p>
                            <input  type="number"
                                    id="exportWishListAsPdfHeaderLines"
                                    name="exportPdfLines"
                                    class="text form-control js-current-cart-name"
                                    value="0"
                                    min="0"
                                    max="100"
                                    maxlength="2"/>
                        </div>
                        <br>
                        <label for="exportAsCsv">
                            <input type="radio" id="exportWishListAsExcel" name="exportType" value="xls" class="js-keep-restored-cart">
                            <spring:theme code="text.account.savedcart.export.asexcel"/>
                        </label>
                        <div class="form-group" id="exportWishListAsExcelBody">
                            <c:if test="${!basketType.equals('ORDER')}">
                                <label for="pdfNettoPrice">
                                    <input  type="checkbox"
                                            id="exportWishListAsExcelNettoPrice"
                                            name="exportExcelNettoPrice"
                                            value="true"
                                            class="toggle-button__input"/>
                                    <spring:theme code="text.account.savedcart.export.asexcel.netprices"/>
                                </label>
                            </c:if>
                        </div>
                        <input  type="hidden"
                                id="cartType"
                                name="basketType"
                                value="${basketType}"/>
                        <input  type="hidden"
                                id="basketName"
                                name="basketName"
                                value="${basketName}"/>

                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal"><spring:theme code="text.button.cancel"/></button>
                <button type="button" class="btn btn-default btn-do-export" value="dododo">
                    <spring:theme code="savedcart.export.btn.text"/>
                </button>
            </div>
        </div>
    </div>
    </form>
</div>
