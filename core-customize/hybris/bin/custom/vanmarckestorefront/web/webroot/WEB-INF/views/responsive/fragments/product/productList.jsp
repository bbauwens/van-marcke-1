<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/addons/vanmarckeblueaddon/responsive/product" %>

<c:set var="isMoreToLoad" value="${((searchPageData.pagination.currentPage + 1) * searchPageData.pagination.pageSize) < searchPageData.pagination.totalNumberOfResults}"/>
<input type="hidden" id="search-product-left-to-load-${searchPageData.pagination.currentPage}" value="${isMoreToLoad}"/>
<input type="hidden" class="currentPage" value="${searchPageData.pagination.currentPage}"/>

<c:forEach items="${searchPageData.results}" var="product" varStatus="status">
    <product:productListerListViewItem product="${product}"
                                       list="${categoryName}"
                                       categoryCode="${categoryCode}"
                                       position="${status.index + 1}" />
</c:forEach>
