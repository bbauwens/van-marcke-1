package com.vanmarcke.storefront.interceptors.beforecontroller;

import com.vanmarcke.storefront.security.evaluator.impl.RequireHardLoginEvaluator;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeControllerHandler;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;

public class RequireHardLoginBeforeControllerHandler implements BeforeControllerHandler {
    public static final String SECURE_GUID_SESSION_KEY = "acceleratorSecureGUID";
    private static final Logger LOG = Logger.getLogger(RequireHardLoginBeforeControllerHandler.class);
    private String loginUrl;
    private String loginAndCheckoutUrl;
    private RedirectStrategy redirectStrategy;
    private RequireHardLoginEvaluator requireHardLoginEvaluator;

    protected String getLoginUrl() {
        return this.loginUrl;
    }

    @Required
    public void setLoginUrl(final String loginUrl) {
        this.loginUrl = loginUrl;
    }

    protected RedirectStrategy getRedirectStrategy() {
        return this.redirectStrategy;
    }

    @Required
    public void setRedirectStrategy(final RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

    public String getLoginAndCheckoutUrl() {
        return this.loginAndCheckoutUrl;
    }

    @Required
    public void setLoginAndCheckoutUrl(final String loginAndCheckoutUrl) {
        this.loginAndCheckoutUrl = loginAndCheckoutUrl;
    }

    protected RequireHardLoginEvaluator getRequireHardLoginEvaluator() {
        return this.requireHardLoginEvaluator;
    }

    @Required
    public void setRequireHardLoginEvaluator(final RequireHardLoginEvaluator requireHardLoginEvaluator) {
        this.requireHardLoginEvaluator = requireHardLoginEvaluator;
    }

    @Override
    public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
                                    final HandlerMethod handler) throws Exception {
        // We only care if the request is secure
        if (request.isSecure()) {
            // Check if the handler has our annotation
            final RequireHardLogIn annotation = findAnnotation(handler, RequireHardLogIn.class);
            if (annotation != null) {
                final boolean redirect = this.requireHardLoginEvaluator.evaluate(request, response);

                if (redirect) {
                    LOG.warn("Redirection required");
                    getRedirectStrategy().sendRedirect(request, response, getRedirectUrl(request));
                    return false;
                }
            }
        }

        return true;
    }

    protected String getRedirectUrl(final HttpServletRequest request) {
        if (request == null) {
            return getLoginUrl();
        } else if (request.getServletPath().contains("checkout")) {
            return getLoginAndCheckoutUrl() + request.getRequestURI();
        } else {
            return getLoginUrl() + request.getRequestURI();
        }
    }

    protected <T extends Annotation> T findAnnotation(final HandlerMethod handlerMethod, final Class<T> annotationType) {
        // Search for method level annotation
        final T annotation = handlerMethod.getMethodAnnotation(annotationType);
        if (annotation != null) {
            return annotation;
        }

        // Search for class level annotation
        return AnnotationUtils.findAnnotation(handlerMethod.getBeanType(), annotationType);
    }
}
