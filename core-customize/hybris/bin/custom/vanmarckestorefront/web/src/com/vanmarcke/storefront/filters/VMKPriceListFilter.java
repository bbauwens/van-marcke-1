package com.vanmarcke.storefront.filters;

import com.vanmarcke.core.model.CommerceGroupModel;
import com.vanmarcke.services.commercegroup.VMKCommerceGroupService;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static de.hybris.platform.europe1.constants.Europe1Constants.PARAMS.UPG;
import static de.hybris.platform.europe1.constants.Europe1Constants.PARAMS.UTG;

/**
 * Responsible for setting the current price list on the session.
 */
public class VMKPriceListFilter extends OncePerRequestFilter {

    private final VMKCommerceGroupService commerceGroupService;

    private final SessionService sessionService;

    public VMKPriceListFilter(final VMKCommerceGroupService commerceGroupService, final SessionService sessionService) {
        this.commerceGroupService = commerceGroupService;
        this.sessionService = sessionService;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) throws ServletException, IOException {
        final CommerceGroupModel commerceGroup = commerceGroupService.getCurrentCommerceGroup();

        final UserPriceGroup userPriceGroup = commerceGroup != null ? commerceGroup.getUserPriceGroup() : null;
        if (userPriceGroup != null) {
            sessionService.setAttribute(UPG, userPriceGroup);
        }

        final UserTaxGroup userTaxGroup = commerceGroup != null ? commerceGroup.getUserTaxGroup() : null;
        if (userTaxGroup != null) {
            sessionService.setAttribute(UTG, userTaxGroup);
        }

        filterChain.doFilter(request, response);
    }
}