/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.vanmarcke.storefront.filters;

import com.vanmarcke.facades.principal.user.VMKUserFacade;
import com.vanmarcke.storefront.security.cookie.CartRestoreCookieGenerator;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.fest.util.Collections;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * The {@link CartRestorationFilter} class is used to restore the shopping cart in the session.
 *
 * @author Cristian Stoica
 * @since 19-11-2021
 */
public class CartRestorationFilter extends OncePerRequestFilter {

    private static final Logger LOG = Logger.getLogger(CartRestorationFilter.class);

    private static final String SESSION_CART_PARAMETER_NAME = "cart";

    private final CartRestoreCookieGenerator cartRestoreCookieGenerator;
    private final BaseSiteService baseSiteService;
    private final CartService cartService;
    private final SessionService sessionService;
    private final CartFacade cartFacade;
    private final VMKUserFacade userFacade;

    /**
     * Creates a new instance of the {@link CartRestorationFilter} class.
     *
     * @param cartRestoreCookieGenerator the cart restore cookie generator
     * @param baseSiteService            the base site service
     * @param cartService                the cart service
     * @param sessionService             the session service
     * @param cartFacade                 the cart facade
     * @param userFacade                 the user facade
     */
    public CartRestorationFilter(final CartRestoreCookieGenerator cartRestoreCookieGenerator,
                                 final BaseSiteService baseSiteService,
                                 final CartService cartService,
                                 final SessionService sessionService,
                                 final CartFacade cartFacade,
                                 final VMKUserFacade userFacade) {
        this.cartRestoreCookieGenerator = cartRestoreCookieGenerator;
        this.baseSiteService = baseSiteService;
        this.cartService = cartService;
        this.sessionService = sessionService;
        this.cartFacade = cartFacade;
        this.userFacade = userFacade;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doFilterInternal(final HttpServletRequest request,
                                 final HttpServletResponse response,
                                 final FilterChain filterChain) throws IOException, ServletException {
        restoreCart(request, response);
        filterChain.doFilter(request, response);
    }

    /**
     * Restores the cart in the session.
     *
     * @param request  the HTTP request
     * @param response the HTTP response
     */
    private void restoreCart(final HttpServletRequest request,
                             final HttpServletResponse response) {
        if (userFacade.isAnonymousUser()) {
            processAnonymousUser(request, response);
        } else {
            restoreLoggedInUserCart();
        }
    }

    /**
     * handles the cart restoration case when the current user is a logged in user (not anonymous)
     */
    protected void restoreLoggedInUserCart() {
        if (!isValidCartOnSession()) {
            restoreOrCreateValidCart(null);
        }
    }

    /**
     * check if current session has a cart set which is on the same site as the current session site
     *
     * @return true if the session has a cart and the cart site is the same as the session site, false otherwise
     */
    protected boolean isValidCartOnSession() {
        return cartService.hasSessionCart()
                && baseSiteService.getCurrentBaseSite().equals(baseSiteService
                .getBaseSiteForUID(cartService.getSessionCart().getSite().getUid()));    }

    /**
     * handles the cart restoration case when the current user is anonymous
     *
     * @param request the HTTP request
     * @param response the HTTP response
     */
    protected void processAnonymousUser(final HttpServletRequest request, final HttpServletResponse response) {
        if (isValidCartOnSession()) {
            final String guid = cartService.getSessionCart().getGuid();

            if (!StringUtils.isEmpty(guid)) {
                cartRestoreCookieGenerator.addCookie(response, guid);
            }
        } else {
            restoreAnonymousCart(request, response);
        }
    }
    /**
     * restore anonymous cart from cookie or create new cart and store it on a cookie for anonymous
     *
     * @param request the HTTP request
     * @param response the HTTP response
     */
    protected void restoreAnonymousCart(final HttpServletRequest request, HttpServletResponse response) {
        String cartGuid = readCartFromCookie(request);

        if (!StringUtils.isEmpty(cartGuid)) {
            String sessionCartGuid = restoreOrCreateValidCart(cartGuid);
            cartRestoreCookieGenerator.addCookie(response, sessionCartGuid);
        } else {
            CartModel newCart = cartService.getSessionCart();
            if (newCart != null) {
                sessionService.removeAttribute(SESSION_CART_PARAMETER_NAME);
                cartRestoreCookieGenerator.addCookie(response, newCart.getGuid());
                cartService.setSessionCart(newCart);
            }
            sessionService.setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.FALSE);
        }
    }

    /**
     * Restore the cart based on a cart guid and create new cart if restored cart is not valid
     *
     * @param cartGuid the cart guid to be restored
     * @return the new or restored cart guid
     */
    protected String restoreOrCreateValidCart(String cartGuid) {
        doRestoreCart(cartGuid);
        CartRestorationData cartRestorationData = sessionService.getAttribute(WebConstants.CART_RESTORATION);
        setCartRestaurationSessionAttributes(cartRestorationData);
        if (!isValidCartOnSession()) {
            sessionService.removeAttribute(SESSION_CART_PARAMETER_NAME);
            CartModel newCart = cartService.getSessionCart();
            cartService.setSessionCart(newCart);
            return newCart.getGuid();
        }

        return cartGuid;
    }

    /**
     * restore cart by guid
     *
     * @param cartGuid the cart guid
     */
    protected void doRestoreCart(String cartGuid) {
        try {
            sessionService.setAttribute(WebConstants.CART_RESTORATION,
                    cartFacade.restoreSavedCart(cartGuid));
        } catch (final CommerceCartRestorationException e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(e);
            }
            sessionService.setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
                    WebConstants.CART_RESTORATION_ERROR_STATUS);
        }
    }

    /**
     * Set necessary session attributes after restoring a cart
     *
     * @param cartRestorationData the restoration data resul
     */
    protected void setCartRestaurationSessionAttributes(CartRestorationData cartRestorationData) {
        if (cartRestorationData == null || Collections.isEmpty(cartRestorationData.getModifications())) {
            sessionService.setAttribute(WebConstants.CART_RESTORATION, null);
            sessionService.setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.FALSE);
        } else {
            sessionService.setAttribute(WebConstants.CART_RESTORATION, cartRestorationData);
            sessionService.setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
        }
    }
    /**
     * Returns the cart from the stored cookie.
     *
     * @param request the HTTP request
     * @return the point of service
     */
    private String readCartFromCookie(final HttpServletRequest request) {
        String cartGuid = null;
        final String cookieName = cartRestoreCookieGenerator.getCookieName();
        final String defaultCookiePath = cartRestoreCookieGenerator.getCookiePath();
        final Cookie[] cookies = request.getCookies();
        if (StringUtils.isNotEmpty(cookieName) && ArrayUtils.isNotEmpty(cookies)) {
            cartGuid = Arrays
                    .stream(cookies)
                    .filter(cookie -> cookieName.equals(cookie.getName()) && (defaultCookiePath.equals(cookie.getPath()) || StringUtils.isEmpty(cookie.getPath())))
                    .findFirst()
                    .map(Cookie::getValue)
                    .orElse(null);
        }
        return cartGuid;
    }
}
