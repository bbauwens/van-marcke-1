package com.vanmarcke.storefront.filters;

import com.vanmarcke.facades.cart.VMKCartFacade;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.apache.commons.lang3.StringUtils.contains;

/**
 * Responsible for checking and setting in which mode the cart is.
 *
 * @author Taki Korovessis
 * @since 2-10-2019
 */
public class VMKCartCheckoutModeDecisionFilter extends OncePerRequestFilter {

    private static final String URL_CHECKOUT = "/checkout/multi";
    private static final String URL_SAFERPAY = "/checkout/payment/saferpay";

    private final VMKCartFacade cartFacade;

    /**
     * Provides an instance of the VMKCartCheckoutModeDecisionFilter.
     *
     * @param cartFacade the cart facade
     */
    public VMKCartCheckoutModeDecisionFilter(VMKCartFacade cartFacade) {
        this.cartFacade = cartFacade;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String requestUrl = request.getRequestURL().toString();
        boolean checkoutMode = contains(requestUrl, URL_CHECKOUT) || contains(requestUrl, URL_SAFERPAY);

        cartFacade.updateCartCheckoutMode(checkoutMode);
        filterChain.doFilter(request, response);
    }
}