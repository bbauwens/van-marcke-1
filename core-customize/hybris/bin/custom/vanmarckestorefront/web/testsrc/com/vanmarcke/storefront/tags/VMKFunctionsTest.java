package com.vanmarcke.storefront.tags;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VMKFunctionsTest {

    @Mock
    private Converter<ProductModel, ProductData> productUrlConverter;
    @Mock
    private Converter<CategoryModel, CategoryData> categoryUrlConverter;

    @Test
    public void testGetUrlForCMSLinkComponent_ContentPage_Homepage() {
        CMSLinkComponentModel component = mock(CMSLinkComponentModel.class);
        ContentPageModel contentPage = mock(ContentPageModel.class);

        when(component.getContentPage()).thenReturn(contentPage);
        when(contentPage.isHomepage()).thenReturn(Boolean.TRUE);
        String url = VMKFunctions.getUrlForCMSLinkComponent(component, productUrlConverter, categoryUrlConverter);

        assertThat(url).isEqualTo("/");
        verifyZeroInteractions(productUrlConverter);
        verifyZeroInteractions(categoryUrlConverter);
    }

    @Test
    public void testGetUrlForCMSLinkComponent_ContentPage_NotHomepage() {
        CMSLinkComponentModel component = mock(CMSLinkComponentModel.class);
        ContentPageModel contentPage = mock(ContentPageModel.class);

        when(component.getContentPage()).thenReturn(contentPage);
        when(contentPage.isHomepage()).thenReturn(Boolean.FALSE);
        when(contentPage.getLabel()).thenReturn("/Other");
        String url = VMKFunctions.getUrlForCMSLinkComponent(component, productUrlConverter, categoryUrlConverter);

        assertThat(url).isEqualTo("/Other");
        verifyZeroInteractions(productUrlConverter);
        verifyZeroInteractions(categoryUrlConverter);
    }

    @Test
    public void testGetUrlForCMSLinkComponent_Category() {
        CMSLinkComponentModel component = mock(CMSLinkComponentModel.class);
        CategoryModel category = mock(CategoryModel.class);
        CategoryData categoryData = mock(CategoryData.class);

        when(component.getContentPage()).thenReturn(null);
        when(component.getCategory()).thenReturn(category);
        when(categoryUrlConverter.convert(category)).thenReturn(categoryData);
        when(categoryData.getUrl()).thenReturn("/c/category01");
        String url = VMKFunctions.getUrlForCMSLinkComponent(component, productUrlConverter, categoryUrlConverter);

        assertThat(url).isEqualTo("/c/category01");
        verifyZeroInteractions(productUrlConverter);
        verify(categoryUrlConverter).convert(category);
    }

    @Test
    public void testGetUrlForCMSLinkComponent_Product() {
        CMSLinkComponentModel component = mock(CMSLinkComponentModel.class);
        ProductModel product = mock(ProductModel.class);
        ProductData productData = mock(ProductData.class);

        when(component.getContentPage()).thenReturn(null);
        when(component.getCategory()).thenReturn(null);
        when(component.getProduct()).thenReturn(product);
        when(productUrlConverter.convert(product)).thenReturn(productData);
        when(productData.getUrl()).thenReturn("/p/1234");
        String url = VMKFunctions.getUrlForCMSLinkComponent(component, productUrlConverter, categoryUrlConverter);

        assertThat(url).isEqualTo("/p/1234");
        verify(productUrlConverter).convert(product);
        verifyZeroInteractions(categoryUrlConverter);
    }

    @Test
    public void testGetUrlForCMSLinkComponent_LocalizedURL() {
        CMSLinkComponentModel component = mock(CMSLinkComponentModel.class);

        when(component.getContentPage()).thenReturn(null);
        when(component.getCategory()).thenReturn(null);
        when(component.getProduct()).thenReturn(null);
        when(component.getLocalizedUrl()).thenReturn("/localized");
        String url = VMKFunctions.getUrlForCMSLinkComponent(component, productUrlConverter, categoryUrlConverter);

        assertThat(url).isEqualTo("/localized");
        verifyZeroInteractions(productUrlConverter);
        verifyZeroInteractions(categoryUrlConverter);
    }

    @Test
    public void testGetUrlForCMSLinkComponent_FallbackURL() {
        CMSLinkComponentModel component = mock(CMSLinkComponentModel.class);

        when(component.getContentPage()).thenReturn(null);
        when(component.getCategory()).thenReturn(null);
        when(component.getProduct()).thenReturn(null);
        when(component.getLocalizedUrl()).thenReturn(null);
        when(component.getUrl()).thenReturn("/fallback");
        String url = VMKFunctions.getUrlForCMSLinkComponent(component, productUrlConverter, categoryUrlConverter);

        assertThat(url).isEqualTo("/fallback");
        verifyZeroInteractions(productUrlConverter);
        verifyZeroInteractions(categoryUrlConverter);
    }

    @Test
    public void testGetUrlForCMSLinkComponent_NoURL() {
        CMSLinkComponentModel component = mock(CMSLinkComponentModel.class);

        when(component.getContentPage()).thenReturn(null);
        when(component.getCategory()).thenReturn(null);
        when(component.getProduct()).thenReturn(null);
        when(component.getLocalizedUrl()).thenReturn(null);
        when(component.getUrl()).thenReturn(null);
        String url = VMKFunctions.getUrlForCMSLinkComponent(component, productUrlConverter, categoryUrlConverter);

        assertThat(url).isEqualTo(null);
        verifyZeroInteractions(productUrlConverter);
        verifyZeroInteractions(categoryUrlConverter);
    }
}