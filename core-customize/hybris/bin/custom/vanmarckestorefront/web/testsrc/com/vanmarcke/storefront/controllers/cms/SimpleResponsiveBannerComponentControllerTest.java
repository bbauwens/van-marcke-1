package com.vanmarcke.storefront.controllers.cms;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorcms.model.components.SimpleBannerComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.media.MediaContainerModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.*;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SimpleResponsiveBannerComponentControllerTest {

    private static final String LOCALIZED_URL = RandomStringUtils.randomAlphabetic(10);
    private static final String DEFAULT_URL = RandomStringUtils.randomAlphabetic(10);
    private static final Locale DUMMY_LOCALE = Locale.JAPANESE;

    @Mock
    private ResponsiveMediaFacade responsiveMediaFacade;

    @Mock
    private CommerceCommonI18NService commerceCommonI18NService;

    @InjectMocks
    private SimpleResponsiveBannerComponentController bannerController;

    @Test
    public void testFillModelWithLocalizedUrl() {
        HttpServletRequest request =  new MockHttpServletRequest();
        Model model = new ExtendedModelMap();

        SimpleResponsiveBannerComponentModel bannerComponent = mock(SimpleResponsiveBannerComponentModel.class);

        List<ImageData> mediaDataList = new ArrayList<>();
        MediaContainerModel mediaContainer = new MediaContainerModel();

        when(commerceCommonI18NService.getCurrentLocale()).thenReturn(DUMMY_LOCALE);
        when(bannerComponent.getMedia(DUMMY_LOCALE)).thenReturn(mediaContainer);
        when(bannerComponent.getUrlLink()).thenReturn(LOCALIZED_URL);
        when(responsiveMediaFacade.getImagesFromMediaContainer(mediaContainer))
                .thenReturn(mediaDataList);

        bannerController.fillModel(request, model, bannerComponent);

        Assertions.
                assertThat(model.asMap().get("medias"))
                .isEqualTo(mediaDataList);

        Assertions
                .assertThat(model.asMap().get("urlLink"))
                .isEqualTo(LOCALIZED_URL);

        verify(commerceCommonI18NService).getCurrentLocale();
        verify(responsiveMediaFacade).getImagesFromMediaContainer(mediaContainer);
        verifyNoMoreInteractions(commerceCommonI18NService, responsiveMediaFacade);
    }

    @Test
    public void testFillModelWithDefaultUrl() {
        HttpServletRequest request =  new MockHttpServletRequest();
        Model model = new ExtendedModelMap();

        SimpleResponsiveBannerComponentModel bannerComponent = mock(SimpleResponsiveBannerComponentModel.class);
        bannerComponent.setUrlLink(DEFAULT_URL);

        List<ImageData> mediaDataList = new ArrayList<>();
        MediaContainerModel mediaContainer = new MediaContainerModel();

        when(commerceCommonI18NService.getCurrentLocale()).thenReturn(DUMMY_LOCALE);
        when(bannerComponent.getMedia(DUMMY_LOCALE)).thenReturn(mediaContainer);
        when(bannerComponent.getUrlLink()).thenReturn(DEFAULT_URL);
        when(responsiveMediaFacade.getImagesFromMediaContainer(mediaContainer))
                .thenReturn(mediaDataList);

        bannerController.fillModel(request, model, bannerComponent);

        Assertions.
                assertThat(model.asMap().get("medias"))
                .isEqualTo(mediaDataList);

        Assertions
                .assertThat(model.asMap().get("urlLink"))
                .isEqualTo(DEFAULT_URL);

        verify(commerceCommonI18NService).getCurrentLocale();
        verify(responsiveMediaFacade).getImagesFromMediaContainer(mediaContainer);
        verifyNoMoreInteractions(commerceCommonI18NService, responsiveMediaFacade);
    }
}